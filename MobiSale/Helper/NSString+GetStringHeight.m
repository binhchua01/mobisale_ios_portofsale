//
//  NSString+GetStringHeight.m
//  MobiSale
//
//  Created by ISC-DanTT on 4/8/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "NSString+GetStringHeight.h"

@implementation NSString (GetStringHeight)

- (CGFloat)getStringHeightWithFontSize:(NSInteger)fontSize withSizeMake:(CGSize)constraint {    
    // constratins the size of the table row according to the text
    CGRect textRect = [self boundingRectWithSize:constraint options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Light" size:fontSize]} context:nil];
    
    return textRect.size.height;
    
}

@end
