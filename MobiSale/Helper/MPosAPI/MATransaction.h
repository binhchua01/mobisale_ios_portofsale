//
//  MATransaction.h
//  MPosAPI
//
//  Created by LoiTT on 3/4/14.
//  Last updated by LoiTT 2/3/15.
//  Copyright (c) 2015 FPT Information System. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MATransaction : NSObject <NSCopying, NSCoding>

/// Transaction information
@property (strong, readonly, nonatomic) NSString *referenceId;           // Reference ID
@property (strong, readonly, nonatomic) NSString *transactionKey;        // Transaction Key
@property (assign, readonly, nonatomic) unsigned long long amount;       // Amount
@property (strong, readonly, nonatomic) NSString *paymentType;           // Payment type

@property (strong, readonly, nonatomic) NSString *responseCode;          // Response code, response from bank
@property (strong, readonly, nonatomic) NSString *referenceNumber;       // Referenced number, response from bank
@property (strong, readonly, nonatomic) NSString *approvedCode;          // Approve code, response from bank
@property (strong, readonly, nonatomic) NSString *terminalId;            // Terminal ID
@property (strong, readonly, nonatomic) NSString *merchantId;            // Merchant ID
@property (strong, readonly, nonatomic) NSString *maskedCardNumber;      // Masked card number
@property (strong, readonly, nonatomic) NSString *cardHolder;            // Card owner's name
@property (strong, readonly, nonatomic) UIImage *signature;              // Signature
@property (strong, readonly, nonatomic) NSDate *transactionDate;         // Transaction date

/// Additional information
@property (assign, readonly, nonatomic) BOOL isTransactionAlreadyPaid;   // Flag to identify that transaction has been paid previously.
@end
