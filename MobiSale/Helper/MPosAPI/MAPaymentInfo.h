//
//  MAPaymentInfo.h
//  MPosAPI
//
//  Created by LoiTT on 3/4/14.
//  Last updated by LoiTT on 2/3/15.
//  Copyright (c) 2015 FPT Information System. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MAConstant.h"

@interface MAPaymentInfo : NSObject <NSCopying, NSCoding>

@property (strong, nonatomic) NSString *referenceId;            // Reference ID
@property (assign, nonatomic) unsigned long long amount;        // Amount

// Optional
@property (assign, nonatomic) BOOL checkTransactionAlreadyPaid; // Check if transaction has been paid or not yet.
@property (strong, nonatomic) NSString *loggedInUsername;       // The logged in username on custom app
@property (strong, nonatomic) NSString *customerName;           // Customer name
@property (strong, nonatomic) NSString *customerMobile;         // Customer phone (mobile)
@property (strong, nonatomic) NSString *customerEmail;          // Customer email
@property (strong, nonatomic) NSString *paymentDescription;     // Payment description (note)
@property (assign, nonatomic) MAPaymentMethod paymentMethod;    // Payment method
@property (assign, nonatomic) MAPaymentStep paymentStep;        // Display view in step

// Factory method.
+ (MAPaymentInfo *)paymentInfoWithReferenceId:(NSString *)refId
                                       amount:(unsigned long long)amount
                                 customerName:(NSString *)customerName
                               customerMobile:(NSString *)customerMobile
                                customerEmail:(NSString *)customerEmail
                           paymentDescription:(NSString *)paymentDescription
                                  paymentStep:(MAPaymentStep)paymentStep __deprecated_msg("Use new method instead.");

+ (MAPaymentInfo *)paymentInfoWithReferenceId:(NSString *)refId
                                       amount:(unsigned long long)amount
                                 customerName:(NSString *)customerName
                               customerMobile:(NSString *)customerMobile
                                customerEmail:(NSString *)customerEmail
                           paymentDescription:(NSString *)paymentDescription
                                paymentMethod:(MAPaymentMethod)paymentMethod
                                  paymentStep:(MAPaymentStep)paymentStep
                             loggedInUsername:(NSString *)username;
@end
