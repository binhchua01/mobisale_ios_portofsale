//
//  MAConstant.h
//  MPosAPI
//
//  Created by LoiTT on 3/4/14.
//  Last updated by LoiTT on 2/3/15.
//  Copyright (c) 2015 FPT Information System. All rights reserved.
//

#import <Foundation/Foundation.h>

/// URL type
typedef enum _MAURLType {
    MAURLTypeUndefined = 0, // Current url is undefined
    MAURLTypeSuccess,       // Current url is x-success
    MAURLTypeError,         // Current url is x-error
    MAURLTypeCancel,        // Current url is x-cancel
} MAURLType;

/// Request operator
typedef enum _MARequestOperator {
    MARequestOperatorPay = 0,                           // Pay operator
    MARequestOperatorRegisterDevice = 1,                // Register device operator
    MARequestOperatorVoidTransaction = 2,               // Void transaction operator
} MARequestOperator;

/// Payment method
typedef enum _MAPaymentMethod {
    MAPaymentMethodPayByCard = 0,                       // Pay by card (ATM, VISA,...)
    MAPaymentMethodPayByTransfer = 1,                   // Pay by transfer
    MAPaymentMethodDefault = MAPaymentMethodPayByCard,  // Default is pay by card
} MAPaymentMethod;

/// Payment step
typedef enum _MAPaymentStep {
    MAPaymentStepInputData __deprecated_msg("Use MAPaymentStepFirstStep instead.") = 1,   // Input payment data step
    MAPaymentStepGetCardData __deprecated_msg("Use MAPaymentStepSecondStep instead.") = 2, // Get card data step
    
    MAPaymentStepFirstStep = 1,                         // Input payment data step
    MAPaymentStepSecondStep = 2,                        // Get card data step if pay by card
                                                        // Confirm step if pay by transfer
    MAPaymentStepDefault = MAPaymentStepFirstStep,
} MAPaymentStep;

/// Handle status
typedef enum _MAStatus {
    MAStatusSuccess = 0,                                 // Success
    MAStatusAPIOperationError = 1,                       // API Operation Error
    MAStatusNotSetAuthenticationParams = 2,              // Authentication Parameters are not set
    MAStatusNotSetXCallbackParams = 3,                   // Parameters for x-callback are not set
    MAStatusInvalidReferenceId = 4,                      // Invalid reference id
    MAStatusInvalidAmount = 5,                           // Invalid amount
    MAStatusInvalidCustomerName = 6,                     // Invalid customer name
    MAStatusInvalidCustomerMobile = 7,                   // Invalid customer mobile
    MAStatusInvalidCustomerEmail = 8,                    // Invalid customer email
    MAStatusCanNotOpenPaymentApp = 9,                    // Payment application cannot be opened
    MAStatusBadURL = 10,                                 // Handle URL is not registered to API
    MAStatusCanNotReadResponseData = 11,                 // Cannot read response data from payment app
    MAStatusCanNotDecryptData = 12,                      // Cannot decrypt response data from payment app
    MAStatusCanNotParseJsonData = 13,                    // Cannot parse json from decrypted data.
    MAStatusCanNotReadJsonData = 14,                     // Cannot read json data which was parsed.
    MAStatusInvalidTransactionKey = 15,                  // Invalid transaction key
    MAStatusInprogressOtherRequest = 999,                // Inprogress other request
} MAStatus;

/// Error code
typedef enum _MAErrorCode {
    MAErrorCodePaymentAppOperationError = 100,            // Processing error occurred on Payment App
    MAErrorCodePaymentSystemError = 101,                  // Error while processing on Payment system (Cannot call service, processing error occured on server, read server data fails, ...)
    MAErrorCodePaymentAppCanNotReadRecievedData = 102,    // Payment App failed to read received data
    MAErrorCodeAuthenticationError = 103,                 // Application authentication failed
    MAErrorCodeInvalidPaymentInfo __deprecated_msg("Use MAErrorCodeInvalidRequestData instead.") = 104,// Invalid payment info
    MAErrorCodeInvalidRequestData = 104,                  // Invalid request data (Including: payment info, void transaction info)
    MAErrorCodeBankSystemError = 105,                     // Error while processing Bank system
    MAErrorCodePaymentTimeout = 106,                      // Processing timeout. Please try again!
    MAErrorCodePaymentAppDenyRequest = 107,               // Payment App denied payment request.
    MAErrorCodeCardProcessingError = 108,                 // Card proccessing error.
    MAErrorCodeCompanyNotSupportPaymentMethod = 109,      // Company does not support <payment method>.
    MAErrorCodeCompanyNotSupportRequestOperator = 110,    // Company does not support request operator.
    MAErrorCodeTransactionNotExistOnPaymentSystem = 111,  // Transaction does not exist on payment system.
    MAErrorCodeTransactionCanNotBeVoided = 112,           // Transaction cannot be voided.
} MAErrorCode;


/// Payment type
extern NSString *const kMAPaymentTypeTransfer;
extern NSString *const kMAPaymentTypeATM;
extern NSString *const kMAPaymentTypeVisa;
extern NSString *const kMAPaymentTypeMasterCard;
extern NSString *const kMAPaymentTypeOther;

/// Constant for post notification
extern NSString *const kMAMPosAPIRequestOperatorKey;
extern NSString *const kMAMPosAPIStatusKey;
extern NSString *const kMAMPosAPITransctionKey;
extern NSString *const kMAMPosAPIErrorKey;

extern NSString *const kMAMPosAPICallBackSuccessURLNotification;
extern NSString *const kMAMPosAPICallBackErrorURLNotification;
extern NSString *const kMAMPosAPICallBackCancelURLNotification;


