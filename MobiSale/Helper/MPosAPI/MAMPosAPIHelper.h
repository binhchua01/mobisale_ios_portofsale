//
//  MAMPosAPIHelper.h
//  MPosAPI
//
//  Created by LoiTT on 2/3/15.
//  Last updated by LoiTT on 2/25/15.
//  Copyright (c) 2015 FPT Information System. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "MAMPosAPI.h"

#pragma mark - Payment helper delegate
/// Protocol to handle API Events
@protocol MAMPosAPIHelperDelegate <NSObject>

@optional
/**
 @method onCallBackXSuccessWithHandleStatus: requestOperator: transaction:
 @description Handle x-success url results.
 */
- (void)onCallBackXSuccessWithHandleStatus:(MAStatus)status
                           requestOperator:(MARequestOperator)requestOperator
                               transaction:(MATransaction *)transaction;

/**
 @method onCallBackXErrorWithHandleStatus: requestOperator: error: transaction:
 @description Handle x-error url results.
 */
- (void)onCallBackXErrorWithHandleStatus:(MAStatus)status
                         requestOperator:(MARequestOperator)requestOperator
                                   error:(MAError *)error
                             transaction:(MATransaction *)transaction;

/**
 @method onCallBackXCancelWithHandleStatus: requestOperator: transaction:
 @description Handle x-cancel url results.
 */
- (void)onCallBackXCancelWithHandleStatus:(MAStatus)status
                          requestOperator:(MARequestOperator)requestOperator
                              transaction:(MATransaction *)transaction;

@end


#pragma mark - Payment helper class
/// mPOS API helper
@interface MAMPosAPIHelper : NSObject <UIAlertViewDelegate>

/// Helper delegate.
@property (assign, nonatomic) id<MAMPosAPIHelperDelegate> delegate;

/// mPOS API object. Please set up your credential key, credential password, x-callback urls for mPOS API before calling API functions.
@property (readonly, nonatomic) MAMPosAPI *mPosAPI;

/**
 @method sharedHelper
 @description Get global object of MAMPosAPIHelper class.
 */
+ (MAMPosAPIHelper *)sharedAPIHelper;

/**
 @method reset
 @description Set default value for MAMPosAPIHelper object.
 */
- (void)reset;

/**
 @method registerCurrentDeviceWithUsername:
 @description Call Payment App to send register device request.
 @param username: the logged in username on custom app
 @return Handling result (Refer to MAMPosAPI.h)
 */
- (MAStatus)registerCurrentDeviceWithUsername:(NSString *)username;

/**
 @method payWithPaymentInfo:
 @description Call Payment App to send payment request.
 @param paymentInfo: Payment information
 @return Handling result (Refer to MAMPosAPI.h)
 */
- (MAStatus)payWithPaymentInfo:(MAPaymentInfo *)paymentInfo;

/**
 @method voidTransactionWithTransactionInfo:
 @description Send void transaction request to payment app (Open payment url). If void transaction information is invalid (Reference ID is nil, Length of Transaction key is larger than 20, ...) or x-callback params, authentication params do not be set, request will not be sent.
 @param voidInfo: Void transaction Information (Reference ID, TransactionKey).
 @returns andling result (Refer to MAMPosAPI.h)
 */
- (MAStatus)voidTransactionWithTransactionInfo:(MAVoidTransactionInfo *)voidInfo;


/**
 @method handleCallBackURLFromPaymentApp:
 @description Check whether opened url is a callback url. If yes, this method will extract response data from callback url and call delegate methods to sending response data. This method should be called in AppDelegate as below:
     //   - (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
     //       // Handle URL.
     //       return [[MAMPosAPIHelper sharedHelper] handleCallBackURLFromPaymentApp:url];
     //   }
 
 @param callbackURL: Open url.
 @return The flag to determine that url is being handled.
 */
- (BOOL)handleCallBackURLFromPaymentApp:(NSURL *)callbackURL;
@end
