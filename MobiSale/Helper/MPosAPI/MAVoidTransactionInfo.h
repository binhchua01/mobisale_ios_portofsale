//
//  MAVoidTransactionInfo.h
//  MPosAPI
//
//  Created by LoiTT on 2/10/15.
//  Copyright (c) 2015 FPT Information System. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAVoidTransactionInfo : NSObject

@property (strong, nonatomic) NSString *referenceId;          // Reference ID
@property (strong, nonatomic) NSString *transactionKey;       // Transaction key

// Optional
@property (strong, nonatomic) NSString *loggedInUsername;     // The logged in username on custom app

// Factory method
+ (MAVoidTransactionInfo *)voidTransactionInfoWithReferenceId:(NSString *)refId
                                               transactionKey:(NSString *)transactionKey;

+ (MAVoidTransactionInfo *)voidTransactionInfoWithReferenceId:(NSString *)refId
                                               transactionKey:(NSString *)transactionKey
                                             loggedInUsername:(NSString *)username;
@end
