//
//  MAError.h
//  MPosAPI
//
//  Created by LoiTT on 3/4/14.
//  Copyright (c) 2015 FPT Information System. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MAConstant.h"

@interface MAError : NSObject <NSCopying, NSCoding>

@property (assign, readonly, nonatomic) MAErrorCode code;    // Message Code
@property (strong, readonly, nonatomic) NSString *content;   // Message Content
@end
