//
//  MPosAPI.h
//  MPosAPI
//
//  Created by LoiTT on 3/4/14.
//  Last updated by LoiTT on 2/25/15.
//  Version 1.2.3.1
//  Copyright (c) 2015 FPT Information System. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MAConstant.h"
#import "MAPaymentInfo.h"
#import "MAVoidTransactionInfo.h"
#import "MATransaction.h"
#import "MAError.h"

@interface MAMPosAPI : NSObject

// Authentication properties.
@property (strong, nonatomic) NSString *credentialKey;
@property (strong, nonatomic) NSString *credentialPassword;

// X-Callback parameters.
@property (strong, nonatomic) NSString *x_Source;                // X-Source (Default value is CFBundleDisplayName from NSBundle).
@property (strong, nonatomic) NSURL *x_SuccessURL;               // X-Success.
@property (strong, nonatomic) NSURL *x_ErrorURL;                 // X-Error.
@property (strong, nonatomic) NSURL *x_CancelURL;                // X-Cancel.

// Additional parameters
@property (assign, nonatomic) BOOL checkTransactionAlreadyPaid __deprecated_msg("Set data to MAPaymentInfo instead.");  // Check if transaction has been paid or not yet.

// Methods.
/**
 @method sharedAPI
 @description Get static instance of class.
 @returns MAMPosAPI Object.
 */
+ (MAMPosAPI *)sharedAPI;

/**
 @method canOpenPaymentApplication:
 @description Check if there can open payment app or not.
 @returns YES: if can open payment app. NO: if Payment app is not installed.
 */
- (BOOL)canOpenPaymentApplication;

/**
 @method registerCurrentDeviceWithUsername:
 @description Send register device request to payment app (Open payment url). This operator will send the logged in username to payment app to register current device on the mPOS system.
 @param username: the logged in username on custom app.
 @returns Sending status: { MAStatusSuccess
                            MAStatusNotSetAuthenticationParams
                            MAStatusNotSetXCallbackParams
                            MAStatusCanNotOpenPaymentApp
                            MAStatusAPIOperationError }
 */
- (MAStatus)registerCurrentDeviceWithUsername:(NSString *)username;

/**
 @method payWithPaymentInfo:
 @description Send pay request to payment app (Open payment url). If payment information is invalid (Reference ID is nil, amount is less than 0 or larger than 15 digit number (>= 1000000000000000), ...) or x-callback params, authentication params do not be set, request will not be sent.
 @param paymentInfo: Payment Information (Reference ID, Amount).
 @returns Sending status: { MAStatusSuccess
                            MAStatusNotSetAuthenticationParams
                            MAStatusNotSetXCallbackParams
                            MAStatusInvalidReferenceId
                            MAStatusInvalidAmount
                            MAStatusInvalidCustomerName
                            MAStatusInvalidCustomerMobile
                            MAStatusInvalidCustomerEmail
                            MAStatusCanNotOpenPaymentApp
                            MAStatusAPIOperationError }
 */
- (MAStatus)payWithPaymentInfo:(MAPaymentInfo *)paymentInfo;

/**
 @method voidTransactionWithTransactionInfo:
 @description Send void transaction request to payment app (Open payment url). If void transaction information is invalid (Reference ID is nil, Length of Transaction key is larger than 20, ...) or x-callback params, authentication params do not be set, request will not be sent.
 @param transactionInfo: Void transaction Information (Reference ID, TransactionKey).
 @returns Sending status: { MAStatusSuccess
                            MAStatusNotSetAuthenticationParams
                            MAStatusNotSetXCallbackParams
                            MAStatusInvalidReferenceId
                            MAStatusInvalidTransactionKey
                            MAStatusCanNotOpenPaymentApp
                            MAStatusAPIOperationError }
 */
- (MAStatus)voidTransactionWithTransactionInfo:(MAVoidTransactionInfo *)transactionInfo;

/**
 @method urlTypeFromURL:
 @description Determine url type base on x-success, x-error, x-cancel.
 @param url: URL to detemine url type.
 @returns URL type.
 */
- (MAURLType)urlTypeFromURL:(NSURL *)url;

#pragma mark - New methods
/**
 @method handleSuccessURL: saveRequestOperatorTo: saveTransactionTo:
 @description Get transaction data which response from payment app and set to a transaction object.
 @param successURL: Success url as passed to [UIApplicationDelegate application:openURL:sourceApplication:annotation:].
 @param (out) requestOperator: Object which request operator data will be save in.
 @param (out) transaction: Object which transaction data will be save in.
 @returns Handle results: { MAStatusSuccess
                            MAStatusBadURL
                            MAStatusCanNotReadResponseData
                            MAStatusCanNotDecryptData
                            MAStatusCanNotParseJsonData
                            MAStatusCanNotReadJsonData
                            MAStatusAPIOperationError }
 */
- (MAStatus)handleSuccessURL:(NSURL *)successURL
       saveRequestOperatorTo:(MARequestOperator *)requestOperator
           saveTransactionTo:(MATransaction **)transaction;

/**
 @method handleSuccessURL: completeBlock:
 @description Get transaction which response from payment app and set it to a transaction object.
 @param successURL: Success url as passed to [UIApplicationDelegate application:openURL:sourceApplication:annotation:].
 @param completeBlock: Block to callback and pass transaction after handle success url completely.
 */
- (void)handleSuccessURL:(NSURL *)successURL
           completeBlock:(void (^)(MAStatus status, MARequestOperator requestOperator, MATransaction *transaction))completeBlock;

/**
 @method handleErrorURL: saveRequestOperatorTo: saveErrorTo: saveTransactionTo:
 @description Get error description and transaction data which response from payment app and set it to a error object, a transaction object.
 @param errorURL: Error url as passed to [UIApplicationDelegate application:openURL:sourceApplication:annotation:].
 @param (out) requestOperator: Object which request operator data will be save in.
 @param (out) error: Object which error data will be save in.
 @param (out) transaction: Object which transaction data will be save in.
 @returns Handle results: { MAStatusSuccess
                            MAStatusBadURL
                            MAStatusCanNotReadResponseData
                            MAStatusCanNotDecryptData
                            MAStatusCanNotParseJsonData
                            MAStatusCanNotReadJsonData
                            MAStatusAPIOperationError }
 */
- (MAStatus)handleErrorURL:(NSURL *)errorURL
     saveRequestOperatorTo:(MARequestOperator *)requestOperator
               saveErrorTo:(MAError **)error
         saveTransactionTo:(MATransaction **)transaction;

/**
 @method handleErrorURL: completeBlock:
 @description Get error description and transaction data which response from payment app and set it to a error object, a transaction object.
 @param errorURL: Error url as passed to [UIApplicationDelegate application:openURL:sourceApplication:annotation:].
 @param completeBlock: Block to callback and pass error description after handle error url completely.
 */
- (void)handleErrorURL:(NSURL *)errorURL
         completeBlock:(void (^)(MAStatus status, MARequestOperator requestOperator, MAError *error, MATransaction *transaction))completeBlock;

/**
 @method handleCancelURL:
 @description Get reference id of canceled transaction, responsed from payment app and set it to a transaction object.
 @param cancelURL: Cancel url as passed to [UIApplicationDelegate application:openURL:sourceApplication:annotation:].
 @param (out) requestOperator: Object which request operator data will be save in.
 @param (out) transaction: Object which transaction data will be save in.
 @returns Handle results: { MAStatusSuccess
                            MAStatusBadURL
                            MAStatusCanNotReadResponseData
                            MAStatusCanNotDecryptData
                            MAStatusCanNotParseJsonData
                            MAStatusCanNotReadJsonData
                            MAStatusAPIOperationError }
 */
- (MAStatus)handleCancelURL:(NSURL *)cancelURL
      saveRequestOperatorTo:(MARequestOperator *)requestOperator
          saveTransactionTo:(MATransaction **)transaction;

/**
 @method handleCancelURL: completeBlock:
 @description Get reference id of canceled transaction, responsed from payment app and set it to a transaction object.
 @param cancelURL: Cancel url as passed to [UIApplicationDelegate application:openURL:sourceApplication:annotation:].
 @param completeBlock: Block to callback and pass cancel transaction after handle cancel url completely.
 */
- (void)handleCancelURL:(NSURL *)cancelURL
          completeBlock:(void (^)(MAStatus status, MARequestOperator requestOperator, MATransaction *transaction))completeBlock;

#pragma mark - Deprecated methods
/**
 @method handleSuccessURL:
 @description Get transaction data which response from payment app and set to a transaction object.
 @param successURL: Success url as passed to [UIApplicationDelegate application:openURL:sourceApplication:annotation:].
 @param (out) transaction: Object which transaction data will be save in.
 @returns Handle results: { MAStatusSuccess
                            MAStatusBadURL
                            MAStatusCanNotReadResponseData
                            MAStatusCanNotDecryptData
                            MAStatusCanNotParseJsonData
                            MAStatusCanNotReadJsonData
                            MAStatusAPIOperationError }
 */
- (MAStatus)handleSuccessURL:(NSURL *)successURL
           saveTransactionTo:(MATransaction **)transaction __deprecated_msg("Use new method instead.");


/**
 @method handleErrorURL: saveErrorTo: saveTransactionTo:
 @description Get error description and transaction data which response from payment app and set it to a error object, a transaction object.
 @param errorURL: Error url as passed to [UIApplicationDelegate application:openURL:sourceApplication:annotation:].
 @param (out) error: Object which error data will be save in.
 @param (out) transaction: Object which transaction data will be save in.
 @returns Handle results: { MAStatusSuccess
                            MAStatusBadURL
                            MAStatusCanNotReadResponseData
                            MAStatusCanNotDecryptData
                            MAStatusCanNotParseJsonData
                            MAStatusCanNotReadJsonData
                            MAStatusAPIOperationError }
 */
- (MAStatus)handleErrorURL:(NSURL *)errorURL
               saveErrorTo:(MAError **)error
         saveTransactionTo:(MATransaction **)transaction __deprecated_msg("Use new method instead.");

/**
 @method handleCancelURL:
 @description Get reference id of canceled transaction, responsed from payment app and set it to a transaction object.
 @param cancelURL: Cancel url as passed to [UIApplicationDelegate application:openURL:sourceApplication:annotation:].
 @param (out) transaction: Object which transaction data will be save in.
 @returns Handle results: { MAStatusSuccess
                            MAStatusBadURL
                            MAStatusCanNotReadResponseData
                            MAStatusCanNotDecryptData
                            MAStatusCanNotParseJsonData
                            MAStatusCanNotReadJsonData
                            MAStatusAPIOperationError }
 */
- (MAStatus)handleCancelURL:(NSURL *)cancelURL
          saveTransactionTo:(MATransaction **)transaction __deprecated_msg("Use new method instead.");
@end
