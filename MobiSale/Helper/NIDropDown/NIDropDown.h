//
//  NIDropDown.h
//  NIDropDown
//
//  Created by Bijesh N on 12/28/12.
//  Copyright (c) 2012 Nitor Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NIDropDown;
@protocol NIDropDownDelegate
- (void)niDropDownDelegateMethod: (NIDropDown *) sender;
- (void)dropDownListView:(NIDropDown *)dropDownListView
      didSelectIndexPath:(NSIndexPath *)indexPath atButton:(UIButton *)button;
@end

@interface NIDropDown : UIView <UITableViewDelegate, UITableViewDataSource> {
    NSString *animationDirection;
    UIImageView *imgView;
    
    UIControl   *_overlayView;
}
@property (nonatomic, retain) id <NIDropDownDelegate> delegate;
@property (nonatomic, retain) NSString *animationDirection;
-(void)hideDropDown:(UIButton *)b;
- (id)showDropDownAtButton:(UIButton *)b height:(CGFloat *)height width:(CGFloat *)width data:(NSArray *)arr images:(NSArray *)imgArr animation:(NSString *)direction;
@end
