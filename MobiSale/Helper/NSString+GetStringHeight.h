//
//  NSString+GetStringHeight.h
//  MobiSale
//
//  Created by ISC-DanTT on 4/8/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (GetStringHeight)

- (CGFloat)getStringHeightWithFontSize:(NSInteger)fontSize withSizeMake:(CGSize)constraint;

@end
