//
//  ACNavBarDrawer.h
//  ACNavBarDrawer
//
//  Created by albert on 13-7-29.
//  Copyright (c) 2013年 albert. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ACNavBarDrawerDelegate <NSObject>

@required

- (void)didTapButtonAtIndex:(NSInteger)itemIndex;

@optional

- (void)drawerWillClose;
- (void)drawerDidClose;
- (void)didTapOnMask;

@end


@interface ACNavBarDrawer : UIView


@property (nonatomic, assign) id <ACNavBarDrawerDelegate> delegate;

@property (nonatomic) BOOL isOpen;

- (id)initWithView:(UIView *)view andItemInfoArray:(NSArray *)array;

- (void)openNavBarDrawer;

- (void)closeNavBarDrawer;

@end
