//
//  UIPopoverListView.h
//  UIPopoverListViewDemo
//
//  Created by su xinde on 13-3-13.
//  Copyright (c) 2013年 su xinde. All rights reserved.
//

@class UIPopoverListView2;

@protocol UIPopoverListView2DataSource <NSObject>
@required

- (UITableViewCell *)popoverListView:(UIPopoverListView2 *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath;

- (NSInteger)popoverListView:(UIPopoverListView2 *)popoverListView
       numberOfRowsInSection:(NSInteger)section;

@end

@protocol UIPopoverListView2Delegate <NSObject>
@optional

- (void)popoverListView:(UIPopoverListView2 *)popoverListView
     didSelectIndexPath:(NSIndexPath *)indexPath;

- (void)popoverListViewCancel:(UIPopoverListView2 *)popoverListView;

- (CGFloat)popoverListView:(UIPopoverListView2 *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath;

@end


@interface UIPopoverListView2 : UIView <UITableViewDataSource, UITableViewDelegate>
{
    UITableView *_listView;
    UILabel     *_titleView;
    UIControl   *_overlayView;
    
    id<UIPopoverListView2DataSource> datasource;
    id<UIPopoverListView2Delegate>   delegate;
    
}

@property (nonatomic, assign) id<UIPopoverListView2DataSource> datasource;
@property (nonatomic, assign) id<UIPopoverListView2Delegate>   delegate;

@property (nonatomic, retain) UITableView *listView;

- (void)setTitle:(NSString *)title;

- (void)show;
- (void)dismiss;

@end
