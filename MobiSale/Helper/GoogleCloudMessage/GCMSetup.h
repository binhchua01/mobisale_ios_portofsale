//
//  GCMSetup.h
//  MobiSale
//
//  Created by ISC-DanTT on 11/12/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Google/CloudMessaging.h>

@interface GCMSetup : UIResponder <UIApplicationDelegate, GGLInstanceIDDelegate, GCMReceiverDelegate>

@property (strong, nonatomic) UIWindow *window;

@property(nonatomic, readonly, strong) NSString *registrationKey;
@property(nonatomic, readonly, strong) NSString *messageKey;
@property(nonatomic, strong) NSString *gcmSenderID;
@property(nonatomic, readonly, strong) NSDictionary *registrationOptions;

@property(nonatomic, strong) void (^registrationHandler)
(NSString *registrationToken, NSError *error);
@property(nonatomic, assign) BOOL connectedToGCM;
@property(nonatomic, strong) NSString* registrationToken;
@property(nonatomic, assign) BOOL subscribedToTopic;

// register for remote notifications
- (void) registerRemoteNotification:(UIApplication *)application
      didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
// connect GCM service
- (void) connectGCMService:(UIApplication *)application;
// disconnect GCM service
- (void) disconnectGCMService:(UIApplication *)application;
// get GCM registration token
- (void) getGCMRegistrationToken:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;
// receive APNs token error
- (void)receiveAPNsTokenError:(UIApplication *)application
didFailToRegisterForRemoteNotificationsWithError:(NSError *)error;
// ACK message reception
- (void)ackMessageReception:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo;
// reveive message
- (void)receiveMessage:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))handler;

@end
