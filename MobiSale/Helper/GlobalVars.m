//
//  GlobalVar.m
//  MobiPay
//  define app global variables
//  Created by VANDN on 2/24/14.
//  Copyright (c) 2014 VANDN. All rights reserved.
//

#import "GlobalVars.h"

@implementation GlobalVars

static GlobalVars *instance = nil;
+(GlobalVars *)getInstance{
    @synchronized(self){
        if(instance==nil){
            
            instance= [GlobalVars new];
        }
    }
    return instance;
}

int CURRENT_MENU = 0;
// add by DanTT 2015.10.08
NSString *SESSIONID = @"";
NSString *TOKEN = @"";
NSString *DEVICEIMEI = @"";
NSString *USERREGISTRATIONID = @"";
NSString *ADMINNAME = @"";
NSString *ADMINREGISTRATIONID = @"";

@end
