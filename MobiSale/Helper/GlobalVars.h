//
//  Constants.h
//  MobiPay
//  define app global variables
//  Created by THAOLTT8 on 23/09/14.
//  Copyright (c) 2014 THAOLTT8. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface GlobalVars : NSObject

+(GlobalVars*)getInstance;

extern int CURRENT_MENU;
// add by DanTT 2015.10.08
extern NSString *SESSIONID;
extern NSString *TOKEN;
extern NSString *DEVICEIMEI;
extern NSString *USERREGISTRATIONID;
extern NSString *ADMINNAME;
extern NSString *ADMINREGISTRATIONID;

@end
