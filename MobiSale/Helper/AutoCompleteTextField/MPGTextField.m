//
//  MPGTextField.m
//
//  Created by Gaurav Wadhwani on 05/06/14.
//  Copyright (c) 2014 Mappgic. All rights reserved.
//

#import "MPGTextField.h"

#define IS_IPHONE4 (([[UIScreen mainScreen] bounds].size.height-480)?NO:YES)

@implementation MPGTextField
@dynamic delegate;

//Private declaration of UITableViewController that will present the results in a popover depending on the search query typed by the user.
UITableViewController *tableViewController;

//Private declaration of NSArray that will hold the data supplied by the user for showing results in search popover.
NSArray *data;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if (self.isFirstResponder) {
        //User entered some text in the textfield. Check if the delegate has implemented the required method of the protocol. Create a popover and show it around the MPGTextField.
        
        if ([self.delegate respondsToSelector:@selector(dataForPopoverInTextField:)]) {
            data = [[self delegate] dataForPopoverInTextField:self];
            [self provideSuggestions];
        }
        else{
            NSLog(@"<MPGTextField> WARNING: You have not implemented the requred methods of the MPGTextField protocol.");
        }
    }
    else{
        //No text entered in the textfield. If -textFieldShouldSelect is YES, select the first row from results using -handleExit method.tableView and set the displayText on the text field. Check if suggestions view is visible and dismiss it.
        if ([tableViewController.tableView superview] != nil) {
            [tableViewController.tableView removeFromSuperview];
        }
    }
}

//Override UITextField -resignFirstResponder method to ensure the 'exit' is handled properly.
- (BOOL)resignFirstResponder {
    [UIView animateWithDuration:0.3
                     animations:^{
                         [tableViewController.tableView setAlpha:0.0];
                     }
                     completion:^(BOOL finished){
                         [tableViewController.tableView removeFromSuperview];
                         tableViewController = nil;
                     }];
    [self handleExit];
    return [super resignFirstResponder];
}

//This method checks if a selection needs to be made from the suggestions box using the delegate method -textFieldShouldSelect. If a user doesn't tap any search suggestion, the textfield automatically selects the top result. If there is no result available and the delegate method is set to return YES, the textfield will wrap the entered the text in a NSDictionary and send it back to the delegate with 'CustomObject' key set to 'NEW'
- (void)handleExit {
    
    [tableViewController.tableView removeFromSuperview];
    
    if ([[self delegate] respondsToSelector:@selector(textFieldShouldSelect:)]) {
        
        if ([[self delegate] textFieldShouldSelect:self]) {
            
            NSArray *arr = [self applyFilterWithSearchQuery:self.text];
            
            if (arr.count > 0) {
                
                if (self.text.length <= 0) {
                    if ([[self delegate] respondsToSelector:@selector(textField:didEndEditingWithSelection:)]) {
                        [[self delegate] textField:self didEndEditingWithSelection:[NSDictionary dictionaryWithObjectsAndKeys:self.text,@"DisplayText",@"NEW",@"PromotionModel", nil]];
                    } else {
                        NSLog(@"<MPGTextField> WARNING: You have not implemented a method from MPGTextFieldDelegate that is called back when the user selects a search suggestion.");
                    }
                    return;
                }
                
                self.text = [[arr objectAtIndex:0] objectForKey:@"DisplayText"];
                
                if ([[self delegate] respondsToSelector:@selector(textField:didEndEditingWithSelection:)]) {
                    [[self delegate] textField:self didEndEditingWithSelection:[arr objectAtIndex:0]];
                    
                } else {
                    
                    NSLog(@"<MPGTextField> WARNING: You have not implemented a method from MPGTextFieldDelegate that is called back when the user selects a search suggestion.");
                }
                return;
            }
            
            if (self.text.length > 0){
                //Make sure that delegate method is not called if no text is present in the text field.
                if ([[self delegate] respondsToSelector:@selector(textField:didEndEditingWithSelection:)]) {
                    [[self delegate] textField:self didEndEditingWithSelection:[NSDictionary dictionaryWithObjectsAndKeys:self.text,@"DisplayText",@"NEW",@"PromotionModel", nil]];
                } else {
                    NSLog(@"<MPGTextField> WARNING: You have not implemented a method from MPGTextFieldDelegate that is called back when the user selects a search suggestion.");
                }
                return;
            }
        }
    }
}


#pragma mark UITableView DataSource & Delegate Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSUInteger count = [[self applyFilterWithSearchQuery:self.text] count];
    if (count == 0) {
        [UIView animateWithDuration:0.3
                         animations:^{
                             [tableViewController.tableView setAlpha:0.0];
                         }
                         completion:^(BOOL finished){
                             [tableViewController.tableView removeFromSuperview];
                             tableViewController = nil;
                         }];
    }
    return count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSDictionary *dataForRowAtIndexPath = [[self applyFilterWithSearchQuery:self.text] objectAtIndex:indexPath.row];
    
    NSString *text = [dataForRowAtIndexPath objectForKey:@"DisplayText"];
    
    CGSize constraint = CGSizeMake(210, 20000.0f);
    
    // constratins the size of the table row according to the text
    CGRect textRect = [text boundingRectWithSize:constraint options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Light" size:14]} context:nil];
    
    CGFloat height = MAX(textRect.size.height,49);
    
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary *dataForRowAtIndexPath = [[self applyFilterWithSearchQuery:self.text] objectAtIndex:indexPath.row];
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell.textLabel setNumberOfLines:0];
    [cell.textLabel sizeToFit];
    [cell.textLabel setText:[dataForRowAtIndexPath objectForKey:@"DisplayText"]];
    [cell.textLabel setTextColor:[UIColor whiteColor]];
    [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica-Light" size:14]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.text = [[[self applyFilterWithSearchQuery:self.text] objectAtIndex:indexPath.row] objectForKey:@"DisplayText"];
    [self resignFirstResponder];
}

#pragma mark Filter Method

- (NSArray *)applyFilterWithSearchQuery:(NSString *)filter {
    if (filter.length <= 0) {
        return data;
    }
    NSArray *arrFilter = [filter componentsSeparatedByString:@"*"];
    NSArray *filteredGoods;
    NSPredicate *predicate;
    if (arrFilter.count > 1) {
        for (NSString *strFilter in arrFilter) {
            if (![strFilter isEqualToString: @""]) {
                predicate = [NSPredicate predicateWithFormat:@"DisplayText CONTAINS[cd] %@", strFilter];
                filteredGoods = [NSArray arrayWithArray:[data filteredArrayUsingPredicate:predicate]];
                data = filteredGoods;
            } else {
                break;
            }
        }
        return filteredGoods;
    }
    predicate = [NSPredicate predicateWithFormat:@"DisplayText CONTAINS[cd] %@", filter];
    filteredGoods = [NSArray arrayWithArray:[data filteredArrayUsingPredicate:predicate]];
    return filteredGoods;
}

#pragma mark Popover Method(s)

- (void)provideSuggestions {
    //Providing suggestions
    if (tableViewController.tableView.superview == nil ) {
        //Add a tap gesture recogniser to dismiss the suggestions view when the user taps outside the suggestions view
        UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapped:)];
        [tapRecognizer setNumberOfTapsRequired:1];
        [tapRecognizer setCancelsTouchesInView:NO];
        [tapRecognizer setDelegate:self];
        [self.superview addGestureRecognizer:tapRecognizer];
        
        tableViewController = [[UITableViewController alloc] init];
        [tableViewController.tableView setDelegate:  self];
        [tableViewController.tableView setDataSource:self];
        
        if (self.backgroundColor == nil) {
            //Background color has not been set by the user. Use default color instead.
            tableViewController.tableView.backgroundColor = [UIColor colorWithRed:0.239 green:0.239 blue:0.239 alpha:0.9];
            
            self.backgroundColor = tableViewController.tableView.backgroundColor;
            
        } else {
            [tableViewController.tableView setBackgroundColor:self.backgroundColor];
        }
        
        [tableViewController.tableView setSeparatorColor:self.seperatorColor];
        
        if (self.popoverSize.size.height == 0.0) {
            //PopoverSize frame has not been set. Use default parameters instead.
            CGRect frameForPresentation      = [self frame];
            frameForPresentation.origin.y   -= 187;
            frameForPresentation.size.height = 177;
            frameForPresentation.size.width -=  12;

            if (IS_IPHONE4) {
                frameForPresentation.size.height = 89;
                frameForPresentation.origin.y   -= 77;
            }
            
            [tableViewController.tableView setFrame:frameForPresentation];
            self.popoverSize = frameForPresentation;
            
        } else {
            [tableViewController.tableView setFrame:self.popoverSize];
        }
        
        [[self superview] addSubview:tableViewController.tableView];
        
        tableViewController.tableView.alpha = 0.6;
        [UIView animateWithDuration:0.3 animations:^{
            [tableViewController.tableView setAlpha:1.0];
        } completion:^(BOOL finished){
            
        }];
        [tableViewController.tableView reloadData];
    } else {
        [tableViewController.tableView reloadData];
    }
}

- (void)tapped:(UIGestureRecognizer *)gesture {
    
}


@end
