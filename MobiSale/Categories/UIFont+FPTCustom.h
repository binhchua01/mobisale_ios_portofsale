//
//  UIFont+FPTCustom.h
//  MobiPayCam
//
//  Created by Hieu Do on 8/18/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>

#define mainFontName @"Helvetica-Light"

@interface UIFont (FPTCustom)

+ (UIFont *)fontTextFieldLogin;
+ (UIFont *)fontTilteDocument;

@end
