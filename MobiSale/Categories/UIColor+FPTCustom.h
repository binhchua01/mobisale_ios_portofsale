//
//  UIColor+FPTCustom.h
//  MobiPayCam
//
//  Created by Hieu Do on 8/18/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (FPTCustom)

+ (UIColor *)colorMain;
+ (UIColor *)colorNavigationBar;
+ (UIColor *)colorBorderTextLogin;
+ (UIColor *)colorUIPopoverListView;

@end
