//
//  UINavigationBar+FPTCustom.m
//  MobiPay
//
//  Created by ISC-HieuDT20 on 1/26/16.
//  Copyright © 2016 Hieu Do. All rights reserved.
//

#import "UINavigationBar+FPTCustom.h"

@implementation UINavigationBar (FPTCustom)

- (void)drawRect:(CGRect)rect {
    UIImage *image = [UIImage imageNamed:@"background_action_bar.png"];
    [image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
}

@end
