//
//  UIViewController+FPTCustom.h
//  MobiPayCam
//
//  Created by FPT on 8/25/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSInteger {
    ErrorTag = 0,
    SuccessTag,
    OtherTag
} AlertTagType;

@interface UIViewController (FPTCustom) <UIActionSheetDelegate>

- (void)showHUDWithMessage:(NSString *)message;
- (void)hideHUD;
- (void)removeAllNavigationBarButtons;
// Show MBProcess View
- (void)showMBProcess;
- (void)hideMBProcess;
// Show AlertView
- (void)showAlertBox:(NSString *)title message:(NSString *)message tag:(NSInteger)tag;
- (void)showAlertBoxWithDelayDismiss:(NSString*)tile message:(NSString*)message tag:(NSInteger)tag;
// Show ActionSheet View
- (void)showActionSheetWithTitle:(NSString *)title andTag:(NSInteger)tag;
- (BOOL)checkingEmail:(NSString *)candidate;

@end
