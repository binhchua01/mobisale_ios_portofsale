//
//  UIImage+FPTCustom.m
//  MobiPayCam
//
//  Created by FPT on 9/8/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "UIImage+FPTCustom.h"

@implementation UIImage (FPTCustom)

+ (UIImage *)scaleImage:(UIImage *)image toSize:(CGSize)newSize
{
    UIGraphicsBeginImageContextWithOptions(newSize, NO, [UIScreen mainScreen].scale);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
