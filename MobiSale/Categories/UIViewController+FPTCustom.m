//
//  UIViewController+FPTCustom.m
//  MobiPayCam
//
//  Created by FPT on 8/25/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "UIViewController+FPTCustom.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "UIFont+FPTCustom.h"

@implementation UIViewController (FPTCustom)

MBProgressHUD *HUD;

- (void)showHUDWithMessage:(NSString *)message
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = message;
    hud.labelFont = [UIFont fontWithName:mainFontName size:15];
}

- (void)hideHUD
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)removeAllNavigationBarButtons
{
    self.title = @"";
    for (UIView *view in self.navigationController.navigationBar.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            [view removeFromSuperview];
        }
    }
}

#pragma mark process bar
- (void) showMBProcess {
    if (self.navigationController.view == nil) {
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    } else {
        HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    }
    //[self.view addSubview:HUD];
    HUD.delegate = (id<MBProgressHUDDelegate>)self;
    HUD.labelText = @"Loading";
    [HUD show:YES];
}

- (void) hideMBProcess {
    [HUD hide:YES];
}

#pragma mark - Show Alert view method
- (void)showAlertBox:(NSString *)title message:(NSString *)message tag:(NSInteger)tag {
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
    alertView.tag = tag;
    [alertView show];
}

-(void)showAlertBoxWithDelayDismiss:(NSString*)tile message:(NSString*)message tag:(NSInteger)tag {
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:tile message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    alertView.tag = tag;
    [alertView show];
    [self performSelector:@selector(dismiss:) withObject:alertView afterDelay:2.0];
}

-(void)dismiss:(UIAlertView*)alert{
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}

#pragma mark - Show ActionSheet View method
- (void)showActionSheetWithTitle:(NSString *)title andTag:(NSInteger)tag {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:title delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
    actionSheet.tag = tag;
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
    
}

#pragma mark - validate email

- (BOOL)checkingEmail:(NSString *)candidate {
    NSString *string= candidate;
    NSError *error = NULL;
    NSRegularExpression *regex = nil;
    regex = [NSRegularExpression regularExpressionWithPattern:@"\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,6}"
                                                      options:NSRegularExpressionCaseInsensitive
                                                        error:&error];
    NSUInteger numberOfMatches = 0;
    numberOfMatches = [regex numberOfMatchesInString:string
                                             options:0
                                               range:NSMakeRange(0, [string length])];
    // NSLog(@"numberOfMatches is: %lu", numberOfMatches);
    if(numberOfMatches != 0){
        return TRUE;
    }
    return FALSE;
}

@end
