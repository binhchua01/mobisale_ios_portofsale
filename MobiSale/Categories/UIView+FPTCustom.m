//
//  UIView+FPTCustom.m
//  MobiPayCam
//
//  Created by FPT on 9/1/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "UIView+FPTCustom.h"
#import "UIColor+FPTCustom.h"

@implementation UIView (FPTCustom)

- (void)styleBorderViewWithCGFloat:(CGFloat )CGFloat
{
    self.layer.borderColor = [UIColor colorMain].CGColor;
    self.layer.borderWidth = CGFloat;
}

- (void)styleBorderLogin
{
    [self.layer setCornerRadius:3.0f];
    [self.layer setBorderColor:[UIColor colorBorderTextLogin].CGColor];
    [self.layer setBorderWidth:1.0f];
}

- (void)styleBorderMap
{
    [self.layer setCornerRadius:6.0f];
    [self.layer setBorderColor:[UIColor colorMain].CGColor];
    [self.layer setBorderWidth:1.0f];
}

- (void)setShadow {
    [self.layer setShadowRadius:2.0f];
    [self.layer setShadowOpacity:0.8];
    [self.layer setShadowColor:[UIColor grayColor].CGColor];
    [self.layer setShadowOffset:CGSizeMake(-5, -5)];
    
}

@end
