//
//  UIButton+FPTCustom.m
//  MobiPayCam
//
//  Created by FPT on 9/10/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "UIButton+FPTCustom.h"
#import "UIColor+FPTCustom.h"

@implementation UIButton (FPTCustom)

- (void)styleButtonUpdate {
    [self.layer setCornerRadius:6.0f];
    [self.layer setBorderColor:[UIColor colorMain].CGColor];
    [self.layer setBorderWidth:0.6f];
    
}

- (void)setShadow {
    [self.layer setShadowRadius:6.0f];
    [self.layer setShadowOpacity:0.8];
    [self.layer setShadowColor:[UIColor grayColor].CGColor];
    [self.layer setShadowOffset:CGSizeMake(6, 6)];

}

- (void)setStyle {
    [self.layer setCornerRadius:self.bounds.size.width/2];
    [self setClipsToBounds:YES];
    [self.layer setBorderColor:[UIColor colorMain].CGColor];
    [self.layer setBorderWidth:0.6f];

}


@end
