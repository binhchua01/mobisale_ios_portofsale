//
//  UIButton+FPTCustom.h
//  MobiPayCam
//
//  Created by FPT on 9/10/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIButton (FPTCustom)

- (void)styleButtonUpdate;

- (void)setShadow;
- (void)setStyle;
@end
