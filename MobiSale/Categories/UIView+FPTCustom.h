//
//  UIView+FPTCustom.h
//  MobiPayCam
//
//  Created by FPT on 9/1/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (FPTCustom)

- (void)styleBorderViewWithCGFloat:(CGFloat )CGFloat;
- (void)styleBorderLogin;
- (void)styleBorderMap;
- (void)setShadow;

@end
