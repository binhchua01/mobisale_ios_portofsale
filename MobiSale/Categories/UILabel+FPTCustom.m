//
//  UILabel+FPTCustom.m
//  MobiPay
//
//  Created by ISC-HieuDT20 on 4/6/16.
//  Copyright © 2016 Hieu Do. All rights reserved.
//

#import "UILabel+FPTCustom.h"

@implementation UILabel (FPTCustom)

- (NSInteger)lineCount
{
    // Calculate height text according to font
    NSInteger lineCount = 0;
    CGSize labelSize = (CGSize){self.frame.size.width, FLT_MAX};
    CGRect requiredSize = [self.text boundingRectWithSize:labelSize  options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.font} context:nil];
    
    // Calculate number of lines
    int charSize = self.font.leading;
    int rHeight = requiredSize.size.height;
    lineCount = rHeight/charSize;
    
    return lineCount;
}

- (void)setHTML:(NSString *)html {
    NSError *error = nil;
    self.attributedText = [[NSAttributedString alloc]
                           initWithData:[html dataUsingEncoding:NSUnicodeStringEncoding]
                           options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType}
                           documentAttributes:nil
                           error:&error];
    if (error) {
        NSLog(@"Unale to parse label text: %@", error.description);
    }
}

- (void)autoFixFrame {
    [self setNumberOfLines:0];
    [self sizeToFit];
}

@end
