//
//  UIColor+FPTCustom.m
//  MobiPayCam
//
//  Created by Hieu Do on 8/18/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "UIColor+FPTCustom.h"

@implementation UIColor (FPTCustom)

+ (UIColor *)colorMain
{
//    return [UIColor colorWithRed:0 green:153.0f/255.0f blue:153.0f/255.0f alpha:1.0f];
    return [UIColor colorWithRed:0.133 green:0.698 blue:0.569 alpha:1];
}

+ (UIColor *)colorBorderTextLogin
{
    return [UIColor colorWithRed:255.0f / 255.0f
                           green:81.0f / 255.0f
                            blue:65.0f / 255.0f
                           alpha:1.0f];
}

+ (UIColor *)colorNavigationBar
{
    return [UIColor colorWithRed:50.0f / 255.0f
                           green:53.0f / 255.0f
                            blue:67.0f / 255.0f
                           alpha:1.0f];
}

+ (UIColor *)colorUIPopoverListView{
    
    return [UIColor colorWithRed:255.0f/255.0f
                           green:77.0f/255.0f
                            blue:80.0f/255.0f
                           alpha:1.0f];
    
    
}





@end
