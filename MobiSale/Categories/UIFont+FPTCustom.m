//
//  UIFont+FPTCustom.m
//  MobiPayCam
//
//  Created by Hieu Do on 8/18/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "UIFont+FPTCustom.h"

@implementation UIFont (FPTCustom)

+ (UIFont *)fontTextFieldLogin
{
    return [UIFont fontWithName:mainFontName size:15];
}

+ (UIFont *)fontTilteDocument
{
    return [UIFont fontWithName:mainFontName size:14];
}

@end
