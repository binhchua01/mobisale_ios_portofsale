//
//  FPTUtility.m
//  MobiSale
//
//  Created by TranTuanVu on 12/6/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//
#define SECRETKEY @"tamtm11"
#define CC_MD5_DIGEST_LENGTH    16
#import "FPTUtility.h"
#import <CommonCrypto/CommonDigest.h>

@implementation FPTUtility
+ (NSString *) base64:(NSString *) input {
    
    NSData *data = [input dataUsingEncoding:NSUTF8StringEncoding];
    
    NSString *base64String = [data base64EncodedStringWithOptions:0];
    base64String = [base64String stringByAppendingString:[NSString stringWithFormat:@"%@",SECRETKEY]];
    
    return base64String;
}

+ (NSString *) md5Encrypt:(NSString *) input {
    
    const char *cStr = [input UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr,strlen(cStr),digest);
    NSMutableString *md5 = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH*2];
    for (int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        
        [md5 appendFormat:@"%02x",digest[i]];
    }
    return md5;
}

@end
