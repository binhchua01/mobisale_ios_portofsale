//
//  Common.m
//  MobiPay
//
//  Created by VANDN on 2/14/14.
//  Copyright (c) 2014 VANDN. All rights reserved.
//

#import "Common_client.h"
#import "KeyValueModel.h"
#import "Reachability.h"
#import <CommonCrypto/CommonDigest.h>
#import <GoogleMaps/GoogleMaps.h>

#import "ShareData.h"

#import <sys/utsname.h>

@implementation Common_client{
    Reachability* hostReachable;
}

#pragma mark - UI ALERT VIEW
+ (void)showAlertMessage:(NSString*)message{
    NSString *title= TitleNotification;
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title                                                    message:message delegate:nil cancelButtonTitle:@"OK"                                          otherButtonTitles:nil];
    [alert show];
}


+ (void)showConfirmAlert:(NSString*)message title:(NSString*)title
{
    NSString *lblYes= NSLocalizedString(@"LabelYes", @"");
    NSString *lblNo= NSLocalizedString(@"LabelNo", @"");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:lblNo otherButtonTitles:lblYes, nil];
    [alert show];
}
// reset position of frame container pushed view
+ (CGRect)defaultFrame:(UIView *)view withDeltaY:(float) deltaY{
    CGRect rect = view.frame;
    
    float y = 0;
    
    if(IS_OS_7_OR_LATER){
        y = deltaY;
    }
    
    return CGRectMake(0, y, rect.size.width, rect.size.height);
}

#pragma mark -
#pragma mark FORMAT NUMBER (Ex: 123,125.0000) - add by thaoltt,02.04.2014
#pragma mark -
+ (NSString *) formatNumber:(NSNumber *)number
{
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    //[numberFormatter setPositiveFormat:@"#,##0.0000"];
    [numberFormatter setPositiveFormat:@"#,##0"];
    
    NSString *formattedNumberString = [numberFormatter stringFromNumber:number];
    
    //NSLog(@"formattedNumberString: %@", formattedNumberString);
    // Output for locale en_US: "formattedNumberString: formattedNumberString: 122,344.45"
    
    return formattedNumberString;
}

#pragma mark -
#pragma mark CHECK TEXT FIELD IS EMPTY - add by thaoltt,02.04.2014
#pragma mark -
+ (BOOL)isEmpty:(UITextField *)textField
{
    NSString *rawString = [textField text];
    NSCharacterSet *whitespace = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimmed = [rawString stringByTrimmingCharactersInSet:whitespace];
    if ([trimmed length] == 0)
        // Text was empty or only whitespace.
        return YES;
    else
        return NO;
}

#pragma mark -
#pragma mark CREATE PADDING FOR TEXT - add by thaoltt, 16.04.2014
#pragma mark -
+ (UILabel *)paddingText
{
    UILabel * paddingView = [[UILabel alloc] initWithFrame:CGRectMake(15,0, 7,26)];
    paddingView.backgroundColor = [UIColor clearColor];
    
    return paddingView;
}

#pragma mark -
#pragma mark CHECK INPUT FILTER MIN MAX DATE - add by thaoltt,02.04.2014
#pragma mark -
+ (BOOL)inputFilterMinMaxDate:(NSString *)inputVal
{
    if([inputVal intValue] < 1 || [inputVal intValue] > 31)
        return NO;
    else
        return YES;
}

#pragma mark -
#pragma mark DROPDOWN DATA (CUSTOMER INFO) - add by thaoltt, 10.04.2014
#pragma mark -



#pragma mark - CHANGE IMG SIZE
+ (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size {
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

+ (UIImage *)imageWithImage:(UIImage *)image convertToSize:(CGSize)size point:(CGPoint)xy
{
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    [image drawInRect:CGRectMake(xy.x, xy.y, size.width, size.height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

+ (UIImage *)imageWithImageHeight:(UIImage *)image convertToSizeWithHeight:(float)height
{
    CGSize itemSize = CGSizeMake( image.size.width*height/image.size.height, height);
    UIGraphicsBeginImageContextWithOptions(itemSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, image.size.width*height/image.size.height, height)];
    UIImage *destImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return destImage;
}

#pragma mark - DATETIME HELPER

+ (NSDate *)getCurrentGMTTime
{
    if([[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] integerValue] >= 1.0 ){
        //return [NSDate date];
    }
    //get the current application timezone
    NSTimeZone *currentTimeZone = [NSTimeZone defaultTimeZone];
    
    //get the current time offset from the GMT time
    NSTimeInterval timeOffset = [currentTimeZone secondsFromGMT];
    
    NSDate *currentTime = [NSDate date];// this date is a date of current time zone
    
    //to get the GMT time the timeOffset should be deducted from the current time
    NSDate *gmtTime = [currentTime dateByAddingTimeInterval:timeOffset];
    
    return gmtTime;
}

+ (NSString *)getDateTimeByFormatTimeDay:(NSDate *) startDay {
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"HH:mm dd/MM/YYYY"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    
    NSString *startingDate = [dateFormatter stringFromDate:startDay];
    
    return startingDate;
}

+ (NSString *)getDateTimeByFormatDayTime:(NSDate *) startDay {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"dd/MM/YYYY HH:mm"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    
    NSString *startingDate = [dateFormatter stringFromDate:startDay];
    return startingDate;
}

+ (NSString *)getDateTimeByFormatDay:(NSDate *) startDay {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"dd/MM/YYYY"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    
    NSString *startingDate = [dateFormatter stringFromDate:startDay];
    return startingDate;
}

+ (NSString *)getDateTimeByFormatTime:(NSDate *) startDay {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"HH'h':mm"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    
    NSString *startingDate = [dateFormatter stringFromDate:startDay];
    return startingDate;
}

#pragma mark - CHECK NETWORK AVAILABILITY
+(BOOL)isNetworkAvailable{
    NSString *remoteHostName = @"mobisales.fpt.net";
    Reachability *hostReachability= [Reachability reachabilityWithHostName:remoteHostName];
    NetworkStatus netStatus= [hostReachability currentReachabilityStatus];
    BOOL connectionRequired = [hostReachability connectionRequired];
    switch (netStatus) {
        case NotReachable:{
            connectionRequired = NO;
            break;
        }
        case ReachableViaWWAN:{
            // Connect by 3G
            connectionRequired =YES;
            break;
        }
        case ReachableViaWiFi:{
            //Connect by wifi
            connectionRequired =YES;
            break;
        }
    }

    return connectionRequired;
    
}

#pragma mark - MD5 Encrypt 
+ (NSString *) md5Encrypt:(NSString *) input
{
    NSString *secretKey = @"thomasNGUYEN";
    input = [input stringByAppendingString:[NSString stringWithFormat:@"%@",secretKey]];
    input = [input stringByReplacingOccurrencesOfString:@"/" withString:@""];
    const char *cStr = [input UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5( cStr, (CC_LONG)strlen(cStr), digest ); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return  output;
}
#pragma mark - MAP COMMON
#pragma mark -  Convert string LatLng to GeoPoint

+(CLLocation*)ConvertStrToLocation:(NSString *)strLatLng
{
    CLLocation *point = nil;
    if(![strLatLng isEqualToString:@"null"]){
        strLatLng = [strLatLng stringByReplacingOccurrencesOfString:@"("                                                                           withString:@""];
        strLatLng = [strLatLng stringByReplacingOccurrencesOfString:@")"                                                                           withString:@""];
        NSArray *coordinares = [strLatLng componentsSeparatedByString:@","];
        
        double lat = [[coordinares objectAtIndex:0] doubleValue];
        double lng = [[coordinares objectAtIndex:1] doubleValue];
        point= [[CLLocation alloc] initWithLatitude:lat longitude:lng];
     }
    return point;                     
}

#pragma mark -  Convert GeoPoint to String LatLng

+(NSString*)ConvertLocationToStrlatLng:(CLLocation *)p{
    NSString *strLatlng = [[NSString alloc] initWithFormat:@"(%f,%f)", p.coordinate.latitude,p.coordinate.longitude];
    return strLatlng;
}

+(NSString*)ConvertDegreesToStrlatLng:(CLLocationCoordinate2D)latlng{
    NSString *strLatlng = [NSString stringWithFormat:@"(%f,%f)",latlng.latitude,latlng.longitude];
    return strLatlng;
}


+(void)setHeaderRequestAPI: (NSMutableURLRequest*) request{
// add by DanTT 2015.10.08, add info header request
    ShareData *share = [ShareData instance];
    [request setValue:share.currentUser.sessionID forHTTPHeaderField:@"CookieFTEL"];
    [request setValue:share.currentUser.userName forHTTPHeaderField:@"UserName"];
    [request setValue:share.currentUser.userDeviceIMEI forHTTPHeaderField:@"DeviceIMEI"];
    [request setValue:share.currentUser.token forHTTPHeaderField:@"Token"];
    
}

+(NSString*)parseJsonHeaderResponseAPI:(NSHTTPURLResponse*) response{
// add by DanTT 2015.10.08
    NSDictionary *jsonHeader = [(NSHTTPURLResponse*) response allHeaderFields];
    NSString *headerData = [jsonHeader objectForKey:@"FTEL_MOBISALE_HEADER"] ?:nil;
    NSDictionary* dictHeaderData = [headerData JSONValue];
    
    NSArray *arrObject = [dictHeaderData objectForKey:@"ListObject"];
    
    NSString *headerErrorCode = StringFormat(@"%@", [dictHeaderData objectForKey:@"ErrorCode"]);
    NSString *headerError = StringFormat(@"%@",[dictHeaderData objectForKey:@"Error"]);
    if (![headerErrorCode isEqualToString:@"0"]) {
        // || ![headerErrorCode isEqualToString:@"<null>"] || ![headerErrorCode isEqual:[NSNull null]]
//        if (![headerErrorCode isEqualToString:@"(null)"]) {
            NSLog(@"Header Error: %@",headerError);
            return headerErrorCode;
//        } else {
//            headerErrorCode = @"0";
//            return headerErrorCode;
//        }
    }
    ShareData *share = [ShareData instance];
    share.currentUser.token = StringFormat(@"%@",[[arrObject objectAtIndex:0] objectForKey:@"Token"]);
    return headerErrorCode;
}

//
+ (NSString *)getDeviceModel {
    
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString *machine = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    if ([machine isEqualToString:@"iPad1,1"]) {
        return @"iPad";
    }
    
    if ([machine isEqualToString:@"iPad2,1"]) {
        return @"iPad_2";
    }
    
    if ([machine isEqualToString:@"iPad2,5"] || [machine isEqualToString:@"iPad4,4"] || [machine isEqualToString:@"iPad4,5"] || [machine isEqualToString:@"iPad4,7"]) {
        return @"iPad_Mini";
    }
    
    if ([machine isEqualToString:@"iPad3,1"] || [machine isEqualToString:@"iPad3,4"]) {
        return @"iPad_3";
    }
    
    if ([machine isEqualToString:@"iPad4,1"] || [machine isEqualToString:@"iPad4,2"]) {
        return @"iPad_Air";
    }
    
    if ([machine isEqualToString:@"iPhone3,1"] || [machine isEqualToString:@"iPhone3,3"]) {
        return @"iPhone_4";
    }
    
    if ([machine isEqualToString:@"iPhone4,1"]) {
        return @"iPhone_4S";
    }
    
    if ([machine isEqualToString:@"iPhone5,1"] || [machine isEqualToString:@"iPhone5,2"]) {
        return @"iPhone_5";
    }
    
    if ([machine isEqualToString:@"iPhone5,3"] || [machine isEqualToString:@"iPhone5,34"]) {
        return @"iPhone_5C";
    }
    
    if ([machine isEqualToString:@"iPhone6,1"] || [machine isEqualToString:@"iPhone6,2"]) {
        return @"iPhone_5S";
    }
    
    if ([machine isEqualToString:@"iPhone7,1"]) {
        return @"iPhone_6_Plus";
    }
    
    if ([machine isEqualToString:@"iPhone7,2"]) {
        return @"iPhone_6";
    }
    
    if ([machine isEqualToString:@"iPhone8,1"]) {
        
        return @"iPhone_6S";
    }
    
    if ([machine isEqualToString:@"iPhone8,2"]) {
        return @"iPhone_6S_Plus";
    }
    
    if ([machine isEqualToString:@"iPhone8,4"]) {
        return @"iPhone_SE";
    }
    if ([machine isEqualToString:@"iPhone9,1"]) {
        return @"iPhone_7";
    }
    if ([machine isEqualToString:@"iPhone9,2"]) {
        return @"iPhone_7_Plus";
    }
    if ([machine isEqualToString:@"iPhone9,3"]) {
        return @"iPhone_7";
    }
    if ([machine isEqualToString:@"iPhone9,4"]) {
        return @"iPhone_7_Plus";
    }
   return @"Unknow";
    
}


@end
