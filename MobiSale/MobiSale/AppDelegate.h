//
//  AppDelegate.h
//  FTool
//
//  Created by MAC on 11/4/14.
//  Copyright (c) 2014 FPT.RAD.FTool. All rights reserved.
//
//vutt11-8291
#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "../Helper/GoogleAnalytics/GAI.h"
#import <Google/CloudMessaging.h>
#import "GCMSetup.h"
#import <Crashlytics/Crashlytics.h>
#import <Fabric/Fabric.h>

#define MAIN_COLOR_LIGHT [UIColor colorWithRed:255.0f/255.0f green:77.0f/255.0f blue:80.0f/255.0f alpha:1.0f];

#define XEM_TINHTRANG_KH = 1;


@interface AppDelegate : GCMSetup//UIResponder

//@property (strong, nonatomic) UIWindow *window;
@property(nonatomic, strong) id<GAITracker> tracker;

@property (assign, nonatomic) BOOL shouldRotate;
@property(nonatomic, strong) NSString* registrationTokenAPS;


@end
