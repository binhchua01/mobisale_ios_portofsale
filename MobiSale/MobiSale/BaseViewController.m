//
//  BaseViewController.m
//  FTool
//
//  Created by Pham Duy Khang on 11/6/14.
//  Copyright (c) 2014 FPT.RAD.FTool. All rights reserved.
//

#import "BaseViewController.h"
#import <netinet/in.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#import <sys/socket.h>
#import "ShareData.h"
#import "InfoController.h"
#import "SupportDeploymentController.h"
#import "AppDelegate.h"
#import "CustomerDetailViewController.h"
#import "ToDoListViewController.h"
#import "CustomerCareRecord.h"
#import "MessageRecord.h"
#import "MessageListViewController.h"
#import "HDNotificationView.h"

@interface BaseViewController () <MFMailComposeViewControllerDelegate>{
    MBProgressHUD *HUD;
    UIAlertView *basicAlertView;
    CustomerCareRecord *record;
    MessageRecord *messageRecord;
 }

@end

@implementation BaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupMenuBarButtonItems];
    [self.menuContainerViewController setRightMenuWidth:0];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UIBarButtonItems
- (void)setupMenuBarButtonItems {
    if(self.menuContainerViewController.menuState == MFSideMenuStateClosed &&
       ![[self.navigationController.viewControllers objectAtIndex:0] isEqual:self]) {
        self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
    } else {
        self.navigationItem.leftBarButtonItem = [self leftMenuBarButtonItem];
    }
}

- (UIBarButtonItem *)leftMenuBarButtonItem {
    UIImage *image = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"ic_menu"] height:30];
    return [[UIBarButtonItem alloc]
            initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered
            target:self
            action:@selector(leftSideMenuButtonPressed:)];
}

- (UIBarButtonItem *)rightMenuBarButtonItem {
    UIImage *image = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"ic_menu"] height:30];
    return [[UIBarButtonItem alloc]
            initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered
            target:self
            action:@selector(rightSideMenuButtonPressed:)];
    
}

#pragma mark - UIBarButtonItem Callbacks
- (void)backButtonPressed:(id)sender {
    ShareData *shared = [ShareData instance];
    if(shared.check){
        [self showBasicAlertView];
        return;
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showBasicAlertView {
    NSString *alertTitle = @"Thông báo";
    NSString *alertMessage = @"Bạn có muốn trở lại màn hình chính không";
    
    // ADD BY DANTT2
    basicAlertView = [[UIAlertView alloc] initWithTitle:alertTitle message:alertMessage delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [basicAlertView show];

}

- (void)leftSideMenuButtonPressed:(id)sender {
    
    [self.menuContainerViewController toggleLeftSideMenuCompletion:^{
    }];
}

- (UIBarButtonItem *)backBarButtonItem{
    UIImage *image = [SiUtils imageWithImageHeight:[[UIImage imageNamed:@"ic_pre"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] height:30];
    return [[UIBarButtonItem alloc]
            initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered
            target:self
            action:@selector(backButtonPressed:)];
}

#pragma show alert
- (void)showAlertBox:(NSString *)title
             message:(NSString *)message {
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
    [alertView show];
    
}

-(void)ShowAlertBoxEmpty{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:Mesage_DataEmpty delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    [alert show];
    [self performSelector:@selector(dismiss:) withObject:alert afterDelay:1.0];
}

-(void)showAlertBoxWithDelayDismiss:(NSString*)tile message:(NSString*)message {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:tile message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    [alert show];
    [self performSelector:@selector(dismiss:) withObject:alert afterDelay:2.0];
}
//vutt11
-(void)ShowAlertErrorSession{
    
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:@"Đã hết phiên làm việc. Vui lòng đăng nhập lại để tiếp tục" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    [alert show];
    [self LogOut];
    [self performSelector:@selector(dismiss:) withObject:alert afterDelay:2.0];
    
    
}

-(void)dismiss:(UIAlertView*)alert {
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}

// ADD BY DANTT2
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView == basicAlertView) {
        switch (buttonIndex) {
            case 0:
                break;
            case 1:
            {
                [self.navigationController popViewControllerAnimated:YES];
                ShareData *shared = [ShareData instance];
                shared.check =false;
            }
                break;
            default:
                break;
        }
    }
    
}

#pragma mark process bar
- (void) showMBProcess {
    if (self.navigationController.view == nil) {
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    } else {
        HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    }
    //[self.view addSubview:HUD];
    HUD.delegate = (id<MBProgressHUDDelegate>)self;
    HUD.labelText = @"Loading";
    [HUD show:YES];
}

- (void) hideMBProcess {
    [HUD hide:YES];
}


#pragma retun special type
- (BOOL) returnSpecialType:(NSString *)type
{
    if([type rangeOfString:@"?"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"˜"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"`"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"!"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"#"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"ˆ"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"*"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"("].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@")"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"+"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"{"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"}"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"["].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"]"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"|"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"&"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"$"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@"@"].location != NSNotFound)
        return 1;
    else if([type rangeOfString:@":"].location != NSNotFound)
        return 1;
    else if([type isEqualToString:@""])
        return 1;
    return 0;
    
}

#pragma getIPadress
-(NSString *)getIPAddress
{
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    NSString *wifiAddress = nil;
    NSString *cellAddress = nil;
    NSString *addr;
    if(!getifaddrs(&interfaces)) {
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            sa_family_t sa_type = temp_addr->ifa_addr->sa_family;
            if(sa_type == AF_INET || sa_type == AF_INET6) {
                NSString *name = [NSString stringWithUTF8String:temp_addr->ifa_name];
                addr = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                if([name isEqualToString:@"en0"]) {
                    wifiAddress = addr;
                } else
                    if([name isEqualToString:@"pdp_ip0"]) {
                        cellAddress = addr;
                    }
            }
            temp_addr = temp_addr->ifa_next;
        }
        // Free memory
        freeifaddrs(interfaces);
    }
    return addr;
}

-(void)LogOut{
    self.menuContainerViewController.menuState = MFSideMenuStateClosed;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:[NSBundle mainBundle]];
    if(IS_IPHONE4){
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone4" bundle:[NSBundle mainBundle]];
    }
    MFSideMenuContainerViewController *container = self.menuContainerViewController;
    
    NSString *firstViewControllerName = @"LoginViewController";
    
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:firstViewControllerName];
    [container setCenterViewController:navigationController];
    //container.enableMenu = NO;
    
}

#pragma mark - CustomerCareListCell delegate method - send email
- (void)sendEmailToAddress:(NSString*)stringAddress {
    
    @try {
        // To address
        NSArray *toRecipents = [NSArray arrayWithObject:stringAddress];
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setToRecipients:toRecipents];
             
        // Present mail view controller on screen
        [self presentViewController:mc animated:YES completion:NULL];
        
    } @catch (NSException *exception) {
        // Send email error
        NSLog(@"--------Send email error: %@", exception.description);
        NSLog(@"--------Send email error: %@", exception.description);
        [CrashlyticsKit recordCustomExceptionName:@"at BaseViewController - (function)sendEmailToAddress" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
        return;
        
    } @finally {
        return;
    }

}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            [Common_client showAlertMessage:[NSString stringWithFormat:@"Mail đã lưu"]];
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            [Common_client showAlertMessage:[NSString stringWithFormat:@"Mail đã gửi"]];
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            [Common_client showAlertMessage:[NSString stringWithFormat:@"Mail gửi bị lỗi"]];
            break;
        default:
            break;
    }
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

// set underline for title of button
- (void)setUnderLineForTitle:(UIButton*)button title:(NSString*)title{
    NSMutableAttributedString *commentString = [[NSMutableAttributedString alloc] initWithString:title];
    [commentString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInteger:NSUnderlineStyleSingle] range:NSMakeRange(0, [commentString length])];
    [button setAttributedTitle:commentString forState:UIControlStateNormal];
}
 

@end
