//
//  RightMenu.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 1/26/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "../Client/ShareData.h"

@protocol MenuRightClickDeleagate <NSObject>

-(void)MenuRightClicked: (NSString *) flat;

@end

@interface RightMenu : UITableViewController

@property (nonatomic, retain) id<MenuRightClickDeleagate> delegate;


@end
