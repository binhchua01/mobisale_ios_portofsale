//
//  MainPageViewController.m
//  MobiSale
//
//  Created by HIEUPC on 4/7/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "MainPageViewController.h"
#import "ReportSBIViewController.h"
#import "ShareData.h"
#import "InfoController.h"
#import "SupportDeploymentController.h"
#import "ConstructionVoteReturnListViewController.h"
// report
#import "ReportPrecheckListViewController.h"
#import "ReportSalaryViewController.h"
#import "ReportSurveyViewController.h"
#import "ReportRegisterViewController.h"
#import "ReportListSBIController.h"
#import "ReportListDeploymentN.h"
#import "ReportDeploymentPoup.h"
#import "ReportPayTVViewController.h"
#import "ReportSuspendCustomerViewController.h"
#import "ListPrechecklist.h"
#import "ListContactPrechecklist.h"
#import "RegisteredFormController.h"
#import "ListRegisteredFormController.h"
#import "ListRegistrationFormPageViewController.h"
#import "MainMap.h"
#import "Common_client.h"
#import "../Helper/GoogleAnalytics/GAITrackedViewController.h"
#import "SupportMobiSaleViewController.h"
#import "ListPotentialCustomersViewController.h"
#import "MapsListPotentialCustomersViewController.h"
#import "MessageListViewController.h"
#import "AppDelegate.h"

#import "CustomerCareViewController.h"
#import "NotificationListViewController.h"
#import "ReportPotentialViewController.h"
#import "PromotionProgramViewController.h"
//recive notification
#import "HDNotificationView.h"
#import "CustomerDetailViewController.h"
#import "ToDoListViewController.h"
#import "CustomerCareRecord.h"
#import "ActivitiesListViewController.h"
#import "AcitivitiesListOfSaleViewController.h"
#import "PotentialCustomerDetailForm.h"
#import "DetailCareCustomerViewController.h"
//chat view
#import "MessageListViewController.h"
#import "ChatViewController.h"
#import "UIButton+Badge.h"
// Sale more service
#import "SaleMoreServiceViewController.h"
#import "PotentialTabBarViewController.h"
#import "AppDelegate.h"
#import "ListRegisteredFormViewController.h"

#define messageNotice  @"message"
#define errorNotice    @"error"

@interface MainPageViewController ()<SupportMobiSaleViewControllerDelegate>

@end

@implementation MainPageViewController{
    NSDictionary *dic;
    CustomerCareRecord *record;
    BOOL isViewShowed;
    AppDelegate *appDelegate;
    NSMutableArray *arrRecord;
    NSDictionary *userInfo;
    NSDictionary *data;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    [CrashlyticsKit crash];
    
    // Do any additional setup after loading the view.
    self.title = @"MOBISALE";
    self.lblUserName.text = [ShareData instance].currentUser.userName;
    self.screenName=@"CHỨC NĂNG CHÍNH";
    // add by DanTT 2015.11.15
    // receive Notification
    isViewShowed = YES;
    arrRecord = [NSMutableArray array];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showReceivedMessage:)
                                                 name:appDelegate.messageKey
                                               object:nil];
    
    
    
    
    [self LoadSBI];
    
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self sendRegistrationTokenToServer];
        });
    });
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self loadDepartmentList];
    });
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self getAdminRegistrationID];
    });
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //    [self LoadSBI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - recive push notification
- (void)showReceivedMessage:(NSNotification *)notification {
    NSLog(@"show notification");
    
    userInfo = notification.userInfo;
    data = userInfo[@"data"];
    @try {
        NSString *contract, *imageName, *title, *message, *category = @"";
        category = data[@"category"];
        //contract = notification.userInfo[@"Contract"];
        contract = data[@"Contract"];
        
        if ([category isEqualToString:@"PTC_Return"] || [category isEqualToString:@"1800600"] || contract.length > 0) {
            imageName = @"notification_32";
            title    = userInfo[@"aps"][@"alert"][@"title"];
            message  = userInfo[@"aps"][@"alert"][@"body"];
            
        } else {
            imageName = @"icon_chat_512.png";
            title    = data[@"sendfrom"];
            message  = data[@"message"];
        }
        
        UIImage *image = [UIImage imageNamed:imageName];
        [self showNotificationWithTitle:title message:message icon:image];
        
    } @catch (NSException *exception) {
        NSLog(@"Parse data push notify error: %@", exception.description);
         [CrashlyticsKit recordCustomExceptionName:@"MainPageViewController - funtion (saveReceiveRemoteNotification)-Error handle Parse data push notify error" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
    }
}

- (void)showNotificationWithTitle:(NSString *)title message:(NSString *)message icon:(UIImage *)icon {
    [HDNotificationView showNotificationViewWithImage:icon title:title message:message isAutoHide:NO onTouch:^{
        [HDNotificationView hideNotificationViewOnComplete:nil];
        @try {
            UIViewController *vc = [self pushToView];
            [self.navigationController pushViewController:vc animated:YES];
            
        } @catch (NSException *exception) {
            NSLog(@"Push notify error: %@", exception.description);
             [CrashlyticsKit recordCustomExceptionName:@"MainPageViewController - funtion (saveReceiveRemoteNotification)-Error handle Parse data push notify error" reason:[[ShareData instance].currentUser.userName lowercaseString]frameArray:@[]];
        }

    }];
}

- (UIViewController *)pushToView {
    // IF notification received:
    /// Category is "PTC_Return": push to ConstructionVoteReturnListViewController.
    /// Category is "1800600": push to PotentialCustomerDetailForm View.
    /// Other Category:
    //// IF contract code # null: push to Customer Care View.
    //// IF contract code = null: push to Chat View.
    NSString *category = data[@"category"];
    
    // Notify Phiếu thi công trả về
    if ([category isEqualToString:@"PTC_Return"]) {
        
        return [self pushToConstructionVoteReturnListView];
        
    }
    // Notify Khách hàng tiềm năng từ tổng đài
    if ([category isEqualToString:@"1800600"]) {
        
        return [self pushToPotentialCustomerDetailView];
        
    }
    
    NSString *contract = data[@"Contract"];
    // Notify Chăm sóc khách hàng
    if (contract.length > 0) {
        return [self pushToCareCustomerDetailView];
    }
    // Notify Chat
    else {
        if (isChatViewActived) {
            return nil;
        }
        
        self.btnChat.badgeValue = StringFormat(@"%li",(long)[self.btnChat.badgeValue integerValue] + 1);
        return [self pushToChatView];
    }
}

// Notify là tin nhắn thì push tới view Chat
- (UIViewController *)pushToChatView {
    MessageListViewController *mvc = [[MessageListViewController alloc] init];
    return mvc;
}

// Notify là chăm sóc khách hàng thì push tới Chăm sóc khách hàng View
- (UIViewController *)pushToCareCustomerDetailView {
    CustomerCareRecord *rc = [[CustomerCareRecord alloc] initWithData:data];
    DetailCareCustomerViewController *dvc = [[DetailCareCustomerViewController alloc] initWithData:rc];
    return dvc;
    
}

// Notify là Phiếu thi công trả về thì push tới Phiếu thi công trả về View
- (UIViewController *)pushToConstructionVoteReturnListView {
    ConstructionVoteReturnListViewController *cvc = [[ConstructionVoteReturnListViewController alloc] init];
    return cvc;
}

// Notify là Khách hàng tiềm năng từ tổng đài thì push tới View Thông tin Khách hàng tiềm năng.
- (UIViewController *)pushToPotentialCustomerDetailView {
    PotentialCustomerDetailForm *pvc = [[PotentialCustomerDetailForm alloc] init];
    pvc.ID = data[@"PotentialObjID"];
    pvc.potentialType = FromNotification;
    
    return pvc;
}

#pragma mark - event button

-(IBAction)btnReport_clicked:(id)sender{
    [self CancelMenu];
    [self showMenuView:self.viewmenureport];

    
}

-(IBAction)btnCareCustomer_clicked:(id)sender{
    [self CancelMenu];
    [self showMenuView:self.viewmenuprechecklist];
}


-(IBAction)btnSale_clicked:(id)sender{
    [self CancelMenu];
    [self showMenuView:self.viewmenusale];
}

-(IBAction)btnInfo_clicked:(id)sender {
    [self CancelMenu];
    
    SupportMobiSaleViewController *vc = [[SupportMobiSaleViewController alloc] initWithNibName:@"SupportMobiSaleViewController" bundle:nil];
    vc.delegate = self;
    [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomTop];
    
}
-(IBAction)btnSupporDeployment_clicked:(id)sender
{
    [self CancelMenu];
    [self showMenuView:self.viewMenuSupportDeployment];

}

- (IBAction)btnChat_Clicked:(id)sender {
    MessageListViewController *vc = [[MessageListViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
    [self CancelMenu];
    self.btnChat.badgeValue = @"";
}


-(IBAction)btnSBIAble_clicked:(id)sender {
    //  [self showMBProcess];
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        //   [self hideMBProcess];
        return;
    }
    SBIViewController *vc= [[SBIViewController alloc]initWithNibName:@"SBIViewController" bundle:nil];
    vc.delegate = (id)self;
    vc.dic = dic;
    [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomTop];
}

-(IBAction)btnSBIUnAble_clicked:(id)sender {
    //  [self showMBProcess];
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        //      [self hideMBProcess];
        return;
    }
    SBIViewController *vc= [[SBIViewController alloc]initWithNibName:@"SBIViewController" bundle:nil];
    vc.delegate = (id)self;
    vc.dic = dic;
    [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomTop];
}

-(IBAction)btnTouchMenu_clicked:(id)sender{
    [self CancelMenu];
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    if(sender == self.btnSBI_clicked){
        [self repostSBIInit];
        return;
    }
    
    UIViewController *vc =[self getViewControllerWhenClickedButton:sender];
    [self.navigationController pushViewController:vc animated:YES];
    
//    UINavigationController *nav = [((UINavigationController *)self.menuContainerViewController.centerViewController) initWithRootViewController:vc ];
//    self.menuContainerViewController.centerViewController = nav;
//    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
}

- (UIViewController *)getViewControllerWhenClickedButton:(UIButton *)sender {
    
    UIViewController *vc;
    
    if(sender == self.btnReportContact){
        vc = [[ReportListDeploymentN alloc] init];
        return vc;
        
    }
    
    if (sender == self.btnReportPotential) { // report potential customer
        ReportPotentialViewController *rvc = [[ReportPotentialViewController alloc] init];
        rvc.reportType = reportPotential;
        
        return rvc;
    }
    
    if (sender == self.btnReportContractAuto) { // report contract auto
        ReportPotentialViewController *rvc = [[ReportPotentialViewController alloc] init];
        rvc.reportType = reportContractAuto;
        
        return rvc;
        
    }
    
    if (sender == self.btnReportPayTV) { // report Pay TV
        
        ReportPayTVViewController *rvc = [[ReportPayTVViewController alloc] init];
        
        return rvc;
        
    }
    // report suspend customer
    if (sender == self.btnReportSuspendCustomer) {
        vc = [[ReportSuspendCustomerViewController alloc] init];
        return vc;
    }
    
    if(sender == self.btnReportPrechecklist){
        
        vc = [[ReportPrecheckListViewController alloc]initWithNibName:@"ReportPrecheckListViewController" bundle:nil];
        return vc;
        
    }
    
    if(sender == self.btnReportRegister){
        
        vc = [[ReportRegisterViewController alloc]initWithNibName:@"ReportRegisterViewController" bundle:nil];
        return vc;
        
    }
    
    if(sender == self.btnReportSalary){
        
        vc = [[ReportSalaryViewController alloc]initWithNibName:@"ReportSalaryViewController" bundle:nil];
        return vc;
    }
    
    if(sender == self.btnReportSurvey){
        
        vc = [[ReportSurveyViewController alloc]initWithNibName:@"ReportSurveyViewController" bundle:nil];
        return vc;
    }
    
    
    if(sender == self.btnPromotionPrograms){ // chương trình khuyến mãi
        
        vc = [[PromotionProgramViewController alloc] initWithNibName:@"PromotionProgramViewController" bundle:nil];
        return vc;
    }
    
    if (sender == self.btnPotentialCustomersList){
        // khách hàng tiềm năng;
        vc = [self setViewPotentialCustomers];
        return vc;
    }
    
    if(sender == self.btnRegisterForm){
        
//        vc = [[RegisteredFormController alloc]initWithNibName:@"RegisteredFormController" bundle:nil];
        vc = [[ListRegisteredFormViewController alloc] initWithNibName:@"ListRegisteredFormViewController" bundle:nil];
        return vc;
    }
    
    if(sender == self.btnRegisterList){
        vc = [[ListRegistrationFormPageViewController alloc] init];
        return vc;
     
    }
    
    if(sender == self.btnRegisterListAutoContract){
        
        vc = [[ListRegisteredFormController alloc]initWithNibName:@"ListRegisteredFormController" bundle:nil];
        vc.title = @"DANH SÁCH PĐK/HĐ CHƯA DUYỆT";
        return vc;
    }
    // Tập điểm
    if(sender == self.btnIPTV){
        ShareData * share=[ShareData instance];
        share.tapDiem=true;
        vc = [[MainMap alloc]initWithNibName:@"MainMap" bundle:nil];
        return vc;
    }
    // Bán thêm loại dịch vụ
    if (sender == self.btnSaleMoreService) {
        vc = [[SaleMoreServiceViewController alloc] init];
        return vc;
        
    }
    
    if(sender == self.btnCustomerCare){
        // KS ĐỘ HÀI LÒNG KHÁCH HÀNG
        vc = [self setViewCustomerCare];
        return vc;
    }
    
    if(sender == self.btnActivitiesOfCustomer){
        // Hoạt động của khách hàng
        vc = [[AcitivitiesListOfSaleViewController alloc]initWithNibName:@"AcitivitiesListOfSaleViewController" bundle:nil];
        return vc;
    }
    
    if(sender == self.btnCreatePrecheckList){
        
        vc = [[ListContactPrechecklist alloc]initWithNibName:@"ListContactPrechecklist" bundle:nil];
        return vc;
    }
    
    if(sender == self.btnListPrecheckList){
        
        vc = [[ListPrechecklist alloc]initWithNibName:@"ListPrechecklist" bundle:nil];
        return vc;
    }
    
    // Hỗ trợ triển khai
    if(sender == self.btnSupportDeployment){
        NSString *nib =@"SupportDeploymentController";
        if(IS_IPHONE4){
            nib =@"SupportDeploymentController_iphone4";
        }
        
        vc = [[SupportDeploymentController alloc]initWithNibName:nib bundle:nil];
        return vc;
    }
    // Phiếu thi công trả về
    if(sender == self.btnConstructionVoteReturn){
        vc = [[ConstructionVoteReturnListViewController alloc]initWithNibName:@"ConstructionVoteReturnListViewController" bundle:nil];
        return vc;
    }
    
    return vc;
}

-(void)repostSBIInit{
    [self CancelMenu];
    //  [self showMBProcess];
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        //     [self hideMBProcess];
        return;
    }
    
    ReportSBIViewController *reportSBI = [[ReportSBIViewController alloc]initWithNibName:@"ReportSBIViewController" bundle:nil];
    reportSBI.delegate = (id)self;
    [self presentPopupViewController:reportSBI animationType:MJPopupViewAnimationSlideBottomTop];
    
}

#pragma mark - SBI delegate
-(void)callSBIAPISearch:(NSDictionary *)Data{
    //  [self showMBProcess];
    [self showMBProcess];
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    NSString *userName =[Data objectForKey:@"UserName"];
    NSString *Day =shared.daySBI =[Data objectForKey:@"Day"];
    NSString *Month =shared.monthSBI=[Data objectForKey:@"Month"];
    NSString *Year =shared.yearSBI =[Data objectForKey:@"Year"];
    NSString *Status =shared.statusSBI =[Data objectForKey:@"Status"];
    NSString *Agent =shared.typeSBI =[Data objectForKey:@"Agent"];
    NSString *AgentName =shared.contentSBI =[Data objectForKey:@"AgentName"];
    NSString *PageNumber =[Data objectForKey:@"PageNumber"];
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideLeftLeft];
    [shared.appProxy reportSBITotal:userName Day:Day Month:Month Year:Year Status:Status Agent:Agent AgentName:AgentName PageNumber:PageNumber completionHandler:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            return ;
        }
        NSArray *ListObject =[result objectForKey:@"ListObject"];
        if([ListObject count] <1){
            [self showAlertBox:@"Thông báo" message:@"Không có báo cáo SBI"];
            
        }else{
            shared.repostSBIList =ListObject;
            ReportListSBIController *vc = [[ReportListSBIController alloc]initWithNibName:@"ReportListSBIController" bundle:nil];
            [self.navigationController pushViewController:vc animated:YES];
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
        
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
    
}

-(void)ReportListDeploymentInit {
    [self CancelMenu];
    //  [self showMBProcess];
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    ReportDeploymentPoup *ReportListDeployment = [[ReportDeploymentPoup alloc]initWithNibName:@"ReportDeploymentPoup" bundle:nil];
    ReportListDeployment.delegate = (id)self;
    [self presentPopupViewController:ReportListDeployment animationType:MJPopupViewAnimationSlideBottomTop];
}

#pragma mark - ReportListDeployment delegate
-(void)callReportListDeploymentAPI {
    //  [self showMBProcess];
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideLeftLeft];
    ReportListDeploymentN *vc = [[ReportListDeploymentN alloc]initWithNibName:@"ReportListDeploymentN" bundle:nil];
    [self.navigationController pushViewController:vc animated:YES];
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
}

-(IBAction)btnCancelMenu_clicked:(id)sender {
    if(sender == self.btnCancelReport){
        [self hideMenuView:self.viewmenureport];
        
        return;
        
    }
    
    if(sender == self.btnCancelSale){
        [self hideMenuView:self.viewmenusale];
        
        return;
    }
    
    if(sender == self.btnCancelPrechecklist){
        [self hideMenuView:self.viewmenuprechecklist];
        
        return;
    }
    
    if(sender == self.btnCancelSupportDeployment){
        [self hideMenuView:self.viewMenuSupportDeployment];
        
        return;
    }
}

- (void)hideMenuView:(UIView *)menuView {
    [UIView animateWithDuration:0.4
                          delay:0.1
                        options: UIViewAnimationOptionTransitionNone
                     animations:^{
                         
                         menuView.frame = CGRectMake(0, [SiUtils getScreenFrameSize].height, menuView.frame.size.width, menuView.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         
                     }];
}

- (void)showMenuView:(UIView *)menuView {
    [self.view addSubview:menuView];
    
    menuView.frame = CGRectMake(0, [SiUtils getScreenFrameSize].height , menuView.frame.size.width, menuView.frame.size.height);
    [menuView setShadow];
    [UIView animateWithDuration:0.4
                          delay:0.1
                        options: UIViewAnimationOptionTransitionNone
                     animations:^{
                         
                         menuView.frame = CGRectMake(0, [SiUtils getScreenFrameSize].height - menuView.frame.size.height, menuView.frame.size.width, menuView.frame.size.height);
                     }
                     completion:^(BOOL finished){
                     }];
}

#pragma mark - Load data
-(void)LoadSBI {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    [self showMBProcess];
    
    ShareData *shared = [ShareData instance];
    [shared.appProxy GetSBIList:shared.currentUser.userName RegCode:@"" Status:@"-2" completionHandler:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        dic = result;
        
        [self.btnSBIAble setTitle:[dic objectForKey:@"SBIAble"] forState:UIControlStateNormal];
        [self.btnSBIUnAble setTitle:[dic objectForKey:@"SBIUnAble"] forState:UIControlStateNormal];
        
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
        [self hideMBProcess];
    }];
}

// add by DanTT 2015.11.16
- (void) sendRegistrationTokenToServer{
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    ShareData *shared = [ShareData instance];
    [shared.supportMobiSaleProxy registerPushNotificationGCMWithRegistrationID:shared.tokenAPNS completeHander:^(id result, NSString *errorCode, NSString *message) {
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        NSLog(@"status registration token to server: %@",message);
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
        [self hideMBProcess];
    }];
}

// add by DanTT 2015.11.16
- (void) getAdminRegistrationID{
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    ShareData *shared = [ShareData instance];
    [shared.supportMobiSaleProxy getRegisteredIDGCMWithAgent:@"0" agentName:@"HCM.GiauTQ" completeHander:^(id result, NSString *errorCode, NSString *message) {
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        NSLog(@"status get admin registration ID: %@",message);
        SupportMobiSaleRecord *rc = result;
        shared.supportMobiSaleRecord = rc;
        [self hideMBProcess];
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
        [self hideMBProcess];
    }];
}

#pragma mark - SBI delegate
-(void)CancelPopupInfo {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
}
-(void)CancelPopup {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
}

#pragma mark - Cancel menu
-(void)CancelMenu{
    [self.viewmenuprechecklist removeFromSuperview];
    [self.viewmenureport removeFromSuperview];
    [self.viewmenusale removeFromSuperview];
    [self.viewMenuSupportDeployment removeFromSuperview];
}

#pragma mark - Set view Potential Customers
- (UIViewController *)setViewPotentialCustomers{
    PotentialTabBarViewController *vc = [[PotentialTabBarViewController alloc] init];
    return vc;
}

#pragma mark - Set view Care Customers
- (UIViewController *)setViewCustomerCare{
    CustomerCareViewController *vc = [[CustomerCareViewController alloc] init];
    return vc;
}

- (void)addNewPotentialCustomer:(id) sender{
    ShareData *shared = [ShareData instance];
    shared.potentialRecord = [[PotentialCustomerDetailRecord alloc] init];
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"PotentialCustomerDetail" bundle:nil];
    UIViewController * initialVC = [storyboard instantiateInitialViewController];
    initialVC.title = @"THÊM KH TIỀM NĂNG";
    [self.navigationController pushViewController:initialVC animated:YES];
    
}

/*
 * load department list
 */
- (void)loadDepartmentList {
    ShareData *shared = [ShareData instance];
    [shared.reportlistdeploymentproxy getDepartmentList:shared.currentUser.userName completeionHander:^(id result, NSString *errorCode, NSString *message) {
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"0"] || result == nil){
            NSLog(@"Không lấy được danh sách phòng");
            return ;
        }
        NSMutableArray *arr = [NSMutableArray array];
        for (NSDictionary *dict in result) {
            NSString *key   = [dict objectForKey:@"ID"];
            NSString *value = [dict objectForKey:@"Name"];
            [arr addObject:[NSDictionary dictionaryWithObject:value forKey:key]];
        }
        shared.currentUser.arrDepartment = [NSArray arrayWithArray:arr];
        NSLog(@"Lấy được danh sách phòng");
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
        [self hideMBProcess];
    }];
    
}

@end
