//
//  SubMenuTableViewCell.m
//  FTool
//
//  Created by MAC on 11/4/14.
//  Copyright (c) 2014 FPT.RAD.FTool. All rights reserved.
//

#import "SubMenuTableViewCell.h"

@implementation SubMenuTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        CGRect rectImv = CGRectMake(20, 0, 50, 40);
        self.imv_Icon = [[UIImageView alloc] initWithFrame:rectImv];
        self.imv_Icon.contentMode = UIViewContentModeCenter;
        CGRect rectText = CGRectMake(rectImv.origin.x+rectImv.size.width -10, 0, 280, 40);
        
        self.lbl_SubMenu = [[UILabel alloc] initWithFrame:rectText];
        self.lbl_SubMenu.text = @"";
        self.lbl_SubMenu.font = [UIFont fontWithName:FontThin size:14];
        self.lbl_SubMenu.textColor = [UIColor whiteColor];//colorWithRGB(184, 184, 184,1);;
        
        [self addSubview:self.imv_Icon];
        [self addSubview:self.lbl_SubMenu];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
