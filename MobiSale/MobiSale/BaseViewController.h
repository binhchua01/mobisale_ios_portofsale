//
//  BaseViewController.h
//  FTool
//
//  Created by Pham Duy Khang on 11/6/14.
//  Copyright (c) 2014 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../Helper/MFSideMenu/MFSideMenu.h"
#import "../Helper/SiUtils.h"
#import <GoogleMaps/GoogleMaps.h>
//#import "../Helper/MBProgress/MBProgressHUD.h"
#import "UIViewController+MJPopupViewController.h"
#import "../Helper/GoogleAnalytics/GAITrackedViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "CustomerCareListViewCell.h"
#import "ToDoListViewCell.h"
#import <MessageUI/MessageUI.h>
#import "NSUserDefaults+Notification.h"
#import "UIColor+FPTCustom.h"
#import "UIButton+FPTCustom.h"
#import "UILabel+FPTCustom.h"
#import "UIView+FPTCustom.h"
#import "Common.h"

typedef enum {
    ListRegisteredFormNorm = 1,
    ListRegisteredFormAutoContract
} typeListRegisteredForm;

@interface BaseViewController : GAITrackedViewController <GMSMapViewDelegate, CustomerCareListCellDelegate, ToDoListCellDelegate, UIActionSheetDelegate>{
    UIControl *_overlayView;
}

@property GMSMapView *t;

@property (nonatomic,retain) UITableView *tableView;
@property (nonatomic,retain) NSArray *objectNameList;
@property (nonatomic,retain) NSString *regcode;

/*!CreatetableView*/
//- (void)createTableView:(CGRect)rect;
/*!show/hiden*/
//- (void)hiddenTableObjectName:(BOOL)_bool;
/* return special type*/
- (UIBarButtonItem *)rightMenuBarButtonItem;
- (UIBarButtonItem *)backBarButtonItem;
- (BOOL)returnSpecialType:(NSString *)type;
- (void)showAlertBoxWithDelayDismiss:(NSString*)tile message:(NSString*)message;
- (void)showAlertBox:(NSString *)title
             message:(NSString *)message;
- (void)ShowAlertBoxEmpty;
- (void)ShowAlertErrorSession;
- (void)showMBProcess;
- (void)hideMBProcess;
- (NSString *)getIPAddress;
- (void)LogOut;
- (void)setUnderLineForTitle:(UIButton*)button title:(NSString*)title;

@end
