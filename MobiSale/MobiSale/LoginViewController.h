//
//  LoginViewController.h
//  FTool
//
//  Created by MAC on 11/4/14.
//  Copyright (c) 2014 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../Client/ShareData.h"
#import "../Helper/SiUtils.h"
//#import "../Helper/MBProgress/MBProgressHUD.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "UIViewController+MJPopupViewController.h"
#import "InfoController.h"
#import "GuidController.h"


@interface LoginViewController : BaseViewController<InfoDelegate,GuidDelegate, UITextFieldDelegate>


@property(nonatomic,retain)IBOutlet UIButton *btn_Login;
@property(nonatomic,retain)IBOutlet UITextField *txt_UserName;
@property(nonatomic,retain)IBOutlet UITextField *txt_PassWord;

@property (nonatomic,retain)IBOutlet UIButton *btnGuid;
@property (nonatomic,retain)IBOutlet UIButton *btnInfo;
@property (nonatomic,retain)IBOutlet UIButton *btnExit;




@property (retain,nonatomic) UIAlertView *alertViewGetVersion;
@property (retain,nonatomic) UIAlertView *alertLogout;

@property (retain,nonatomic) NSString *link;

- (IBAction)btn_Login_touchUpInside:(id)sender;
-(IBAction)btnGuid_clicked:(id)sender;
-(IBAction)btnInfo_clicked:(id)sender;
-(IBAction)btnExit_clicked:(id)sender;

@end
