//
//  LeftMenuTableViewController.m
//  FTool
//
//  Created by MAC on 11/4/14.
//  Copyright (c) 2014 FPT.RAD.FTool. All rights reserved.
//

#import "LeftMenuTableViewController.h"
#import "../Helper/SiUtils.h"
#import "../Helper/MFSideMenu/MFSideMenu.h"
#import "LeftMenuCell.h"
#import "RegisteredFormController.h"
#import "ListRegisteredFormController.h"
#import "ListRegistrationFormPageViewController.h"
#import "SupportDeploymentController.h"
#import "ConstructionVoteReturnListViewController.h"
#import "ListPrechecklist.h"
#import "ListContactPrechecklist.h"

#import "ReportSalaryViewController.h"
#import "ReportSurveyViewController.h"
#import "ReportPrecheckListViewController.h"
#import "ReportRegisterViewController.h"
#import "MainMap.h"
#import "MainPageViewController.h"
#import "ReportDeploymentPoup.h"
#import "ReportSBIViewController.h"
#import "ReportPotentialViewController.h"
#import "ReportListDeploymentN.h"
#import "ReportPayTVViewController.h"

#import "ListPotentialCustomersViewController.h"
#import "MapsListPotentialCustomersViewController.h"
#import "CustomerCareViewController.h"
#import "CustomerDetailViewController.h"
#import "ToDoListViewController.h"
#import "CustomerCareRecord.h"
#import "ActivitiesListViewController.h"
#import "AcitivitiesListOfSaleViewController.h"
#import "NotificationListViewController.h"
#import "PromotionProgramViewController.h"

#import "SaleMoreServiceViewController.h"
#import "ReportSuspendCustomerViewController.h"
#import "PotentialTabBarViewController.h"

#import "ListRegisteredFormViewController.h"

@interface LeftMenuTableViewController ()


@end

@implementation LeftMenuTableViewController {
    NSMutableArray *arrRecord;
}


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:0.263 green:0.361 blue:0.522 alpha:1] ;
    arrRecord = [NSMutableArray array];
    NSString *userName = [[ShareData instance].currentUser.userName lowercaseString];
    NSString *keyUserDefautls = StringFormat(@"%@-notification",userName);

    NSArray *arrUser = [[[NSUserDefaults standardUserDefaults] arrayForKey:keyUserDefautls] mutableCopy];
    [arrRecord addObjectsFromArray:arrUser];
    
}
-(void)viewDidAppear:(BOOL)animated{
    self.tableView.frame = CGRectMake(0, 0, self.view.frame.size.width, [SiUtils getScreenFrameSize].height);

}

#pragma layout view
- (void)layoutView
{

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        // Trang chủ
        case 0:
            return 1;
            break;
        // Bán hàng
        case 1:
            return 6;
            break;
        // Chăm sóc khách hàng
        case 2:
            return 4;
            break;
        // Hỗ trợ triển khai
        case 3:
            return 2;
            break;
        // Báo cáo
        case 4:
            return 10;
            break;
        // Hệ thống
        case 5:
            return 3;
            break;
        default:
            break;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:
                                 CGRectMake(0, 100, tableView.frame.size.width, 50)];
    sectionHeaderView.backgroundColor = [UIColor colorWithRed:0.00 green:0.18 blue:0.31 alpha:1.0];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:
                            CGRectMake(5, 3, tableView.frame.size.width, 25.0)];
    
    headerLabel.backgroundColor = [UIColor clearColor];
    [headerLabel setFont:[UIFont fontWithName:@"" size:15.0]];
    headerLabel.textColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
    [sectionHeaderView addSubview:headerLabel];
    
    switch (section) {
        case 0:
            headerLabel.text = @"";
            return sectionHeaderView;
            break;
        case 1:
            headerLabel.text = @"BÁN HÀNG";
            return sectionHeaderView;
            break;
        case 2:
            headerLabel.text = @"CHĂM SÓC KHÁCH HÀNG";
            return sectionHeaderView;
            break;
        case 3:
            headerLabel.text = @"HỖ TRỢ TRIỂN KHAI";
            return sectionHeaderView;
            break;
        case 4:
            headerLabel.text = @"BÁO CÁO";
            return sectionHeaderView;
            break;
        case 5:
            headerLabel.text = @"HỆ THỐNG";
            return sectionHeaderView;
            break;
        default:
            break;
    }
    
    return sectionHeaderView;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    return 30;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"LeftMenuCell";
    LeftMenuCell *cell = (LeftMenuCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"LeftMenuCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    
    cell.backgroundColor = [UIColor colorWithRed:0.263 green:0.361 blue:0.522 alpha:1];

    // Trang chủ
    /// Màn hình chính
    if (indexPath.section == 0) {
        cell.lbl_Label.text = @"MÀN HÌNH CHÍNH";
        cell.ic_image.image = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"logo_icon.png"] height:30];
        return cell;
    }
    // Bán hàng
    /// Bán thêm dịch vụ
    /// Danh sách PĐK
    /// Tạo PĐK
    /// Danh sách PĐK đã lên hợp đồng chưa duyệt
    /// Khách hàng tiềm năng
    /// Chương trình khuyến mãi
    if(indexPath.section == 1){
        switch (indexPath.row)
        {
            case 0:
                cell.lbl_Label.text = @"BÁN THÊM DỊCH VỤ";
                cell.ic_image.image = [UIImage imageNamed:@"ic_menu_create_registration.png"];
                break;
                
            case 1:
                cell.lbl_Label.text = @"DANH SÁCH PHIẾU ĐĂNG KÝ";
                cell.ic_image.image = [UIImage imageNamed:@"ic_menu_list_registration.png"];
                break;
                
            case 2:
                cell.lbl_Label.text = @"TẠO PHIẾU ĐĂNG KÝ";
                cell.ic_image.image = [UIImage imageNamed:@"ic_menu_create_registration.png"];
                break;
                
            case 3:
                cell.lbl_Label.text = @"DANH SÁCH PĐK/HĐ CHƯA DUYỆT";
                cell.ic_image.image = [UIImage imageNamed:@"ic_menu_list_registration.png"];
                break;
                
            case 4:
                cell.lbl_Label.text = @"KHÁCH HÀNG TIỀM NĂNG";
                cell.ic_image.image = [UIImage imageNamed:@"ic_menu_potential_obj.png"];
                break;
                
            case 5:
                cell.lbl_Label.text = @"CHƯƠNG TRÌNH KHUYẾN MÃI";
                cell.ic_image.image = [UIImage imageNamed:@"ic_menu_list_registration.png"];
                break;
            default:
                break;

        }
        return cell;
    }
    // Chăm sóc khách hàng
    /// Khảo sát độ hài lòng khách hàng
    /// Hoạt động khách hàng
    /// Danh sách PrecheckList
    /// Tạo PrecheckList
    if (indexPath.section == 2) {
        switch (indexPath.row) {
                
            case 0:
                cell.lbl_Label.text = @"KS ĐỘ HÀI LÒNG KHÁCH HÀNG";
                cell.ic_image.image = [UIImage imageNamed:@"ic_menu_customer_care.png"];
                break;
                
            case 1:
                cell.lbl_Label.text = @"HOẠT ĐỘNG CỦA KHÁCH HÀNG";
                cell.ic_image.image = [UIImage imageNamed:@"ic_menu_activity.png"];
                break;
                
            case 2:
                cell.lbl_Label.text = @"DANH SÁCH PRECHECKLIST";
                cell.ic_image.image = [UIImage imageNamed:@"ic_menu_list_prechecklist.png"];
                break;
            
            case 3:
                cell.lbl_Label.text = @"TẠO PRECHECKLIST";
                cell.ic_image.image = [UIImage imageNamed:@"ic_menu_create_prechecklist.png"];
                break;

            default:
                break;

        }
        return cell;
    }
    // Hỗ trợ triển khai
    /// Hỗ trợ triển khai
    /// Phiếu thi công trả về
    if (indexPath.section == 3) {
        switch (indexPath.row) {
            
            case 0:
                cell.lbl_Label.text = @"HỖ TRỢ TRIỂN KHAI";
                cell.ic_image.image = [UIImage imageNamed:@"ic_menu_support_develop.png"];
                break;
                
            case 1:
                cell.lbl_Label.text = @"PHIẾU THI CÔNG TRẢ VỀ";
                cell.ic_image.image = [UIImage imageNamed:@"ic_menu_support_develop.png"];
                break;
                
            default:
                break;
        }
        
        return cell;
    }
    // Báo cáo
    /// Báo cáo Pay TV
    /// Báo cáo Hợp đồng tự động
    /// Báo cáo Khách hàng tiềm năng
    /// Báo cáo SBI
    /// Báo cáo Khảo sát
    /// Báo cáo Phát triển thuê bao
    /// Báo cáo PrecheckList
    /// Báo cáo PĐK
    /// Báo cáo Lương
    /// Báo cáo Khách hàng rời mạng
    if(indexPath.section == 4){
        switch (indexPath.row) {

            case 0:
                cell.lbl_Label.text = @"BÁO CÁO PAY TV";
                cell.ic_image.image = [UIImage imageNamed:@"ic_report_prechecklist_2.png"];
                break;
                
            case 1:
                cell.lbl_Label.text = @"BÁO CÁO HỢP ĐỒNG TỰ ĐỘNG";
                cell.ic_image.image = [UIImage imageNamed:@"ic_report_register.png"];
                break;
                
            case 2:
                cell.lbl_Label.text = @"BÁO CÁO KHÁCH HÀNG TIỀM NĂNG";
                cell.ic_image.image = [UIImage imageNamed:@"ic_report_register.png"];
                break;
            
            case 3:
                cell.lbl_Label.text = @"BÁO CÁO SBI";
                cell.ic_image.image = [UIImage imageNamed:@"ic_report_prechecklist_2.png"];
                break;
                
            case 4:
                cell.lbl_Label.text = @"BÁO CÁO KHẢO SÁT";
                cell.ic_image.image = [UIImage imageNamed:@"ic_report_survey.png"];
                break;
                
            case 5:
                cell.lbl_Label.text = @"BÁO CÁO PHÁT TRIỂN THUÊ BAO";
                cell.ic_image.image = [UIImage imageNamed:@"ic_report_new_object.png"];
                break;
                
            case 6:
                cell.lbl_Label.text = @"BÁO CÁO PRECHECKLIST";
                cell.ic_image.image = [UIImage imageNamed:@"ic_report_prechecklist.png"];
                break;
                
            case 7:
                cell.lbl_Label.text = @"BÁO CÁO ĐĂNG KÝ";
                cell.ic_image.image = [UIImage imageNamed:@"ic_report_register.png"];
                break;
                
            case 8:
                cell.lbl_Label.text = @"BÁO CÁO LƯƠNG";
                cell.ic_image.image = [UIImage imageNamed:@"ic_report_salary.png"];
                break;
                
            case 9:
                cell.lbl_Label.text = @"BÁO CÁO KH RỜI MẠNG";
                cell.ic_image.image = [UIImage imageNamed:@"ic_report_salary.png"];
                break;
            default:
                break;
        }
    }
    // Hệ thống
    /// Đổi mật khẩu
    /// Thông tin ứng dụng
    /// Đăng xuất
    if(indexPath.section == 5){
        switch (indexPath.row)
        {
            case 0:
                cell.lbl_Label.text = @"ĐỔI MẬT KHẨU";
                cell.ic_image.image = [UIImage imageNamed:@"ic_reset_pasword.png"];
                break;
            case 1:
                cell.lbl_Label.text = @"THÔNG TIN ỨNG DỤNG";
                cell.ic_image.image = [UIImage imageNamed:@"ic_menu_info.png"];
                
                break;
            case 2:
                cell.lbl_Label.text = @"ĐĂNG XUẤT";
                cell.ic_image.image = [UIImage imageNamed:@"ic_menu_logout.png"];
                break;
        }
        
        return cell;
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UIViewController *vc ;
    NSString *nibName = @"";
    // Trang chủ
    /// Màn hình chính
    if (indexPath.section == 0) {
        nibName = @"MainPageViewController";
        if(IS_IPHONE4){
            nibName = @"MainPageViewController_IPhone4";
        }
        vc =  [[MainPageViewController alloc] initWithNibName:nibName bundle:nil];
        
        UINavigationController *nav = [((UINavigationController *)self.menuContainerViewController.centerViewController) initWithRootViewController:vc ];
        
        self.menuContainerViewController.centerViewController = nav;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
        return;
    }
    // Bán hàng
    /// Bán thêm dịch vụ
    /// Danh sách PĐK
    /// Tạo PĐK
    /// Danh sách PĐK đã lên hợp đồng chưa duyệt
    /// Khách hàng tiềm năng
    /// Chương trình khuyến mãi
    if(indexPath.section == 1){
        switch (indexPath.row)
        {
            // Bán thêm dịch vụ
            case 0:
                vc = [[SaleMoreServiceViewController alloc] init];
                break;
                
            // Danh sách PĐK
            case 1:
                vc = [[ListRegistrationFormPageViewController alloc] init];
                break;
                
            // Tạo PĐK
            case 2:
                
                vc = [[ListRegisteredFormViewController alloc] initWithNibName:@"ListRegisteredFormViewController" bundle:nil];
                
//               vc =  [[RegisteredFormController alloc] initWithNibName:@"RegisteredFormController" bundle:nil];
                
                break;
                
            // Danh sách PĐK đã lên hợp đồng chưa duyệt
            case 3:
                nibName = @"ListRegisteredFormController";
                vc =  [[ListRegisteredFormController alloc] initWithNibName:nibName bundle:nil];
                vc.title = @"DANH SÁCH PĐK/HĐ CHƯA DUYỆT";
                break;
                
            // Khách hàng tiềm năng;
            case 4:
                vc = [self setViewPotentialCustomers];
                break;
                
            // Chương trình khuyến mãi
            case 5:
                vc = [[PromotionProgramViewController alloc] init];
                break;
                
            default:
                break;
                
        }
        
        UINavigationController *nav = [((UINavigationController *)self.menuContainerViewController.centerViewController) initWithRootViewController:vc ];
        
        self.menuContainerViewController.centerViewController = nav;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
        return;
    }
    // Chăm sóc khách hàng
    /// Khảo sát độ hài lòng khách hàng
    /// Hoạt động khách hàng
    /// Danh sách PrecheckList
    /// Tạo PrecheckList
    if (indexPath.section == 2) {
        switch (indexPath.row) {
            case 0:
                // KS ĐỘ HÀI LÒNG KHÁCH HÀNG
                vc = [self setViewCustomerCare];
                break;
                
            case 1:
                // Hoạt động của khách hàng
                vc = [[AcitivitiesListOfSaleViewController alloc]initWithNibName:@"AcitivitiesListOfSaleViewController" bundle:nil];
                break;
                
            // Danh sách PrecheckList
            case 2:
                nibName = @"ListPrechecklist";
                if(IS_IPHONE4){
                    nibName = @"ListPrechecklist_iphone4";
                }
                vc =  [[ListPrechecklist alloc] initWithNibName:nibName bundle:nil];
                break;
                
            // Tạo PrecheckList
            case 3:
                nibName = @"ListContactPrechecklist";
                if(IS_IPHONE4){
                    nibName = @"ListContactPrechecklist_iphone4";
                }
                vc =  [[ListContactPrechecklist alloc] initWithNibName:nibName bundle:nil];
                break;
  
            default:
                break;
        }
        
        UINavigationController *nav = [((UINavigationController *)self.menuContainerViewController.centerViewController) initWithRootViewController:vc ];
        
        self.menuContainerViewController.centerViewController = nav;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
        return;
        
    }
    // Hỗ trợ triển khai
    /// Hỗ trợ triển khai
    /// Phiếu thi công trả về
    if (indexPath.section == 3) {
        switch (indexPath.row) {
            // Hỗ trợ triển khai
            case 0:
                nibName = @"SupportDeploymentController";
                if(IS_IPHONE4){
                    nibName = @"SupportDeploymentController_iphone4";
                }
                vc =  [[SupportDeploymentController alloc] initWithNibName:nibName bundle:nil];
                break;
                
            // Phiếu thi công trả về
            case 1:
            {
                ConstructionVoteReturnListViewController *rvc = [[ConstructionVoteReturnListViewController alloc] init];
                vc = rvc;
            }
                break;
        }
        
        UINavigationController *nav = [((UINavigationController *)self.menuContainerViewController.centerViewController) initWithRootViewController:vc ];
        
        self.menuContainerViewController.centerViewController = nav;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
        return;
    }
    // Báo cáo
    /// Báo cáo Pay TV
    /// Báo cáo Hợp đồng tự động
    /// Báo cáo Khách hàng tiềm năng
    /// Báo cáo SBI
    /// Báo cáo Khảo sát
    /// Báo cáo Phát triển thuê bao
    /// Báo cáo PrecheckList
    /// Báo cáo PĐK
    /// Báo cáo Lương
    /// Báo cáo Khách hàng rời mạng
    if (indexPath.section == 4){
        switch (indexPath.row) {
            // Báo cáo Pay TV
            case 0:
            {
                ReportPayTVViewController *rvc = [[ReportPayTVViewController alloc] init];
                vc = rvc;
            }
                break;
                
            // Báo cáo Hợp đồng tự động
            case 1:
            {
                ReportPotentialViewController *vcr = [[ReportPotentialViewController alloc] init];
                vcr.reportType = reportContractAuto;
                vc = vcr;
            }
                break;
            // Báo cáo Khách hàng tiềm năng
            case 2:
            {
                ReportPotentialViewController *vcr = [[ReportPotentialViewController alloc] init];
                vcr.reportType = reportPotential;
                vc = vcr;
            }
                break;
            
            // Báo cáo SBI
            case 3:
                [self repostSBIInit];
                return;
                break;
                
            // Báo cáo Khảo sát
            case 4:
                nibName = @"ReportSurveyViewController";
                vc =  [[ReportSurveyViewController alloc] initWithNibName:nibName bundle:nil];
                break;
     
            // Báo cáo Phát triển thuê bao
            case 5:
                vc = [[ReportListDeploymentN alloc] init];
                break;
                
            // Báo cáo PrecheckList
            case 6:
                nibName = @"ReportPrecheckListViewController";
                vc =  [[ReportPrecheckListViewController alloc] initWithNibName:nibName bundle:nil];
                break;
                
            // Báo cáo PĐK
            case 7:
                nibName = @"ReportRegisterViewController";
                vc =  [[ReportRegisterViewController alloc] initWithNibName:nibName bundle:nil];
                break;
                
            // Báo cáo Lương
            case 8:
                nibName = @"ReportSalaryViewController";
                vc =  [[ReportSalaryViewController alloc] initWithNibName:nibName bundle:nil];
                break;
                
            // Báo cáo Khách hàng rời mạng
            case 9:
                vc =  [[ReportSuspendCustomerViewController alloc] init];
                break;
                
            default:
                break;
        }
        UINavigationController *nav = [((UINavigationController *)self.menuContainerViewController.centerViewController) initWithRootViewController:vc ];
        
        self.menuContainerViewController.centerViewController = nav;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        
        return;
         
    }
    // Hệ thống
    /// Đổi mật khẩu
    /// Thông tin ứng dụng
    /// Đăng xuất
    if (indexPath.section == 5) {
        // Đổi mật khẩu
        if(indexPath.row == 0){
            ChangePassViewController *change = [[ChangePassViewController alloc]initWithNibName:@"ChangePassViewController" bundle:nil];
            [self presentPopupViewController:change animationType:MJPopupViewAnimationSlideBottomTop];
            change.delegateChange = (id)self;
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
            
            return;
        }
        // Thông tin ứng dụng
        if(indexPath.row == 1){
            InfoController *info =  [[InfoController alloc] initWithNibName:@"InfoController" bundle:nil];
            info.isLogin = YES;
            [self presentPopupViewController:info animationType:MJPopupViewAnimationSlideBottomTop];
            info.delegate = (id)self;
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
            
            return;
          
        }
        // Đăng xuất
        if(indexPath.row == 2){
            [self LogOut];
            
            return;
        }
    }
    
}

-(void)repostSBIInit{
    UIViewController *vc ;
    self.menuContainerViewController.menuState = MFSideMenuStateClosed;
    NSString *nibName = @"MainPageViewController";
    if(IS_IPHONE4){
        nibName = @"MainPageViewController_IPhone4";
    }
    vc  =  [[MainPageViewController alloc] initWithNibName:nibName bundle:nil];
    UINavigationController *nav = [((UINavigationController *)self.menuContainerViewController.centerViewController) initWithRootViewController:vc ];
    self.menuContainerViewController.centerViewController = nav;
    
    ReportSBIViewController *reportSBI = [[ReportSBIViewController alloc]initWithNibName:@"ReportSBIViewController" bundle:nil];
    reportSBI.delegate = (id)vc;
    [self presentPopupViewController:reportSBI animationType:MJPopupViewAnimationSlideBottomTop];
    
}

-(void)ReportListDeploymentInit{
    UIViewController *vc ;
      self.menuContainerViewController.menuState = MFSideMenuStateClosed;
    ReportDeploymentPoup *ReportListDeployment = [[ReportDeploymentPoup alloc]initWithNibName:@"ReportDeploymentPoup" bundle:nil];
   
    NSString *nibName = @"MainPageViewController";
    if(IS_IPHONE4){
        nibName = @"MainPageViewController_IPhone4";
    }
     vc  =  [[MainPageViewController alloc] initWithNibName:nibName bundle:nil];
    UINavigationController *nav = [((UINavigationController *)self.menuContainerViewController.centerViewController) initWithRootViewController:vc ];
    self.menuContainerViewController.centerViewController = nav;
    ReportListDeployment.delegate = (id)vc;
    
    [self presentPopupViewController:ReportListDeployment animationType:MJPopupViewAnimationSlideBottomTop];
    
}

- (BOOL) allowsHeaderViewsToFloat{
    return YES;
}

-(void)LogOut
{
    self.menuContainerViewController.menuState = MFSideMenuStateClosed;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:[NSBundle mainBundle]];
    if(IS_IPHONE4){
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone4" bundle:[NSBundle mainBundle]];
    }
    MFSideMenuContainerViewController *container = self.menuContainerViewController;
    
    NSString *firstViewControllerName = @"LoginViewController";
    
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:firstViewControllerName];
    [container setCenterViewController:navigationController];

}

#pragma mark -delegate
-(void)CancelPopupInfo{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
}

-(void)CancelPopupChangePass
{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
}

#pragma mark - Set view Care Customers

- (UIViewController *)setViewCustomerCare{
    CustomerCareViewController *vc = [[CustomerCareViewController alloc] init];
    return vc;
}

#pragma mark - Set view Potential Customers
- (UIViewController *)setViewPotentialCustomers{
    PotentialTabBarViewController *vc = [[PotentialTabBarViewController alloc] init];
    return vc;
}

- (void)addNewPotentialCustomer:(id)sender{
    
    ShareData *shared = [ShareData instance];
    shared.potentialRecord = [[PotentialCustomerDetailRecord alloc] init];
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"PotentialCustomerDetail" bundle:nil];
    UIViewController * initialVC = [storyboard instantiateInitialViewController];
    initialVC.navigationItem.rightBarButtonItem = [self setRightBarButtonItem];
    initialVC.title = @"THÊM KH TIỀM NĂNG";
    UINavigationController *nav = [((UINavigationController *)self.menuContainerViewController.centerViewController) initWithRootViewController:initialVC ];
    self.menuContainerViewController.centerViewController = nav;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    
}


- (UIBarButtonItem *)setRightBarButtonItem {
    UIImage *image = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"save_32_2"] height:30];
    return [[UIBarButtonItem alloc]
            initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered
            target:self
            action:@selector(buttonSave_Click:)];
    
}

- (void)buttonSave_Click:(id)sender {
    NSLog(@"Saving new potential customer...");
}


@end
