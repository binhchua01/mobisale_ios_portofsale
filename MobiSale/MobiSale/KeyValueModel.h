//
//  KeyValueModel.h
//  MobiPay
//
//  Created by VANDN on 3/13/14.
//  Copyright (c) 2014 VANDN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KeyValueModel : NSObject{
    NSString *Values;
    NSString *Key;
}
@property(nonatomic,copy)NSString *Values;
@property(nonatomic,copy)NSString *Key;

-(id)initWithName:(NSString*)n description:(NSString *)desc;

@end
