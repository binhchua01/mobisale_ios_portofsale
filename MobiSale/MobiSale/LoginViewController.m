//
//  LoginViewController.m
//  FTool
//
//  Created by Nguyen Tan Tho on 11/4/14.
//  Copyright (c) 2014 FPT.RAD.FTool. All rights reserved.
//

#import "LoginViewController.h"
#import "../Helper/MFSideMenu/MFSideMenu.h"
#import "LeftMenuTableViewController.h"
#import "../Helper/OpenUDID/OpenUDID.h"
#import "GuidController.h"
#import "InfoController.h"
#import "MainPageViewController.h"
#import "Common_client.h"

#import "../Helper/GoogleAnalytics/GAITrackedViewController.h"

// recieve push notification
#import "AppDelegate.h"
#import "ConstructionVoteReturnListViewController.h"
#import "HDNotificationView.h"
#import "CustomerDetailViewController.h"
#import "ToDoListViewController.h"
#import "CustomerCareRecord.h"
#import "ActivitiesListViewController.h"
#import "AcitivitiesListOfSaleViewController.h"
#import "MessageListViewController.h"
#import "PotentialCustomerDetailForm.h"
#import "DetailCareCustomerViewController.h"
#import "AppDelegate.h"

typedef enum : NSUInteger {
    e_ShowMessage,
    e_RegisterIMEI,
    
} AlertType;

@interface LoginViewController ()
{
    bool flagClick;
    // recive push notification
    CustomerCareRecord *record;
    BOOL isNotificationViewShowed;
    AppDelegate *appDelegate;
    BOOL isLoginSuccessed;
    NSDictionary *data;
}
@end

@implementation LoginViewController {
    
    UIAlertView *alertView;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.txt_UserName.delegate = self;
    self.txt_PassWord.delegate = self;
    //ShareData *shared = [ShareData instance];
    [self layoutView];
    
    [self getversion];
    self.title  = @"MOBISALE";
    self.menuContainerViewController.leftMenuWidth = 0;
    flagClick  = NO;
    self.screenName = @"ĐĂNG NHẬP";
    [self.btn_Login setEnabled:NO];
    
    // add by DanTT 2015.11.15
    // receive Notification
    isLoginSuccessed = NO;
    isNotificationViewShowed = NO;
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showReceivedMessage:)
                                                 name:appDelegate.messageKey
                                               object:nil];
    
    [self checkNotification];
    
    
}

- (void)checkNotification
{
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(currentUserNotificationSettings)]){ // Check it's iOS 8 and above
        UIUserNotificationSettings *grantedSettings = [[UIApplication sharedApplication] currentUserNotificationSettings];
        
        if (grantedSettings.types == UIUserNotificationTypeNone) {
            
            [self showAlertView];
            alertView.tag = 9;
        }
       
    }
}

#pragma mark - custormize layout view
- (void)layoutView {
    [self.btn_Login setShadow];
    [self.btn_Login styleBorderMap];
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showAlertView {
    
    alertView = [[UIAlertView alloc] initWithTitle:@"Thông Báo" message:@"Chức năng Notification đã bị tắt vui lòng mở lại" delegate:self cancelButtonTitle:@"Có" otherButtonTitles:@"Không", nil];
    [alertView show];
}

#pragma mark - recive push notification
- (void)showReceivedMessage:(NSNotification *) notification {
    NSLog(@"show notification");
    
    data = notification.userInfo;
    
    @try {
        NSString *contract, *imageName, *title, *message, *category = @"";
        category = data[@"category"];
        contract = notification.userInfo[@"Contract"];
        
        if ([category isEqualToString:@"PTC_Return"] || [category isEqualToString:@"1800600"] || contract.length > 0) {
            imageName = @"notification_32";
            title    = data[@"aps"][@"alert"][@"title"];
            message  = data[@"aps"][@"alert"][@"body"];
            
        } else {
            imageName = @"icon_chat_512.png";
            title    = data[@"sendfrom"];
            message  = data[@"message"];
        }
        
        UIImage *image = [UIImage imageNamed:imageName];
        isNotificationViewShowed = [self showNotificationWithTitle:title message:message icon:image];
        
    } @catch (NSException *exception) {
        NSLog(@"Parse data push notify error: %@", exception.description);
        [CrashlyticsKit recordCustomExceptionName:@"LoginViewController - funtion (saveReceiveRemoteNotification)-Error handle Parse data push notify error" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
        
    }
   
}

- (BOOL)showNotificationWithTitle:(NSString *)title message:(NSString *)message icon:(UIImage *)icon {
    [HDNotificationView showNotificationViewWithImage:icon title:title message:message isAutoHide:NO onTouch:^{
        [self showAlertBox:@"Thông báo" message:@"Vui lòng đăng nhập để xem thông báo"];
        [HDNotificationView hideNotificationViewOnComplete:nil];
    }];
    return YES;
}

// Notify là tin nhắn thì push tới view Chat
- (UIViewController *)pushToChatView {
    MessageListViewController *mvc = [[MessageListViewController alloc] init];
    return mvc;
}

// Notify là chăm sóc khách hàng thì push tới Chăm sóc khách hàng View
- (UIViewController *)pushToCareCustomerDetailView {
    CustomerCareRecord *rc = [[CustomerCareRecord alloc] initWithData:data];
    DetailCareCustomerViewController *dvc = [[DetailCareCustomerViewController alloc] initWithData:rc];
    return dvc;
    
}

// Notify là Phiếu thi công trả về thì push tới Phiếu thi công trả về View
- (UIViewController *)pushToConstructionVoteReturnListView {
    ConstructionVoteReturnListViewController *cvc = [[ConstructionVoteReturnListViewController alloc] init];
    return cvc;
}

// Notify là Khách hàng tiềm năng từ tổng đài thì push tới View Thông tin Khách hàng tiềm năng.
- (UIViewController *)pushToPotentialCustomerDetailView {
    PotentialCustomerDetailForm *pvc = [[PotentialCustomerDetailForm alloc] init];
    pvc.ID = data[@"PotentialObjID"];
    pvc.potentialType = FromNotification;
    
    return pvc;
}

#pragma mark - button action
-(IBAction)btnExit_clicked:(id)sender{
    self.alertLogout = [[UIAlertView alloc]
                        initWithTitle:@"Thông Báo"
                        message:@"Bạn có muốn thoát chương trình không?"
                        delegate:self
                        cancelButtonTitle:@"Không"
                        otherButtonTitles:@"Có",nil];
    [self.alertLogout show];
    
}
- (IBAction)btn_Login_touchUpInside:(id)sender
{
    if (![Common_client isNetworkAvailable]) {
        [self showAlertMessage:@"Thông báo" errorCode:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    NSString *username = self.txt_UserName.text;
    NSString *password = self.txt_PassWord.text;
    username = [username stringByTrimmingCharactersInSet:
                [NSCharacterSet whitespaceCharacterSet]];
    if([username isEqualToString:@""]) {
        [self.txt_UserName becomeFirstResponder];
        [self showAlertMessage:@"Thông báo" errorCode:@"Chưa điền tài khoản"];
        [self hideMBProcess];
        return;
    }
    
    if([password isEqualToString:@""]){
        [self.txt_PassWord becomeFirstResponder];
        [self showAlertMessage:@"Thông báo" errorCode:@"Chưa điền mật khẩu"];
        [self hideMBProcess];
        return;
    }
    [self doLogin:username password:password];
}

-(IBAction)btnGuid_clicked:(id)sender
{
    GuidController *vc = [[GuidController alloc] initWithNibName:@"GuidController" bundle:nil];
    
    vc.delegate = (id)self;
    [self presentPopupViewController:vc animationType:1];
}

-(IBAction)btnInfo_clicked:(id)sender
{
    if(flagClick == NO){
        if(sender == self.btnInfo){
            InfoController *vc = [[InfoController alloc] initWithNibName:@"InfoController" bundle:nil];
            vc.delegate = (id)self;
            
            [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomTop];
            flagClick = YES;
        }else if(sender == self.btnGuid){
            GuidController *vc = [[GuidController alloc] initWithNibName:@"GuidController" bundle:nil];
            
            vc.delegate = (id)self;
            [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomTop];
            flagClick = YES;
        }
        
    }
    
}

#pragma mark - ViewControllerAction
- (void)loginSuccess {
    isLoginSuccessed = YES;
    // self.menuContainerViewController.enableMenu = YES;
    NSString *nib = @"MainPageViewController";
    if (IS_IPHONE4) {
        nib = @"MainPageViewController_IPhone4";
    }
    MainPageViewController *vc = [[MainPageViewController alloc]initWithNibName:nib bundle:nil];
    UINavigationController *nav = [[UINavigationController alloc]
                                   initWithRootViewController:vc];
    LeftMenuTableViewController *leftMenuViewController = [[LeftMenuTableViewController alloc] init];
    MFSideMenuContainerViewController *container = [MFSideMenuContainerViewController
                                                    containerWithCenterViewController:nav
                                                    leftMenuViewController:leftMenuViewController
                                                    rightMenuViewController:nil];
    // IF notification received and when login success:
    /// Category is "PTC_Return": push to ConstructionVoteReturnListViewController.
    /// Category is "1800600": push to PotentialCustomerDetailForm View.
    /// Other Category:
    //// IF contract code # null: push to Customer Care View.
    //// IF contract code = null: push to Chat View.
    
    if (isNotificationViewShowed) {
        isNotificationViewShowed = NO;
        [HDNotificationView hideNotificationViewOnComplete:nil];
        
        @try {
            UIViewController *vc = [self pushToView];
            [nav pushViewController:vc animated:YES];
            
        } @catch (NSException *exception) {
            NSLog(@"push notifiy error: %@", exception.description);
             [CrashlyticsKit recordCustomExceptionName:@"LoginViewController - funtion (saveReceiveRemoteNotification)-Error handle Parse data push notify error" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
        }
        

    }
    
    self.view.window.rootViewController = container;
    
    [self hideMBProcess];
}

- (UIViewController *)pushToView {
    NSString *category = data[@"category"];
    
    if ([category isEqualToString:@"PTC_Return"]) {
        
        return [self pushToConstructionVoteReturnListView];
        
    }
    
    if ([category isEqualToString:@"1800600"]) {
        
        return [self pushToPotentialCustomerDetailView];
        
    }
    
    NSString *contract = data[@"Contract"];
    
    if (contract.length > 0) {
        return [self pushToCareCustomerDetailView];
    }
    
    else {
        return [self pushToChatView];
    }
}

#pragma mark - Service
- (void)doLogin:(NSString *)username password:(NSString*)password {
    //  Thont5
    [self.txt_PassWord endEditing:YES];
    [self.txt_UserName endEditing:YES];
    ShareData *shared = [ShareData instance];
    NSString *openUDID = [OpenUDID value];
    
    
    [self showMBProcess];
    
    [shared.userProxy doLogin:username password:password completeHandler:^(id result, NSString *errorCode, NSString *message){
        
        if([errorCode isEqualToString:@"0"]) {
            UserRecord *user = result;
            
            user.userName = self.txt_UserName.text;
            user.userPass = password;
            user.userDeviceIMEI = openUDID;
            shared.currentUser = user;
            
            [self GetInfo:user.userName];
            
            //Gửi Crash...
            NSString *userCrash = [[ShareData instance] currentUser].userName ?: @"Not Found";
            [[Crashlytics sharedInstance] setUserName:userCrash];
            [[Crashlytics sharedInstance] setUserIdentifier:openUDID];
            [CrashlyticsKit setUserName:userCrash];
            [CrashlyticsKit setUserIdentifier:openUDID];
            
            
            //Save userName pass
            NSData *dtpass = [SiUtils encryptAESString:password withKey:ftooldef];
            NSData *dtuser = [SiUtils encryptAESString:username withKey:ftooldef];
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            [dict setValue:dtuser forKeyPath:@"u_a"];
            [dict setValue:dtpass forKeyPath:@"u_p"];
            [SiUtils saveUserDefaults:dict];
            
            [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
            
            return;
            
        }
        
        // show error
        [self showAlertMessage:@"Thông báo" errorCode:message];
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        [self showAlertMessage:@"Thông báo" errorCode:[NSString stringWithFormat:@"%@",[error localizedDescription]]];
        [self hideMBProcess];
        
    }];
    [self.txt_PassWord endEditing:NO];
    [self.txt_UserName endEditing:NO];
    
}


#pragma mark - Get version
-(void)getversion
{
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    
    [self showMBProcess];
    
    ShareData *shared = [ShareData instance];
    [shared.appProxy getVesion:^(NSArray *result, NSString *errorCode, NSString *message){
        
        if([errorCode isEqualToString:@"<null>"]){
            self.link  = [result objectAtIndex:1];
            
            if([self.link isEqual:[NSNull null]]){
                [self CheckIMEI];
                
            }else{
                [self showAlertGetVersion];
                [self hideMBProcess];
            }
        }
        else {
            [self showAlertMessage:@"Thông báo" errorCode:message];
            [self hideMBProcess];
        }
        [self.btn_Login setEnabled:YES];
        
    } errorHandler:^(NSError *error) {
        [self showAlertMessage:@"Thông báo " errorCode:[NSString stringWithFormat:@"%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
    
    
}

#pragma mark - Get version
-(void)CheckIMEI {
    
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        
        return;
    }
    
    ShareData *shared = [ShareData instance];
    [shared.appProxy registerIMEI:^(id result, NSString *errorCode, NSString *message) {
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            
            return ;
        }
        
        NSMutableArray *arr = result;
        NSString *IsActive = [arr objectAtIndex:0];
        if([IsActive isEqualToString:@"0"]){
            [self showAlertMessage:@"Thông báo" errorCode:@"IMEI chưa được kích hoạt"];
            [self updateImei];
            
        }else {
            NSString *UserID = [arr objectAtIndex:1];
            self.txt_UserName.text = UserID;
        }
        
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        [self showAlertMessage:@"Thông báo" errorCode:[NSString stringWithFormat:@"%@",[error localizedDescription]]];
        [self hideMBProcess];
        
    }];
    
}

#pragma mark - getlocation
-(void)GetInfo:(NSString *)username
{
    if (![Common_client isNetworkAvailable]) {
        [self showAlertMessage:@"Thông báo" errorCode:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    [shared.appProxy GetInfo:username completionHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            
            return;
        }
        
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            
            return ;
        }
        
        [self loginSuccess];
        
        
        
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}
-(void)updateImei {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    NSString  *imeiOld = [OpenUDID valueold];
    NSString  *imeiNew = [OpenUDID value];
    [shared.appProxy updateIMEI:imeiOld ImeiNew:imeiNew completionHandler:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            return ;
        }
        
        [self showAlertMessage:@"Thông báo" errorCode:[[result objectAtIndex:0] objectForKey:@"Result"]?:nil];
        
    } errorHandler:^(NSError *error) {
        
        [self showAlertMessage:@"Thông Báo" errorCode:[NSString stringWithFormat:@"%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}

#pragma mark - Load Bonus

- (void)showAlertGetVersion{
    
    self.alertViewGetVersion = [[UIAlertView alloc]initWithTitle:@"Update phiên bản mới"
                                                         message:@"Đã có phiên bản mới xin vui lòng update" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [self.alertViewGetVersion show];
}
-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(alertView == self.alertViewGetVersion)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:self.link]];
    }
    if(alertView == self.alertLogout){
        if(buttonIndex == 1){
            exit(0);
        }
    }
}

#pragma login user exist

- (void)doLoginIfUserIsExist{
    
    NSData *userdt = [SiUtils getUserDefaults:@"u_a"];
    NSData *passdt = [SiUtils getUserDefaults:@"u_p"];
    NSString *username = [SiUtils decryptAESData:userdt withKey:ftooldef];
    NSString *password = [SiUtils decryptAESData:passdt withKey:ftooldef];
    
    username = [username stringByTrimmingCharactersInSet:
                [NSCharacterSet whitespaceCharacterSet]];
    self.txt_UserName.text = username;
    if(![username isEqualToString:@""] && ![password isEqualToString:@""]){
        self.txt_UserName.text = username;
        self.txt_PassWord.text = password;
        
        [self btn_Login_touchUpInside:self.btn_Login];
    }
}
#pragma mark handlerError
- (void)handlerError:(NSError *)error {
    
    if([error.domain isEqualToString:HTTPDOMAIN]){
        [self showAlertMessage:@"Không thấy phản hồi từ server.(Service's busy)" errorCode:StringFormat(@"%ld",(long)error.code)];
        return;
    }
    
    [self showAlertMessage:@"Kết nối service không thành công. Bạn kiểm tra lại internet nhé." errorCode:@""];
}

#pragma mark Alert Function
- (void)showAlertMessage:(NSString *)mess errorCode:(NSString *)errorCode{
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:mess message:errorCode delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    alertView.tag = e_ShowMessage;
    [alertView show];
}

-(void)ShowAlertErrorSession{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:@"Đã hết phiên làm việc. Vui long đăng nhập lại để tiếp tục" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    [alert show];
    [self performSelector:@selector(dismiss:) withObject:alert afterDelay:3.0];
}

-(void)dismiss:(UIAlertView*)alert{
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}

#pragma mark - delegate
-(void)CancelPopupInfo{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
    flagClick = NO;
}

-(void)CancelPopupGuid{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
    flagClick = NO  ;
}

- (void)reCheckVersion {
    [self getversion];
}

-(void)LogOut{
    self.menuContainerViewController.menuState = MFSideMenuStateClosed;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:[NSBundle mainBundle]];
    if(IS_IPHONE4){
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone4" bundle:[NSBundle mainBundle]];
    }
    MFSideMenuContainerViewController *container = self.menuContainerViewController;
    
    NSString *firstViewControllerName = @"LoginViewController";
    
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:firstViewControllerName];
    [container setCenterViewController:navigationController];
    //container.enableMenu = NO;
    
}

#pragma mark - uiTextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    if (textField == self.txt_UserName) {
        textField.returnKeyType = UIReturnKeyNext;
        return;
    }
    if (textField == self.txt_PassWord) {
        textField.returnKeyType = UIReturnKeyDone;
        return;
    }
    
}

-(BOOL)textFieldShouldReturn:(UITextField*)textField {
    if (textField == self.txt_UserName) {
        [self.txt_PassWord becomeFirstResponder];
        return NO;
    }
    if (textField == self.txt_PassWord) {
        [self btn_Login_touchUpInside:self.btn_Login];
        return NO;
    }
    return NO; // We do not want UITextField to insert line-breaks.
}


#pragma mark - alert view delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 9 && buttonIndex == 0) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        
        NSLog(@"opened Setting enable GPS");
        
        return;
    }
}
@end
