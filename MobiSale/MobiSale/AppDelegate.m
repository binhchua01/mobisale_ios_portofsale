//
//  AppDelegate.m
//  FTool
//
//  Created by MAC on 11/4/14.
//  Copyright (c) 2014 FPT.RAD.FTool. All rights reserved.
//

#import "AppDelegate.h"
#import "../Helper/MFSideMenu/MFSideMenu.h"
#import "RightMenu.h"
#import "Common.h"
#import "IQKeyboardManager.h"
#import "ShareData.h"
#import <AudioToolbox/AudioToolbox.h>
#import "MAMPosAPIHelper.h"

@implementation AppDelegate {
    
     UIView *notifView ;
}

@synthesize registrationTokenAPS;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
//CraschAlitics

    [Fabric with:@[[Crashlytics class]]];
    
    [[Fabric sharedSDK] setDebug: YES];
    
    [Fabric with:@[CrashlyticsKit]];
    
//    custom keyboard
    
    ShareData *shared = [ShareData instance];
    shared.tokenAPNS = @"";
    
    [[IQKeyboardManager sharedManager] setToolbarManageBehaviour:IQAutoToolbarByPosition];
//    Gooogle Analytics
    GAI *ga = [GAI sharedInstance];
//   [ga trackerWithTrackingId:@"UA-65154926-1"];
    [ga trackerWithTrackingId:@"UA-62233259-3"];
//    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:(NSString *)kCFBundleVersionKey];
//    [ga.defaultTracker set:kGAIVersion value:version];
    ga.trackUncaughtExceptions =YES;
    ga.dispatchInterval = 30;
//    Push notification from google cloud message
    [self registerRemoteNotification:application didFinishLaunchingWithOptions:launchOptions];
    
//    [UIApplication sharedApplication]setStatusBarHidden:YES];
//    Google Map API
    //AIzaSyCLaOqEkrEe5u5MeWQnUvtFBDnSrokzMfQ --- Cũ
    //AIzaSyDWtAQbNew-H6ICXhfteSWKeWTo0ZrYoB8 --- Mới(Của A.Bình)
    [GMSServices provideAPIKey:@"AIzaSyCLaOqEkrEe5u5MeWQnUvtFBDnSrokzMfQ"];
    [self initSideMenuControl];
//    Delete title "Back" of back button on navigation item
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60) forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
    
    /*
 * Setup mPOS
 */
    NSLog(@"------setting mPOS");
    [self setupMPOSAPI];
    
    
    return YES;
}

// [START connect_gcm_service]
- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    [application setApplicationIconBadgeNumber:0];
    
    [self connectGCMService:application];
    
}
//LOCALNOTIFICATION
- (void)application:(UIApplication *)application didReceiveLocalNotification:(nonnull UILocalNotification *)notification {
    
    UIApplicationState state = [application applicationState];
    if (state == UIApplicationStateActive) {
        [self customViewBanner:notification.alertBody];
    }
    // set vibrate when received notification while device set silent
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    // set badge for app icon when received notification
    [application setApplicationIconBadgeNumber:application.applicationIconBadgeNumber + 1];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadData" object:self];
    //application.applicationIconBadgeNumber = 0;
    
}

//CUSTOMEVIEW LOCALNOTIFICATION
- (void)customViewBanner:(NSString *)notifMessage {
    
    [notifView removeFromSuperview];
    notifView = [[UIView alloc] initWithFrame:CGRectMake(0, -7, self.window.frame.size.width, 80)];
    [notifView setBackgroundColor:[UIColor grayColor]];
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 15, 60, 60)];
    imageView.image = [UIImage imageNamed:@"logo_icon.png"];
    
    UILabel *myLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 30, self.window.frame.size.width - 100, 30)];
    myLabel.font = [UIFont fontWithName:@"Helvetica" size:15.0];
    myLabel.text = notifMessage;
    [myLabel setTextColor:[UIColor whiteColor]];
    [myLabel setNumberOfLines:0];
    [notifView setAlpha:1];
    [notifView addSubview:imageView];
    [notifView addSubview:myLabel];
    [self.window addSubview:notifView];
    
    UITapGestureRecognizer *tapToDismissNotif = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                        action:@selector(dismissNotifFromScreen)];
    
    tapToDismissNotif.numberOfTapsRequired = 1;
    tapToDismissNotif.numberOfTouchesRequired = 1;
    [notifView addGestureRecognizer:tapToDismissNotif];
    
    [UIView animateWithDuration:1.0 delay:.1 usingSpringWithDamping:0.5 initialSpringVelocity:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        [notifView setFrame:CGRectMake(0, 0, self.window.frame.size.width, 70)];
        
    } completion:^(BOOL finished) {
        
        
    }];
    
   // [self performSelector:@selector(dismissNotifFromScreen) withObject:nil afterDelay:5.0];
}

- (void)dismissNotifFromScreen{
    
    [UIView animateWithDuration:1.0 delay:.1 usingSpringWithDamping:0.5 initialSpringVelocity:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
     
        [notifView setFrame:CGRectMake(0, -70, self.window.frame.size.width, 70)];
       
    } completion:^(BOOL finished) {
        
        
    }];
    
}



// [START disconnect_gcm_service]
- (void)applicationDidEnterBackground:(UIApplication *)application {
    [self disconnectGCMService:application];
}
// [END disconnect_gcm_service]

// [START receive_apns_token]
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // [END receive_apns_token]
    NSLog(@"My deviceToken: %@", deviceToken);
    ShareData *shared = [ShareData instance];

    NSString * token = [NSString stringWithFormat:@"%@", deviceToken];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    
    shared.tokenAPNS = token;
    
    //vutt11
    // [START get_gcm_reg_token] APNS but using GGMS
    [self getGCMRegistrationToken:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
    // [END get_gcm_reg_token]
}
//Vutt11
// [START receive_apns_token_error]
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    [self receiveAPNsTokenError:application didFailToRegisterForRemoteNotificationsWithError:error];
}

// [START ack_message_reception]
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    
    [self handleReceiveNotify:application userInfo:userInfo];
    [self ackMessageReception:application didReceiveRemoteNotification:userInfo];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))handler {
    [self handleReceiveNotify:application userInfo:userInfo];
    [self receiveMessage:application didReceiveRemoteNotification:userInfo fetchCompletionHandler:handler];
    
    // [END ack_message_reception]
}
// [END ack_message_reception]

- (void)applicationWillResignActive:(UIApplication *)application
{
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if (self.shouldRotate) {
        return UIInterfaceOrientationMaskAllButUpsideDown;
    }
        return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - handle URLs for mPOS API

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    return [[MAMPosAPIHelper sharedAPIHelper] handleCallBackURLFromPaymentApp:url];
}

#pragma mark - setup mPOS API
- (void)setupMPOSAPI {

    MAMPosAPI *mPosAPI = [MAMPosAPIHelper sharedAPIHelper].mPosAPI;
    [mPosAPI setCredentialKey:@"dbaae094f049c7eeb1494c8e442264b6"];
    [mPosAPI setCredentialPassword:@"97ec7ae1398fa1a90d2b0c1b0c012506"];
//    
//    [mPosAPI setX_SuccessURL:[NSURL URLWithString:@"MobiSale-X-Callback://x-success"]];
//    [mPosAPI setX_ErrorURL:[NSURL URLWithString:@"MobiSale-X-Callback://x-error"]];
//    [mPosAPI setX_CancelURL:[NSURL URLWithString:@"MobiSale-X-Callback://x-cancel"]];
    [mPosAPI setX_SuccessURL:[NSURL URLWithString:@"vn.tpb.mpos.third-party.app0://x-success"]];
    [mPosAPI setX_ErrorURL:[NSURL URLWithString:@"vn.tpb.mpos.third-party.app0://x-error"]];
    [mPosAPI setX_CancelURL:[NSURL URLWithString:@"vn.tpb.mpos.third-party.app0://x-cancel"]];
}

#pragma SideMenuControl
- (void)initSideMenuControl {
    
    UIStoryboard *storyboard ;
    if(IS_IPHONE5|| IS_IPAD){
       storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:[NSBundle mainBundle]];
    }
    if(IS_IPHONE4){
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone4" bundle:[NSBundle mainBundle]];
    }
    MFSideMenuContainerViewController *container = (MFSideMenuContainerViewController *)self.window.rootViewController;
    
    NSString *firstViewControllerName = @"LoginViewController";
    
    
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:firstViewControllerName];
    UIViewController *leftSideMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"LeftMenuTableViewController"];
     RightMenu *rightSideMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"RightMenu"];
    
    [container setCenterViewController:navigationController];
    [container setLeftMenuViewController:leftSideMenuViewController];
    [container setRightMenuViewController:rightSideMenuViewController];
    [container setRightMenuWidth:0];
    
    //layout sideMenuControl
    container.shadow.opacity = 0.1f;
    container.shadow.radius = 2.0f;
    container.shadow.color = [UIColor blackColor];
   // container.enableMenu = NO;
    
}

//VUTT11 (Handel Data PushNotification)
// Handle receive notify
- (void)handleReceiveNotify:(UIApplication *)application userInfo:(NSDictionary *)userInfo {
    // set vibrate when received notification while device set silent
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    // set badge for app icon when received notification
    [application setApplicationIconBadgeNumber:application.applicationIconBadgeNumber + 1];
    
    NSDictionary *data = userInfo[@"data"];
    
    NSString *category = [NSString stringWithFormat:@"%@",data[@"category"]];
   
    if (![category isEqualToString:@"PTC_Return"] && ![category isEqualToString:@"1800600"] ) {
        // save notification received
        [self saveReceiveRemoteNotification:userInfo];
    }
}

- (void)saveReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSString *contract = userInfo[@"Contract"];
    NSString *userName = [[ShareData instance].currentUser.userName lowercaseString];
    NSString *keyUserDefautls = @"";
    
    @try {
        if (contract == nil || contract.length <= 0) {
            // Save receive notification for notice
            NSString *sendFrom = userInfo[@"sendfrom"];
            keyUserDefautls = StringFormat(@"%@-message",userName);
            [NSUserDefaults saveMessages:userInfo withKey:keyUserDefautls userName:[sendFrom lowercaseString]];
        }
        else {
            // Save receive notification for chat
            keyUserDefautls = StringFormat(@"%@-notification",userName);
            [NSUserDefaults saveNotifications:userInfo withKey:keyUserDefautls];
        }
        
    } @catch (NSException *exception) {
        NSLog(@"%@", exception.description);
        
        [CrashlyticsKit recordCustomExceptionName:@"AppDelegate - funtion (saveReceiveRemoteNotification)" reason:[[ShareData instance].currentUser.userName lowercaseString]frameArray:@[]];
    }
}


@end
