//
//  LeftMenuTableViewController.h
//  FTool
//
//  Created by MAC on 11/4/14.
//  Copyright (c) 2014 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubMenuTableViewCell.h"
#import "BaseViewController.h"
#import "../Client/ShareData.h"
#import "InfoController.h"
#import "ChangePassViewController.h"

@interface LeftMenuTableViewController : UITableViewController<InfoDelegate,ChangePassDelegate>

- (void)layoutView;
@end
