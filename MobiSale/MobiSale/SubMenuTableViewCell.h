//
//  SubMenuTableViewCell.h
//  FTool
//
//  Created by MAC on 11/4/14.
//  Copyright (c) 2014 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubMenuTableViewCell : UITableViewCell

@property (nonatomic,retain) UILabel *lbl_SubMenu;
@property (nonatomic,retain) UIImageView *imv_Icon;

@end
