//
//  MainPageViewController.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 4/7/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "SBIViewController.h"
#import "IGLDropDownMenu.h"

@interface MainPageViewController : BaseViewController<SBIDelegate, UIActionSheetDelegate>

@property (nonatomic,retain) IBOutlet UILabel *lblUserName;
@property (nonatomic,retain) IBOutlet UIButton *btnSBIAble;
@property (nonatomic,retain) IBOutlet UIButton *btnSBIUnAble;
@property (nonatomic,retain) IBOutlet UIButton *btnCancelSale;
@property (nonatomic,retain) IBOutlet UIButton *btnCancelReport;
@property (nonatomic,retain) IBOutlet UIButton *btnCancelPrechecklist;
@property (nonatomic,retain) IBOutlet UIButton *btnCancelSupportDeployment;
@property (nonatomic,retain) IBOutlet UIButton *btnListPrecheckList;
@property (nonatomic,retain) IBOutlet UIButton *btnCreatePrecheckList;
// SALE MENU
@property (strong, nonatomic) IBOutlet UIButton *btnRegisterListAutoContract;
@property (nonatomic,retain) IBOutlet UIButton *btnRegisterList;
@property (nonatomic,retain) IBOutlet UIButton *btnRegisterForm;
@property (nonatomic,retain) IBOutlet UIButton *btnIPTV;
@property (weak, nonatomic) IBOutlet UIButton *btnSaleMoreService;

@property (nonatomic,retain) IBOutlet UIButton *btnPotentialCustomersList;
@property (strong, nonatomic) IBOutlet UIButton *btnCustomerCare;
@property (strong, nonatomic) IBOutlet UIButton *btnActivitiesOfCustomer;
@property (strong, nonatomic) IBOutlet UIButton *btnChat;

@property (nonatomic,retain) IBOutlet UIButton *btnReportSurvey;
@property (nonatomic,retain) IBOutlet UIButton *btnReportContact;
@property (nonatomic,retain) IBOutlet UIButton *btnReportPrechecklist;
@property (nonatomic,retain) IBOutlet UIButton *btnReportSalary;
@property (nonatomic,retain) IBOutlet UIButton *btnReportRegister;
@property (strong, nonatomic) IBOutlet UIButton *btnReportPotential;
@property (strong, nonatomic) IBOutlet UIButton *btnReportContractAuto;
@property (weak, nonatomic) IBOutlet UIButton *btnReportPayTV;
// Báo cáo Khách hàng rời mạng
@property (weak, nonatomic) IBOutlet UIButton *btnReportSuspendCustomer;

@property (strong, nonatomic) IBOutlet UIButton *btnPromotionPrograms;
@property (weak, nonatomic) IBOutlet UIButton *btnSupportDeployment;
@property (weak, nonatomic) IBOutlet UIButton *btnConstructionVoteReturn;

@property (nonatomic,retain) IBOutlet UIView *viewmenureport;
@property (nonatomic,retain) IBOutlet UIView *viewmenusale;
@property (nonatomic,retain) IBOutlet UIView *viewmenuprechecklist;
@property (strong, nonatomic) IBOutlet UIView *viewMenuSupportDeployment;

@property (nonatomic, strong) UILabel *textLabel;

@property (weak, nonatomic) IBOutlet UIButton *btnSBI_clicked;

-(IBAction)btnSale_clicked:(id)sender;
-(IBAction)btnInfo_clicked:(id)sender;
-(IBAction)btnCareCustomer_clicked:(id)sender;
-(IBAction)btnSupporDeployment_clicked:(id)sender;
-(IBAction)btnSBIAble_clicked:(id)sender;
-(IBAction)btnSBIUnAble_clicked:(id)sender;
-(IBAction)btnReport_clicked:(id)sender;
-(IBAction)btnCancelMenu_clicked:(id)sender;
-(IBAction)btnTouchMenu_clicked:(id)sender;




@end
