//
//  RightMenu.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 1/26/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "RightMenu.h"
#import "LeftMenuCell.h"
#import "MainMap.h"
#import "DetailRegisteredForm.h"

@implementation RightMenu

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithRed:0.00 green:0.18 blue:0.31 alpha:0.8];
    [self.tableView setSeparatorColor:[UIColor clearColor]];
    self.tableView.separatorColor = [UIColor clearColor];
    self.tableView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
   
}

#pragma layout view
//- (void)layoutView
//{
//
//    self.view.backgroundColor = [UIColor colorWithRed:0.00 green:0.18 blue:0.31 alpha:0.8];
//    [self.tableView setSeparatorColor:[UIColor clearColor]];
//    self.tableView.separatorColor = [UIColor clearColor];
//    self.tableView.frame = CGRectMake(0, 10, self.view.frame.size.width, self.view.frame.size.height);
//    self.view.frame = CGRectMake(0, 200, self.view.frame.size.width, self.view.frame.size.height);
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 0;
            break;
        case 1:
            return 3;
            break;
        default:
            break;
    }
    return 3;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:
                                 CGRectMake(0, 100, tableView.frame.size.width, 50)];
    sectionHeaderView.backgroundColor = [UIColor colorWithRed:0.00 green:0.18 blue:0.31 alpha:1.0];
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:
                            CGRectMake(5, 3, tableView.frame.size.width, 25.0)];
    
    headerLabel.backgroundColor = [UIColor clearColor];
    [headerLabel setFont:[UIFont fontWithName:@"" size:15.0]];
    headerLabel.textColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
    [sectionHeaderView addSubview:headerLabel];
    
    switch (section) {
        case 0:
            headerLabel.text = @"";
            sectionHeaderView.backgroundColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
            return sectionHeaderView;
            break;
        case 1:
            headerLabel.text = @"CÁC CHỨC NĂNG CHÍNH";
            return sectionHeaderView;
            break;
        default:
            break;
    }
    
    return sectionHeaderView;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return 35;
            break;
        case 1:
            return 30;
            break;
            
        default:
            break;
    }
    return 30;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LeftMenuCell"];
    static NSString *simpleTableIdentifier = @"LeftMenuCell";
    LeftMenuCell *cell = (LeftMenuCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"LeftMenuCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    if(indexPath.section == 1){
        switch (indexPath.row)
        {
            case 0:
                cell.lbl_Label.text = @"CẬP NHẬT";
                cell.ic_image.image = [UIImage imageNamed:@"ic_menu_list_registration.png"];
                break;
            case 1:
                cell.lbl_Label.text = @"BOOK PORT";
                cell.ic_image.image = [UIImage imageNamed:@"ic_menu_create_registration.png"];
                
                break;
            case 2:
                cell.lbl_Label.text = @"KHẢO SÁT";
                cell.ic_image.image = [UIImage imageNamed:@"ic_menu_list_prechecklist.png"];
                break;
            default:
                break;
        }
    }
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MainMap *vc ;
    NSString *flat = @"";
    if(indexPath.section == 1){
        switch (indexPath.row)
        {
            case 0:
                flat = @"0";
                break;
            case 1:
                flat = @"1";
                break;
            case 2:
                flat = @"2";
                break;
        }
    }
    vc =  [[MainMap alloc] initWithNibName:@"MainMap" bundle:nil];
    UINavigationController *nav = self.menuContainerViewController.centerViewController;
    [nav pushViewController:vc animated:NO];
    
    self.menuContainerViewController.centerViewController = nav;
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
}

@end
