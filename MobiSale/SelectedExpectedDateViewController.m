//
//  SelectedExpectedDateViewController.m
//  TicketMobile
//
//  Created by ISC-DanTT on 7/31/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import "SelectedExpectedDateViewController.h"
#import "ShareData.h"

@interface SelectedExpectedDateViewController () {
    UIDatePicker *datePicker;
    UIDatePicker *timePicker;
}

@end

@implementation SelectedExpectedDateViewController
@synthesize ticketId, type, createDate;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *temp;
    if ([self.type isEqualToString: @"1"]) {
        temp = @"CHỌN THỜI GIAN XẢY RA SỰ CỐ";
    }
    else if([self.type isEqualToString: @"2"]) {
        temp = @"CHỌN TG DỰ KIẾN HOÀN THÀNH";
    }
    self.lblTitle.text = temp;
    
    CGRect datePickerFrame = CGRectMake(0,0, self.view.frame.size.width, 200);
    datePicker = [[UIDatePicker alloc] initWithFrame:datePickerFrame];
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    CGRect timePickerFrame = CGRectMake(0,0, self.view.frame.size.width, 200);
    timePicker = [[UIDatePicker alloc] initWithFrame:timePickerFrame];
    timePicker.datePickerMode = UIDatePickerModeTime;
    
    
    [self limitDate];
    [self setSelectedDateInField];
    [self setSelectedTimeInField];
    [datePicker addTarget:self action:@selector(setSelectedDateInField) forControlEvents:UIControlEventValueChanged];
    
    [timePicker addTarget:self action:@selector(setSelectedTimeInField) forControlEvents:UIControlEventValueChanged];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) limitDate{
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:0];
    NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:self.createDate options:0];
    NSDate *minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    if ([self.type isEqualToString:@"1"]) {
        [datePicker setMaximumDate:maxDate];
        return;
    }
    if ([self.type isEqualToString:@"2"]) {
        [datePicker setMinimumDate:minDate];
        return;
    }
}

- (IBAction)clickBtnExpectedDate:(id)sender {
    [self alertDidShow];
    [self showDatePicker:datePicker];
    
}


- (IBAction)clickBtnExpectedTime:(id)sender {
    [self alertDidShow];
    [self showDatePicker:timePicker];
    
}

- (IBAction)clickBtnUpdate:(id)sender {
    self.isUpdateExpectedDate = YES;
    self.expectedDate = [NSString stringWithFormat:@"%@",self.chooseExpectedDate];
    if (self.delegate) {
        [self.delegate CancelPopup:[self setStringExpectedDate] expectedDateUpdate:self.expectedDate isUpdateExpectedDate:self.isUpdateExpectedDate];
    }
}

- (IBAction)clickBtnClose:(id)sender {
    self.isUpdateExpectedDate = NO;
    if (self.delegate) {
        [self.delegate CancelPopup:[self setStringExpectedDate] expectedDateUpdate:nil isUpdateExpectedDate:self.isUpdateExpectedDate];
    }
}

- (NSString *) setStringExpectedDate{
    NSString *strDate = self.btnExpectedDate.titleLabel.text;
   // NSString *strTime = self.btnExpectedTime.titleLabel.text;
    NSString * str = [NSString stringWithFormat:@"%@",strDate];
    return str;
}

-(void) showDatePicker: (UIDatePicker *) modeDatePicker{
    
    UIView *viewDatePicker = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width+30, 200)];
    [viewDatePicker setBackgroundColor:[UIColor clearColor]];
    
    modeDatePicker.hidden = NO;
    modeDatePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"Vietnamese"];
    
    [viewDatePicker addSubview:modeDatePicker];
  
    if(IS_OS_8_OR_LATER){ //if [[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"\n\n\n\n\n\n\n\n\n\n" preferredStyle:UIAlertControllerStyleActionSheet];
        
        [alertController.view addSubview:viewDatePicker];
        
        UIAlertAction *doneAction = [UIAlertAction actionWithTitle:@"Xong" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            [self alertDidHide];
            NSLog(@"OK action");
        }];
        [alertController addAction:doneAction];
        [self.view.window.rootViewController presentViewController:alertController animated:YES completion:nil];
        //[self presentViewController:alertController animated:YES completion:nil]; // error on iOS 9
    }
    else{
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"\n\n\n\n\n\n\n\n\n\n" delegate:self cancelButtonTitle:nil destructiveButtonTitle:@"Xong" otherButtonTitles:nil, nil];
        [actionSheet addSubview:viewDatePicker];
        //actionSheet.bounds = CGRectMake(0, 0, self.view.frame.size.width, 200);
        //[actionSheet showInView:self.view];
        [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
    }
}

// Methods to set selected date and time in related field

-(void)setSelectedDateInField {
    NSLog(@"date: %@",datePicker.date.description);
    
    //set Date formatter show on view
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    [self.btnExpectedDate setTitle:[dateFormatter stringFromDate:datePicker.date] forState:UIControlStateNormal];
    
    // set Date formatter for update to server
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    self.chooseExpectedDate = [dateFormatter stringFromDate:datePicker.date];
}

-(void)setSelectedTimeInField {
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat: @"HH:mm"];
    [self.btnExpectedTime setTitle:[timeFormatter stringFromDate:timePicker.date] forState:UIControlStateNormal];
}

// Action sheet Delegate In case of iOS 7
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    [self alertDidHide];
}

- (void)alertDidShow{
    //if(IS_IPHONE4){
        CGRect frm = self.view.frame;
        frm.origin.x = frm.origin.x ;
        frm.origin.y = frm.origin.y - 60;
        frm.size.width = frm.size.width;
        frm.size.height = frm.size.height;
        
        [UIView animateWithDuration:0.4
                         animations:^{
                             self.view.frame = frm;
                         }];
   // }
    
}

- (void)alertDidHide{
   // if(IS_IPHONE4){
        CGRect frm = self.view.frame;
        frm.origin.x = frm.origin.x ;
        frm.origin.y = frm.origin.y + 60;
        frm.size.width = frm.size.width;
        frm.size.height = frm.size.height;
        [UIView animateWithDuration:0.3
                         animations:^{
                             self.view.frame = frm;
                         }];
  //  }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
