//
//  AppProxy.h
//  FTool
//
//  Created by SinhNGN89 on 9/5/14.
//  Copyright (c) 2014 RAD. All rights reserved.
//

#import "BaseProxy.h"
#import "Common_client.h"

@interface AppProxy : BaseProxy

/*!Get get version. arg1:Complete handler*/
- (void)getVesion:(DidGetResultBlock)handler
         errorHandler:(ErrorBlock)errHandler;

- (void)registerIMEI: (DidGetResultBlock)handler
        errorHandler:(ErrorBlock)errHandler;

- (void)updateIMEI:(NSString *)imeiOld ImeiNew:(NSString *)imeinew completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)GetDistrictList:(NSString *)locationid
completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)GetWardList:(NSString *)locationid DistrictName:(NSString *)districtname completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)GetInfo:(NSString *)username completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)GetStreetOrCondominiumList:(NSString *)locationid DistrictName:(NSString *)districtname Ward:(NSString *)ward Type:(NSString *)type completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)GetListLocalType:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)GetListLocalTypePost:(NSMutableDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)GetListPromotion:(NSString *)localtype Complatehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)GetPromotionIPTVList:(NSString *)package withBoxOrder:(NSString *)boxOrder CustomerType:(NSString *)customerType Contract:(NSString *)contract RegCode:(NSString *)regCode Handler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)GetSBIList:(NSString *)username RegCode:(NSString *)regcode Status:(NSString *)status completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)GetSBIDetail:(NSString *)username RegCode:(NSString *)regcode Status:(NSString *)status completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)RequestToCancelSBI:(NSString *)username SBI:(NSString *)sbi completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
///ReportSBITotal
- (void)reportSBITotal:(NSString *)UserName Day:(NSString *)day Month:(NSString*)month Year:(NSString*)year Status:(NSString*)status Agent:(NSString*)agent AgentName:(NSString*)agentName PageNumber:(NSString*)pageNumer completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;



///ReportSBITotal
- (void)ReportSBIDetail:(NSString *)UserName Day:(NSString *)day Month:(NSString*)month Year:(NSString*)year ReportType:(NSString*)reportType Agent:(NSString*)agent AgentName:(NSString*)agentName PageNumber:(NSString*)pageNumer completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

//1.	API lấy thông tin cập nhật.
//GetSubTeamID1
-(void)GetSubTeamID:(NSString *)UserName RegCode:(NSString*)regCode completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

//UpdateDeployAppointment
-(void)UpdateDeployAppointment:(NSMutableDictionary *)dicIn completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

//GetDeployAppointmentList
-(void)GetDeployAppointmentList:(NSMutableDictionary *)dicIn completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

///CheckRegistration
-(void)CheckRegistration:(NSMutableDictionary *)dicIn completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

///CheckRegistration 2
-(void)CheckRegistration2:(NSMutableDictionary *)dicIn completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

// UpdateHasShowDuplicate
-(void)updateHasShowDuplicate:(NSMutableDictionary *)dicIn completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
//GetListDevice
- (void)GetListDevice:(NSString *)type Handler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
//GetDevicePromotion
- (void)GetDevicePromotion:(NSString *)type Handler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
//GetTotalDevice
- (void)GetTotalDevice:(NSMutableArray *)arrayListDevice CustomerStatus:(NSString *)customerStatus Handler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler ;
//nang luc kh
- (void)getAbility:(NSDictionary *)dict Handler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

//StevenNguyen ListDeviceOTT
- (void)getListDeviceOTT:(NSDictionary *)dict Handler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

//StevenNguyen ListPromotion
- (void)getGetDeviceOTTPromotion:(NSDictionary *)dict Handler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

@end
