//
//  BaseProxy.m
//  FTool
//
//  Created by MAC on 8/17/14.
//  Copyright (c) 2014 Tran Minh Dung. All rights reserved.
//

#import "BaseProxy.h"

@implementation BaseProxy

@synthesize parser, writer;

- (id)init
{
    self = [super init];
    if (self) {
        parser = [[SBJsonParser alloc] init];
        writer = [[SBJsonWriter alloc] init];
        writer.humanReadable = YES;
        writer.sortKeys = YES;
    }
    return self;
}

//#pragma mark - Error handler
//- (void)handleError:(NSError *)error
//{
//    self.errorHandler(error);
//}

#pragma mark support function
- (BOOL)isNumber:(NSString *)str {
    NSScanner *scanner = [NSScanner scannerWithString:str];
    return [scanner scanInteger:NULL] && [scanner isAtEnd];
}

@end
