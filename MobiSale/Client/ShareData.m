//
//  ShareData.m
//  FTool
//
//  Created by tmd on 8/28/14.
//  Copyright (c) 2014 RAD. All rights reserved.
//

#import "ShareData.h"

@implementation ShareData
@synthesize userProxy;
@synthesize appProxy;
@synthesize registrationFormProxy;
@synthesize prechecklistProxy;
@synthesize supportDeploymentProxy;
@synthesize reportlistdeploymentproxy;
@synthesize mapproxy;
@synthesize check,tapDiem, surveyDone;
@synthesize constructionVoteReturnProxy;
@synthesize saleMoreServiceProxy;
// add By DanTT 2015.11.16
@synthesize chatProxy;
@synthesize promotionProgramProxy;
@synthesize supportMobiSaleProxy;
@synthesize potentialCustomerProxy;
@synthesize customerCareProxy;
// Repost SBI
@synthesize repostSBIList,daySBI,monthSBI,yearSBI,contentSBI,statusSBI,typeSBI,RegCode;
@synthesize checkParam,amountTotal,toiuuhoa,hopthoa,pro,plus;
@synthesize imageData,imageSignature,imageView;
@synthesize tokenAPNS,idBox2;


static ShareData *_instance = nil;
- (NSMutableArray *)arrayPacKage
{
    if (!_arrayPacKage) {
        _arrayPacKage = [[NSMutableArray alloc] init];
    }
    return _arrayPacKage;
}
- (NSArray *)arrayPacKage1
{
    if (!_arrayPacKage1) {
        _arrayPacKage1 = [[NSMutableArray alloc] init];
    }
    return _arrayPacKage1;
}
- (NSArray *)arrayPacKage2
{
    if (!_arrayPacKage2) {
        _arrayPacKage2 = [[NSMutableArray alloc] init];
    }
    return _arrayPacKage2;
}
- (NSArray *)arrayPacKageParam
{
    if (!_arrayPacKageParam) {
        _arrayPacKageParam = [[NSArray alloc] init];
    }
    return _arrayPacKageParam;
}
- (NSArray *)arrayPacKageParam2
{
    if (!_arrayPacKageParam2) {
        _arrayPacKageParam2 = [[NSArray alloc] init];
    }
    return _arrayPacKageParam2;
}
- (NSArray *)arrayLocalNotification
{
    if (!_arrayLocalNotification) {
        _arrayLocalNotification = [[NSArray alloc] init];
    }
    return _arrayLocalNotification;
}
- (id)init
{
    self = [super init];
    if (self) {
        userProxy = [[UserProxy alloc] init];
        appProxy = [[AppProxy alloc] init];
        registrationFormProxy = [[RegistrationFormProxy alloc]init];
        prechecklistProxy = [[PrechecklistProxy alloc]init];
        supportDeploymentProxy = [[SupportDeploymentProxy alloc]init];
        reportlistdeploymentproxy = [[ReportListDeploymentProxy alloc]init];
        mapproxy = [[MapProxy alloc]init];
        // add by DanTT 2015.11.16
        chatProxy = [[ChatProxy alloc] init];
        promotionProgramProxy = [[PromotionProgramsProxy alloc] init];
        supportMobiSaleProxy = [[SupportMobiSaleProxy alloc] init];
        potentialCustomerProxy = [[PotentialCustomerProxy alloc] init];
        customerCareProxy = [[CustomerCareProxy alloc] init];
        // new API 23.06.2016
        constructionVoteReturnProxy = [[ConstructionVoteReturnProxy alloc] init];
        // new API 04.08.2016
        saleMoreServiceProxy = [[SaleMoreServiceProxy alloc] init];
        
    }
    
    return self;
}
+ (ShareData *)instance {
    @synchronized(self)
    {
        if(_instance == nil)
        {
            _instance= [ShareData new];
            
        }
    }
    return _instance;
}

@end
