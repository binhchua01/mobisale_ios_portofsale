//
//  AppProxy.m
//  FTool
//
//  Created by SinhNGN89 on 9/5/14.
//  Copyright (c) 2014 RAD. All rights reserved.
//

#import "AppProxy.h"
#import "../Helper/OpenUDID/OpenUDID.h"
#import "KeyValueModel.h"
#import "UserRecord.h"
#import "ShareData.h"
#import "PromotionModel.h"
#import "SBIRecord.h"
#import "FPTUtility.h"
#import "LoginViewController.h"
#import "ListDeviceModel.h"


#define mGetMenuByUser                          @"GetMenuByUser"
#define mGetVersion                             @"GetVersion"
#define mCheckIMEI                              @"CheckIMEI"
#define mGetDistrictList                        @"GetDistrictList"
#define mGetWardList                            @"GetWardList"
#define mGetStreetOrCondominiumList             @"GetStreetOrCondominiumList"
#define mGetInfo                                @"GetInfo"
#define mGetListLocalType                       @"GetListLocalType"
#define mGetListPromotion                       @"GetListPromotionPost"
#define mGetPromotionIPTVList                   @"GetPromotionIPTVList"
#define mGetSBIList                             @"GetSBIList"
#define mRequestToCancelSBI                     @"RequestToCancelSBI"
#define mGetListLocalTypePost                   @"GetListLocalTypePost"
#define mCheckRegistration                      @"CheckRegistration"
#define mCheckRegistration2                     @"CheckRegistration2"
#define mUpdateHasShowDuplicate                 @"UpdateHasShowDuplicate"
#define mGetListDevice                          @"GetListDevice"
#define mGetDevicePromotion                     @"GetDevicePromotion"
#define mGetTotalDevice                         @"GetTotalDevice"
#define mGetAbility4Days                        @"GetAbility4Days"
#define mGetListDeviceOTT                       @"GetListDeviceOTT"
#define mGetDeviceOTTPromotion                  @"GetDeviceOTTPromotion"

@implementation AppProxy

// CheckRegistration
-(void)CheckRegistration:(NSMutableDictionary *)dicIn completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler{
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI,mCheckRegistration)];
    
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    postString = [dicIn JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endCheckRegistration:result response:res completionHandler:handler errorHandler:errHandler];
    };
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

-(void)endCheckRegistration:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by Vutt11 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSString *data = [jsonDict objectForKey:@"CheckRegistrationResult"]?:nil;
    if(data == nil){
        handler(nil, @"-1", @"Không có dữ liệu");
        return;
    }
    NSArray * arr = [data JSONValue];
    NSString *message = [[arr objectAtIndex:0] objectForKey:@"Messages"];
    handler(arr, @"", message);
    
}

// CheckRegistration 2
-(void)CheckRegistration2:(NSMutableDictionary *)dicIn completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler{
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI,mCheckRegistration2)];
    
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    postString = [dicIn JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endCheckRegistration2:result response:res completionHandler:handler errorHandler:errHandler];
    };
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    [callOp start];
    
    
}

-(void)endCheckRegistration2:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    NSString *error = StringFormat(@"%@",[dict objectForKey:@"Error"]);
    
    if(![error isEqualToString:@"<null>"]){
        handler(nil, error, error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    
    if(arr.count <= 0){
        return;
    }
    
    handler(arr, @"", @"Check Registration 2 success");

}

// UpdateHasShowDuplicate
-(void)updateHasShowDuplicate:(NSMutableDictionary *)dicIn completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI,mUpdateHasShowDuplicate)];
    
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    postString = [dicIn JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endUpdateHasShowDuplicate:result response:res completionHandler:handler errorHandler:errHandler];
    };
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    [callOp start];
    
}

-(void)endUpdateHasShowDuplicate:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    NSString *error = StringFormat(@"%@",[dict objectForKey:@"Error"]);
    
    if(![error isEqualToString:@"<null>"]){
        handler(nil, error, error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    
    if(arr.count <= 0){
        return;
    }
    
    handler(arr, @"", @"Update Has Show Duplicate success");
    
    
}

//GetDeployAppointmentList
-(void)GetDeployAppointmentList:(NSMutableDictionary *)dicIn completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler{
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, @"GetDeployAppointmentList")];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    postString = [dicIn JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetDeployAppointmentList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    [callOp start];
    
    
}

-(void)endGetDeployAppointmentList:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *data = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    if(data == nil){
        return;
    }
    NSString *ErrorCode = StringFormat(@"%@",[data objectForKey:@"ErrorCode"]);
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, ErrorCode);
        return;
    }
    
    NSArray *arr = [data objectForKey:@"ListObject"]?:nil;
    
    handler((NSArray *)arr, ErrorCode, @"");
    
    
}

//UpdateDeployAppointment
-(void)UpdateDeployAppointment:(NSMutableDictionary *)dicIn completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler{
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, @"UpdateDeployAppointment")];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    postString = [dicIn JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self engUpdateDeployAppointment:result response:res completionHandler:handler errorHandler:errHandler];
    };
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
    
}

-(void)engUpdateDeployAppointment:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *data = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    if(data == nil){
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[data objectForKey:@"ErrorCode"]);
    
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, ErrorCode);
        return;
    }
    
    NSArray *arr = [data objectForKey:@"ListObject"]?:nil;
    
    handler((NSArray *)arr, ErrorCode, @"");
    
}

////GetSubTeamID1
-(void)GetSubTeamID:(NSString *)UserName RegCode:(NSString*)regCode completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler{
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, @"GetSubTeamID1")];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:UserName forKey:@"UserName"];
    [dict setObject:regCode forKey:@"RegCode"];
    //[dict setObject:@"SGK00023" forKey:@"RegCode"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self engGetSubTeamID:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}
-(void)engGetSubTeamID:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result]?:nil;
    NSDictionary *data = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    if(data == nil){
        return;
    }
    NSString *ErrorCode = StringFormat(@"%@",[data objectForKey:@"ErrorCode"]);
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, ErrorCode);
        return;
    }

    // NSArray *arr = [data objectForKey:@"ListObject"]?:nil;
    //  NSArray * values = [dic allValues];
    handler((NSArray *)data, ErrorCode, @"");
    
}

#pragma mark GetVersion
- (void)getVesion:(DidGetResultBlock)handler
     errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI,mGetVersion)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    NSString *deviceIMEI = [OpenUDID value];
    
    NSString *currentVersion = App_version;
    NSString *apptype = @"6";
    NSString *platform = @"2";
    
    [dict setObject:deviceIMEI forKey:@"DeviceIMEI"];
    [dict setObject:currentVersion forKey:@"CurrentVersion"];
    [dict setObject:apptype forKey:@"AppType"];
    [dict setObject:platform forKey:@"Platform"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
   [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
   
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetVersion:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
    
}
- (void)endGetVersion:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dictData = [jsonDict objectForKey:@"GetVersionMethodPostResult"];
    
    NSString *ErrorService = StringFormat(@"%@",[dictData objectForKey:@"ErrorService"]?:nil);
    
    if(![ErrorService isEqualToString:@"<null>"]){
        
        handler(nil, ErrorService, ErrorService);
        return;
    }
    
    NSString *ID = StringFormat(@"%@",[dictData objectForKey:@"ID"])?:nil;
    NSString *Link =[dictData objectForKey:@"Link"]?:nil;
    NSString *version =[dictData objectForKey:@"Version"]?:nil;
    
    NSMutableArray *resultArr =  [NSMutableArray array];
    [resultArr addObject:ID];
    
    [resultArr addObject:Link];
    [resultArr addObject:version];
    handler(resultArr, ErrorService, @"");
    
}
#pragma mark - ReportSBIDetail
- (void)ReportSBIDetail:(NSString *)UserName Day:(NSString *)day Month:(NSString*)month Year:(NSString*)year ReportType:(NSString*)reportType Agent:(NSString*)agent AgentName:(NSString*)agentName PageNumber:(NSString*)pageNumer completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler{
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, @"ReportSBIDetail")];
   
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:UserName forKey:@"UserName"];
    [dict setObject:day forKey:@"Day"];
    [dict setObject:month forKey:@"Month"];
    [dict setObject:year forKey:@"Year"];
    [dict setObject:reportType forKey:@"ReportType"];
    [dict setObject:agent forKey:@"Agent"];
    [dict setObject:agentName forKey:@"AgentName"];
    [dict setObject:pageNumer forKey:@"PageNumber"];
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endReportSBIDetail:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}
- (void)endReportSBIDetail:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *data = [jsonDict objectForKey:@"ResponseResult"]?:nil;

    if(data == nil){
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[data objectForKey:@"ErrorCode"]);
    
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, ErrorCode);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    // NSArray *arr = [data objectForKey:@"ListObject"]?:nil;
    //  NSArray * values = [dic allValues];
    handler((NSArray *)data, ErrorCode, @"");
    
}

#pragma mark - reportSBITotal
- (void)reportSBITotal:(NSString *)UserName Day:(NSString *)day Month:(NSString*)month Year:(NSString*)year Status:(NSString*)status Agent:(NSString*)agent AgentName:(NSString*)agentName PageNumber:(NSString*)pageNumer completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler{
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, @"ReportSBITotal")];
    //http://mobisales.fpt.net/MobiSaleService.svc/ReportSBITotal
    
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:UserName forKey:@"UserName"];
    [dict setObject:day forKey:@"Day"];
    [dict setObject:month forKey:@"Month"];
    [dict setObject:year forKey:@"Year"];
    [dict setObject:status forKey:@"Status"];
    [dict setObject:agent forKey:@"Agent"];
    [dict setObject:agentName forKey:@"AgentName"];
    [dict setObject:pageNumer forKey:@"PageNumber"];
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endreportSBITotal:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}
- (void)endreportSBITotal:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *data = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    if(data == nil){
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[data objectForKey:@"ErrorCode"]);
    
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, ErrorCode);
        return;
    }
    
    handler((NSArray *)data, ErrorCode, @"");
}

#pragma mark - updateIMEI
- (void)updateIMEI:(NSString *)imeiOld ImeiNew:(NSString *)imeinew completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, @"UpdateiOSDevice")];    
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:imeiOld forKey:@"DeviceIMEIOld"];
    [dict setObject:imeinew forKey:@"DeviceIMEI"];
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];

    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endUpdateImei:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}
- (void)endUpdateImei:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    

    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *data = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    
    if(data == nil){
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[data objectForKey:@"ErrorCode"]);
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, ErrorCode);
        return;
    }
    NSArray *arr = [data objectForKey:@"ListObject"]?:nil;
    //  NSArray * values = [dic allValues];
    handler((NSArray *)arr, ErrorCode, @"");
    
}

#pragma mark - registerIMEI
- (void)registerIMEI: (DidGetResultBlock)handler
        errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI,mCheckIMEI)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    NSString *deviceIMEI = [OpenUDID value];
    
    NSString *simIMEI = @"-1";
    NSString *iOSVersion  = [[UIDevice currentDevice] systemVersion]; //iOS_version;
    NSString *modelNumber = [Common_client getDeviceModel]; // @"IPhone";
   
    [dict setObject:deviceIMEI forKey:@"DeviceIMEI"];
    [dict setObject:simIMEI forKey:@"SimIMEI"];
    [dict setObject:iOSVersion forKey:@"AndroidVersion"];
    [dict setObject:modelNumber forKey:@"ModelNumber"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];


    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endRegisterIMEI:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endRegisterIMEI:(NSData *)result response:(NSURLResponse *)response
      completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }

    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *root = [jsonDict objectForKey:@"CheckIMEIMethodPostResult"]?:nil;
    
    if(root == nil) return;
    
    NSString *ErrorService = StringFormat(@"%@",[root objectForKey:@"ErrorService"]);
    
    if(![ErrorService isEqualToString:@"<null>"]){;
        handler(nil, ErrorService, ErrorService);
        return;
    }
    NSString *IsActive = StringFormat(@"%@",[root objectForKey:@"IsActive"]);
    NSString *UserID = [root objectForKey:@"UserID"]?:nil;
    
    NSMutableArray *resultArr =  [NSMutableArray array];
    [resultArr addObject:IsActive];
    [resultArr addObject:UserID];
    
    handler(resultArr, ErrorService, @"");
    
}

#pragma mark - Get Info User
- (void)GetInfo:(NSString *)username completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetInfo)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:username forKey:@"Username"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetInfo:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}
- (void)endGetInfo:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *data = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(data == nil){
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[data objectForKey:@"ErrorCode"]);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, ErrorCode);
        return;
    }
    
    NSArray *arr = [data objectForKey:@"ListObject"]?:nil;
    NSMutableArray *mArray = [NSMutableArray array];
    
    ShareData *shared = [ShareData instance];
    if(arr.count > 0){
        
        UserRecord *rc = [[UserRecord alloc] init];
        
        NSDictionary *dict = [arr objectAtIndex:0];
        
        shared.currentUser.LocationID     = StringFormat(@"%@",[dict objectForKey:@"LocationID"]);
        
        shared.currentUser.IsManager      = StringFormat(@"%@",[dict objectForKey:@"IsManager"]);
        
        shared.currentUser.LocationName   = StringFormat(@"%@",[dict objectForKey:@"LocationName"]);
        
        shared.currentUser.BrachCode      = StringFormat(@"%@",[dict objectForKey:@"BrachCode"]);
        
        shared.currentUser.ManagerName    = StringFormat(@"%@",[dict objectForKey:@"ManagerName"]);
        
        shared.currentUser.PortLocationID = StringFormat(@"%@",[dict objectForKey:@"PortLocationID"]);
        
        shared.currentUser.LocationParent = StringFormat(@"%@",[dict objectForKey:@"LocationParent"]);
        
        shared.currentUser.UseMPOS        = StringFormat(@"%@",[dict objectForKey:@"UseMPOS"]);
        
        //Use for AutoBookPort, by StevenNguyen
        
        //AutoBookPort
        NSString *strAutoBookPort                       = StringFormat(@"%@",[dict objectForKey:@"AutoBookPort"]);
        shared.currentUser.autoBookPort                 = [[dict objectForKey:@"AutoBookPort"] boolValue];
        
        //AutoBookPort_RetryConnect
        NSString *strAutoBookPort_RetryConnect          = StringFormat(@"%@",[dict objectForKey:@"AutoBookPort_RetryConnect"]);
        shared.currentUser.autoBookPort_RetryConnect    = [strAutoBookPort_RetryConnect integerValue];
        
        //AutoBookPort_Timeout
        NSString *strAutoBookPort_Timeout               = StringFormat(@"%@",[dict objectForKey:@"AutoBookPort_Timeout"]);
        shared.currentUser.autoBookPort_Timeout         = [strAutoBookPort_Timeout integerValue];
        
        //AutoBookPort_iR
        NSString *strAutoBookPort_iR                    = StringFormat(@"%@",[dict objectForKey:@"AutoBookPort_iR"]);
        shared.currentUser.autoBookPort_iR              = [strAutoBookPort_iR integerValue];
        
        //TypePortOfSale
        NSArray *arrTypePortOfSale                      = [dict objectForKey:@"TypePortOfSale"];
        NSMutableArray *lst = [[NSMutableArray alloc] init];
        for (int i = 0; i < arrTypePortOfSale.count; i++) {
            NSDictionary *dictTypePort = [arrTypePortOfSale objectAtIndex:i];
            NSString *strKey = StringFormat(@"%@",[dictTypePort objectForKey:@"Id"]);
            NSString *strValue = StringFormat(@"%@",[dictTypePort objectForKey:@"Name"]);
            KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:strKey description:strValue];
            [lst addObject:s1];
        }
        shared.currentUser.arrTypePortOfSale = arrTypePortOfSale;
        
        
        [mArray addObject:rc];
    }
    handler(nil, ErrorCode, @"");
    
}

#pragma mark - Get District
- (void)GetDistrictList:(NSString *)locationid completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetDistrictList)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:locationid forKey:@"LocationID"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetDistrictList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
    
}
- (void)endGetDistrictList:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSArray *arr = [jsonDict objectForKey:@"GetDistrictListMethodPostResult"]?:nil;
    
    if(arr.count <= 0){
        handler (nil, @"<null>", @"<null>");
        return;
    }
    
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    
    if(![ErrorService isEqualToString:@"<null>"]){
        handler(nil, ErrorService, ErrorService);
        return;
    }
 
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        NSString *FullName = StringFormat(@"%@",[d objectForKey:@"FullName"]);
        NSString *Name = StringFormat(@"%@",[d objectForKey:@"Name"]);
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:Name description:FullName];
        
        [mArray addObject:s];
    }
    handler(mArray, ErrorService, @"");
    
}

#pragma mark - Get Ward
- (void)GetWardList:(NSString *)locationid DistrictName:(NSString *)districtname completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler
{
    if(locationid == nil){
        locationid = @"0";
    }
    if(districtname == nil){
        districtname = @"";
    }
    BaseOperation * callOp = [[BaseOperation alloc] init];
    
   // districtname = [districtname stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetWardList)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:locationid forKey:@"LocationID"];
    [dict setObject:districtname forKey:@"District"];
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetWardList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
    
}
- (void)endGetWardList:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    
    NSArray *arr = [jsonDict objectForKey:@"GetWardListMethodPostResult"]?:nil;
    
    if(arr.count <= 0){
        handler(nil, @"<null>", @"<null>");
        return;
    }
    
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    
    if(![ErrorService isEqualToString:@"<null>"]){
        handler(nil, ErrorService, ErrorService);
        return;
    }
 
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        NSString *FullName = StringFormat(@"%@",[d objectForKey:@"Name"]);
        NSString *Name = StringFormat(@"%@",[d objectForKey:@"Name"]);
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:Name description:FullName];
        
        [mArray addObject:s];
    }
    handler(mArray, ErrorService, @"");
    
}

#pragma mark - Get StreetOrCondominiumList
- (void)GetStreetOrCondominiumList:(NSString *)locationid DistrictName:(NSString *)districtname Ward:(NSString *)ward Type:(NSString *)type completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler
{
    if(locationid == nil){
        locationid = @"";
    }
    if(districtname == nil){
        districtname = @"";
    }
    if(ward == nil){
        ward = @"";
    }
    if(type == nil){
        type = @"";
    }
    BaseOperation * callOp = [[BaseOperation alloc] init];
    //districtname =[districtname stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    //ward =[ward stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetStreetOrCondominiumList)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:locationid forKey:@"LocationID"];
    [dict setObject:districtname forKey:@"District"];
    [dict setObject:ward forKey:@"Ward"];
    [dict setObject:type forKey:@"Type"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetStreetOrCondominiumList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
    
}
- (void)endGetStreetOrCondominiumList:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSArray *arr = [jsonDict objectForKey:@"GetStreetOrCondominiumListMethodPostResult"]?:nil;
    if(arr.count <= 0){
        handler(nil, @"<null>", @"<null>");
        return;
    }
    
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    
    if(![ErrorService isEqualToString:@"<null>"]){
        handler(nil, ErrorService, ErrorService);
        return;
    }
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        NSString *FullName = StringFormat(@"%@",[d objectForKey:@"Name"]);
        NSString *Name = StringFormat(@"%@",[d objectForKey:@"Name"]);
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:Name description:FullName];
        
        [mArray addObject:s];
    }
    handler(mArray, ErrorService, @"");
    
}
//vutt {}
#pragma mark - Get List local type
- (void)GetListLocalType:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler
{
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetListLocalType)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    NSString *postString = @"";
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetListLocalType:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
    
}
- (void)endGetListLocalType:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    
    
    NSArray *arr = [jsonDict objectForKey:@"GetListLocalTypeResult"]?:nil;
       if(arr.count <= 0){
        return;
    }
    
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    
    if(![ErrorService isEqualToString:@"<null>"]){
        handler(nil, ErrorService, ErrorService);
        return;
    }
  
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        NSString *Name = StringFormat(@"%@",[d objectForKey:@"LocalTypeName"]);
        NSString *Id = StringFormat(@"%@",[d objectForKey:@"LocalTypeID"]);
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:Id description:Name];
        
        [mArray addObject:s];
    }
    handler(mArray, ErrorService, @"");
    
}


#pragma mark - Get List local type POST

- (void)GetListLocalTypePost:(NSMutableDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetListLocalTypePost)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetListLocalTypePost:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];

    
    
}

- (void)endGetListLocalTypePost:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
   }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    
    NSArray *arr = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(arr.count <= 0){
        handler(arr, @"", @"");
        return;
    }
    
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    if(![ErrorService isEqualToString:@"<null>"]){
        handler(nil, ErrorService, ErrorService);
        return;
    }
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        NSString *Name = StringFormat(@"%@",[d objectForKey:@"LocalTypeName"]);
        NSString *Id = StringFormat(@"%@",[d objectForKey:@"LocalTypeID"]);
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:Id description:Name];
        
        [mArray addObject:s];
    }
    handler(mArray, ErrorService, @"");
    
}

#pragma mark - Get List promotion
- (void)GetListPromotion:(NSString *)localtype Complatehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler
{
    if(localtype == nil){
        localtype = @"";
    }
    
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI,mGetListPromotion)];
    
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    // ShareData *shared = [ShareData instance];
    
    //Vutt11 confix Get to Post
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:[ShareData instance].currentUser.userName forKey:@"UserName"];
    [dict setObject:[ShareData instance].currentUser.LocationID forKey:@"LocationID"];
    [dict setObject:localtype forKey:@"LocalType"];
    [dict setObject:@"0" forKey:@"ServiceType"];
    
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetListPromotion:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
    
}
- (void)endGetListPromotion:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    
    //NSArray *arr = [jsonDict objectForKey:@"GetListPromotionResult"]?:nil;
    //vutt11 fix
    NSDictionary *data = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    if(data == nil){
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[data objectForKey:@"ErrorCode"]);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, ErrorCode);
        return;
    }
    
    //
    
    NSArray *arr = [data objectForKey:@"ListObject"]?:nil;
    if(arr.count <= 0){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    if(![ErrorService isEqualToString:@"<null>"]){
        handler(nil, ErrorService, ErrorService);
        return;
    }
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        NSString *PromotionName = StringFormat(@"%@",[d objectForKey:@"PromotionName"]);
        NSString *PromotionID = StringFormat(@"%@",[d objectForKey:@"PromotionID"]);
        NSString *RealPrepaid = StringFormat(@"%@",[d objectForKey:@"RealPrepaid"]);
        PromotionModel *pr = [[PromotionModel alloc]initWithName:PromotionID description:PromotionName Amount:RealPrepaid Type:@""];
        
        [mArray addObject:pr];
    }
    handler(mArray, ErrorService, @"");
    
}
//vutt11
#pragma mark - Get List promotion IPTV
- (void)GetPromotionIPTVList:(NSString *)package withBoxOrder:(NSString *)boxOrder CustomerType:(NSString *)customerType Contract:(NSString *)contract RegCode:(NSString *)regCode Handler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler
{
    if(package == nil){
        package = @"";
    }
    if(boxOrder == nil){
        boxOrder = @"";
    }
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetPromotionIPTVList)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:[ShareData instance].currentUser.userName forKey:@"Username"];
    [dict setObject:package forKey:@"Package"];
    [dict setObject:boxOrder forKey:@"BoxOrder"];
    [dict setObject:StringFormat(@"%d",[customerType intValue]) forKey:@"CustomerType"];
    [dict setObject:contract forKey:@"Contract"];
    [dict setObject:regCode forKey:@"RegCode"];
    
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetPromotionIPTVList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}
- (void)endGetPromotionIPTVList:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
   
  
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(dict == nil){
        handler(nil, @"1", @"1");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@", [dict objectForKey:@"ErrorCode"]);
    NSString *Error = [dict objectForKey:@"Error"];
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, Error);
        return;
    }
   
    NSArray *arr = [dict objectForKey:@"ListObject"]?:nil;
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        NSString *PromotionName = StringFormat(@"%@",[d objectForKey:@"PromotionName"]);
        NSString *PromotionID = StringFormat(@"%@",[d objectForKey:@"PromotionID"]);
        NSString *Amount = StringFormat(@"%@",[d objectForKey:@"Amount"]);
        NSString *Type = StringFormat(@"%@",[d objectForKey:@"Type"]);
        PromotionModel *pr = [[PromotionModel alloc]initWithName:PromotionID description:PromotionName Amount:Amount Type:Type];
        
        [mArray addObject:pr];
        
        
    }
    handler(mArray, ErrorCode, @"");
    
}

#pragma mark - Get SBI
- (void)GetSBIList:(NSString *)username RegCode:(NSString *)regcode Status:(NSString *)status completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler
{
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetSBIList)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:username forKey:@"UserName"];
    [dict setObject:regcode forKey:@"RegCode"];
    [dict setObject:status forKey:@"Status"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetSBIList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}
- (void)endGetSBIList:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *data = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    if(data == nil){
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[data objectForKey:@"ErrorCode"]);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, ErrorCode);
        return;
    }

    NSArray *arr = [data objectForKey:@"ListObject"]?:nil;
    NSDictionary *dic = nil;
    int SBIAble = 0;
    int SBIUnAble = 0;
    int SBIDelete = 0;
    int SBIUsed = 0;
    for (int i=0; i < arr.count; i++) {
        NSString *status = StringFormat(@"%@",[[arr objectAtIndex:i] objectForKey:@"Status"]?:nil);
        if([status isEqualToString:@"1"]){
            SBIAble ++;
        }else if([status isEqualToString:@"0"]) {
            SBIUnAble ++;
        }else if([status isEqualToString:@"2"]) {
            SBIUsed ++;
        }else if([status isEqualToString:@"3"]) {
            SBIDelete ++;
        }
        
    }
    
    dic = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",SBIAble],         @"SBIAble",[NSString stringWithFormat:@"%d",SBIUnAble],
           @"SBIUnAble",[NSString stringWithFormat:@"%d",SBIUsed],
           @"SBIUsed",[NSString stringWithFormat:@"%d",SBIDelete],
           @"SBIDelete", nil];
    //  NSArray * values = [dic allValues];
    handler((NSArray *)dic, ErrorCode, @"");
    
}

#pragma mark - Get SBI detail

- (void)GetSBIDetail:(NSString *)username RegCode:(NSString *)regcode Status:(NSString *)status completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler
{
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetSBIList)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:username forKey:@"UserName"];
    [dict setObject:regcode forKey:@"RegCode"];
    [dict setObject:status forKey:@"Status"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request

    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetSBIDetail:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}
- (void)endGetSBIDetail:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *data = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    if(data == nil){
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[data objectForKey:@"ErrorCode"]?:nil);
    
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, ErrorCode);
        return;
    }

    NSArray *arr = [data objectForKey:@"ListObject"]?:nil;
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *p in arr ){
        SBIRecord *record = [self ParseRecordLSBI:p];
        [mArray addObject:record];
    }
    handler(mArray, ErrorCode, @"");
    
}

-(SBIRecord *) ParseRecordLSBI:(NSDictionary *)dict{
    SBIRecord *rc = [[SBIRecord alloc]init];
    
    rc.ID = StringFormat(@"%@",[dict objectForKey:@"ID"]?:nil);
    rc.SBI = StringFormat(@"%@",[dict objectForKey:@"SBI"]?:nil);
    rc.Status = StringFormat(@"%@",[dict objectForKey:@"Status"]?:nil);
    rc.StatusName = StringFormat(@"%@",[dict objectForKey:@"StatusName"]?:nil);
    rc.Type = StringFormat(@"%@",[dict objectForKey:@"Type"]?:nil);
    switch ([rc.Type integerValue]) {
        case 1:
            rc.TypeName = @"ADSL";
            break;
        case 2:
            rc.TypeName = @"FTTH";
            break;
        case 8:
            rc.TypeName = @"IPTV";
        default:
            break;
    }
    return rc;
}

#pragma  mark - Thu hồi SBI
- (void)RequestToCancelSBI:(NSString *)username SBI:(NSString *)sbi completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler
{
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mRequestToCancelSBI)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:username forKey:@"UserName"];
    [dict setObject:sbi forKey:@"SBI"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endRequestToCancelSBI:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}
- (void)endRequestToCancelSBI:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *data = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(data == nil){
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[data objectForKey:@"ErrorCode"]?:nil);
    
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, ErrorCode);
        return;
    }
    
    NSArray *arr = [data objectForKey:@"ListObject"]?:nil;
    NSString *Result = StringFormat(@"%@", [[arr objectAtIndex:0] objectForKey:@"Result"]?:nil);
    NSString *ResultID = StringFormat(@"%@", [[arr objectAtIndex:0] objectForKey:@"ResultID"]?:nil);
    
    handler(nil, ResultID, Result);
    
}

#pragma mark - GetListDevice

- (void)GetListDevice:(NSString *)type Handler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation *callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI,mGetListDevice)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:[ShareData instance].currentUser.userName forKey:@"Username"];
    [dict setObject:type forKey:@"type"];
    
    NSString *postString  = [dict JSONRepresentation];
    
    [(NSMutableURLRequest *)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest *)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetListDevice:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
    
}

- (void)endGetListDevice:(NSData *)result response :(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(dict == nil){
        handler(nil, @"1", @"1");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@", [dict objectForKey:@"ErrorCode"]);
    NSString *Error = [dict objectForKey:@"Error"];
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, Error);
        return;
    }
    NSArray *arr = [dict objectForKey:@"ListObject"]?:nil;
    NSMutableArray *mArray = [NSMutableArray array];
    
    for (NSDictionary *d in arr) {
        NSString *DeviceName = StringFormat(@"%@",[d objectForKey:@"DeviceName"]);
        NSString *DeviceID   = StringFormat(@"%@",[d objectForKey:@"DeviceID"]);
        NSString *DevicePrice = StringFormat(@"%@",[d objectForKey:@"DevicePrice"]);
        ListDeviceModel *ld = [[ListDeviceModel alloc] initWithName:DeviceName DeViceID:DeviceID DevicePrice:DevicePrice];
        [mArray addObject:ld];
        
    }
    
    handler(mArray,ErrorCode,@"");

}
#pragma mark - GetDevicePromotion

- (void)GetDevicePromotion:(NSString *)deviceID Handler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation *callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI,mGetDevicePromotion)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:[ShareData instance].currentUser.userName forKey:@"Username"];
    [dict setObject:[ShareData instance].currentUser.LocationID forKey:@"LocationID"];
    [dict setObject:deviceID forKey:@"DeviceID"];
    
    
    NSString *postString  = [dict JSONRepresentation];
    
    [(NSMutableURLRequest *)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest *)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetDevicePromotion:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}

- (void)endGetDevicePromotion:(NSData *)result response :(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(dict == nil){
        handler(nil, @"1", @"1");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@", [dict objectForKey:@"ErrorCode"]);
    NSString *Error = [dict objectForKey:@"Error"];
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, Error);
        return;
    }
    NSArray *arr = [dict objectForKey:@"ListObject"]?:nil;
    
    //NSMutableArray *mArray = [NSMutableArray array];
    
    
    handler(arr,ErrorCode,@"");
}

- (void)GetTotalDevice:(NSMutableArray *)arrayListDevice CustomerStatus:(NSString *)customerStatus Handler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation *callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI,mGetTotalDevice)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:[ShareData instance].currentUser.userName forKey:@"username"];
    [dict setObject:[ShareData instance].currentUser.LocationID forKey:@"locationid"];
    [dict setObject:arrayListDevice forKey:@"listdevice"];
    [dict setObject:customerStatus forKey:@"customerStatus"];
     postString = [dict JSONRepresentation];
    
    NSString *strUTF8 = @"";
    NSMutableString *s = [NSMutableString stringWithString:postString];
    [s replaceOccurrencesOfString:@"\\" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    strUTF8 = [NSString stringWithString:s];
    
    [(NSMutableURLRequest *)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest *)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSString *base64 = [FPTUtility base64:strUTF8];
    NSString *md5 = [FPTUtility md5Encrypt:base64];

    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetTotalDevice:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endGetTotalDevice:(NSData *)result response :(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(dict == nil){
        handler(nil, @"1", @"1");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@", [dict objectForKey:@"ErrorCode"]);
    NSString *Error = [dict objectForKey:@"Error"];
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, Error);
        return;
    }
    NSArray *arr = [dict objectForKey:@"ListObject"]?:nil;
    
    handler(arr,ErrorCode,@"");
    
}
//NĂNG LỰC TRIỂN KHAI
- (void)getAbility:(NSDictionary *)dict Handler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation *callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI,mGetAbility4Days)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    NSString *postString = [dict JSONRepresentation];
    NSString *strUTF8 = @"";
    NSMutableString *s = [NSMutableString stringWithString:postString];
    [s replaceOccurrencesOfString:@"\\" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    strUTF8 = [NSString stringWithString:s];
    
    [(NSMutableURLRequest *)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest *)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:strUTF8];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result,NSURLResponse *res){
         [self endAbility:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
        
        
        
    };
    [callOp start];
    
}
- (void)endAbility:(NSData *)result response :(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
     NSDictionary *jsonDict = [self.parser objectWithData:result];
     NSDictionary *data = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    if(data == nil){
        return;
    }
    NSString *ErrorCode = StringFormat(@"%@",[data objectForKey:@"ErrorCode"]);
    
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, ErrorCode);
        return;
    }
    NSArray *arr = [data objectForKey:@"Object"]?:nil;
    
    handler((NSArray *)arr, ErrorCode, @"");
}


//MARK: StevenNguyen GetListDeviceOTT
- (void)getListDeviceOTT:(NSDictionary *)dict Handler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation *callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI,mGetListDeviceOTT)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = [dict JSONRepresentation];
    NSString *strUTF8 = @"";
    
    NSMutableString *s = [NSMutableString stringWithString:postString];
    [s replaceOccurrencesOfString:@"\\" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    strUTF8 = [NSString stringWithString:s];
    
    [(NSMutableURLRequest *)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest *)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:strUTF8];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result,NSURLResponse *res){
        [self endListDeviceOTT:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    [callOp start];
    
}

- (void)endListDeviceOTT:(NSData *)result response :(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *data = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    if(data == nil){
        return;
    }
    NSString *ErrorCode = StringFormat(@"%@",[data objectForKey:@"ErrorCode"]);
    
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, ErrorCode);
        return;
    }
    NSArray *arr = [data objectForKey:@"ListObject"]?:nil;
    
    handler((NSArray *)arr, ErrorCode, @"");
}

//MARK: StevenNguyen GetDeviceOTTPromotion
- (void)getGetDeviceOTTPromotion:(NSDictionary *)dict Handler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation *callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI,mGetDeviceOTTPromotion)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = [dict JSONRepresentation];
    NSString *strUTF8 = @"";
    
    NSMutableString *s = [NSMutableString stringWithString:postString];
    [s replaceOccurrencesOfString:@"\\" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    strUTF8 = [NSString stringWithString:s];
    
    [(NSMutableURLRequest *)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest *)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:strUTF8];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result,NSURLResponse *res){
        [self endGetDeviceOTTPromotion:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    [callOp start];
    
}

- (void)endGetDeviceOTTPromotion:(NSData *)result response :(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *data = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    if(data == nil){
        return;
    }
    NSString *ErrorCode = StringFormat(@"%@",[data objectForKey:@"ErrorCode"]);
    
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, ErrorCode);
        return;
    }
    NSArray *arr = [data objectForKey:@"ListObject"]?:nil;
    
    handler((NSArray *)arr, ErrorCode, @"");
}



@end
