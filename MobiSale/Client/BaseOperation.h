
#import <Foundation/Foundation.h>

#define domainService  @"mobisales.fpt.net"
// Thont5
//static NSString *const urlAPI = @"http://mobisales.fpt.net/MobiSaleService.svc";
//static NSString *const urlAPI = @"http://beta.wsmobisale.fpt.net/MobiSaleService.svc";
//https://betawsmobisale.fpt.vn
//static NSString *const urlAPI = @"https://wsmobisale.fpt.vn/MobiSaleService.svc"; //Beta

// test that
static NSString *const urlAPI = @"https://wsmobisale.fpt.vn/MobiSaleService.svc/";

// test beta bản version 1.4.2 để khong bị báo update
//static NSString *const urlAPI = @"https://betawsmobisale.fpt.vn/MobiSaleService.svc/";

// url upload and download Image
//@"http://mobisales.fpt.net/MobiSaleService.svc"
static NSString *const urlAPIIMAGE = @"http:beta.wsmobisale.fpt.net/MobiSaleService.svc/";

typedef void (^FinishBlock)(NSData *result, NSURLResponse *response);
typedef void (^ErrorBlock)(NSError *error);

@interface BaseOperation : NSObject

{
    NSString *uri;
    NSURLConnection *conn;
    NSURLRequest *request;
    NSURLResponse *response;
    NSMutableData *result;
    ErrorBlock errorHandler;
    FinishBlock completionHandler;
    
}

@property (nonatomic, retain) NSString *uri;
@property (nonatomic, retain) NSMutableData *result;
@property (nonatomic, retain) NSURLRequest *request;
@property (nonatomic, retain) NSURLResponse *response;
@property (nonatomic, retain) NSURLConnection *conn;
@property (nonatomic, copy) ErrorBlock errorHandler;
@property (nonatomic, copy) FinishBlock completionHandler;

- (void)start;
- (void)cancel;

@end
