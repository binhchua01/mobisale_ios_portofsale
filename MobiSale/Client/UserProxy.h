//
//  UserProxy.h
//  FTool
//
//  Created by tmd on 8/28/14.
//  Copyright (c) 2014 RAD. All rights reserved.
//

#import "BaseProxy.h"
#import "Common_client.h"

@interface UserProxy : BaseProxy

/*!Login with user name. arg1:UserName arg2:Password*/
- (void)doLogin:(NSString *)username password:(NSString *)password
completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)ResetPassword:(NSString *)username password:(NSString *)password NewPassword:(NSString *)newpassword completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;


@end
