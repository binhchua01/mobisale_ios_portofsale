//
//  BaseProxy.h
//  FTool
//
//  Created by MAC on 8/17/14.
//  Copyright (c) 2014 Tran Minh Dung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "../Helper/SBjsonHelper/SBJson.h"
#import "BaseOperation.h"

#define HTTPDOMAIN @"https://wsmobisale.fpt.vn/MobiSaleService.svc"

typedef void (^DidGetItemsBlock)(NSArray *result, NSString *errorCode, NSString *message);
typedef void (^DidGetResultBlock)(id result, NSString *errorCode, NSString *message);
typedef void (^DidActionBlock)(id obj);

@class SBJsonParser;
@class SBJsonWriter;

@interface BaseProxy : NSObject
{
    SBJsonParser *parser;
    SBJsonWriter *writer;
}

@property (nonatomic, retain) SBJsonParser *parser;
@property (nonatomic, retain) SBJsonWriter *writer;
//@property (nonatomic, copy) ErrorBlock errorHandler;

/*!Error Handel is method handle when is error*/
//- (void)handleError:(NSError *)error;
@end
