//
//  UserProxy.m
//  FMap
//
//  Created by tmd on 8/28/14.
//  Copyright (c) 2014 RAD.MOBISALE All rights reserved.
//

#import "UserProxy.h"
#import "ShareData.h"
#import "SiUtils.h"
#import "../Helper/OpenUDID/OpenUDID.h"
#import "FPTUtility.h"

#define methodLogin         @"Login"
#define methodRegisterIMEI  @"RegisterIMEI"
#define methodResetPassword @"ResetPassword"

@implementation UserProxy

#pragma mark LOGIN
- (void)doLogin:(NSString *)username password:(NSString *)password
completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodLogin)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *deviceKey = [OpenUDID value];
//    NSString *deviceKey = @"353722055431839";
    NSString *postString = @"";
    password = [SiUtils getSHA512:password];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:username forKey:@"Username"];
    [dict setObject:password forKey:@"Password"];
    [dict setObject:@"-1" forKey:@"SimKey"];
    [dict setObject:deviceKey forKey:@"DeviceKey"];

    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endLogin:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        
        errHandler(err);
    };
    
    [callOp start];


}

- (void)endLogin:(NSData *)result response:(NSURLResponse *)response
completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }

    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSDictionary *jsonHeader = [(NSHTTPURLResponse*) response allHeaderFields];
    NSString *headerData = [jsonHeader objectForKey:@"FTEL_MOBISALE_HEADER"] ?:nil;
    NSDictionary* dictHeaderData = [headerData JSONValue];
    NSArray *arrObject = [dictHeaderData objectForKey:@"ListObject"];
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *root = [jsonDict objectForKey:@"LoginResult"]?:nil;
    
    if(root == nil) return;
    NSString *errorCode = StringFormat(@"%@",[root objectForKey:@"ErrorCode"]);
    if(![errorCode isEqualToString:@"0"]){
        NSString *message = StringFormat(@"%@",[root objectForKey:@"Description"]); ;
        handler(nil, errorCode, message);
        return;
    }
    
    /*Login success*/
    
    UserRecord *rc = [[UserRecord alloc] init];
    rc.userName = StringFormat(@"%@",[root objectForKey:@"UserName"]);
    rc.userIsActive = StringFormat(@"%@",[root objectForKey:@"IsActive"]);
    NSString *str = StringFormat(@"%@", [root objectForKey:@"IsCreateContract"]);
    rc.IsCreateContract = [str intValue];
    // add by DanTT 2105.10.08
    
    rc.sessionID = StringFormat(@"%@",[[arrObject objectAtIndex:0] objectForKey:@"SessionID"]);
    rc.token = StringFormat(@"%@",[[arrObject objectAtIndex:0] objectForKey:@"Token"]);
    rc.userDeviceIMEI = StringFormat(@"%@",[[arrObject objectAtIndex:0] objectForKey:@"DeviceIMEI"]);
    
    handler(rc, errorCode, @"");
    
    
}

#pragma mark - Change password
- (void)ResetPassword:(NSString *)username password:(NSString *)password NewPassword:(NSString *)newpassword completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, methodResetPassword)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = @"";
    password = [SiUtils getSHA512:password];
    newpassword = [SiUtils getSHA512:newpassword];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:username forKey:@"UserName"];
    [dict setObject:password forKey:@"PasswordOld"];
    [dict setObject:newpassword forKey:@"PasswordNew"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];

    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endResetPassword:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endResetPassword:(NSData *)result response:(NSURLResponse *)response
completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSArray *root = [jsonDict objectForKey:@"ResetPasswordResult"]?:nil;
    
    if(root.count <= 0){
        handler(nil,@"1", @"");
        return;
    }
    
    NSString *errorCode = StringFormat(@"%@",[[root objectAtIndex:0] objectForKey:@"ResultID"]);
     NSString *message = StringFormat(@"%@",[[root objectAtIndex:0]  objectForKey:@"Result"]); ;
    if(![errorCode isEqualToString:@"0"]){
       
        handler(nil, errorCode, message);
        return;
    }
  
    handler(nil, errorCode, message);
}


@end
