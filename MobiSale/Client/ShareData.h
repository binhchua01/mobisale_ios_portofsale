//
//  ShareData.h
//  FMap
//
//  Created by tmd on 8/28/14.
//  Copyright (c) 2014 RAD. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserProxy.h"
#import "AppProxy.h"
#import <GoogleMaps/GoogleMaps.h>
#import "UserRecord.h"
#import "RegistrationFormProxy.h"
#import "PrechecklistProxy.h"
#import "SupportDeploymentProxy.h"
#import "ReportListDeploymentProxy.h"
#import "MapProxy.h"
#import "ChatProxy.h"
#import "SupportMobiSaleProxy.h"
#import "SupportMobiSaleRecord.h"
#import "PotentialCustomerProxy.h"
#import "PotentialCustomerDetailRecord.h"
#import "CustomerCareProxy.h"
#import "CustomerCareRecord.h"
#import "ToDoListRecord.h"
#import "PromotionProgramsProxy.h"
#import "ConstructionVoteReturnProxy.h"
#import "SaleMoreServiceProxy.h"

 double LatLocation;
 double LngLocation;

@interface ShareData : NSObject

@property(nonatomic,retain) NSArray *cookies;
@property(nonatomic,retain) UserProxy *userProxy;
@property(nonatomic,retain) AppProxy *appProxy;
@property(nonatomic,retain) UserRecord *currentUser;
@property(nonatomic,retain) RegistrationFormProxy *registrationFormProxy;
@property(nonatomic,retain) PrechecklistProxy *prechecklistProxy;
@property(nonatomic,retain) SupportDeploymentProxy *supportDeploymentProxy;
@property(nonatomic,retain) ReportListDeploymentProxy *reportlistdeploymentproxy;
@property(nonatomic,retain) MapProxy *mapproxy;
@property (nonatomic, strong) SaleMoreServiceProxy *saleMoreServiceProxy;
@property (nonatomic, retain) ConstructionVoteReturnProxy *constructionVoteReturnProxy;
// add by DanTT 2015.11.16
@property(nonatomic,retain) ChatProxy *chatProxy;

@property(nonatomic,retain) PromotionProgramsProxy *promotionProgramProxy;

@property(nonatomic,retain) SupportMobiSaleProxy *supportMobiSaleProxy;
@property(nonatomic,retain) SupportMobiSaleRecord *supportMobiSaleRecord;

@property(nonatomic,retain) PotentialCustomerProxy *potentialCustomerProxy;
@property(nonatomic,retain) PotentialCustomerDetailRecord *potentialRecord;
@property(nonatomic,retain) NSMutableArray *arrPotentialCutomers;

@property(nonatomic,retain) CustomerCareProxy *customerCareProxy;
@property(nonatomic,retain) CustomerCareRecord *customerCareRecord;
@property(nonatomic,retain) ToDoListRecord *toDoListRecord;
@property(nonatomic,retain) NSMutableArray *arrCustomerCare;
@property(nonatomic,retain) NSMutableArray *arrToDoList;
@property(nonatomic,retain) NSMutableArray *arrListBonus;
@property(nonatomic,retain) NSMutableArray *arrayPacKage;
@property(nonatomic,retain) NSArray *arrayPacKage1;
@property(nonatomic,retain) NSArray *arrayPacKage2;
@property(nonatomic,retain) NSArray *arrayPacKageParam;
@property(nonatomic,retain) NSArray *arrayPacKageParam2;
@property bool check;
@property bool tapDiem;
@property bool surveyDone;
@property BOOL ischeckUpdatehaveListDevice;

// Repost SBI
@property(nonatomic,retain) NSArray *repostSBIList;
@property(nonatomic,retain) NSString *daySBI;
@property(nonatomic,retain) NSString *monthSBI;
@property(nonatomic,retain) NSString *yearSBI;
@property(nonatomic,retain) NSString *typeSBI;
@property(nonatomic,retain) NSString *statusSBI;
@property(nonatomic,retain) NSString *contentSBI;
@property(nonatomic,retain) NSString *imageData;
@property(nonatomic,retain) NSString *imageSignature;
@property(nonatomic,retain) NSString *tokenAPNS;
@property BOOL checkParam;

@property(nonatomic,retain) NSString *RegCode;
//@property(nonatomic,retain) NSString *contentDetailsUser;
//BÁO CÁO PHÁT TRIỂN THUÊ BAO.
// vutt11
@property (nonatomic) NSInteger checkMonthAdd;
@property (nonatomic) NSInteger pro;
@property (nonatomic) NSInteger plus;
@property (nonatomic) NSInteger toiuuhoa;
@property (nonatomic) NSInteger hopthoa;
//OTT
@property (assign, nonatomic) NSInteger amountTotal;
@property (nonatomic, retain) UIImage *imageView;
//PromotionIPTVBox2
@property (nonatomic,retain) NSString *idBox2;
//ArrayLocalSave
@property (retain,nonatomic)NSMutableArray *arrayStyleLocalCell;
// array localNotification
@property (retain,nonatomic) NSArray *arrayLocalNotification;
+ (ShareData *)instance;

@end
