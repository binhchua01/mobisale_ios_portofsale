//
//  Common_client.h
//  MobiPay
//
//  Created by VANDN on 2/14/14.
//  Copyright (c) 2014 VANDN. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface Common_client : NSObject

//============ DEFAULT FRAME (create by thont, 03.04.2015) ========
+ (CGRect)defaultFrame:(UIView *)view withDeltaY:(float) deltaY;

//=========== FORMAT NUMBER ===============
+ (NSString *) formatNumber:(NSNumber *)number;

//========== CHECK TEXT FIELD IS EMPTY (create by thont, 03.04.2015) ============
+ (BOOL)isEmpty:(UITextField *)textField;

//========== CREATE PADDING FOR UITEXTFIELD (create by thont, 03.04.2015) ============
+ (UILabel *)paddingText;

//========== CHECK INPUT FILTER MIN MAX DATE (create by thont, 03.04.2015) ============
+ (BOOL)inputFilterMinMaxDate:(NSString *)inputVal;

//========== SHOW ALERT (create by thont, 03.04.2015) ============
//===SHOW ALERT VIEW FUNCTIONS====
//===SHOW ALERT VIEW FUNCTIONS====
+ (void)showAlertMessage:(NSString*)message;
+ (void)showConfirmAlert:(NSString*)message title:(NSString*)title;

//===DATETIME HELPER FUNCTIONS====
+ (NSDate *)getCurrentGMTTime;
+ (NSString *)getDateTimeByFormatTimeDay:(NSDate *) startDay;
+ (NSString *)getDateTimeByFormatDayTime:(NSDate *) startDay;
+ (NSString *)getDateTimeByFormatDay:(NSDate *) startDay;
+ (NSString *)getDateTimeByFormatTime:(NSDate *) startDay;
//============== CHECK NETWORK AVAILABILITY =================
+(BOOL)isNetworkAvailable;

//============ MD5 Encrypt ======================
+ (NSString *) md5Encrypt:(NSString *) input;

//======== SET HEADER REQUEST, PARSE HEADER RESPONSE API (create by DanTT, 12.10.2015) =======
+(void)setHeaderRequestAPI: (NSMutableURLRequest*) request;
+(NSString*)parseJsonHeaderResponseAPI:(NSHTTPURLResponse*) response;

//
+ (NSString *)getDeviceModel;

@end
