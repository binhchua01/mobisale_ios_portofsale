//
//  ActivityViewCell.h
//  MobiSale
//
//  Created by ISC-DanTT on 12/7/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblActivityName;
@property (strong, nonatomic) IBOutlet UILabel *lblActivityTypeName;
@property (strong, nonatomic) IBOutlet UILabel *lblActivityBy;
@property (strong, nonatomic) IBOutlet UILabel *lblContent;
@property (strong, nonatomic) IBOutlet UILabel *lblActivityCode;
@property (strong, nonatomic) IBOutlet UILabel *lblRemark;

@property (strong, nonatomic) IBOutlet UILabel *lblDate;
@end
