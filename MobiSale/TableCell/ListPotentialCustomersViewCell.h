//
//  ListPotentialCustomersViewCell.h
//  MobiSale
//
//  Created by ISC-DanTT on 11/17/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PotentialCustomerFormRecord.h"
#import "ListPotentialCustomersViewController.h"

@protocol ListPotentialCustomersViewCellDelegate <NSObject>

-(void)didSelectedAlarm:(NSString *)ID andSupporter:(NSString *)supporter;

@end

@interface ListPotentialCustomersViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblCustomerName;
@property (strong, nonatomic) IBOutlet UILabel *lblCustomerPhoneNumber;
@property (strong, nonatomic) IBOutlet UILabel *lblCustomerEmail;
@property (strong, nonatomic) IBOutlet UILabel *lblCreateDate;

@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblNote;
@property (strong, nonatomic) IBOutlet UILabel *lblSTT;
@property (weak, nonatomic) IBOutlet UILabel *lblAcceptStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblRemainder;

@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (strong, nonatomic) PotentialCustomerFormRecord *cellModel;
@property (assign, nonatomic) NSInteger cellIndex;
@property (assign, nonatomic) ListPotentialType source;
@property (weak, nonatomic) IBOutlet UIView *viewAlarm;
@property (weak, nonatomic) IBOutlet UILabel *lbltotalSchedule;

@property (nonatomic, weak)id<ListPotentialCustomersViewCellDelegate>delegate;
@property (weak, nonatomic) IBOutlet UIImageView *imageAlarm;
@property (weak, nonatomic) IBOutlet UIView *viewSTT;

@property (weak, nonatomic) IBOutlet UIButton *btnSchedule;


@end
