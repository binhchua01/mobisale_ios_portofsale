//
//  ActivityOfSaleViewCell.h
//  MobiSale
//
//  Created by ISC-DanTT on 12/9/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityOfSaleViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblFullname;
@property (strong, nonatomic) IBOutlet UILabel *lblActivityTypeName;
@property (strong, nonatomic) IBOutlet UILabel *lblActivityName;
@property (strong, nonatomic) IBOutlet UILabel *lblActivityBy;
@property (strong, nonatomic) IBOutlet UILabel *lblContent;
@property (strong, nonatomic) IBOutlet UILabel *lblActivityCode;
@property (strong, nonatomic) IBOutlet UILabel *lblRemark;
@property (strong, nonatomic) IBOutlet UILabel *lblAssignDate;

@property (strong, nonatomic) IBOutlet UILabel *lblActivityDate;

@end
