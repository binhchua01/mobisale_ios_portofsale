//
//  SBIDetailCell.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 4/9/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SBIDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTypeName;
@property (weak, nonatomic) IBOutlet UILabel *lblID;

@end
