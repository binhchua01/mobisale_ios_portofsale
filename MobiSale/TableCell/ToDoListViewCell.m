//
//  ToDoListViewCell.m
//  MobiSale
//
//  Created by ISC-DanTT on 11/30/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ToDoListViewCell.h"
#import "Common.h"
#import "UIView+FPTCustom.h"
#import "UIButton+FPTCustom.h"
#import "UILabel+FPTCustom.h"
#import "Common_client.h"
#import "ShareData.h"
#import <MBProgressHUD/MBProgressHUD.h>

@implementation ToDoListViewCell {
    UIAlertView *alertCall;
    UIAlertView *alertSendEmail;
    NSString *title;
    MBProgressHUD *HUD;

}

- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    self.lblCountdownTime.text = @"";
    [self.saleInfoView styleBorderMap];
    [self.confirmInfoButton styleButtonUpdate];
    [self.confirmInfoButton setShadow];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)layoutSubViews {
    [super layoutSubviews];
    [self stopTimer];
    
}
- (void)drawRect:(CGRect)rect{
    
    //  Phần nội dung trong mỗi Item kết quả KS sẽ trả về 1 trong 2 dạng: Json hoặc HTML. Do đó sẽ thử parse json trước, nếu parse thành công thì sẽ lấy đối tượng parse được để hiển thị.
    if (_cellModel.statusDescRecord != nil) {
        //  ♣	Ca khảo sát = time  + “ – Mã KS: “ + code.
        self.surveySessionLabel.text    = StringFormat(@"%@ - Mã KS: %@:", _cellModel.statusDescRecord.time, _cellModel.statusDescRecord.code);
        //  ♣	Đánh giá của KH: csat + “ (CSAT = ” + point + ảnh (add sẵn trong app 5 tấm ảnh tương ứng 5 điểm từ 1 -> 5).
        self.customerCommentLabel.text  = StringFormat(@"%@ (CSAT = %@)", _cellModel.statusDescRecord.csat, _cellModel.statusDescRecord.point);
        //  ♣   ảnh (add sẵn trong app 5 tấm ảnh tương ứng 5 điểm từ 1 -> 5)
        self.customerCommentImageView.image = [self setCustomerCommentImage:_cellModel.statusDescRecord.point];
        //  ♣	Ghi chú của NVKS: note
        self.surveyStaffNotedLabel.text = _cellModel.statusDescRecord.note;
        
        [self.surveyStaffNotedLabel autoFixFrame];
        [self.customerCommentLabel  autoFixFrame];
        [self.surveyStaffNotedLabel autoFixFrame];
        
        [self.lblObjStatusDesc setHidden:YES];
        
        [self showStatusDescriptionView:YES];
        
        //  Ngược lại, sẽ hiển thị dạng HTML
    } else {
        [self showStatusDescriptionView:NO];
        [self.lblObjStatusDesc setHTML:_cellModel.objStatusDesc];
        
    }
    
    [self.lblDivisionName autoFixFrame];
    [self.lblObjStatusName autoFixFrame];
    [self.lblContact autoFixFrame];
    [self.lblObjStatusDesc autoFixFrame];
    
    [self.activityIndicator startAnimating];
    //Creating a Circular Profile Image
    self.lblLevelName.layer.cornerRadius = self.lblLevelName.frame.size.width/2;
    self.lblLevelName.clipsToBounds = YES;

    //Adding Border
    //self.lblLevelName.layer.borderWidth = 1.0f;
    //self.lblLevelName.layer.borderColor = [UIColor grayColor].CGColor;
    
    if (self.btnEmail.titleLabel.text.length <= 0) {
        [self.btnEmail setEnabled:NO];
    }
    if (self.btnPhoneNumber.titleLabel.text.length <= 0) {
        [self.btnPhoneNumber setEnabled:NO];
    }
    
    self.processTime = self.lblProcessTime.text;
    self.lblProcessTime.text = [Common formatTimeInMinutes:[self.processTime intValue]];
    self.startDate = [self convertStringToDate:self.lblStartDate.text];
    NSLog(@"start date: %@",[self.startDate description]);
    NSLog(@"start date text: %@",self.lblStartDate.text);
    NSLog(@"time process: %@",self.lblProcessTime.text);
    [self startTimer];
}

- (void) setCellModel:(ToDoListRecord *)cellModel {
    _cellModel = cellModel;
    [self.btnPhoneNumber setTitle:cellModel.phoneNumber forState:UIControlStateNormal];
    [self.btnEmail setTitle:cellModel.email forState:UIControlStateNormal];
    self.lblDivisionName.text = cellModel.divisionName;
    self.lblObjStatusName.text = cellModel.objStatusName;
    self.lblContact.text = cellModel.fullName;
    self.statusNameLabel.text = cellModel.statusName;
    self.lblStartDate.text = cellModel.startDate;
    self.lblLevelName.text = cellModel.levelName;
    self.lblProcessTime.text = cellModel.processTime;
    
    [self.lblLevelName setBackgroundColor:[self setLabelBackgroundColorWithLevel:cellModel.levelID]];
    
    // Nếu DivisionID = 82 thì hiển thị thêm thông tin Sale: account Sale, họ tên, SĐT và email.
    if ([cellModel.divisionID isEqualToString:@"82"]) {
        self.saleNameLabel.text = cellModel.saleName;
        self.saleFullNameLabel.text = cellModel.saleFullName;
        [self.salePhoneButton setTitle:cellModel.salePhone forState:UIControlStateNormal];
        [self.saleEmailButton setTitle:cellModel.saleEmail forState:UIControlStateNormal];
        [self showSaleInfoView:YES];
        
    } else {
        [self showSaleInfoView:NO];
    }
    
}

- (UIColor *)setLabelBackgroundColorWithLevel:(NSString *)level {
    if ([level isEqualToString:@"1"]) {
        return [UIColor greenColor];
    }
    if ([level isEqualToString:@"2"]) {
        return [UIColor orangeColor];
    }
    if ([level isEqualToString:@"3"]) {
        return [UIColor redColor];
    }
    return [UIColor clearColor];
}

- (UIImage *)setCustomerCommentImage:(NSString *)point {
    if ([point isEqual:@"1"]) {
        return[UIImage imageNamed:@"Point_01"];
    }
    
    if ([point isEqual:@"2"]) {
        return[UIImage imageNamed:@"Point_02"];
    }
    
    if ([point isEqual:@"3"]) {
        return[UIImage imageNamed:@"Point_03"];
    }
    
    if ([point isEqual:@"4"]) {
        return[UIImage imageNamed:@"Point_04"];
    }
    
    if ([point isEqual:@"5"]) {
        return[UIImage imageNamed:@"Point_05"];
    }
    
    return nil;
}

- (void)showSaleInfoView:(BOOL)status {
    [self.saleInfoView setHidden:!status];
    if (!status) {
        self.saleInfoViewHeightLC.constant = 0;
        return;
    }
    
    self.saleInfoViewHeightLC.constant = 105;
}

- (void)showStatusDescriptionView:(BOOL)status {
    [self.statusDescriptionView setHidden:!status];
    if (!status) {
        self.statusDescriptionViewHeightLC.constant = 0;
        return;
    }
    self.statusDescriptionLabelHeightLC.constant = 0;
    self.statusDescriptionViewHeightLC.constant = 20 + self.surveySessionLabel.frame.size.height +  self.surveyStaffNotedLabel.frame.size.height + self.customerCommentLabel.frame.size.height ;
}

- (IBAction)confirmInfoButtonPressed:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Bạn có muốn xác nhận đã nhận thông tin?" delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
    actionSheet.tag = 1;
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}

- (IBAction)btnPhoneNumber_Clicked:(id)sender {
    if (sender == self.btnPhoneNumber) {
        title = self.btnPhoneNumber.titleLabel.text;
        
    } else if (sender == self.salePhoneButton) {
        title = self.salePhoneButton.titleLabel.text;
    }
    if (title.length <= 0) {
        return;
    }
    alertCall = [[UIAlertView alloc] initWithTitle:title message:nil delegate:self cancelButtonTitle:@"Gọi" otherButtonTitles:@"Đóng", nil];
    [alertCall show];
}

- (IBAction)btnEmail_Clicked:(id)sender {
    if (sender == self.btnEmail) {
        title = self.btnEmail.titleLabel.text;
        
    } else if (sender == self.saleEmailButton) {
        title = self.saleEmailButton.titleLabel.text;
    }
    
    if (title.length <= 0) {
        return;
    }
    alertSendEmail = [[UIAlertView alloc] initWithTitle:title message:nil delegate:self cancelButtonTitle:@"Gửi email" otherButtonTitles:@"Đóng", nil];
    [alertSendEmail show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView == alertCall) {
        switch (buttonIndex) {
            case 0:{
                [Common callPhone:title];
            }
                break;
            case 1:
                break;
            default:
                break;
        }
    }
    if (alertView == alertSendEmail) {
        switch (buttonIndex) {
            case 0:{
                if (self.delegate) {
                    [self.delegate sendEmailToAddress:title];
                }
            }
                break;
            case 1:
                break;
            default:
                break;
        }
    }
}

#pragma mark - UIActionSheetDelegate methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag == 1 && buttonIndex == 0) {
//        if (self.delegate) {
//            [self.delegate confirmNotificationWithData:self.cellModel];
//        }
        [self confirmNotificationWithData:_cellModel];
    }
}

- (void)updateTimer {
    // Create date from the elapsed time
    NSString *stringDate = [self convertDateToString:[NSDate date]];
    NSLog(@"current date string: %@",stringDate);
    NSDate *currentDate = [self convertStringToDate:stringDate];
    NSLog(@"current date: %@",[currentDate description]);

    NSTimeInterval timeInterval = [currentDate timeIntervalSinceDate:self.startDate];
    NSLog(@"time interval %f",timeInterval);
    //processTime seconds count down
    NSTimeInterval timeIntervalCountDown = [self.processTime intValue]*60 - timeInterval;
    NSLog(@"time interval countdown: %f",timeIntervalCountDown);
    self.lblCountdownTime.text = [Common formatTimeInSeconds:timeIntervalCountDown];
    [self.activityIndicator stopAnimating];
    [self.activityIndicator setHidesWhenStopped:YES];
}

- (void)stopTimer {
    [self.stopWatchTimer invalidate];
    self.stopWatchTimer = nil;
    [self updateTimer];
}

- (void)startTimer {
    if (self.stopWatchTimer) {
        [self.stopWatchTimer invalidate];
        self.stopWatchTimer = nil;
    }
    //self.startDate = [NSDate date];
    // Create the stop watch timer that fires every 1s
    self.stopWatchTimer = [NSTimer scheduledTimerWithTimeInterval:1.0/1.0
                                                           target:self
                                                         selector:@selector(updateTimer)
                                                         userInfo:nil
                                                          repeats:YES];
}

- (NSDate *)convertStringToDate:(NSString *)dateStringInput{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // this is imporant - we set our input date format to match our input string
    // if format doesn't match you'll get nil from your string, so be careful
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"GMT"];
    [dateFormatter setTimeZone:timeZone];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:dateStringInput];
    return dateFromString;
}

- (NSString *)convertDateToString:(NSDate*)dateInput{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"GMT+7"];
    [dateFormatter setTimeZone:timeZone];
    NSString *stringDate = [dateFormatter stringFromDate:dateInput];
    return stringDate;
}


#pragma mark - Comfirm Received Info
- (void)confirmNotificationWithData:(ToDoListRecord *)data {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:data.statusDescRecord.confirmCode ?:@"" forKey:@"Code"];
    [dict setObject:data.saleName forKey:@"Name"];
    [dict setObject:@"" forKey:@"Note"];
    
    [self showMBProcess];
    
    ShareData *shared = [ShareData instance];
    [shared.customerCareProxy confirmNotification:dict completeHander:^(id result, NSString *errorCode, NSString *message) {
        if ([errorCode isEqual:@"1"]) {
            // Cập nhật thành công
            [self.confirmInfoButton setEnabled:NO];
        }
        [self showAlertViewWithTitle:@"Thông báo" andMessage:message];
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        [self showAlertViewWithTitle:@"Lỗi" andMessage:[error localizedDescription]];
        [self hideMBProcess];
        
    }];
}

- (void)showAlertViewWithTitle:(NSString *)string andMessage:(NSString *)message {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:string message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertView show];
}

#pragma mark process bar
- (void)showMBProcess {
    HUD = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].keyWindow  animated:YES];
    HUD.delegate = (id<MBProgressHUDDelegate>)self;
    HUD.labelText = @"Loading";
    [HUD show:YES];
}

- (void)hideMBProcess {
    [HUD hide:YES];
}


@end
