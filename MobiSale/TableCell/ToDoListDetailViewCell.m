//
//  ToDoListDetailViewCell.m
//  MobiSale
//
//  Created by ISC-DanTT on 12/1/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ToDoListDetailViewCell.h"

@implementation ToDoListDetailViewCell

- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    self.txtDescription.editable = NO;
    self.txtDescription.layer.borderColor= [UIColor colorWithRed:215.0/255.0 green:215.0/255.0 blue:215.0/255.0 alpha:1].CGColor;
    self.txtDescription.layer.borderWidth = 0.7f;
    self.txtDescription.layer.cornerRadius = 6.0f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
