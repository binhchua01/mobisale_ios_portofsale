//
//  ListContactPrechecklistCell.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 1/26/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListContactPrechecklistCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblContact;
@property (strong, nonatomic) IBOutlet UILabel *lblFullName;
@property (strong, nonatomic) IBOutlet UILabel *lblAdress;
@property (strong, nonatomic) IBOutlet UILabel *lblSTT;
@property (strong, nonatomic) IBOutlet UIView *lblView;

@end
