//
//  ReportSurveyDetailCell.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 4/12/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportSurveyDetailCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblAfterTotalCab;
@property (weak, nonatomic) IBOutlet UILabel *lblBalance;
@property (weak, nonatomic) IBOutlet UILabel *lblContract;
@property (weak, nonatomic) IBOutlet UILabel *lblFirstTotalCab;
@property (weak, nonatomic) IBOutlet UILabel *lblFullName;
@property (weak, nonatomic) IBOutlet UILabel *lblRegCode;
@property (weak, nonatomic) IBOutlet UILabel *lblRowNumber;

@end
