//
//  ReportSurveyCell.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 4/11/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ReportSurveyCell.h"

@implementation ReportSurveyCell

- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
