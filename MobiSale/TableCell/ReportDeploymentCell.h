//
//  ReportDeploymentCell.h
//  MobiSale
//
//  Created by HIEUPC on 2/3/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportDeploymentCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblServiceName;
@property (strong, nonatomic) IBOutlet UILabel *lblServiceNumber;
@property (strong, nonatomic) IBOutlet UIView *lblview;

@end
