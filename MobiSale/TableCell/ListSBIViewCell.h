//
//  ListSBIViewCell.h
//  MobiSale
//
//  Created by Tan Tho Nguyen on 7/27/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListSBIViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *bl_number;

@property (weak, nonatomic) IBOutlet UILabel *lb_sbiNumber;
@property (weak, nonatomic) IBOutlet UILabel *lb_status;
@property (weak, nonatomic) IBOutlet UILabel *lb_date;

@property (weak, nonatomic) IBOutlet UILabel *lb_sgkNumber;
@property (weak, nonatomic) IBOutlet UILabel *lb_HDNumber;
@property (weak, nonatomic) IBOutlet UILabel *lb_SBIType;

@end
