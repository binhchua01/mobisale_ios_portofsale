//
//  ReportRegisterCell.h
//  MobiSale
//
//  Created by HIEUPC on 4/13/15.
//  Copyright (c) 2015 FPT.RAD.MOBISALE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportRegisterCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblSaleName;
@property (weak, nonatomic) IBOutlet UILabel *lblIncomplete;
@property (weak, nonatomic) IBOutlet UILabel *lblUnIncomplete;
@property (weak, nonatomic) IBOutlet UILabel *lblTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblRowNumber;

@end
