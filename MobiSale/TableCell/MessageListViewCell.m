//
//  MessageListViewCell.m
//  MobiSale
//
//  Created by ISC-DanTT on 12/3/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "MessageListViewCell.h"

@implementation MessageListViewCell

- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    //Creating a Circular Profile Image
    self.imageAvatar.layer.cornerRadius = self.imageAvatar.frame.size.width/2;
    self.imageAvatar.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
