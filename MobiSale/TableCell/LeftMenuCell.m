//
//  LeftMenuCell.m
//  MobiSales
//
//  Created by Nguyen Tan Tho on 1/21/15.
//  Copyright (c) 2015 Aryan Ghassemi. All rights reserved.
//

#import "LeftMenuCell.h"

@implementation LeftMenuCell

@synthesize lbl_Label;
@synthesize ic_image;

- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
