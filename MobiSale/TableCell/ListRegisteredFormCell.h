//
//  ListRegisteredFormCell.h
//  MobiSales
//
//  Created by Nguyen Tan Tho on 1/23/15.
//  Copyright (c) 2015 Aryan Ghassemi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListRegisteredFormCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblContract;
@property (strong, nonatomic) IBOutlet UILabel *lblId;
@property (strong, nonatomic) IBOutlet UILabel *lblFullName;
@property (strong, nonatomic) IBOutlet UILabel *lblAdress;
@property (strong, nonatomic) IBOutlet UILabel *lblTypeSerivce;
@property (strong, nonatomic) IBOutlet UILabel *lblStatusPort;
@property (strong, nonatomic) IBOutlet UILabel *lblIsBillling;
@property (strong, nonatomic) IBOutlet UILabel *lblisInVest;
@property (strong, nonatomic) IBOutlet UILabel *lblStatusDeposit;
@property (strong, nonatomic) IBOutlet UILabel *lblSTT;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *lblIDConstraintTopSpace;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *lblSTTConstraintTopSpace;

@end
