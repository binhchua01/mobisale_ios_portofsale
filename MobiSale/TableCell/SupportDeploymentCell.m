//
//  SupportDeploymentCell.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 1/24/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "SupportDeploymentCell.h"

@implementation SupportDeploymentCell

- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
    self.lblDeploymentAppointmentDate.text = @"";
    self.lblContact.text = @"";
    self.lblId.text = @"";
    self.lblCreateDate.text = @"";
    self.lblPhone.text = @"";
    self.lblDeploymentDate.text = @"";
    self.lblAssign.text = @"";
    self.lblFinishDate.text = @"";
    self.lblInventory.text = @"";
    self.lblSTT.text = @"";
    self.lblDeploymentAppointmentDate.text = @"";
    self.lblFullName.text = @"";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
