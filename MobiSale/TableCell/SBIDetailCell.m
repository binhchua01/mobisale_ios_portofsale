//
//  SBIDetailCell.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 4/9/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "SBIDetailCell.h"

@implementation SBIDetailCell



- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
