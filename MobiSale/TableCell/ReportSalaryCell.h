//
//  ReportSalaryCell.h
//  MobiSale
//
//  Created by HIEUPC on 4/11/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportSalaryCell : UITableViewCell

@property (strong, nonatomic)IBOutlet UILabel *lblName;
@property (strong, nonatomic)IBOutlet UILabel *lblFullName;
@property (strong, nonatomic)IBOutlet UILabel *lblBranch;
@property (strong, nonatomic)IBOutlet UILabel *lblDept;
@property (strong, nonatomic)IBOutlet UILabel *lblDateSalary;
@property (strong, nonatomic)IBOutlet UILabel *lblBasicSalary;
@property (strong, nonatomic)IBOutlet UILabel *lblMonthCommission;
@property (strong, nonatomic)IBOutlet UILabel *lblPrepaidNew;
@property (strong, nonatomic)IBOutlet UILabel *lblIncomeSalary;
@property (strong, nonatomic)IBOutlet UILabel *lblTPlusTotal;
@property (strong, nonatomic)IBOutlet UILabel *lblTMinTotal;
@property (strong, nonatomic)IBOutlet UILabel *lblBoxTPlusTotal;
@property (strong, nonatomic)IBOutlet UILabel *lblBoxTMinTotal;
@property (strong, nonatomic)IBOutlet UILabel *lblNetSalary;
@property (strong, nonatomic)IBOutlet UILabel *lblTotalPage;
//vutt11
@property (weak, nonatomic) IBOutlet UIWebView *webViewContent;


@end
