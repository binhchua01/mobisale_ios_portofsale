//
//  PrechecklistCell.h
//  MobiSale
//
//  Created by HIEUPC on 1/25/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrechecklistCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblContact;
@property (strong, nonatomic) IBOutlet UILabel *lblFullName;
@property (strong, nonatomic) IBOutlet UILabel *lblCreateDate;
@property (strong, nonatomic) IBOutlet UILabel *lblSTT;
@property (strong, nonatomic) IBOutlet UILabel *lblNote;
@property (strong, nonatomic) IBOutlet UIView *lblView;
@property (strong, nonatomic) IBOutlet UILabel *lblDateProcess;
@property (strong, nonatomic) IBOutlet UILabel *lblPerSon;

@property (strong, nonatomic) IBOutlet UITextView *tvContent;


@end
