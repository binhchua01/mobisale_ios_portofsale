//
//  ListReportContractAutoDetailViewCell.h
//  MobiSale
//
//  Created by ISC-DanTT on 3/21/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListReportContractAutoDetailViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblOrderNumber;

@property (strong, nonatomic) IBOutlet UILabel *lblContract;

@property (strong, nonatomic) IBOutlet UILabel *lblRegCode;

@property (strong, nonatomic) IBOutlet UILabel *lblFullName;

@property (strong, nonatomic) IBOutlet UILabel *lblPhoneNumber;

@property (strong, nonatomic) IBOutlet UILabel *lblStartDate;

@property (strong, nonatomic) IBOutlet UILabel *lblAddress;

@property (strong, nonatomic) IBOutlet UILabel *lblStatus;

@end
