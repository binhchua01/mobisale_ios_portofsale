//
//  ToDoListDetailViewCell.h
//  MobiSale
//
//  Created by ISC-DanTT on 12/1/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ToDoListDetailViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblCareType;
@property (strong, nonatomic) IBOutlet UILabel *lblDateProcess;
@property (strong, nonatomic) IBOutlet UILabel *lblStatus;
@property (strong, nonatomic) IBOutlet UITextView *txtDescription;

@end
