//
//  ReportPrecheckListCell.h
//  MobiSale
//
//  Created by HIEUPC on 4/13/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportPrecheckListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblClose;
@property (weak, nonatomic) IBOutlet UILabel *lblInprocess;
@property (weak, nonatomic) IBOutlet UILabel *lblRowNumber;

@end
