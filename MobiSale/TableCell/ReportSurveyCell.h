//
//  ReportSurveyCell.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 4/11/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportSurveyCell : UITableViewCell

@property (strong, nonatomic)IBOutlet UILabel *lblCabPlus;
@property (strong, nonatomic)IBOutlet UILabel *lblCabSub;
@property (strong, nonatomic)IBOutlet UILabel *lblObjectCabPlus;
@property (strong, nonatomic)IBOutlet UILabel *lblObjectCabSub;
@property (strong, nonatomic)IBOutlet UILabel *lblSaleName;
@property (strong, nonatomic)IBOutlet UILabel *lblTotalObject;
@property (strong, nonatomic)IBOutlet UILabel *lblRowNumber;

@end
