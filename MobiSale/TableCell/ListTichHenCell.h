//
//  ListTichHenCell.h
//  MobiSale
//
//  Created by Tan Tho Nguyen on 7/27/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListTichHenCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbl_NhanVien;
@property (weak, nonatomic) IBOutlet UILabel *lbl_NgayTao;
@property (weak, nonatomic) IBOutlet UILabel *lbl_NgayHen;


@property (weak, nonatomic) IBOutlet UILabel *lbl_DoiTac;
@property (weak, nonatomic) IBOutlet UILabel *lbl_PhieuDangKy;
@property (weak, nonatomic) IBOutlet UILabel *lbl_PhieuThiCong;

@property (weak, nonatomic) IBOutlet UILabel *lbl_ThoiGianThiCong;
@property (weak, nonatomic) IBOutlet UILabel *lbl_toCon;
@property (weak, nonatomic) IBOutlet UILabel *lbl_SoHoDong;
@property (weak, nonatomic) IBOutlet UILabel *lbl_STT;

@end
