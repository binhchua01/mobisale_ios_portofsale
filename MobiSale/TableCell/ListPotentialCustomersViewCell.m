//
//  ListPotentialCustomersViewCell.m
//  MobiSale
//
//  Created by ISC-DanTT on 11/17/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ListPotentialCustomersViewCell.h"


@implementation ListPotentialCustomersViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self animationAlarm];
    
    self.viewSTT.layer.cornerRadius = 15;
    self.viewSTT.layer.masksToBounds = TRUE;
    
}

- (void)animationAlarm{
    
    CABasicAnimation* anim = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    [anim setToValue:[NSNumber numberWithFloat:0.0f]];
    [anim setFromValue:[NSNumber numberWithDouble:M_PI/3]]; // rotation angle
    [anim setDuration:0.15];
    [anim setRepeatCount:NSUIntegerMax];
    [anim setAutoreverses:YES];
    [[self.imageAlarm layer] addAnimation:anim forKey:@"iconShake"];
    
}
- (void) drawRect:(CGRect)rect{
    [self.lblCustomerName setNumberOfLines:0];
    [self.lblCustomerName sizeToFit];
    
    [self.lblCustomerEmail setNumberOfLines:0];
    [self.lblCustomerEmail sizeToFit];
    
    [self.lblAddress setNumberOfLines:0];
    [self.lblAddress sizeToFit];
    
    [self.lblNote setNumberOfLines:0];
    [self.lblNote sizeToFit];
    
    if (self.source == Sale) {
        [self setHiddenStatusAndTime:YES];
        
    } else {
        [self setHiddenStatusAndTime:NO];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellModel:(PotentialCustomerFormRecord *)cellModel {
    _cellModel = cellModel;
    self.lblCustomerName.text = cellModel.FullName;
    self.lblCustomerPhoneNumber.text = cellModel.Phone1;
    self.lblCustomerEmail.text = cellModel.Email;
    self.lblCreateDate.text = cellModel.CreateDate;
    self.lblAddress.text = cellModel.Address;
    
    self.lblNote.text = cellModel.Note;
    self.lblRemainder.text = cellModel.remainder;
    
    if (![cellModel.TotalSchedule isEqualToString:@"0"]){
        
        [self.viewAlarm setHidden:NO];
        self.lbltotalSchedule.text = cellModel.TotalSchedule;
        [self.btnSchedule setEnabled:YES];
    }else {
        [self.viewAlarm setHidden:YES];
        [self.btnSchedule setEnabled:NO];
    }
    
    
    if (cellModel.acceptStatus == 0) {
        self.lblAcceptStatus.text = @"Chưa nhận";
    } else {
        self.lblAcceptStatus.text = @"Đã nhận";

    }

    self.lblSTT.text = [NSString stringWithFormat:@"%li", (long)self.cellIndex];
    
}

- (void)setHiddenStatusAndTime:(BOOL)status {
    self.statusLabel.hidden = status;
    self.timeLabel.hidden   = status;
    self.lblAcceptStatus.hidden = status;
    self.lblRemainder.hidden    = status;
}

- (IBAction)btnSchedule_clicked:(id)sender {
    
    //[self.delegate didSelectedAlarm:_cellModel.ID];
    [self.delegate didSelectedAlarm:_cellModel.ID andSupporter:_cellModel.Supporter];
   
}


@end
