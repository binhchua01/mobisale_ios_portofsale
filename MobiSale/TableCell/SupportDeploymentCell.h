//
//  SupportDeploymentCell.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 1/24/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SupportDeploymentCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblContact;
@property (strong, nonatomic) IBOutlet UILabel *lblId;
@property (strong, nonatomic) IBOutlet UILabel *lblCreateDate;
@property (strong, nonatomic) IBOutlet UILabel *lblPhone;
@property (strong, nonatomic) IBOutlet UILabel *lblDeploymentDate;
@property (strong, nonatomic) IBOutlet UILabel *lblAssign;
@property (strong, nonatomic) IBOutlet UILabel *lblFinishDate;
@property (strong, nonatomic) IBOutlet UILabel *lblInventory;
@property (strong, nonatomic) IBOutlet UILabel *lblSTT;
@property (strong, nonatomic) IBOutlet UILabel *lblDeploymentAppointmentDate;
@property (strong, nonatomic) IBOutlet UIView *lblView;
@property (strong, nonatomic) IBOutlet UIImageView *img;
@property (weak, nonatomic) IBOutlet UILabel *lblFullName;

@end
