//
//  ListSBIDetailsViewCell.h
//  TicketMobile
//
//  Created by Thont on 3/3/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListSBIDetailsViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblNumber;
@property (strong, nonatomic) IBOutlet UILabel *lblName;

@end
