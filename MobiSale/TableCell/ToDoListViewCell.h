//
//  ToDoListViewCell.h
//  MobiSale
//
//  Created by ISC-DanTT on 11/30/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ToDoListRecord.h"

@protocol ToDoListCellDelegate <NSObject>

- (void)sendEmailToAddress:(NSString*)stringAddress;
@optional
- (void)confirmNotificationWithData:(ToDoListRecord *)data;

@end

@interface ToDoListViewCell : UITableViewCell <UIActionSheetDelegate>

@property (strong, nonatomic) IBOutlet UILabel *lblObjStatusName;
@property (strong, nonatomic) IBOutlet UILabel *lblLevelName;
@property (strong, nonatomic) IBOutlet UILabel *lblDivisionName;
@property (strong, nonatomic) IBOutlet UILabel *lblObjStatusDesc;
@property (strong, nonatomic) IBOutlet UILabel *lblStartDate;
@property (strong, nonatomic) IBOutlet UILabel *lblProcessTime;
@property (strong, nonatomic) IBOutlet UILabel *lblContact;
@property (strong, nonatomic) IBOutlet UILabel *lblCountdownTime;
@property (weak, nonatomic) IBOutlet UILabel *statusNameLabel;
@property (strong, nonatomic) IBOutlet UIButton *btnEmail;
@property (strong, nonatomic) IBOutlet UIButton *btnPhoneNumber;
@property (weak, nonatomic) IBOutlet UIButton *confirmInfoButton;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (weak, nonatomic) IBOutlet UIView *saleInfoView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *saleInfoViewHeightLC;
@property (weak, nonatomic) IBOutlet UILabel *saleNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *saleFullNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *saleEmailButton;
@property (weak, nonatomic) IBOutlet UIButton *salePhoneButton;
//
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *statusDescriptionLabelHeightLC;

//
@property (weak, nonatomic) IBOutlet UIView *statusDescriptionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *statusDescriptionViewHeightLC;
@property (weak, nonatomic) IBOutlet UILabel *surveySessionLabel;
@property (weak, nonatomic) IBOutlet UILabel *customerCommentLabel;
@property (weak, nonatomic) IBOutlet UILabel *surveyStaffNotedLabel;
@property (weak, nonatomic) IBOutlet UIImageView *customerCommentImageView;
//
@property (retain, nonatomic) id<ToDoListCellDelegate> delegate;
@property (strong, nonatomic) ToDoListRecord *cellModel;

@property (strong, nonatomic) NSDate *startDate;
@property (strong, nonatomic) NSTimer *stopWatchTimer;

@property (strong, nonatomic) NSString *processTime;
@end
