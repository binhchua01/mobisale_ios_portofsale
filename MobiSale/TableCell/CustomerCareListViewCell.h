//
//  CustomerCareListViewCell.h
//  MobiSale
//
//  Created by ISC-DanTT on 11/30/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomerCareRecord.h"

@protocol CustomerCareListCellDelegate <NSObject>

- (void)sendEmailToAddress:(NSString*)stringAddress;
@optional
- (void)confirmNotificationWithData:(CustomerCareRecord *)data;

@end

@interface CustomerCareListViewCell : UITableViewCell <UIActionSheetDelegate>

@property (retain, nonatomic) id<CustomerCareListCellDelegate> delegate;
@property (strong, nonatomic) CustomerCareRecord *cellModel;

@property (strong, nonatomic) IBOutlet UILabel *lblContract;
@property (strong, nonatomic) IBOutlet UILabel *lblObjStatusName;
@property (strong, nonatomic) IBOutlet UILabel *lblFullname;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblObjStatusDesc;
@property (strong, nonatomic) IBOutlet UILabel *lblCreateDate;
@property (strong, nonatomic) IBOutlet UIButton *btnPhoneNumber;
@property (strong, nonatomic) IBOutlet UIButton *btnEmail;
@property (weak, nonatomic) IBOutlet UIButton *confirmInfoButton;

@property (strong, nonatomic) IBOutlet UIImageView *imageAvatar;
@property (strong, nonatomic) IBOutlet UILabel *lblBackgroundColor;

@property (weak, nonatomic) IBOutlet UIView *saleInfoView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *saleInfoViewHeightLC;
//
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *statusDescriptionLabelHeightLC;
//
@property (weak, nonatomic) IBOutlet UIView *statusDescriptionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *statusDescriptionViewHeightLC;
@property (weak, nonatomic) IBOutlet UILabel *surveySessionLabel;
@property (weak, nonatomic) IBOutlet UILabel *customerCommentLabel;
@property (weak, nonatomic) IBOutlet UILabel *surveyStaffNotedLabel;
@property (weak, nonatomic) IBOutlet UIImageView *customerCommentImageView;

//
@property (weak, nonatomic) IBOutlet UILabel *saleNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *saleFullNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *saleEmailButton;
@property (weak, nonatomic) IBOutlet UIButton *salePhoneButton;


@end
