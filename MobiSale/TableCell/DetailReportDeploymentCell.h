//
//  DetailReportDeploymentCell.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 2/4/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailReportDeploymentCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblContact;
@property (strong, nonatomic) IBOutlet UILabel *lblFullName;
@property (strong, nonatomic) IBOutlet UILabel *lblLocaltype;
@property (strong, nonatomic) IBOutlet UILabel *lblCreateDate;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblSTT;
@property (weak, nonatomic) IBOutlet UILabel *lblPhoneNumber;
@property (strong, nonatomic) IBOutlet UIView *lblView;

@end
