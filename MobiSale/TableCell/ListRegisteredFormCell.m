//
//  ListRegisteredFormCell.m
//  MobiSales
//
//  Created by Nguyen Tan Tho on 1/23/15.
//  Copyright (c) 2015 Aryan Ghassemi. All rights reserved.
//

#import "ListRegisteredFormCell.h"

@implementation ListRegisteredFormCell

- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)drawRect:(CGRect)rect {
    [self.lblisInVest sizeToFit];

}

@end
