//
//  PromotionModel.m
//  MobiSale
//
//  Created by HIEUPC on 2/2/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "PromotionModel.h"

@implementation PromotionModel

@synthesize PromotionName,PromotionId,Amount;
-(id)initWithName:(NSString*)n description:(NSString *)desc Amount:(NSString *)amount Type:(NSString *)type{
    self.PromotionName = desc;
    self.PromotionId = n;
    self.Amount = amount;
    self.Type = type;
    return self;
}

@end
