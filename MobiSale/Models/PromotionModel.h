//
//  PromotionModel.h
//  MobiSale
//
//  Created by HIEUPC on 2/2/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PromotionModel : NSObject{
    NSString *Values;
    NSString *Key;
}
@property(nonatomic,copy)NSString *PromotionName;
@property(nonatomic,copy)NSString *PromotionId;
@property(nonatomic,copy)NSString *Amount;
@property(nonatomic,copy)NSString *Type;

-(id)initWithName:(NSString*)n description:(NSString *)desc Amount:(NSString *)amount Type:(NSString *)type;
@end
