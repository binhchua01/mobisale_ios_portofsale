//
//  Common.m
//  MobiSales
//
//  Created by Nguyen Tan Tho on 21/01/15.
//  Copyright (c) 2014 FPT.RAD.Mobisale. All rights reserved.
//

#import "Common.h"
#import <netinet/in.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#import <sys/socket.h>

@implementation Common


//---------------
+ (NSMutableArray *) getTypeSearch: (NSString *)Key
{
    if([Key isEqualToString:@"1"]){
        //KeyValueModel *s0 = [[KeyValueModel alloc] initWithName:@"0" description:@"Tất cả"];
        KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"1" description:@"Số HĐ"];
        KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"2" description:@"Tên KH"];
        KeyValueModel *s3 = [[KeyValueModel alloc] initWithName:@"3" description:@"Số ĐT"];
        NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s1,s2,s3,nil];
        return lst;

    }else if([Key isEqualToString:@"2"]){
        //KeyValueModel *s0 = [[KeyValueModel alloc] initWithName:@"0" description:@"Tất cả"];
        KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"1" description:@"Số HĐ"];
        KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"2" description:@"Tên KH"];
        KeyValueModel *s3 = [[KeyValueModel alloc] initWithName:@"3" description:@"Số ĐT"];
        KeyValueModel *s4 = [[KeyValueModel alloc] initWithName:@"4" description:@"Số phiếu ĐK"];
        NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s1,s2,s3,s4,nil];
        return lst;

    }else if([Key isEqualToString:@"3"]){
        KeyValueModel *s0 = [[KeyValueModel alloc] initWithName:@"0" description:@"Tất cả"];
        KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"1" description:@"Nhân viên"];
        NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s0,s1,nil];
        return lst;
        
    }

    return nil;
}

+ (NSMutableArray *) getPhoneType
{
    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"1" description:@"Cơ quan"];
    KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"2" description:@"Fax"];
    KeyValueModel *s3 = [[KeyValueModel alloc] initWithName:@"3" description:@"Nhà riêng"];
    KeyValueModel *s4 = [[KeyValueModel alloc] initWithName:@"4" description:@"Di động"];
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s1,s2,s3,s4,nil];
    return lst;
    
}

+ (NSMutableArray *) getPartNer {

    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"1" description:@"Khách hàng mới hoàn toàn"];
    KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"2" description:@"Khách hàng từ ISP khác"];
    KeyValueModel *s3 = [[KeyValueModel alloc] initWithName:@"3" description:@"Khách hàng FPT huỷ cũ ký lại"];
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s1,s2,s3,nil];
    return lst;
    
}

+ (NSMutableArray *) getISP
{
    KeyValueModel *s0 = [[KeyValueModel alloc] initWithName:@"0" description:@"[Vui lòng chọn]"];
    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"1" description:@"Viettel"];
    KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"2" description:@"VNPT"];
    KeyValueModel *s3 = [[KeyValueModel alloc] initWithName:@"3" description:@"Netnam"];
    KeyValueModel *s4 = [[KeyValueModel alloc] initWithName:@"4" description:@"CMC"];
    KeyValueModel *s5 = [[KeyValueModel alloc] initWithName:@"5" description:@"SCTV"];
    KeyValueModel *s6 = [[KeyValueModel alloc] initWithName:@"6" description:@"HCTV"];
    KeyValueModel *s7 = [[KeyValueModel alloc] initWithName:@"7" description:@"Khác"];
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s0,s1,s2,s3,s4,s5,s6,s7,nil];
    return lst;
    
}

+ (NSMutableArray *) getCustomerType
{
    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"10" description:@"Công ty"];
    KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"11" description:@"Học sinh, sinh viên"];
    KeyValueModel *s3 = [[KeyValueModel alloc] initWithName:@"12" description:@"Hộ gia đình"];
    KeyValueModel *s4 = [[KeyValueModel alloc] initWithName:@"13" description:@"Hộ kinh doanh"];
    KeyValueModel *s5 = [[KeyValueModel alloc] initWithName:@"14" description:@"Khách hàng nước ngoài"];
    KeyValueModel *s6 = [[KeyValueModel alloc] initWithName:@"15" description:@"KXD"];
    KeyValueModel *s7 = [[KeyValueModel alloc] initWithName:@"16" description:@"Ký túc xá"];
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s1,s2,s3,s4,s5,s6,s7,nil];
    return lst;
    
}

+ (NSMutableArray *) getCurrentPlace
{

    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"1" description:@"Ở trọ, ở thuê"];
    KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"2" description:@"Định cư lâu dài"];
    KeyValueModel *s3 = [[KeyValueModel alloc] initWithName:@"3" description:@"Ký túc xá"];
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s1,s2,s3,nil];
    return lst;
    
}

+ (NSMutableArray *) getSolvency
{

    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"1" description:@"Cao"];
    KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"2" description:@"Trung bình"];
    KeyValueModel *s3 = [[KeyValueModel alloc] initWithName:@"3" description:@"Thấp"];
    KeyValueModel *s4 = [[KeyValueModel alloc] initWithName:@"4" description:@"Rất thấp"];
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s1,s2,s3,s4,nil];
    return lst;
    
}

+ (NSMutableArray *) getCustomerOfPartner
{

    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"1" description:@"SST"];
    KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"2" description:@"BTS"];
    KeyValueModel *s3 = [[KeyValueModel alloc] initWithName:@"3" description:@"VITA"];
    KeyValueModel *s4 = [[KeyValueModel alloc] initWithName:@"5" description:@"KNT"];
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s1,s2,s3,s4,nil];
    return lst;
    
}

+ (NSMutableArray *) getLegal
{

    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"1" description:@"FPT"];
    KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"2" description:@"Đối tác"];
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s1,s2,nil];
    return lst;
    
}

+ (NSMutableArray *) getTypeHouse
{
    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"1" description:@"Nhà phố"];
    KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"2" description:@"C.Cư, C.Xá, Chợ, Thương xá"];
    KeyValueModel *s3 = [[KeyValueModel alloc] initWithName:@"3" description:@"Nhà không địa chỉ"];
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s1,s2,s3,nil];
    return lst;
    
}

+ (NSMutableArray *) getHouseCoordinate
{
    KeyValueModel *s0 = [[KeyValueModel alloc] initWithName:@"0" description:@"Vui lòng chọn"];
    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"1" description:@"Kế bên phải"];
    KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"2" description:@"Kế bên trái"];
    KeyValueModel *s3 = [[KeyValueModel alloc] initWithName:@"3" description:@"Đối diện"];
    KeyValueModel *s4 = [[KeyValueModel alloc] initWithName:@"4" description:@"Phía sau"];
    KeyValueModel *s5 = [[KeyValueModel alloc] initWithName:@"5" description:@"Cách"];
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s0,s1,s2,s3,s4,s5,nil];
    return lst;
    
}

+ (NSMutableArray *) getDeposit
{
    KeyValueModel *s0 = [[KeyValueModel alloc] initWithName:@"0" description:@"0"];
    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"330000" description:@"330,000"];
    KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"660000" description:@"660,000"];
    KeyValueModel *s3 = [[KeyValueModel alloc] initWithName:@"1100000" description:@"1,100,000"];
    KeyValueModel *s4 = [[KeyValueModel alloc] initWithName:@"2200000" description:@"2,200,000"];
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s0,s1,s2,s3,s4,nil];
    return lst;
    
}

+ (NSMutableArray *) getDepositBlackPoint
{
    KeyValueModel *s0 = [[KeyValueModel alloc] initWithName:@"0" description:@"0"];
    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"330000" description:@"330,000"];
    KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"660000" description:@"660,000"];
    KeyValueModel *s3 = [[KeyValueModel alloc] initWithName:@"1100000" description:@"1,100,000"];
    KeyValueModel *s4 = [[KeyValueModel alloc] initWithName:@"2200000" description:@"2,200,000"];
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s0,s1,s2,s3,s4,nil];
    return lst;
    
}

+ (NSMutableArray *) getIndoor
{
    KeyValueModel *s0 = [[KeyValueModel alloc] initWithName:@"0" description:@"Vui lòng chọn"];
    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"52" description:@"1 core"];
    KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"62" description:@"2 core"];
    KeyValueModel *s3 = [[KeyValueModel alloc] initWithName:@"72" description:@"4 core"];
    KeyValueModel *s4 = [[KeyValueModel alloc] initWithName:@"812" description:@"0.5 mm"];
    
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s0,s1,s2,s3,s4,nil];
    return lst;
    
}

+ (NSMutableArray *) getOutdoor
{
    KeyValueModel *s0 = [[KeyValueModel alloc] initWithName:@"0" description:@"Vui lòng chọn"];
    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"52" description:@"1 core"];
    KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"62" description:@"2 core"];
    KeyValueModel *s3 = [[KeyValueModel alloc] initWithName:@"72" description:@"4 core"];
    KeyValueModel *s4 = [[KeyValueModel alloc] initWithName:@"812" description:@"0.5 mm"];
    
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s0,s1,s2,s3,s4,nil];
    return lst;
    
}

+ (NSMutableArray *) getModem
{
    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"0" description:@"Khong lay Modem"];
    KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"1" description:@"Modem thue"];
    KeyValueModel *s3 = [[KeyValueModel alloc] initWithName:@"2" description:@"Modem tang"];
    KeyValueModel *s4 = [[KeyValueModel alloc] initWithName:@"3" description:@"Modem ban"];
    
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s1,s2,s3,s4,nil];
    return lst;
    
}

+ (NSMutableArray *) getCodeMap
{
    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"M001" description:@"M001"];
    KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"M002" description:@"M002"];
    KeyValueModel *s3 = [[KeyValueModel alloc] initWithName:@"M003" description:@"M003"];
    KeyValueModel *s4 = [[KeyValueModel alloc] initWithName:@"M004" description:@"M004"];
    KeyValueModel *s5 = [[KeyValueModel alloc] initWithName:@"M005" description:@"M005"];
    KeyValueModel *s6 = [[KeyValueModel alloc] initWithName:@"M006" description:@"M006"];
    KeyValueModel *s7 = [[KeyValueModel alloc] initWithName:@"M007" description:@"M007"];
    KeyValueModel *s8 = [[KeyValueModel alloc] initWithName:@"M008" description:@"M008"];
    KeyValueModel *s9 = [[KeyValueModel alloc] initWithName:@"M009" description:@"M009"];
    KeyValueModel *s10 = [[KeyValueModel alloc] initWithName:@"M0010" description:@"M0010"];
    KeyValueModel *s11 = [[KeyValueModel alloc] initWithName:@"M0011" description:@"M0011"];
    KeyValueModel *s12 = [[KeyValueModel alloc] initWithName:@"M0012" description:@"M0012"];
    
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12,nil];
    return lst;
    
}

+ (NSMutableArray *) getDepartments
{
    KeyValueModel *s0 = [[KeyValueModel alloc] initWithName:@"0" description:@"Vui lòng chọn"];
    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"4" description:@"IBB"];
    KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"7" description:@"CUS"];
    KeyValueModel *s3 = [[KeyValueModel alloc] initWithName:@"43" description:@"CS"];
    KeyValueModel *s4 = [[KeyValueModel alloc] initWithName:@"39" description:@"BDD"];
    
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s0,s1,s2,s3,s4,nil];
    return lst;
    
}

+ (NSMutableArray *) getCombo
{
    KeyValueModel *s0 = [[KeyValueModel alloc] initWithName:@"0" description:@"[Chọn combo]"];
    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"2" description:@"Combo"];
    KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"3" description:@"Không combo"];
    
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s0,s1,s2,nil];
    return lst;
    
}

+ (NSMutableArray *) getDeployment
{
    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"1" description:@"Triển khai mới nhiều line"];
    KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"5" description:@"Triển khai mới một line"];
    
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s2,s1,nil];
    return lst;
    
}

+ (NSMutableArray *) getBoxType
{
    KeyValueModel *s0 = [[KeyValueModel alloc] initWithName:@"0" description:@"Không chọn"];
    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"1" description:@"HD Box"];
    
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s0,s1,nil];
    return lst;
    
}

+ (NSMutableArray *) getServicePackage
{
  //  KeyValueModel *s0 = [[KeyValueModel alloc] initWithName:@"Premium HD" description:@"Premium HD"];
    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"VOD HD"     description:@"Truyền hình FPT"];
    
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s1,nil];
    return lst;
    
}

+ (NSMutableArray *) getLocalType
{
    KeyValueModel *s0 = [[KeyValueModel alloc] initWithName:@"0" description:@"Internet"];
    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"1" description:@"IPTV"];
    KeyValueModel *s3 = [[KeyValueModel alloc] initWithName:@"3" description:@"Office 365"];
    KeyValueModel *s4 = [[KeyValueModel alloc]initWithName:@"4" description:@"FPT Play Box"];
    KeyValueModel *s5 = [[KeyValueModel alloc]initWithName:@"5" description:@"Bán Thiết Bị"];
  
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s0 ,s1 ,s3,s4,s5,nil];
    
    return lst;
    
}

+ (NSMutableArray *) getCusType
{
    KeyValueModel *s0  = [[KeyValueModel alloc] initWithName:@"1" description:@"Cá nhân Việt Nam"];
    KeyValueModel *s1  = [[KeyValueModel alloc] initWithName:@"2" description:@"Cá nhân nước ngoài"];
    KeyValueModel *s2  = [[KeyValueModel alloc] initWithName:@"3" description:@"Cơ quan Việt Nam"];
    KeyValueModel *s3  = [[KeyValueModel alloc] initWithName:@"4" description:@"Cơ quan nước ngoài"];
    KeyValueModel *s4  = [[KeyValueModel alloc] initWithName:@"5" description:@"Công ty tư nhân"];
    KeyValueModel *s5  = [[KeyValueModel alloc] initWithName:@"6" description:@"Nhân viên FPT"];
    KeyValueModel *s6  = [[KeyValueModel alloc] initWithName:@"7" description:@"Nhà nước"];
    KeyValueModel *s7  = [[KeyValueModel alloc] initWithName:@"8" description:@"Nước ngoài"];
    KeyValueModel *s8  = [[KeyValueModel alloc] initWithName:@"9" description:@"Công ty"];
    KeyValueModel *s9  = [[KeyValueModel alloc] initWithName:@"10" description:@"Giáo dục"];
    KeyValueModel *s10 = [[KeyValueModel alloc] initWithName:@"11" description:@"Nhân viên FTEL"];
    KeyValueModel *s11 = [[KeyValueModel alloc] initWithName:@"12" description:@"Nhân viên tập đoàn FPT"];
    KeyValueModel *s12 = [[KeyValueModel alloc] initWithName:@"13" description:@"Nhân viên TIN/PNC"];
    
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s0,s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12,nil];
    return lst;
    
}

+ (NSMutableArray *) getPartCustomer{
    KeyValueModel *s0 = [[KeyValueModel alloc] initWithName:@"0" description:@"[Đối tác]"];
    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"1" description:@"SST"];
    KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"2" description:@"BTS"];
    KeyValueModel *s3 = [[KeyValueModel alloc] initWithName:@"3" description:@"VITA"];
    KeyValueModel *s4 = [[KeyValueModel alloc] initWithName:@"5" description:@"KNT"];
    
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s0,s1,s2,s3,s4,nil];
    return lst;
    
}

+(NSString *)getIPAddress {
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    NSString *wifiAddress = nil;
    NSString *cellAddress = nil;
    NSString *addr;
    if(!getifaddrs(&interfaces)) {
        temp_addr = interfaces;
        while(temp_addr != NULL) {
            sa_family_t sa_type = temp_addr->ifa_addr->sa_family;
            if(sa_type == AF_INET || sa_type == AF_INET6) {
                NSString *name = [NSString stringWithUTF8String:temp_addr->ifa_name];
                addr = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                if([name isEqualToString:@"en0"]) {
                    wifiAddress = addr;
                } else
                    if([name isEqualToString:@"pdp_ip0"]) {
                        cellAddress = addr;
                    }
            }
            temp_addr = temp_addr->ifa_next;
        }
        // Free memory
        freeifaddrs(interfaces);
    }
    return addr;
}

//add by dantt, chuyển câu có dấu thành không dấu
+ (NSString *)normalizeVietnameseString:(NSString *)str {
    NSMutableString *originStr = [NSMutableString stringWithString:str];
    CFStringNormalize((CFMutableStringRef)originStr, kCFStringNormalizationFormD);
    
    CFStringFold((CFMutableStringRef)originStr, kCFCompareDiacriticInsensitive, NULL);
    
    NSString *finalString1 = [originStr stringByReplacingOccurrencesOfString:@"đ"withString:@"d"];
    NSString *finalString2 = [finalString1 stringByReplacingOccurrencesOfString:@"Đ"withString:@"D"];
    
    return finalString2;
}
// add by dantt, lựa chọn loại hổ trợ trong update ToDoListDetail
+ (NSMutableArray *) getCareType{
    KeyValueModel *s0 = [[KeyValueModel alloc] initWithName:@"Call" description:@"Gọi điện cho KH"];
    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"SMS" description:@"Gửi SMS"];
    KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"Email" description:@"Gửi Email"];
    KeyValueModel *s3 = [[KeyValueModel alloc] initWithName:@"" description:@"Khác..."];
    
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s0,s1,s2,s3,nil];
    return lst;
    
}

+ (NSMutableArray *) getServiceType {
    KeyValueModel *s0 = [[KeyValueModel alloc] initWithName:@"0" description:@"Đăng ký Internet"];
    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"1" description:@"Đăng ký IPTV"];
    KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"2" description:@"Đăng ký IPTV và Internet"];
    
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s0,s1,s2,nil];
    return lst;
    
}

+ (NSString*)formatTimeInMinutes:(int)totalMin{
    int days = totalMin/1440;
    int hours = (totalMin/60)/60;
    int minutes = totalMin%60;
    if (days>0) {
        return StringFormat(@"%i ngày, %i giờ, %i phút",days,hours,minutes);
        
    }else{
        return StringFormat(@"%i giờ, %i phút",hours,minutes);
    }
}

+ (NSString*)formatTimeInSeconds:(int)totalSeconds {
    int days = totalSeconds /86400;
    int hours = (totalSeconds %86400)/3600;
    int minutes = ((totalSeconds %86400)%3600)/60;
    int seconds = totalSeconds %60;
    if (days!=0) {
        return StringFormat(@"%i ngày %i:%i:%i",days,abs(hours),abs(minutes),abs(seconds));
        
    }else{
        return StringFormat(@"%i:%i:%i",hours,abs(minutes),abs(seconds));
    }
}

+ (void)callPhone:(NSString*)phoneNumber {
    NSString *phoneNumberString = phoneNumber;
    phoneNumberString = [phoneNumberString stringByReplacingOccurrencesOfString:@" " withString:@""];
    phoneNumberString = [NSString stringWithFormat:@"tel:%@", phoneNumberString];
    NSURL *phoneNumberURL = [NSURL URLWithString:phoneNumberString];
    if ([[UIApplication sharedApplication] canOpenURL:phoneNumberURL]) {
        [[UIApplication sharedApplication] openURL:phoneNumberURL];
        return;
    }
    UIAlertView *calert = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:@"Số điện thoại không tồn tại" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
    
    [calert show];
}

+ (NSMutableArray *) getMarkerType {
    KeyValueModel *s0  = [[KeyValueModel alloc] initWithName:@"AN VIÊN" description:@"ic_marker_an_vien"];
    KeyValueModel *s1  = [[KeyValueModel alloc] initWithName:@"CMC" description:@"ic_marker_cmc"];
    KeyValueModel *s2  = [[KeyValueModel alloc] initWithName:@"HCTV" description:@"ic_marker_hctv"];
    KeyValueModel *s3  = [[KeyValueModel alloc] initWithName:@"HTVC" description:@"ic_marker_htvc"];
    KeyValueModel *s4  = [[KeyValueModel alloc] initWithName:@"K+" description:@"ic_marker_k_plus"];
    KeyValueModel *s5  = [[KeyValueModel alloc] initWithName:@"KHÁC" description:@"ic_marker_other"];
    KeyValueModel *s6  = [[KeyValueModel alloc] initWithName:@"MY TV" description:@"ic_marker_my_tv"];
    KeyValueModel *s7  = [[KeyValueModel alloc] initWithName:@"NETNAM" description:@"ic_marker_netnam"];
    KeyValueModel *s8  = [[KeyValueModel alloc] initWithName:@"NEXT TV" description:@"ic_marker_next_tv"];
    KeyValueModel *s9  = [[KeyValueModel alloc] initWithName:@"SCTV" description:@"ic_marker_sctv"];
    KeyValueModel *s10 = [[KeyValueModel alloc] initWithName:@"VIETTEL" description:@"ic_marker_viettel"];
    KeyValueModel *s11 = [[KeyValueModel alloc] initWithName:@"VNPT" description:@"ic_marker_vnpt"];
    KeyValueModel *s12 = [[KeyValueModel alloc] initWithName:@"VTC" description:@"ic_marker_vtc"];
    KeyValueModel *s13 = [[KeyValueModel alloc] initWithName:@"VTVCAB" description:@"ic_marker_vtv_cab"];
    NSMutableArray *lst= [[NSMutableArray alloc] initWithObjects:s0,s1,s2,s3,s4,s5,s6,s7,s8,s9,s10,s11,s12,s13,nil];
    return lst;
    
}

@end
