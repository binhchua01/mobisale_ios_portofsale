//
//  Common.h
//  FTool
//
//  Created by MAC on 11/17/14.
//  Copyright (c) 2014 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeyValueModel.h"

@interface Common : NSObject

+ (NSMutableArray *) getTypeSearch: (NSString *)Key;
+ (NSMutableArray *) getPhoneType;
+ (NSMutableArray *) getPartNer;
+ (NSMutableArray *) getISP;
+ (NSMutableArray *) getCustomerType;
+ (NSMutableArray *) getCurrentPlace;
+ (NSMutableArray *) getSolvency;
+ (NSMutableArray *) getCustomerOfPartner;
+ (NSMutableArray *) getLegal;
+ (NSMutableArray *) getTypeHouse;
+ (NSMutableArray *) getHouseCoordinate;
+ (NSMutableArray *) getDeposit;
+ (NSMutableArray *) getIndoor;
+ (NSMutableArray *) getOutdoor;
+ (NSMutableArray *) getModem;
+ (NSMutableArray *) getDepartments;
+ (NSMutableArray *) getCombo;
+ (NSMutableArray *) getDeployment;
+ (NSMutableArray *) getBoxType;
+ (NSMutableArray *) getServicePackage;
+ (NSMutableArray *) getCusType;
+ (NSMutableArray *) getDepositBlackPoint;
+ (NSMutableArray *) getLocalType;
+ (NSMutableArray *) getCodeMap;
+ (NSMutableArray *) getPartCustomer;
+ (NSString *)getIPAddress;
// add by dantt
+ (NSString *)normalizeVietnameseString:(NSString *)str;
+ (NSMutableArray *)getCareType;
+ (NSMutableArray *) getServiceType;
+ (NSString*)formatTimeInMinutes:(int)totalMin;
+ (NSString*)formatTimeInSeconds:(int)totalSeconds;
+ (void)callPhone:(NSString*)phoneNumber;
+ (NSMutableArray *) getMarkerType;

@end
