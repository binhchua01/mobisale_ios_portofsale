//
//  UserRecord.m
//  Ftool
//
//  Created by Nguyen Tan Tho on 3/11/14.
//  Copyright (c) 2014 RAD. All rights reserved.
//

#import "UserRecord.h"

@implementation UserRecord

@synthesize userDeviceIMEI;
@synthesize userIsActive;
@synthesize userisInsideAccount;
@synthesize userName;
@synthesize userPass;
@synthesize userSIMIMEI;
@synthesize IsManager;
@synthesize LocationID;
@synthesize LocationName;
@synthesize BrachCode;
@synthesize ManagerName;
@synthesize PortLocationID;
@synthesize LocationParent;
@synthesize UseMPOS;

// add by DanTT 2015.10.08
@synthesize sessionID;
@synthesize token;
@synthesize IsCreateContract;
@synthesize arrDepartment;

//add for auto bookport, by StevenNguyen
@synthesize autoBookPort;
@synthesize autoBookPort_RetryConnect;
@synthesize autoBookPort_Timeout;
@synthesize autoBookPort_iR;
@synthesize arrTypePortOfSale;

- (id)initWithCoder:(NSCoder *)aDecoder {
	if ((self = [self init])){

		self.userDeviceIMEI      = [aDecoder decodeObjectForKey:@"userDeviceIMEI"];
        self.userIsActive        = [aDecoder decodeObjectForKey:@"userIsActive"];
        self.userisInsideAccount = [aDecoder decodeObjectForKey:@"userisInsideAccount"];
        self.userName            = [aDecoder decodeObjectForKey:@"userName"];
        self.userPass            = [aDecoder decodeObjectForKey:@"userPass"];
        self.userSIMIMEI         = [aDecoder decodeObjectForKey:@"userSIMIMEI"];
        self.IsManager           = [aDecoder decodeObjectForKey:@"IsManager"];
        self.LocationID          = [aDecoder decodeObjectForKey:@"LocationID"];
        self.LocationName        = [aDecoder decodeObjectForKey:@"LocationName"];
        self.BrachCode           = [aDecoder decodeObjectForKey:@"BrachCode"];
        self.ManagerName         = [aDecoder decodeObjectForKey:@"ManagerName"];
        self.PortLocationID      = [aDecoder decodeObjectForKey:@"PortLocationID"];
        self.LocationParent      = [aDecoder decodeObjectForKey:@"LocationParent"];
        self.UseMPOS             = [aDecoder decodeObjectForKey:@"UseMPOS"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder {
    
    [aCoder encodeObject:self.userDeviceIMEI      forKey:@"userDeviceIMEI"];
    [aCoder encodeObject:self.userIsActive        forKey:@"userIsActive"];
    [aCoder encodeObject:self.userisInsideAccount forKey:@"userisInsideAccount"];
    [aCoder encodeObject:self.userName            forKey:@"userName"];
    [aCoder encodeObject:self.userPass            forKey:@"userPass"];
    [aCoder encodeObject:self.userSIMIMEI         forKey:@"userSIMIMEI"];
    [aCoder encodeObject:self.userDeviceIMEI      forKey:@"IsManager"];
    [aCoder encodeObject:self.userIsActive        forKey:@"LocationID"];
    [aCoder encodeObject:self.userisInsideAccount forKey:@"LocationName"];
    [aCoder encodeObject:self.userName            forKey:@"BrachCode"];
    [aCoder encodeObject:self.userPass            forKey:@"ManagerName"];
    [aCoder encodeObject:self.userSIMIMEI         forKey:@"PortLocationID"];
    [aCoder encodeObject:self.LocationParent      forKey:@"LocationParent"];
    [aCoder encodeObject:self.UseMPOS             forKey:@"UseMPOS"];
    
}

- (id) copyWithZone:(NSZone *)zone{
    
    UserRecord *object      = [[UserRecord alloc] init];
    object.userDeviceIMEI   = [self.userDeviceIMEI copyWithZone:zone];
    object.userIsActive     = [self.userIsActive copyWithZone:zone];
    object.userisInsideAccount = [self.userisInsideAccount copyWithZone:zone];
    object.userName         = [self.userName copyWithZone:zone];
    object.userPass         = [self.userPass copyWithZone:zone];
    object.userSIMIMEI      = [self.userSIMIMEI copyWithZone:zone];
    object.IsManager        = [self.IsManager copyWithZone:zone];
    object.LocationID       = [self.LocationID copyWithZone:zone];
    object.LocationName     = [self.LocationName copyWithZone:zone];
    object.BrachCode        = [self.BrachCode copyWithZone:zone];
    object.ManagerName      = [self.ManagerName copyWithZone:zone];
    object.PortLocationID   = [self.PortLocationID copyWithZone:zone];
    object.LocationParent   = [self.LocationParent copyWithZone:zone];
    object.UseMPOS          = [self.UseMPOS copyWithZone:zone];
    
    return object;
}


@end
