//
//  KeyValueModel.m
//  MobiPay
//
//  Created by VANDN on 3/13/14.
//  Copyright (c) 2014 VANDN. All rights reserved.
//

#import "KeyValueModel.h"

#define kTitleValues         @"KeyValueModel_Values"
#define kTitleKey           @"KeyValueModel_Key"

@implementation KeyValueModel

@synthesize Values,Key;

//Method Coder Object...
-(void)encodeWithCoder:(NSCoder *)aCoder {
    [aCoder encodeObject:Values forKey:kTitleValues];
    [aCoder encodeObject:Key forKey:kTitleKey];
}

//Method Decoder Object...
-(id)initWithCoder:(NSCoder *)aDecoder {
    NSString *titleValues = [aDecoder decodeObjectForKey:kTitleValues];
    NSString *titleKey = [aDecoder decodeObjectForKey:kTitleKey];
    return [self initWithName:titleKey description:titleValues];
}

-(id)initWithName:(NSString*)n description:(NSString*)desc{
    self.Values = desc;
    self.Key = n;
    return self;
}

@end
