//
//  UserRecord.h
//  Ftool
//
//  Created by Nguyen Tan Tho on 3/11/14.
//  Copyright (c) 2014 RAD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserRecord : NSObject

@property (nonatomic,retain) NSString *userName;
@property (nonatomic,retain) NSString *userPass;
@property (nonatomic,retain) NSString *userDeviceIMEI;
@property (nonatomic,retain) NSString *userSIMIMEI;
@property (nonatomic,retain) NSString *userIsActive;
@property (nonatomic,retain) NSString *userisInsideAccount;
@property (nonatomic,retain) NSString *IsManager;
@property (nonatomic,retain) NSString *LocationName;
@property (nonatomic,retain) NSString *ManagerName;
@property (nonatomic,retain) NSString *LocationID;
@property (nonatomic,retain) NSString *PortLocationID;
@property (nonatomic,retain) NSString *BrachCode;
@property (nonatomic,retain) NSString *LocationParent;
@property (nonatomic,retain) NSString *UseMPOS;

//add for auto bookport, by StevenNguyen
@property (nonatomic) BOOL autoBookPort;
@property (nonatomic) NSInteger autoBookPort_RetryConnect;
@property (nonatomic) NSInteger autoBookPort_Timeout;
@property (nonatomic) NSInteger autoBookPort_iR;
@property (nonatomic,retain) NSArray *arrTypePortOfSale;

// add by DanTT 2015.10.08
@property (nonatomic,retain) NSString *sessionID;
@property (nonatomic,retain) NSString *token;

@property (nonatomic) int IsCreateContract;

@property (nonatomic,retain) NSArray *arrDepartment;

@end
