//
//  SelectedExpectedDateViewController.h
//  TicketMobile
//
//  Created by ISC-DanTT on 7/31/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@protocol SelectedExpectedDateViewControllerDelegate <NSObject>

//-(void) CancelPopup:(NSString *) stringExpectedDate isUpdateExpectedDate: (BOOL)isUpdateExpectedDate;
-(void)CancelPopup:(NSString *)stringExpectedDate expectedDateUpdate:(NSString *)expectedDateUpdate isUpdateExpectedDate:(BOOL)isUpdateExpectedDate;
@end

@interface SelectedExpectedDateViewController : BaseViewController <UIActionSheetDelegate>
@property (strong, nonatomic) NSString *ticketId;
@property (strong, nonatomic) NSString *expectedDate;
@property (strong, nonatomic) NSString *chooseExpectedDate;
@property BOOL isUpdateExpectedDate;
@property (nonatomic, assign) id <SelectedExpectedDateViewControllerDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIButton *btnExpectedDate;
@property (strong, nonatomic) IBOutlet UIButton *btnExpectedTime;

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSDate *createDate;


@end
