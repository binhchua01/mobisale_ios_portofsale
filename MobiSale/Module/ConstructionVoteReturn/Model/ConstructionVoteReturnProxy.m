//
//  ConstructionVoteReturnProxy.m
//  MobiSale
//
//  Created by ISC on 6/27/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ConstructionVoteReturnProxy.h"
#import "ShareData.h"
#import "ConstructionVoteReturnModel.h"//
#import "FPTUtility.h"

#define mGetPTCReturnList            @"GetPTCReturnList"
#define mChangePTCStatusForPTCReturn @"ChangePTCStatusForPTCReturn"

@implementation ConstructionVoteReturnProxy

#pragma mark - Lấy danh sách Phiếu thi công trả về - API 69
- (void)getPTCReturnList:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetPTCReturnList)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetPTCReturnList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}

- (void)endGetPTCReturnList:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    NSString *error = StringFormat(@"%@",[dict objectForKey:@"Error"]);
    NSString *errorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]);
    
    if(![error isEqualToString:@"<null>"]){
        handler(nil, errorCode, error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    
    if(arr.count <= 0){
        handler(nil, errorCode, error);
        return;
    }
    
    NSMutableArray *arrData = [NSMutableArray array];
    
    for (NSDictionary *dict in arr) {
        ConstructionVoteReturnModel *model = [ConstructionVoteReturnModel constructionVoteReturnModelWithDict:dict];
        [arrData addObject:model];
    }
    
    
    
    handler(arrData, errorCode, error);
    
    return;
    
}

#pragma mark - Chuyển tình trạng PTC trả về - API 70

- (void)changePTCStatusForPTCReturn:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mChangePTCStatusForPTCReturn)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endChangePTCStatusForPTCReturn:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}

- (void)endChangePTCStatusForPTCReturn:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    NSString *error = StringFormat(@"%@",[dict objectForKey:@"Error"]);
    
    if(![error isEqualToString:@"<null>"]){
        handler(nil, error, error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    
    if(arr.count <= 0){
        handler(nil, error, error);
        return;
    }
    
    NSString *resultID  = [arr[0] objectForKey:@"ResultID"];
    NSString *resultStr = [arr[0] objectForKey:@"Result"];
    
    handler(arr, resultID, resultStr);
    
    return;
    
}

@end
