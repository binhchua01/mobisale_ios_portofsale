//
//  ConstructionVoteReturnModel.h
//  MobiSale
//
//  Created by ISC on 6/27/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConstructionVoteReturnModel : NSObject

@property (nonatomic, assign) NSInteger PTCId;          // ID Phiếu thi công (PTC)
@property (nonatomic, assign) NSInteger returnTimes;    // Số lần PTC trả về
@property (nonatomic, assign) NSInteger currentPage;    // Trang lấy dữ liệu hiện tại
@property (nonatomic, assign) NSInteger rowNumber;      // Số dữ liệu của trang hiện tại
@property (nonatomic, assign) NSInteger totalPage;      // Tổng số trang dữ liệu
@property (nonatomic, assign) NSInteger totalRow;       // Tổng số dữ liệu
@property (nonatomic, copy)   NSString  *reason;        // Lý do trả về
@property (nonatomic, copy)   NSString  *desc;          // Ghi chú trả về
@property (nonatomic, copy)   NSString  *PTCStatusName; // Tên tình trạng PTC
@property (nonatomic, copy)   NSString  *contract;      // Số hợp đồng
@property (nonatomic, copy)   NSString  *fullName;      // Họ tên khách hàng
@property (nonatomic, copy)   NSString  *address;       // Địa chỉ khách hàng
@property (nonatomic, copy)   NSString  *emailList;     // Danh sách Địa chỉ email nhân viên liên quan
@property (nonatomic, copy)   NSString  *returnDate;    // Ngày PTC trả về

+ (id)constructionVoteReturnModelWithDict:(NSDictionary *)dict;

@end
