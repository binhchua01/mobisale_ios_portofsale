//
//  ConstructionVoteReturnProxy.h
//  MobiSale
//
//  Created by ISC on 6/27/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseProxy.h"

@interface ConstructionVoteReturnProxy : BaseProxy

// Lấy danh sách Phiếu thi công trả về - API 69
- (void)getPTCReturnList:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Chuyển tình trạng PTC trả về - API 70
- (void)changePTCStatusForPTCReturn:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

@end
