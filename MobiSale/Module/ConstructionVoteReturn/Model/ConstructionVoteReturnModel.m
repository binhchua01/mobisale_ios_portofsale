//
//  ConstructionVoteReturnModel.m
//  MobiSale
//
//  Created by ISC on 6/27/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ConstructionVoteReturnModel.h"

@implementation ConstructionVoteReturnModel

+ (id)constructionVoteReturnModelWithDict:(NSDictionary *)dict {
    ConstructionVoteReturnModel *model = [[ConstructionVoteReturnModel alloc] init];
    
    model.PTCId       = [dict[@"PTCId"] integerValue];
    model.currentPage = [dict[@"CurrentPage"]   integerValue];
    model.rowNumber   = [dict[@"RowNumber"] integerValue];
    model.totalRow    = [dict[@"TotalRow"]   integerValue];
    model.totalPage   = [dict[@"TotalPage"] integerValue];
    model.returnTimes = [dict[@"ReturnTimes"]   integerValue];
    model.returnDate  = StringFormat(@"%@",dict[@"ReturnDate"]);
    model.reason      = StringFormat(@"%@",dict[@"Reason"]);
    model.desc        = StringFormat(@"%@",dict[@"Description"]);
    model.PTCStatusName = StringFormat(@"%@",dict[@"PTCStatusName"]);
    model.contract      = StringFormat(@"%@",dict[@"Contract"]);
    model.fullName      = StringFormat(@"%@",dict[@"FullName"]);
    model.emailList     = StringFormat(@"%@",dict[@"EmailList"]);
    model.address       = StringFormat(@"%@",dict[@"Address"]);
    
    return model;
}

@end
