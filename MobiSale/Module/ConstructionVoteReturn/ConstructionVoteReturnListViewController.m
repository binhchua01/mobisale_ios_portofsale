//
//  ConstructionVoteReturnListViewController.m
//  MobiSale
//
//  Created by ISC on 6/27/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ConstructionVoteReturnListViewController.h"
#import "ConstructionVoteReturnModel.h"
#import "DemoTableFooterView.h"
#import "ShareData.h"

@implementation ConstructionVoteReturnListViewController {
    ConstructionVoteReturnModel *model;
    
    DemoTableFooterView *tableFooterView; // show view load more when scroll table to bottom
    
    NSMutableArray *arrData;
    
    NSInteger           totalPage;
    NSInteger           currentPage;
    CGFloat             startOffset;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = self.screenName = @"DS PHIẾU THI CÔNG TRẢ VỀ";
    
    [self.lblStatusData setHidden:YES];

    // set offset scroll view of table
    startOffset = -20;
    
    // Init data
    arrData = [NSMutableArray array];
    
    /*
     *set the custom view for "load more". See DemoTableFooterView.xib.
     */
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DemoTableFooterView" owner:self options:nil];
    tableFooterView = (DemoTableFooterView *)[nib objectAtIndex:0];
    
    // add refresh control when scroll top of tableview
    [self.listTableView addSubview:[self setRefreshControlWithTitle:@"Kéo thả để tải lại dữ liệu..."]];
    
    // Get data
    [self getPTCReturnListWithPageNumber:@"1"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - set type for Refresh Control
- (UIRefreshControl *)setRefreshControlWithTitle:(NSString *)title {
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    
    refreshControl.tintColor = [UIColor colorMain];
    
    NSMutableAttributedString *titleAttString =  [[NSMutableAttributedString alloc] initWithString:title];
    
    [titleAttString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica" size:14.0f] range:NSMakeRange(0, [title length])];
    
    [titleAttString addAttribute:NSForegroundColorAttributeName value:[UIColor colorMain] range:NSMakeRange(0, [title length])];
    
    refreshControl.attributedTitle = titleAttString;
    
    return refreshControl;
}

#pragma refresh table view
- (void)refresh:(UIRefreshControl *)refreshControl {
    [arrData removeAllObjects];
    // get data
    [self getPTCReturnListWithPageNumber:@"1"];
    [refreshControl endRefreshing];
}

#pragma mark - Lấy danh sách phiếu thi công trả về
- (void)getPTCReturnListWithPageNumber:(NSString *)pageNumber {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    [dict setObject:pageNumber forKey:@"PageNumber"]; // Trang lấy dữ liệu

//    [dict setObject:@"0" forKey:@"Agent"];      // Loại tìm kiếm (defult = 0)
//    [dict setObject:@"" forKey:@"AgentName"];   // Giá trị cần tìm kiếm (defult = "")

    [self showMBProcess];
    
    [shared.constructionVoteReturnProxy getPTCReturnList:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        
        if(![errorCode isEqualToString:@"0"] || result == nil) {
            [self showAlertBoxWithDelayDismiss:@"Thông báo" message:@"Không có kết quả trả về"];
            [self.lblStatusData setHidden:NO];
            self.listTableView.hidden = YES;
            [self hideMBProcess];
            [self.navigationController popViewControllerAnimated:YES];
            return ;
        }
        
        [self.lblStatusData setHidden:YES];

        NSArray *arr = result;
        [arrData addObjectsFromArray:arr];
        model = arrData[0];
        currentPage = model.currentPage;
        totalPage   = model.totalPage;
        
        self.listTableView.hidden = NO;
        [self.listTableView reloadData];
        [self loadMoreCompleted];
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
        [self hideMBProcess];
    }] ;
}

#pragma mark - Chuyển tình trạng phiếu thi công bị trả về
- (void)changePTCStatusForPTCReturnWithPTCId:(NSString *)PTCId withContract:(NSString *)contract andStatus:(NSString *)status {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    [dict setObject:contract forKey:@"Contract"];
    [dict setObject:PTCId forKey:@"PTCId"];
    [dict setObject:status forKey:@"Status"];
    
    [self showMBProcess];
    
    [shared.constructionVoteReturnProxy changePTCStatusForPTCReturn:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        
        if( result == nil) {
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",@"Không có kết quả trả về"]];
            [self hideMBProcess];
            
            return ;
        }
        
        [self showAlertBoxWithDelayDismiss:@"Thông báo" message:message];
        [self hideMBProcess];

        if ([errorCode intValue] == 1) {
            [arrData removeAllObjects];
            [self getPTCReturnListWithPageNumber:@"1"];
        }
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
        [self hideMBProcess];
    }] ;
}

#pragma  mark - set chiều cao cua label tự động theo chiều dài text
- (CGFloat)setAutoHeightForLabelWithText:(NSString *)text {
    
    CGSize constraint = CGSizeMake(186, 20000.0f);
    UIFont *font = [UIFont fontWithName:@"Helvetica-Light" size:15];
    
    // constratins the size of the table row according to the text
    CGRect textRect = [text boundingRectWithSize:constraint options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{NSFontAttributeName:font} context:nil];
    
    if (textRect.size.height > 21) {
        return textRect.size.height - 21;
    }
    
    return 0;

}

#pragma mark - TableView methods Delegate and DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return arrData.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    model = arrData[indexPath.section];
 
    NSArray *arr = [NSArray arrayWithObjects:model.fullName, model.address, model.reason, model.desc, model.emailList, nil];
    
    CGFloat height = 0;
    
    for ( NSString *text in arr) {
        height += [self setAutoHeightForLabelWithText:text];
    }

    return 339 + height;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor colorMain];
    return headerView;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"ConstructionVoteReturnCellID";
    ConstructionVoteReturnTableViewCell *cell = (ConstructionVoteReturnTableViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (!cell) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ConstructionVoteReturnTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    if ( arrData.count > 0) {
        cell.model = arrData[indexPath.section];
        cell.delegate = self;

    }
    return cell;
}

#pragma mark - TableView didSelected Cell

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}


#pragma mark - Scroll in table view delegate
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    CGFloat currentOffset = scrollView.contentOffset.y;
    
    startOffset = currentOffset;
    
    CGFloat maximumOffset = fabs(scrollView.contentSize.height - scrollView.frame.size.height);
    if (maximumOffset - currentOffset < 0) {
        [self loadMore];
    }
}

#pragma mark - Load More
//
// The method -loadMore was called and will begin fetching data for the next page (more).
// Do custom handling of -footerView if you need to.
//
- (void) willBeginLoadingMore {
    [tableFooterView.activityIndicator startAnimating];
}

// Do UI handling after the "load more" process was completed. In this example, -footerView will
// show a "No more items to load" text.
//
- (void)loadMoreCompleted {
    [tableFooterView.activityIndicator stopAnimating];
}

- (void)loadMore {
    // Do your async loading here
    self.listTableView.tableFooterView = tableFooterView;
    currentPage ++;
    if (currentPage <= totalPage) {
        [self willBeginLoadingMore];
        [self getPTCReturnListWithPageNumber:StringFormat(@"%li",(long)currentPage)];
        return;
    }
    tableFooterView.infoLabel.hidden = NO;
}

@end
