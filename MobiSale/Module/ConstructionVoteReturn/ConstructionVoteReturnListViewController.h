//
//  ConstructionVoteReturnListViewController.h
//  MobiSale
//
//  Created by ISC on 6/27/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ConstructionVoteReturnTableViewCell.h"

@interface ConstructionVoteReturnListViewController :BaseViewController<ConstructionVoteReturnCellDelegate>
@property (weak, nonatomic) IBOutlet UITableView *listTableView;
@property (weak, nonatomic) IBOutlet UILabel *lblStatusData;

@end
