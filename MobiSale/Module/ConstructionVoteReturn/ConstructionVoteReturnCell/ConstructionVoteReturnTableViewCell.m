//
//  ConstructionVoteReturnTableViewCell.m
//  MobiSale
//
//  Created by ISC on 6/27/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ConstructionVoteReturnTableViewCell.h"
#import "UIButton+FPTCustom.h"

@implementation ConstructionVoteReturnTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self.btnChangePTC styleButtonUpdate];
    [self.btnChangePTC setShadow];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setModel:(ConstructionVoteReturnModel *)model {
    _model = model;
    
    self.lblFullName.text = model.fullName;
    self.lblContract.text = model.contract;
    self.lblAddress.text  = model.address;
    self.lblPTCStatus.text = model.PTCStatusName;
    self.lblReason.text   = model.reason;
    self.lblDescription.text = model.desc;
    self.lblReturnTimes.text = StringFormat(@"%li", (long)model.returnTimes);
    self.lblReturnDate.text = model.returnDate;
    self.lblEmailList.text = model.emailList;
    
    [self setAutoLayout];
}

#pragma mark - UIActionSheetDelegate methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag == 1) {
        if (buttonIndex == 0) {
            if (self.delegate) {
                [self.delegate changePTCStatusForPTCReturnWithPTCId:StringFormat(@"%li", (long)_model.PTCId) withContract:_model.contract andStatus:@"0"];
            }
            return;
        }
    }
}

- (void)showActionSheetWithTitle:(NSString *)title andTag:(NSInteger)tag {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:title delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
    actionSheet.tag = tag;
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}

- (IBAction)btnChangePTCStatus_Clicked:(id)sender {
    NSString *title = StringFormat(@"Bạn có muốn chuyển lại Phiếu thi công của hợp đồng: %@", _model.contract);
    [self showActionSheetWithTitle:title andTag:1];
}

- (void)setAutoLayout {
    [self.lblFullName sizeToFit];
    [self.lblAddress sizeToFit];
    [self.lblPTCStatus sizeToFit];
    [self.lblReason sizeToFit];
    [self.lblDescription sizeToFit];
    [self.lblReturnDate sizeToFit];
    [self.lblEmailList sizeToFit];
}

@end
