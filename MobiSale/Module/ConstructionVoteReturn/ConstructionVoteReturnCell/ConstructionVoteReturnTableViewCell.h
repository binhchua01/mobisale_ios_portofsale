//
//  ConstructionVoteReturnTableViewCell.h
//  MobiSale
//
//  Created by ISC on 6/27/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConstructionVoteReturnModel.h"

@protocol ConstructionVoteReturnCellDelegate <NSObject>

- (void)changePTCStatusForPTCReturnWithPTCId:(NSString *)PTCId withContract:(NSString *)contract andStatus:(NSString *)status;
@end

@interface ConstructionVoteReturnTableViewCell : UITableViewCell<UIActionSheetDelegate>

@property (strong, nonatomic) ConstructionVoteReturnModel *model;
@property (strong, nonatomic) id<ConstructionVoteReturnCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *lblFullName;
@property (weak, nonatomic) IBOutlet UILabel *lblContract;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblPTCStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblReason;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblReturnTimes;
@property (weak, nonatomic) IBOutlet UILabel *lblReturnDate;
@property (weak, nonatomic) IBOutlet UILabel *lblEmailList;

@property (weak, nonatomic) IBOutlet UIButton *btnChangePTC;

@end
