//
//  GuidController.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 1/24/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BaseViewController.h"

@protocol GuidDelegate <NSObject>

-(void)CancelPopupGuid;

@end


@interface GuidController : BaseViewController

-(IBAction)btnCancel:(id)sender;

@property (retain, nonatomic) id<GuidDelegate>delegate;

@end
