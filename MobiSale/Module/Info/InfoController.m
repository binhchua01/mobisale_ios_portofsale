//
//  InfoController.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 1/24/15.
//  Copyright (c) 2015 FPT.RAD.MOBISALE. All rights reserved.
//

#import "InfoController.h"
#import "../../Helper/OpenUDID/OpenUDID.h"
#import "../../Helper/GoogleAnalytics/GAITrackedViewController.h"
#import "PasteboardLabel.h"

@interface InfoController ()
@end

@implementation InfoController {
    PasteboardLabel *label;
}

@synthesize isLogin;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.screenName=@"THÔNG TIN ỨNG DỤNG";
    
    NSString *appVersion = [NSString stringWithFormat:@"%@",[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    [self.btnVersion setTitle:appVersion forState:UIControlStateNormal];
    
    label = [[PasteboardLabel alloc] initWithFrame:self.lblImie.frame];
    [label setFont:[UIFont systemFontOfSize:13]];
    [label setTextAlignment:NSTextAlignmentRight];
    [label setNumberOfLines:2];
    label.text = [OpenUDID value];
    label.userInteractionEnabled = YES;
    [self.view addSubview:label];
    UIGestureRecognizer *gestureRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPressGestureRecognizer:)];
    [label addGestureRecognizer:gestureRecognizer];
    
    UITapGestureRecognizer *tapSreen = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnscreen:)];
    [self.view addGestureRecognizer:tapSreen];
    
}

-(void)viewDidAppear:(BOOL)animated{
    float pointY;
    if(IS_IPHONE5){
        pointY = 50;
    }else {
        pointY = 50;
    }
    
    self.view.frame =CGRectMake(0, 30, 320,[SiUtils getScreenFrameSize].height);
    self.lblCopy.frame = CGRectMake(0, [SiUtils getScreenFrameSize].height - pointY, self.lblCopy.frame.size.width, self.lblCopy.frame.size.height);
    //self.lblImie.text = [OpenUDID value];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btnGuide_Clicked:(id)sender {
    
    //http://bit.ly/mobisaleguide hoặc https://mobisaleguide.fpt.vn
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://bit.ly/mobisaleguide"]];
    
}

-(IBAction)btnCancel:(id)sender {
    if(self.delegate){
        [self.delegate CancelPopupInfo];
    }
}

- (IBAction)btnVersion_Clicked:(id)sender {
    if (isLogin == YES) {
        return;
    }
    if (self.delegate) {
        [self.delegate CancelPopupInfo];
        [self.delegate reCheckVersion];
    }
}

#pragma mark - Gestures
- (void)tapOnscreen: (UIGestureRecognizer *)recognizer {
    [label setTextColor:[UIColor blackColor]];
}

- (void)longPressGestureRecognizer:(UIGestureRecognizer *)recognizer {
    [label setTextColor:[UIColor grayColor]];
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    [menuController setTargetRect:recognizer.view.frame inView:recognizer.view.superview];
    [menuController setMenuVisible:YES animated:YES];
    [recognizer.view becomeFirstResponder];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
