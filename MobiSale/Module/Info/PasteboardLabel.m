//
//  PasteboardLabel.m
//  MobiSale
//
//  Created by ISC-DanTT on 2/24/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "PasteboardLabel.h"

@implementation PasteboardLabel

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    return (action == @selector(copy:));
}

#pragma mark - UIResponderStandardEditActions
- (void)copy:(id)sender {
    UIPasteboard *generalPasteboard = [UIPasteboard generalPasteboard];
    generalPasteboard.string = self.text;
    [self setTextColor:[UIColor blackColor]];
}

@end
