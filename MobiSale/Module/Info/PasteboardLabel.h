//
//  PasteboardLabel.h
//  MobiSale
//
//  Created by ISC-DanTT on 2/24/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PasteboardLabel : UILabel

@end
