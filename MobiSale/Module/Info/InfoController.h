//
//  InfoController.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 1/24/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BaseViewController.h"
#import "PasteboardLabel.h"

@protocol InfoDelegate <NSObject>

- (void)CancelPopupInfo;

@optional
- (void)reCheckVersion;

@end

@interface InfoController : BaseViewController

-(IBAction)btnCancel:(id)sender;
- (IBAction)btnVersion_Clicked:(id)sender;

@property (retain, nonatomic) id<InfoDelegate>delegate;

@property (weak, nonatomic) IBOutlet UIButton *btnVersion;
@property (weak, nonatomic) IBOutlet UILabel *lblCopy;
@property (weak, nonatomic) IBOutlet UILabel *lblImie;

@property (assign, nonatomic) BOOL isLogin;

@end
