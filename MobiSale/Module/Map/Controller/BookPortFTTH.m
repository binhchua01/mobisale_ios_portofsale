//
//  BookPortFTTH.m
//  MobiSale
//
//  Created by  Nguyen Tan Tho on 2/5/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BookPortFTTH.h"
#import "KeyValueModel.h"
#import "UIPopoverListView.h"
#import "ShareData.h"
#import "LocationBranchPopRecord.h"
#import "PortFreeRecord.h"
#import "Common_client.h"
#import "DetailRegisteredForm.h"

@interface BookPortFTTH ()<UIAlertViewDelegate>

@end

@implementation BookPortFTTH{
    KeyValueModel *selectedLocation, *selectedBranch, *selectedServiceType, *selectedPop, *selectedTD, *selectedType;
    PortFreeRecord *selectedPortTD, *selectedPortSC;
    NSString *TDID;
    LocationBranchPopRecord *rc;
    
    //add by DanTT2
    UIAlertView *noticeError;

}

@synthesize arrPortSC;
@synthesize arrBranch;
@synthesize arrLocation;
@synthesize arrPop;
@synthesize arrPortTD;
@synthesize arrTD;
@synthesize arrType;
@synthesize arrTypeService;

enum tableSourceStype {
    Location = 1,
    Branch = 2,
    ServiceType = 3,
    Pop= 4,
    TD= 5,
    Type= 6,
    PortTD= 7,
    PortSC = 8,
};
    

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"BOOK PORT FTTH";
    // Do any additional setup after loading the view.
    //self.txtNumber.text = @"1";
    self.txtRegistNumber.text =self.Regcode;
    self.txtTD.text = self.TdName;
    [self LoadLocation];
    [self LoadTypeService];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated {
    self.view.frame = CGRectMake(0, 30, self.view.frame.size.width,  self.view.frame.size.height);
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

#pragma mark - DropDown view

- (void)setupDropDownView:(NSInteger)tag {
    
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.tag = tag;
    poplistview.listView.scrollEnabled = TRUE;
    switch (tag) {
        case Location:
            [poplistview setTitle:NSLocalizedString(@"Chọn vùng miền", @"")];
            break;
        case Branch:
            [poplistview setTitle:NSLocalizedString(@"Chọn chi nhánh", @"")];
            break;
        case ServiceType:
            [poplistview setTitle:NSLocalizedString(@"Chọn loại dịch vụ", @"")];
            break;
        case Pop:
            [poplistview setTitle:NSLocalizedString(@"Chọn Pop", @"")];
            break;
        case TD:
            [poplistview setTitle:NSLocalizedString(@"Chọn tập điểm", @"")];
            break;
        case Type:
            [poplistview setTitle:NSLocalizedString(@"Chọn loại", @"")];
            break;
        case PortTD:
            [poplistview setTitle:NSLocalizedString(@"Chọn port tập điểm", @"")];
            break;
        case PortSC:
            [poplistview setTitle:NSLocalizedString(@"Chọn port SC", @"")];
            break;
        default:
            break;
    }
    
    [poplistview show];
    
}
- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35.0f;
}

#pragma mark - UIPopoverListViewDataSource

- (void) setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell tag:(NSInteger)tag{
    KeyValueModel *model;
    PortFreeRecord *port;
    switch (tag) {
        case Location:
            model = [self.arrLocation objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Branch:
            model = [self.arrBranch objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case ServiceType:
            model = [self.arrTypeService objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Pop:
            model = [self.arrPop objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case TD:
            model = [self.arrTD objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Type:
            model = [self.arrType objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case PortTD:
            port = [self.arrPortTD objectAtIndex:row];
            cell.textLabel.text = port.PortID;
            break;
        case PortSC:
            port = [self.arrPortSC objectAtIndex:row];
            cell.textLabel.text = port.PortID;
            break;
        default:
            break;
    }
    
    
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row cell:cell tag:popoverListView.tag];
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    switch (popoverListView.tag) {

        case Location:
            count = self.arrLocation.count;
            break;
        case Branch:
            count = self.arrBranch.count;
            break;
        case ServiceType:
            count = self.arrTypeService.count;
            break;
        case Pop:
            count = self.arrPop.count;
            break;
        case TD:
           count = self.arrTD.count;
            break;
        case Type:
            count = self.arrType.count;
            break;
        case PortTD:
            count = self.arrPortTD.count;
            break;
        case PortSC:
            count = self.arrPortSC.count;
            break;

        default:
            break;
    }
    return count;
}

#pragma mark - UIPopoverListViewDelegate

- (void) didDropDownSelected:(NSInteger)row tag:(NSInteger)tag{
    
    switch (tag) {
        case ServiceType:
            selectedServiceType = [self.arrTypeService objectAtIndex:row];
            [self.btnService setTitle:selectedServiceType.Values forState:UIControlStateNormal];
            [self LoadPortSC:rc.PopName];
            break;
        case Type:
            selectedType = [self.arrType objectAtIndex:row];
            [self.btnType setTitle:selectedType.Values forState:UIControlStateNormal];
            break;
        case PortTD:
            selectedPortTD = [self.arrPortTD objectAtIndex:row];
            [self.btnPortTD setTitle:selectedPortTD.PortID forState:UIControlStateNormal];
            break;
        case PortSC:
            selectedPortSC = [self.arrPortSC objectAtIndex:row];
            [self.btnPortSC setTitle:selectedPortSC.PortID forState:UIControlStateNormal];
            break;
        default:
            break;
    }
}


#pragma mark - UIPopoverListViewDelegate

- (void)popoverListView:(UIPopoverListView *)popoverListView
     didSelectIndexPath:(NSIndexPath *)indexPath
{
    [self didDropDownSelected:indexPath.row tag:popoverListView.tag];
    
}

#pragma  mark - event button
-(IBAction)btnLocation_clicked:(id)sender{
    [self setupDropDownView:Location];
}
-(IBAction)btnBrank_clicked:(id)sender{
    [self setupDropDownView:Branch];
}
-(IBAction)btnService_clicked:(id)sender{
    [self setupDropDownView:ServiceType];
}
-(IBAction)btnPop_clicked:(id)sender{
    [self setupDropDownView:Pop];
}
-(IBAction)btnTD_clicked:(id)sender{
    [self setupDropDownView:TD];
}
-(IBAction)btnType_clicked:(id)sender{
    [self setupDropDownView:Type];
}
-(IBAction)btnPortTD_clicked:(id)sender{
    [self setupDropDownView:PortTD];
}
-(IBAction)btnPortSC_clicked:(id)sender{
    [self setupDropDownView:PortSC];
}

-(IBAction)btnUpdate_clicked:(id)sender {
    if(rc == nil){
        [self showAlertBox:@"Thông Báo" message:@"Dữ liệu không hợp lệ"];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    if(![shared.currentUser.PortLocationID isEqualToString:rc.LocationID]){
        [self showAlertBox:@"Thông Báo" message:@"Tài khoản không có quyền book port trong tỉnh thành này"];
        return;
    }
    if(selectedPortTD.PortID == nil){
        [self showAlertBox:@"Thông Báo" message:@"Không có port TĐ trống nên không thể book port"];
        return;
    }
    
    //add by DanTT 2
    UIActionSheet *noticeSave = [[UIActionSheet alloc] initWithTitle:@"Bạn chắc chắn muốn book port?" delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
    noticeSave.tag = 0;
    [noticeSave showInView:[UIApplication sharedApplication].keyWindow];
    
}

#pragma mark - event click button
-(IBAction)btnCancel_clicked:(id)sender {
    if(self.delegate){
        [self.delegate cancelPopupBookPortFTTH];
    }
}

-(IBAction)btnRecovery_clicked:(id)sender {
    //add by DanTT 2
    UIActionSheet *noticeSave = [[UIActionSheet alloc] initWithTitle:@"Bạn chắc chắn muốn thu hồi port?" delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
    noticeSave.tag = 1;
    [noticeSave showInView:[UIApplication sharedApplication].keyWindow];

}

- (void)bookport {
    [self showMBProcess];
    ShareData *shared = [ShareData instance];
    [shared.mapproxy BookPortFTTH:self.Regcode CardNumber:self.txtNumber.text UserName:shared.currentUser.userName TDID:rc.TDID IDCoreSC:selectedPortSC.ID IDCoreTD:selectedPortTD.ID TDName:self.TdName PortTextTD:selectedPortTD.PortID LocationID:rc.LocationID BranchID:rc.BranchID IP:[self getIPAddress] TypeCore:selectedType.Key TypeContract:selectedServiceType.Key Latlng:self.Latlng LatlngDevice:(NSString *)self.latlngDevice Completehander:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            return ;
        }
        [self showAlertBoxWithDelayDismiss:@"Thông Báo" message:message];
        [self.delegate cancelPopupBookPortFTTH];
        if ([message isEqualToString:@"Bookport thành công."]) {
            [self.delegate backToDetailRegisteredForm];
        }
        //[self LoadLocation];
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}

- (void)recovery {
    //add by DanTT 2
    NSString *tempType = [NSString stringWithFormat:@"%li", (long)[self.Type integerValue] + 1];
    
    [self showMBProcess];
    ShareData *shared = [ShareData instance];
    // thay self.Type = tempType;
    [shared.mapproxy RecoverRegistration:tempType RegCode:self.Regcode CreateBy:shared.currentUser.userName IP:@"" Completehander:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        [self showAlertBoxWithDelayDismiss:@"Thông Báo" message:message];
        [self LoadPortSC:rc.PopName];
        
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}

#pragma mark - Load data
-(void)LoadLocation {
    [self showMBProcess];
    ShareData * shared = [ShareData instance];
    [shared.mapproxy GetLocationBranchPOP:self.TdName Type:self.Type Completehander:^(id result, NSString *errorCode, NSString *message) {
        // add by dantt2
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        
        if (result == nil) {
            [self showAlertErrorBox:@"Thông báo" message:message];
            [self hideMBProcess];
            return;
        }
        
        if(![errorCode isEqualToString:@"<null>"]){
            // add by DanTT2
            [self showAlertErrorBox:@"Thông báo" message:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }

        rc = [result objectAtIndex:0];
        if(rc != nil){
            self.txtLocation.text = rc.LocationName;
            self.txtBranch.text = rc.NameBranch;
            self.txtPop.text = rc.PopName;
            TDID = rc.TDID;
            [self LoadPortSC:rc.PopName];
            //[self LoadType];
        }else {
            [self ShowAlertBoxEmpty];
            [self hideMBProcess];
            if(self.delegate){
                [self.delegate cancelPopupBookPortFTTH];
            }
        }
    } errorHandler:^(NSError *error) {
        // add by DanTT2
        [self showAlertErrorBox:@"Thông báo" message:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}

-(void)LoadType {
    if([rc.LocationID isEqualToString:@"36"] ){
        ShareData *shared = [ShareData instance];
        [shared.mapproxy GetPercenTDFTTH:self.TdName Completehander:^(id result, NSString *errorCode, NSString *message) {
            // add by dantt2
            if ([message isEqualToString:@"het phien lam viec"]) {
                [self ShowAlertErrorSession];
                [self LogOut];
                [self hideMBProcess];
                return;
            }
            
            [self LoadPortTD:rc.TDID];
            
            if (result == nil) {
                [self hideMBProcess];
                return;
            }
            
            if(![errorCode isEqualToString:@"<null>"]){
                // add by DanTT2
                [self showAlertErrorBox:@"Thông báo" message:[NSString stringWithFormat:@"%@",message]];
                [self hideMBProcess];
                return ;
            }
            
            NSString *Result = StringFormat(@"%@", [[result objectAtIndex:0] objectForKey:@"Result"]?:nil);
            if([Result intValue] >= 46){
                KeyValueModel *s1 = [[KeyValueModel alloc]initWithName:@"2" description:@"1 Core"];
                NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s1,nil];
                self.arrType = lst;
                
                selectedType= [self.arrType objectAtIndex:0];
                [self.btnType setTitle:selectedType.Values forState:UIControlStateNormal];
            }else {
                KeyValueModel *s1 = [[KeyValueModel alloc]initWithName:@"1" description:@"2 Core"];
                NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s1,nil];
                self.arrType = lst;
                
                selectedType = [self.arrType objectAtIndex:0];
                [self.btnType setTitle:selectedType.Values forState:UIControlStateNormal];
                
            }
//            [self LoadPortTD:rc.TDID];
            
        } errorHandler:^(NSError *error) {
            // add by DanTT2
            [self showAlertErrorBox:@"Thông báo" message:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
            [self hideMBProcess];

        }];

    }else {
        KeyValueModel *s1 = [[KeyValueModel alloc]initWithName:@"2" description:@"1 Core"];
        KeyValueModel *s2 = [[KeyValueModel alloc]initWithName:@"1" description:@"2 Core"];
        NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s1,s2,nil];
        self.arrType = lst;
        
        selectedType = [self.arrType objectAtIndex:0];
        [self.btnType setTitle:selectedType.Values forState:UIControlStateNormal];
        [self LoadPortTD:rc.TDID];
    }
}

-(void)LoadPortTD:(NSString *)IDTD {
    ShareData * shared = [ShareData instance];
    [shared.mapproxy GetPortFreeFTTH:IDTD Completehander:^(id result, NSString *errorCode, NSString *message) {
        
        //add by dantt2
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self hideMBProcess];
            [self LogOut];
            return;
        }
        if (result == nil) {
            [self hideMBProcess];
            [self.btnPortTD setEnabled:NO];
            return;
        }
        
        if(![errorCode isEqualToString:@"<null>"]){
            // add by DanTT2
            [self showAlertErrorBox:@"Thông báo" message:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        self.arrPortTD = result;
        if(self.arrPortTD.count > 0){
            selectedPortTD = [self.arrPortTD objectAtIndex:0];
            [self.btnPortTD setTitle:selectedPortTD.PortID forState:UIControlStateNormal];
            [self.btnPortTD setEnabled:NO];
            NSArray *arrStr = [selectedPortTD.PortID componentsSeparatedByString:@"-"];
            self.txtNumber.text = [arrStr objectAtIndex:0];
        }
        
        if (self.arrPortTD.count <= self.arrPortSC.count) {
            self.lblTotalPort.text =[NSString stringWithFormat:@"%d",(int)self.arrPortTD.count] ;
            [self hideMBProcess];
            return;
        }
        self.lblTotalPort.text =[NSString stringWithFormat:@"%d",(int)self.arrPortSC.count] ;
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        // add by DanTT2
        [self showAlertErrorBox:@"Thông báo" message:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];

    }];
}

-(void)LoadPortSC:(NSString *)popname {
    ShareData * shared = [ShareData instance];
    [shared.mapproxy GetPortFreeSwitchCardFTTH:popname Type:selectedServiceType.Key Completehander:^(id result, NSString *errorCode, NSString *message) {
        // add by DanTT2
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self hideMBProcess];
            [self LogOut];
            return;
        }
        [self LoadType];
        if (result == nil) {
            [self hideMBProcess];
            [self.btnPortSC setEnabled:NO];
            return;
        }
        
        if(![errorCode isEqualToString:@"<null>"]){
            [self showAlertErrorBox:@"Thông báo" message:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        self.arrPortSC = result;
        if(self.arrPortSC.count > 0){
            selectedPortSC = [self.arrPortSC objectAtIndex:0];
            [self.btnPortSC setTitle:selectedPortSC.PortID forState:UIControlStateNormal];
            [self.btnPortSC setEnabled:NO];
        }
        //[self LoadType];

        
    } errorHandler:^(NSError *error) {
        // add by DanTT2
        [self showAlertErrorBox:@"Thông báo" message:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];

    }];
}

#pragma mark - Load type search
-(void)LoadTypeService {
   
    KeyValueModel *s1 = [[KeyValueModel alloc]initWithName:@"1" description:@"FTTH"];
    KeyValueModel *s2 = [[KeyValueModel alloc]initWithName:@"2" description:@"Lease-line"];
    NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s1,s2,nil];
    self.arrTypeService = lst;
    
    selectedServiceType = [self.arrTypeService objectAtIndex:0];
    [self.btnService setTitle:selectedServiceType.Values forState:UIControlStateNormal];
}

#pragma mark - UI ALERT VIEW
// add by DanTT2
- (void)showAlertErrorBox:(NSString *)title
                  message:(NSString *)message {
    noticeError = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
    [noticeError show];
}

// add by DanTT2
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView == noticeError) {
        if(self.delegate){
            [self.delegate cancelPopupBookPortFTTH];
        }
    }
}

#pragma mark - UIActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag == 0 && buttonIndex == 0) {
        [self bookport];
        return;
    }
    
    if (actionSheet.tag == 1 && buttonIndex == 0) {
        [self recovery];
        return;
    }
}

@end
