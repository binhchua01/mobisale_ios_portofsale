//
//  MainMap.m
//  MobiSale
//
//  Created by HIEUPC on 1/26/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "MainMap.h"
#import "../../../Helper/UIDropDown/UIPopoverListView.h"
#import "KeyValueModel.h"
#import "../../../Helper/MJPopup/UIViewController+MJPopupViewController.h"
#import "MapRecord.h"

#import "ShareData.h"
#import "TichHenPoup.h"
#import "Common_client.h"
#import "DetailRegisteredForm.h"

@interface MainMap (){
    
    IBOutlet NSLayoutConstraint *constraintSpaceRight;
}

@end

@implementation MainMap{
    CLLocation *latlng;
    KeyValueModel *selectedType;
    NSString *lng, *PortFree, *lngMarker;
    CLLocationCoordinate2D position;
    NSString *lngCoordinate;
    NSString *type;
    int statusmenu;
    UIAlertView *alertView;
    
}
@synthesize mapView_;
@synthesize locationManager;
@synthesize record;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
    
    self.mapView_.delegate = self;
    [self loadMaps];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
//    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
//    locationManager.distanceFilter = 5;
    
    if(IS_OS_8_OR_LATER) {
        
        [self.locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
    
    [self.mView setFrame:CGRectMake(0,105,320,465)];
    if(IS_IPHONE4){
        [self.mView setFrame:CGRectMake(0,105,320,465 - 88)];
    }
    [self.view addSubview:self.mView];
    
    [self.mViewButton setFrame:CGRectMake(0,[SiUtils getScreenFrameSize].height - 30,self.mViewButton.frame.size.width,self.mViewButton.frame.size.height)];
    [self.view addSubview:self.mViewButton];
    [self LoadTypeSearch];
    //[self setType];
    statusmenu = 0;
    if(self.arrRecord.RegCode == nil|| [self.arrRecord.RegCode isEqualToString:@""]){
        [self.btnFind setEnabled:NO];
    }
    
    ShareData *sharedata =[ShareData instance];
    if (sharedata.tapDiem) {
        self.navigationItem.rightBarButtonItem = nil;
        self.title = @"TẬP ĐIỂM";
        [self.btnFind setHidden:YES];
        constraintSpaceRight.constant = 8;
        sharedata.tapDiem =false;
    }else{
        self.title = @"BOOK PORT";
    }
    self.screenName=@"BẢN ĐỒ BOOK PORT";
    self.btnRecovery.hidden = YES;
    if (_txtLocationPB.length >2) {
        self.btnRecovery.hidden = NO;
        self.txtTextSearch.text =_txtLocationPB;
        if ([_txtLocationPB rangeOfString:@"/"].location != NSNotFound) {
            selectedType = [self.arrList objectAtIndex:1];
            [self.btnType setTitle:selectedType.Values forState:UIControlStateNormal];
        }
        type = selectedType.Key;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma Load Map and location

- (void)loadMaps {
    [self showCurrentLocation];
    
    double latitude=latlng.coordinate.latitude;
    double longitude=latlng.coordinate.longitude;
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude
                                                            longitude:longitude
                                                                 zoom:15];
    if(!self.mapView_) {
        mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    }
    mapView_.delegate = self;
    mapView_.myLocationEnabled = YES;
    mapView_.settings.compassButton = YES;
    mapView_.settings.myLocationButton = YES;
    self.mView = mapView_;
    
}



- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    latlng = [locations lastObject];
    
    if([CLLocationManager locationServicesEnabled]){
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            [self showAlertBox:@"Thông báo" message:@"Vui lòng cấp phát quyền truy cập GPS cho ứng dụng bằng cách vào Cài đặt ->Quyền riềng tư ->Dịch vụ định vị-> MobiSale"];
            
            return;
        }
        
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latlng.coordinate.latitude longitude:latlng.coordinate.longitude zoom:15];
        [mapView_ animateToCameraPosition:camera];
        [self.locationManager stopUpdatingLocation];
        [self AddMarkerLocation:[NSString stringWithFormat:@"%f",latlng.coordinate.latitude] Longitude:[NSString stringWithFormat:@"%f",latlng.coordinate.longitude]];
        
        //vutt11 add parametter LatlngDevice Get lat and long from GPRS
        
        lng = [NSString stringWithFormat:@"(%@,%@)", [NSString stringWithFormat:@"%f",latlng.coordinate.latitude],[NSString stringWithFormat:@"%f",latlng.coordinate.longitude]];
        
    }
}

- (void)showCurrentLocation {
    mapView_.myLocationEnabled = YES;
    
    if(IS_OS_8_OR_LATER) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    
    [self.locationManager startUpdatingLocation];
    
    if([CLLocationManager locationServicesEnabled]){
        
        NSLog(@"Location Services Enabled");
        
        //vutt
        switch (CLLocationManager.authorizationStatus) {
            case kCLAuthorizationStatusDenied:
               alertView = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:@"Vui lòng chọn vị trí Khi dùng Ứng dụng để sử dụng bản đồ" delegate:self cancelButtonTitle:@"Đóng" otherButtonTitles:@"Cài đặt",nil];
                alertView.tag = 99;
                [alertView show];
                break;
           
            default:
                break;
        }
        
        
            }
    else{
        
       NSLog(@"Location Services DisEnabled");
                if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
                    
                   alertView = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:@"Vui lòng bật GPS để sử dụng bản đồ" delegate:self cancelButtonTitle:@"Đóng" otherButtonTitles:@"Cài đặt",nil];
                    alertView.tag = 99;
                    [alertView show];
                    
                    return;
                }

        
    }
    
}

#pragma mark - DropDown view

- (void)setupDropDownView {
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.listView.scrollEnabled = TRUE;
    [poplistview setTitle:NSLocalizedString(@"Chọn loại", @"")];
    [poplistview show];
    
}

- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 35.0f;
}

#pragma mark - UIPopoverListViewDataSource

- (void)setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell{
    KeyValueModel *model;
    model = [self.arrList objectAtIndex:row];
    cell.textLabel.text = model.Values;
    
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier];
    [self setUpDropDownCell:indexPath.row cell:cell];
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section {
    NSInteger count = self.arrList.count;
    return count;
}

#pragma mark - UIPopoverListViewDelegate

- (void) didDropDownSelected:(NSInteger)row {
    selectedType = [self.arrList objectAtIndex:row];
    [self.btnType setTitle:selectedType.Values forState:UIControlStateNormal];
}

//btnIPTV
#pragma mark - UIPopoverListViewDelegate
- (void)popoverListView:(UIPopoverListView *)popoverListView
     didSelectIndexPath:(NSIndexPath *)indexPath {
    [self didDropDownSelected:indexPath.row];
    
}

#pragma mark - Load type search
-(void)LoadTypeSearch {
    if(self.arrRecord.RegCode == nil|| [self.arrRecord.RegCode isEqualToString:@""]){
        KeyValueModel *s1 = [[KeyValueModel alloc]initWithName:@"2" description:@"FTTHNew"];
        KeyValueModel *s2 = [[KeyValueModel alloc]initWithName:@"1" description:@"FTTH"];
        KeyValueModel *s3 = [[KeyValueModel alloc]initWithName:@"0" description:@"ADSL"];
        NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s1,s2,s3,nil];
        self.arrList = lst;
        selectedType = [self.arrList objectAtIndex:0];
        [self.btnType setTitle:selectedType.Values forState:UIControlStateNormal];
        return;
        
    }
    if([self.arrRecord.BookPortType isEqualToString:@"0"]){
        KeyValueModel *s1 = [[KeyValueModel alloc]initWithName:@"0" description:@"ADSL"];
        KeyValueModel *s2 = [[KeyValueModel alloc]initWithName:@"2" description:@"FTTHNew"];
        NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s1,s2,nil];
        self.arrList = lst;
        selectedType = [self.arrList objectAtIndex:0];
        [self.btnType setTitle:selectedType.Values forState:UIControlStateNormal];
        return;
        
    }else{
        KeyValueModel *s1 = [[KeyValueModel alloc]initWithName:@"1" description:@"FTTH"];
        KeyValueModel *s2 = [[KeyValueModel alloc]initWithName:@"2" description:@"FTTHNew"];
        NSMutableArray *lst = [[NSMutableArray alloc] initWithObjects:s1,s2,nil];
        self.arrList = lst;
        
        selectedType = [self.arrList objectAtIndex:0];
        [self.btnType setTitle:selectedType.Values forState:UIControlStateNormal];
        return;
    }
    
}


#pragma mark - event button click
-(IBAction)btnFind_Clicked:(id)sender {
    [self.view endEditing:YES]; // hide keyboard if it showing
    
    if ([self.txtTextSearch.text isEqualToString:@""]) {
        [self showAlertBox:@"Thông Báo" message:@"Chưa có tập điểm"];
        return;
    }
    if([lngMarker isEqualToString:@"0"]){
        [self showAlertBox:@"Thông Báo" message:@"Không có port trống"];
        return;
    }
    if(![self.txtTextSearch.text isEqualToString:@""]) {
        if([selectedType.Key isEqualToString:@"0"]){
            BookPortADSL *vc = [[BookPortADSL alloc]initWithNibName:@"BookPortADSL" bundle:nil];
            vc.delegate = (id)self;
            vc.Regcode = self.arrRecord.RegCode;
            vc.Type = selectedType.Key;
            vc.TdName = self.txtTextSearch.text;
            vc.Latlng = lngCoordinate;
            vc.latlngDevice = lng;
            
            [self presentPopupViewController:vc animationType:1];
            return;
            
        }
        if([selectedType.Key isEqualToString:@"1"]) {
            BookPortFTTH *vc = [[BookPortFTTH alloc]initWithNibName:@"BookPortFTTTH" bundle:nil];
            vc.delegate = (id)self;
            vc.Regcode = self.arrRecord.RegCode;
            vc.Type = selectedType.Key;
            vc.TdName = self.txtTextSearch.text;
            vc.Latlng = lngCoordinate;
            vc.latlngDevice = lng;
            
            [self presentPopupViewController:vc animationType:1];
            return;
            
        }
        if([selectedType.Key isEqualToString:@"2"]) {
            BookPortFTTHNew *vc = [[BookPortFTTHNew alloc]initWithNibName:@"BookPortFTTHNew" bundle:nil];
            vc.delegate = (id)self;
            vc.Latlng = lngCoordinate;
            vc.Regcode = self.arrRecord.RegCode;
            vc.Type = selectedType.Key;
            vc.TdName = self.txtTextSearch.text;
            vc.ODCCableType = self.arrRecord.ObjectType;
            vc.latlngDevice = lng;
            
            [self presentPopupViewController:vc animationType:1];
            return;
            
        };
    }
    
}


-(IBAction)btnType_Clicked:(id)sender{
    [self.view endEditing:YES];
    [self setupDropDownView];
}

-(IBAction)btnBookPort_Clicked:(id)sender{
    self.Flat = @"1";
    [self.mViewMenu removeFromSuperview];
    statusmenu = 0;
    [self.view endEditing:YES];
}

- (IBAction)btnRecovery_Clicked:(id)sender {
    //add by DanTT 2
    UIActionSheet *noticeSave = [[UIActionSheet alloc] initWithTitle:@"Bạn chắc chắn muốn thu hồi port?" delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
    [noticeSave showInView:[UIApplication sharedApplication].keyWindow];
    
}

-(IBAction)btnSurvey_Clicked:(id)sender{
    self.Flat = @"2";
    [self.mViewMenu removeFromSuperview];
    statusmenu = 0;
    if([self.arrRecord.TDName isEqualToString:@""]){
        [self showAlertBox:@"Thông Báo" message:@"Phiếu đăng ký chưa bookport"];
        return;
    }
    Survey *vc = [[Survey alloc]initWithNibName:@"Survey" bundle:nil];
    vc.delegate = (id)self;
    vc.arrRecord = self.arrRecord;
    [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomTop];
}


-(IBAction)btnTypeMap_Clicked:(id)sender{
    (self.btn_changeMapType.selected == NO) ? (self.btn_changeMapType.selected = YES):(self.btn_changeMapType.selected= NO);
    
    if(self.btn_changeMapType.selected){
        self.mapView_.mapType = kGMSTypeSatellite;
        [self.btn_changeMapType setTitle:@"Bản đồ" forState:UIControlStateNormal];
    } else {
        self.mapView_.mapType = kGMSTypeNormal;
        [self.btn_changeMapType setTitle:@"Vệ tinh" forState:UIControlStateNormal];
    }
    
}

- (IBAction)btn_TichHen_Clicked:(id)sender {
    
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    // add by DanTT2
    if(![self.record.ODCCableType isEqualToString:@""]){
        [self ShowTichHen];
        return;
    }else {
        [self showAlertBox:@"Thông Báo" message:@"Phiếu đăng ký chưa book port nên không tích hẹn được"];
    }
}

- (void)recovery {
    NSString *tempType = [NSString stringWithFormat:@"%li", (long)[type integerValue] + 1];
    [self showMBProcess];
    ShareData *shared = [ShareData instance];
    [shared.mapproxy RecoverRegistration:tempType RegCode:self.regcode CreateBy:shared.currentUser.userName IP:@"" Completehander:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        [self showAlertBoxWithDelayDismiss:@"Thông Báo" message:message];
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}

-(void)ShowTichHen{
    [self showMBProcess];
    ShareData*share =[ShareData instance];
    TichHenPoup *vc= [[TichHenPoup alloc]initWithNibName:@"TichHenPoup" bundle:nil];
    vc.delegate = (id)self;
    [share.appProxy GetSubTeamID:share.currentUser.userName  RegCode:self.record.RegCode completionHandler:^(id result, NSString *errorCode, NSString *message) {
        
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            return ;
        }
        
        vc.arrList = [result objectForKey:@"ListObject"];
        vc.RegCode =share.RegCode;
        [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomTop];
        
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",error]];
        [self hideMBProcess];
    }];
    [self hideMBProcess];
}
-(void)UpdateDeployAppointment:(NSMutableDictionary *)dic{
    [self showMBProcess];
    ShareData*shared =[ShareData instance];
    [self CancelTichHenPoup];
    [shared.appProxy UpdateDeployAppointment:dic completionHandler:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            return ;
        }
        NSString *mess =[result[0] objectForKey:@"Result"]?:nil;
        [Common_client showAlertMessage:mess];
        [self hideMBProcess];
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",error]];
    }];
}
-(void)CancelTichHenPoup{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
    
}


-(IBAction)btnFindTD_Clicked:(id)sender{
    [self LoadData];
}

#pragma mark - add marker
-(void)AddMarker:(NSString *)latitude Longitude:(NSString *)longitude Snipet:(NSString *)snipet Title:(NSString *)title Icon:(UIImage *)icon {
    double lat = [latitude doubleValue];
    double ln = [longitude doubleValue];
    CLLocationCoordinate2D position2d = CLLocationCoordinate2DMake(lat, ln);
    GMSMarker *marker = [GMSMarker markerWithPosition:position2d];
    
    [marker setSnippet: snipet];
    [marker setTitle: title];
    marker.icon = icon;
    [marker setDraggable:NO];
    marker.map = self.mapView_;
}

-(void)AddMarkerLocation:(NSString *)latitude Longitude:(NSString *)longitude {
    [self.mapView_ clear];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[latitude doubleValue]
                                                            longitude:[longitude doubleValue]
                                                                 zoom:15];
    [mapView_ animateToCameraPosition:camera];
    CLLocationCoordinate2D positions = CLLocationCoordinate2DMake([latitude doubleValue], [longitude doubleValue]);
    
    GMSMarker *marker = [GMSMarker markerWithPosition:positions];
    marker.icon = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"icon_home.png"] height:32] ;
    [marker setDraggable:YES];
    [self.mapView_ setSelectedMarker:marker];
    marker.map = self.mapView_;
    
}

#pragma mark - Map delegate

-(BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker{
    [mapView animateToLocation:marker.position];
    [self.mapView_ setSelectedMarker:marker];
    NSArray *nip = [marker.snippet componentsSeparatedByString:@":"];
    if(nip.count > 0){
        lngMarker = [nip objectAtIndex:1];
    }
    lngCoordinate = [NSString stringWithFormat:@"(%f,%f)", marker.position.latitude, marker.position.longitude];
    
    self.txtTextSearch.text = marker.title;
    return YES;
}

//method để di chuyển marker và lấy toạ độ tại vị trí marker
-(void)mapView:(GMSMapView *)mapView didDragMarker:(GMSMarker *)marker{
    
    lng = [NSString stringWithFormat:@"(%f,%f)", marker.position.latitude, marker.position.longitude];
}

-(void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker{
    self.txtTextSearch.text = marker.title;
}

-(void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    [self.txtTextSearch endEditing:YES];
    [self.mViewMenu removeFromSuperview];
    statusmenu = 0;
}

#pragma mark - popup delegate
-(void)cancelPopupBookPortADSL{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
}

-(void)cancelPopupBookPortFTTH{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
}

-(void)cancelPopupBookPortFTTHNew{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
}

-(void)CancelPopupSurvey{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
}

-(void)backToDetailRegisteredForm{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - LoadData
-(void)LoadData {
    [self.mapView_ clear];
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    NSString *latlngmap = [lng stringByReplacingOccurrencesOfString:@"(" withString:@""];
    latlngmap = [latlngmap stringByReplacingOccurrencesOfString:@")" withString:@""];
    NSArray *arr  = [latlngmap componentsSeparatedByString:@","];
    NSString *lat = [[arr objectAtIndex:0] stringByReplacingOccurrencesOfString:@" " withString:@"" ];
    NSString *longitude = [[arr objectAtIndex:1] stringByReplacingOccurrencesOfString:@" " withString:@"" ];
    
    [self showMBProcess];
    
    [self AddMarkerLocation:lat Longitude:longitude];
    ShareData *shared = [ShareData instance];
    [shared.mapproxy GetTapdiemLocation:lng Type:selectedType.Key Completehander:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(result != nil){
            NSMutableArray *arrObject = result;
            for (MapRecord *rc in arrObject) {
                NSArray *arr = [rc.Latlng componentsSeparatedByString:@","];
                NSString *lat = [[arr objectAtIndex:0] stringByReplacingOccurrencesOfString:@"(" withString:@""];
                NSString *ln = [[arr objectAtIndex:1] stringByReplacingOccurrencesOfString:@")" withString:@""];
                NSString *snipet = StringFormat(@"Port trống: %@ \n%@ \nKhoảng cách: %@ m",rc.PortFree, rc.Address, rc.Distance);
                UIImage *icon = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"ic_td"] height:32];
                icon = [self setImageForMarkerWithData:rc];
                [self AddMarker:lat Longitude:ln Snipet:snipet Title:rc.TDName Icon:icon];
                
                
            }
            
        }else {
            [self ShowAlertBoxEmpty];
        }
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",error]];
        [self hideMBProcess];
    }];
}

// set image for marker with port free number
- (UIImage *)setImageForMarkerWithData:(MapRecord *)rc {
    UIImage *icon = [[UIImage alloc] init];
    CGFloat portFreeRatio = [rc.PortFreeRatio floatValue];
    NSInteger portFreeNumbers = [rc.PortFree integerValue];
    
    if (portFreeNumbers == 0) {
        icon = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"ic_marker_orange"] height:32];
        return icon;
        
    }
    
    if (portFreeNumbers == 1) {
        icon = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"ic_marker_red"] height:32];
        return icon;
    }
    
    if (portFreeRatio <= 0.25) {
        icon = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"ic_marker_yellow"] height:32];
        return icon;
        
    }
    
    icon = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"ic_marker_blue"] height:32];
    
    return icon;
    
}

#pragma mark - set type button
-(void)setType
{
    self.btn_changeMapType.showsTouchWhenHighlighted = YES;
    self.btn_changeMapType.selected= NO;
    
    self.btnBookPort.showsTouchWhenHighlighted = YES;
    self.btnBookPort.selected= NO;
    
    self.btnSurvey.showsTouchWhenHighlighted = YES;
    self.btnSurvey.selected= NO;
    
    self.btnFind.showsTouchWhenHighlighted = YES;
    self.btnFind.layer.cornerRadius = 8.0f;
    self.btnFind.selected= NO;
}


- (void)rightSideMenuButtonPressed:(id)sender {
    
    if(statusmenu  == 0){
        [self.mViewMenu setFrame:CGRectMake(150,64,320,self.mViewMenu.frame.size.height)];
        [self.view addSubview:self.mViewMenu];
        statusmenu = 1;
    }else if(statusmenu == 1){
        [self.mViewMenu removeFromSuperview];
        statusmenu = 0;
    }
    
}

#pragma mark - UIActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self recovery];
    }
}
//vutt11
#pragma mark - alert view delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 99 && buttonIndex == 1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        
        NSLog(@"opened Setting enable GPS");
        
        return;
    }
}

@end
