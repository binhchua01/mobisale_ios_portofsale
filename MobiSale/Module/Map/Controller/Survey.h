//
//  Survey.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 5/3/15.
//  Copyright (c) 2015 FPT.RAD.MOBISALE. All rights reserved.
//

#import "BaseViewController.h"
#import "SurveyMap.h"
#import "RegistrationFormDetailRecord.h"


@protocol SurveyDelegate <NSObject>

-(void)CancelPopupSurvey;

@end

@interface Survey : BaseViewController<SurveyMapDelegate1>

@property (strong, nonatomic) IBOutlet UILabel *lblRegID;
@property (weak, nonatomic)IBOutlet UIImageView *img;

@property (strong, nonatomic) IBOutlet UIButton *btnCapType;
@property (weak, nonatomic)IBOutlet UIButton *btnIndoor;
@property (weak, nonatomic)IBOutlet UIButton *btnOutdoor;
@property (weak, nonatomic)IBOutlet UIButton *btnModem;
@property (weak, nonatomic)IBOutlet UIButton *btnCodeMap;

@property (weak, nonatomic)IBOutlet UITextField *txtIndoor;
@property (weak, nonatomic)IBOutlet UITextField *txtOutdoor;
//@property (weak, nonatomic)IBOutlet UITextField *txtTD;
@property (strong, nonatomic) IBOutlet UILabel *lblTD;

@property (strong, nonatomic)NSMutableArray *arrIndoor;
@property (strong, nonatomic)NSMutableArray *arrOutdoor;
@property (strong, nonatomic)NSMutableArray *arrMapCode;
@property (strong, nonatomic)NSMutableArray *arrModem;
@property (strong, nonatomic)NSMutableArray *arrCapType;

//@property (strong, nonatomic)  NSString *imageData;
- (IBAction)btnCapType_Clicked:(id)sender;
-(IBAction)btnIndoor_Clicked:(id)sender;
-(IBAction)btnOutdoor_Clicked:(id)sender;
-(IBAction)btnModem_Clicked:(id)sender;
-(IBAction)btnCodeMap_Clicked:(id)sender;
-(IBAction)btnUpdate_Clicked:(id)sender;
-(IBAction)btnCancel_Clicked:(id)sender;
-(IBAction)btnSurveyMap_Clicked:(id)sender;
-(IBAction)btnUpImage_Clicked:(id)sender;
-(IBAction)btnCamera_Clicked:(id)sender;

@property (strong, nonatomic) RegistrationFormDetailRecord *arrRecord;
@property (strong, nonatomic) UIImage *image;

@property (retain, nonatomic) id<SurveyDelegate> delegate;
- (IBAction)editingOutDoor:(id)sender;

- (IBAction)editingInDoor:(id)sender;

@end
