//
//  BookPortFTTHNew.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 2/5/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BookPortFTTHNew.h"
#import "UIPopoverListView.h"
#import "KeyValueModel.h"
#import "ShareData.h"
#import "PortFreeRecord.h"
#import "LocationBranchPopRecord.h"
#import "../../../Models/Common.h"
#import "Common_client.h"

@interface BookPortFTTHNew (){
    
    //add by DanTT2
    UIAlertView *noticeError;
}

@end

@implementation BookPortFTTHNew{
    KeyValueModel *selectedLocation, *selectedBrank, *selectedPop, *selectedTD, *selectedPort;
    PortFreeRecord *selectedPortTD;
    NSString *TDID;
    LocationBranchPopRecord *rc;
}
@synthesize latlngDevice;

enum tableSourceStype {
    Location = 1,
    Brank = 2,
    Pop = 3,
    TD = 4,
    Port = 5,
};

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"BOOK PORT FTTH NEW";
    self.txtRegis.text = self.Regcode;
    self.txtTD.text = self.TdName;
    
    [self LoadData];
    if(([self.ODCCableType isEqualToString:@""] || self.ODCCableType == nil) && (![self.TdName isEqualToString:@""] || self.TdName != nil)){
        self.btnRecovery.enabled = NO;
    
    }
    //[self LoadPort];
}
-(void)viewDidAppear:(BOOL)animated{
    self.view.frame = CGRectMake(0, 30, self.view.frame.size.width,  self.view.frame.size.height);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - DropDown view

- (void)setupDropDownView:(NSInteger) tag
{
    
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.tag = tag;
    poplistview.listView.scrollEnabled = TRUE;
    switch (tag) {
        case Location:
            [poplistview setTitle:NSLocalizedString(@"Chọn vùng miền", @"")];
            break;
        case Brank:
            [poplistview setTitle:NSLocalizedString(@"Chọn chi nhánh", @"")];
            break;
        case Pop:
            [poplistview setTitle:NSLocalizedString(@"Chọn Pop", @"")];
            break;
        case TD:
            [poplistview setTitle:NSLocalizedString(@"Chọn tập điểm", @"")];
            break;
        case Port:
            [poplistview setTitle:NSLocalizedString(@"Chọn Port", @"")];
            break;
        default:
            break;
    }
    
    [poplistview show];
    
}
- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35.0f;
}

#pragma mark - UIPopoverListViewDataSource

- (void) setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell tag:(NSInteger)tag{
    PortFreeRecord *model;
    switch (tag) {
        case Port:
            model = [self.arrPort objectAtIndex:row];
            cell.textLabel.text = model.PortID;
            break;
        default:
            break;
    }
    
    
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row cell:cell tag:popoverListView.tag];
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section {
    NSInteger count = 0;
    switch (popoverListView.tag) {
        case Port:
            count = self.arrPort.count;
            break;
        default:
            break;
    }
    return count;
}

#pragma mark - UIPopoverListViewDelegate

- (void) didDropDownSelected:(NSInteger)row tag:(NSInteger)tag {
    
    switch (tag) {
        case Port:
            selectedPortTD = [self.arrPort objectAtIndex:row];
            [self.btnPort setTitle:selectedPortTD.PortID forState:UIControlStateNormal];
            break;
        default:
            break;
    }
    
    
}


#pragma mark - UIPopoverListViewDelegate

- (void)popoverListView:(UIPopoverListView *)popoverListView
     didSelectIndexPath:(NSIndexPath *)indexPath {
    [self didDropDownSelected:indexPath.row tag:popoverListView.tag];
    
}


#pragma mark - event click button
-(IBAction)btnCancel_clicked:(id)sender {
    if(self.delegate){
        [self.delegate cancelPopupBookPortFTTHNew];
    }
}


-(IBAction)btnPort_clicked:(id)sender{
    [self setupDropDownView:Port];
}

-(IBAction)btnRecovery_clicked:(id)sender{
    
    ShareData *shared = [ShareData instance];
    if(![shared.currentUser.PortLocationID isEqualToString:rc.LocationID]){
        [self showAlertBox:@"Thông Báo" message:@"Tài khoản không có quyền book port trong tỉnh thành này"];
        return;
    }
    
    //add by DanTT 2
    UIActionSheet *noticeSave = [[UIActionSheet alloc] initWithTitle:@"Bạn chắc chắn muốn thu hồi port?" delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
    noticeSave.tag = 1;
    [noticeSave showInView:[UIApplication sharedApplication].keyWindow];
    
}

-(IBAction)btnUpdate_clicked:(id)sender{
     ShareData *shared = [ShareData instance];
    if(rc == nil){
        [self showAlertBox:@"Thông Báo" message:@"Dữ liệu không hợp lệ"];
        return;
    }
    if(![shared.currentUser.PortLocationID isEqualToString:rc.LocationID]){
        [self showAlertBox:@"Thông Báo" message:@"Tài khoản không có quyền book port trong tỉnh thành này"];
        return;
    }
    if(selectedPortTD.ID == nil){
        [self showAlertBox:@"Thông Báo" message:@"Không có port trống nên không thể book port"];
        return;
    }
    
    //add by DanTT 2
    UIActionSheet *noticeSave = [[UIActionSheet alloc] initWithTitle:@"Bạn chắc chắn muốn book port?" delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
    noticeSave.tag = 0;
    [noticeSave showInView:[UIApplication sharedApplication].keyWindow];
    
}

- (void)bookport {
    [self showMBProcess];
    ShareData *shared = [ShareData instance];
    [shared.mapproxy BookPortFTTHNEW:selectedPortTD.ID RegCode:self.Regcode TDName:self.TdName UserName:shared.currentUser.userName IP:[Common getIPAddress] Latlng:self.Latlng LatlngDevice:self.latlngDevice Completehander:^(id result, NSString *errorCode, NSString *message) {
        
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"<null>"]){
            
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            return ;
        }
        
        [self showAlertBoxWithDelayDismiss:@"Thông Báo" message:message];
        [self.delegate cancelPopupBookPortFTTHNew];
        if ([message isEqualToString:@"Bookport thành công."]) {
            [self.delegate backToDetailRegisteredForm];
        }
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
        
    }];
}

- (void)recovery {
    [self showMBProcess];
    //add by DanTT 2
    NSString * tempType = [NSString stringWithFormat:@"%li", (long)[self.Type integerValue] + 1];
    ShareData *shared = [ShareData instance];
    [shared.mapproxy RecoverRegistration:tempType RegCode:self.Regcode CreateBy:shared.currentUser.userName IP:@"" Completehander:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        
        [self showAlertBoxWithDelayDismiss:@"Thông Báo" message:message];
        [self LoadData];
        
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}

#pragma  mark - load data
-(void)LoadData{
    [self showMBProcess];
    ShareData * shared = [ShareData instance];
    [shared.mapproxy GetLocationBranchPOP:self.TdName Type:self.Type Completehander:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        // add by dantt2
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self hideMBProcess];
            [self LogOut];
            return;
        }
        if (result == nil) {
            [self showAlertErrorBox:@"Thông báo" message:message];
            [self hideMBProcess];
            return;
        }
        
        if(![errorCode isEqualToString:@"<null>"]){
            // add by DanTT2
            [self showAlertErrorBox:@"Thông báo" message:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        rc = [result objectAtIndex:0];
        if(rc!= nil) {
            self.txtLocation.text = rc.LocationName;
            self.txtBranch.text = rc.NameBranch;
            self.txtPop.text = rc.PopName;
        }
        
        [self LoadPort];
    } errorHandler:^(NSError *error) {
        // add by DanTT2
        [self showAlertErrorBox:@"Thông báo" message:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}

-(void)LoadPort{
    ShareData * shared = [ShareData instance];
    [shared.mapproxy GetPortFreeFTTHNEW:self.TdName Completehander:^(id result, NSString *errorCode, NSString *message) {
        // add by dantt2
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        
        if (result == nil) {
            [self.btnPort setEnabled:NO];
            [self hideMBProcess];
            return;
        }
        
        if(![errorCode isEqualToString:@"<null>"] ){
            // add by DanTT2
            [self showAlertErrorBox:@"Thông báo" message:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        
        self.arrPort = result;
        selectedPortTD = [self.arrPort objectAtIndex:0];
        [self.btnPort setTitle:selectedPortTD.PortID forState:UIControlStateNormal];
        [self.btnPort setEnabled:NO];
        self.lblTotalPort.text = [NSString stringWithFormat:@"%d" ,(int)self.arrPort.count];
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        // add by DanTT2
        [self showAlertErrorBox:@"Thông báo" message:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
    }];
}

- (void)showAlertBox:(NSString *)title message:(NSString *)message {
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
    [alertView show];
}

// add by DanTT2
- (void)showAlertErrorBox:(NSString *)title
             message:(NSString *)message {
    noticeError = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
    [noticeError show];
}

// add by DanTT2
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView == noticeError) {
        if(self.delegate){
            [self.delegate cancelPopupBookPortFTTHNew];
        }
    }
}

#pragma mark - UIActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag == 0 && buttonIndex == 0) {
        [self bookport];
        return;
    }
    
    if (actionSheet.tag == 1 && buttonIndex == 0) {
        [self recovery];
        return;
    }
}

@end
