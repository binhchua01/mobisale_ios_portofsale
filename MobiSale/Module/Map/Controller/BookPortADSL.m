//
//  BookPortADSL.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 2/5/15.
//  Copyright (c) 2015 FPT.RAD.MOBISALE. All rights reserved.
//

#import "BookPortADSL.h"
#import "ShareData.h"
#import "LocationBranchPopRecord.h"
#import "UIPopoverListView.h"
#import "PortFreeRecord.h"
#import "Common_client.h"

@interface BookPortADSL ()

@end

@implementation BookPortADSL{
    
    PortFreeRecord *selectedPortTD;
    NSString *TDID;
    LocationBranchPopRecord *rc;
    
    //add by DanTT2
    UIAlertView *noticeError;

}

@synthesize arrPortTD;

enum tableSourceStype {
    PortTD= 1,
};

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"BOOK PORT ADSL";
    // Do any additional setup after loading the view.
    
    [self LoadLocation];
    //[self LoadPortTC];
    
    self.txtRegistNumber.text = self.Regcode;
    //self.txtNumber.text = @"1";
    self.txtTD.text = self.TdName;
}
-(void)viewDidAppear:(BOOL)animated{
    self.view.frame = CGRectMake(0, 30, self.view.frame.size.width,  self.view.frame.size.height);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

#pragma mark - DropDown view

- (void)setupDropDownView:(NSInteger) tag
{
    
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.tag = tag;
    poplistview.listView.scrollEnabled = TRUE;
    switch (tag) {
        case PortTD:
            [poplistview setTitle:NSLocalizedString(@"Chọn port tập điểm", @"")];
            break;
        default:
            break;
    }
    
    [poplistview show];
    
}
- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35.0f;
}

#pragma mark - UIPopoverListViewDataSource

- (void) setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell tag:(NSInteger)tag{
    PortFreeRecord *port;
    switch (tag) {
        case PortTD:
            port = [self.arrPortTD objectAtIndex:row];
            cell.textLabel.text = port.PortID;
            break;
        default:
            break;
    }
    
    
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row cell:cell tag:popoverListView.tag];
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    switch (popoverListView.tag) {
        case PortTD:
            count = self.arrPortTD.count;
            break;
        default:
            break;
    }
    return count;
}

#pragma mark - UIPopoverListViewDelegate

- (void) didDropDownSelected:(NSInteger)row tag:(NSInteger)tag{
    
    switch (tag) {
        case PortTD:
            selectedPortTD = [self.arrPortTD objectAtIndex:row];
            [self.btnPort setTitle:selectedPortTD.PortID forState:UIControlStateNormal];
            break;
        default:
            break;
    }
}


#pragma mark - UIPopoverListViewDelegate

- (void)popoverListView:(UIPopoverListView *)popoverListView
     didSelectIndexPath:(NSIndexPath *)indexPath
{
    [self didDropDownSelected:indexPath.row tag:popoverListView.tag];
    
}


#pragma mark - Load data
-(void)LoadLocation {
    [self showMBProcess];
    ShareData * shared = [ShareData instance];
    [shared.mapproxy GetLocationBranchPOP:self.TdName Type:self.Type Completehander:^(id result, NSString *errorCode, NSString *message) {
        // add by dantt2
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if (result == nil) {
            [self showAlertErrorBox:@"Thông báo" message:message];
            [self hideMBProcess];
            return;
        }
        
        if(![errorCode isEqualToString:@"<null>"]){
            // add by DanTT2
            [self showAlertErrorBox:@"Thông báo" message:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        rc = [result objectAtIndex:0];
        self.lblLocation.text = rc.LocationName;
        self.lblBranch.text = rc.NameBranch;
        self.txtPop.text = rc.PopName;
        [self LoadPortTC];
    } errorHandler:^(NSError *error) {
        // add by DanTT2
        [self showAlertErrorBox:@"Thông báo" message:[NSString stringWithFormat:@"Có lỗi GetPortFreeFirstTD %@",[error localizedDescription]]];
        [self hideMBProcess];

    }];
}

-(void)LoadPortTC {
    ShareData *shared = [ShareData instance];
    [shared.mapproxy GetPortFreeFirstTD:self.TdName Completehander:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self hideMBProcess];
            [self LogOut];
            return;
        }
        
        if (result == nil) {
            [self.btnPort setEnabled:NO];
            [self hideMBProcess];
            return;
        }
        
        if(![errorCode isEqualToString:@"<null>"]){
            // add by DanTT2
            [self showAlertErrorBox:@"Thông báo" message:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        
        self.arrPortTD = result;
        selectedPortTD = [self.arrPortTD objectAtIndex:0];
        [self.btnPort setTitle:selectedPortTD.PortID forState:UIControlStateNormal];
        [self.btnPort setEnabled:NO];
        self.txtNumber.text = selectedPortTD.PortID;
        self.lblTotalPort.text = [NSString stringWithFormat:@"%d" ,(int)self.arrPortTD.count];
        [self hideMBProcess];
    } errorHandler:^(NSError *error) {
        // add by DanTT2
        [self showAlertErrorBox:@"Thông báo" message:[NSString stringWithFormat:@"Có lỗi GetPortFreeFirstTD %@",[error localizedDescription]]];
    }];
}

#pragma mark - event button
-(IBAction)btnPort_clicked:(id)sender {
    [self setupDropDownView:PortTD];
}

-(IBAction)btnUpdate_clicked:(id)sender{
    if(rc == nil){
        [self showAlertBox:@"Thông Báo" message:@"Dữ liệu không hợp lệ"];
        return;
    }
    if(selectedPortTD.ID == nil){
        [self showAlertBox:@"Thông Báo" message:@"Không có port trống nên không thể book port"];
        return;
    }
     ShareData *shared = [ShareData instance];
    if(![shared.currentUser.PortLocationID isEqualToString:rc.LocationID]){
        [self showAlertBox:@"Thông Báo" message:@"Tài khoản không có quyền book port trong tỉnh thành này"];
        return;
    }
    
    //add by DanTT 2
    UIActionSheet *noticeSave = [[UIActionSheet alloc] initWithTitle:@"Bạn chắc chắn muốn book port?" delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
    noticeSave.tag = 0;
    [noticeSave showInView:[UIApplication sharedApplication].keyWindow];
    
}

-(IBAction)btnRecovery_clicked:(id)sender{
    //add by DanTT 2
    UIActionSheet *noticeSave = [[UIActionSheet alloc] initWithTitle:@"Bạn chắc chắn muốn thu hồi port?" delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
    noticeSave.tag = 1;
    [noticeSave showInView:[UIApplication sharedApplication].keyWindow];

}

- (void)bookport {
    [self showMBProcess];
    ShareData *shared = [ShareData instance];
    [shared.mapproxy BookPortADSL:rc.LocationID BranchID:rc.BranchID UserName:shared.currentUser.userName TDID:rc.TDID TypeSevice:@"1" RegCode:self.Regcode CardNumber:self.txtNumber.text IDPortTD:selectedPortTD.ID IP:[self getIPAddress] TDName:self.TdName Latlng:self.Latlng LatlngDevice:(NSString *)self.latlngDevice Completehander:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            return ;
        }
        [self showAlertBoxWithDelayDismiss:@"Thông Báo" message:message];
        [self.delegate cancelPopupBookPortADSL];
        if ([message isEqualToString:@"Bookport thành công."]) {
            [self.delegate backToDetailRegisteredForm];
        }
        
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
        
    }];
}

- (void)recovery {
    //add by DanTT 2
    NSString * tempType = [NSString stringWithFormat:@"%li", (long)[self.Type integerValue] + 1];
    [self showMBProcess];
    ShareData *shared = [ShareData instance];
    [shared.mapproxy RecoverRegistration:tempType RegCode:self.Regcode CreateBy:shared.currentUser.userName IP:@"" Completehander:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        [self showAlertBoxWithDelayDismiss:@"Thông Báo" message:message];
        [self LoadLocation];
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}

#pragma mark - event click button
-(IBAction)btnCancel_clicked:(id)sender
{
    if(self.delegate){
        [self.delegate cancelPopupBookPortADSL];
    }
}

#pragma mark - UI ALERT VIEW
// add by DanTT2
- (void)showAlertErrorBox:(NSString *)title
                  message:(NSString *)message {
    noticeError = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
    [noticeError show];
}

// add by DanTT2
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView == noticeError) {
        if(self.delegate){
            [self.delegate cancelPopupBookPortADSL];
        }
    }
}

#pragma mark - UIActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag == 0 && buttonIndex == 0) {
        [self bookport];
        return;
    }
    
    if (actionSheet.tag == 1 && buttonIndex == 0) {
        [self recovery];
        return;
    }
}

@end
