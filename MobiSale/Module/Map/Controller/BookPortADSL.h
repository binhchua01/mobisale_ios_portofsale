//
//  BookPortADSL.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 2/5/15.
//  Copyright (c) 2015 FPT.RAD.Sale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@protocol BookPortADSLDelegate <NSObject>

- (void)cancelPopupBookPortADSL;
- (void)backToDetailRegisteredForm;

@end

@interface BookPortADSL : BaseViewController

@property (nonatomic, retain) id<BookPortADSLDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIButton *btnCanel;
@property (strong, nonatomic) IBOutlet UITextField *lblLocation;
@property (strong, nonatomic) IBOutlet UITextField *lblBranch;
@property (strong, nonatomic) IBOutlet UITextField *txtPop;
@property (strong, nonatomic) IBOutlet UITextField *txtTD;
@property (strong, nonatomic) IBOutlet UIButton *btnUpdate;
@property (strong, nonatomic) IBOutlet UIButton *btnRecovery;
@property (strong, nonatomic) IBOutlet UIButton *btnPort;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalPort;

@property (strong, nonatomic) IBOutlet UITextField *txtRegistNumber;
@property (strong, nonatomic) IBOutlet UITextField *txtNumber;

-(IBAction)btnCancel_clicked:(id)sender;
-(IBAction)btnUpdate_clicked:(id)sender;
-(IBAction)btnRecovery_clicked:(id)sender;
-(IBAction)btnPort_clicked:(id)sender;

@property (strong, nonatomic) NSMutableArray *arrPortTD;

@property (strong, nonatomic) NSString *TdName;
@property (strong, nonatomic) NSString *Type;
@property (strong, nonatomic) NSString *Regcode;
@property (strong, nonatomic) NSString *Latlng;
@property (strong, nonatomic) NSString *latlngDevice;

@end
