//
//  MainMap.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 1/26/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "SurveyMap.h"
#import "../../../Helper/UIDropDown/UIPopoverListView.h"
#import "KeyValueModel.h"
#import "../../../Helper/MJPopup/UIViewController+MJPopupViewController.h"
#import "MapRecord.h"
#import "ShareData.h"


@interface SurveyMap ()

@end

@implementation SurveyMap{
    CLLocation *latlng;
    KeyValueModel *selectedType;
    NSString *lng, *PortFree, *lngMarker;
    int tapmap;
    CLLocationCoordinate2D position;
    GMSMarker *pin;
    NSMutableArray *arrPolylineAdded;
    NSMutableArray *arrPosition;

    
}
@synthesize mapView_;
@synthesize locationManager,delegate;
@synthesize capType, indoor, outdoor;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    arrPolylineAdded = [NSMutableArray array];
    arrPosition      = [NSMutableArray array];
    self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
    
    
    
    self.mapView_.delegate = self;
    [self loadMaps];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    if(IS_OS_8_OR_LATER) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
    
    [self.mView setFrame:CGRectMake(0,0,320,[SiUtils getScreenFrameSize].height - 30)];
    [self.view addSubview:self.mView];
    
    [self.mViewButton setFrame:CGRectMake(0,0,self.mViewButton.frame.size.width,self.mViewButton.frame.size.height)];
    [self.view addSubview:self.mViewButton];
    
    [self.mViewAnnotation setFrame:CGRectMake(10,[SiUtils getScreenFrameSize].height - 82,self.mViewAnnotation.frame.size.width,self.mViewAnnotation.frame.size.height)];
    [self.view addSubview:self.mViewAnnotation];
    
    self.lblCapType.text = StringFormat(@"Cáp\n%@",capType);
    self.lblIndoor.text  = StringFormat(@"Indoor\n%@ m",indoor);
    self.lblOutdoor.text = StringFormat(@"Outdoor\n%@ m",outdoor);
    
    self.title = @"BẢN ĐỒ KHẢO SÁT";
    self.screenName=self.title;
    
    tapmap = 0;
    [self showMapp];
    
}
-(void)showMapp{
    NSArray *arr = [self.arrRecord.Latlng componentsSeparatedByString:@","];
    if([arr count] <2){
        [self showMessage:@"Toạ độ không chính xác vui lòng liện hệ support"];
        return;
    }
    NSString *lat = [[arr objectAtIndex:0] stringByReplacingOccurrencesOfString:@"(" withString:@""];
    NSString *ln = [[arr objectAtIndex:1] stringByReplacingOccurrencesOfString:@")" withString:@""];
    [self AddMarker:lat Longitude:ln];
    position.longitude = [ln doubleValue];
    position.latitude = [lat doubleValue];
    
}
-(void)showMessage:(NSString*)message{
    NSString *title= @"Thông báo";
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK"                                          otherButtonTitles:nil];
    [alert show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) viewWillDisappear:(BOOL)animated {
    // [self viewWillDisappear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

#pragma Load Map and location

- (void)loadMaps {
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latlng.coordinate.latitude
                                                            longitude:latlng.coordinate.longitude
                                                                 zoom:15];
    
    if(!self.mapView_)
    {
        mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    }
    mapView_.delegate = self;
    mapView_.myLocationEnabled = YES;
    mapView_.settings.compassButton = YES;
    mapView_.settings.myLocationButton = YES;
    self.mView = mapView_;
    [self showCurrentLocation];
    
}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    latlng = [locations lastObject];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latlng.coordinate.latitude
                                                            longitude:latlng.coordinate.longitude
                                                                 zoom:13];
    [mapView_ animateToCameraPosition:camera];
    [self.locationManager stopUpdatingLocation];
    NSString *latitude = [NSString stringWithFormat:@"%f",latlng.coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f",latlng.coordinate.longitude];
    
    [self AddMarkerLocation];
    
    lng = [NSString stringWithFormat:@"(%@,%@)", latitude,longitude];
}
- (void)showCurrentLocation {
    mapView_.myLocationEnabled = YES;
    if(IS_OS_8_OR_LATER) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
    
    
}

#pragma mark - DropDown view

- (void)setupDropDownView {
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.listView.scrollEnabled = TRUE;
    [poplistview setTitle:NSLocalizedString(@"Chọn loại", @"")];
    [poplistview show];
    
}

- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 35.0f;
}



#pragma mark - add marker

-(void)AddMarkerLocation {
    pin.map = nil;
    CLLocationCoordinate2D positions = CLLocationCoordinate2DMake(latlng.coordinate.latitude, latlng.coordinate.longitude);
    //    GMSMarker *marker = [GMSMarker markerWithPosition:positions];
    //    marker.icon = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"cus_location.png"] height:24] ;
    //    [marker setDraggable:YES];
    //    [self.mapView_ setSelectedMarker:marker];
    //    marker.map = self.mapView_;
    pin = [GMSMarker markerWithPosition:positions];
    pin.icon = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"icon_home.png"] height:24] ;
    [pin setDraggable:YES];
    [self.mapView_ setSelectedMarker:pin];
    pin.map = self.mapView_;
}
-(void)AddMarker:(NSString *)latitude Longitude:(NSString *)longitude {
    double lat = [latitude doubleValue];
    double ln = [longitude doubleValue];
    CLLocationCoordinate2D position2d = CLLocationCoordinate2DMake(lat, ln);
    GMSMarker *marker = [GMSMarker markerWithPosition:position2d];
    marker.icon = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"ic_td"] height:24];
    [marker setDraggable:NO];
    marker.map = self.mapView_;
}

#pragma mark - Map delegate

-(void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    GMSMutablePath *path = [GMSMutablePath path];
    [path addLatitude:position.latitude longitude:position.longitude];
    [path addLatitude:coordinate.latitude longitude:coordinate.longitude];
    GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
    polyline.strokeWidth = 1.f;
    polyline.geodesic = YES;
    polyline.map = mapView_;
    [arrPolylineAdded addObject:polyline];
    CLLocation *tempLatlng = [[CLLocation alloc] initWithLatitude:position.latitude longitude:position.longitude];
    [arrPosition      addObject:tempLatlng];
    position = coordinate;
}

#pragma mark - event button clicked
-(IBAction)btnDone_Clicked:(id)sender{
    if(self.delegate){
        [self.delegate CancenPopupSurveyMap];
    }
}

-(IBAction)btnDelete_Clicked:(id)sender{
   // [self.mapView_ clear];
    GMSPolyline *polylineToRemove = [arrPolylineAdded lastObject];
    polylineToRemove.map = nil;
    [arrPolylineAdded removeLastObject];
    position = [[arrPosition lastObject] coordinate];
    if (arrPosition.count > 1) {
        [arrPosition removeLastObject];
    }

}



/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
