//
//  BookPortFTTH.h
//  MobiSale
//
//  Created by  Nguyen Tan Tho on 2/5/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@protocol BookPortFTTHDelegate <NSObject>

- (void)cancelPopupBookPortFTTH;
- (void)backToDetailRegisteredForm;

@end

@interface BookPortFTTH : BaseViewController

@property (nonatomic, retain) id<BookPortFTTHDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIButton *btnCanel;
@property (strong, nonatomic) IBOutlet UIButton *btnUpdate;
@property (strong, nonatomic) IBOutlet UIButton *btnRecovery;
@property (strong, nonatomic) IBOutlet UITextField *txtLocation;
@property (strong, nonatomic) IBOutlet UITextField *txtBranch;
@property (strong, nonatomic) IBOutlet UIButton *btnService;
@property (strong, nonatomic) IBOutlet UITextField *txtPop;
@property (strong, nonatomic) IBOutlet UITextField *txtTD;
@property (strong, nonatomic) IBOutlet UIButton *btnType;
@property (strong, nonatomic) IBOutlet UIButton *btnPortTD;
@property (strong, nonatomic) IBOutlet UIButton *btnPortSC;


@property (strong, nonatomic) IBOutlet UITextField *txtRegistNumber;
@property (strong, nonatomic) IBOutlet UITextField *txtNumber;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalPort;

-(IBAction)btnCancel_clicked:(id)sender;
-(IBAction)btnUpdate_clicked:(id)sender;
-(IBAction)btnRecovery_clicked:(id)sender;
-(IBAction)btnPortTD_clicked:(id)sender;
-(IBAction)btnPortSC_clicked:(id)sender;
-(IBAction)btnService_clicked:(id)sender;
-(IBAction)btnType_clicked:(id)sender;


@property (strong, nonatomic) NSMutableArray *arrPortSC;
@property (strong, nonatomic) NSMutableArray *arrPortTD;
@property (strong, nonatomic) NSMutableArray *arrPop;
@property (strong, nonatomic) NSMutableArray *arrTD;
@property (strong, nonatomic) NSMutableArray *arrType;
@property (strong, nonatomic) NSMutableArray *arrTypeService;
@property (strong, nonatomic) NSMutableArray *arrBranch;
@property (strong, nonatomic) NSMutableArray *arrLocation;

@property (strong, nonatomic) NSString *TdName;
@property (strong, nonatomic) NSString *Type;
@property (strong, nonatomic) NSString *Regcode;
@property (strong, nonatomic) NSString *Portfree;
@property (strong, nonatomic) NSString *TypeContract;
@property (strong, nonatomic) NSString *Latlng;
@property (strong, nonatomic) NSString *latlngDevice;


@end
