//
//  Survey.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 5/3/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "Survey.h"
#import "UIPopoverListView.h"
#import "KeyValueModel.h"
#import "Common.h"
#import "ShareData.h"
#import "Common_client.h"
#import "UIImage+FPTCustom.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface Survey () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate>

@property (strong, nonatomic) UIImagePickerController *imagePickerController;

@end

@implementation Survey{
    KeyValueModel *selectedIndoor, *selectedOutdoor, *selectedMapCode, *selectedModem, *selectedCapType;
    NSString *imageData;
}

@synthesize arrIndoor;
@synthesize arrMapCode;
@synthesize arrModem;
@synthesize arrOutdoor;
@synthesize arrCapType;

enum tableSourceStype {
    
    Indoor  = 1,
    Outdoor = 2,
    MapCode = 3,
    Modem   = 4,
    CapType = 5

};

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.screenName = @"KHẢO SÁT PĐK";
    [self checkBookportType:[self.arrRecord.BookPortType intValue]];
    //[self LoadIndoor];
    //[self LoadOutdoor];
    [self LoadMapCode];
    [self LoadModem];
    
//    self.txtTD.text = self.arrRecord.TDName;
    self.lblRegID.text   = self.arrRecord.RegCode;
    self.lblTD.text      = self.arrRecord.TDName;
    self.txtIndoor.text  = self.arrRecord.InDoor;
    self.txtOutdoor.text = self.arrRecord.OutDoor;
    self.img.image       = self.image;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    self.view.frame = CGRectMake(0, 30, self.view.frame.size.width,  self.view.frame.size.height);
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

#pragma mark - Custom Accessors
- (UIImagePickerController *) imagePickerController {
    if (!_imagePickerController) {
        _imagePickerController = [[UIImagePickerController alloc] init];
        _imagePickerController.allowsEditing = NO;
        _imagePickerController.delegate = self;
        
    }
    return _imagePickerController;
}

#pragma mark - DropDown view

- (void)setupDropDownView:(NSInteger) tag
{
    
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.tag = tag;
    poplistview.listView.scrollEnabled = TRUE;
    switch (tag) {
        case Indoor:
            [poplistview setTitle:NSLocalizedString(@"Chọn Indoor", @"")];
            break;
        case Outdoor:
            [poplistview setTitle:NSLocalizedString(@"Chọn Outdoor", @"")];
            break;
        case MapCode:
            [poplistview setTitle:NSLocalizedString(@"Chọn mã bản đồ", @"")];
            break;
        case Modem:
            [poplistview setTitle:NSLocalizedString(@"Chọn Modem", @"")];
            break;
        case CapType:
            [poplistview setTitle:NSLocalizedString(@"Chọn loại cap", @"")];
            break;
        default:
            break;
    }
    
    [poplistview show];
    
}
- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35.0f;
}

#pragma mark - UIPopoverListViewDataSource

- (void) setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell tag:(NSInteger)tag{
    KeyValueModel *model;

    switch (tag) {
        case Indoor:
            model = [self.arrIndoor objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Outdoor:
            model = [self.arrOutdoor objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case MapCode:
            model = [self.arrMapCode objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Modem:
            model = [self.arrModem objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case CapType:
            model = [self.arrCapType objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        default:
            break;
    }
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row cell:cell tag:popoverListView.tag];
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    switch (popoverListView.tag) {
            
        case Indoor:
            count = self.arrIndoor.count;
            break;
        case Outdoor:
            count = self.arrOutdoor.count;
            break;
        case MapCode:
            count = self.arrMapCode.count;
            break;
        case Modem:
            count = self.arrModem.count;
            break;
        case CapType:
            count = self.arrCapType.count;
            break;
        default:
            break;
    }
    return count;
}

#pragma mark - UIPopoverListViewDelegate

- (void) didDropDownSelected:(NSInteger)row tag:(NSInteger)tag{
    
    switch (tag) {
            
        case Indoor:
            selectedIndoor = [self.arrIndoor objectAtIndex:row];
            [self.btnIndoor setTitle:selectedIndoor.Values forState:UIControlStateNormal];
            break;
        case Outdoor:
            selectedOutdoor = [self.arrOutdoor objectAtIndex:row];
            [self.btnOutdoor setTitle:selectedOutdoor.Values forState:UIControlStateNormal];
            break;
        case MapCode:
            selectedMapCode = [self.arrMapCode objectAtIndex:row];
            [self.btnCodeMap setTitle:selectedMapCode.Values forState:UIControlStateNormal];
            break;
        case Modem:
            selectedModem = [self.arrModem objectAtIndex:row];
            [self.btnModem setTitle:selectedModem.Values forState:UIControlStateNormal];
            break;
        case CapType:
            selectedCapType = [self.arrCapType objectAtIndex:row];
            [self.btnCapType setTitle:selectedCapType.Values forState:UIControlStateNormal];
            break;
        default:
            break;
    }
}


#pragma mark - UIPopoverListViewDelegate

- (void)popoverListView:(UIPopoverListView *)popoverListView
     didSelectIndexPath:(NSIndexPath *)indexPath
{
    [self didDropDownSelected:indexPath.row tag:popoverListView.tag];
    
}

#pragma mark - event button click
- (IBAction)btnCapType_Clicked:(id)sender {
    [self.view endEditing:YES];
    [self setupDropDownView:CapType];
}

-(IBAction)btnIndoor_Clicked:(id)sender{
    [self.view endEditing:YES];
    [self setupDropDownView:Indoor];
}
-(IBAction)btnOutdoor_Clicked:(id)sender{
    [self.view endEditing:YES];
    [self setupDropDownView:Outdoor];
}
-(IBAction)btnModem_Clicked:(id)sender{
    [self.view endEditing:YES];
    [self setupDropDownView:Modem];
}
-(IBAction)btnCodeMap_Clicked:(id)sender{
    [self.view endEditing:YES];
    [self setupDropDownView:MapCode];
}
-(IBAction)btnSurveyMap_Clicked:(id)sender{
    SurveyMap *surveyMap = [[SurveyMap alloc]initWithNibName:@"SurveyMap" bundle:nil];
    
    surveyMap.delegate =self;
    surveyMap.arrRecord = self.arrRecord;
    NSArray *arr = [self.arrRecord.Latlng componentsSeparatedByString:@","];
    if([arr count] <2){
        [self showMessage:@"Toạ độ không chính xác vui lòng liện hệ support"];
        return;
    }
    surveyMap.capType = selectedCapType.Values ;
    surveyMap.indoor  = self.txtIndoor.text;
    surveyMap.outdoor = self.txtOutdoor.text;
    
    [self presentPopupViewController:surveyMap animationType:MJPopupViewAnimationSlideBottomTop];
    
}
-(void)showMessage:(NSString*)message{
    NSString *title= @"Thông báo";
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}
-(IBAction)btnCancel_Clicked:(id)sender
{
    if(self.delegate){
        [self.delegate CancelPopupSurvey];
    }
}

-(IBAction)btnUpdate_Clicked:(id)sender{
    
    [self UpImage];
    
   }

-(void)UpdateSurvery_click:(NSString*)linkImage{
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    ShareData *shared = [ShareData instance];
    [shared.mapproxy UpdateSurvey:self.arrRecord.RegCode
                          OutDType:selectedCapType.Key
                          OutDoor:self.txtOutdoor.text
                          InDType:selectedCapType.Key
                          InDoor:self.txtIndoor.text
                          Image:linkImage
                          MapCode:selectedMapCode.Key
                          Modem:selectedModem.Key
                          TDName:self.lblTD.text
                          UserName:shared.currentUser.userName
                          Latlng:self.arrRecord.Latlng
     
                   Completehander:^(id result, NSString *errorCode, NSString *message) {
                       
                       [self hideMBProcess];
                       
                       if ([message isEqualToString:@"het phien lam viec"]) {
                           [self ShowAlertErrorSession];
                           [self LogOut];
                           return;
                       }
                       
                       if(![errorCode isEqualToString:@"1"]){
                           [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
                           return ;
                       }
                       
                       if ([errorCode intValue] >= 0) {
                           [self showMessage:message];
                           if (self.delegate) {
                               [self.delegate CancelPopupSurvey];
                           }
                           return;
                       }
                       
                       [self showMessage:message];
                       
    } errorHandler:^(NSError *error) {
       [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}

-(void)UpImage {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    if([selectedIndoor.Key isEqualToString:@"0"]){
        [self showAlertBox:@"Thông Báo" message:@"Chưa chọn Indoor"];
        return;
    }
    if([selectedOutdoor.Key isEqualToString:@"0"]){
        [self showAlertBox:@"Thông Báo" message:@"Chưa chọn Outdoor"];
        return;
    }
    if([self.txtIndoor.text isEqualToString:@"0"] || self.txtIndoor.text.length == 0){
        [self showAlertBox:@"Thông Báo" message:@"Chưa điền chiều dài indoor"];
        return;
    }
    if([self.txtOutdoor.text isEqualToString:@"0"] || self.txtOutdoor.text.length == 0){
        [self showAlertBox:@"Thông Báo" message:@"Chưa điền chiều dài outdoor"];
        return;
    }
    
    NSData *imageToUseData = UIImageJPEGRepresentation(self.img.image, 0.2);
    
    imageData = [self base64forData:imageToUseData];
    
    if(imageData.length <10){
        
        [self UpdateSurvery_click:@""];
        //[self showAlertBox:@"Thông Báo" message:@"Chưa có hình ảnh"];
        NSLog(@"Chưa có hình ảnh");
        return;
    }
    
    [self showMBProcess];
    ShareData *shared  = [ShareData instance];
    [shared.mapproxy UploadImageInvest:imageData userName:shared.currentUser.userName Completehander:^(id result, NSString *errorCode, NSString *message) {
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(message.length <10){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
   
        [self UpdateSurvery_click:message];
      
        
    } errorHandler:^(NSError *error) {
        
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
        return;

    }];
}

-(IBAction)btnUpImage_Clicked:(id)sender {
    
    self.imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:self.imagePickerController animated:YES completion:nil];

    
}

-(IBAction)btnCamera_Clicked:(id)sender {
    self.imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:self.imagePickerController animated:YES completion:nil];
}

- (BOOL)startMediaBrowserFromViewController: (UIViewController*) controller
                               usingDelegate: (id <UIImagePickerControllerDelegate,
                                               UINavigationControllerDelegate>) delegate {

    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)
        || (delegate == nil)
        || (controller == nil))
        return NO;
    
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
     mediaUI.delegate = delegate;
    
    mediaUI.mediaTypes =
    [UIImagePickerController availableMediaTypesForSourceType:
     UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    
    // Hides the controls for moving & scaling pictures, or for
    // trimming movies. To instead show the controls, use YES.
    mediaUI.allowsEditing = NO;

 //   [controller presentModalViewController: mediaUI animated: YES];
    [controller presentViewController: mediaUI animated:YES completion:nil];

    return YES;
}

#pragma mark - UIImagePickerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [[picker presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo: (NSDictionary *)info {
    imageData = @"";
    UIImage *originalImage, *editedImage, *imageToUse;
    
    // Handle a still image picked from a photo album
    editedImage = (UIImage *)[info objectForKey:UIImagePickerControllerEditedImage];
    
    originalImage = (UIImage *)[info objectForKey:UIImagePickerControllerOriginalImage];
    
    if (editedImage) {
        imageToUse = editedImage;
    } else {
        imageToUse = originalImage;
    }
    CGFloat scale = imageToUse.size.width / 300;
    imageToUse = [UIImage scaleImage:imageToUse toSize:CGSizeMake(imageToUse.size.width/scale, imageToUse.size.height/scale)];
  //  NSLog(@"dataSize 1: %@",[NSByteCountFormatter stringFromByteCount:imageData.length countStyle:NSByteCountFormatterCountStyleFile]);
    self.img.image = imageToUse;
    
    [self savePhotoTakenFromCameraToLibrary:picker withImage:imageToUse];
    
   [[picker presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}

- (void)savePhotoTakenFromCameraToLibrary:(UIImagePickerController *)picker withImage:(UIImage *)image {
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        // save to library
        ALAssetsLibrary* library = [[ALAssetsLibrary alloc] init];
        [library writeImageToSavedPhotosAlbum:image.CGImage orientation:(ALAssetOrientation)image.imageOrientation completionBlock:^(NSURL *assetURL, NSError *error ) {
            NSLog(@"IMAGE SAVED TO PHOTO ALBUM");
            [library assetForURL:assetURL resultBlock:^(ALAsset *asset ) {
                NSLog(@"we have our ALAsset!");
            } failureBlock:^(NSError *error ) {
                NSLog(@"Error loading asset");
            }];
        }];
    }
}
-(NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for (i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] ;
}

#pragma mark - load data
- (void) checkBookportType:(int)bookportType {
    switch (bookportType) {
        case 0:
            [self loadCapType:0];
            break;
        case 1:
            [self loadCapType:2]; //FTTH
            break;
        default:
            break;
    }
    if(self.arrRecord.TDName.length > 0  && [self.arrRecord.TDName componentsSeparatedByString:@"/"].count > 1)
        [self loadCapType:1];//FTTH New
}

- (void) loadCapType:(int)bookportType {
    self.arrCapType = [NSMutableArray array];
    KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"52" description:@"1 core"];
    KeyValueModel *s2 = [[KeyValueModel alloc] initWithName:@"62" description:@"2 core"];
    //KeyValueModel *s3 = [[KeyValueModel alloc] initWithName:@"72" description:@"4 core"];
    KeyValueModel *s4 = [[KeyValueModel alloc] initWithName:@"812" description:@"0.5 mm"];
    switch (bookportType) {
        case 0: // ADSL
            self.arrCapType = [NSMutableArray arrayWithObject:s4];
            break;
        case 1: // FTTH New
            self.arrCapType = [NSMutableArray arrayWithObject:s1];

            break;
        case 2: // FTTH
            self.arrCapType = [NSMutableArray arrayWithObject:s2];

            break;
        default:
            break;
    }
    selectedCapType = [self.arrCapType objectAtIndex:0];
    [self.btnCapType setTitle:selectedCapType.Values forState:UIControlStateNormal];
}

-(void)LoadIndoor {
    self.arrIndoor = [Common getIndoor];
    if(self.arrRecord.InDType != nil){
        for (int i = 0; i < self.arrIndoor.count; i++) {
            selectedIndoor = [self.arrIndoor objectAtIndex:i];
            if([self.arrRecord.InDType isEqualToString:selectedIndoor.Key]){
                [self.btnIndoor setTitle:selectedIndoor.Values forState:UIControlStateNormal];
                return;
            }
        }
    }
    selectedIndoor = [self.arrIndoor objectAtIndex:0];
    [self.btnIndoor setTitle:selectedIndoor.Values forState:UIControlStateNormal];
}

-(void)LoadOutdoor {
    self.arrOutdoor = [Common getOutdoor];
    if(self.arrRecord.OutDType != nil){
        for (int i = 0; i < self.arrOutdoor.count; i++) {
            selectedOutdoor = [self.arrOutdoor objectAtIndex:i];
            if([self.arrRecord.OutDType isEqualToString:selectedOutdoor.Key]){
                [self.btnOutdoor setTitle:selectedOutdoor.Values forState:UIControlStateNormal];
                return;
            }
        }
    }

    selectedOutdoor = [self.arrOutdoor objectAtIndex:0];
    [self.btnOutdoor setTitle:selectedOutdoor.Values forState:UIControlStateNormal];
    
}

-(void)LoadMapCode {
    self.arrMapCode = [Common getCodeMap];
    if(self.arrRecord.MapCode != nil){
        for (int i = 0; i < self.arrMapCode.count; i++) {
            selectedMapCode = [self.arrMapCode objectAtIndex:i];
            if([self.arrRecord.MapCode isEqualToString:selectedMapCode.Key]){
                [self.btnCodeMap setTitle:selectedMapCode.Values forState:UIControlStateNormal];
                return;
            }
        }
    }
    selectedMapCode= [self.arrMapCode objectAtIndex:0];
    [self.btnCodeMap setTitle:selectedMapCode.Values forState:UIControlStateNormal];
}

-(void)LoadModem {
    self.arrModem = [Common getModem];
    if(self.arrRecord.Modem != nil){
        for (int i = 0; i < self.arrModem.count; i++) {
            selectedModem = [self.arrModem objectAtIndex:i];
            if([self.arrRecord.Modem isEqualToString:selectedModem.Key]){
                [self.btnModem setTitle:selectedModem.Values forState:UIControlStateNormal];
                return;
            }
        }
    }
    selectedModem = [self.arrModem objectAtIndex:0];
    [self.btnModem setTitle:selectedModem.Values forState:UIControlStateNormal];
}

#pragma  mark - delegate popup
-(void)CancenPopupSurveyMap{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
}

//MARK: DELEGATE

- (IBAction)editingOutDoor:(id)sender {
    
    NSInteger numOutDoor = [self.txtOutdoor.text integerValue];
    
    if (numOutDoor <= 0) {
        self.txtOutdoor.text = @"0";
    } else {
        NSString *strNumber = [NSString stringWithFormat:@"%ld",(long)numOutDoor];
        self.txtOutdoor.text = strNumber;
    }
    
}

- (IBAction)editingInDoor:(id)sender {
    
    NSInteger numInDoor = [self.txtIndoor.text integerValue];
    
    if (numInDoor <= 0) {
        self.txtIndoor.text = @"0";
    } else {
        NSString *strNumber = [NSString stringWithFormat:@"%ld",(long)numInDoor];
        self.txtIndoor.text = strNumber;
    }
    
}

@end
