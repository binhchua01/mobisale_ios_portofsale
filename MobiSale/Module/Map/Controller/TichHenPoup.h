//
//  TichHenPoupPoup.h
//  MobiSale
//
//  Created by Tan Tho Nguyen on 7/27/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BaseViewController.h"

@protocol TichHenPoup <NSObject>
-(void)CancelTichHenPoup;
-(void)UpdateDeployAppointment:(NSMutableDictionary *)dic;
-(void)callReportListDeploymentAPI;
@end

@interface TichHenPoup : BaseViewController


@property (weak, nonatomic) IBOutlet UIButton *drop_doiTac;
@property (weak, nonatomic) IBOutlet UIButton *drop_toCon;
@property (weak, nonatomic) IBOutlet UIButton *drop_muiGio;
@property (weak, nonatomic) IBOutlet UIButton *lbl_NgayHen;
@property (weak, nonatomic) IBOutlet UIWebView *webViewAbility;

- (IBAction)drop_doiTac_clicked:(id)sender;
- (IBAction)drop_toCon_clicked:(id)sender;
- (IBAction)drop_muiGio_clicked:(id)sender;

- (IBAction)drop_NgayHen:(id)sender;

- (IBAction)btn_search:(id)sender;
- (IBAction)btnCancel:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnUpdate;
@property (weak, nonatomic) IBOutlet UIButton *btnNangluc;

//RegCode
@property (strong, nonatomic) NSString *RegCode;
@property (strong, nonatomic) NSArray *arrList;
@property (strong, nonatomic) NSMutableArray *arrDoiTac;
@property (strong, nonatomic) NSMutableArray *arrDoiTacCon;
@property (strong, nonatomic) NSMutableArray *arrMuiGio;
@property (nonatomic) NSInteger partnerID,subTeam;

@property (retain, nonatomic) id<TichHenPoup>delegate;


@end
