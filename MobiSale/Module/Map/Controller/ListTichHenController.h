//
//  ReportListSBIController.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 2/4/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BaseViewController.h"


@protocol ListTichHenControllerDelegate <NSObject>

-(void)CancelPopup;

@end

@interface ListTichHenController : BaseViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *viewpage;

-(IBAction)btnCancel_clicked:(id)sender;

@property (strong, nonatomic) NSMutableArray *arrList;


@property (retain, nonatomic) id<ListTichHenControllerDelegate>delegate;

@end
