//
//  NSDate+Utils.m
//  MobiSale
//
//  Created by Tan Tho Nguyen on 8/10/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "NSDate+Utils.h"

@implementation NSDate_Utils
-(NSDate *) toLocalTime
{
    NSTimeZone *tz = [NSTimeZone localTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate: (NSDate*)self];
    return [NSDate dateWithTimeInterval: seconds sinceDate: (NSDate*)self];
}

-(NSDate *) toGlobalTime
{
    NSTimeZone *tz = [NSTimeZone localTimeZone];
    NSInteger seconds = -[tz secondsFromGMTForDate: (NSDate*)self];
    return [NSDate dateWithTimeInterval: seconds sinceDate: (NSDate*)self];
}
@end
