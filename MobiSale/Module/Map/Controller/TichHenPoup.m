//
//  TichHenPoupPoup.m
//  MobiSale
//
//  Created by Tan Tho Nguyen on 7/27/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "TichHenPoup.h"
#import "UIPopoverListView.h"
#import "ShareData.h"
#import "KeyValueModel.h"
#import "Common.h"
#import "NSDate+Utils.h"
@import Foundation;
@interface TichHenPoup ()

@end

@implementation TichHenPoup{
    
    KeyValueModel  *selectedDoiTac,*selectedDoiTacCon,*selectedMuiGio;
    NSString *salename;
    UIDatePicker *datePicker;
    NSString *chooseExpectedDate;
}
@synthesize partnerID,subTeam;
enum tableSourceStype {
    DoiTac= 4,
    DoiTacCon=5,
    MuiGio=6
};

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"HẸN TRIỂN KHAI";
    
    CGRect datePickerFrame = CGRectMake(0,0, self.view.frame.size.width, 200);
    datePicker = [[UIDatePicker alloc] initWithFrame:datePickerFrame];
    datePicker.datePickerMode = UIDatePickerModeDate;
    
    // Do any additional setup after loading the view from its nib.
    [self limitDate];
    [self setSelectedDateInField];
    [datePicker addTarget:self action:@selector(setSelectedDateInField) forControlEvents:UIControlEventValueChanged];
    [self loadDataInput];
    [self stypeView];
    
}
-(void)stypeView {
    
    self.btnUpdate.layer.cornerRadius = 5;
    self.btnNangluc.layer.cornerRadius = 5;
}
-(void)loadDataInput{
    [self LoadDoiTac];
    [self LoadDoiTacCon];
    [self LoadMuiGio];

}
- (void) limitDate{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:0];
    NSDate *minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    [datePicker setMinimumDate:minDate];
}
-(void)setSelectedDateInField{

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    [self.lbl_NgayHen setTitle:[dateFormatter stringFromDate:datePicker.date] forState:UIControlStateNormal];
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    chooseExpectedDate = [dateFormatter stringFromDate:datePicker.date];
    
}
-(void)showDatePicker:(UIDatePicker *)modeDatePicker{
    
    UIView *viewDatePicker = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width+30, 200)];
    [viewDatePicker setBackgroundColor:[UIColor clearColor]];
    
    modeDatePicker.hidden = NO;
    modeDatePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"Vietnamese"];
    
    [viewDatePicker addSubview:modeDatePicker];
    
    if(IS_OS_8_OR_LATER){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"\n\n\n\n\n\n\n\n\n\n" preferredStyle:UIAlertControllerStyleActionSheet];
        [alertController.view addSubview:viewDatePicker];
        
        UIAlertAction *doneAction = [UIAlertAction actionWithTitle:@"Xong" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){

        }];
        [alertController addAction:doneAction];
        [self.view.window.rootViewController presentViewController:alertController animated:YES completion:nil];
        //[self presentViewController:alertController animated:YES completion:nil]; //error iOS 9
    }
    else{
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"\n\n\n\n\n\n\n\n\n\n" delegate:(id)self cancelButtonTitle:nil destructiveButtonTitle:@"Xong" otherButtonTitles:nil, nil];
        [actionSheet addSubview:viewDatePicker];
        [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
    }
}


#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row cell:cell tag:popoverListView.tag];
    return cell;
}

#pragma mark - UIPopoverListViewDataSource

- (void) setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell tag:(NSInteger)tag{
    KeyValueModel *model;
    switch (tag) {
        case DoiTac:
            model = [self.arrDoiTac objectAtIndex:row];
            cell.textLabel.text = [[model.Values componentsSeparatedByString:@"+xxxx+"] objectAtIndex:0];
            break;
        case DoiTacCon:
            model = [self.arrDoiTacCon objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case MuiGio:
                model = [self.arrMuiGio objectAtIndex:row];
               cell.textLabel.text = model.Values;
            break;
        default:
            break;
    }
    
    
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    switch (popoverListView.tag) {
        case DoiTac:
            count = self.arrDoiTac.count;
            break;
        case DoiTacCon:
            count = self.arrDoiTacCon.count;
            break;
        case MuiGio:
            count = self.arrMuiGio.count;
            break;
        default:
            break;
    }
    return count;
}

#pragma mark - UIPopoverListViewDelegate

- (void) didDropDownSelected:(NSInteger)row tag:(NSInteger)tag{
    
    switch (tag) {
        case DoiTac:
            selectedDoiTac = [self.arrDoiTac objectAtIndex:row];
            [self.drop_doiTac setTitle:[[selectedDoiTac.Values componentsSeparatedByString:@"+xxxx+"] objectAtIndex:0] forState:UIControlStateNormal];
            break;
        case DoiTacCon:
            selectedDoiTacCon = [self.arrDoiTacCon objectAtIndex:row];
            [self.drop_toCon setTitle:selectedDoiTacCon.Values forState:UIControlStateNormal];
            break;
        case MuiGio:
             selectedMuiGio = [self.arrMuiGio objectAtIndex:row];
             [self.drop_muiGio setTitle:selectedMuiGio.Values forState:UIControlStateNormal];
            break;
        default:
            break;
    }
    
    
}
#pragma mark - UIPopoverListViewDelegate

- (void)popoverListView:(UIPopoverListView *)popoverListView
     didSelectIndexPath:(NSIndexPath *)indexPath
{
    [self didDropDownSelected:indexPath.row tag:popoverListView.tag];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}


- (void)setupDropDownView:(NSInteger) tag {
    
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.tag = tag;
    poplistview.listView.scrollEnabled = TRUE;
    switch (tag) {
        case DoiTac:
            [poplistview setTitle:NSLocalizedString(@"Chọn đối tác", @"")];
            break;
        case DoiTacCon:
            [poplistview setTitle:NSLocalizedString(@"Chọn đối tác con", @"")];
            break;
        case MuiGio:
            [poplistview setTitle:NSLocalizedString(@"Chọn múi giờ", @"")];
            break;
        default:
            break;
    }
    
    [poplistview show];
    
}
- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35.0f;
}

-(void)LoadDoiTac{
     NSString *name =@"";
    self.arrDoiTac = [NSMutableArray array];
    for (int i = 0; i < [self.arrList count]; i++) {
        KeyValueModel *s;
        name =@"";
        name =[NSString stringWithFormat:@"%@+xxxx+%@",[[self.arrList objectAtIndex:i] objectForKey:@"Partner"],[[self.arrList objectAtIndex:i] objectForKey:@"PartnerID"]];
        s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",i] description:name];
        [self.arrDoiTac addObject: s];
    }
    selectedDoiTac= [self.arrDoiTac objectAtIndex:0];
    [self.drop_doiTac setTitle:[[selectedDoiTac.Values componentsSeparatedByString:@"+xxxx+"] objectAtIndex:0] forState:UIControlStateNormal];

  //  [self.drop_doiTac setTitle:selectedDoiTac.Values forState:UIControlStateNormal];
}
-(void)LoadDoiTacCon {
    
    self.arrDoiTacCon = [NSMutableArray array];
    for (int i = 0; i < [self.arrList count]; i++) {
        KeyValueModel *s;
        s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",i] description:[NSString stringWithFormat:@"%@",[[self.arrList objectAtIndex:i] objectForKey:@"SubTeam"]]];
        [self.arrDoiTacCon addObject: s];
    }
    selectedDoiTacCon= [self.arrDoiTacCon objectAtIndex:0];
    [self.drop_toCon setTitle:selectedDoiTacCon.Values forState:UIControlStateNormal];
}

-(void)LoadMuiGio {
    self.arrMuiGio = [NSMutableArray array];
    KeyValueModel *s;
//    s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",1] description:[NSString stringWithFormat:@"%@",@"08:00 - 12:00"]];
//    [self.arrMuiGio addObject: s];
//    s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",2] description:[NSString stringWithFormat:@"%@",@"13:30 - 17:30"]];
    s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",1] description:@"08:00 - 12:00"];
    [self.arrMuiGio addObject: s];
    s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",2] description:@"13:30 - 17:30"];
    [self.arrMuiGio addObject: s];
    selectedMuiGio= [self.arrMuiGio objectAtIndex:0];
    [self.drop_muiGio setTitle:selectedMuiGio.Values forState:UIControlStateNormal];
    
}


- (IBAction)drop_doiTac_clicked:(id)sender {
     [self setupDropDownView:DoiTac];
}

- (IBAction)drop_toCon_clicked:(id)sender {
    [self setupDropDownView:DoiTacCon];

}

- (IBAction)drop_muiGio_clicked:(id)sender {
    [self setupDropDownView:MuiGio];
}

- (IBAction)drop_NgayHen:(id)sender {
    [self showDatePicker:datePicker];
}

- (IBAction)btn_search:(id)sender {
    NSString *dateSecled = chooseExpectedDate;
    ShareData *shared=[ShareData instance];
    NSMutableDictionary *toReturn = [NSMutableDictionary dictionary];
    [toReturn setValue: shared.currentUser.userName forKey:@"UserName"];
    [toReturn setValue: [[selectedDoiTac.Values componentsSeparatedByString:@"+xxxx+"] objectAtIndex:1] forKey:@"DeptID"];
    [toReturn setValue: selectedDoiTacCon.Values forKey:@"SubID"];
    [toReturn setValue: dateSecled forKey:@"AppointmentDate"];
    [toReturn setValue: selectedMuiGio.Values forKey:@"Timezone"];
    [toReturn setValue: self.RegCode forKey:@"RegCode"];
    if(self.delegate){
        [self.delegate UpdateDeployAppointment:toReturn];
    }

}

- (IBAction)btnCancel:(id)sender {
    if(self.delegate){
        [self.delegate CancelTichHenPoup];
    }
}

- (IBAction)btnNangLucTK_clicked:(id)sender {
  
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    if (self.arrList.count > 0) {
        for (int i = 0; i< self.arrList.count; i++) {
            self.subTeam = [[self.arrList[i] valueForKey:@"SubTeam"] integerValue];
            self.partnerID = [[self.arrList[i]valueForKey:@"PartnerID"]integerValue ];
        }
        
        [dict setValue:StringFormat(@"%ld",self.partnerID) forKey:@"iPartnerID"];
        [dict setValue:StringFormat(@"%ld",self.subTeam) forKey:@"iSubID"];
    }
  [self showMBProcess];
    ShareData *shared = [ShareData instance];
    [shared.appProxy getAbility:dict Handler:^(NSArray* result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return ;
        }
        
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        NSString * stringHTML = [result valueForKey:@"Html"];
       [ self.webViewAbility loadHTMLString: stringHTML baseURL:nil];
         [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",[error localizedDescription]]];
    }];
    
    
}




@end
