//
//  MainMap.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 1/26/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BaseViewController.h"
#import "BookPortADSL.h"
#import "BookPortFTTHNew.h"
#import "BookPortFTTH.h"
#import "RegistrationFormDetailRecord.h"
#import "Survey.h"


@interface MainMap : BaseViewController<GMSMapViewDelegate,CLLocationManagerDelegate,BookPortFTTHDelegate,BookPortFTTHNewDelegate,BookPortADSLDelegate,SurveyDelegate,UIAlertViewDelegate>

@property (strong, nonatomic) RegistrationFormDetailRecord *record;

@property GMSMapView *mapView_;
@property(nonatomic,retain) CLLocationManager *locationManager;

@property (strong,nonatomic) IBOutlet UIButton *btnRecovery;
@property (strong,nonatomic) IBOutlet UIButton *btn_changeMapType;
@property (strong,nonatomic) IBOutlet UIButton *btnBookPort;
@property (strong,nonatomic) IBOutlet UIButton *btnSurvey;
@property (strong,nonatomic) IBOutlet UIButton *btnType;
@property (strong,nonatomic) IBOutlet UIButton *btnFind;
@property (strong,nonatomic) IBOutlet UITextField *txtTextSearch;
@property (strong,nonatomic) IBOutlet UIView *mView;
@property (strong,nonatomic) IBOutlet UIView *mViewButton;
@property (strong,nonatomic) IBOutlet UIView *mViewMenu;

-(IBAction)btnType_Clicked:(id)sender;
-(IBAction)btnFind_Clicked:(id)sender;
//-(IBAction)btn_changeMapType:(id)sender;
-(IBAction)btnBookPort_Clicked:(id)sender;
- (IBAction)btnRecovery_Clicked:(id)sender;
-(IBAction)btnSurvey_Clicked:(id)sender;
-(IBAction)btnFindTD_Clicked:(id)sender;
-(IBAction)btnTypeMap_Clicked:(id)sender;
- (IBAction)btn_TichHen_Clicked:(id)sender;

@property (strong,nonatomic) NSMutableArray *arrList;
@property (strong,nonatomic) NSString *Flat;

@property (strong,nonatomic) NSString *txtLocationPB;
@property (strong,nonatomic) RegistrationFormDetailRecord *arrRecord;

@end
