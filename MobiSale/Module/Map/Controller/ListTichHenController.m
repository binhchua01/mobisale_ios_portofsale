//
//  ReportListDeploymentN.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 2/4/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ContactReportDeploymentRecord.h"
#import "UIPopoverListView.h"
#import "ShareData.h"
#import "KeyValueModel.h"
#import "ReportSBIDetailsViewController.h"
#import "ListTichHenCell.h"
#import "ListTichHenController.h"


@interface ListTichHenController ()

@end

@implementation ListTichHenController{
    KeyValueModel *selectedPage;
    int constRepost;
    NSString *nameSaler;
    NSString *keySecleted;
    NSString *totalPage;
}
@dynamic tableView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"DANH SÁCH HẸN TRIỂN KHAI";
    self.screenName=self.title;
    self.tableView.separatorColor = [UIColor clearColor];
    
    [self.tableView reloadData];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews{
    self.tableView.frame =CGRectMake(0, 62, self.view.frame.size.width, [SiUtils getScreenFrameSize].height  - 78);
}

#pragma mark - TableView


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.arrList.count > 0){
        return self.arrList.count;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 248;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *simpleTableIdentifier = @"ListTichHenCell";
    
    ListTichHenCell *cell = (ListTichHenCell *)[self.tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
    cell = [nib objectAtIndex:0];
    if ([self.arrList count] >=indexPath.row) {
        long cout =indexPath.row+1;
        NSDictionary *tick = [NSMutableDictionary dictionary];
        tick =[self.arrList objectAtIndex:indexPath.row];
        cell.lbl_NhanVien.text=[tick objectForKey:@"Account"];
        cell.lbl_NgayTao.text=[tick objectForKey:@"Date"];
        cell.lbl_NgayHen.text=[tick objectForKey:@"AppointmentDate"];
        cell.lbl_DoiTac.text=StringFormat(@"%@ (%@)",[tick objectForKey:@"Partner"],[tick objectForKey:@"PartnerID"]);
        cell.lbl_PhieuDangKy.text=[NSString stringWithFormat:@"%@",[tick objectForKey:@"RegCode"]];
        cell.lbl_PhieuThiCong.text=[NSString stringWithFormat:@"%@",[tick objectForKey:@"SupID"]];
        cell.lbl_ThoiGianThiCong.text=[tick objectForKey:@"Timezone"];
        cell.lbl_SoHoDong.text=[tick objectForKey:@"Contract"];
        cell.lbl_toCon.text=[NSString stringWithFormat:@"%@",[tick objectForKey:@"SubID"]];
        
        cell.lbl_STT.text=[NSString stringWithFormat:@"%ld",cout];
    }
    cell.backgroundColor = [UIColor clearColor];
    return cell;
    
}
#pragma mark Table DidSelected
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}

-(IBAction)btnCancel_clicked:(id)sender
{
    if(self.delegate){
        [self.delegate CancelPopup];
    }
    [ShareData instance].repostSBIList=nil;
}
@end
