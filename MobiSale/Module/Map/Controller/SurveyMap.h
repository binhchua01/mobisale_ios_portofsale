//
//  MainMap.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 1/26/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BaseViewController.h"
#import "BookPortADSL.h"
#import "BookPortFTTHNew.h"
#import "BookPortFTTH.h"
#import "RegistrationFormDetailRecord.h"


@protocol SurveyMapDelegate1 <NSObject>

-(void)CancenPopupSurveyMap;

@end

@interface SurveyMap : BaseViewController<GMSMapViewDelegate,CLLocationManagerDelegate>

@property GMSMapView *mapView_;
@property(nonatomic,retain) CLLocationManager *locationManager;

@property (strong,nonatomic) IBOutlet UIView *mView;
@property (strong,nonatomic) IBOutlet UIView *mViewButton;
@property (strong, nonatomic) IBOutlet UIView *mViewAnnotation;

@property (strong, nonatomic) IBOutlet UILabel *lblCapType;
@property (strong, nonatomic) IBOutlet UILabel *lblIndoor;
@property (strong, nonatomic) IBOutlet UILabel *lblOutdoor;

@property (strong, nonatomic) NSString *capType;
@property (strong, nonatomic) NSString *indoor;
@property (strong, nonatomic) NSString *outdoor;

-(IBAction)btnDone_Clicked:(id)sender;
-(IBAction)btnDelete_Clicked:(id)sender;

@property (retain, nonatomic) id<SurveyMapDelegate1> delegate;

@property (strong,nonatomic) RegistrationFormDetailRecord *arrRecord;

@end
