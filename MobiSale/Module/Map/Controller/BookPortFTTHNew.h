//
//  BookPortFTTHNew.h
//  MobiSale
//
//  Created by  Nguyen Tan Tho on 2/5/15.
//  Copyright (c) 2015 FPT.RAD.MOBISALE. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "../../../Helper/MBProgress/MBProgressHUD.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "BaseViewController.h"
//#import "../../../MobiSale/BaseViewController.m"

@protocol BookPortFTTHNewDelegate <NSObject>

- (void)cancelPopupBookPortFTTHNew;
- (void)backToDetailRegisteredForm;

@end

@interface BookPortFTTHNew : BaseViewController

@property (nonatomic, retain) id<BookPortFTTHNewDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIButton *btnCanel;
@property (strong, nonatomic) IBOutlet UIButton *btnPort;
@property (strong, nonatomic) IBOutlet UIButton *btnRefresh;
@property (strong, nonatomic) IBOutlet UIButton *btnUpdate;
@property (strong, nonatomic) IBOutlet UIButton *btnRecovery;
@property (strong, nonatomic) IBOutlet UITextField *txtPop;
@property (strong, nonatomic) IBOutlet UITextField *txtTD;
@property (strong, nonatomic) IBOutlet UITextField *txtLocation;
@property (strong, nonatomic) IBOutlet UITextField *txtBranch;
@property (strong, nonatomic) IBOutlet UILabel *lblTotalPort;


@property (strong, nonatomic) IBOutlet UITextField *txtRegis;


-(IBAction)btnCancel_clicked:(id)sender;
-(IBAction)btnPort_clicked:(id)sender;
-(IBAction)btnUpdate_clicked:(id)sender;
-(IBAction)btnRecovery_clicked:(id)sender;

@property (strong, nonatomic) NSMutableArray *arrPort;
@property (strong, nonatomic) NSString *TdName;
@property (strong, nonatomic) NSString *Type;
@property (strong, nonatomic) NSString *Regcode;
@property (strong, nonatomic) NSString *Latlng;
@property (strong, nonatomic) NSString *ODCCableType;
@property (strong, nonatomic) NSString *latlngDevice;

@end
