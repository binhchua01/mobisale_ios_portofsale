//
//  PortFreeRecord.h
//  MobiSale
//
//  Created by HIEUPC on 4/21/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PortFreeRecord : NSObject

@property (strong, nonatomic) NSString *ID;
@property (strong, nonatomic) NSString *PortID;

@end
