//
//  MapProxy.h
//  MobiSale
//
//  Created by HIEUPC on 2/5/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BaseProxy.h"

@interface MapProxy : BaseProxy

- (void)GetTapdiemLocation:(NSString*)latlng Type:(NSString *)type Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)GetLocationBranchPOP:(NSString *)tdname Type:(NSString *)type  Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)GetPortFreeFTTHNEW:(NSString *)tdname  Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler ;

- (void)GetPercenTDFTTH:(NSString*)tdname Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)GetPortFreeSwitchCardFTTH:(NSString*)tdname Type:(NSString *)type Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)GetPortFreeFTTH:(NSString*)tdname Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)GetPortFreeFirstTD:(NSString*)tdname Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)BookPortFTTHNEW:(NSString *)port RegCode:(NSString *)regcode TDName:(NSString *)tdname UserName:(NSString *)username IP:(NSString *)ip Latlng:(NSString *)latlng LatlngDevice:(NSString *)latlngDevice   Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)BookPortFTTH:regcode CardNumber:(NSString *)cardnumber UserName:(NSString *)username TDID:(NSString *)tdid IDCoreSC:(NSString *)idcoresc IDCoreTD:(NSString *)idcoretd TDName:(NSString *)tdname PortTextTD:(NSString *)porttexttd LocationID:(NSString *)locationid BranchID:(NSString *)branchid IP:(NSString *)ip TypeCore:(NSString *)typecore TypeContract:(NSString *)typecontract Latlng:(NSString *)latlng LatlngDevice:(NSString *)latlngDevice Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)BookPortADSL:locationid BranchID:(NSString *)branchid UserName:(NSString *)username TDID:(NSString *)tdid TypeSevice:(NSString *)typesevice RegCode:(NSString *)regcode CardNumber:(NSString *)cardnumber  IDPortTD:(NSString *)idporttd IP:(NSString *)ip TDName:(NSString *)tdname  Latlng:(NSString *)latlng LatlngDevice:(NSString *)latlngDevice Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)GetSumPortTCUseADSL:(NSString*)typeservice CountPortTD:(NSString *)countporttd TDName:(NSString *)tdname Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler ;

- (void)UpdateSurvey:(NSString*)regcode OutDType:(NSString *)outdtype OutDoor:(NSString *)outdoor InDType:(NSString *)indtype InDoor:(NSString *)indoor Image:(NSString *)image MapCode:(NSString *)mapcode Modem:(NSString *)modem TDName:(NSString *)tdname UserName:(NSString *)username Latlng:(NSString *)latlng Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)RecoverRegistration:(NSString*)type RegCode:(NSString *)regcode CreateBy:(NSString *)createby IP:(NSString *)ip  Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)UploadImageInvest:(NSString*)image userName:(NSString *)username Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

@end
