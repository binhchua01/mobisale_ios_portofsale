//
//  PortRecord.h
//  MobiSale
//
//  Created by HIEUPC on 2/6/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PortRecord : NSObject

@property (nonatomic,retain) NSString *BranchID;
@property (nonatomic,retain) NSString *LocationID;
@property (nonatomic,retain) NSString *LocationName;
@property (nonatomic,retain) NSString *NameBranch;
@property (nonatomic,retain) NSString *PopName;
@property (nonatomic,retain) NSString *TDID;

@end
