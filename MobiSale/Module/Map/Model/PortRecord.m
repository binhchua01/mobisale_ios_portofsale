//
//  PortRecord.m
//  MobiSale
//
//  Created by HIEUPC on 2/6/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "PortRecord.h"

@implementation PortRecord

@synthesize BranchID;
@synthesize LocationID;
@synthesize LocationName;
@synthesize NameBranch;
@synthesize PopName;
@synthesize TDID;

@end
