//
//  MapProxy.m
//  MobiSale
//
//  Created by HIEUPC on 2/5/15.
//  Copyright (c) 2015 FPT.RAD.MOBISALE. All rights reserved.
//

#import "MapProxy.h"
#import "ShareData.h"
#import "MapRecord.h"
#import "LocationBranchPopRecord.h"
#import "PortFreeRecord.h"
#import "FPTUtility.h"

#define mGetTapdiemLocation             @"GetTapdiemLocation"
#define mGetLocationBranchPOP           @"GetLocationBranchPOP"
#define mGetPortFreeFTTHNEW             @"GetPortFreeFTTHNEW"
#define mGetPercenTDFTTH                @"GetPercenTDFTTH"
#define mGetPortFreeSwitchCardFTTH      @"GetPortFreeSwitchCardFTTH"
#define mGetPortFreeFTTH                @"GetPortFreeFTTH"
#define mGetPortFreeFirstTD             @"GetPortFreeFirstTD"
#define mBookPortFTTHNEW                @"BookPortFTTHNEW"
#define mBookPortFTTH                   @"BookPortFTTH"
#define mBookPortADSL                   @"BookPortADSL"
#define mGetSumPortTCUseADSL            @"GetSumPortTCUseADSL"
#define mUpdateSurvey                   @"UpdateSurvey"
#define mRecoverRegistration            @"RecoverRegistration"
#define mUploadImageInvest              @"UploadImageInvest"

@implementation MapProxy

#pragma mark - Get List Prechecklist
- (void)GetTapdiemLocation:(NSString*)latlng Type:(NSString *)type Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetTapdiemLocation)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    NSString *username = [ShareData instance].currentUser.userName;
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:username forKey:@"UserName"];
    [dict setObject:latlng forKey:@"Latlng"];
    [dict setObject:type forKey:@"Type"];
    
     postString = [dict JSONRepresentation];
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];

    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetTapdiemLocation:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
    
}
- (void)endGetTapdiemLocation:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }

    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSArray *arr = [jsonDict objectForKey:@"GetTapdiemLocationMethodPostResult"]?:nil;
    
    if(arr.count <= 0){
         handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    if(![ErrorService isEqualToString:@"<null>"]){
        //NSString *message = StringFormat(@"%@",[root objectForKey:@"Message"]); ;
        handler(nil, ErrorService, ErrorService);
        return;
    }
 
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        MapRecord *p = [self ParseRecordList:d];
        [mArray addObject:p];
    }
    handler(mArray, ErrorService, @"");
    
    
}

-(MapRecord *) ParseRecordList:(NSDictionary *)dict{
    MapRecord *rc = [[MapRecord alloc]init];
    
    rc.Cabtype   = StringFormat(@"%@",[dict objectForKey:@"Cabtype"]);
    rc.Latlng    = StringFormat(@"%@",[dict objectForKey:@"Latlng"]);
    rc.PortDie   = StringFormat(@"%@",[dict objectForKey:@"PortDie"]);
    rc.PortFree  = StringFormat(@"%@",[dict objectForKey:@"PortFree"]);
    rc.PortTotal = StringFormat(@"%@",[dict objectForKey:@"PortTotal"]);
    rc.PortUsed  = StringFormat(@"%@",[dict objectForKey:@"PortUsed"]);
    rc.TDName    = StringFormat(@"%@",[dict objectForKey:@"TDName"]);
    rc.TDType    = StringFormat(@"%@",[dict objectForKey:@"TDType"]);
    rc.UserName  = StringFormat(@"%@",[dict objectForKey:@"UserName"]);
    rc.Address   = StringFormat(@"%@",[dict objectForKey:@"Address"]);
    rc.Distance  = StringFormat(@"%@",[dict objectForKey:@"Distance"]);
    rc.PortFreeRatio = StringFormat(@"%@",[dict objectForKey:@"PortFreeRatio"]);
    if ([rc.Address isEqualToString:@"(null)"]) {
        rc.Address = @"";
    }
    if ([rc.Distance isEqualToString:@"(null)"]) {
        rc.Distance = @"";
    }
    return rc;
}


#pragma mark - Get list location book port
- (void)GetLocationBranchPOP:(NSString *)tdname Type:(NSString *)type  Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetLocationBranchPOP)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:tdname forKey:@"TDName"];
    [dict setObject:type forKey:@"Type"];
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetLocationBranchPOP:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}
- (void)endGetLocationBranchPOP:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }

    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSArray *arr = [jsonDict objectForKey:@"GetLocationBranchPOPResult"];
    
    if(arr.count <= 0){
        handler(nil, @"", @"Không có dữ liệu trả về");
        return;
    }
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    if(![ErrorService isEqualToString:@"<null>"]){
        NSString *message = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"Error"]); ;
        handler(nil, ErrorService, message);
        return;
    }

    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        LocationBranchPopRecord *p = [self ParseRecordLocationBranchPop:d];
        [mArray addObject:p];
    }
    handler(mArray, ErrorService, @"");
}

-(LocationBranchPopRecord *) ParseRecordLocationBranchPop:(NSDictionary *)dict{
    LocationBranchPopRecord *rc = [[LocationBranchPopRecord alloc]init];
    
    rc.BranchID = StringFormat(@"%@",[dict objectForKey:@"BranchID"]);
    rc.LocationID = StringFormat(@"%@",[dict objectForKey:@"LocationID"]);
    rc.LocationName = StringFormat(@"%@",[dict objectForKey:@"LocationName"]);
    rc.NameBranch = StringFormat(@"%@",[dict objectForKey:@"NameBranch"]);
    rc.PopName = StringFormat(@"%@",[dict objectForKey:@"PopName"]);
    rc.TDID = StringFormat(@"%@",[dict objectForKey:@"TDID"]);
    
    
    return rc;
}


#pragma mark - Get list GetPortFreeFTTHNEW
- (void)GetPortFreeFTTHNEW:(NSString *)tdname  Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetPortFreeFTTHNEW)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:tdname forKey:@"TDName"];
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetPortFreeFTTHNEW:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}
- (void)endGetPortFreeFTTHNEW:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }

    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSArray *arr = [jsonDict objectForKey:@"GetPortFreeFTTHNEWResult"];
    if(arr.count <= 0){
        handler(nil, @"", @"");
        return;
    }
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    
    if(![ErrorService isEqualToString:@"<null>"]){
        NSString *message = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"Error"]); ;
        handler(nil, ErrorService, message);
        return;
    }

    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        PortFreeRecord *p = [[PortFreeRecord alloc]init];
        p =[self ParseRecordPortFree:d];
        [mArray addObject:p];
    }
    handler(mArray, ErrorService, @"");
}

-(PortFreeRecord *) ParseRecordPortFree:(NSDictionary *)dict{
    PortFreeRecord *rc = [[PortFreeRecord alloc]init];
    
    rc.ID = StringFormat(@"%@",[dict objectForKey:@"ID"]);
    rc.PortID = StringFormat(@"%@",[dict objectForKey:@"PortID"]);
    return rc;
}

#pragma mark - Get percent TDFTTH

- (void)GetPercenTDFTTH:(NSString*)tdname Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetPercenTDFTTH)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:tdname forKey:@"TDName"];
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
 
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetPercenTDFTTH:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
    
}
- (void)endGetPercenTDFTTH:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    
    NSArray *arr = [jsonDict objectForKey:@"GetPercenTDFTTHMethodPostResult"]?:nil;
    
    if(arr.count <= 0){
        return;
    }
    
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    
    if(![ErrorService isEqualToString:@"<null>"]){
        //NSString *message = StringFormat(@"%@",[root objectForKey:@"Message"]); ;
        handler(nil, ErrorService, ErrorService);
        return;
    }

    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        [mArray addObject:d];
    }
    handler(mArray, ErrorService, @"");
}

#pragma mark - GetPortFreeSwitchCardFTTH
- (void)GetPortFreeSwitchCardFTTH:(NSString*)tdname Type:(NSString *)type Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetPortFreeSwitchCardFTTH)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:tdname forKey:@"TDName"];
    [dict setObject:type forKey:@"Type"];

    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];

    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetPortFreeSwitchCardFTTH:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
    
}
- (void)endGetPortFreeSwitchCardFTTH:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    
    NSArray *arr = [jsonDict objectForKey:@"GetPortFreeSwitchCardFTTHMethodPostResult"]?:nil;
    
    if(arr.count <= 0){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    if(![ErrorService isEqualToString:@"<null>"]){
        //NSString *message = StringFormat(@"%@",[root objectForKey:@"Message"]); ;
        handler(nil, ErrorService, ErrorService);
        return;
    }
 
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        PortFreeRecord *p = [self ParseRecordPortFree:d];
        [mArray addObject:p];
    }
    handler(mArray, ErrorService, @"");
}

#pragma mark - GetPortFree FTTH
- (void)GetPortFreeFTTH:(NSString*)tdname Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetPortFreeFTTH)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:tdname forKey:@"TDID"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetPortFreeFTTH:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
    
}
- (void)endGetPortFreeFTTH:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    
    NSArray *arr = [jsonDict objectForKey:@"GetPortFreeFTTHMethodPostResult"]?:nil;
    
    if(arr.count <= 0){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    
    if(![ErrorService isEqualToString:@"<null>"]){
        //NSString *message = StringFormat(@"%@",[root objectForKey:@"Message"]); ;
        handler(nil, ErrorService, ErrorService);
        return;
    }
  
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        PortFreeRecord *p = [self ParseRecordPortFree:d];
        [mArray addObject:p];
    }
    handler(mArray, ErrorService, @"");
}

#pragma mark - GetPortFreeFirstTD
- (void)GetPortFreeFirstTD:(NSString*)tdname Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetPortFreeFirstTD)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:tdname forKey:@"TDName"];
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetPortFreeFirstTD:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
    
}
- (void)endGetPortFreeFirstTD:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }

    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSArray *arr = [jsonDict objectForKey:@"GetPortFreeFirstTDMethodPostResult"]?:nil;
    
    
    if(arr.count <= 0){
        return;
    }
    
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    if(![ErrorService isEqualToString:@"<null>"]){
        //NSString *message = StringFormat(@"%@",[root objectForKey:@"Message"]); ;
        handler(nil, ErrorService, ErrorService);
        return;
    }

    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        PortFreeRecord *p = [self ParseRecordPortFree:d];
        [mArray addObject:p];
    }
    handler(mArray, ErrorService, @"");
}

#pragma mark - Book Port FTTHNEW
- (void)BookPortFTTHNEW:(NSString *)port RegCode:(NSString *)regcode TDName:(NSString *)tdname UserName:(NSString *)username IP:(NSString *)ip Latlng:(NSString *)latlng LatlngDevice:(NSString *)latlngDevice   Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mBookPortFTTHNEW)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    if(latlng == nil){
        latlng = @"";
    }
    if (latlngDevice == nil) {
        latlngDevice = @"";
    }
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:tdname forKey:@"TDName"];
    [dict setObject:port forKey:@"Port"];
    [dict setObject:username forKey:@"UserName"];
    [dict setObject:ip forKey:@"IP"];
    [dict setObject:latlng forKey:@"Latlng"];
    [dict setObject:regcode forKey:@"RegCode"];
    [dict setObject:latlngDevice forKey:@"LatlngDevice"];
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];

    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endBookPortFTTHNEW:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}
- (void)endBookPortFTTHNEW:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSArray *arr = [jsonDict objectForKey:@"BookPortFTTHNEWResult"];
    
    if(arr.count <= 0){
        handler(nil, @"", @"");
        return;
    }
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    
    if(![ErrorService isEqualToString:@"<null>"]){
        NSString *message = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"Error"]); ;
        handler(nil, ErrorService, message);
        return;
    }
 
//    NSMutableArray *mArray = [NSMutableArray array];
//    for(NSDictionary *d in arr){
//        PortFreeRecord *p = [self ParseRecordPortFree:d];
//        [mArray addObject:p];
//    }
//    NSString *ResultID = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ResultID"]);
    NSString *Result = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"Result"]);
    handler(nil, ErrorService, Result);
    
    
}

#pragma mark - Book Port FTTH
- (void)BookPortFTTH:regcode CardNumber:(NSString *)cardnumber UserName:(NSString *)username TDID:(NSString *)tdid IDCoreSC:(NSString *)idcoresc IDCoreTD:(NSString *)idcoretd TDName:(NSString *)tdname PortTextTD:(NSString *)porttexttd LocationID:(NSString *)locationid BranchID:(NSString *)branchid IP:(NSString *)ip TypeCore:(NSString *)typecore TypeContract:(NSString *)typecontract Latlng:(NSString *)latlng LatlngDevice:(NSString *)latlngDevice Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mBookPortFTTH)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    if([typecontract isEqualToString:@"1"]){
        typecontract = @"FD";
    }else {
        typecontract = @"LL";
    }
    if(latlng == nil){
        latlng = @"";
    }
    
    if (latlngDevice == nil) {
        latlngDevice = @"";
    }
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:regcode forKey:@"RegCode"];
    [dict setObject:cardnumber forKey:@"CardNumber"];
    [dict setObject:username forKey:@"UserName"];
    [dict setObject:tdid forKey:@"TDID"];
    [dict setObject:idcoresc?:@"0" forKey:@"IDCoreSC"];
    [dict setObject:idcoretd?:@"0" forKey:@"IDCoreTD"];
    [dict setObject:porttexttd?:@"0" forKey:@"PortTextTD"];
    [dict setObject:locationid?:@"0" forKey:@"LocationID"];
    [dict setObject:branchid?:@"0" forKey:@"BranchID"];
    [dict setObject:ip?:@"0" forKey:@"IP"];
    [dict setObject:typecore?:@"0" forKey:@"TypeCore"];
    [dict setObject:typecontract?:@"0" forKey:@"TypeContract"];
    [dict setObject:tdname forKey:@"TDName"];
    [dict setObject:latlng forKey:@"Latlng"];
    [dict setObject:latlngDevice forKey:@"LatlngDevice"];
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endBookPortFTTH:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}
- (void)endBookPortFTTH:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSArray *arr = [jsonDict objectForKey:@"BookPortFTTHResult"];
    
    if(arr.count <= 0){
        handler(nil, @"", @"");
        return;
    }
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    
    if(![ErrorService isEqualToString:@"<null>"]){
        NSString *message = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"Error"]); ;
        handler(nil, ErrorService, message);
        return;
    }
 
    NSString *Result = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"Result"]);
    handler(nil, ErrorService, Result);
}

#pragma mark - Book Port ADSL
- (void)BookPortADSL:locationid BranchID:(NSString *)branchid UserName:(NSString *)username TDID:(NSString *)tdid TypeSevice:(NSString *)typesevice RegCode:(NSString *)regcode CardNumber:(NSString *)cardnumber  IDPortTD:(NSString *)idporttd IP:(NSString *)ip TDName:(NSString *)tdname  Latlng:(NSString *)latlng LatlngDevice:(NSString *)latlngDevice Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mBookPortADSL)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    if(latlng == nil){
        latlng = @"";
    }
    if (latlngDevice == nil) {
        latlngDevice = @"";
    }
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:regcode forKey:@"RegCode"];
    [dict setObject:cardnumber forKey:@"CardNumber"];
    [dict setObject:username forKey:@"UserName"];
    [dict setObject:tdid forKey:@"IDTD"];
    [dict setObject:locationid forKey:@"LocationID"];
    [dict setObject:branchid forKey:@"BranchID"];
    [dict setObject:idporttd?:@"0" forKey:@"IDPortTD"];
    [dict setObject:ip forKey:@"IP"];
    [dict setObject:tdname forKey:@"TDName"];
    [dict setObject:latlng forKey:@"Latlng"];
    [dict setObject:typesevice?:@"0" forKey:@"TypeSevice"];
    [dict setObject:latlngDevice forKey:@"LatlngDevice"];
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endBookPortADSL:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}
- (void)endBookPortADSL:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }

    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSArray *arr = [jsonDict objectForKey:@"BookPortADSLResult"];
    
    if(arr.count <= 0){
        handler(nil, @"", @"");
        return;
    }
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    
    if(![ErrorService isEqualToString:@"<null>"]){
        NSString *message = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"Error"]); ;
        handler(nil, ErrorService, message);
        return;
    }
  
    //    NSMutableArray *mArray = [NSMutableArray array];
    //    for(NSDictionary *d in arr){
    //        PortFreeRecord *p = [self ParseRecordPortFree:d];
    //        [mArray addObject:p];
    //    }
    //    NSString *ResultID = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ResultID"]);
    NSString *Result = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"Result"]);
    handler(nil, ErrorService, Result);
}

#pragma mark - GetSumPortTCUseADSL
- (void)GetSumPortTCUseADSL:(NSString*)typeservice CountPortTD:(NSString *)countporttd TDName:(NSString *)tdname Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetSumPortTCUseADSL)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:typeservice forKey:@"TypeService"];
    [dict setObject:countporttd forKey:@"CountPortTD"];
    [dict setObject:tdname forKey:@"TDName"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetSumPortTCUseADSL:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
    
}
- (void)endGetSumPortTCUseADSL:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }

    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSArray *arr = [jsonDict objectForKey:@"GetSumPortTCUseADSLMethodPostResult"]?:nil;
    
    if(arr.count <= 0){
        return;
    }
    
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    
    if(![ErrorService isEqualToString:@"<null>"]){
        //NSString *message = StringFormat(@"%@",[root objectForKey:@"Message"]); ;
        handler(nil, ErrorService, ErrorService);
        return;
    }
 
    handler([arr objectAtIndex:0], ErrorService, @"");
}

//UpdateSurvey(string RegCode, string OutDType, string OutDoor, string InDType, string InDoor, string Image, string MapCode, string Modem, string TDName, string UserName, string Latlng)

#pragma mark - UpdateSurvey
- (void)UpdateSurvey:(NSString*)regcode OutDType:(NSString *)outdtype OutDoor:(NSString *)outdoor InDType:(NSString *)indtype InDoor:(NSString *)indoor Image:(NSString *)image MapCode:(NSString *)mapcode Modem:(NSString *)modem TDName:(NSString *)tdname UserName:(NSString *)username Latlng:(NSString *)latlng Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mUpdateSurvey)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:regcode forKey:@"RegCode"];
    [dict setObject:outdtype forKey:@"OutDType"];
    [dict setObject:outdoor forKey:@"OutDoor"];
    [dict setObject:indtype forKey:@"InDType"];
    [dict setObject:indoor forKey:@"InDoor"];
    [dict setObject:image forKey:@"Image"];
    [dict setObject:mapcode forKey:@"MapCode"];
    [dict setObject:modem forKey:@"Modem"];
    [dict setObject:tdname forKey:@"TDName"];
    [dict setObject:username forKey:@"UserName"];
    [dict setObject:latlng forKey:@"Latlng"];
    
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];

    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endUpdateSurvey:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];

    
    
}
- (void)endUpdateSurvey:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }

    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSArray *arr = [jsonDict objectForKey:@"UpdateSurveyResult"]?:nil;
    
    if(arr.count <= 0){
        return;
    }
    
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    
    if(![ErrorService isEqualToString:@"<null>"]){
        handler(nil, ErrorService, ErrorService);
        return;
    }

    NSString *Result   = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"Result"]);
    NSString *ResultID = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ResultID"]);

    handler(nil, ResultID, Result);
}

#pragma mark - RecoverRegistration
- (void)RecoverRegistration:(NSString*)type RegCode:(NSString *)regcode CreateBy:(NSString *)createby IP:(NSString *)ip  Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mRecoverRegistration)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    ip = @"119.112.33.33";
    [dict setObject:type forKey:@"Type"];
    [dict setObject:regcode forKey:@"RegCode"];
    [dict setObject:createby forKey:@"CreateBy"];
    [dict setObject:ip forKey:@"IP"];
    
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endRecoverRegistration:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
    
    
}
- (void)endRecoverRegistration:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }

    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSArray *arr = [jsonDict objectForKey:@"RecoverRegistrationResult"]?:nil;
    
    if(arr.count <= 0){
        return;
    }
    
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    
    if(![ErrorService isEqualToString:@"<null>"]){
        //NSString *message = StringFormat(@"%@",[root objectForKey:@"Message"]); ;
        handler(nil, ErrorService, ErrorService);
        return;
    }

    NSString *Result = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"Result"]);
    
    handler(nil,Result , Result);
}

#pragma mark - RecoverRegistration
- (void)UploadImageInvest:(NSString*)image userName:(NSString *)username Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    //urlAPIIMAGE
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPIIMAGE, mUploadImageInvest)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:image forKey:@"image"];
    [dict setObject:username forKey:@"userName"];
    
    NSString *postString = [dict JSONRepresentation];
    
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endUploadImageInvest:result response:res completionHandler:handler errorHandler:errHandler];
        
    };
    
    callOp.errorHandler = ^(NSError *err) {
        
        
        
        errHandler(err);
    };
    
    [callOp start];
    
    
    
}
- (void)endUploadImageInvest:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
//    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
//    if (![headerErrorCode isEqualToString:@"0"]) {
//        handler (nil, headerErrorCode, @"het phien lam viec");
//        return;
//    }

    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *root = [jsonDict objectForKey:@"UploadImageInvestResult"]?:nil;
    
    if(root == nil){
        handler(nil,@"" , @"Upload ảnh thất bại");
        return;
    }
    
    NSString *ErrorService = StringFormat(@"%@",[root objectForKey:@"ErrorService"]);
    
    if(![ErrorService isEqualToString:@"<null>"]){
        handler(nil, ErrorService, @"Upload ảnh thất bại");
        return;
    }
    

    NSString *Result = StringFormat(@"%@",[root objectForKey:@"Result"]);
    
    handler(nil,Result , Result);
}


@end
