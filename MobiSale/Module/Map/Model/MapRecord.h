//
//  MapRecord.h
//  MobiSale
//
//  Created by HIEUPC on 2/5/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MapRecord : NSObject

@property (nonatomic,retain) NSString *Cabtype;
@property (nonatomic,retain) NSString *Latlng;
@property (nonatomic,retain) NSString *PortDie;
@property (nonatomic,retain) NSString *PortFree;
@property (nonatomic,retain) NSString *PortTotal;
@property (nonatomic,retain) NSString *PortUsed;
@property (nonatomic,retain) NSString *TDName;
@property (nonatomic,retain) NSString *TDType;
@property (nonatomic,retain) NSString *UserName;
@property (nonatomic,retain) NSString *Address;
@property (nonatomic,retain) NSString *Distance;
@property (nonatomic,retain) NSString *PortFreeRatio;
@end
