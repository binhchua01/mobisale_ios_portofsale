//
//  LocationBranchPopRecord.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 4/21/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocationBranchPopRecord : NSObject

@property (strong, nonatomic) NSString *BranchID;
@property (strong, nonatomic) NSString *LocationID;
@property (strong, nonatomic) NSString *LocationName;
@property (strong, nonatomic) NSString *NameBranch;
@property (strong, nonatomic) NSString *PopName;
@property (strong, nonatomic) NSString *TDID;

@end
