//
//  LocationBranchPopRecord.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 4/21/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "LocationBranchPopRecord.h"

@implementation LocationBranchPopRecord

@synthesize BranchID;
@synthesize LocationID;
@synthesize LocationName;
@synthesize NameBranch;
@synthesize PopName;
@synthesize TDID;

@end
