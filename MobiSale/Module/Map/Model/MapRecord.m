//
//  MapRecord.m
//  MobiSale
//
//  Created by HIEUPC on 2/5/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "MapRecord.h"

@implementation MapRecord

@synthesize Cabtype;
@synthesize Latlng;
@synthesize PortDie;
@synthesize PortFree;
@synthesize PortTotal;
@synthesize PortUsed;
@synthesize TDName;
@synthesize TDType;
@synthesize UserName;
@synthesize Address;
@synthesize Distance;
@synthesize PortFreeRatio;

@end
