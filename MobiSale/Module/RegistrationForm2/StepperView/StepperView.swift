//
//  StepperView.swift
//  DemoCreateRegisterForm
//
//  Created by Bored Ninjas on 11/10/16.
//  Copyright © 2016 com.StevenNguyen.DemoCreateRegisterForm. All rights reserved.
//

import UIKit
import Foundation
import CoreGraphics


//MARK: -DECLARE
@objc @IBDesignable class StepperView:UIView {
    
    //MARK: -Private properties
    var _needsRedraw = true {
        didSet {
            if self._needsRedraw {
                self.setNeedsDisplay()
            }
        }
    }
    
    ///The number of displayed points in the component
    @IBInspectable var numberOfPoints:Int = 3 {
        didSet{
            self.setNeedsRedraw()
        }
    }
    
}

//MARK: -OTHER FUNC
extension StepperView {
    
    /**
     Force the component to redraw
    **/
    func setNeedsRedraw() {
        self._needsRedraw = true
    }
    
}


