//
//  AniTextViewCell.h
//  demoRegister
//
//  Created by Bored Ninjas on 11/23/16.
//  Copyright © 2016 com.StevenNguyen.DemoCreateRegisterForm. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AniTextViewCellDelegate <NSObject>

-(void) valueTextfield: (NSString *)value kTag:(NSInteger)tag;

@end

@interface AniTextViewCell : UITableViewCell

@property (nonatomic, weak) id <AniTextViewCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UITextView *txtvTitle;

-(void) configCell:(NSString *)str kTag:(NSInteger)tag;

@end
