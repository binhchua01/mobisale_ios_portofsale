//
//  InternetCell.m
//  demoRegister
//
//  Created by Bored Ninjas on 11/23/16.
//  Copyright © 2016 com.StevenNguyen.DemoCreateRegisterForm. All rights reserved.
//

#import "InternetCell.h"

@interface InternetCell()<UITextViewDelegate> {
    
    __weak IBOutlet NSLayoutConstraint *constaintLabelService;
    
    __weak IBOutlet NSLayoutConstraint *constaintButtonService;
    
    __weak IBOutlet NSLayoutConstraint *constaintLabelPromotion;
    
    __weak IBOutlet NSLayoutConstraint *constaintButtonPromotion;
    
}

@end

@implementation InternetCell {
    
    CGFloat top;
    CGFloat bottom;
    NSTimeInterval duration;
    
    NSInteger kTag;
    
}

//MARK: -ACTION
- (IBAction)pressedService:(id)sender {
    
    self.btnService.selected = !self.btnService.selected;
    
    [self animationForService];
    
    [self.delegate dropDownButton:sender kTag:kTag tagButton:3];
    
}

- (IBAction)pressedPromotion:(id)sender {
    
    self.btnPromotion.selected = !self.btnPromotion.selected;
    
    [self animationForPromotion];
    
    [self.delegate dropDownButton:sender kTag:kTag tagButton:4];
    
}

- (IBAction)pressedSuggest:(id)sender {
    
    [self.delegate dropDownButton:sender kTag:kTag tagButton:5];
    
}

//MARK: -LIFE CYCLE
- (void)awakeFromNib {
    [super awakeFromNib];
    
    top = 22;
    bottom = 15;
    duration = 0.3;
    kTag = -1;
    
    //self.lblService.alpha = 0;
    self.lblPromotion.alpha = 0;
    
    [self configButton];
}

//MARK: -ORTHER FUNC
-(void)configCell:(NSString *)strService titlePromotion:(NSString *)strPromotion stringService:(NSString *)strTextView ktag:(NSInteger)tag {
    

    if (![strPromotion isEqual:@""]) {
        [self.btnPromotion setTitle:strPromotion forState:UIControlStateNormal];
        
        self.txtvPromotion.text = strTextView;
        
    } else {
        
        [self.btnPromotion setTitle:@"Chọn câu lệnh khuyến mãi" forState:UIControlStateNormal];
        
    }
    
    
    kTag = tag;
    
    //[self animationForService];
    
    [self animationForPromotion];
    
}

// animation for cell when textfield isEmpty
- (void)animationForService {
    
    if (self.btnService.selected == YES) {
        
        [UIView animateWithDuration:duration animations:^{
            
            constaintLabelService.constant = 8;
            constaintButtonService.constant = 8;
            self.lblService.alpha = 1;
            
            [self layoutIfNeeded];
            
        }];
        
    } else {
        
        [UIView animateWithDuration:duration animations:^{
            
            constaintLabelService.constant = top;
            constaintButtonService.constant = bottom;
            self.lblService.alpha = 0;
            
            [self layoutIfNeeded];
            
        }];
        
    }
    
}

- (void)animationForPromotion {
    
    if (self.btnPromotion.selected == YES) {
        
        [UIView animateWithDuration:duration animations:^{
            
            constaintLabelPromotion.constant = 8;
            constaintButtonPromotion.constant = 8;
            self.lblPromotion.alpha = 1;
            
            [self layoutIfNeeded];
            
        }];
        
    } else {
        
        [UIView animateWithDuration:duration animations:^{
            
            constaintLabelPromotion.constant = top;
            constaintButtonPromotion.constant = bottom;
            self.lblPromotion.alpha = 0;
            
            [self layoutIfNeeded];
            
        }];
        
    }
    
}

//MARK: -CONFIGURE BUTTON
- (void)configButton {
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    imgView.center = CGPointMake(self.btnService.frame.size.width - 25, self.btnService.frame.size.height/2);
    imgView.image = [UIImage imageNamed:@"ic_downarrow"];
    [self.btnService addSubview:imgView];
    
    UIImageView *imgView2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    
    imgView2.center = CGPointMake(self.btnPromotion.frame.size.width - 25, self.btnPromotion.frame.size.height/2);
    
    imgView2.image = [UIImage imageNamed:@"ic_downarrow"];
    
    [self.btnPromotion addSubview:imgView2];
    
    self.btnPromotion.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 25 );
    
}



@end
