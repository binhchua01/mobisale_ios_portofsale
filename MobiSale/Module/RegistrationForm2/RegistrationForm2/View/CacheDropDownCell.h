//
//  CacheDropDownCell.h
//  MobiSale
//
//  Created by Nguyen Sang on 10/6/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AniDropDownCellTableViewCell.h"

//@protocol AniDropDownCellTableViewCellDelegate <NSObject>
//
//-(void) dropDownButton: (id)sender kTag:(NSInteger)tag;
//
//@end

@interface CacheDropDownCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnChoose;

@property (weak, nonatomic) IBOutlet UIButton *btnRefresh;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBottom;

@property (nonatomic, weak) id <AniDropDownCellTableViewCellDelegate> delegate;

-(void) configDropDownCell:(NSString*)strTitle titleButton:(NSString*)strTitleButton kTag:(NSInteger)tag;

@end
