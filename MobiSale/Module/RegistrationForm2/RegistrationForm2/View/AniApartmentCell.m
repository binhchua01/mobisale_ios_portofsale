//
//  AniApartmentCell.m
//  MobiSale
//
//  Created by Bored Ninjas on 11/30/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "AniApartmentCell.h"

@interface AniApartmentCell()<UITextFieldDelegate> {
    
    __weak IBOutlet NSLayoutConstraint *constraintLot;
    
    __weak IBOutlet NSLayoutConstraint *constraintLabelLot;
    
    __weak IBOutlet NSLayoutConstraint *constraintFloor;
    
    __weak IBOutlet NSLayoutConstraint *constraintLabelFloor;
    
    __weak IBOutlet NSLayoutConstraint *constraintRoom;
    
    __weak IBOutlet NSLayoutConstraint *constraintLabelRoom;
    
    
    
}

@end

@implementation AniApartmentCell {
    
    CGFloat top;
    CGFloat bottom;
    NSTimeInterval duration;
    
    NSInteger kTag;
    
}

@synthesize delegate;

//MARK: -ACTION
- (IBAction)changedLot:(UITextField *)sender {
    
    [self animationForLot];
    
}

//MARK: -ACTION
- (IBAction)changedFloor:(UITextField *)sender {
    
    [self animationForFloor];
    
}

//MARK: -ACTION
- (IBAction)changedRoom:(UITextField *)sender {
    
    [self animationForRoom];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //static value
    top = 20;
    bottom = 15;
    duration = 0.3;
    
    kTag = -1;
    
    self.lblLot.alpha = 0;
    self.lblFloor.alpha = 0;
    self.lblRoom.alpha = 0;
    
    self.txtfLot.delegate = self;
    self.txtfFloor.delegate = self;
    self.txtfRoom.delegate = self;
    
    self.txtfLot.tag = 1;
    self.txtfFloor.tag = 2;
    self.txtfRoom.tag = 3;
    
}

- (void) configCell:(NSString *)strLot placeHolderLot:(NSString *)strPlaceLot titleFloor:(NSString *)strFloor placeHolderFloor:(NSString *)strPlaceFloor titleRoom:(NSString *)strRoom placeHolderRoom:(NSString *)strPlaceRoom {
    
    self.lblLot.text = strLot;
    self.lblFloor.text = strFloor;
    self.lblRoom.text = strRoom;
    
    self.txtfLot.placeholder = strPlaceLot;
    self.txtfFloor.placeholder = strPlaceFloor;
    self.txtfRoom.placeholder = strPlaceRoom;
    
}

- (void) configCellWhenSetText:(NSString *)strLot Floor:(NSString *)strFloor Room:(NSString *)strRoom {
    
    if (![strLot  isEqual: @""]) {
        
        self.txtfLot.text = strLot;
        [self animationForLot];
        
    }
    
    if (![strFloor isEqual:@""]) {
        
        self.txtfFloor.text = strFloor;
        [self animationForFloor];
        
    }
    
    if (![strRoom isEqual:@""]) {
        
        self.txtfRoom.text = strRoom;
        [self animationForRoom];
        
    }
}

- (void) configCellWithTag:(NSInteger)tag {
    
    kTag = tag;
    
}

//MARK: -ANIMATION
-(void)animationForLot {
    
    if (self.txtfLot.text && self.txtfLot.text.length > 0) {
        
        [UIView animateWithDuration:duration animations:^{
            
            constraintLabelLot.constant = 8;
            constraintLot.constant = 8;
            self.lblLot.alpha = 1;
            
            [self layoutIfNeeded];
            
        }];
        
        
    } else {
        
        [UIView animateWithDuration:duration animations:^{
            
            constraintLabelLot.constant = top;
            constraintLot.constant = bottom;
            self.lblLot.alpha = 0;
            
            [self layoutIfNeeded];
            
        }];
        
    }
    
}

-(void)animationForFloor {
    
    if (self.txtfFloor.text && self.txtfFloor.text.length > 0) {
        
        [UIView animateWithDuration:duration animations:^{
            
            constraintLabelFloor.constant = 8;
            constraintFloor.constant = 8;
            self.lblFloor.alpha = 1;
            
            [self layoutIfNeeded];
            
        }];
        
        
    } else {
        
        [UIView animateWithDuration:duration animations:^{
            
            constraintLabelFloor.constant = top;
            constraintFloor.constant = bottom;
            self.lblFloor.alpha = 0;
            
            [self layoutIfNeeded];
            
        }];
        
    }
    
}

-(void)animationForRoom {
    
    if (self.txtfRoom.text && self.txtfRoom.text.length > 0) {
        
        [UIView animateWithDuration:duration animations:^{
            
            constraintLabelRoom.constant = 8;
            constraintRoom.constant = 8;
            self.lblRoom.alpha = 1;
            
            [self layoutIfNeeded];
            
        }];
        
        
    } else {
        
        [UIView animateWithDuration:duration animations:^{
            
            constraintLabelRoom.constant = top;
            constraintRoom.constant = bottom;
            self.lblRoom.alpha = 0;
            
            [self layoutIfNeeded];
            
        }];
        
    }
    
}

//MARK: -UITEXTFIELDDELEGATE
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
    
}

-(void)textFieldDidEndEditing:(UITextField*)textField {
    
    NSString *str = textField.text;
    NSCharacterSet *charset = [NSCharacterSet whitespaceCharacterSet];
    str = [str stringByTrimmingCharactersInSet:charset];
   // str = [str stringByReplacingOccurrencesOfString:@"\'" withString:@""];
    
    if (textField.tag == 1) {
        //LOT
        [self.delegate valueTextfield:str kTag:kTag tagTextField:1];
        
    } else if (textField.tag == 2) {
        //Floor
        [self.delegate valueTextfield:str kTag:kTag tagTextField:2];
        
    } else if (textField.tag == 3) {
        //room
        [self.delegate valueTextfield:str kTag:kTag tagTextField:3];
    }
    
}


@end
