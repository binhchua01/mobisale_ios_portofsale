//
//  AniDropDownCellTableViewCell.m
//  MobiSale
//
//  Created by Bored Ninjas on 11/25/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "AniDropDownCellTableViewCell.h"

@implementation AniDropDownCellTableViewCell {
    
    CGFloat top;
    CGFloat bottom;
    NSTimeInterval duration;
    
    NSInteger kTag;
    
}

//MARK: -ACTION
- (IBAction)pressedChoose:(id)sender {
    
//    self.btnChoose.selected = !self.btnChoose.selected;
    
    [self animationForCell];
    
    [self.delegate dropDownButton:sender kTag:kTag];
    
}

//MARK: -CYCLE LIFE
- (void)awakeFromNib {
    [super awakeFromNib];
    
    top = 22;
    bottom = 15.5;
    duration = 0.3;
    kTag = -1;
    
    self.lblTitle.alpha = 0;
    
    [self configButton];
    
}

- (void)configButton {
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    imgView.center = CGPointMake(self.btnChoose.frame.size.width - 25, self.btnChoose.frame.size.height/2);
    imgView.image = [UIImage imageNamed:@"ic_downarrow"];
    [self.btnChoose addSubview:imgView];
    
}

-(void) configDropDownCell:(NSString*)strTitle titleButton:(NSString*)strTitleButton kTag:(NSInteger)tag {
    
    self.lblTitle.text = strTitle;
    [self.btnChoose setTitle:strTitleButton forState:UIControlStateNormal];
    kTag = tag;
    [self animationForCell];
    
}

// animation for cell when textfield isEmpty
- (void)animationForCell {
    
    if (self.btnChoose.selected == YES) {
        
        [UIView animateWithDuration:duration animations:^{
            
            self.constraintTopLabel.constant = 8;
            self.constraintBottom.constant = 8;
            self.lblTitle.alpha = 1;
            
            [self layoutIfNeeded];
            
        }];
        
    } else {
        
        [UIView animateWithDuration:duration animations:^{
            
            self.constraintTopLabel.constant = top;
            self.constraintBottom.constant = bottom;
            self.lblTitle.alpha = 0;
            
            [self layoutIfNeeded];
            
        }];
        
    }
    
}

@end
