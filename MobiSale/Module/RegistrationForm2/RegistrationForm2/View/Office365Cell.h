//
//  Office365Cell.h
//  demoRegister
//
//  Created by Bored Ninjas on 11/23/16.
//  Copyright © 2016 com.StevenNguyen.DemoCreateRegisterForm. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol Office365CellDelegate <NSObject>

-(void) dropDownButton: (id)sender kTag:(NSInteger)tag;

@end

@interface Office365Cell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnOffice;

@property (weak, nonatomic) IBOutlet UILabel *lblOffice;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBottom;

@property (nonatomic, weak) id <Office365CellDelegate> delegate;

-(void) configDropDownCell:(NSString*)strTitle titleButton:(NSString*)strTitleButton kTag:(NSInteger)tag;

@end
