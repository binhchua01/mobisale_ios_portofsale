//
//  IPTVCell.h
//  demoRegister
//
//  Created by Bored Ninjas on 11/23/16.
//  Copyright © 2016 com.StevenNguyen.DemoCreateRegisterForm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SAStepperControl.h"
#import "RadioButton.h"

@protocol IPTVCellDelegate <NSObject>

-(void) dropDownIPTVButton: (id)sender kTag:(NSInteger)tag tagButton:(NSInteger)tagButton;

-(void) PressedBoxCount:(NSInteger)Count;

@end

@interface IPTVCell : UITableViewCell

- (void)configPriceCell:(NSString *)strPricePlus FilmHot:(NSString *)strPriceHot FilmKPlus:(NSString *)strPriceKPlus FilmHD:(NSString *)strPriceHD FilmVTV:(NSString *)strPriceVTV;

- (void)configCell:(NSString *)serviceBase cbRequest:(NSString *)cbRequest cbDrillWall:(NSString *)cbDrillWall combo:(NSString *)strCombo delopment:(NSString *)strDelop IPTVPackage:(NSString *)iptvPackage promotion1:(NSString *)promotion1 promotion2:(NSString *)promotion2;

-(void) configSteppersForCell:(NSString *)strBoxCount PLCCount:(NSString *)strPLCCount STBCount:(NSString *)strSTBCount PostageCount:(NSString *)strPortageCount;

-(void) configFilmForCell:(NSString *)strFilmPlusChargeTime FilmPlusPrepaidMonth:(NSString *)strFilmPlusPrepaidMonth FilmHotChargeTime:(NSString *)strHotChargeTime FilmHotPrepaidMonth:(NSString *)strHotPrepaidMonth FilmKPlusChargeTime:(NSString *)strKPlusChargeTime FilmKPlusPrepaidMonth:(NSString *)strKPlusPrepaidMonth FilmVTCChargeTime:(NSString *)strVTCChargeTime FilmVTCPrepaidMonth:(NSString *)strVTCPrepaidMonth FilmVTVChargeTime:(NSString *)strVTVChargeTime FilmVTVPrepaidMonth:(NSString *)strVTVPrepaidMonth;

@property (nonatomic, weak) id <IPTVCellDelegate> delegate;

/****************************** Label ****************************************/
@property (weak, nonatomic) IBOutlet UILabel *lblServiceBase;

@property (weak, nonatomic) IBOutlet UILabel *lblDeploy;

@property (weak, nonatomic) IBOutlet UILabel *lblCombo;

@property (weak, nonatomic) IBOutlet UILabel *lblService;

@property (weak, nonatomic) IBOutlet UILabel *lblPromotion1;

@property (weak, nonatomic) IBOutlet UILabel *lblPromotion2;

@property (weak, nonatomic) IBOutlet UILabel *lblPriceFilmPlus;

@property (weak, nonatomic) IBOutlet UILabel *lblPriceHot;

@property (weak, nonatomic) IBOutlet UILabel *lblPriceKPlus;

@property (weak, nonatomic) IBOutlet UILabel *lblPriceHD;

@property (weak, nonatomic) IBOutlet UILabel *lblPriceVTV;

/****************************** Button ****************************************/
@property (weak, nonatomic) IBOutlet UIButton *btnServiceBase;

@property (weak, nonatomic) IBOutlet UIButton *btnDeploy;

@property (weak, nonatomic) IBOutlet UIButton *btnCombo;

@property (weak, nonatomic) IBOutlet UIButton *btnService;

@property (weak, nonatomic) IBOutlet UIButton *btnPromotion1;

@property (weak, nonatomic) IBOutlet UIButton *btnPromotion2;

@property (weak, nonatomic) IBOutlet UIButton *btnFilmPlus;

@property (weak, nonatomic) IBOutlet UIButton *btnFilmHot;

@property (weak, nonatomic) IBOutlet UIButton *btnFilmKPlus;

@property (weak, nonatomic) IBOutlet UIButton *btnFilmHD;

@property (weak, nonatomic) IBOutlet UIButton *btnFilmVTVCab;

/****************************** TextView ****************************************/
@property (weak, nonatomic) IBOutlet UITextView *txtvPromotion1;

@property (weak, nonatomic) IBOutlet UITextView *txtvPromotion2;


/****************************** Check Box ****************************************/
@property (strong, nonatomic) IBOutlet RadioButton *rdYesSetup;
@property (strong, nonatomic) IBOutlet RadioButton *rdNoSetup;

//checkbox khoan tường
@property (strong, nonatomic) IBOutlet RadioButton *rdYesDrillWall;
@property (strong, nonatomic) IBOutlet RadioButton *rdNoDrillWall;

/************************************** Stepper ***********************************/
@property (weak, nonatomic) IBOutlet SAStepperControl *stepperBox;//số lượng box

//stepper số lượng PLC
@property (weak, nonatomic) IBOutlet SAStepperControl *stepperPLC;

//stepper số STB thu hồi
@property (weak, nonatomic) IBOutlet SAStepperControl *stepperSTB;

//stepper số lần tính cước
@property (weak, nonatomic) IBOutlet SAStepperControl *stepperPostage;

//stepper Film+ số lần tính cước
@property (weak, nonatomic) IBOutlet SAStepperControl *stepperFilmPlusPostage;
@property (weak, nonatomic) IBOutlet SAStepperControl *stepperFilmPlusPrepay;

//stepper Film Hot số lần tính cước
@property (weak, nonatomic) IBOutlet SAStepperControl *stepperFilmHotPostage;
@property (weak, nonatomic) IBOutlet SAStepperControl *stepperFilmHotPrepay;

//stepper K+ số lần tính cước
@property (weak, nonatomic) IBOutlet SAStepperControl *stepperKPlusPostage;
@property (weak, nonatomic) IBOutlet SAStepperControl *stepperKPlusPrepay;

//stepper Đặc sắc HD số lần tính cước
@property (weak, nonatomic) IBOutlet SAStepperControl *stepperHDPostage;
@property (weak, nonatomic) IBOutlet SAStepperControl *stepperHDPrepay;

//stepper VTV cab số lần tính cước
@property (weak, nonatomic) IBOutlet SAStepperControl *stepperVTVCabPostage;
@property (weak, nonatomic) IBOutlet SAStepperControl *stepperVTVCabPrepay;

@end
