//
//  InternetCell.h
//  demoRegister
//
//  Created by Bored Ninjas on 11/23/16.
//  Copyright © 2016 com.StevenNguyen.DemoCreateRegisterForm. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol InternetCellDelegate <NSObject>

-(void) dropDownButton: (id)sender kTag:(NSInteger)tag tagButton:(NSInteger)tagButton;

@end

@interface InternetCell : UITableViewCell

@property (nonatomic, weak) id <InternetCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *btnService;

@property (weak, nonatomic) IBOutlet UIButton *btnPromotion;

@property (weak, nonatomic) IBOutlet UILabel *lblService;

@property (weak, nonatomic) IBOutlet UILabel *lblPromotion;

@property (weak, nonatomic) IBOutlet UITextView *txtvPromotion;

-(void)configCell:(NSString *)strService titlePromotion:(NSString *)strPromotion stringService:(NSString *)strTextView ktag:(NSInteger)tag;

@end
