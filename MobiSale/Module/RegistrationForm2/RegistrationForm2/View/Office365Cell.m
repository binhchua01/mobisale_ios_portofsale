//
//  Office365Cell.m
//  demoRegister
//
//  Created by Bored Ninjas on 11/23/16.
//  Copyright © 2016 com.StevenNguyen.DemoCreateRegisterForm. All rights reserved.
//

#import "Office365Cell.h"

@implementation Office365Cell {
    
    CGFloat top;
    CGFloat bottom;
    NSTimeInterval duration;
    
    NSInteger kTag;
    
}

//MARK: -ACTION
- (IBAction)pressedChoose:(id)sender {
    
//    self.btnOffice.selected = true;
//
    [self animationForCell];
    
    [self.delegate dropDownButton:sender kTag:kTag];
    
}

//MARK: -CYCLE LIFE
- (void)awakeFromNib {
    [super awakeFromNib];
    
    top = 21;
    bottom = 15;
    duration = 0.3;
    kTag = -1;
    
    self.lblOffice.alpha = 0;
    
    self.btnOffice.selected = true;
    
    [self configButton];
    
}

- (void)configButton {
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    imgView.center = CGPointMake(self.btnOffice.frame.size.width - 25, self.btnOffice.frame.size.height/2);
    imgView.image = [UIImage imageNamed:@"ic_downarrow"];
    [self.btnOffice addSubview:imgView];
    
}

-(void) configDropDownCell:(NSString*)strTitle titleButton:(NSString*)strTitleButton kTag:(NSInteger)tag {
    
    self.lblOffice.text = strTitle;
    [self.btnOffice setTitle:strTitleButton forState:UIControlStateNormal];
    kTag = tag;
    
    [self animationForCell];
    
}

// animation for cell when textfield isEmpty
- (void)animationForCell {
    
//    if (self.btnOffice.selected == YES) {
        
        [UIView animateWithDuration:duration animations:^{
            
            self.constraintTopLabel.constant = 8;
            self.constraintBottom.constant = 8;
            self.lblOffice.alpha = 1;
            
            [self layoutIfNeeded];
            
        }];
        

    
}

@end
