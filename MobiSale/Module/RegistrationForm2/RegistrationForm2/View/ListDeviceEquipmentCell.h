//
//  ListDeviceEquipmentCell.h
//  MobiSale
//
//  Created by Tran Vu on 6/8/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIPopoverListView2.h"
#import "UIPopoverListView.h"

@protocol ListDeviceEquipmentCellDelegate <NSObject>
@optional
- (void)getArrayPromotionListDevice1: (NSMutableArray *)arrayPromotionListDevice1 ArrayDevice:(NSMutableArray *)arrayDevice1;
- (void)getNumberCount:(NSString *)stringCount;



@end

@interface ListDeviceEquipmentCell : UITableViewCell<UIPopoverListView2Delegate,UIPopoverListView2DataSource,UIPopoverListViewDelegate,UIPopoverListViewDataSource>{
    
}

@property (weak, nonatomic) IBOutlet UILabel *lblNameDevices;

@property (weak, nonatomic) IBOutlet UIView *viewContent;

@property (strong, nonatomic) NSMutableArray *array;
@property (strong, nonatomic) NSMutableArray *arrayDevice;
@property (strong, nonatomic) NSMutableArray *listDevicePacketArray;
- (void)setDataForCell:(NSMutableArray *)arrayListDevice andNumberRow:(int)numberRow;

- (IBAction)btnPromotionListDevice_clicked:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnPromotionListDevice;
@property (weak, nonatomic) IBOutlet UIButton *minusButton;
@property (weak, nonatomic) IBOutlet UIButton *plusButton;
@property (weak, nonatomic) IBOutlet UITextField *countTextField;
@property ( assign ,nonatomic) NSInteger count;
@property (assign , nonatomic) NSInteger minimum;
@property (assign, nonatomic) NSInteger maximum;

@property(weak, nonatomic) id<ListDeviceEquipmentCellDelegate>delegate;

@property (nonatomic,strong) NSMutableArray *arrayListPromotionDevice;
@property (nonatomic,strong) NSString *deviceID;
@property (nonatomic,strong) NSIndexPath *indexPath;
@property (nonatomic,strong) NSMutableArray *arrayAddListDevice;
@property (nonatomic,strong) NSMutableDictionary *dictListDevice;
@property (nonatomic,strong) NSString *Id;

//Array DatalistPromotionDevice into sigalstance
@property (nonatomic,strong) NSMutableArray *arrayDataDevice;

@property (nonatomic,strong) NSMutableDictionary *dictPromotionListDevice;

- (void)showHUDWithMessage:(NSString *)message;
- (void)hideHUD;
@end
