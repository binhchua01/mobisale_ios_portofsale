//
//  TypeOfServiceCell.m
//  MobiSale
//
//  Created by Bored Ninjas on 12/7/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "TypeOfServiceCell.h"
#import "KeyValueModel.h"
#import "EGOCache.h"

@interface TypeOfServiceCell()<UITextViewDelegate> {
    
    __weak IBOutlet NSLayoutConstraint *constaintLabelService;
    
    __weak IBOutlet NSLayoutConstraint *constaintButtonService;
    
    __weak IBOutlet NSLayoutConstraint *constaintLabelPackage;
    
    __weak IBOutlet NSLayoutConstraint *constaintButtonPackage;
    
}

@end

@implementation TypeOfServiceCell {
    
    CGFloat top;
    CGFloat bottom;
    NSTimeInterval duration;
    
    NSInteger kTag;
    
}

//MARK: -ACTION
- (IBAction)pressedTypeOfService:(id)sender {
    
    self.btnTypeOfService.selected = !self.btnTypeOfService.selected;

    [self animationForService];

    [self.delegate dropDownButton:sender kTag:kTag tagButton:1];
    
}

- (IBAction)pressedPackageService:(id)sender {
    
    self.btnPackageService.selected = !self.btnPackageService.selected;

    [self animationForPromotion];

    [self.delegate dropDownButton:sender kTag:kTag tagButton:2];
    
}

- (IBAction)pressedFreshCache:(id)sender {
    
    [[EGOCache globalCache] clearCache];
    
    [self.delegate freshCompleted:kTag];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    top = 22;
    bottom = 15;
    duration = 0.3;
    kTag = -1;
    
    self.lblTypeOfService.alpha = 0;
    self.lblPackageService.alpha = 0;
    
    [self.btnRefresh setImage:[UIImage imageNamed:@"refresh"] forState:UIControlStateNormal];
    
    [self configButton];
    
}

-(void)configCellTitleButton:(NSMutableArray *)arrLocalType titlePackage:(NSString *)titlePackage {
    
    NSString *strPackage = titlePackage;
    
    NSString *strLocalType = @"";
    
    for ( KeyValueModel *keyVa in arrLocalType ) {
        
        if ([strLocalType  isEqual: @""]) {
            
            strLocalType = [NSString stringWithFormat:@"%@",keyVa.Values];
            
        } else {
            
            strLocalType = [NSString stringWithFormat:@"%@, %@",strLocalType,keyVa.Values];
            
        }
        
    }
    
    if ([strLocalType  isEqual: @""]) {
        strLocalType = @"Loại dịch vụ";
    }
    
    if ([titlePackage isEqual:@""]) {
        strPackage = @"Gói dịch vụ";
    }
    
    [self.btnTypeOfService setTitle:strLocalType forState:UIControlStateNormal];
    
    [self.btnPackageService setTitle:strPackage forState:UIControlStateNormal];
    
}

-(void)configCell:(NSString *)strService titlePromotion:(NSString *)strPackage ktag:(NSInteger)tag {
    
    [self.btnTypeOfService setTitle:strService forState:UIControlStateNormal];
    [self.btnTypeOfService setTitle:strPackage forState:UIControlStateNormal];
    
    kTag = tag;
    
}

// animation for cell when textfield isEmpty
- (void)animationForService {
    
    if (self.btnTypeOfService.selected == YES) {
        
        [UIView animateWithDuration:duration animations:^{
            
            constaintLabelService.constant = 8;
            constaintButtonService.constant = 8;
            self.lblTypeOfService.alpha = 1;
            
            [self layoutIfNeeded];
            
        }];
        
    } else {
        
        [UIView animateWithDuration:duration animations:^{
            
            constaintLabelService.constant = top;
            constaintButtonService.constant = bottom;
            self.lblTypeOfService.alpha = 0;
            
            [self layoutIfNeeded];
            
        }];
        
    }
    
}

- (void)animationForPromotion {
    
    if (self.btnPackageService.selected == YES) {
        
        [UIView animateWithDuration:duration animations:^{
            
            constaintLabelPackage.constant = 8;
            constaintButtonPackage.constant = 8;
            self.lblPackageService.alpha = 1;
            
            [self layoutIfNeeded];
            
        }];
        
    } else {
        
        [UIView animateWithDuration:duration animations:^{
            
            constaintLabelPackage.constant = top;
            constaintButtonPackage.constant = bottom;
            self.lblPackageService.alpha = 0;
            
            [self layoutIfNeeded];
            
        }];
        
    }
    
}

//MARK: -CONFIGURE BUTTON
- (void)configButton {
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    imgView.center = CGPointMake(self.btnTypeOfService.frame.size.width - 25, self.btnTypeOfService.frame.size.height/2);
    imgView.image = [UIImage imageNamed:@"ic_downarrow"];
    [self.btnTypeOfService addSubview:imgView];
    
    UIImageView *imgView2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    imgView2.center = CGPointMake(self.btnPackageService.frame.size.width - 25, self.btnPackageService.frame.size.height/2);
    imgView2.image = [UIImage imageNamed:@"ic_downarrow"];
    [self.btnPackageService addSubview:imgView2];
    
}

@end
