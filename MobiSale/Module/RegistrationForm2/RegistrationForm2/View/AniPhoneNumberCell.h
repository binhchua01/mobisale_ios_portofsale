//
//  AniPhoneNumberCell.h
//  demoRegister
//
//  Created by Bored Ninjas on 11/23/16.
//  Copyright © 2016 com.StevenNguyen.DemoCreateRegisterForm. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AniPhoneNumberCellDelegate <NSObject>

-(void) valueTextfield: (NSString *)value kTag:(NSInteger)tag tagTextField:(NSInteger)tagTextField;

@end

@interface AniPhoneNumberCell : UITableViewCell

//delegate
@property (nonatomic, weak) id <AniPhoneNumberCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *lblPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;

@property (weak, nonatomic) IBOutlet UITextField *txtfPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtfNameUser;

- (void) configCell:(NSString *)strPhoneNumber placeHolderPhoneNumber:(NSString *)strPlacePhoneNumber titleUserName:(NSString *)strUserName placeHolderUserName:(NSString *)strPlaceUserName;

-(void) configCellWhenSetText:(NSString *)strPhoneNumber username:(NSString *)strUserName;

- (void) configCellWithTag:(NSInteger)tag;

@end
