//
//  ButtonNextCell.h
//  demoRegister
//
//  Created by Bored Ninjas on 11/23/16.
//  Copyright © 2016 com.StevenNguyen.DemoCreateRegisterForm. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ButtonNextCellDelegate <NSObject>

-(void) pressedButton: (id)sender kTag:(NSInteger)tag ktagButton:(NSInteger)tagButton;

@end

@interface ButtonNextCell : UITableViewCell

@property (nonatomic, weak) id <ButtonNextCellDelegate> delegate;

-(void)hiddenButtonBack;

-(void) changeTitleButtonNext:(NSString *)strTitle;

@end
