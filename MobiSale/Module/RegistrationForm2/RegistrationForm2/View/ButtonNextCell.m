//
//  ButtonNextCell.m
//  demoRegister
//
//  Created by Bored Ninjas on 11/23/16.
//  Copyright © 2016 com.StevenNguyen.DemoCreateRegisterForm. All rights reserved.
//

#import "ButtonNextCell.h"

@implementation ButtonNextCell {
    
    __weak IBOutlet UIButton *btnNext;
    __weak IBOutlet UIButton *btnBack;
}

@synthesize delegate;

//MARK: -ACTION
-(IBAction)pressedSave:(id)sender {
    
    [delegate pressedButton:sender kTag:1 ktagButton:1];
    
}

-(IBAction)pressedBack:(id)sender {
    
    [delegate pressedButton:sender kTag:2 ktagButton:2];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self configCellCornerRadius];
    
}

-(void) changeTitleButtonNext:(NSString *)strTitle {
    [btnNext setTitle:strTitle forState:UIControlStateNormal];
}

-(void) configCell {
    
    
    
}

-(void) configCellCornerRadius {
    
    btnBack.layer.cornerRadius = 3;
    btnBack.clipsToBounds = YES;
    
    btnNext.layer.cornerRadius = 3;
    btnNext.clipsToBounds = YES;
    
}

//MARK: PUBLIC
- (void)hiddenButtonBack {
    
    btnBack.hidden = YES;
    
}

@end
