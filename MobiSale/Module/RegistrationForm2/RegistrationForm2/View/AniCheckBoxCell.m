//
//  AniCheckBoxCell.m
//  demoRegister
//
//  Created by Bored Ninjas on 11/23/16.
//  Copyright © 2016 com.StevenNguyen.DemoCreateRegisterForm. All rights reserved.
//

#import "AniCheckBoxCell.h"

@implementation AniCheckBoxCell {
    
    NSInteger kTag;
    
    NSString *selectedYes;
    
    NSString *selectedNo;
    
}

@synthesize delegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    
    kTag = -1;
    
}

-(void) configCell:(NSString *)strTitle kTag:(NSInteger)tag {
    
    self.lblTitle.text = strTitle;
    kTag = tag;
    
}

-(void) configCellWithYesNo:(NSString *)strSelectedYes SelectedNo:(NSString *)strNo SelectedYes:(NSString *)strYes {
    
    selectedYes = strYes;
    selectedNo = strNo;
    
    if ([strSelectedYes isEqualToString:strYes]) {
        self.rdYes.selected = YES;
        self.rdNo.selected = NO;
        [self.delegate valueTextfield:selectedYes kTag:kTag];
    } else {
        self.rdYes.selected = NO;
        self.rdNo.selected = YES;
        [self.delegate valueTextfield:selectedNo kTag:kTag];
    }
    
}

-(IBAction)RadioButtonCableStatus:(RadioButton *)sender {
    if(sender == self.rdYes){
        self.rdYes.selected = YES;
        self.rdNo.selected = NO;
//        CableStatus = @"1";
        [self.delegate valueTextfield:selectedYes kTag:kTag];
    }else {
        self.rdYes.selected = NO;
        self.rdNo.selected = YES;
//        CableStatus = @"2";
        [self.delegate valueTextfield:selectedNo kTag:kTag];
    }
}

@end
