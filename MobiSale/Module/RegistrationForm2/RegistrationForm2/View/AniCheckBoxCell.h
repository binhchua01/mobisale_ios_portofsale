//
//  AniCheckBoxCell.h
//  demoRegister
//
//  Created by Bored Ninjas on 11/23/16.
//  Copyright © 2016 com.StevenNguyen.DemoCreateRegisterForm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RadioButton.h"

@protocol AniCheckBoxCellDelegate <NSObject>

-(void) valueTextfield: (NSString *)value kTag:(NSInteger)tag;

@end

@interface AniCheckBoxCell : UITableViewCell

@property (nonatomic, weak) id <AniCheckBoxCellDelegate> delegate;

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@property (strong, nonatomic) IBOutlet RadioButton *rdYes;

@property (strong, nonatomic) IBOutlet RadioButton *rdNo;

-(void) configCell:(NSString *)strTitle kTag:(NSInteger)tag;

-(void) configCellWithYesNo:(NSString *)strSelectedYes SelectedNo:(NSString *)strNo SelectedYes:(NSString *)strYes;

@end
