//
//  FPTPlayCell.h
//  MobiSale
//
//  Created by StevenNguyen on 12/28/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SAStepperControl.h"

@interface FPTPlayCell : UITableViewCell

@property (weak, nonatomic) IBOutlet SAStepperControl *stepperBox;//số lượng box

- (void)configFPTPlayBox:(NSString*) stringfptPlayBoxCount;

@end
