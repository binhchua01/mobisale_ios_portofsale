//
//  AniTextFieldCell.h
//  demoRegister
//
//  Created by Bored Ninjas on 11/23/16.
//  Copyright © 2016 com.StevenNguyen.DemoCreateRegisterForm. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AniTextFieldCellDelegate <NSObject>

-(void) valueTextfield: (NSString *)value kTag:(NSInteger)tag;

@end

@interface AniTextFieldCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *txtFValue;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (nonatomic, weak) id <AniTextFieldCellDelegate> delegate;

- (void) configCell:(NSString *)strTitle placeHolderTextField:(NSString *)strPlaceholder;

-(void) configCellWithTag:(NSInteger)tag;

@end
