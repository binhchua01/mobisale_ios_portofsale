//
//  AniTextViewCell.m
//  demoRegister
//
//  Created by Bored Ninjas on 11/23/16.
//  Copyright © 2016 com.StevenNguyen.DemoCreateRegisterForm. All rights reserved.
//

#import "AniTextViewCell.h"

@interface AniTextViewCell()<UITextViewDelegate> {
    
//    __weak IBOutlet NSLayoutConstraint *constraintPhoneNumber;
//    
//    __weak IBOutlet NSLayoutConstraint *constraintLabelPhoneNumber;
//    
//    __weak IBOutlet NSLayoutConstraint *constraintNameUser;
//    
//    __weak IBOutlet NSLayoutConstraint *constraintLabelNameUser;
    
    
    
}

@end

@implementation AniTextViewCell {
    
    NSInteger kTag;
    
}

@synthesize delegate;

- (void)awakeFromNib {
    [super awakeFromNib];
    
    kTag = -1;
    
    [self setType];
    self.txtvTitle.delegate = self;
    
}

-(void) configCell:(NSString *)str kTag:(NSInteger)tag {
    
    self.txtvTitle.text = str;
    kTag = tag;
    
}

-(void)textViewDidEndEditing:(UITextView *)textView {
    
    [self.delegate valueTextfield:textView.text kTag:kTag];
    
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
    }
    
    return YES;
}


-(void) setType {
    
    self.txtvTitle.layer.borderColor= [UIColor colorWithRed:0 green:0.173 blue:0.294 alpha:1].CGColor;
    self.txtvTitle.layer.borderWidth = 1.f;
    self.txtvTitle.layer.cornerRadius = 6.0f;
}


@end
