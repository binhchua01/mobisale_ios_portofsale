//
//  TableListDeviceEquipmentCell.m
//  MobiSale
//
//  Created by Tran Vu on 6/8/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import "TableListDeviceEquipmentCell.h"


@implementation TableListDeviceEquipmentCell 


#define LISTDEVICES @"ListDeviceEquipmentCell"

- (void)awakeFromNib {
    [super awakeFromNib];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}


@end
