
//
//  ListDeviceTableCell.m
//  MobiSale
//
//  Created by Tran Vu on 6/20/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import "ListDeviceTableCell.h"
#import "RegistrationFormModel.h"

#define LISTDEVICE_CELL @"ListDeviceEquipmentCell"

@implementation ListDeviceTableCell

@synthesize delegate;

-(NSMutableArray *)arrayListDevice {
    
    if (!_arrayListDevice) {
        _arrayListDevice = [[NSMutableArray alloc] init];
    }
    return _arrayListDevice;
}

-(NSMutableArray *)arrayData {
    
    if (!_arrayData) {
        _arrayData = [[NSMutableArray alloc] init];
    }
    return _arrayData;
}

- (NSMutableArray *)arrayDevice {
    if (!_arrayDevice) {
        _arrayDevice = [[NSMutableArray alloc] init];
    }
    return _arrayDevice;
}

- (NSMutableArray *)arrayUpdateDeviceAdd {
    if (!_arrayUpdateDeviceAdd) {
        _arrayUpdateDeviceAdd = [[NSMutableArray alloc] init];
    }
    return _arrayUpdateDeviceAdd;
}

- (void)awakeFromNib {
    [super awakeFromNib];
   
    self.tableViewInfo.delegate = self;
    self.tableViewInfo.dataSource = self;
    
    self.constraintHeightTableView.constant = 0;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

//Reload Tableview
-(void) reloadTableViewCell {
    
    [self.tableViewInfo reloadData];
    
    self.constraintHeightTableView.constant = self.arrayData.count * 160;
    
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = 0;
    
    count = self.arrayData.count;
    
    return count;
}
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 160;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ListDeviceEquipmentCell *cell = (ListDeviceEquipmentCell *)[tableView dequeueReusableCellWithIdentifier:LISTDEVICE_CELL];
    cell.delegate = self;
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:LISTDEVICE_CELL owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    // tao moi
    self.Id = StringFormat(@"%@",self.Id);
    cell.Id = self.Id;
    if ([self.Id isEqualToString:@"(null)"]) {
        
        cell.indexPath = indexPath;
        cell.lblNameDevices.text = [self.arrayData [indexPath.row]valueForKey:@"DeviceName"];
        cell.array = self.arrayData;
        cell.deviceID = [self.arrayData [indexPath.row]valueForKey:@"DeviceID"];
        cell.arrayDevice  = self.arrayData[indexPath.row];
        
        cell.countTextField.text = [self.arrayData [indexPath.row]valueForKey:@"Number"];
        [cell.btnPromotionListDevice setTitle:[self.arrayData[indexPath.row]valueForKey:@"PromotionDeviceText"] forState:UIControlStateNormal];
  
    }
    
    // cap nhat
    if (![self.Id isEqualToString:@"(null)"]) {
        
        cell.lblNameDevices.text = [self.arrayData [indexPath.row]valueForKey:@"DeviceName"];
        cell.deviceID = [self.arrayData [indexPath.row]valueForKey:@"DeviceID"];
        cell.countTextField.text = StringFormat(@"%d",[[self.arrayData [indexPath.row]valueForKey:@"Number"] intValue]);
        
        [cell.btnPromotionListDevice setTitle:[self.arrayData [indexPath.row]valueForKey:@"PromotionDeviceText"] forState:UIControlStateNormal];
        cell.arrayDevice  = self.arrayData[indexPath.row];
        
    }
    
    return cell;
}

- (void)getArrayPromotionListDevice1:(NSMutableArray *)arrayPromotionListDevice1 ArrayDevice:(NSMutableArray *)arrayDevice1 {
    [delegate getArrayPromotion2:arrayPromotionListDevice1 ArrayDevice:arrayDevice1];
    
}

- (void)getNumberCount:(NSString *)stringCount {
    [self.delegate getNumberCount:stringCount ];
    
}


@end
