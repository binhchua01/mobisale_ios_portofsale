//
//  UploadImageCell.m
//  MobiSale
//
//  Created by StevenNguyen on 12/29/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "UploadImageCell.h"

@implementation UploadImageCell {

}

@synthesize delegate;

//MARK: -ACTION
-(IBAction)pressedChooseImage:(id)sender {
    
    [delegate ButtonUploadImage];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
