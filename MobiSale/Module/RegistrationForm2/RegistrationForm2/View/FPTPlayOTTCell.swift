//
//  FPTPlayOTTCell.swift
//  MobiSale
//
//  Created by Nguyen Sang on 1/22/18.
//  Copyright © 2018 FPT.RAD.FTool. All rights reserved.
//

import UIKit

@objc protocol FPTPlayOTTCellDelegate:class {
    func changeValueFPTPlayOTT(dic:NSDictionary)
}

public class FPTPlayOTTCell: UITableViewCell {
    
    @IBOutlet weak var viewBorder:UIView!
    
    @IBOutlet weak var minusButton:UIButton!
    
    @IBOutlet weak var plusButton:UIButton!
    
    @IBOutlet weak var countTextField:UITextField!
    
    @IBOutlet weak var lblNameDevices:UILabel!
    
    @IBOutlet weak var btnPromotionListDevice:UIButton!
    
    weak var delegate:FPTPlayOTTCellDelegate!
    
    let maximum = 100
    let minimum = 1
    
    var deviceOTTID:Int = 0
    var arrListPromotionOTT:[NSDictionary] = []
    
    var dicOTT:NSDictionary = [:]
    
}

//MARK: -ACTION
extension FPTPlayOTTCell {
    
    //btnMinusButton_Cliked
    @IBAction func btnMinusButton_Cliked(sender:UIButton) {
        self.changedValueCount(sender: sender)
    }
    
    //btnPlustButton_clicked
    @IBAction func btnPlustButton_clicked(sender:UIButton) {
        self.changedValueCount(sender: sender)
    }
    
    //btnPromotionListDevice_clicked
    @IBAction func btnPromotionListDevice_clicked(sender:UIButton) {
        
        self.getPromotionOTT(deviceID: self.deviceOTTID)
        
    }
    
}

extension FPTPlayOTTCell {
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        
        self.configBase()
        
    }
    
    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

extension FPTPlayOTTCell {
    
    func configCell(dic:NSDictionary) {
        
        self.dicOTT = dic
        
        let deviceID = dic.value(forKey: FPTPlayBoxOTT.id.rawValue) as? Int
        self.deviceOTTID = deviceID ?? 0
        
        let strNameDevice = dic.value(forKey: FPTPlayBoxOTT.name.rawValue) as? String
        self.lblNameDevices.text = strNameDevice ?? ""
        
        let promotionID = dic.value(forKey: "PromotionID") as? Int
        if (promotionID ?? 0) == -1 {
            self.btnPromotionListDevice.setTitle("Vui lòng chọn CLKM", for: .normal)
        } else {
            let textPromotion = dic.value(forKey: "PromotionText") as? String
            self.btnPromotionListDevice.setTitle(textPromotion ?? "", for: .normal)
        }
        
        let countOTT = dic.value(forKey: FPTPlayBoxOTT.number.rawValue) as? Int
        if (countOTT ?? 0) == 0 {
            self.countTextField.text = "1"
        } else {
            self.countTextField.text = "\(countOTT ?? 0)"
        }
        
    }
    
    func configBase() {
        
        self.viewBorder.layer.cornerRadius = 5
        self.viewBorder.layer.borderWidth = 1
        self.viewBorder.layer.borderColor = UIColor(red: 18.0/255.0, green: 193.0/255.0, blue: 164.0/255.0, alpha: 1.0).cgColor
        
        self.minusButton.setStyle()
        self.plusButton.setStyle()
        
        self.countTextField.isUserInteractionEnabled = false
        
    }
    
    func changedValueCount(sender:UIButton) {
        
        var countValue = Int(self.countTextField.text ?? "0") ?? 0
        
        if sender == self.minusButton {
            countValue -= 1
            if countValue < self.minimum {
                countValue = self.minimum
            }
        } else if sender == self.plusButton {
            countValue += 1
            if countValue > self.maximum {
                countValue = self.maximum
            }
        }
        
        self.countTextField.text = "\(countValue)"
        self.changeValueOTT(key: FPTPlayBoxOTT.number.rawValue, value: countValue)
        
    }
    
    func setupDropDownView() {
        
        self.viewBorder.endEditing(true)
        let xWidth = self.viewBorder.bounds.size.width - 20
        let yHeight:CGFloat = 272
        let yOffset = (self.viewBorder.bounds.size.height - yHeight)/2
        let rectFrame = CGRect(x: 10, y: yOffset, width: xWidth, height: yHeight)
        let popListView = UIPopoverListView(frame: rectFrame)
        popListView.delegate = self
        popListView.datasource = self
        popListView.listView.isScrollEnabled = true
        popListView.isUserInteractionEnabled = true
        popListView.setTitle("Câu lệnh khuyến mãi của thiết bị")
        popListView.show()
        
    }
    
    func changeValueOTT(key:String, value:Any) {
        
        self.dicOTT.setValue(value, forKey: key)
        self.delegate.changeValueFPTPlayOTT(dic: self.dicOTT)
        print(self.dicOTT)
        
    }
    
}

//MARK: -API
extension FPTPlayOTTCell {
    
    func getPromotionOTT(deviceID:Int) {
        
        if !Common_client.isNetworkAvailable() {
            Common_client.showAlertMessage("Kết nối thất bại, Vui lòng kiểm tra lại đường truyền")
            return
        }
        
        let shared = ShareData.instance()
        
        let dic:NSDictionary = NSDictionary(dictionary: ["UserName":shared?.currentUser.userName ?? "","LocationID":shared?.currentUser.locationID ?? "","DeviceID":"\(deviceID)"])
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ShowMBProcessListRegis"), object: nil)
        shared?.appProxy.getGetDeviceOTTPromotion(dic as! [AnyHashable : Any], handler: { (result, errorCode, message) in
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "HideMBProcessListRegis"), object: nil)
            if message == "het phien lam viec" {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LogOutWhenEndSession"), object: nil)
                return
            }
            if message == "Error Service: Unauthorized" {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LogOutWhenEndSession"), object: nil)
                return
            }
            if errorCode != "0" {
                Common_client.showAlertMessage(message)
                return
            }
            
            let dataJSON = result as? [NSDictionary]
            self.arrListPromotionOTT = dataJSON ?? []
            
            if self.arrListPromotionOTT.count <= 0 {
                return
            } else {
                self.setupDropDownView()
            }
            
            
        }, errorHandler: { (error) in
            print(error)
            
            
        })
        
    }
    
}

extension FPTPlayOTTCell:UIPopoverListViewDelegate, UIPopoverListViewDataSource {
    
    public func popoverListView(_ popoverListView: UIPopoverListView!, heightForRowAt indexPath: IndexPath!) -> CGFloat {
        return 50
    }
    
    public func popoverListView(_ popoverListView: UIPopoverListView!, numberOfRowsInSection section: Int) -> Int {
        return self.arrListPromotionOTT.count
    }
    
    public func popoverListView(_ popoverListView: UIPopoverListView!, cellFor indexPath: IndexPath!) -> UITableViewCell! {
        
        let identifier = "cell"
        let cell = UITableViewCell(style: .default, reuseIdentifier: identifier)
        
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.sizeToFit()
        
        let strNamePromotion = self.arrListPromotionOTT[indexPath.row].value(forKey: PromotionFPTPlayBoxOTT.name.rawValue) as? String
        cell.textLabel?.text = strNamePromotion ?? ""
        
        return cell
        
    }
    
    public func popoverListView(_ popoverListView: UIPopoverListView!, didSelect indexPath: IndexPath!) {
        
        let title = self.arrListPromotionOTT[indexPath.row].value(forKey: PromotionFPTPlayBoxOTT.name.rawValue) as? String
        self.btnPromotionListDevice.setTitle(title ?? "", for: .normal)
        self.changeValueOTT(key: FPTPlayBoxOTT.textPromotion.rawValue, value: title ?? 0)
        
        let idPromotion = self.arrListPromotionOTT[indexPath.row].value(forKey: PromotionFPTPlayBoxOTT.id.rawValue) as? Int
        self.changeValueOTT(key: PromotionFPTPlayBoxOTT.id.rawValue, value: idPromotion ?? 0)
        
        
        
        
    }
    
}













