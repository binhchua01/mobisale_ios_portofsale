//
//  ButtonTitleCell.h
//  MobiSale
//
//  Created by Bored Ninjas on 12/14/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ButtonTitleCellDelegate <NSObject>

-(void) dropDownButton: (id)sender kTag:(NSInteger)tag;

@end

@interface ButtonTitleCell : UITableViewCell

@property (nonatomic, weak) id <ButtonTitleCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *btnChoose;

- (void)configCell:(NSString *)strTitle ktag:(NSInteger)tag;


@end
