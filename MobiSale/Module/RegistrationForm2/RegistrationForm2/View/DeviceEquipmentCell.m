//
//  DeviceEquipmentCell.m
//  MobiSale
//
//  Created by Tran Vu on 6/8/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import "DeviceEquipmentCell.h"
#import "ListDeviceViewController.h"
#import "UIViewController+MJPopupViewController.h"

@implementation DeviceEquipmentCell {
    
    CGFloat top;
    CGFloat bottom;
    NSTimeInterval duration;
    
     NSInteger kTag;
}
@synthesize delegate;
- (void)awakeFromNib {
    [super awakeFromNib];
    
    
    top = 21;
    bottom = 15;
    duration = 0.3;
    
    self.btnListDevies.selected = true;
    self.lblListDevies.alpha = 0;
    
    [self configButton];
    
   
}
- (void)configButton {
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    imgView.center = CGPointMake(self.btnListDevies.frame.size.width - 25, self.btnListDevies.frame.size.height/2);
    imgView.image = [UIImage imageNamed:@"ic_downarrow"];
    [self.btnListDevies addSubview:imgView];
    

    
}

- (void)animationForCell {
    
    [UIView animateWithDuration:duration animations:^{
        
                self.contraintLabel.constant = 8;
                self.contraintBotton.constant = 8;
                self.alpha = 1;
        
                [self layoutIfNeeded];
    }];
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    
}

- (IBAction)btn_ClickedDevies:(id)sender {
    
    [self animationForCell];
    
    [self.delegate showListDevicesViewController];
    
   // [self.delegate showListDevicesViewController2];
    
}

- (void)configDropDowCell:(NSString *)strTitile PlaceTitle:(NSString *)placeTitle {
    
    self.lblListDevies.text = strTitile;
    [self.btnListDevies setTitle:placeTitle forState:UIControlStateNormal];
    
    [self animationForCell];
    
}

@end
