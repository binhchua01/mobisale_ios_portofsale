//
//  AniTextFieldCell.m
//  demoRegister
//
//  Created by Bored Ninjas on 11/23/16.
//  Copyright © 2016 com.StevenNguyen.DemoCreateRegisterForm. All rights reserved.
//

#import "AniTextFieldCell.h"

@interface AniTextFieldCell()<UITextFieldDelegate> {
    
    __weak IBOutlet NSLayoutConstraint *constaintLabel;
    
    __weak IBOutlet NSLayoutConstraint *constaintTextField;
    
}

@end

@implementation AniTextFieldCell {
    
    CGFloat top;
    CGFloat bottom;
    NSTimeInterval duration;
    
    NSInteger kTag;
    
}

@synthesize delegate;

//MARK: -ACTION
- (IBAction)changedTextField:(UITextField *)sender {
    
    [self animationForCell];
    
}

//MARK: -CYCLE LIFE
- (void)awakeFromNib {
    [super awakeFromNib];
    top = 21;
    bottom = 15.5;
    duration = 0.3;
    kTag = -1;
    self.lblTitle.alpha = 0;
    self.txtFValue.delegate = self;
}

-(void)prepareForReuse {
    [super prepareForReuse];
    
}

-(void) configCell:(NSString *)strTitle placeHolderTextField:(NSString *)strPlaceholder {
    
    self.lblTitle.text = strTitle;
    self.txtFValue.placeholder = strPlaceholder;
    
    [self animationForCell];
    
}

-(void) configCellWithTag:(NSInteger)tag {
    
    kTag = tag;
}

// animation for cell when textfield isEmpty
- (void)animationForCell {
    
    if (self.txtFValue && self.txtFValue.text.length > 0) {
        
        [UIView animateWithDuration:duration animations:^{
            
            constaintLabel.constant = 8;
            constaintTextField.constant = 8;
            self.lblTitle.alpha = 1;
            
            [self layoutIfNeeded];
            
        }];
        
    } else {
        
        [UIView animateWithDuration:duration animations:^{
            
            constaintLabel.constant = top;
            constaintTextField.constant = bottom;
            self.lblTitle.alpha = 0;
            
            [self layoutIfNeeded];
            
        }];
        
    }
    
}

//MARK: -UITEXTFIELDDELEGATE
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField*)textField {
    
    [self.delegate valueTextfield:textField.text kTag:kTag];
}

@end
