//
//  AniPhoneNumberCell.m
//  demoRegister
//
//  Created by Bored Ninjas on 11/23/16.
//  Copyright © 2016 com.StevenNguyen.DemoCreateRegisterForm. All rights reserved.
//

#import "AniPhoneNumberCell.h"

@interface AniPhoneNumberCell()<UITextFieldDelegate> {
    
    __weak IBOutlet NSLayoutConstraint *constraintPhoneNumber;
    
    __weak IBOutlet NSLayoutConstraint *constraintLabelPhoneNumber;
    
    __weak IBOutlet NSLayoutConstraint *constraintNameUser;
    
    __weak IBOutlet NSLayoutConstraint *constraintLabelNameUser;
    
}

@end

@implementation AniPhoneNumberCell {
    
    CGFloat top;
    CGFloat bottom;
    NSTimeInterval duration;
    
    NSInteger kTag;
    
}

@synthesize delegate;

//MARK: -ACTION
- (IBAction)changedPhoneNumber:(UITextField *)sender {
    
    [self animationForPhoneNumber];
    
}

//MARK: -ACTION
- (IBAction)changedNameUser:(UITextField *)sender {
    
    [self animationForUserName];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //static value
    top = 22;
    bottom = 15;
    duration = 0.3;
    
    kTag = -1;
    
    self.lblUserName.alpha = 0;
    self.lblPhoneNumber.alpha = 0;
    
    self.txtfNameUser.delegate = self;
    self.txtfPhoneNumber.delegate = self;
    
    self.txtfPhoneNumber.tag = 1;
    self.txtfNameUser.tag = 2;
    
}

-(void) configCell:(NSString *)strPhoneNumber placeHolderPhoneNumber:(NSString *)strPlacePhoneNumber titleUserName:(NSString *)strUserName placeHolderUserName:(NSString *)strPlaceUserName {
    
    self.lblPhoneNumber.text = strPhoneNumber;
    self.txtfPhoneNumber.placeholder = strPlacePhoneNumber;
    
    self.lblUserName.text = strUserName;
    self.txtfNameUser.placeholder = strPlaceUserName;
    
    [self animationForPhoneNumber];
    [self animationForUserName];
    
}

-(void) configCellWhenSetText:(NSString *)strPhoneNumber username:(NSString *)strUserName {
    
    if (![strPhoneNumber  isEqual: @""]) {
        
        self.txtfPhoneNumber.text = strPhoneNumber;
        [self animationForPhoneNumber];
        
    }
    
    if (![strUserName isEqual:@""]) {
        
        self.txtfNameUser.text = strUserName;
        [self animationForUserName];
        
    }
    
}

-(void) configCellWithTag:(NSInteger)tag {
    
    kTag = tag;
    
}

//MARK: -ANIMATION
-(void)animationForPhoneNumber {
    
    if (self.txtfPhoneNumber.text && self.txtfPhoneNumber.text.length > 0) {
        
        [UIView animateWithDuration:duration animations:^{
            
            constraintLabelPhoneNumber.constant = 8;
            constraintPhoneNumber.constant = 8;
            self.lblPhoneNumber.alpha = 1;
            
            [self layoutIfNeeded];
            
        }];
        
        
    } else {
        
        [UIView animateWithDuration:duration animations:^{
            
            constraintLabelPhoneNumber.constant = top;
            constraintPhoneNumber.constant = bottom;
            self.lblPhoneNumber.alpha = 0;
            
            [self layoutIfNeeded];
            
        }];
        
    }
    
}

-(void)animationForUserName {
    
    if (self.txtfNameUser.text && self.txtfNameUser.text.length > 0) {
        
        [UIView animateWithDuration:duration animations:^{
            
            constraintLabelNameUser.constant = 8;
            constraintNameUser.constant = 8;
            self.lblUserName.alpha = 1;
            
            [self layoutIfNeeded];
            
        }];
        
        
    } else {
        
        [UIView animateWithDuration:duration animations:^{
            
            constraintLabelNameUser.constant = top;
            constraintNameUser.constant = bottom;
            self.lblUserName.alpha = 0;
            
            [self layoutIfNeeded];
            
        }];
        
    }
    
}

//MARK: -UITEXTFIELDDELEGATE

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
    
}

-(void)textFieldDidEndEditing:(UITextField*)textField {
    
    NSString *str = textField.text;
    
    //Phone Number
    if (textField.tag == 1) {
        
        [self.delegate valueTextfield:str kTag:kTag tagTextField:1];
        
    } else if (textField.tag == 2) {
    //User Name
        
        [self.delegate valueTextfield:str kTag:kTag tagTextField:2];
        
    }
    
}


@end
