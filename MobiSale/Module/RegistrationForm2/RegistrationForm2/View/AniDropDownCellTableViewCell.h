//
//  AniDropDownCellTableViewCell.h
//  MobiSale
//
//  Created by Bored Ninjas on 11/25/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AniDropDownCellTableViewCellDelegate <NSObject>

-(void) dropDownButton: (id)sender kTag:(NSInteger)tag;

-(void) freshCompleted:(NSInteger)tag;

@end

@interface AniDropDownCellTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnChoose;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBottom;

@property (nonatomic, weak) id <AniDropDownCellTableViewCellDelegate> delegate;

-(void) configDropDownCell:(NSString*)strTitle titleButton:(NSString*)strTitleButton kTag:(NSInteger)tag;

@end
