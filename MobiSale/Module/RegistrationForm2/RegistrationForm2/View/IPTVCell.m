//
//  IPTVCell.m
//  demoRegister
//
//  Created by Bored Ninjas on 11/23/16.
//  Copyright © 2016 com.StevenNguyen.DemoCreateRegisterForm. All rights reserved.
//

#import "IPTVCell.h"
#import "SAStepperControl.h"
#import "ListRegisteredFormViewController.h"
#import "RegistrationFormModel.h"

enum IPTVCell {
    serviceBase = 6,
    deploy = 7,
    combo = 8,
    service = 9,
    promotion1 = 10,
    promotion2 = 11,
    filmPlus = 12,
    filmHot = 13,
    filmKPlus = 14,
    filmHD = 15,
    filmVTVCab = 16
};

@implementation UIColor (HexString)

+ (UIColor *)colorFromHexString:(NSString *)hexString {
    
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1];
    [scanner scanHexInt:&rgbValue];
    
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
    
}

@end

@implementation IPTVCell {
    
    CGFloat top;
    CGFloat bottom;
    NSTimeInterval duration;
    
    NSInteger kTag;
    
    
}

@synthesize delegate;

//MARK: -ACTION CHECKBOX
-(IBAction)RadioButtonSetup:(RadioButton *)sender {
    if(sender == self.rdYesSetup){
        self.rdYesSetup.selected = YES;
        self.rdNoSetup.selected = NO;
        //        CableStatus = @"1";
        [[RegistrationFormModel sharedInstance] setIptvRequest:@"0"];
    }else {
        self.rdYesSetup.selected = NO;
        self.rdNoSetup.selected = YES;
        //        CableStatus = @"2";
        [[RegistrationFormModel sharedInstance] setIptvRequest:@"1"];
    }
}

-(IBAction)RadioButtonDrillWall:(RadioButton *)sender {
    if(sender == self.rdYesDrillWall){
        self.rdYesDrillWall.selected = YES;
        self.rdNoDrillWall.selected = NO;
        //        CableStatus = @"1";
        [[RegistrationFormModel sharedInstance] setIptvWallDrilling:@"0"];
    }else {
        self.rdYesDrillWall.selected = NO;
        self.rdNoDrillWall.selected = YES;
        //        CableStatus = @"2";
        [[RegistrationFormModel sharedInstance] setIptvWallDrilling:@"1"];
    }
}

//MARK: -ACTION
-(IBAction)pressedServiceBase:(id)sender {
    
    [delegate dropDownIPTVButton:sender kTag:kTag tagButton:serviceBase];
    
}

-(IBAction)pressedCombo:(id)sender {
    
    [delegate dropDownIPTVButton:sender kTag:kTag tagButton:combo];
    
}

-(IBAction)pressedDeploy:(id)sender {
    
    [delegate dropDownIPTVButton:sender kTag:kTag tagButton:deploy];
    
}

-(IBAction)pressedService:(id)sender {
    
    [delegate dropDownIPTVButton:sender kTag:kTag tagButton:service];
    
}

-(IBAction)pressedPromotion1:(id)sender {
    
    [delegate dropDownIPTVButton:sender kTag:kTag tagButton:promotion1];
    
}

-(IBAction)pressedPromotion2:(id)sender {
    
    [delegate dropDownIPTVButton:sender kTag:kTag tagButton:promotion2];
    
}

-(IBAction)pressedFilmPlus:(id)sender {
    
    if (self.btnFilmPlus.selected == YES) {
        self.btnFilmPlus.selected = NO;
        self.stepperFilmPlusPrepay.enabled = NO;
        self.stepperFilmPlusPostage.enabled = NO;
        self.stepperFilmPlusPrepay.value = 0;
        self.stepperFilmPlusPostage.minimumValue = 0;
        self.stepperFilmPlusPostage.value = 0;
        [[RegistrationFormModel sharedInstance] setIptvFilmPlusChargeTime:@"0"];
        [[RegistrationFormModel sharedInstance] setIptvFilmPlusPrepaidMonth:@"0"];
        
    } else {
        self.btnFilmPlus.selected = YES;
        self.stepperFilmPlusPrepay.enabled = YES;
        self.stepperFilmPlusPostage.enabled = YES;
        self.stepperFilmPlusPrepay.value = 0;
        self.stepperFilmPlusPostage.value = 1;
        self.stepperFilmPlusPostage.minimumValue = 1;
        [[RegistrationFormModel sharedInstance] setIptvFilmPlusChargeTime:@"1"];
        [[RegistrationFormModel sharedInstance] setIptvFilmPlusPrepaidMonth:@"0"];
        
    }
    
    [delegate dropDownIPTVButton:sender kTag:kTag tagButton:filmPlus];
    
}

-(IBAction)pressedFilmHot:(id)sender {
    
    if (self.btnFilmHot.selected == YES) {
        self.btnFilmHot.selected = NO;
        self.stepperFilmHotPrepay.enabled = NO;
        self.stepperFilmHotPostage.enabled = NO;
        self.stepperFilmHotPrepay.value = 0;
        self.stepperFilmHotPostage.minimumValue = 0;
        self.stepperFilmHotPostage.value = 0;
        [[RegistrationFormModel sharedInstance] setIptvFilmHotChargeTime:@"0"];
        [[RegistrationFormModel sharedInstance] setIptvFilmHotPrepaidMonth:@"0"];
        
    } else {
        self.btnFilmHot.selected = YES;
        self.stepperFilmHotPrepay.enabled = YES;
        self.stepperFilmHotPostage.enabled = YES;
        self.stepperFilmHotPrepay.value = 0;
        self.stepperFilmHotPostage.value = 1;
        self.stepperFilmHotPostage.minimumValue = 1;
        [[RegistrationFormModel sharedInstance] setIptvFilmHotChargeTime:@"1"];
        [[RegistrationFormModel sharedInstance] setIptvFilmHotPrepaidMonth:@"0"];
        
    }
    
    [delegate dropDownIPTVButton:sender kTag:kTag tagButton:filmHot];
    
}

-(IBAction)pressedFilmKPlus:(id)sender {
    
    if (self.btnFilmKPlus.selected == YES) {
        self.btnFilmKPlus.selected = NO;
        self.stepperKPlusPrepay.enabled = NO;
        self.stepperKPlusPostage.enabled = NO;
        self.stepperKPlusPrepay.value = 0;
        self.stepperKPlusPostage.minimumValue = 0;
        self.stepperKPlusPostage.value = 0;
        
        [[RegistrationFormModel sharedInstance] setIptvFilmKPlusChargeTime:@"0"];
        [[RegistrationFormModel sharedInstance] setIptvFilmKPlusPrepaidMonth:@"0"];
        
    } else {
        self.btnFilmKPlus.selected = YES;
        self.stepperKPlusPrepay.enabled = YES;
        self.stepperKPlusPostage.enabled = YES;
        self.stepperKPlusPrepay.value = 0;
        self.stepperKPlusPostage.value = 1;
        self.stepperKPlusPostage.minimumValue = 1;
        [[RegistrationFormModel sharedInstance] setIptvFilmKPlusChargeTime:@"1"];
        [[RegistrationFormModel sharedInstance] setIptvFilmKPlusPrepaidMonth:@"0"];
        
    }
    
    [delegate dropDownIPTVButton:sender kTag:kTag tagButton:filmKPlus];
    
}

-(IBAction)pressedFilmHD:(id)sender {
    
    if (self.btnFilmHD.selected == YES) {
        self.btnFilmHD.selected = NO;
        self.stepperHDPrepay.enabled = NO;
        self.stepperHDPostage.enabled = NO;
        self.stepperHDPrepay.value = 0;
        self.stepperHDPostage.minimumValue = 0;
        self.stepperHDPostage.value = 0;
        
        [[RegistrationFormModel sharedInstance] setIptvFilmVTCChargeTime:@"0"];
        [[RegistrationFormModel sharedInstance] setIptvFilmVTCPrepaidMonth:@"0"];
        
    } else {
        self.btnFilmHD.selected = YES;
        self.stepperHDPrepay.enabled = YES;
        self.stepperHDPostage.enabled = YES;
        self.stepperHDPrepay.value = 0;
        self.stepperHDPostage.value = 1;
        self.stepperHDPostage.minimumValue = 1;
        [[RegistrationFormModel sharedInstance] setIptvFilmVTCChargeTime:@"1"];
        [[RegistrationFormModel sharedInstance] setIptvFilmVTCPrepaidMonth:@"0"];
        
    }

    
    [delegate dropDownIPTVButton:sender kTag:kTag tagButton:filmHD];
    
}

-(IBAction)pressedFilmVTVCab:(id)sender {
    
    if (self.btnFilmVTVCab.selected == YES) {
        self.btnFilmVTVCab.selected = NO;
        self.stepperVTVCabPrepay.enabled = NO;
        self.stepperVTVCabPostage.enabled = NO;
        self.stepperVTVCabPrepay.value = 0;
        self.stepperVTVCabPostage.minimumValue = 0;
        self.stepperVTVCabPostage.value = 0;
        
        [[RegistrationFormModel sharedInstance] setIptvFilmVTVChargeTime:@"0"];
        [[RegistrationFormModel sharedInstance] setIptvFilmVTVPrepaidMonth:@"0"];
        
    } else {
        self.btnFilmVTVCab.selected = YES;
        self.stepperVTVCabPrepay.enabled = YES;
        self.stepperVTVCabPostage.enabled = YES;
        self.stepperVTVCabPrepay.value = 0;
        self.stepperVTVCabPostage.value = 1;
        self.stepperVTVCabPostage.minimumValue = 1;
        [[RegistrationFormModel sharedInstance] setIptvFilmVTVChargeTime:@"1"];
        [[RegistrationFormModel sharedInstance] setIptvFilmVTVPrepaidMonth:@"0"];
        
    }

    
    [delegate dropDownIPTVButton:sender kTag:kTag tagButton:filmVTVCab];
    
}

//MARK: -ACTION STEPPER
-(IBAction)pressedSteppedBox:(id)sender {

    
    int number = (int)self.stepperBox.value;
    NSString *strNumber = [NSString stringWithFormat:@"%d", number];
    [[RegistrationFormModel sharedInstance] setIptvDeviceBoxCount:strNumber];
    
    self.stepperPostage.maximumValue = self.stepperBox.value;
    self.stepperFilmPlusPostage.maximumValue = self.stepperBox.value;
    self.stepperFilmHotPostage.maximumValue = self.stepperBox.value;
    self.stepperKPlusPostage.maximumValue = self.stepperBox.value;
    self.stepperVTVCabPostage.maximumValue = self.stepperBox.value;
    self.stepperHDPostage.maximumValue = self.stepperBox.value;
    
    if (number == 1) {
        
        self.btnPromotion2.enabled = NO;
        
    } else {
        
        self.btnPromotion2.enabled = YES;
        
    }
    
    [delegate PressedBoxCount:number];
}

-(IBAction)pressedSteppedPLC:(id)sender {
    
    int number = (int)self.stepperPLC.value;
    NSString *strNumber = [NSString stringWithFormat:@"%d", number];
    [[RegistrationFormModel sharedInstance] setIptvDevicePLCCount:strNumber];
}

-(IBAction)pressedSteppedSTB:(id)sender {
    
    int number = (int)self.stepperSTB.value;
    NSString *strNumber = [NSString stringWithFormat:@"%d", number];
    [[RegistrationFormModel sharedInstance] setIptvDeviceSTBCount:strNumber];
}

-(IBAction)pressedSteppedPostage:(id)sender {
    
    int number = (int)self.stepperPostage.value;
    NSString *strNumber = [NSString stringWithFormat:@"%d", number];
    [[RegistrationFormModel sharedInstance] setIptvChargeTimes:strNumber];
}

-(IBAction)pressedSteppedFilmPlusPostage:(id)sender {
    
    int number = (int)self.stepperFilmPlusPostage.value;
    NSString *strNumber = [NSString stringWithFormat:@"%d", number];
    [[RegistrationFormModel sharedInstance] setIptvFilmPlusChargeTime:strNumber];
}

-(IBAction)pressedSteppedFilmPlusPrepay:(id)sender {
    
    int number = (int)self.stepperFilmPlusPrepay.value;
    NSString *strNumber = [NSString stringWithFormat:@"%d", number];
    [[RegistrationFormModel sharedInstance] setIptvFilmPlusPrepaidMonth:strNumber];
}

-(IBAction)pressedSteppedFilmHotPostage:(id)sender {
    
    int number = (int)self.stepperFilmHotPostage.value;
    NSString *strNumber = [NSString stringWithFormat:@"%d", number];
    [[RegistrationFormModel sharedInstance] setIptvFilmHotChargeTime:strNumber];
}

-(IBAction)pressedSteppedFilmHotPrepay:(id)sender {
    
    int number = (int)self.stepperFilmHotPrepay.value;
    NSString *strNumber = [NSString stringWithFormat:@"%d", number];
    [[RegistrationFormModel sharedInstance] setIptvFilmHotPrepaidMonth:strNumber];
}

-(IBAction)pressedSteppedKPlusPostage:(id)sender {
    
    int number = (int)self.stepperKPlusPostage.value;
    NSString *strNumber = [NSString stringWithFormat:@"%d", number];
    [[RegistrationFormModel sharedInstance] setIptvFilmKPlusChargeTime:strNumber];
}

-(IBAction)pressedSteppedKPlusPrepay:(id)sender {
    
    int number = (int)self.stepperKPlusPrepay.value;
    NSString *strNumber = [NSString stringWithFormat:@"%d", number];
    [[RegistrationFormModel sharedInstance] setIptvFilmKPlusPrepaidMonth:strNumber];
}

-(IBAction)pressedSteppedHDPostage:(id)sender {
    
    int number = (int)self.stepperHDPostage.value;
    NSString *strNumber = [NSString stringWithFormat:@"%d", number];
    [[RegistrationFormModel sharedInstance] setIptvFilmVTCChargeTime:strNumber];
}

-(IBAction)pressedSteppedHDPrepay:(id)sender {
    
    int number = (int)self.stepperHDPrepay.value;
    NSString *strNumber = [NSString stringWithFormat:@"%d", number];
    [[RegistrationFormModel sharedInstance] setIptvFilmVTCPrepaidMonth:strNumber];
}

-(IBAction)pressedSteppedVTVCabPostage:(id)sender {
    
    int number = (int)self.stepperVTVCabPostage.value;
    NSString *strNumber = [NSString stringWithFormat:@"%d", number];
    [[RegistrationFormModel sharedInstance] setIptvFilmVTVChargeTime:strNumber];
}

-(IBAction)pressedSteppedVTVCabPrepay:(id)sender {
    
    int number = (int)self.stepperVTVCabPrepay.value;
    NSString *strNumber = [NSString stringWithFormat:@"%d", number];
    [[RegistrationFormModel sharedInstance] setIptvFilmVTVPrepaidMonth:strNumber];
}

//MARK: -CYCLE LIFE
- (void)awakeFromNib {
    [super awakeFromNib];
    
    top = 22;
    bottom = 15;
    duration = 0.3;
    kTag = -1;
    
    //self.lblServiceBase.alpha = 0;
    self.lblCombo.alpha = 0;
    self.lblDeploy.alpha = 0;
    self.lblService.alpha = 0;
    self.lblPromotion1.alpha = 0;
    self.lblPromotion2.alpha = 0;
    
    [self configSteppers];
    [self configButton];
    
}

- (void)configPriceCell:(NSString *)strPricePlus FilmHot:(NSString *)strPriceHot FilmKPlus:(NSString *)strPriceKPlus FilmHD:(NSString *)strPriceHD FilmVTV:(NSString *)strPriceVTV {
    
    self.lblPriceFilmPlus.text = strPricePlus;
    self.lblPriceHot.text = strPriceHot;
    self.lblPriceKPlus.text = strPriceKPlus;
    self.lblPriceHD.text = strPriceHD;
    self.lblPriceVTV.text = strPriceVTV;
    
}

- (void)configCell:(NSString *)serviceBase cbRequest:(NSString *)cbRequest cbDrillWall:(NSString *)cbDrillWall combo:(NSString *)strCombo delopment:(NSString *)strDelop IPTVPackage:(NSString *)iptvPackage promotion1:(NSString *)promotion1 promotion2:(NSString *)promotion2{
    
    if ([serviceBase  isEqual: @""]) {
        [self.btnServiceBase setTitle:@"Gói dịch vụ" forState:UIControlStateNormal];
    } else {
        [self.btnServiceBase setTitle:serviceBase forState:UIControlStateNormal];
    }
    
    if ([strCombo  isEqual: @""]) {
        [self.btnCombo setTitle:@"Chọn Combo" forState:UIControlStateNormal];
    } else {
        [self.btnCombo setTitle:strCombo forState:UIControlStateNormal];
    }
    
    if ([strDelop  isEqual: @""]) {
        [self.btnDeploy setTitle:@"Hình thức triển khai" forState:UIControlStateNormal];
    } else {
        [self.btnDeploy setTitle:strDelop forState:UIControlStateNormal];
    }
    
    if ([iptvPackage  isEqual: @""]) {
        [self.btnService setTitle:@"Gói dịch vụ" forState:UIControlStateNormal];
    } else {
        [self.btnService setTitle:iptvPackage forState:UIControlStateNormal];
    }
    
    if ([promotion1  isEqual: @""]) {
        [self.btnPromotion1 setTitle:@"Khuyến mãi box đầu tiên" forState:UIControlStateNormal];
        self.txtvPromotion1.text = @"Khuyến mãi box đầu tiên";
    } else {
        [self.btnPromotion1 setTitle:promotion1 forState:UIControlStateNormal];
        self.txtvPromotion1.text = promotion1;
    }
    
    if ([promotion2  isEqual: @""]) {
        [self.btnPromotion2 setTitle:@"Khuyến mãi box thứ hai" forState:UIControlStateNormal];
        self.txtvPromotion2.text = @"Khuyến mãi box thứ hai";
    } else {
        [self.btnPromotion2 setTitle:promotion2 forState:UIControlStateNormal];
        self.txtvPromotion2.text = promotion2;
    }

    
    if ([cbRequest  isEqual: @"0"]) {
        self.rdYesSetup.selected = YES;
        self.rdNoSetup.selected = NO;
    } else {
        self.rdYesSetup.selected = NO;
        self.rdNoSetup.selected = YES;
    }
    
    if ([cbDrillWall  isEqual: @"0"]) {
        self.rdYesDrillWall.selected = YES;
        self.rdNoDrillWall.selected = NO;
    } else {
        self.rdYesDrillWall.selected = NO;
        self.rdNoDrillWall.selected = YES;
    }
}

-(void) configSteppersForCell:(NSString *)strBoxCount PLCCount:(NSString *)strPLCCount STBCount:(NSString *)strSTBCount PostageCount:(NSString *)strPortageCount {
    
    double douBox = [strBoxCount doubleValue] ?:0.0;
    double douPLC = [strPLCCount doubleValue] ?:0.0;
    double douSTB = [strSTBCount doubleValue] ?:0.0;
    double douPostage = [strPortageCount doubleValue] ?:0.0;
    
    if (douBox == 0 && douPostage == 0) {
        [[RegistrationFormModel sharedInstance] setIptvDeviceBoxCount:@"1"];
        [[RegistrationFormModel sharedInstance] setIptvChargeTimes:@"1"];
        
        self.btnPromotion2.enabled = NO;
        
    }
    
    self.stepperBox.value = douBox;
    self.stepperPLC.value = douPLC;
    self.stepperSTB.value = douSTB;
    self.stepperPostage.value = douPostage;
    
    self.stepperPostage.maximumValue = self.stepperBox.value;
    self.stepperFilmPlusPostage.maximumValue = self.stepperBox.value;
    self.stepperFilmHotPostage.maximumValue = self.stepperBox.value;
    self.stepperKPlusPostage.maximumValue = self.stepperBox.value;
    self.stepperVTVCabPostage.maximumValue = self.stepperBox.value;
    self.stepperHDPostage.maximumValue = self.stepperBox.value;
    
}

-(void) configFilmForCell:(NSString *)strFilmPlusChargeTime FilmPlusPrepaidMonth:(NSString *)strFilmPlusPrepaidMonth FilmHotChargeTime:(NSString *)strHotChargeTime FilmHotPrepaidMonth:(NSString *)strHotPrepaidMonth FilmKPlusChargeTime:(NSString *)strKPlusChargeTime FilmKPlusPrepaidMonth:(NSString *)strKPlusPrepaidMonth FilmVTCChargeTime:(NSString *)strVTCChargeTime FilmVTCPrepaidMonth:(NSString *)strVTCPrepaidMonth FilmVTVChargeTime:(NSString *)strVTVChargeTime FilmVTVPrepaidMonth:(NSString *)strVTVPrepaidMonth {
    
    double FilmPlusCharge = [strFilmPlusChargeTime doubleValue] ?:0.0;
    double FilmPlusPrepaid = [strFilmPlusPrepaidMonth doubleValue] ?:0.0;
    double FilmHotCharge = [strHotChargeTime doubleValue] ?:0.0;
    double FilmHotPrepaid = [strHotPrepaidMonth doubleValue] ?:0.0;
    double FilmKPlusCharge = [strKPlusChargeTime doubleValue] ?:0.0;
    double FilmKPlusPrepaid = [strKPlusPrepaidMonth doubleValue] ?:0.0;
    double FilmVTCCharge = [strVTCChargeTime doubleValue] ?:0.0;
    double FilmVTCPrepaid = [strVTCPrepaidMonth doubleValue] ?:0.0;
    double FilmVTVCharge = [strVTVChargeTime doubleValue] ?:0.0;
    double FilmVTVPrepaid = [strVTVPrepaidMonth doubleValue] ?:0.0;
    
    
    if (![strFilmPlusChargeTime isEqualToString:@"0"]) {
        self.btnFilmPlus.selected = YES;
        self.stepperFilmPlusPrepay.enabled = YES;
        self.stepperFilmPlusPostage.enabled = YES;
        self.stepperFilmPlusPrepay.value = FilmPlusPrepaid;
        self.stepperFilmPlusPostage.value = FilmPlusCharge;
        self.stepperFilmPlusPostage.minimumValue = 1;
    }
    if (![strHotChargeTime isEqualToString:@"0"]) {
        self.btnFilmHot.selected = YES;
        self.stepperFilmHotPrepay.enabled = YES;
        self.stepperFilmHotPostage.enabled = YES;
        self.stepperFilmHotPrepay.value = FilmHotPrepaid;
        self.stepperFilmHotPostage.value = FilmHotCharge;
        self.stepperFilmHotPostage.minimumValue = 1;
    }
    if (![strKPlusChargeTime isEqualToString:@"0"]) {
        self.btnFilmKPlus.selected = YES;
        self.stepperKPlusPrepay.enabled = YES;
        self.stepperKPlusPostage.enabled = YES;
        self.stepperKPlusPrepay.value = FilmKPlusPrepaid;
        self.stepperKPlusPostage.value = FilmKPlusCharge;
        self.stepperKPlusPostage.minimumValue = 1;
    }
    if (![strVTCChargeTime isEqualToString:@"0"]) {
        self.btnFilmHD.selected = YES;
        self.stepperHDPrepay.enabled = YES;
        self.stepperHDPostage.enabled = YES;
        self.stepperHDPrepay.value = FilmVTCPrepaid;
        self.stepperHDPostage.value = FilmVTCCharge;
        self.stepperHDPostage.minimumValue = 1;
    }
    if (![strVTVChargeTime isEqualToString:@"0"]) {
        self.btnFilmVTVCab.selected = YES;
        self.stepperVTVCabPrepay.enabled = YES;
        self.stepperVTVCabPostage.enabled = YES;
        self.stepperVTVCabPrepay.value = FilmVTVPrepaid;
        self.stepperVTVCabPostage.value = FilmVTVCharge;
        self.stepperVTVCabPostage.minimumValue = 1;
    }

    
    
}

- (void)configSteppers {
    
    self.stepperBox.tintColor = [UIColor colorFromHexString:@"22B2A8"];
    self.stepperPLC.tintColor = [UIColor colorFromHexString:@"22B2A8"];
    self.stepperSTB.tintColor = [UIColor colorFromHexString:@"22B2A8"];
    self.stepperPostage.tintColor = [UIColor colorFromHexString:@"22B2A8"];
    
    self.stepperVTVCabPostage.tintColor = [UIColor colorFromHexString:@"22B2A8"];
    self.stepperVTVCabPrepay.tintColor = [UIColor colorFromHexString:@"22B2A8"];
    
    self.stepperFilmPlusPostage.tintColor = [UIColor colorFromHexString:@"22B2A8"];
    self.stepperFilmPlusPrepay.tintColor = [UIColor colorFromHexString:@"22B2A8"];
    
    self.stepperFilmHotPostage.tintColor = [UIColor colorFromHexString:@"22B2A8"];
    self.stepperFilmHotPrepay.tintColor = [UIColor colorFromHexString:@"22B2A8"];
    
    self.stepperKPlusPostage.tintColor = [UIColor colorFromHexString:@"22B2A8"];
    self.stepperKPlusPrepay.tintColor = [UIColor colorFromHexString:@"22B2A8"];
    
    self.stepperHDPostage.tintColor = [UIColor colorFromHexString:@"22B2A8"];
    self.stepperHDPrepay.tintColor = [UIColor colorFromHexString:@"22B2A8"];
    
    //Enabled
//    self.stepperBox.enabled = NO;
//    self.stepperPLC.enabled = NO;
//    self.stepperSTB.enabled = NO;
//    self.stepperPostage.enabled = NO;
    
    self.stepperVTVCabPostage.enabled = NO;
    self.stepperVTVCabPrepay.enabled = NO;
    
    self.stepperFilmPlusPostage.enabled = NO;
    self.stepperFilmPlusPrepay.enabled = NO;
    
    self.stepperFilmPlusPostage.enabled = NO;
    self.stepperFilmPlusPrepay.enabled = NO;
    
    self.stepperFilmHotPostage.enabled = NO;
    self.stepperFilmHotPrepay.enabled = NO;
    
    self.stepperKPlusPostage.enabled = NO;
    self.stepperKPlusPrepay.enabled = NO;
    
    self.stepperHDPostage.enabled = NO;
    self.stepperHDPrepay.enabled = NO;
    
}

- (void)configButton {
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    imgView.center = CGPointMake(self.btnCombo.frame.size.width - 25, self.btnCombo.frame.size.height/2);
    imgView.image = [UIImage imageNamed:@"ic_downarrow"];
    [self.btnCombo addSubview:imgView];
    
    UIImageView *imgView2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    imgView2.center = CGPointMake(self.btnDeploy.frame.size.width - 25, self.btnDeploy.frame.size.height/2);
    imgView2.image = [UIImage imageNamed:@"ic_downarrow"];
    [self.btnDeploy addSubview:imgView2];
    
    UIImageView *imgView3 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    imgView3.center = CGPointMake(self.btnService.frame.size.width - 25, self.btnService.frame.size.height/2);
    imgView3.image = [UIImage imageNamed:@"ic_downarrow"];
    [self.btnService addSubview:imgView3];
    
    UIImageView *imgView4 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    imgView4.center = CGPointMake(self.btnPromotion1.frame.size.width - 25, self.btnPromotion1.frame.size.height/2);
    imgView4.image = [UIImage imageNamed:@"ic_downarrow"];
    [self.btnPromotion1 addSubview:imgView4];
    
    UIImageView *imgView5 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    imgView5.center = CGPointMake(self.btnPromotion2.frame.size.width - 25, self.btnPromotion2.frame.size.height/2);
    imgView5.image = [UIImage imageNamed:@"ic_downarrow"];
    [self.btnPromotion2 addSubview:imgView5];
    
    self.txtvPromotion1.editable = NO;
    self.txtvPromotion2.editable = NO;
    
}

@end
