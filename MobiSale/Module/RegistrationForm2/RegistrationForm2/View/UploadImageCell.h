//
//  UploadImageCell.h
//  MobiSale
//
//  Created by StevenNguyen on 12/29/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol UploadImageCellDelegate <NSObject>

-(void) ButtonUploadImage;

@end

@interface UploadImageCell : UITableViewCell

@property (nonatomic, weak) id <UploadImageCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@end
