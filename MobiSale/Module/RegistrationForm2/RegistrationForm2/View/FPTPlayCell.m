//
//  FPTPlayCell.m
//  MobiSale
//
//  Created by StevenNguyen on 12/28/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "FPTPlayCell.h"
#import "RegistrationFormModel.h"

@implementation UIColor (HexString)
+ (UIColor *)colorFromHexString:(NSString *)hexString {
    
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1];
    [scanner scanHexInt:&rgbValue];
    
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
    
}

@end

@implementation FPTPlayCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self configStepper];
    
}

//MARK: -ACTION STEPPER
-(IBAction)pressedSteppedBox:(id)sender {
    
    int number = (int)self.stepperBox.value;
  
    [[RegistrationFormModel sharedInstance] setCountFPTPlay:number];
    
    
}

- (void)configFPTPlayBox:(NSString *)stringfptPlayBoxCount {
    
    double douBox = [stringfptPlayBoxCount doubleValue] ?:0.0;
    self.stepperBox.value = douBox;
    
}

- (void)configStepper {
    
    self.stepperBox.tintColor = [UIColor colorFromHexString:@"22B2A8"];
    self.stepperBox.minimumValue = 0.0;
    self.stepperBox.maximumValue = 100.0;
    
}

@end
