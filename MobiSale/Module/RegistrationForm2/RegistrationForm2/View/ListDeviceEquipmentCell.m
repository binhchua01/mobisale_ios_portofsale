//
//  ListDeviceEquipmentCell.m
//  MobiSale
//
//  Created by Tran Vu on 6/8/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//
#define MAXIMUM 100
#define MINIMUM 1
#define FONT_OPENSANS_LIGHT             @"OpenSans-Light"

#import "ListDeviceEquipmentCell.h"
#import "Common_client.h"
#import "ShareData.h"
#import "BaseViewController.h"
#import "UIButton+FPTCustom.h"
#import "RegistrationFormModel.h"
#import <MBProgressHUD/MBProgressHUD.h>

@implementation ListDeviceEquipmentCell{
    
     MBProgressHUD *HUD;
}
@synthesize delegate;

- (NSMutableArray*)arrayListPromotionDevice {
    
    if (!_arrayListPromotionDevice) {
        _arrayListPromotionDevice = [[NSMutableArray alloc] init];
    }
    return _arrayListPromotionDevice;
}

- (NSMutableArray*)arrayAddListDevice {
    
    if (!_arrayAddListDevice) {
        _arrayAddListDevice = [[NSMutableArray alloc] init];
    }
    return _arrayAddListDevice;
}
- (NSMutableArray*)listDevicePacketArray {
    
    if (!_listDevicePacketArray) {
        _listDevicePacketArray = [[NSMutableArray alloc] init];
    }
    return _listDevicePacketArray;
}

- (NSMutableArray*)arrayDataDevice    {
    
    if (!_arrayDataDevice) {
        _arrayDataDevice = [[NSMutableArray alloc] init];
    }
    return _arrayDataDevice;
}

- (NSMutableDictionary *)dictListDevice {
    if (!_dictListDevice) {
        _dictListDevice = [[NSMutableDictionary alloc] init];
    }
    return _dictListDevice;
}

- (NSMutableDictionary *)dictPromotionListDevice {
    if (!_dictPromotionListDevice) {
        _dictPromotionListDevice = [[NSMutableDictionary alloc] init];
    }
    return _dictPromotionListDevice;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
   
    self.count = 1;
    self.viewContent.layer.cornerRadius = 5;
    self.viewContent.layer.borderWidth = 1;
    self.viewContent.layer.borderColor = [UIColor colorWithRed:18.0f/255.0f green:193.0f/255.0f blue:164.0f/255.0f alpha:1.0].CGColor;
    
    [self.minusButton setStyle];
    [self.plusButton setStyle];
   
    self.countTextField.userInteractionEnabled = NO;
    if (_maximum <= 0) {
        _maximum = MAXIMUM;
    }
    if (_minimum <= 0) {
        _minimum = MINIMUM;
    }
}

- (void)setDataForCell:(NSMutableArray *)arrayListDevice andNumberRow:(int)numberRow{
    
    self.lblNameDevices.text = [arrayListDevice[numberRow] valueForKey:@"DeviceName"];
    
}

- (IBAction)btnPromotionListDevice_clicked:(id)sender {
    
   
                    [self getDevicePromotion:self.deviceID];
                    NSLog(@"%@",self.deviceID);
}

- (IBAction)btnminusButton_Cliked:(id)sender {
    
    [self changedValueCount:sender];
   // [self.btnPromotionListDevice setTitle:@"Vui lòng chọn CLKM" forState:UIControlStateNormal];
    
}
- (IBAction)btnPlustButton_clicked:(id)sender {
    
    [self changedValueCount:sender];
   // [self.btnPromotionListDevice setTitle:@"Vui lòng chọn CLKM" forState:UIControlStateNormal];
}

- (void)changedValueCount:(UIButton*)sender {
    
    self.count = [self.countTextField.text integerValue];
    if (sender == self.minusButton) {
        self.count -- ;
        if (self.count < self.minimum) {
            self.count = self.minimum;
        }
    }
    
    if (sender == self.plusButton) {
        self.count ++;
        if (self.count > self.maximum) {
            self.count = self.maximum;
        }
    }
    [self changeValueCountTextField:StringFormat(@"%li", (long)self.count)];
    
    // truong hop thai doi so luong
    self.listDevicePacketArray = [[RegistrationFormModel sharedInstance] listDevicePackagesArray];
    
    NSString *deviceID,*promotionDeviceID,*promotionDeviceText,*deviceName,*numBer;
    
    promotionDeviceID = StringFormat(@"%ld",(long)[self.arrayDevice valueForKey:@"PromotionID"] );
    promotionDeviceText = StringFormat(@"%@",[self.arrayDevice valueForKey:@"PromotionDeviceText"]);
    deviceID = StringFormat(@"%@",[self.arrayDevice valueForKey:@"DeviceID"]);
    deviceName = StringFormat(@"%@",[self.arrayDevice valueForKey:@"DeviceName"]);
    numBer = StringFormat(@"%ld",(long)self.count);
    
    for (int i = 0; i < self.listDevicePacketArray.count; i ++) {
        NSString *deviceID_2 ;
        deviceID_2 = StringFormat(@"%@",[self.listDevicePacketArray[i] valueForKey:@"DeviceID"]);
        if (deviceID == deviceID_2) {
            
            [self.listDevicePacketArray[i]setObject:numBer forKey:@"Number"];
            
        }
    }
}

- (void)changeValueCountTextField:(NSString *)text {
    [self.countTextField setText:text];
    [self.countTextField layoutIfNeeded];
    [[RegistrationFormModel sharedInstance] setNumber:text];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    
}
#pragma mark - UIPopoverListViewDataSource


- (void)setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell {
    
    cell.textLabel.numberOfLines = 0;
    [cell.textLabel sizeToFit];
    cell.textLabel.text = [[self.arrayListPromotionDevice objectAtIndex:row] valueForKey:@"PromotionName"];
}

- (void)getDevicePromotion :(NSString *)deviceID {
    
    //kiem tra ket noi Internet
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        
        return;
    }
    [[NSNotificationCenter defaultCenter]postNotificationName:@"ShowMBProcessListRegis" object:nil];
    
    ShareData *shared = [ShareData instance];
    [shared.appProxy GetDevicePromotion:deviceID Handler:^(id result,NSString *errorCode,NSString *message){
        
        //[self showHUDWithMessage:@"Đang lấy thông tin..."];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LogOutWhenEndSession" object:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"HideMBProcessListRegis" object:nil];
            
            return ;
        }
        if ([message isEqualToString:@"Error Service: Unauthorized"]) {
            NSLog(@"%@",message);
            [[NSNotificationCenter defaultCenter] postNotificationName:@"HideMBProcessListRegis" object:nil];
             // [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"LogOutWhenEndSession" object:nil];
            
            return;
        }
        
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
           [[NSNotificationCenter defaultCenter] postNotificationName:@"HideMBProcessListRegis" object:nil];
            return ;
        }
        
        NSArray *arr = result;
         // [self hideHUD];
         [[NSNotificationCenter defaultCenter] postNotificationName:@"HideMBProcessListRegis" object:nil];
        if (arr.count <= 0) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"HideMBProcessListRegis" object:nil];
            return;
            
        } else {
            
            self.arrayListPromotionDevice = [NSMutableArray arrayWithArray:arr];
            
            [self setupDropDownView];
           [[NSNotificationCenter defaultCenter] postNotificationName:@"HideMBProcessListRegis" object:nil];
            
            //vutt test
            NSString *promotionID,*promotionname;
            promotionID = StringFormat(@"%@",[self.arrayListPromotionDevice valueForKey:@"PromotionID"]);
            promotionname = StringFormat(@"%@",[self.arrayListPromotionDevice valueForKey:@"Promotionname"]);
            
            [[RegistrationFormModel sharedInstance]setupPromotionListDevices:promotionID PromotionName:promotionname DeviceID:deviceID];
            
}
        
    } errorHandler:^(NSError *error){
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"HideMBProcessListRegis" object:nil];
        
    }];
    
}

//config UiPoplistView
- (void)setupDropDownView {
    
    [self.viewContent endEditing:YES];
    CGFloat xWight = self.viewContent.bounds.size.width - 20.0f;
    CGFloat yHight = 272.0f;
    CGFloat yOffset = (self.viewContent.bounds.size.height - yHight)/2.0f;
    UIPopoverListView *poplistView = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWight, yHight)];
    poplistView.delegate = (id)self;
    poplistView.datasource = (id)self;
    poplistView.listView.scrollEnabled = TRUE;
    poplistView.userInteractionEnabled = TRUE;
    [poplistView setTitle:NSLocalizedString(@"Câu lệnh khuyến mãi của thiết bị", @"")];
    [poplistView show];
}
- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
#pragma mark - UIPopoverListViewDataSoure

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView cellForIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row cell:cell];
    cell.textLabel.font = [UIFont fontWithName:@"Arial" size:14];
    
    return cell;
    
}
- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView numberOfRowsInSection:(NSInteger)section {
    return self.arrayListPromotionDevice.count;
}


#pragma mark - UIPopoverListViewDelegate

- (void) didDropDownSelected:(NSInteger)row tag:(NSInteger)tag {
    
    [self.btnPromotionListDevice setTitle:[NSString stringWithFormat:@"%@",[self.arrayListPromotionDevice[row] valueForKey:@"PromotionName"]] forState:UIControlStateNormal];
    
    NSMutableDictionary *dict;
    dict = [[NSMutableDictionary alloc] init];
    
    NSString *promotionDeviceID = StringFormat(@"%@",[self.arrayListPromotionDevice[row] valueForKey:@"PromotionID"] );
    
    NSString *promotionDeviceText = StringFormat(@"%@",[self.arrayListPromotionDevice[row] valueForKey:@"PromotionName"]);
    
    NSString *deviceID = StringFormat(@"%@",[self.arrayDevice valueForKey:@"DeviceID"]);
    NSString *deviceName = StringFormat(@"%@",[self.arrayDevice valueForKey:@"DeviceName"]);
    NSString *numBer = StringFormat(@"%ld", (long)self.count);
    
    
    // init Dict
    [dict setValue:numBer forKey:@"Number"];
    [dict setValue:deviceID forKey:@"DeviceID"];
    [dict setValue:deviceName forKey:@"DeviceName"];
    [dict setValue:promotionDeviceID forKey:@"PromotionDeviceID"];
    [dict setValue:promotionDeviceText forKey:@"PromotionDeviceText"];
    
    // kiem tra xem trong mang cua sharedintance da co Device chon trung device dang chon khong  neu co remove  cai device cu va add cai device moi vao
    
    self.listDevicePacketArray = [[RegistrationFormModel sharedInstance] listDevicePackagesArray];
    
    for (int i = 0; i < self.listDevicePacketArray.count; i ++) {
        NSString *deviceID_2 ;
        deviceID_2 = StringFormat(@"%@",[self.listDevicePacketArray[i] valueForKey:@"DeviceID"]);
        if (deviceID == deviceID_2) {
            
            [self.listDevicePacketArray[i]setObject:promotionDeviceText forKey:@"PromotionDeviceText"];
        }
    }
}

#pragma mark - UIPopoverListViewDelegate

- (void)popoverListView:(UIPopoverListView *)popoverListView didSelectIndexPath:(NSIndexPath *)indexPath {
    [self didDropDownSelected:indexPath.row tag:popoverListView.tag];
    [popoverListView dismiss];
    
}
#pragma mark process bar
- (void)showHUDWithMessage:(NSString *)message
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.viewContent animated:YES];
    hud.labelText = message;
    hud.labelFont = [UIFont fontWithName:FONT_OPENSANS_LIGHT size:15];
}

- (void)hideHUD
{
    [MBProgressHUD hideHUDForView:self.viewContent animated:YES];
}

@end
