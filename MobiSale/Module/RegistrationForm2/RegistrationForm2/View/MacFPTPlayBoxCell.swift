//
//  MacFPTPlayBoxCell.swift
//  MobiSale
//
//  Created by Nguyen Sang on 1/24/18.
//  Copyright © 2018 FPT.RAD.FTool. All rights reserved.
//

import UIKit

protocol MacFPTPlayBoxCellDelegate {
    func deletedMACFPTPlayBox(index:IndexPath)
}

class MacFPTPlayBoxCell: UITableViewCell {
    
    @IBOutlet weak var viewBorder:UIView!
    
    @IBOutlet weak var lblMac:UILabel!
    
    @IBOutlet weak var btnDelete:UIButton!
    
    var delegate:MacFPTPlayBoxCellDelegate!
    
    var indexPath:IndexPath!
    
    @IBAction func btnDelete_clicked(sender:UIButton) {
        
        self.delegate.deletedMACFPTPlayBox(index: indexPath)
        
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        configCell()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension MacFPTPlayBoxCell {
    
    func configCell(strMac:NSDictionary) {
        
        let strDicMac = (strMac["MAC"] as? String) ?? ""
        self.lblMac.text = "MAC: \(strDicMac)"
        
    }
    
    func configCell() {
        
        self.viewBorder.layer.cornerRadius = 5
        self.viewBorder.layer.borderWidth = 1
        self.viewBorder.layer.borderColor = UIColor(red: 18.0/255.0, green: 193.0/255.0, blue: 164.0/255.0, alpha: 1.0).cgColor
        
        self.btnDelete.setStyle()
        
    }
    
}
