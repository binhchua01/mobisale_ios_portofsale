//
//  FPTPlayNewCell.swift
//  MobiSale
//
//  Created by Nguyen Sang on 1/16/18.
//  Copyright © 2018 FPT.RAD.FTool. All rights reserved.
//

import UIKit

enum FPTPlayBoxOTT:String {
    case id             = "OTTID"
    case name           = "OTTName"
    case price          = "OTTPrice"
    case number         = "OTTCount"
    case promotionID    = "PromotionID"
    case textPromotion  = "PromotionText"
}

enum PromotionFPTPlayBoxOTT:String {
    case id             = "PromotionID"
    case name           = "PromotionName"
    case price          = "PromotionPrice"
}

@objc protocol FPTPlayNewCellDelegate: class {
    func showListOTTViewController()
    func scanFromScanViewController()
    func changeValueFPTPlayOTT(dic:[NSDictionary])
    func changeMacFPTPlayOTT(index:Int)
}

public class FPTPlayNewCell: UITableViewCell {
    
    @IBOutlet weak var myTableView:UITableView!
    
    @IBOutlet weak var macCodeTableView:UITableView!
    
    @IBOutlet weak var btnChooseDevice:UIButton!
    
    @IBOutlet weak var btnScanDevice:UIButton!
    
    @IBOutlet weak var heightContentView: NSLayoutConstraint!
    
    @IBOutlet weak var heightTableDevice: NSLayoutConstraint!
    
    @IBOutlet weak var heightTableScan: NSLayoutConstraint!
    
    var delegate:FPTPlayNewCellDelegate?
    
    var indexPath:IndexPath!

    var arrTableView:[NSDictionary] = [] {
        didSet {
            self.myTableView.reloadData()
            self.setupHeight()
        }
    }
    
    var arrMacCode:[NSDictionary] = [] {
        didSet {
            self.macCodeTableView.reloadData()
            self.setupHeight()
        }
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        
        self.btnChooseDevice.isSelected = true
        self.btnScanDevice.isSelected = true
        
        self.setupTableView()
        self.setupHeight()
        
    }

    override public func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension FPTPlayNewCell {
    
    @IBAction func pressedChooseDevice(sender:UIButton) {
        
        self.delegate?.showListOTTViewController()
        
    }
    
    @IBAction func pressedScan(sender:UIButton) {
        
        self.delegate?.scanFromScanViewController()
        
    }
    
}

extension FPTPlayNewCell {
    
    func configCell(dic:[NSDictionary], indexPath:IndexPath) {
        
        self.arrTableView = dic
        self.indexPath = indexPath
        
        self.converDictToRequest()
        
    }
    
    func setupTableView() {
        
        macCodeTableView.delegate = self
        macCodeTableView.dataSource = self
        macCodeTableView.isScrollEnabled = false
        let nibBox = UINib(nibName: "MacFPTPlayBoxCell", bundle: nil)
        macCodeTableView.register(nibBox, forCellReuseIdentifier: "MacFPTPlayBoxCell")
        
        
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.isScrollEnabled = false
        let nib = UINib(nibName: "FPTPlayOTTCell", bundle: nil)
        myTableView.register(nib, forCellReuseIdentifier: "FPTPlayOTTCell")
        
        
    }
    
    func setupHeight() {
        
        let headerView = 90
        let btnScan = 30
        let tableView = 140 * arrTableView.count
        let macTableView = 52 * arrMacCode.count
        
        self.heightContentView.constant = CGFloat(headerView + tableView + btnScan + macTableView + 44)
        self.heightTableDevice.constant = CGFloat(tableView)
        self.heightTableScan.constant = CGFloat(macTableView)
        
        
    }
    
    func converDictToRequest() {
        
        let jsonMAC = arrMacCode
        let jsonOTT = arrTableView
        
        var dic:NSMutableDictionary = [:]
        
        dic["listDeviceOTT"] = arrTableView
        
        var arrDicMAC:[NSMutableDictionary] = []
        for mac in jsonMAC {
//            ["MAC":"FC:D5:D9:14:FC:6D"]
            let strMac = (mac["MAC"] as? String) ?? ""
            let dicMac:NSMutableDictionary = NSMutableDictionary(dictionary: ["MAC":strMac])
            arrDicMAC.append(dicMac)
        }
        
        dic["listMACOTT"] = arrDicMAC
        
        print(dic)
        
        RegistrationFormModel.sharedInstance().listFPTPlayBoxOTT = dic
        
    }
    
}

extension FPTPlayNewCell:UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == myTableView {
            return arrTableView.count
        } else {
            return arrMacCode.count
        }
        
        
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == myTableView {
            return 140
        } else {
            return 52
        }
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == myTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FPTPlayOTTCell", for: indexPath) as? FPTPlayOTTCell
            
            cell?.delegate = self
            cell?.configCell(dic: arrTableView[indexPath.row])
            
            return cell!
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MacFPTPlayBoxCell", for: indexPath) as? MacFPTPlayBoxCell
            
            cell?.delegate = self
            cell?.indexPath = indexPath
            cell?.configCell(strMac: arrMacCode[indexPath.row])
            
            return cell!
            
        }
        
    }
    
    public func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if tableView == myTableView {
            return false
        } else {
            return true
        }
    }
    
    public func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            self.arrMacCode.remove(at: indexPath.row)
        }
        
    }
    
}

extension FPTPlayNewCell:FPTPlayOTTCellDelegate {
    
    func changeValueFPTPlayOTT(dic: NSDictionary) {
        
        let dicJSONChange = self.arrTableView.filter { (dicJSON) -> Bool in
            let idDeviceOne = (dicJSON.value(forKey: FPTPlayBoxOTT.id.rawValue) as? Int) ?? 0
            let idDeviceTwo = (dic.value(forKey: FPTPlayBoxOTT.id.rawValue) as? Int) ?? 0
            return idDeviceOne == idDeviceTwo
        }
        
        if dicJSONChange.count > 0 {
            if let index = self.arrTableView.index(of: dicJSONChange.first ?? [:]) {
//                let idDeviceOne = (self.arrTableView[(index?.hashValue) ?? 0].value(forKey: "OTTID") as? Int) ?? 0
//                let idDeviceTwo = (dic.value(forKey: "OTTID") as? Int) ?? 0
//                if idDeviceOne == idDeviceTwo {
                    self.arrTableView[index.hashValue] = dic
//                }
            }
        }
        
        self.converDictToRequest()
        self.delegate?.changeValueFPTPlayOTT(dic: self.arrTableView)
        
    }
    
}

extension FPTPlayNewCell:MacFPTPlayBoxCellDelegate {
    
    func deletedMACFPTPlayBox(index: IndexPath) {
        
        self.arrMacCode.remove(at: index.row)
        self.delegate?.changeMacFPTPlayOTT(index: index.row)
        self.converDictToRequest()
        
    }
    
}











