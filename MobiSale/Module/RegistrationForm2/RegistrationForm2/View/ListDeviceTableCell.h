//
//  ListDeviceTableCell.h
//  MobiSale
//
//  Created by Tran Vu on 6/20/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListDeviceEquipmentCell.h"
#import "StyleOfServiceViewController.h"


@protocol ListDeviceTableCellDelegate <NSObject>

- (void)getArrayPromotion2:(NSMutableArray*)arrayPromotion2 ArrayDevice:(NSMutableArray *)arrayDevice2;
- (void)getNumberCount:(NSString *)stringNumberCount;


@end


@interface ListDeviceTableCell : UITableViewCell <UITableViewDelegate, UITableViewDataSource,ListDeviceEquipmentCellDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightTableView;


@property (weak, nonatomic) IBOutlet UITableView *tableViewInfo;
@property (strong, nonatomic) NSMutableArray *arrayData;
// Array cau lenh khuyen mai cua thiet bi
@property (retain, nonatomic) NSMutableArray *arrayDevice;
//Array add listdevice when Update
@property (retain, nonatomic) NSMutableArray *arrayUpdateDeviceAdd;
@property (weak, nonatomic) id<ListDeviceTableCellDelegate>delegate;
@property (nonatomic)BOOL ischeckLoad;
@property (nonatomic)BOOL ischeckNameDevice;
@property (nonatomic)BOOL isCheckAtUpdateAddListDevice;
@property (nonatomic)BOOL isCheckShowUpdateListDevice;

- (void)configCell:(NSString *)number PromotionName:(NSString *)promotionName;
@property (strong,nonatomic) NSString *number;
@property (strong,nonatomic) NSString *promotionName;
@property (strong,nonatomic) NSString *nameDevice;
@property (strong, nonatomic) NSString *Id;
@property (strong, nonatomic) NSMutableArray *arrayListDevice;

-(void) reloadTableViewCell;

@end
