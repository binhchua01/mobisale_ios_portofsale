//
//  AniApartmentCell.h
//  MobiSale
//
//  Created by Bored Ninjas on 11/30/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AniApartmentCellDelegate <NSObject>

-(void) valueTextfield: (NSString *)value kTag:(NSInteger)tag tagTextField:(NSInteger)tagTextField;

@end

@interface AniApartmentCell : UITableViewCell

//delegate
@property (nonatomic, weak) id <AniApartmentCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *lblLot;
@property (weak, nonatomic) IBOutlet UILabel *lblFloor;
@property (weak, nonatomic) IBOutlet UILabel *lblRoom;

@property (weak, nonatomic) IBOutlet UITextField *txtfLot;
@property (weak, nonatomic) IBOutlet UITextField *txtfFloor;
@property (weak, nonatomic) IBOutlet UITextField *txtfRoom;

- (void) configCell:(NSString *)strLot placeHolderLot:(NSString *)strPlaceLot titleFloor:(NSString *)strFloor placeHolderFloor:(NSString *)strPlaceFloor titleRoom:(NSString *)strRoom placeHolderRoom:(NSString *)strPlaceRoom;

- (void) configCellWhenSetText:(NSString *)strLot Floor:(NSString *)strFloor Room:(NSString *)strRoom;

- (void) configCellWithTag:(NSInteger)tag;

@end
