//
//  DeviceEquipmentCell.h
//  MobiSale
//
//  Created by Tran Vu on 6/8/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol DeviceEquipmentCellDelegate <NSObject>

- (void) showListDevicesViewController;
- (void) showListDevicesViewController2;

@end

@interface DeviceEquipmentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contraintBotton;

@property (weak, nonatomic) IBOutlet UILabel *lblListDevies;

@property (weak, nonatomic) IBOutlet UIButton *btnListDevies;


@property(nonatomic,weak) id <DeviceEquipmentCellDelegate> delegate;

- (void)configDropDowCell :(NSString *)strTitile PlaceTitle:(NSString *)placeTitle ;

@end
