//
//  ButtonTitleCell.m
//  MobiSale
//
//  Created by Bored Ninjas on 12/14/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ButtonTitleCell.h"

@implementation ButtonTitleCell {
    
    NSInteger kTag;
    
}

@synthesize delegate;



-(IBAction)pressedAction:(id)sender {
    
    [delegate dropDownButton:sender kTag:kTag];
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    kTag = -1;
}

- (void)configCell:(NSString *)strTitle ktag:(NSInteger)tag {
    
    [self.btnChoose setTitle:strTitle forState:UIControlStateNormal];
    kTag = tag;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
