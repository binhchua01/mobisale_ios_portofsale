//
//  TypeOfServiceCell.h
//  MobiSale
//
//  Created by Bored Ninjas on 12/7/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TypeOfServiceCellDelegate <NSObject>

-(void) dropDownButton: (id)sender kTag:(NSInteger)tag tagButton:(NSInteger)tagButton;

-(void) freshCompleted:(NSInteger)tag;

@end

@interface TypeOfServiceCell : UITableViewCell

@property (nonatomic, weak) id <TypeOfServiceCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UILabel *lblTypeOfService;

@property (weak, nonatomic) IBOutlet UILabel *lblPackageService;

@property (weak, nonatomic) IBOutlet UIButton *btnTypeOfService;

@property (weak, nonatomic) IBOutlet UIButton *btnPackageService;

@property (weak, nonatomic) IBOutlet UIButton *btnRefresh;

-(void)configCell:(NSString *)strService titlePromotion:(NSString *)strPackage ktag:(NSInteger)tag;

-(void)configCellTitleButton:(NSMutableArray *)arrLocalType titlePackage:(NSString *)titlePackage;

@end
