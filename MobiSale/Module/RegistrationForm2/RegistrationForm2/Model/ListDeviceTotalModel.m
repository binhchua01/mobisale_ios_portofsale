//
//  ListDeviceTotalModel.m
//  MobiSale
//
//  Created by Tran Vu on 6/21/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import "ListDeviceTotalModel.h"

@implementation ListDeviceTotalModel
@synthesize DeviceID,DeviceName,PromotionDeviceID,PromotionDeviceText,Number;

- (id)initWithName:(NSString *)deviceName DeViceID:(NSString *)deviceID Number:(NSString *)number PromotionDeviceID:(NSString *)promotionDeviceID PromotionDeviceText:(NSString *)promotionDeviceText {
    
    self.DeviceID = deviceID;
    self.DeviceName = deviceName;
    self.PromotionDeviceID = promotionDeviceID;
    self.PromotionDeviceText = promotionDeviceText;
    
    return  self;
}

@end
