//
//  ListDeviceModel.h
//  MobiSale
//
//  Created by Tran Vu on 6/19/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ListDeviceModel : NSObject

@property (nonatomic, copy) NSString *DeviceID;
@property (nonatomic, copy) NSString *DeviceName;
@property (nonatomic, copy) NSString *DevicePrice;

- (id)initWithName:(NSString *)deviceName DeViceID:(NSString *)deviceID DevicePrice:(NSString *)devicePrice;


@end
