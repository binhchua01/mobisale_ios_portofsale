//
//  ReputationView.h
//  MobiSale
//
//  Created by Bored Ninjas on 11/18/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ReputationView : NSObject

@property (nonatomic, retain) NSString *title;
@property (nonatomic, strong) UIViewController *viewController;

@end
