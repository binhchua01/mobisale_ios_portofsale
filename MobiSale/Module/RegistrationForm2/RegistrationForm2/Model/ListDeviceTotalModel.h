//
//  ListDeviceTotalModel.h
//  MobiSale
//
//  Created by Tran Vu on 6/21/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ListDeviceTotalModel : NSObject

@property (nonatomic, copy) NSString *DeviceID;
@property (nonatomic, copy) NSString *DeviceName;
@property (nonatomic, copy) NSString *Number;
@property (nonatomic, copy)  NSString *PromotionDeviceID;
@property (nonatomic, copy ) NSString *PromotionDeviceText;

- (id)initWithName:(NSString *)deviceName DeViceID:(NSString *)deviceID Number:(NSString *)number PromotionDeviceID:(NSString *)promotionDeviceID PromotionDeviceText:(NSString *)promotionDeviceText;

@end
