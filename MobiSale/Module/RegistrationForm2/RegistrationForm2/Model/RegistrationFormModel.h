//
//  RegistrationFormModel.h
//  demoRegister
//
//  Created by Bored Ninjas on 11/25/16.
//  Copyright © 2016 com.StevenNguyen.DemoCreateRegisterForm. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeyValueModel.h"
#import "PromotionModel.h"
#import "IPTVPrice.h"

@interface RegistrationFormModel : NSObject

///loại hình
@property (retain, nonatomic) KeyValueModel *type;

///loại khách hàng
@property (retain, nonatomic) KeyValueModel *typeCustomer;

///Họ và tên
@property (retain, nonatomic) NSString *name;

///CMND
@property (retain, nonatomic) NSString *isCard;

///Ngày sinh
@property (retain, nonatomic) NSString *date;

///Ngay sinh dung so sanh
@property (retain, nonatomic) NSDate *dateBirthday;

///địa chỉ trên cmnd
@property (retain, nonatomic) NSString *address;

///mã số thuế
@property (retain, nonatomic) NSString *taxCode;

///di động 1
@property (retain, nonatomic) KeyValueModel *typePhone1;
@property (retain, nonatomic) NSString *numberPhone1;
@property (retain, nonatomic) NSString *contact1;

///di động 2
@property (retain, nonatomic) KeyValueModel *typePhone2;
@property (retain, nonatomic) NSString *numberPhone2;
@property (retain, nonatomic) NSString *contact2;

///email
@property (retain, nonatomic) NSString *email;

///Quận
@property (retain, nonatomic) KeyValueModel *county;

///Huyện
@property (retain, nonatomic) KeyValueModel *ward;

///loại nhà
@property (retain, nonatomic) KeyValueModel *typeOfHouse;

///đường
@property (retain, nonatomic) KeyValueModel *street;

///vị trí nhà
@property (retain, nonatomic) KeyValueModel *housePosition;

///lô
@property (retain, nonatomic) NSString *lot;

///tầng
@property (retain, nonatomic) NSString *floor;

///phòng
@property (retain, nonatomic) NSString *room;

///tên chung cư
@property (strong, nonatomic) KeyValueModel *nameAparment;

///số nhà
@property (retain, nonatomic) NSString *numberOfHouse;

///ghi chú đia chỉ
@property (retain, nonatomic) NSString *noteAddress;

///ghi chú kế hoạch
@property (retain, nonatomic) NSString *notePlant;

/****************************************************************************/

///đối tượng khách hàng
@property (retain, nonatomic) KeyValueModel *objectCustomer;

///isp
@property (retain, nonatomic) KeyValueModel *isp;

///nơi ở hiện tại
@property (retain, nonatomic) KeyValueModel *addressNow;

///khả năng thanh toán
@property (retain, nonatomic) KeyValueModel *solvency;

///khách hàng của đối tác
@property (retain, nonatomic) KeyValueModel *partnerCustomer;

///pháp nhân
@property (retain, nonatomic) KeyValueModel *entity;

///tình trạng truyền hình cáp...
@property (retain, nonatomic) NSString *cableStatus;

///Nhu cầu lắp đặt truyền hình...
@property (retain, nonatomic) NSString *isIPTV;

///chọn gói dịch vụ (Internet, IPTV, Office365, Device)
@property (retain, nonatomic) NSMutableArray *localType;

/******************************* Internet *********************************************/

///gói dịch vụ
@property (retain, nonatomic) KeyValueModel *internetService;

///khuyến mãi
@property (retain, nonatomic) PromotionModel *internetPromotion;

///office 365
@property (retain, nonatomic) KeyValueModel *registedOffice;

/******************************* IPTV *********************************************/

///Yêu cầu cài đặt
@property (retain, nonatomic) NSString *iptvRequest;

///Yêu cầu khoan tường
@property (retain, nonatomic) NSString *iptvWallDrilling;

///Gói dịch vụ...
@property (retain, nonatomic) KeyValueModel *iptvServiceBase;

///Hình thức triển khai
@property (retain, nonatomic) KeyValueModel *iptvDeployment;

///Gói dịch vụ
@property (retain, nonatomic) KeyValueModel *iptvPackageIPTV;

///khuyến mãi box đầu tiên
@property (retain, nonatomic) PromotionModel *iptvPromotionIPTV;

///khuyến mãi box hai trở đi
@property (retain, nonatomic) PromotionModel *iptvPromotionIPTV2;

///Đăng ký combo Khi đăng ký 2 gói cước internet va iptv
@property (retain, nonatomic) KeyValueModel *iptvCombo;

///Thiết bị số lượng Box
@property (retain, nonatomic) NSString *iptvDeviceBoxCount;

///Thiết bị số lượng PLC
@property (retain, nonatomic) NSString *iptvDevicePLCCount;

///Thiết bị số STB thu hồi
@property (retain, nonatomic) NSString *iptvDeviceSTBCount;

///Số lần tính cước
@property (retain, nonatomic) NSString *iptvChargeTimes;

///Film+ số lần tính cước
@property (retain, nonatomic) NSString *iptvFilmPlusChargeTime;

///Film+ số tháng trả trước
@property (retain, nonatomic) NSString *iptvFilmPlusPrepaidMonth;

///Film Hot số lần tính cước
@property (retain, nonatomic) NSString *iptvFilmHotChargeTime;

///Film Hot số tháng trả trước
@property (retain, nonatomic) NSString *iptvFilmHotPrepaidMonth;

///Film K+ số lần tính cước
@property (retain, nonatomic) NSString *iptvFilmKPlusChargeTime;

///Film K+ số tháng trả trước
@property (retain, nonatomic) NSString *iptvFilmKPlusPrepaidMonth;

///Film Đặc sắc HD VTC số lần tính cước
@property (retain, nonatomic) NSString *iptvFilmVTCChargeTime;

///Film Đặc sắc HD VTC số tháng trả trước
@property (retain, nonatomic) NSString *iptvFilmVTCPrepaidMonth;

///Film VTV Cab số lần tính cước
@property (retain, nonatomic) NSString *iptvFilmVTVChargeTime;

///Film VTV Cab số tháng trả trước
@property (retain, nonatomic) NSString *iptvFilmVTVPrepaidMonth;

///Giá Film+
@property (retain, nonatomic) NSString *iptvPriceFilmPlus;

///Giá Film Hot
@property (retain, nonatomic) NSString *iptvPriceFilmHot;

///Giá K+
@property (retain, nonatomic) NSString *iptvPriceFilmKPlus;

///Giá HD
@property (retain, nonatomic) NSString *iptvPriceFilmHD;

///Giá VTV Cab
@property (retain, nonatomic) NSString *iptvPriceFilmVTV;

///Cau lenh khuyen mai IPTV Box 2
@property (retain, nonatomic) NSString *promotionIPTVBox2;


/******************************* FPT Play *********************************************/

///Số BOX FPTPlay
@property (nonatomic) NSInteger countFPTPlay;

//FPT Play Box OTT
@property (strong, nonatomic) NSMutableDictionary *listFPTPlayBoxOTT;

///Tổng cộng FPTPlay
@property (nonatomic) int totalFPTPlay;

/******************************* Upload Image *********************************************/

///UIImage
@property (retain, nonatomic) UIImage *imageUpload;

///Thông tin image
@property (retain, nonatomic) NSString *imgInfo;


/******************************* Thanh Toán *********************************************/


///Đặt cọc điểm đen
@property (retain, nonatomic) KeyValueModel *depositBlackPoint;

///Đặt cọc thue bao
@property (retain, nonatomic) KeyValueModel *deposit;

///hình thức thanh toán
@property (retain, nonatomic) KeyValueModel *paymentType;

///Tổng giá internet
@property (nonatomic) int totalInternet;

///Tổng cộng IPTV
@property (nonatomic) int totalIPTV;

///Tổng cộng Office 365
@property (nonatomic) NSInteger totalOffice365;

///GetTotalDevice
@property (nonatomic, strong) NSString *totalDevice;

///Tổng cộng
@property (nonatomic) int totalAmount;

///Indoor
@property (retain, nonatomic) NSString *indoor;

///Outdoor
@property (retain, nonatomic) NSString *outdoor;

///RegCode
@property (retain, nonatomic) NSString *regCode;

///ArraySaveLocalCell
@property (retain, nonatomic) NSMutableArray *arraySaveLocalCell;

///NSMutableDictionari ArrayListdevice
@property (retain ,nonatomic)NSDictionary *dictionaryArrayListdevice;

@property (retain ,nonatomic)NSString *number;
@property (retain ,nonatomic)NSString *promotionName;
//vutt11
@property (retain ,nonatomic)NSMutableArray *arrayLocalCell;
@property (retain, nonatomic)NSMutableArray *arrayListDevices;
@property (retain, nonatomic)NSMutableArray *arrayListDevicesModel;
@property (retain, nonatomic)NSMutableArray *arraySaveWhenUpdate;
//Array save ListDevice da chon khi update
@property (retain, nonatomic)NSMutableArray *arrayListDevicesUpdate;
//Array selected of Saleadd
@property (retain, nonatomic)NSMutableArray *arrayListSelectedAddSale;
//Array
@property (retain, nonatomic)NSMutableArray *arrayListDeviceAdd;
//Array PromotionListDevice
@property (retain, nonatomic)NSMutableArray *listPromotionDevice;
//DictionaryPromotionDevice
@property (retain, nonatomic)NSMutableDictionary *dictPromotionListDevice;

//set uo property of ListPacketDevice
@property (nonatomic, copy) NSString *DeviceID;
@property (nonatomic, copy) NSString *DeviceName;
@property (nonatomic, copy) NSString *Number;
@property (nonatomic, copy)  NSString *PromotionDeviceID;
@property (nonatomic, copy ) NSString *PromotionDeviceText;

@property (strong, nonatomic) NSMutableArray *listDevicePackagesArray;


- (NSMutableDictionary *)setParamater:(NSString *)deviceID DeviceName:(NSString *)deviceName PromotionDeviceID:(NSString *)promotionDeviceID PromotionDeviceText:(NSString *)promotionDeviceText Number:(NSString *)number;

+ (RegistrationFormModel *)sharedInstance;

+ (void) selfDestruct;

//
- (void)setupPromotionListDevices:(NSString *)promotionID PromotionName:(NSString*)promotionName DeviceID:(NSString *)device ;


@end
