//
//  RegistrationFormModel.m
//  demoRegister
//
//  Created by Bored Ninjas on 11/25/16.
//  Copyright © 2016 com.StevenNguyen.DemoCreateRegisterForm. All rights reserved.
//

#import "RegistrationFormModel.h"
@implementation RegistrationFormModel {
    
}

static RegistrationFormModel *sharedInstance = nil;

+ (RegistrationFormModel *)sharedInstance {
    
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [[RegistrationFormModel alloc] init];
            sharedInstance.listDevicePackagesArray = [NSMutableArray array];
            sharedInstance.listPromotionDevice = [NSMutableArray array];
        }
    }
    return sharedInstance;
}

+ (void) selfDestruct {

    sharedInstance.imageUpload = nil;
    sharedInstance = nil;
    
}

- (NSMutableDictionary *)setParamater:(NSString *)deviceID DeviceName:(NSString *)deviceName PromotionDeviceID:(NSString *)promotionDeviceID PromotionDeviceText:(NSString *)promotionDeviceText Number:(NSString *)number
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    if (self) {
        [dict setObject:deviceID      ?:@"" forKey:@"DeviceID"];
        [dict setObject:deviceName      ?:@"" forKey:@"DeviceName"];
        [dict setObject:promotionDeviceID        ?:@"" forKey:@"PromotionDeviceID"];
        [dict setObject:promotionDeviceText ?:@"" forKey:@"PromotionDeviceText"];
        [dict setObject:number       ?:@"" forKey:@"Number"];
    
    }
    
     [self.listDevicePackagesArray addObject:dict];
    
    return dict;
    
}

- (void)setupPromotionListDevices:(NSString *)promotionID PromotionName:(NSString *)promotionName DeviceID:(NSString *)device {
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    if (self) {
        [dict setObject:promotionID forKey:@"PromotionID"];
        [dict setObject:promotionName forKey:@"PromotionName"];
        [dict setObject:device forKey:@"DeviceID"];
    }
    [self.listPromotionDevice addObject:dict];
}

@end
