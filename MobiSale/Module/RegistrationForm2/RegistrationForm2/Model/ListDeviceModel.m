//
//  ListDeviceModel.m
//  MobiSale
//
//  Created by Tran Vu on 6/19/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import "ListDeviceModel.h"

@implementation ListDeviceModel
@synthesize DeviceID,DeviceName,DevicePrice;

-(id)initWithName:(NSString *)deviceName DeViceID:(NSString *)deviceID DevicePrice:(NSString *)devicePrice {
    
    self.DeviceID = deviceID;
    self.DeviceName = deviceName;
    self.DevicePrice = devicePrice;
    
    return self;
}

@end
