//
//  ScanBarCodeViewController.swift
//  MobiSale
//
//  Created by Nguyen Sang on 1/16/18.
//  Copyright © 2018 FPT.RAD.FTool. All rights reserved.
//

import UIKit
import AVFoundation
import SafariServices

class ScanBarCodeViewController: UIViewController {
    
    @IBOutlet weak var viewPreview: PreviewView!
    @IBOutlet weak var viewCenter: UIView!
    
    @IBOutlet weak var lblString: UILabel!
    
    var session: AVCaptureSession!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer!
    var videoDeviceInput: AVCaptureDeviceInput!
    
    let metadataOutput = AVCaptureMetadataOutput()
    let metadataObjectsQueue = DispatchQueue(label: "metadata objects queue", attributes: [], target: nil)
    let sessionQueue = DispatchQueue(label: "session queue")
    
    var removeMetadataObjectOverlayLayersTimer: Timer?
    
    class MetadataObjectLayer: CAShapeLayer {
        var metadataObject: AVMetadataObject?
    }
    
    /**
     A dispatch semaphore is used for drawing metadata object overlays so that
     only one group of metadata object overlays is drawn at a time.
     */
    let metadataObjectsOverlayLayersDrawingSemaphore = DispatchSemaphore(value: 1)
    
    var metadataObjectOverlayLayers = [MetadataObjectLayer]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        self.setupData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //        self.session.startRunning()
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if #available(iOS 8.0, *) {
            super.viewWillTransition(to: size, with: coordinator)
        } else {
            // Fallback on earlier versions
        }
        
        if let videoPreviewLayerConnection = viewPreview.videoPreviewLayer.connection {
            let deviceOrientation = UIDevice.current.orientation
            guard let newVideoOrientation = AVCaptureVideoOrientation(rawValue: deviceOrientation.rawValue),
                deviceOrientation.isPortrait || deviceOrientation.isLandscape else {
                    return
            }
            
            videoPreviewLayerConnection.videoOrientation = newVideoOrientation
            
            /*
             When we transition to a new size, we need to recalculate the preview
             view's region of interest rect so that it stays in the same
             position relative to the camera.
             */
            coordinator.animate(alongsideTransition: { context in
                
                let newRegionOfInterest = self.viewPreview.videoPreviewLayer.metadataOutputRectOfInterest(for: self.metadataOutput.rectOfInterest)
                self.viewPreview.setRegionOfInterestWithProposedRegionOfInterest(newRegionOfInterest)
            },completion: { context in
                                    
                                    // Remove the old metadata object overlays.
//                                    self.removeMetadataObjectOverlayLayers()
            })
        }
    }
    
}

//MARK: CYCLE LIFE
extension ScanBarCodeViewController {
    
    func setupView() {
        
        if (self.startReading()) {
            //            btnStartStop.setTitle("Stop", for: .normal)
            lblString.text = "Scanning for QR Code..."
        }
        
        self.viewPreview.layer.zPosition = 0
        self.viewPreview.session = session
        
//        switch AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) {
//        case .authorized:
//            // The user has previously granted access to the camera.
//            break
//            
//        case .notDetermined:
//            /*
//             The user has not yet been presented with the option to grant
//             video access. We suspend the session queue to delay session
//             setup until the access request has completed.
//             */
//            sessionQueue.suspend()
//            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { granted in
//                if !granted {
//                    //                    self.setupResult = .notAuthorized
//                }
//                self.sessionQueue.resume()
//            })
//            
//        default: break
//            // The user has previously denied access.
//            //            setupResult = .notAuthorized
//        }
//        
//        sessionQueue.async {
//            self.configureSession()
//        }
        
    }
    
    func setupData() {
        
    }
    
    func configureSession() {
        
        //        session.beginConfiguration()
        
        // Add video input.
        do {
            
            var defaultVideoDevice: AVCaptureDevice?
            // Choose the back wide angle camera if available, otherwise default to the front wide angle camera.
            if #available(iOS 10.0, *) {
                
                if let backCameraDevice = AVCaptureDevice.defaultDevice(withDeviceType: AVCaptureDeviceType.builtInWideAngleCamera, mediaType: AVMediaTypeVideo, position: .back) {
                    defaultVideoDevice = backCameraDevice
                } else if let frontCameraDevice = AVCaptureDevice.defaultDevice(withDeviceType: AVCaptureDeviceType.builtInWideAngleCamera, mediaType: AVMediaTypeVideo, position: .front) {
                    // Default to the front wide angle camera if the back wide angle camera is unavailable.
                    defaultVideoDevice = frontCameraDevice
                } else {
                    defaultVideoDevice = nil
                }
                
            } else {
                // Fallback on earlier versions
            }
            
            guard let videoDevice:AVCaptureDevice = defaultVideoDevice else {
                print("Could not get video device")
                //                setupResult = .configurationFailed
                session.commitConfiguration()
                return
            }
            
            let videoDeviceInput = try AVCaptureDeviceInput(device: videoDevice)
            
            if session.canAddInput(videoDeviceInput) {
                session.addInput(videoDeviceInput)
                self.videoDeviceInput = videoDeviceInput
                
                DispatchQueue.main.async {
                    /*
                     Why are we dispatching this to the main queue?
                     Because AVCaptureVideoPreviewLayer is the backing layer for PreviewView and UIView
                     can only be manipulated on the main thread.
                     Note: As an exception to the above rule, it is not necessary to serialize video orientation changes
                     on the AVCaptureVideoPreviewLayer’s connection with other session manipulation.
                     
                     Use the status bar orientation as the initial video orientation. Subsequent orientation changes are
                     handled by CameraViewController.viewWillTransition(to:with:).
                     */
                    let statusBarOrientation = UIApplication.shared.statusBarOrientation
                    var initialVideoOrientation: AVCaptureVideoOrientation = .portrait
                    if statusBarOrientation != .unknown {
                        if let videoOrientation = AVCaptureVideoOrientation(rawValue: statusBarOrientation.rawValue) {
                            initialVideoOrientation = videoOrientation
                        }
                    }
                    
                    self.viewPreview.videoPreviewLayer.connection!.videoOrientation = initialVideoOrientation
                }
            } else {
                print("Could not add video device input to the session")
                //                setupResult = .configurationFailed
                session.commitConfiguration()
                return
            }
        } catch {
            print("Could not create video device input: \(error)")
            //            setupResult = .configurationFailed
            session.commitConfiguration()
            return
        }
        
        // Add metadata output.
        if session.canAddOutput(metadataOutput) {
            session.addOutput(metadataOutput)
            
            // Set this view controller as the delegate for metadata objects.
            metadataOutput.setMetadataObjectsDelegate(self, queue: metadataObjectsQueue)
            metadataOutput.metadataObjectTypes = metadataOutput.availableMetadataObjectTypes // Use all metadata object types by default.
            
            /*
             Set an inital rect of interest that is 80% of the view's shortest side
             and 25% of the longest side. This means that the region of interest will
             appear in the same spot regardless of whether the app starts in portrait
             or landscape.
             */
            let width = 0.25
            let height = 0.8
            let x = (1.0 - width) / 2.0
            let y = (1.0 - height) / 2.0
            let initialRectOfInterest = CGRect(x: x, y: y, width: width, height: height)
            metadataOutput.rectOfInterest = initialRectOfInterest
            
            DispatchQueue.main.async {
                let initialRegionOfInterest = self.viewPreview.videoPreviewLayer.metadataOutputRectOfInterest(for: initialRectOfInterest)
                self.viewPreview.setRegionOfInterestWithProposedRegionOfInterest(initialRegionOfInterest)
            }
            
        } else {
            print("Could not add metadata output to the session")
            //            setupResult = .configurationFailed
            session.commitConfiguration()
            return
        }
        
        session.commitConfiguration()
        
    }
    
}

//MARK: -OTHER FUNC
extension ScanBarCodeViewController {
    
    //    // MARK: - Custom Method
    func startReading() -> Bool {
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            session = AVCaptureSession()
            session?.addInput(input)
            // Do the rest of your work...
        } catch let error as NSError {
            // Handle any errors
            print(error)
            return false
        }
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
        videoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        videoPreviewLayer.frame = viewPreview.layer.bounds
        viewPreview.layer.addSublayer(videoPreviewLayer)
        
        /* Check for metadata */
        let captureMetadataOutput = AVCaptureMetadataOutput()
        session?.addOutput(captureMetadataOutput)
        captureMetadataOutput.metadataObjectTypes = captureMetadataOutput.availableMetadataObjectTypes
        print(captureMetadataOutput.availableMetadataObjectTypes)
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        session?.startRunning()
        
        return true
    }
    //
    //    func stopReading() {
    //        captureSession?.stopRunning()
    //        captureSession = nil
    //        videoPreviewLayer.removeFromSuperlayer()
    //    }
    
}

extension ScanBarCodeViewController: AVCaptureMetadataOutputObjectsDelegate {
    
    func createMetadataObjectOverlayWithMetadataObject(_ metadataObject: AVMetadataObject) -> MetadataObjectLayer {
        // Transform the metadata object so the bounds are updated to reflect those of the video preview layer.
        let transformedMetadataObject = viewPreview.videoPreviewLayer.transformedMetadataObject(for: metadataObject)
        
        // Create the initial metadata object overlay layer that can be used for either machine readable codes or faces.
        let metadataObjectOverlayLayer = MetadataObjectLayer()
        metadataObjectOverlayLayer.metadataObject = transformedMetadataObject
        metadataObjectOverlayLayer.lineJoin = kCALineJoinRound
        metadataObjectOverlayLayer.lineWidth = 7.0
        metadataObjectOverlayLayer.strokeColor = view.tintColor.withAlphaComponent(0.7).cgColor
        metadataObjectOverlayLayer.fillColor = view.tintColor.withAlphaComponent(0.3).cgColor
        
        if let barcodeMetadataObject = transformedMetadataObject as? AVMetadataMachineReadableCodeObject {
            
            let barcodeOverlayPath = barcodeOverlayPathWithCorners(barcodeMetadataObject.corners as! [CGPoint])
            metadataObjectOverlayLayer.path = barcodeOverlayPath
            
            // If the metadata object has a string value, display it.
            var textLayerString: String = ""
            
            if let stringValue = barcodeMetadataObject.stringValue, !stringValue.characters.isEmpty {
                textLayerString = stringValue
            }
//            if #available(iOS 11.0, *) {
//                if let stringValue = barcodeMetadataObject.stringValue, !stringValue.characters.isEmpty {
//                    textLayerString = stringValue
//                } else if let barcodeDescriptor = barcodeMetadataObject.description {
//                    if barcodeDescriptor is CIQRCodeDescriptor {
//                        textLayerString = "<QR Code Binary Data Present>"
//                    } else if barcodeDescriptor is CIAztecCodeDescriptor {
//                        textLayerString = "<Aztec Code Binary Data Present>"
//                    } else if barcodeDescriptor is CIPDF417CodeDescriptor {
//                        textLayerString = "<PDF417 Code Binary Data Present>"
//                    } else if barcodeDescriptor is CIDataMatrixCodeDescriptor {
//                        textLayerString = "<Data Matrix Code Binary Data Present>"
//                    } else {
//                        fatalError("Unexpected barcode descriptor found: \(barcodeDescriptor)")
//                    }
//                } else {
//                    textLayerString = ""
//                }
//            } else {
//                // Fallback on earlier versions
//            }
            
            if textLayerString != "" {
                let barcodeOverlayBoundingBox = barcodeOverlayPath.boundingBox
                
                let textLayer = CATextLayer()
                textLayer.alignmentMode = kCAAlignmentCenter
                textLayer.bounds = CGRect(x: 0.0, y: 0.0, width: barcodeOverlayBoundingBox.size.width, height: barcodeOverlayBoundingBox.size.height)
                textLayer.contentsScale = UIScreen.main.scale
                textLayer.font = UIFont.boldSystemFont(ofSize: 19).fontName as CFString
                textLayer.position = CGPoint(x: barcodeOverlayBoundingBox.midX, y: barcodeOverlayBoundingBox.midY)
//                textLayer.string = NSAttributedString(string: textLayerString,
//                                                      attributes: [.font: UIFont.boldSystemFont(ofSize: 19),
//                                                                   .foregroundColor: UIColor.white.cgColor,
//                                                                   .strokeWidth: -5.0,
//                                                                   .strokeColor: UIColor.black.cgColor])
                textLayer.isWrapped = true
                
                // Invert the effect of transform of the video preview so the text is orientated with the interface orientation.
                textLayer.transform = CATransform3DInvert(CATransform3DMakeAffineTransform(viewPreview.transform))
                
                metadataObjectOverlayLayer.addSublayer(textLayer)
            }
        } else if let faceMetadataObject = transformedMetadataObject as? AVMetadataFaceObject {
            metadataObjectOverlayLayer.path = CGPath(rect: faceMetadataObject.bounds, transform: nil)
        }
        
        return metadataObjectOverlayLayer
    }
    
    private func barcodeOverlayPathWithCorners(_ corners: [CGPoint]) -> CGMutablePath {
        let path = CGMutablePath()
        
        if let corner = corners.first {
            path.move(to: corner, transform: .identity)
            
            for corner in corners[1..<corners.count] {
                path.addLine(to: corner)
            }
            
            path.closeSubpath()
        }
        
        return path
    }
    
    @objc
    private func removeMetadataObjectOverlayLayers() {
        for sublayer in metadataObjectOverlayLayers {
            sublayer.removeFromSuperlayer()
        }
        metadataObjectOverlayLayers = []
        
        removeMetadataObjectOverlayLayersTimer?.invalidate()
        removeMetadataObjectOverlayLayersTimer = nil
    }
    
    private func addMetadataObjectOverlayLayersToVideoPreviewView(_ metadataObjectOverlayLayers: [MetadataObjectLayer]) {
        // Add the metadata object overlays as sublayers of the video preview layer. We disable actions to allow for fast drawing.
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        for metadataObjectOverlayLayer in metadataObjectOverlayLayers {
            viewPreview.videoPreviewLayer.addSublayer(metadataObjectOverlayLayer)
        }
        CATransaction.commit()
        
        // Save the new metadata object overlays.
        self.metadataObjectOverlayLayers = metadataObjectOverlayLayers
        
        // Create a timer to destroy the metadata object overlays.
        removeMetadataObjectOverlayLayersTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(removeMetadataObjectOverlayLayers), userInfo: nil, repeats: false)
    }
    
//    private lazy var openBarcodeURLGestureRecognizer: UITapGestureRecognizer = {
//        UITapGestureRecognizer(target: self, action: #selector(ScanBarCodeViewController.openBarcodeURL(with:)))
//    }()
    
    @objc
    private func openBarcodeURL(with openBarcodeURLGestureRecognizer: UITapGestureRecognizer) {
        for metadataObjectOverlayLayer in metadataObjectOverlayLayers {
            if metadataObjectOverlayLayer.path!.contains(openBarcodeURLGestureRecognizer.location(in: viewPreview), using: .winding, transform: .identity) {
                if let barcodeMetadataObject = metadataObjectOverlayLayer.metadataObject as? AVMetadataMachineReadableCodeObject {
                    if let stringValue = barcodeMetadataObject.stringValue {
                        if let url = URL(string: stringValue) {
                            if #available(iOS 9.0, *) {
                                let safariViewController = SFSafariViewController(url: url)
                                 present(safariViewController, animated: true, completion: nil)
                            } else {
                                // Fallback on earlier versions
                            }
                           
                        }
                    }
                }
            }
        }
    }
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
        // wait() is used to drop new notifications if old ones are still processing, to avoid queueing up a bunch of stale data.
        if metadataObjectsOverlayLayersDrawingSemaphore.wait(timeout: .now()) == .success {
            DispatchQueue.main.async {
                self.removeMetadataObjectOverlayLayers()
                
                var metadataObjectOverlayLayers = [MetadataObjectLayer]()
                for metadataObject in metadataObjects {
                    let metadataObjectOverlayLayer = self.createMetadataObjectOverlayWithMetadataObject(metadataObject as! AVMetadataObject)
                    metadataObjectOverlayLayers.append(metadataObjectOverlayLayer)
                }
                
                self.addMetadataObjectOverlayLayersToVideoPreviewView(metadataObjectOverlayLayers)
                
                self.metadataObjectsOverlayLayersDrawingSemaphore.signal()
            }
        }
        
    }
    
//        func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
//    
//            for data in metadataObjects {
//                let metaData = data as! AVMetadataObject
//                print(metaData.description)
//                let transformed = videoPreviewLayer?.transformedMetadataObject(for: metaData) as? AVMetadataMachineReadableCodeObject
//                if let unwraped = transformed {
//                    print(unwraped.stringValue)
//    //                lblString.text = unwraped.stringValue
//    //                btnStartStop.setTitle("Start", for: .normal)
//                    self.performSelector(onMainThread: #selector(stopReading), with: nil, waitUntilDone: false)
//    //                isReading = false;
//                }
//            }
//            
//        }
    
}










