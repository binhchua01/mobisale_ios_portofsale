//
//  ListDeviceOTTViewController.swift
//  MobiSale
//
//  Created by Nguyen Sang on 1/22/18.
//  Copyright © 2018 FPT.RAD.FTool. All rights reserved.
//

import UIKit

@objc protocol ListDeviceOTTViewControllerDelegate:class {
    func cancelListDeviceOTT(arrChecked:[NSDictionary])
}

public class ListDeviceOTTViewController: UIViewController {
    
    @IBOutlet weak var myTableView:UITableView!
    
    weak var delegate:ListDeviceOTTViewControllerDelegate!
    
    var Id:String = ""
    
    var arrDictionary:[NSDictionary] = []
    
    var arrDeviceChecked:[NSDictionary] = []

}

extension ListDeviceOTTViewController {
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupView()
        self.setupData()
        
    }
    
    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//MARK: ACTION
extension ListDeviceOTTViewController {
    
    @IBAction func pressedCancel(sender:UIButton) {
        
        self.delegate.cancelListDeviceOTT(arrChecked: arrDeviceChecked)
        
    }
    
}

extension ListDeviceOTTViewController {
    
    func setupView() {
        
        self.setupTableView()
        
    }
    
    func setupData() {
        
        self.getListOTT()
        
    }
    
    func setupTableView() {
        
        self.myTableView.delegate = self
        self.myTableView.dataSource = self
        self.myTableView.allowsSelection = true
        
        self.myTableView.allowsSelectionDuringEditing = true
        self.myTableView.allowsMultipleSelectionDuringEditing = true
        self.myTableView.setEditing(true, animated: true)
        
    }
    
}

//MARK: -API
extension ListDeviceOTTViewController {
    
    func getListOTT() {
        
        let share = ShareData.instance()
        
        let dic:NSDictionary = NSDictionary(dictionary: ["UserName":share?.currentUser.userName ?? ""])
        
//        let dic = [
//            "UserName":share?.currentUser.userName
//        ]
//        
        share?.appProxy.getListDeviceOTT(dic as! [AnyHashable : Any], handler: { (result, errorCode, message) in
            
            let dataJSON = result as? [NSDictionary]
            
            self.arrDictionary = dataJSON ?? []
            
            self.myTableView.reloadData()
            
        }, errorHandler: { (error) in
            print(error ?? "")
        })
        
    }
    
}

//MARK: -UITABLEVIEWDELEGATE, UITABLEVIEWDATASOURCE
extension ListDeviceOTTViewController:UITableViewDelegate, UITableViewDataSource {
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrDictionary.count
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "cell"
        
        let cell = UITableViewCell(style: .default, reuseIdentifier: identifier)
        
        let strNameOTT = self.arrDictionary[indexPath.row].value(forKey: FPTPlayBoxOTT.name.rawValue) as? String
        cell.textLabel?.text = strNameOTT ?? ""
        
        if arrDeviceChecked.count > 0 {
            
            let arrDuplicate = self.arrDeviceChecked.filter({ (dicOTTChecked) -> Bool in
                let strOTTChecked = dicOTTChecked["OTTID"] as? Int
                let strOTT = self.arrDictionary[indexPath.row]["OTTID"] as? Int
                return (strOTTChecked ?? 0) == (strOTT ?? 0)
            })
            
            if arrDuplicate.count > 0 {
                self.myTableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
            }
            
        }
        
        
        return cell
    }
    
//    public func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        
//        if arrDeviceChecked.count > 0 {
//            
//            let arrDuplicate = self.arrDeviceChecked.filter({ (dicOTTChecked) -> Bool in
//                let strOTTChecked = dicOTTChecked["OTTID"] as? Int
//                let strOTT = self.arrDictionary[indexPath.row]["OTTID"] as? Int
//                return (strOTTChecked ?? 0) == (strOTT ?? 0)
//            })
//            
//            if arrDuplicate.count > 0 {
//                self.myTableView.selectRow(at: indexPath, animated: true, scrollPosition: .none)
//            }
//            
//        }
//        
//    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let idOTT = self.arrDictionary[indexPath.row].value(forKey: FPTPlayBoxOTT.id.rawValue) as? Int
        
        let arrPredicate = self.arrDeviceChecked.filter { (diction) -> Bool in
            let strOtt = diction.value(forKey: FPTPlayBoxOTT.id.rawValue) as? Int
            return  (strOtt ?? 0) == (idOTT ?? 0)
        }
        
        if arrPredicate.count <= 0 {//append
            
            let dicSelected = self.arrDictionary[indexPath.row]
            
            let numBe:Int = -1
            dicSelected.setValue(numBe, forKey: "PromotionID")
            dicSelected.setValue(1, forKey: "OTTCount")
            
            self.arrDeviceChecked.append(dicSelected)
            
        } else {//remove
            
            let dicSelected = self.arrDictionary[indexPath.row]
            if let indexDictionary = self.arrDeviceChecked.index(of: dicSelected) {
                self.arrDeviceChecked.remove(at: indexDictionary)
            }
            
        }
        
    }
    
    public func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
//        let dicSelected = self.arrDictionary[indexPath.row]
//        if let indexDictionary = self.arrDeviceChecked.index(of: dicSelected) {
//            self.arrDeviceChecked.remove(at: indexDictionary)
//        }
        
        if arrDeviceChecked.count > 0 {
            
            let arrDuplicate = self.arrDeviceChecked.filter({ (dicOTTChecked) -> Bool in
                let strOTTChecked = dicOTTChecked["OTTID"] as? Int
                let strOTT = self.arrDictionary[indexPath.row]["OTTID"] as? Int
                return (strOTTChecked ?? 0) == (strOTT ?? 0)
            })
            
            if arrDuplicate.count > 0 {
                if let indexDictionary = self.arrDeviceChecked.index(of: arrDuplicate.first!) {
                    self.arrDeviceChecked.remove(at: indexDictionary)
                }
            }
            
        }
        
        
    }
    
    
}


