//
//  ListDeviceViewController.h
//  MobiSale
//
//  Created by Tran Vu on 6/11/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@protocol ListDeviceViewControllerDelegate <NSObject>
@required
- (void)cancelListDevices :(NSMutableArray *)arrayData;

@end

@interface ListDeviceViewController : BaseViewController
@property (weak,nonatomic)id<ListDeviceViewControllerDelegate>delegate;
@property (weak, nonatomic) IBOutlet UITableView *tableViewInfo;
@property (weak, nonatomic) IBOutlet UIButton *btnClose;
@property (strong, nonatomic) NSString *Id;
@property(nonatomic,strong) NSMutableArray *arrayDevieChecked;
@property(nonatomic,strong) NSMutableArray *arraySelectedDevies;
@end
