//
//  ListRegisteredFormViewController.h
//  MobiSale
//
//  Created by Bored Ninjas on 11/18/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "TDDatePicker.h"
#import "UIPopoverListView.h"
#import "RegistrationFormDetailRecord.h"

//vutt11 init Protocol trancision Arraydata(ListDevies saler checked) from StyleOfServiceView

@protocol LisRegisteredFormViewControllerDelegate <NSObject>

- (void)transitionArrayListDevies :(NSMutableArray *)arrayListDevies;

@end


@interface ListRegisteredFormViewController : TDDatePicker<UIPopoverListViewDataSource, UIPopoverListViewDelegate>

@property (strong, nonatomic) UIImagePickerController *imagePickerController;

@property (nonatomic , retain) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) NSString *Id;
@property (strong, nonatomic) RegistrationFormDetailRecord *rc;
@property (retain, nonatomic) NSArray *arrayListPacketOld;
@property (weak,nonatomic)id<LisRegisteredFormViewControllerDelegate>delegate;

@end
