 
//
//  PaymentViewController.m
//  demoRegister
//
//  Created by Bored Ninjas on 11/24/16.
//  Copyright © 2016 com.StevenNguyen.DemoCreateRegisterForm. All rights reserved.
//

#import "PaymentViewController.h"
#import "AniTextFieldCell.h"
#import "AniPhoneNumberCell.h"
#import "AniTextViewCell.h"
#import "ButtonNextCell.h"
#import "AniDropDownCellTableViewCell.h"
#import "RegistrationFormModel.h"
#import "KeyValueModel.h"
#import "PromotionModel.h"
#import "Common_client.h"
#import "ShareData.h"
#import "IPTVCell.h"
#import "UploadImageCell.h"
#import "UIImage+FPTCustom.h"
#import "CacheDropDownCell.h"

//Define import add cell DeviceEquipment
#import "DeviceEquipmentCell.h"
#import "ListDeviceEquipmentCell.h"
#import "TableListDeviceEquipmentCell.h"

#define kArrayPaymentType @"arraypaymenttype"


enum actionSheetTagType {
    back = 1,
    save = 2,
    datePicker = 3
};

enum StyleCell{
    
    depositBlackPoint = 0,
    depositSubscribers = 1,
    office365 = 2,
    iptv = 3,
    internet = 4,
    fptPlay = 5,
    device = 6,
    total = 7,
    formOfPayment = 8,
    indoor = 9,
    outdoor = 10,
    button = 11
   
    
};

@interface PaymentViewController ()<UITableViewDelegate, UITableViewDataSource, AniDropDownCellTableViewCellDelegate,ButtonNextCellDelegate,AniTextFieldCellDelegate,UploadImageCellDelegate> {
    
    __weak IBOutlet UITableView *myTableView;
    

    NSString *imageData;
}
@property (strong, nonatomic) NSString *extension;
@property (strong, nonatomic) NSString *baseName;
@property (strong, nonatomic) NSCharacterSet *doNotWant;
@property (strong, nonatomic) NSMutableArray *arrayListDevice;

@end

@implementation PaymentViewController {
    
    BOOL isSelectedDeposit;
    BOOL isSelectedDepositBlackPoint;
    BOOL isSelectedPayment;
    
   
    NSString *TypeDeployment, *TypeDrillWall, *Address, *BoxId1, *BoxId2, *BoxId3, *BoxId4, *CableStatus, *InsCable, *Contract , *BoxQuanlity, *PrepaidTotal, *DeviceTotal;
    
    NSString *ID;
    
    bool flagtotaliptv,flagAlert;
    
    UIAlertView * updateCheck, *updateSuccess;
    
    int deposit, amountInternet, amountIPTV, amountTotal, office365Total, amountOTT;
    
    NSString *amountDevice;
    
    RegistrationFormDetailRecord *rc;

}
- (NSMutableArray*)arrayListDevice {
    if (!_arrayListDevice) {
        _arrayListDevice = [[NSMutableArray alloc] init];
    }
    return _arrayListDevice;
}

@synthesize delegate;
@synthesize arrDeposit;
@synthesize arrDepositBlackPoint;
@synthesize arrPaymentType;
@synthesize regcode;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    deposit = 0;
    amountInternet = 0;
    amountIPTV = 0;
    amountTotal = 0;
    office365Total = 0;
    amountOTT = 0;
    
    isSelectedDeposit = NO;
    isSelectedDepositBlackPoint = NO;
    isSelectedPayment = NO;
    
    flagAlert = FALSE;
    [self configTableView];
    

    if (self.Id.length <= 0 || [self.Id isEqualToString:@"0"]) {
        
        [self LoadDeposit:@"0"];
        [self LoadDepositBlackPoint:@"0"];
//        [self loadPaymentType:@"0"];
        [self loadCachePaymentType:@"0"];
        
    }
    
}

//MARK: -StevenNguyen-FixError
-(void) loadCachePaymentType:(NSString *)idPayment {
    
//    kArrayPaymentType
    NSMutableArray *arrCachePaymentType = [[EGOCache globalCache] objectForKey:kArrayPaymentType];
    
    if (arrCachePaymentType.count > 0) {//Có dữ liệu từ Cache
        
        self.arrPaymentType = arrCachePaymentType;
        
        for ( KeyValueModel *selectedPaymentType in self.arrPaymentType ) {
            if([idPayment isEqualToString:selectedPaymentType.Key]){
                [[RegistrationFormModel sharedInstance] setPaymentType:selectedPaymentType];
                break;
            }
        }
        
        [myTableView reloadData];
        
    } else {//Không có dữ liệu nên request API
        
        [self.delegate showMBProcessListRegis];
        [self loadPaymentType:idPayment];
        
    }
}

//MARK: -StevenNguyen-FixError
-(void) saveCacheWith:(id)obj KeyCache:(NSString *)keyCache {
    
    dispatch_queue_t dispatchQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(dispatchQueue, ^ {
        [[EGOCache globalCache] setObject:obj forKey:keyCache];
    });
    
}


-(void)reloadDataViewController {
    
    [self.delegate hideMBProcessListRegis];
    
    [self checkPackageService];
    [myTableView reloadData];
    
}

//Refesh lai tong tien 365 neu bo chon offce 365

- (void) checkTotalOffice365 {
    NSMutableArray *selectedLocalType = [[RegistrationFormModel sharedInstance] localType];
    BOOL checkStypeOffice365;
    checkStypeOffice365 = false;
    
    for (int i = 0; i<selectedLocalType.count;i++) {
        NSString *keyTotalOffice365 = [[selectedLocalType objectAtIndex:i] valueForKey:@"Key"];
        if (![keyTotalOffice365 isEqualToString:@"3"]) {
            
            office365Total = 0;
            
        }
        if ([keyTotalOffice365 isEqualToString:@"3"]) {
            checkStypeOffice365 = true;
        }
    }
    
    if (checkStypeOffice365 == true) {
        
        office365Total = [[RegistrationFormModel sharedInstance] totalOffice365];
    }
    
}

//config and register cell for tableview.
- (void)configTableView {
    
    myTableView.delegate = self;
    myTableView.dataSource = self;
    
    UINib *cellTextField = [UINib nibWithNibName:@"AniTextFieldCell" bundle:nil];
    UINib *cellNote = [UINib nibWithNibName:@"AniTextViewCell" bundle:nil];
    UINib *cellButton = [UINib nibWithNibName:@"ButtonNextCell" bundle:nil];
    UINib *cellDropDown = [UINib nibWithNibName:@"AniDropDownCellTableViewCell" bundle:nil];
    UINib *cellCache = [UINib nibWithNibName:@"CacheDropDownCell" bundle:nil];
    
    [myTableView registerNib:cellTextField forCellReuseIdentifier:@"cellTxtF"];
    [myTableView registerNib:cellNote forCellReuseIdentifier:@"cellNote"];
    [myTableView registerNib:cellButton forCellReuseIdentifier:@"cellButton"];
    [myTableView registerNib:cellDropDown forCellReuseIdentifier:@"cellDropDown"];
    [myTableView registerNib:cellCache forCellReuseIdentifier:@"CacheDropDownCell"];
    
}

- (void)checkPackageService {
    
    NSMutableArray *arr = [[RegistrationFormModel sharedInstance] localType];
    
    BOOL isInternet = NO;
    BOOL isIPTV = NO;
    BOOL isOffice365 = NO;
    BOOL isFPTPlay = NO;
    BOOL isDevice = NO;
    
    for ( KeyValueModel *key in arr ) {
        
        if ([key.Key  isEqual: @"0"]) {
            //Internet
            isInternet = YES;
            
        } else if ([key.Key isEqual:@"1"]) {
            //IPTV
            isIPTV = YES;
            
        } else if ([key.Key isEqual:@"3"]) {
            //Office
            isOffice365 = YES;
            
        } else if ([key.Key isEqual:@"4"]) {
            isFPTPlay = YES;
        }
        else if ([key.Key isEqual:@"5"]) {
            isDevice = YES;
        }
        
    }
    
    if (isFPTPlay == NO) {
        //IF FptP lay == NO...
        [[RegistrationFormModel sharedInstance] setTotalFPTPlay:0];
        [[RegistrationFormModel sharedInstance] setCountFPTPlay:0];
        
        amountOTT = 0;
        
    } else {
        
        [self getToTalOTT];
        
        
        
        
    }
    
    if (isInternet == NO) {
        
        [[RegistrationFormModel sharedInstance] setInternetPromotion:nil];
        [[RegistrationFormModel sharedInstance] setTotalInternet:0];
        
    } else {
        
        [self Total];
    }
    
    if ( isIPTV == NO ) {
        
        [[RegistrationFormModel sharedInstance] setIptvRequest:@"0"];
        [[RegistrationFormModel sharedInstance] setIptvRequest:@"0"];
        [[RegistrationFormModel sharedInstance] setIptvServiceBase:nil];
        [[RegistrationFormModel sharedInstance] setIptvDeployment:nil];
        [[RegistrationFormModel sharedInstance] setIptvPackageIPTV:nil];
        [[RegistrationFormModel sharedInstance] setIptvPromotionIPTV:nil];
        [[RegistrationFormModel sharedInstance] setIptvPromotionIPTV2:nil];
        [[RegistrationFormModel sharedInstance] setIptvCombo:nil];
        [[RegistrationFormModel sharedInstance] setIptvDeviceBoxCount:@"0"];
        [[RegistrationFormModel sharedInstance] setIptvDevicePLCCount:@"0"];
        [[RegistrationFormModel sharedInstance] setIptvDeviceSTBCount:@"0"];
        [[RegistrationFormModel sharedInstance] setIptvFilmPlusChargeTime:@"0"];
        [[RegistrationFormModel sharedInstance] setIptvFilmPlusPrepaidMonth:@"0"];
        [[RegistrationFormModel sharedInstance] setIptvFilmHotChargeTime:@"0"];
        [[RegistrationFormModel sharedInstance] setIptvFilmHotPrepaidMonth:@"0"];
        [[RegistrationFormModel sharedInstance] setIptvFilmKPlusChargeTime:@"0"];
        [[RegistrationFormModel sharedInstance] setIptvFilmKPlusPrepaidMonth:@"0"];
        [[RegistrationFormModel sharedInstance] setIptvFilmVTCChargeTime:@"0"];
        [[RegistrationFormModel sharedInstance] setIptvFilmVTCPrepaidMonth:@"0"];
        [[RegistrationFormModel sharedInstance] setIptvFilmVTVChargeTime:@"0"];
        [[RegistrationFormModel sharedInstance] setIptvFilmVTVPrepaidMonth:@"0"];
        [[RegistrationFormModel sharedInstance] setTotalIPTV:0];
        
        [self Total];
        
    } else {
        [self getTotalIPTV];
    }
    
    if (isOffice365 == NO) {
        
    } else {
        [self Total];
    }
    
    // tinh tong tien thiet bi
   // self.arrayListDevice = [[RegistrationFormModel sharedInstance] listDevicePackagesArray];
   // [self getTotalDevice:self.arrayListDevice];
    
    if (isDevice == NO) {
        [[RegistrationFormModel sharedInstance] setTotalDevice:@"0"];
        amountDevice = 0;
        [self Total];
        [myTableView reloadData];
        
    } else {
    
        self.arrayListDevice = [[RegistrationFormModel sharedInstance] listDevicePackagesArray];
        
        [self getTotalDevice:self.arrayListDevice];
        
    }
    
}

//MARK: -LOADALL

-(void)LoadDeposit:(NSString *)Id {
    
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    ShareData *shared = [ShareData instance];
    
    if (self.regcode.length <= 0) {
        self.regcode = @"";
    }
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    [dict setObject:self.regcode forKey:@"RegCode"];
    
    [shared.registrationFormProxy getContractDepositList:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
        
            [self.delegate logOutWhenEndSession];
            [self.delegate hideMBProcessListRegis];
            return;
        }
        if (errorCode.length < 1) {
            errorCode=@"<null>";
        }
        
        if ([message isEqualToString:@"Error Service: Unauthorized"]) {

            [self.delegate hideMBProcessListRegis];
            return;
        }
        if(![errorCode isEqualToString:@"<null>"]){
            
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self.delegate hideMBProcessListRegis];
            return ;
        }
        self.arrDeposit = result;
       // vutt11 issue asynchronous
        if (self.Id.length <= 0 || [self.Id isEqualToString:@"0"]) {
            [self loadPaymentType:@"0"];
        }
        else {
            [ self loadPaymentType:self.rc.Payment];
        }
        
        for ( KeyValueModel *selectedDeposit in self.arrDeposit ) {
            if([Id isEqualToString:selectedDeposit.Key]){
                [[RegistrationFormModel sharedInstance] setDeposit:selectedDeposit];
                isSelectedDeposit = YES;
                [myTableView reloadData];
                  [self Total];
                return;
            }
        }
        
        [myTableView reloadData];
         NSLog(@"qua day roi 2");
        
    } errorHandler:^(NSError *error) {

        [self.delegate hideMBProcessListRegis];
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetContractDepositList",[error localizedDescription]]];
    }];
    
   
    
}

-(void)LoadDepositBlackPoint:(NSString *) Id {
    self.arrDepositBlackPoint = [Common getDepositBlackPoint];
    int i = 0;
    if([Id isEqualToString:@"0"]){
        [[RegistrationFormModel sharedInstance] setDepositBlackPoint:[self.arrDepositBlackPoint objectAtIndex:0]];
        [myTableView reloadData];
    }else {
        for (i = 0; i< self.arrDepositBlackPoint.count; i++) {
            KeyValueModel *selectedDepositBlackPoint = [self.arrDepositBlackPoint objectAtIndex:i];
            if([Id isEqualToString:selectedDepositBlackPoint.Key]){
                isSelectedDepositBlackPoint = YES;
                [[RegistrationFormModel sharedInstance] setDepositBlackPoint:selectedDepositBlackPoint];
                 [self Total];
                return;
            }
        }
       
        [myTableView reloadData];
    }
    
}

- (void)loadPaymentType:(NSString *)Id {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self.delegate hideMBProcessListRegis];
        return;
    }
    
    
    if ([[RegistrationFormModel sharedInstance] county] == nil) {
        [[RegistrationFormModel sharedInstance] setCounty:[[KeyValueModel alloc]initWithName:@"" description:@""]];
    }
    
    if ([[RegistrationFormModel sharedInstance] ward] == nil) {
        [[RegistrationFormModel sharedInstance] setWard:[[KeyValueModel alloc]initWithName:@"" description:@""]];
    }
    
    if ([[RegistrationFormModel sharedInstance] street] == nil) {
        [[RegistrationFormModel sharedInstance] setStreet:[[KeyValueModel alloc]initWithName:@"" description:@""]];
    }
    
    if ([[RegistrationFormModel sharedInstance] nameAparment] == nil) {
        [[RegistrationFormModel sharedInstance] setNameAparment:[[KeyValueModel alloc]initWithName:@"" description:@""]];
    }
    // vutt11 Deleted Space while
     NSCharacterSet *charaset = [NSCharacterSet whitespaceCharacterSet];
    NSString * numberHouse = [[RegistrationFormModel sharedInstance] numberOfHouse] ;
    numberHouse = [numberHouse stringByTrimmingCharactersInSet:charaset];
    
    ShareData *shared = [ShareData instance];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:shared.currentUser.userName     forKey:@"UserName"];
    [dict setObject:shared.currentUser.LocationName forKey:@"BillTo_City"];
    [dict setObject:[[RegistrationFormModel sharedInstance] county].Key            forKey:@"BillTo_Dictrict"];
    [dict setObject:[[RegistrationFormModel sharedInstance] ward].Key                forKey:@"BillTo_Ward"];
    [dict setObject:[[RegistrationFormModel sharedInstance] street].Key              forKey:@"BillTo_Street"];
    [dict setObject:[[RegistrationFormModel sharedInstance] nameAparment].Key           forKey:@"BillTo_NameVilla"];
    
    
    [dict setObject:numberHouse ?:@"" forKey:@"BillTo_Number"];
    
    [shared.registrationFormProxy getPaymentType:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self.delegate logOutWhenEndSession];
            [self.delegate hideMBProcessListRegis];
            return;
        }
        
        if (errorCode.length < 1) {
            errorCode=@"<null>";
        }
        if ([message isEqualToString:@"Error Service: Unauthorized"]) {
            [self.delegate hideMBProcessListRegis];
            return;
        }
        
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self.delegate hideMBProcessListRegis];
            return ;
        }
        
        self.arrPaymentType = result;
        
        [self saveCacheWith:result KeyCache:kArrayPaymentType];
        
        for ( KeyValueModel *selectedPaymentType in self.arrPaymentType ) {
            if([Id isEqualToString:selectedPaymentType.Key]){
                [[RegistrationFormModel sharedInstance] setPaymentType:selectedPaymentType];
                break;
            }
        }
        
        [myTableView reloadData];
//        [self hideMBProcess];
        
        [self.delegate hideMBProcessListRegis];
        
        
    } errorHandler:^(NSError *error) {
        [self.delegate hideMBProcessListRegis];
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetContractDepositList",[error localizedDescription]]];
    }];
}


#pragma mark - load image
- (void)getImageWithUrl:(NSString *)url title:(NSString *)title {
    
    ShareData *shared = [ShareData instance];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    NSString *str = StringFormat(@"Images/%@",url);
    [dict setObject:str forKey:@"Path"];
    //urlimage
    [shared.promotionProgramProxy getImage:dict completeionHander:^(id result, NSString *errorCode, NSString *message) {

        [self.delegate hideMBProcessListRegis];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self.delegate logOutWhenEndSession];
            [self.delegate hideMBProcessListRegis];
            return;
        }
        
        if(![errorCode isEqualToString:@"0"] || result == nil ) {
            
            return ;
        }
        if ([message isEqualToString:@"Error Service: Unauthorized"]) {
            [self.delegate hideMBProcessListRegis];
            return;
        }
        
        NSString *strData = StringFormat(@"%@",[result objectForKey:@"Image"]);
        if ([strData isEqualToString:@"<null>"]) {
            return;
        }
        
        UIImage *image = [self decodeBase64ToImage:strData title:title];
        
        [[RegistrationFormModel sharedInstance] setImageUpload:image];
        
    } errorHandler:^(NSError *error) {
        
    }];
}

#pragma mark - get image from binary string
- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData title:(NSString *)title{
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    
    return [UIImage imageWithData:data];
}

//MARK: -SAVE...
- (void)showActionSheetUpdate {
    NSLog(@"----- Đang Lưu thông tin Phiếu đăng ký...");
    UIActionSheet *noticeSave = [[UIActionSheet alloc] initWithTitle:@"Bạn chắc chắn muốn cập nhật thông tin Phiếu đăng ký?" delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
    noticeSave.tag = save;
    [noticeSave showInView:[UIApplication sharedApplication].keyWindow];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        switch (actionSheet.tag) {
            case back:
                [self.navigationController popViewControllerAnimated:YES];
                break;
            case save: {
                [delegate showMBProcessListRegis];
                if (![[[RegistrationFormModel sharedInstance] typeOfHouse].Key isEqualToString:@"2"]) {
                    [self checkAddressNumber];
                } else {
                    [self checkRegistration2];
                }
            }
                break;
            case datePicker:
                break;
            default:
                break;
        }
    }
    
}

- (void)checkAddressNumber {
    ShareData *shared = [ShareData instance];
    

    // Deleted Character specili
   NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
    NSString *billToNumber = [[RegistrationFormModel sharedInstance] numberOfHouse];
    billToNumber = [billToNumber stringByTrimmingCharactersInSet:charSet];
    
    [shared.registrationFormProxy checkAddressNumber:shared.currentUser.userName locationParent:shared.currentUser.LocationParent locationID:shared.currentUser.LocationID houseNumber:billToNumber completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self.delegate logOutWhenEndSession];
            [self.delegate hideMBProcessListRegis];
            
            return;
        }
        if (result == nil) {
            return;
        }
        NSString *strIgnoreAction = [result objectForKey:@"IgnoreAction"];
        NSString *strResultID     = [result objectForKey:@"ResultID"];
        NSString *strResult       = [result objectForKey:@"Result"];
        if ([strResultID intValue] > 0) {

            [self checkRegistration2];
            
            return;
        }
        if ([strIgnoreAction intValue] <= 0) {
            [self showAlertBox:@"Lỗi" message:strResult];
            [self.delegate hideMBProcessListRegis];
            return;
        }
        if ([strIgnoreAction intValue] > 0) {
            [self showCheckReg:@"Số nhà không hợp lệ"];
            updateCheck.tag = 1;
        }
        
    } errorHandler:^(NSError *error) {
        [self.delegate hideMBProcessListRegis];
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@",[error localizedDescription]]];
    }];
}

//StevenNguyen...checkRegistrationAPI

- (void)checkRegistration2 {
    
    if (self.regcode.length <= 0) {
        self.regcode = @"";
    }
    
    NSString *typeHouse = [[RegistrationFormModel sharedInstance] typeOfHouse].Key;
    
    if (![typeHouse isEqualToString:@"2"]) {
        
        [[RegistrationFormModel sharedInstance] setNameAparment:nil];
        [[RegistrationFormModel sharedInstance] setLot:@""];
        [[RegistrationFormModel sharedInstance] setFloor:@""];
        [[RegistrationFormModel sharedInstance] setRoom:@""];
    }
    
    if (![typeHouse isEqualToString:@"3"]) {
        [[RegistrationFormModel sharedInstance] setHousePosition:nil];
    }
    
    ShareData *share = [ShareData instance];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:[[RegistrationFormModel sharedInstance] isCard] ?:@""          forKey:@"Passport"];
    [dict setObject:[[RegistrationFormModel sharedInstance] taxCode] ?:@""            forKey:@"TaxID"];
    [dict setObject:self.regcode ?:@""                forKey:@"RegCode"];
    [dict setObject:share.currentUser.userName ?:@""  forKey:@"UserName"];
    [dict setObject:[[RegistrationFormModel sharedInstance] numberPhone1] ?:@""         forKey:@"PhoneNumber"];
    
    [dict setObject:[[RegistrationFormModel sharedInstance] street].Key ?:@""    forKey:@"Street"];
    [dict setObject:[[RegistrationFormModel sharedInstance] ward].Key   ?:@""    forKey:@"Ward"];
    [dict setObject:[[RegistrationFormModel sharedInstance] county].Key ?:@""  forKey:@"District"];
    [dict setObject:[[RegistrationFormModel sharedInstance] typeOfHouse].Key ?:@"" forKey:@"TypeHouse"];
    [dict setObject:@""                         forKey:@"HousePosition"];
    [dict setObject:@""                         forKey:@"Number"];
    [dict setObject:@""                         forKey:@"NameVilla"];
    [dict setObject:@""                         forKey:@"Lot"];
    [dict setObject:@""                         forKey:@"Floor"];
    [dict setObject:@""                         forKey:@"Room"];
    
    NSString *deployment = [[RegistrationFormModel sharedInstance] iptvDeployment].Key ?:@"";
    NSString *servicePack = [[RegistrationFormModel sharedInstance] internetService].Key ?:@"";
    NSString *promotionIternet = [[RegistrationFormModel sharedInstance] internetPromotion].PromotionId ?:@"";
    
    NSString *packageIPTV = [[RegistrationFormModel sharedInstance] iptvPackageIPTV].Key ?:@"";
    NSString *promotionIPTV = [[RegistrationFormModel sharedInstance] iptvPromotionIPTV].PromotionId ?:@"";
    
    [dict setObject:deployment            forKey:@"PayTVStatus"];
    [dict setObject:servicePack           forKey:@"LocalType"];
    [dict setObject:promotionIternet     forKey:@"PromotionID"];
    [dict setObject:packageIPTV           forKey:@"IPTVPackpage"];
    [dict setObject:promotionIPTV forKey:@"IPTVPromotionID"];
    
    //OTT and Upload Image
    int countBox = [[RegistrationFormModel sharedInstance] countFPTPlay];
    
    NSString *imageInfo = [[RegistrationFormModel sharedInstance] imgInfo];
    
    [dict setObject:[NSString stringWithFormat:@"%ld",(long)countBox]?:@"" forKey:@"OTTBoxCount"];
    
    [dict setObject:[NSString stringWithFormat:@"%@",imageInfo]?:@"" forKey:@"ImageInfo"];
    
//    NSString *typeHouse = [[RegistrationFormModel sharedInstance] typeOfHouse].Key;
    // Nhà phố
    if ([typeHouse isEqualToString:@"1"]) {
        [dict setObject:[[RegistrationFormModel sharedInstance] numberOfHouse] forKey:@"Number"];
    }
    
    NSString *strLot = [[RegistrationFormModel sharedInstance] lot] ?:@"";
    NSString *strFloor = [[RegistrationFormModel sharedInstance] floor] ?:@"";
    NSString *strRoom = [[RegistrationFormModel sharedInstance] room] ?:@"";
    NSString *strNameVilla = [[RegistrationFormModel sharedInstance] nameAparment].Key ?:@"";
    // chung cư
    if ([typeHouse isEqualToString:@"2"]) {
        [dict setObject:strLot    forKey:@"Lot"];
        [dict setObject:strFloor  forKey:@"Floor"];
        [dict setObject:strRoom   forKey:@"Room"];
        [dict setObject:strNameVilla forKey:@"NameVilla"];
        
        [[RegistrationFormModel sharedInstance] setNumberOfHouse:@""];
    }
    // Nhà không số
    if ([typeHouse isEqualToString:@"3"]) {
        [dict setObject:[[RegistrationFormModel sharedInstance] numberOfHouse]   forKey:@"Number"];
        [dict setObject:[[RegistrationFormModel sharedInstance] housePosition].Key ?:@""  forKey:@"HousePosition"];
    }
    
    [share.appProxy CheckRegistration2:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self.delegate logOutWhenEndSession];
            [self.delegate hideMBProcessListRegis];
            return;
        }
        //vutt11
        if ([message isEqualToString:@"Error Service: Unauthorized"]) {
            [self.delegate hideMBProcessListRegis];
            return;
        }
        
        NSArray *arrData = result;
        NSInteger ResultID = [[[arrData objectAtIndex:0] objectForKey:@"ResultID"] integerValue];
        
        if (ResultID != 0) {
            
            NSString *msg = [[arrData objectAtIndex:0] objectForKey:@"Messages"];
            NSString *dupID = [[arrData objectAtIndex:0] objectForKey:@"DupID"];
            // save notice on server
            [self updateHasShowDuplicateWithDupID:dupID];
            // show alert update continue yes or no when duplicate info
            [self showCheckReg:msg];
            
            updateCheck.tag = 2;
            [self.delegate hideMBProcessListRegis];
            return;
        }
        
        [self UpdateRegistrationGo];
        [self hideHUD];
        [self.delegate hideMBProcessListRegis];
        
    } errorHandler:^(NSError *error) {
;
        [self.delegate hideMBProcessListRegis];
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@",[error localizedDescription]]];

    }];
}

- (void)UpdateRegistrationGo {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self.delegate hideMBProcessListRegis];
        return;
    }
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters = [self setDataUpdateRegistration];
    [self.delegate showMBProcessListRegis];
    ShareData *shareData = [ShareData instance];
    [shareData.registrationFormProxy updateRegistration:parameters
                                      completionHandler:^(id result, NSString *errorCode, NSString *message) {
                                        
                                          NSArray *arrData = result;
                                          if (arrData.count <= 0) {
                                              // Cập nhật PĐK thất bại.
                                              /// Show thông báo.
                                              [self showAlertBox:@"Thông báo!" message:message tag:ErrorTag];
//                                               [self hideMBProcess];
                                              [self.delegate hideMBProcessListRegis];
                                              
                                          } else {
                                              
                                              NSString *result = StringFormat(@"%@",[arrData[0]  objectForKey:@"Result"]);
                                              NSString *resultID = StringFormat(@"%@",[arrData[0]  objectForKey:@"ResultID"]);
                                              [self.delegate hideMBProcessListRegis];
                                              [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",result]];
                                              
                                              // Cập nhật PĐK thành công
                                              if(![resultID isEqualToString:@"0"]){
                                                [self.delegate hideMBProcessListRegis];
                                                  ID = resultID;
                                                  flagAlert = TRUE;
                                                  [delegate popViewController:resultID];
                                              }
                                          }
                                          
                                         // [self hideHUD];
                                         // [self.delegate hideMBProcessListRegis];
                                          return;
                                          
                                      } errorHandler:^(NSError *error) {
                                          [self showAlertBox:@"Lỗi!" message:[error localizedDescription] tag:ErrorTag];
                                          [self.delegate hideMBProcessListRegis];
                                      }];
    
}

// Set parameter for Update registration API.
- (NSMutableDictionary *)setDataUpdateRegistration
{
    NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    NSString *promotionid = @"0";
    NSString *iptvPackage = @"";
    NSString *fullname = @"";
    NSString *fullname1 = @"";
    
    NSString *textPersionContact1 = [[RegistrationFormModel sharedInstance] contact1];
    NSString *textPersionContact2 = [[RegistrationFormModel sharedInstance] contact2];
    
    // deleted ("\'") Error 203
   // textPersionContact1 = [textPersionContact1 stringByReplacingOccurrencesOfString:@"\'" withString:@""];
   // textPersionContact2 = [textPersionContact2 stringByReplacingOccurrencesOfString:@"\'" withString:@""];
    
    NSString *strLot = [[RegistrationFormModel sharedInstance] lot];
    NSString *strFloor = [[RegistrationFormModel sharedInstance] floor];
    NSString *strRoom = [[RegistrationFormModel sharedInstance] room];
    NSString *strIDCard = [[RegistrationFormModel sharedInstance] isCard];
    NSString *strTaxCode = [[RegistrationFormModel sharedInstance] taxCode];
    NSString *strPhoneNumber1 = [[RegistrationFormModel sharedInstance] numberPhone1];
    NSString *strPhoneNumber2 = [[RegistrationFormModel sharedInstance] numberPhone2];
    //repalace @" "
    strPhoneNumber1 = [strPhoneNumber1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    strPhoneNumber2 = [strPhoneNumber2 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSString *strBirthday = [[RegistrationFormModel sharedInstance] date];
    
    //Check Deleted NumberHouse
    NSString *billToNumber = [[RegistrationFormModel sharedInstance] numberOfHouse];
    billToNumber = [billToNumber stringByTrimmingCharactersInSet:charSet];

    NSString *fullNameUser = [[RegistrationFormModel sharedInstance] name];
    //I'M(truong hop co dau nhay "\'") loi 203
    //fullNameUser = [fullNameUser stringByReplacingOccurrencesOfString:@"\'" withString:@""];
    
    NSString *email = [[RegistrationFormModel sharedInstance] email];
    
    KeyValueModel *typeHouse = [[RegistrationFormModel sharedInstance] typeOfHouse];
    KeyValueModel *selectedDistrict = [[RegistrationFormModel sharedInstance] county];
    KeyValueModel *selectedWard = [[RegistrationFormModel sharedInstance] ward];
    KeyValueModel *selectedStreet = [[RegistrationFormModel sharedInstance] street];
    KeyValueModel *selectedNameVilla = [[RegistrationFormModel sharedInstance] nameAparment];
    
    NSMutableArray *selectedLocalType = [[RegistrationFormModel sharedInstance] localType];
    
    PromotionModel *selectedPromotion = [[RegistrationFormModel sharedInstance] internetPromotion];
    KeyValueModel *selectedPackageIPTV = [[RegistrationFormModel sharedInstance] iptvPackageIPTV];
    
    KeyValueModel *selectedCombo = [[RegistrationFormModel sharedInstance] iptvCombo];
    KeyValueModel *selectedServicePack = [[RegistrationFormModel sharedInstance] internetService];
    KeyValueModel *selectedTypeHouse = [[RegistrationFormModel sharedInstance] typeOfHouse];
    KeyValueModel *selectedPosition = [[RegistrationFormModel sharedInstance] housePosition];
    KeyValueModel *selectedPhone1 = [[RegistrationFormModel sharedInstance] typePhone1];
    KeyValueModel *selectedPhone2 = [[RegistrationFormModel sharedInstance] typePhone2];
    KeyValueModel *selectedISP = [[RegistrationFormModel sharedInstance] isp];
    KeyValueModel *selectedTypeCustomer = [[RegistrationFormModel sharedInstance] typeCustomer];
    KeyValueModel *selectedPlaceCurrent = [[RegistrationFormModel sharedInstance] addressNow];
    KeyValueModel *selectedSolvency = [[RegistrationFormModel sharedInstance] solvency];
    KeyValueModel *selectedClientPartners = [[RegistrationFormModel sharedInstance] partnerCustomer];
    KeyValueModel *selectedLegal = [[RegistrationFormModel sharedInstance] entity];
    NSString *amountTotal = [NSString stringWithFormat:@"%d", [[RegistrationFormModel sharedInstance] totalAmount]] ?:@"0";
    KeyValueModel *selectedDeposit = [[RegistrationFormModel sharedInstance] deposit];
    KeyValueModel *selectedDepositBlackPoint = [[RegistrationFormModel sharedInstance] depositBlackPoint];
    KeyValueModel *selectedObjectCustomer = [[RegistrationFormModel sharedInstance] objectCustomer];
    CableStatus = [[RegistrationFormModel sharedInstance] cableStatus];
    InsCable = [[RegistrationFormModel sharedInstance] isIPTV];
    KeyValueModel *selectedCusType = [[RegistrationFormModel sharedInstance] type];
    TypeDeployment = [[RegistrationFormModel sharedInstance] iptvRequest];
    TypeDrillWall = [[RegistrationFormModel sharedInstance] iptvWallDrilling];
    KeyValueModel *selectedDeployment = [[RegistrationFormModel sharedInstance] iptvDeployment];
    PromotionModel *selectedPromotionIPTV = [[RegistrationFormModel sharedInstance] iptvPromotionIPTV];
    KeyValueModel *selectedPaymentType = [[RegistrationFormModel sharedInstance] paymentType];
    PromotionModel *selectedPromotionIPTVBoxOrder = [[RegistrationFormModel sharedInstance] iptvPromotionIPTV2];
    
    NSString *deviceBoxCount = [[RegistrationFormModel sharedInstance] iptvDeviceBoxCount];
    NSString *devicePLCCount = [[RegistrationFormModel sharedInstance] iptvDevicePLCCount];
    NSString *deviceSTBCount = [[RegistrationFormModel sharedInstance] iptvDeviceSTBCount];
    NSString *iptvChargeTimes = [[RegistrationFormModel sharedInstance] iptvChargeTimes];
    NSString *iptvFilmPlusChargeTime = [[RegistrationFormModel sharedInstance] iptvFilmPlusChargeTime];
    NSString *iptvFilmPlusPrepaidMonth = [[RegistrationFormModel sharedInstance] iptvFilmPlusPrepaidMonth];
    
    NSString *iptvFilmHotChargeTime = [[RegistrationFormModel sharedInstance] iptvFilmHotChargeTime];
    NSString *iptvFilmHotPrepaidMonth = [[RegistrationFormModel sharedInstance] iptvFilmHotPrepaidMonth];
    
    NSString *iptvFilmKPlusChargeTime = [[RegistrationFormModel sharedInstance] iptvFilmKPlusChargeTime];
    NSString *iptvFilmKPLusPrepaidMonth = [[RegistrationFormModel sharedInstance] iptvFilmKPlusPrepaidMonth];
    
    NSString *iptvFilmVTCChargeTime = [[RegistrationFormModel sharedInstance] iptvFilmVTCChargeTime];
    NSString *iptvFilmVTCPrepaidMonth = [[RegistrationFormModel sharedInstance] iptvFilmVTCPrepaidMonth];
    
    NSString *iptvFilmVTVChargeTime = [[RegistrationFormModel sharedInstance] iptvFilmVTVChargeTime];
    NSString *iptvFilmVTVPrepaidMonth = [[RegistrationFormModel sharedInstance] iptvFilmVTVPrepaidMonth];
    
    //vutt11
  
    /************ Check Note Address before save ************/
    NSString *noteAddress = [[RegistrationFormModel sharedInstance] noteAddress];
    //noteAddress = [noteAddress stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    //noteAddress = [noteAddress stringByReplacingOccurrencesOfString:@"\'" withString:@""];
    //check white space for note Address
  
    NSString *trimmedNoteAddress = [noteAddress stringByTrimmingCharactersInSet:charSet];
    noteAddress = trimmedNoteAddress;
    /**************************************************************/
    
    /************ Check Deleted Character specifili Address before save ************/
    NSString *addressIdentifyCard = [[RegistrationFormModel sharedInstance] address];
    addressIdentifyCard = [addressIdentifyCard stringByTrimmingCharactersInSet:charSet];
    
    //addressIdentifyCard = [addressIdentifyCard stringByReplacingOccurrencesOfString:@"\'" withString:@""];
    
    /************ Check Note DescriptionIBB before save ************/
    NSString *noteDescriptionIBB = [[RegistrationFormModel sharedInstance] notePlant];
    NSString *trimmedNoteDes = [noteDescriptionIBB stringByTrimmingCharactersInSet:charSet];
    
    noteDescriptionIBB = trimmedNoteDes;
    //noteDescriptionIBB = [noteDescriptionIBB stringByReplacingOccurrencesOfString:@"\'" withString:@""];
    
    strLot = [strLot stringByTrimmingCharactersInSet:charSet];
    strFloor = [strFloor stringByTrimmingCharactersInSet:charSet];
    strRoom = [strRoom stringByTrimmingCharactersInSet:charSet];
    
   //strLot = [strLot stringByReplacingOccurrencesOfString:@"\'" withString:@""];
    //strFloor = [strFloor stringByReplacingOccurrencesOfString:@"\'" withString:@""];
   // strRoom = [strRoom stringByReplacingOccurrencesOfString:@"\'" withString:@""];
    
    
    /**************************************************************/
    
    NSString *strIndoor = [[RegistrationFormModel sharedInstance] indoor];
    
    NSString *strOutdoor = [[RegistrationFormModel sharedInstance] outdoor];
    
    ///OTT and UploadImage
    int countBoxOTT = [[RegistrationFormModel sharedInstance] countFPTPlay];
    NSString *imageInfo = [[RegistrationFormModel sharedInstance] imgInfo];
    
    if([textPersionContact1 isEqualToString:@""] && ![textPersionContact2 isEqualToString:@""]){
        fullname = textPersionContact2;
        fullname1 = textPersionContact2;
    }
    if(![textPersionContact1 isEqualToString:@""] && [textPersionContact2 isEqualToString:@""]){
        fullname = textPersionContact1;
        fullname1 = textPersionContact1;
    }
    if(![textPersionContact1 isEqualToString:@""] && ![textPersionContact2 isEqualToString:@""]){
        fullname = textPersionContact1;
        fullname1 = textPersionContact2;
    }
    
    NSString *LocationName = [ShareData instance].currentUser.LocationName;
    if([typeHouse.Key isEqualToString:@"1"]){
        Address = [NSString stringWithFormat:@"%@ ,%@, %@, %@, %@", billToNumber, selectedStreet.Values, selectedWard.Values, selectedDistrict.Values, LocationName];
    }else if([typeHouse.Key isEqualToString:@"2"]) {
        Address = [NSString stringWithFormat:@"L.%@ ,T.%@, P.%@, %@, %@, %@, %@, %@", strLot, strFloor, strRoom, selectedNameVilla.Values, selectedStreet.Values, selectedWard.Values, selectedDistrict.Values, LocationName];
        
    }else {
        Address = [NSString stringWithFormat:@"%@ ,%@, %@, %@, %@", billToNumber, selectedStreet.Values, selectedWard.Values, selectedDistrict.Values, [ShareData instance].currentUser.LocationName];
    }
    //NSString *IPTVUseComboHR=@"2";
    
    NSString *IPTVUseComboHR=@"0";
    //vutt11
    IPTVUseComboHR = selectedCombo.Key;
    
    for ( KeyValueModel *keyVa in selectedLocalType ) {
        
        if([keyVa.Key isEqualToString:@"0"]){
            promotionid = selectedPromotion.PromotionId;
        
        }else if([keyVa.Key isEqualToString:@"1"]){
            iptvPackage = selectedPackageIPTV.Key;
            
        }else if([keyVa.Key isEqualToString:@"2"]){
            promotionid = selectedPromotion.PromotionId;
            iptvPackage = selectedPackageIPTV.Key;
            IPTVUseComboHR = selectedCombo.Key;
        }
        
    }
    
    NSString *addressPassport = addressIdentifyCard;
    if (addressPassport.length <= 0) {
        addressPassport = @"";
    }
    
    ShareData *shared = [ShareData instance];

    // Set parameter office 365 for Update registration API
    if (office365Total > 0) {
        NSDictionary *officeParameterDict = [[Office365Model sharedManager] setParamater];
        [dict setDictionary:officeParameterDict];
        
    } else {
        [dict setObject:@"" forKey:@"EmailAdmin"];
        [dict setObject:@"" forKey:@"DomainName"];
        [dict setObject:@"" forKey:@"TechName"];
        [dict setObject:@"" forKey:@"TechPhoneNumber"];
        [dict setObject:@"" forKey:@"TechEmail"];
        [dict setObject:@[] forKey:@"ListPackage"];
        
    }
    
    for ( KeyValueModel *keyVa in selectedLocalType ) {
        // Nếu là Office 365 only, truyền LocalType = 208, LocalTypeNam = @"Microsoft365".
        //FPT Play
        if ([keyVa.Key isEqualToString:@"4"]) {
            [dict setObject:@"210" forKey:@"LocalType"];
            NSString *strOTTBox = [NSString stringWithFormat:@"%d", (int)[[RegistrationFormModel sharedInstance] countFPTPlay]];
            [dict setObject:strOTTBox forKey:@"OTTBoxCount"];
            [dict setObject:[[RegistrationFormModel sharedInstance] listFPTPlayBoxOTT] forKey:@"lOtt"];
            
            break;

        }
        //Office 365 only
        if ([keyVa.Key isEqualToString:@"3"] && selectedLocalType.count==1) {
            [dict setObject:@"208" forKey:@"LocalType"];
            
        } else
    
        {
            [dict setObject:selectedServicePack.Key ?:@"0" forKey:@"LocalType"];
        }
    }

    [dict setObject:self.rc.ID       ?:@"0" forKey:@"ID"];
    [dict setObject:self.rc.ObjID    ?:@"0" forKey:@"ObjID"];
    [dict setObject:self.rc.Contract ?:@"" forKey:@"Contract"];
    [dict setObject:shared.currentUser.LocationID ?:@"0" forKey:@"LocationID"];
    [dict setObject:fullNameUser         ?:@"" forKey:@"FullName"];
    [dict setObject:fullname         ?:@"" forKey:@"Contact"];
    [dict setObject:shared.currentUser.LocationName ?:@"0" forKey:@"BillTo_City"];
    [dict setObject:selectedDistrict.Key    ?:@"0" forKey:@"BillTo_District"];
    [dict setObject:selectedWard.Key        ?:@"0" forKey:@"BillTo_Ward"];
    [dict setObject:selectedTypeHouse.Key   ?:@"0" forKey:@"TypeHouse"];
    [dict setObject:selectedStreet.Key      ?:@"0" forKey:@"BillTo_Street"];
    [dict setObject:strLot       ?:@"0" forKey:@"Lot"];
    [dict setObject:strFloor      ?:@"0" forKey:@"Floor"];
    [dict setObject:strRoom       ?:@"0" forKey:@"Room"];
    [dict setObject:selectedNameVilla.Key   ?:@"" forKey:@"NameVilla"];
    [dict setObject:selectedPosition.Key    ?:@"0" forKey:@"Position"];
    [dict setObject:billToNumber ?:@"0" forKey:@"BillTo_Number"];
    [dict setObject:Address ?:@"" forKey:@"Address"];
    [dict setObject:noteAddress   ?:@"" forKey:@"Note"];
    [dict setObject:strIDCard   ?:@"" forKey:@"Passport"];
    [dict setObject:strTaxCode    ?:@"" forKey:@"TaxId"];
    [dict setObject:strPhoneNumber1 ?:@"" forKey:@"Phone_1"];
    [dict setObject:strPhoneNumber2 ?:@"" forKey:@"Phone_2"];
    [dict setObject:selectedPhone1.Key  ?:@"0" forKey:@"Type_1"];
    [dict setObject:selectedPhone2.Key  ?:@"0" forKey:@"Type_2"];
    [dict setObject:fullname ?:@"" forKey:@"Contact_1"];
    [dict setObject:fullname1 ?:@"" forKey:@"Contact_2"];
    [dict setObject:selectedISP.Key ?:@"0" forKey:@"ISPType"];
    [dict setObject:selectedTypeCustomer.Key    ?:@"" forKey:@"CusTypeDetail"];
    [dict setObject:selectedPlaceCurrent.Key    ?:@"" forKey:@"CurrentHouse"];
    [dict setObject:selectedSolvency.Key        ?:@"0" forKey:@"PaymentAbility"];
    [dict setObject:selectedClientPartners.Key  ?:@"0" forKey:@"PartnerID"];
    [dict setObject:selectedLegal.Key ?:@"0" forKey:@"LegalEntity"];
    [dict setObject:self.rc.Supporter ?:@"" forKey:@"Supporter"];
    
    [dict setObject:promotionid       ?:@"0" forKey:@"PromotionID"];
    //TOTAL
    [dict setObject:[NSString stringWithFormat:@"%@", amountTotal] ?:@"0" forKey:@"Total"];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    [dict setObject:selectedDeposit.Key         ?:@"0" forKey:@"Deposit"];
    [dict setObject:selectedDepositBlackPoint.Key   ?:@"0" forKey:@"DepositBlackPoint"];
    [dict setObject:selectedObjectCustomer.Key      ?:@"0" forKey:@"ObjectType"];
    [dict setObject:CableStatus ?:@"0" forKey:@"CableStatus"];
    [dict setObject:InsCable    ?:@"0" forKey:@"InsCable"];
    [dict setObject:selectedCusType.Key ?:@"0" forKey:@"CusType"];
    [dict setObject:IPTVUseComboHR  ?:@"0" forKey:@"IPTVUseCombo"];
    [dict setObject:TypeDeployment  ?:@"0" forKey:@"IPTVRequestSetUp"];
    [dict setObject:TypeDrillWall   ?:@"0" forKey:@"IPTVRequestDrillWall"];
    [dict setObject:selectedDeployment.Key  ?:@"0" forKey:@"IPTVStatus"];
    [dict setObject:email      ?:@"" forKey:@"Email"];
    [dict setObject:selectedPromotionIPTV.PromotionId  ?:@"0" forKey:@"IPTVPromotionID"];
    [dict setObject:iptvPackage ?:@"0" forKey:@"IPTVPackage"];
    
    //so box
    [dict setObject:deviceBoxCount  ?:@"0" forKey:@"IPTVBoxCount"];
    //so PLC
    [dict setObject:devicePLCCount  ?:@"0" forKey:@"IPTVPLCCount"];
    //so STB
    [dict setObject:deviceSTBCount  ?:@"0" forKey:@"IPTVReturnSTBCount"];
    //self.txtIPTVChargeTimes.text
    [dict setObject:iptvChargeTimes ?:@"0" forKey:@"IPTVChargeTimes"];
    
    [dict setObject:@"0" ?:@"0" forKey:@"IPTVPrepaid"];
    [dict setObject:@"0" forKey:@"IPTVHBOPrepaidMonth"];
    [dict setObject:@"0" forKey:@"IPTVHBOChargeTimes"];
    
    [dict setObject:iptvFilmVTVPrepaidMonth   ?:@"0" forKey:@"IPTVVTVPrepaidMonth"];
    [dict setObject:iptvFilmVTVChargeTime    ?:@"0" forKey:@"IPTVVTVChargeTimes"];
    [dict setObject:iptvFilmKPLusPrepaidMonth ?:@"0" forKey:@"IPTVKPlusPrepaidMonth"];
    [dict setObject:iptvFilmKPlusChargeTime  ?:@"0" forKey:@"IPTVKPlusChargeTimes"];
    [dict setObject:iptvFilmVTCPrepaidMonth   ?:@"0" forKey:@"IPTVVTCPrepaidMonth"];
    [dict setObject:iptvFilmVTCChargeTime    ?:@"0" forKey:@"IPTVVTCChargeTimes"];
    
    //TOTAL
    [dict setObject:DeviceTotal ?:@"0" forKey:@"IPTVDeviceTotal"];
    [dict setObject:PrepaidTotal ?:@"0" forKey:@"IPTVPrepaidTotal"];
    
    //TOTAL
    [dict setObject:[NSString stringWithFormat:@"%d", amountIPTV] ?:@"0" forKey:@"IPTVTotal"];
    [dict setObject:[NSString stringWithFormat:@"%d", amountInternet] ?:@"0" forKey:@"InternetTotal"];
    
    [dict setObject:noteDescriptionIBB ?:@"" forKey:@"DescriptionIBB"];
  
    [dict setObject:self.rc.PotentialID  ?:@"" forKey:@"PotentialID"];
    [dict setObject:strIndoor  ?:@"0" forKey:@"InDoor"];
    [dict setObject:strOutdoor ?:@"0" forKey:@"OutDoor"];
    
    [dict setObject:selectedPaymentType.Key ?:@"" forKey:@"Payment"];
    [dict setObject:addressPassport  ?:@"" forKey:@"AddressPassport"];
    [dict setObject:strBirthday ?:@"" forKey:@"Birthday"];
    [dict setObject:iptvFilmPlusPrepaidMonth ?:@"0" forKey:@"IPTVFimPlusPrepaidMonth"];
    [dict setObject:iptvFilmPlusChargeTime  ?:@"0" forKey:@"IPTVFimPlusChargeTimes"];
    [dict setObject:iptvFilmHotPrepaidMonth  ?:@"0" forKey:@"IPTVFimHotPrepaidMonth"];
    [dict setObject:iptvFilmHotChargeTime   ?:@"0" forKey:@"IPTVFimHotChargeTimes"];
    [dict setObject:selectedPromotionIPTVBoxOrder.PromotionId ?:@"0" forKey:@"IPTVPromotionIDBoxOrder"];
    
    [dict setObject:selectedPromotionIPTV.Type ?:@"-1" forKey:@"IPTVPromotionType"];
    [dict setObject:selectedPromotionIPTVBoxOrder.Type ?:@"-1" forKey:@"IPTVPromotionTypeBoxOrder"];
    
    
    //OTT and UploadImage
    [dict setObject:[NSString stringWithFormat:@"%ld",(long)countBoxOTT] ?:@"0" forKey:@"OTTBoxCount"];
    [dict setObject:[NSString stringWithFormat:@"%@",imageInfo] forKey:@"ImageInfo"];

    //set ListDevice
    self.arrayListDevice = [[RegistrationFormModel sharedInstance]listDevicePackagesArray];
    [dict setObject:self.arrayListDevice forKey:@"ListDevice"];
    
    NSLog(@"%@", dict);
    
    return dict;
    
}

// this function not show info for user
- (void)updateHasShowDuplicateWithDupID:(NSString *)Id {
    
    ShareData *share = [ShareData instance];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    [dict setObject:share.currentUser.userName forKey:@"UserName"];
    [dict setObject:Id forKey:@"ID"];
    
    [share.appProxy updateHasShowDuplicate:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        NSString *resultMsg = [[result objectAtIndex:0] objectForKey:@"Result"];
        NSLog(@"-------Update has show duplicate status: %@", resultMsg);
        return ;
        
    } errorHandler:^(NSError *error) {
        NSLog(@"-------Update has show duplicate status: %@", [error description]);
        
    }];
    
}

//message
- (void)showCheckReg:(NSString*)message {
    NSString *messageAlert = StringFormat(@"%@ \nBạn có muốn tiếp tục cập nhật không? ",message);
    updateCheck = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:messageAlert delegate:self cancelButtonTitle:@"Không" otherButtonTitles:@"Có", nil];
    [updateCheck show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView == updateCheck) {
        switch (buttonIndex) {
            case 0:
//                [self hideMBProcess];
                [self.delegate hideMBProcessListRegis];
                break;
            case 1: {
                if (updateCheck.tag == 1) {
                    //    [self checkRegistrationAPI];
                    [self checkRegistration2];
                }
                if (updateCheck.tag == 2) {
                    [self UpdateRegistrationGo];
                }
            }
                break;
            default:
                break;
        }
    }
}

//MARK: -UITABLEVIEWDELEGATE

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 60;
}

//MARK: -UITABLEVIEWDATASOURCE

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return button + 1;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case depositBlackPoint: {
            
            AniDropDownCellTableViewCell *cell = (AniDropDownCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellDropDown" forIndexPath:indexPath];
            NSString *str = [[RegistrationFormModel sharedInstance] depositBlackPoint].Values ?:@"";
            cell.delegate = self;
            if ( ![str isEqualToString:@""] ) {
                cell.btnChoose.selected = YES;
                [cell configDropDownCell:@"Đặt cọc điểm đen" titleButton:str kTag:depositBlackPoint];
            } else {
                cell.btnChoose.selected = YES;
                [cell configDropDownCell:@"Đặt cọc điểm đen" titleButton:@"0" kTag:depositBlackPoint];
            }
            return cell;
            
        }
            
        case depositSubscribers: {
            
            AniDropDownCellTableViewCell *cell = (AniDropDownCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellDropDown" forIndexPath:indexPath];
            NSString *str = [[RegistrationFormModel sharedInstance] deposit].Values ?:@"";
            cell.delegate = self;
            if ( ![str isEqualToString:@""] ) {
                cell.btnChoose.selected = YES;
                [cell configDropDownCell:@"Đặt cọc thuê bao" titleButton:str kTag:depositSubscribers];
            } else {
                cell.btnChoose.selected = NO;
                [cell configDropDownCell:@"Đặt cọc thuê bao" titleButton:@"Đặt cọc thuê bao" kTag:depositSubscribers];
            }
            
            return cell;
            
        }
            
        case office365: {
            
            AniTextFieldCell *cell = (AniTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTxtF" forIndexPath:indexPath];
            
            if ( office365Total == 0) {
                
                cell.txtFValue.text = @"0";
                cell.txtFValue.enabled = FALSE;
                [cell configCell:@"Office 365" placeHolderTextField:@"Office 365"];
               
            } else {
                cell.txtFValue.text = [self formatIntToPrice:office365Total];
                cell.txtFValue.enabled = FALSE;
                [cell configCell:@"Office 365" placeHolderTextField:@"Office 365"];
                
            }
            
            return cell;
           
        }
            
        case iptv: {
            
            AniTextFieldCell *cell = (AniTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTxtF" forIndexPath:indexPath];
            
            NSString *str = [NSString stringWithFormat:@"%d",[[RegistrationFormModel sharedInstance] totalIPTV]] ?:@"0";
            if ( [str isEqualToString:@"0"] ) {
                cell.txtFValue.text = @"0";
                cell.txtFValue.enabled = FALSE;
                [cell configCell:@"IPTV" placeHolderTextField:@"IPTV"];
            } else {
                cell.txtFValue.text = [self formatIntToPrice:[[RegistrationFormModel sharedInstance] totalIPTV]];
                cell.txtFValue.enabled = FALSE;
                [cell configCell:@"IPTV" placeHolderTextField:@"IPTV"];
            }
            
            return cell;
        }
            
        case internet: {
            
            AniTextFieldCell *cell = (AniTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTxtF" forIndexPath:indexPath];
            
            NSString *str = [NSString stringWithFormat:@"%d",[[RegistrationFormModel sharedInstance] totalInternet]] ?:@"";
            if ( [str isEqual:@""] ) {
                
                cell.txtFValue.text = @"0";
                cell.txtFValue.enabled = FALSE;
                [cell configCell:@"Internet" placeHolderTextField:@"Internet"];
                
            } else {
                
                cell.txtFValue.text = [self formatIntToPrice:[[RegistrationFormModel sharedInstance] totalInternet]];
                cell.txtFValue.enabled = FALSE;
                [cell configCell:@"Internet" placeHolderTextField:@"Internet"];
                
            }
            
            return cell;
            
        }
            
        case fptPlay: {
            
            AniTextFieldCell *cell = (AniTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTxtF" forIndexPath:indexPath];
            
            NSString *str = [NSString stringWithFormat:@"%d",[[RegistrationFormModel sharedInstance] totalFPTPlay]] ?:@"";
            if ( [str isEqual:@""] ) {
                
                cell.txtFValue.text = @"0";
                cell.txtFValue.enabled = FALSE;
                [cell configCell:@"FPTPlay Box" placeHolderTextField:@"FPTPlay Box"];
                
                
            } else {
                
                cell.txtFValue.text = [self formatIntToPrice:[[RegistrationFormModel sharedInstance] totalFPTPlay]];
                cell.txtFValue.enabled = FALSE;
                [cell configCell:@"FPTPlay Box" placeHolderTextField:@"FPTPlay Box"];
                
            }
            
            return cell;
            
        }
            //Vutt11 add total Device
        case device: {
            
            AniTextFieldCell *cell = (AniTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTxtF" forIndexPath:indexPath];
            
            NSString *str = [NSString stringWithFormat:@"%@",[[RegistrationFormModel sharedInstance] totalDevice]]?:@"";
            
            if ([str isEqualToString:@"(null)"] || [str isEqualToString:@""]) {
                cell.txtFValue.text = @"0";
                cell.txtFValue.enabled = FALSE;
                [cell configCell:@"Tiền bán thiết bị" placeHolderTextField:@"Tiền bán thiết bị"];
                
            } else {
                
                cell.txtFValue.text = [self formatIntToPrice:[[[RegistrationFormModel sharedInstance] totalDevice] intValue]];
                cell.txtFValue.enabled = FALSE;
                [cell configCell:@"Tiền bán thiết bị" placeHolderTextField:@"Tiền bán thiết bị"];
            }
            
            return cell;
        }
            
        case total: {
            
            AniTextFieldCell *cell = (AniTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTxtF" forIndexPath:indexPath];
            
            NSString *str = [NSString stringWithFormat:@"%d",[[RegistrationFormModel sharedInstance] totalAmount]] ?:@"";
            
            if ( [str isEqual:@""] ) {
                cell.txtFValue.text = @"0";
                [cell configCell:@"Tổng tiền" placeHolderTextField:@"Tổng Tiền"];
            } else {
                cell.txtFValue.text = [self formatIntToPrice:[[RegistrationFormModel sharedInstance] totalAmount]];
                [cell configCell:@"Tổng tiền" placeHolderTextField:@"Tổng Tiền"];
            }
            
            return cell;
        }
        case formOfPayment: {
            
            CacheDropDownCell *cell = (CacheDropDownCell *)[tableView dequeueReusableCellWithIdentifier:@"CacheDropDownCell" forIndexPath:indexPath];
            NSString *str = [[RegistrationFormModel sharedInstance] paymentType].Values ?:@"";
            cell.delegate = self;
            if ( ![str isEqualToString:@""] ) {
                cell.btnChoose.selected = YES;
                [cell configDropDownCell:@"Hình thức thanh toán" titleButton:str kTag:formOfPayment];
            } else {
                [cell configDropDownCell:@"Hình thức thanh toán" titleButton:@"Hình thức thanh toán" kTag:formOfPayment];
            }
            
            return cell;
            
        }
            
        case indoor: {
            
            AniTextFieldCell *cell = (AniTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTxtF" forIndexPath:indexPath];
            
            cell.delegate = self;
            cell.txtFValue.enabled = TRUE;
            cell.txtFValue.text = [[RegistrationFormModel sharedInstance] indoor] ?:@"";
            [cell configCell:@"Indoor" placeHolderTextField:@"Indoor"];
            [cell configCellWithTag:indoor];
            [cell.txtFValue setKeyboardType:UIKeyboardTypeNumberPad];
            return cell;
            
        }
            
        case outdoor: {
            
            AniTextFieldCell *cell = (AniTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTxtF" forIndexPath:indexPath];
            
            cell.delegate = self;
            cell.txtFValue.enabled = TRUE;
            cell.txtFValue.text = [[RegistrationFormModel sharedInstance] outdoor] ?:@"";
            [cell configCell:@"Outdoor" placeHolderTextField:@"Outdoor"];
            [cell configCellWithTag:outdoor];
            [cell.txtFValue setKeyboardType:UIKeyboardTypeNumberPad];
            return cell;
        }
            
        case button: {
            
            ButtonNextCell *cell = (ButtonNextCell *)[tableView dequeueReusableCellWithIdentifier:@"cellButton" forIndexPath:indexPath];
            cell.delegate = self;
            [cell changeTitleButtonNext:@"Cập Nhật"];
            return cell;
        }
        default: {
            
            AniTextFieldCell *cell = (AniTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTxtF" forIndexPath:indexPath];
            
            cell.lblTitle.text = @"Họ tên khách hàng";
            cell.txtFValue.placeholder = @"Nhập họ và tên";
            return cell;
        }
    }
    
}

//MARK: -FORMAT INT TO PRICE
-(NSString *)formatIntToPrice:(int)price {
    
    NSNumberFormatter *format = [[NSNumberFormatter alloc] init];
    format.numberStyle = NSNumberFormatterDecimalStyle;
    format.groupingSeparator = @",";
    
    NSNumber *number = [NSNumber numberWithInt:price];
    
    NSString *display = [format stringFromNumber:number];
    
    return display;
}

//MARK: -ANIDROPDOWNCELLTABLEVIEWCELLDELEGATE
-(void)dropDownButton:(id)sender kTag:(NSInteger)tag {
    
    switch (tag) {
        case depositBlackPoint:{
            [self setupDropDownView:depositBlackPoint];
            isSelectedDepositBlackPoint = YES;
            break;
        }
        case depositSubscribers:{
            [self setupDropDownView:depositSubscribers];
            isSelectedDeposit = YES;
            break;
        }
        case formOfPayment:{
            [self setupDropDownView:formOfPayment];
            isSelectedPayment = YES;
            break;
        }
        default:
            break;
    }
    
}

-(void)freshCompleted:(NSInteger)tag {
    
    switch (tag) {
        case formOfPayment:
            [self loadCachePaymentType:@"0"];
            break;
        default:
            break;
    }
    
}

/****************************/
//MARK: - UIPOPOVERLISTVIEW
/****************************/
- (void)setupDropDownView: (NSInteger) tag {
    [self.view endEditing:YES];
    
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.tag = tag;
    poplistview.listView.scrollEnabled = TRUE;
    switch (tag) {
        case depositBlackPoint:
            [poplistview setTitle:NSLocalizedString(@"Đặt cọc điểm đen", @"")];
            break;
        case depositSubscribers:
            [poplistview setTitle:NSLocalizedString(@"Đặt cọc thuê bao", @"")];
            break;
        case formOfPayment:
            [poplistview setTitle:NSLocalizedString(@"Hình thức thanh toán", @"")];
            break;
        default:
            break;
    }
    
    [poplistview show];
    
}

- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

#pragma mark - UIPopoverListViewDataSource

- (void)setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell tag:(NSInteger)tag{
    KeyValueModel *model;
   // PromotionModel *promotionmodel;
    cell.textLabel.numberOfLines = 0;
    [cell.textLabel sizeToFit];
    switch (tag) {
        case depositBlackPoint:
            model = [self.arrDepositBlackPoint objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case depositSubscribers:
            model = [self.arrDeposit objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case formOfPayment:
            model = [self.arrPaymentType objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        default:
            break;
            
    }
    
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView cellForIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row cell:cell tag:popoverListView.tag];
    cell.textLabel.font = [UIFont fontWithName:@"Arial" size:14];
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = 0;
    switch (popoverListView.tag) {
        case depositBlackPoint: {
            count = self.arrDepositBlackPoint.count;
            break;
        }
        case depositSubscribers: {
            count = self.arrDeposit.count;
            break;
        }
        case formOfPayment: {
            count = self.arrPaymentType.count;
            break;
        }
            
    }
    return count;
    
}

#pragma mark - UIPopoverListViewDelegate

- (void) didDropDownSelected:(NSInteger)row tag:(NSInteger)tag {
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    switch (tag) {
        case depositBlackPoint:{
            NSLog(@"%ld",(long)row);
            [[RegistrationFormModel sharedInstance] setDepositBlackPoint:[self.arrDepositBlackPoint objectAtIndex:row]];
            
            [[RegistrationFormModel sharedInstance] setDeposit:[self.arrDeposit objectAtIndex:0]];
            
            [self Total];
            
            [myTableView reloadData];
            break;
        }
        case depositSubscribers:{
            NSLog(@"%ld",(long)row);
            [[RegistrationFormModel sharedInstance] setDeposit:[self.arrDeposit objectAtIndex:row]];
            
            [[RegistrationFormModel sharedInstance] setDepositBlackPoint:[self.arrDepositBlackPoint objectAtIndex:0]];
            
            [self Total];
            
            [myTableView reloadData];
            break;
        }
        case formOfPayment:{
            NSLog(@"%ld",(long)row);
            [[RegistrationFormModel sharedInstance] setPaymentType:[self.arrPaymentType objectAtIndex:row]];
            [myTableView reloadData];
            break;
        }
        default:
            break;
    }
    
}

#pragma mark - UIPopoverListViewDelegate

- (void)popoverListView:(UIPopoverListView *)popoverListView didSelectIndexPath:(NSIndexPath *)indexPath {
    [self didDropDownSelected:indexPath.row tag:popoverListView.tag];
    [popoverListView dismiss];
    
}

//MARK: -BUTTONCELLDELEGATE
-(void)pressedButton:(id)sender kTag:(NSInteger)tag ktagButton:(NSInteger)tagButton {
    
    switch (tagButton) {
        case 1:{
            //save
        
            [self CheckRegistration];
            
            break;
        }
        case 2:{
            //back
            
            [delegate scrollToView:4 tagButton:2];
            break;
        }
        default:
            break;
    }
}

- (void)CheckRegistration {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    //vutt11
    //NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];

    NSString *textPersionContact1 = [[RegistrationFormModel sharedInstance] contact1] ?:@"";
    NSString *textPersionContact2 = [[RegistrationFormModel sharedInstance] contact2] ?:@"";
    NSString *strIDCard = [[RegistrationFormModel sharedInstance] isCard] ?:@"";
    NSString *strTaxCode = [[RegistrationFormModel sharedInstance] taxCode] ?:@"";
    NSString *strPhoneNumber1 = [[RegistrationFormModel sharedInstance] numberPhone1] ?:@"";
    NSString *strBirthday = [[RegistrationFormModel sharedInstance] date] ?:@"";
    NSString *addressIdentifyCard = [[RegistrationFormModel sharedInstance] address] ?:@"";
    NSString *billToNumber = [[RegistrationFormModel sharedInstance] numberOfHouse] ?:@"";
    
    NSString *fullNameUser = [[RegistrationFormModel sharedInstance] name] ?:@"";
    NSString *email = [[RegistrationFormModel sharedInstance] email] ?:@"";
    
    KeyValueModel *selectedDistrict = [[RegistrationFormModel sharedInstance] county];
    KeyValueModel *selectedWard = [[RegistrationFormModel sharedInstance] ward];
    KeyValueModel *selectedStreet = [[RegistrationFormModel sharedInstance] street];
    KeyValueModel *selectedNameVilla = [[RegistrationFormModel sharedInstance] nameAparment];
    NSMutableArray *selectedLocalType = [[RegistrationFormModel sharedInstance] localType];
    //InternetPromotion
    PromotionModel *selectedPromotion = [[RegistrationFormModel sharedInstance] internetPromotion];
    //InternetService
    KeyValueModel *internetService = [[RegistrationFormModel sharedInstance] internetService];
    KeyValueModel *selectedCombo = [[RegistrationFormModel sharedInstance] iptvCombo];
    KeyValueModel *selectedTypeHouse = [[RegistrationFormModel sharedInstance] typeOfHouse];
    KeyValueModel *selectedPosition = [[RegistrationFormModel sharedInstance] housePosition];
    KeyValueModel *selectedTypeCustomer = [[RegistrationFormModel sharedInstance] typeCustomer];
    CableStatus = [[RegistrationFormModel sharedInstance] cableStatus];
    InsCable = [[RegistrationFormModel sharedInstance] isIPTV];
    TypeDeployment = [[RegistrationFormModel sharedInstance] iptvRequest];
    TypeDrillWall = [[RegistrationFormModel sharedInstance] iptvWallDrilling];
    PromotionModel *selectedPromotionIPTV = [[RegistrationFormModel sharedInstance] iptvPromotionIPTV];
    // NSString *iptvPackage = [[RegistrationFormModel sharedInstance] iptv]
    KeyValueModel *selectedPaymentType = [[RegistrationFormModel sharedInstance] paymentType];
    KeyValueModel *selectedType =  [[RegistrationFormModel sharedInstance] type];
    
    KeyValueModel *selectedObjectCutomer = [[RegistrationFormModel sharedInstance] objectCustomer];
    KeyValueModel *selectedISP = [[RegistrationFormModel sharedInstance] isp];
    KeyValueModel *selectedAddressNow = [[RegistrationFormModel sharedInstance] addressNow];
    KeyValueModel *selectedSolvency = [[RegistrationFormModel sharedInstance] solvency];
    KeyValueModel *selectedPartnetCustomer = [[RegistrationFormModel sharedInstance] partnerCustomer];
    KeyValueModel *selectedEntity = [[RegistrationFormModel sharedInstance] entity];
    

    //kiem tra chua chon CLKM cua thiet bi
     self.arrayListDevice = [[RegistrationFormModel sharedInstance] listDevicePackagesArray];
    if (![self.arrayListDevice isEqual:[NSNull null]]) {
        for (int i = 0; i < self.arrayListDevice.count; i++) {
            NSString *promotionDeviceText = [self.arrayListDevice[i] valueForKey:@"PromotionDeviceText"];
            if ([promotionDeviceText isEqualToString:@"Vui lòng chọn CLKM"]) {
                [delegate scrollToViewValidate:36];
                return;
            }
            //return;
        }
    }
   
    if ([selectedType.Key  isEqual: @""] || selectedType.Key == nil) {
        [delegate scrollToViewValidate:-1];
        return;
    }
    
    if ([selectedTypeCustomer.Key isEqual:@""] || selectedTypeCustomer.Key == nil) {
        [delegate scrollToViewValidate:0];
        return;
    }
    
    if([fullNameUser isEqual:@""] || fullNameUser == nil){
        [delegate scrollToViewValidate:1];
        return;
    }
    
    if(![selectedTypeCustomer.Key isEqualToString:@"10"]){
        if([strIDCard isEqualToString:@""] || strIDCard == nil){
            [delegate scrollToViewValidate:2];
            return;
        }
        
        int len = (int)[[strIDCard stringByReplacingOccurrencesOfString:@" " withString:@""] length];
        if(len < 9 || len > 15){;
            [delegate scrollToViewValidate:3];
            return;
        }
        
        if (![[strIDCard stringByTrimmingCharactersInSet:[NSCharacterSet alphanumericCharacterSet]] isEqualToString:@""]) {
            [delegate scrollToViewValidate:4];
            return;
        }
        // check address identify card not empty
        if ([addressIdentifyCard isEqualToString:@""] || addressIdentifyCard == nil) {
            [delegate scrollToViewValidate:5];
            return;
        }
    }
    // check birthday choosed yes or no ?
    if ([strBirthday isEqualToString:@"Chọn Ngày Sinh"] || [strBirthday isEqualToString:@""] || strBirthday == nil ) {
        [delegate scrollToViewValidate:6];
        return;
    }
    
    // Obligatory enter tax code when customer type is company
    if([selectedTypeCustomer.Key isEqualToString:@"10"] || selectedTypeCustomer.Key == nil){
        if([strTaxCode isEqualToString:@""]){
            [delegate scrollToViewValidate:7];
            return;
        }
    }
    
    if([strPhoneNumber1 isEqualToString:@""] || strPhoneNumber1 == nil){

        [delegate scrollToViewValidate:8];

        return;
        
    }
    //else {
//        int len = (int)[strPhoneNumber1 length];
//        if(len < 10 || len > 11){
//            [delegate scrollToViewValidate:9];
//            return;
//        }else {
//            NSString* num = [strPhoneNumber1 substringToIndex:1];
//            if(![num isEqualToString:@"0"]){
//                [delegate scrollToViewValidate:9];
//                return;
//            }
//        }
//    }
    
    if([textPersionContact1 isEqualToString:@""]){
        if( [textPersionContact2 isEqualToString:@""]){
            [delegate scrollToViewValidate:10];
            return;
        }
    }
    
    if(![email isEqualToString:@""]){
        if(![self validateEmail:email]){
            [delegate scrollToViewValidate:11];
            return;
        }
    }
    
    if ([selectedDistrict.Key isEqualToString:@""] || selectedDistrict.Key == nil || [selectedDistrict.Key  isEqual: @"0"]) {
        [delegate scrollToViewValidate:12];
        return;
    }
    if ([selectedWard.Key isEqualToString:@""] || selectedWard.Key == nil) {
        [delegate scrollToViewValidate:13];
        return;
    }
    
    if([selectedStreet.Key isEqualToString:@""] || selectedStreet.Key == nil){
        [delegate scrollToViewValidate:14];
        return;
    }
    
    if([selectedTypeHouse.Key isEqualToString:@"1"]){
        if([billToNumber isEqualToString:@""]){
            [delegate scrollToViewValidate:15];
            return;
        }
    }
    
    if([selectedTypeHouse.Key isEqualToString:@"2"]){
        if([selectedNameVilla.Key isEqualToString:@""] || selectedNameVilla.Key == nil){
//            [self showAlertBox:@"Thông Báo" message:@"Vui lòng chọn chung cư"];
            [delegate scrollToViewValidate:16];
            return;
        }
    }
    
    if ([selectedTypeHouse.Key isEqualToString:@"3"]) {
        if([billToNumber isEqualToString:@""]){
//            [self showAlertBox:@"Thông Báo" message:@"Vui lòng điền số nhà"];
            [delegate scrollToViewValidate:17];
//            [self.txtBilltoNumber becomeFirstResponder];
            return;
        }
        if ([selectedPosition.Key isEqualToString:@"0"]) {
//            [self showAlertBox:@"Thông Báo" message:@"Vui lòng chọn vị trí nhà"];
            [delegate scrollToViewValidate:18];
            return;
        }
    }
    
    if (selectedDistrict.Values.length < 1 || [selectedDistrict.Key  isEqual: @"0"]) {
//        [self showAlertBox:@"Thông Báo" message:@"Quận/Huyện không hợp lệ"];
        [delegate scrollToViewValidate:19];
        return;
    }
    
    if (selectedWard.Values.length < 1) {
//        [self showAlertBox:@"Thông Báo" message:@"Phường/Xã không hợp lệ"];
        [delegate scrollToViewValidate:20];
        return;
    }
    
    if (selectedStreet.Values.length < 3) {
//        [self showAlertBox:@"Thông Báo" message:@"Chưa nhập tên đường"];
        [delegate scrollToViewValidate:21];
        return;
    }
    
    
    if ([selectedObjectCutomer.Key  isEqual: @""] || selectedObjectCutomer.Key == nil
        ) {
        [delegate scrollToViewValidate:26];
        return;
    } else {
        if ([selectedObjectCutomer.Key isEqual:@"2"]) {
            if ([selectedISP.Key isEqual:@""] || selectedISP.Key == nil) {
                [delegate scrollToViewValidate:27];
                return;
            }
        }
    }
    
    if ([selectedAddressNow.Key isEqual:@""] || selectedAddressNow.Key == nil) {
        [delegate scrollToViewValidate:28];
        return;
    }
    
    if ([selectedSolvency.Key isEqual:@""] || selectedSolvency.Key == nil) {
        [delegate scrollToViewValidate:29];
        return;
    }
    
    if ([selectedPartnetCustomer.Key isEqual:@""] || selectedPartnetCustomer.Key == nil) {
        [delegate scrollToViewValidate:30];
        return;
    }
    
    if ([selectedEntity.Key isEqual:@""] || selectedEntity.Key == nil) {
        [delegate scrollToViewValidate:31];
        return;
    }
    
    // check payment type choose yes ? or
    if ([selectedPaymentType.Key isEqualToString:@"0"] || [selectedPaymentType.Values isEqualToString:@"[Chọn hình thức thanh toán]"] || selectedPaymentType.Key == nil) {
//        [self showAlertBox:@"Thông báo" message:@"Vui lòng chọn hình thức thanh toán"];
        [delegate scrollToViewValidate:22];
        return;
    }
    
    int totalOffice = [[RegistrationFormModel sharedInstance] totalOffice365];
    //StevenNguyen
    for (KeyValueModel *keyVa in selectedLocalType) {
        // Check Registed Office 365
        if ([keyVa.Key isEqualToString:@"3"]) {
            if (totalOffice <= 0) {

                [delegate scrollToViewValidate:32];
                return;
            }
        }
        
        if ([keyVa.Key isEqualToString:@"5"]) {
            
            if (self.arrayListDevice.count == 0) {
                [delegate scrollToViewValidate:37];
                return;
                
            }
            
        }
        /*
         * check seleted promotion internet if local type not IPTV and local type not FPTPlay Box
         */
        if ([keyVa.Key isEqualToString:@"0"]) {
            NSString *stringNiLL = selectedPromotion.PromotionId;
            //[selectedPromotion.PromotionId isEqualToString:@"0"] ||
            if ([selectedPromotion.PromotionId isEqualToString:@"0"] || stringNiLL == nil) {
                [delegate scrollToViewValidate:23];
                return;
            }
            
            //Vu fix (Trường hợp có câu lệnh khuyến mãi Internet (InternetPromotion) nhưng Gói dịch vụ của Internet không có)
            //[selectedType.Key  isEqual: @""] || selectedType.Key == nil
            
            if ( !(selectedPromotion.PromotionId == nil)) {
                if ([internetService.Key isEqualToString:@"0"]||[internetService.Key isEqualToString:@""] ||internetService.Key == nil) {
                    [delegate scrollToViewValidate:35];
                    return;
                }
            }
        }
        
        //kiem tra IPTV co chon cau lenh khuyen mai 1 chua?
        if ([keyVa.Key isEqualToString:@"1"]) {
            if ([selectedPromotionIPTV.PromotionId isEqualToString:@"0"] || selectedPromotionIPTV.PromotionId == nil) {
                [delegate scrollToViewValidate:24];
                return;
            }
        }
        
        //kiem tra IPTV co chon cau lenh khuyen mai 2 chua?
        PromotionModel *selectedPromotionIPTV2 = [[RegistrationFormModel sharedInstance] iptvPromotionIPTV2];
        NSString *countBoxIPTV = [[RegistrationFormModel sharedInstance] iptvDeviceBoxCount] ?: @"0";
        int numberCountBoxIPTV = [countBoxIPTV intValue];
        if ([keyVa.Key isEqualToString:@"1"]) {
            if ([selectedPromotionIPTV.PromotionId isEqualToString:@"0"] || selectedPromotionIPTV.PromotionId == nil) {
                [delegate scrollToViewValidate:24];
                return;
            }
            
            if (([selectedPromotionIPTV2.PromotionId isEqualToString:@"0"] || selectedPromotionIPTV2.PromotionId == nil) && numberCountBoxIPTV >= 2) {
                [delegate scrollToViewValidate:34];
                return;
            }
            
        }
        
        // check selected IPTVCombo if local type is net & IPTV
        //[keyVa.Key isEqualToString:@"2"]
        //vutt11
        if ([selectedCombo.Key isEqualToString:@"0"]) {
            if ([selectedCombo.Key isEqualToString:@"0"]) {
                [delegate scrollToViewValidate:25];
                return;
            }
        }
        //check selected FPTPlayBOx
        if ([keyVa.Key isEqualToString:@"4" ]) {
            
              NSString *strBoxFPTPlay = [NSString stringWithFormat:@"%ld",(long)[[RegistrationFormModel sharedInstance] countFPTPlay] ];
            if ([strBoxFPTPlay isEqualToString:@"0"]) {
//                [delegate scrollToViewValidate:33];
//                return;
            }
            
            NSDictionary *dicBoxFPTPlay = [[RegistrationFormModel sharedInstance] listFPTPlayBoxOTT];
            
            NSMutableArray *arrDicBox = [dicBoxFPTPlay valueForKey:@"listDeviceOTT"];
            NSMutableArray *arrDicmac = [dicBoxFPTPlay valueForKey:@"listMACOTT"];
            
            if (arrDicBox.count <= 0) {//Chua Chon Box
                [delegate scrollToViewValidate:409];
                return;
            }
            
//            NSPredicate *pre = [NSPredicate predicateWithFormat:@"SELF['PromotionID'] == %@",@"-1"];
//            NSArray *arrPredicated = [arrDicBox filteredArrayUsingPredicate:pre];
//            
//            if (arrPredicated.count > 0) {//Chua Chon Promotion
//                [delegate scrollToViewValidate:408];
//                return;
//            }
            
            NSInteger numberBox = 0;
            for (NSDictionary *dicBox in arrDicBox) {
                
//                NSString *idPro = [dicBox valueForKey:@"PromotionID"];
//                if ([idPro isEqualToString:@"-1"]) {
//                    [delegate scrollToViewValidate:408];
//                    return;
//                }
                
                NSString *strCountOTT = [dicBox valueForKey:@"OTTCount"];
                NSInteger num = [strCountOTT integerValue];
                numberBox += num;
            }
            
            [[RegistrationFormModel sharedInstance] setCountFPTPlay:numberBox];
            
            NSInteger numberMac = arrDicmac.count;
            
            if (numberBox > numberMac) { //Box Lon Hon MAC
                [delegate scrollToViewValidate:411];
                return;
            } else if (numberBox < numberMac) { //MAC Lon Hon Box
                [delegate scrollToViewValidate:412];
                return;
            } else {
                
            }
            
        }
    }
    
     [self showActionSheetUpdate];
}

//get Total FPT Play
- (void)getToTalOTT {
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    
    NSString *stringCount = [NSString stringWithFormat:@"%ld",(long)[[RegistrationFormModel sharedInstance] countFPTPlay]];
    
    NSDictionary *dicDevice = [[RegistrationFormModel sharedInstance] listFPTPlayBoxOTT];
    
    NSArray *arrDicDevice = dicDevice[@"listDeviceOTT"];
    
    //[self showMBProcess];
    ShareData *shared = [ShareData instance];
    
    [shared.registrationFormProxy GetToTalOTT:shared.currentUser.userName OTTBoxCount:@"" dicJSON:arrDicDevice Completehander:^(id result, NSString *errorCode, NSString *message) {
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self.delegate logOutWhenEndSession];
            
            [self.delegate hideMBProcessListRegis];
            return;
        }
        //vutt11
        if ([message isEqualToString:@"Error Service: Unauthorized"]) {
            
            [self.delegate hideMBProcessListRegis];
            return;
        }
        
        NSDictionary *dict = result;
        amountOTT = [StringFormat(@"%@",[dict objectForKey:@"Amount"])intValue];
        
        [[RegistrationFormModel sharedInstance] setTotalFPTPlay:amountOTT];
        
        [self Total];
        
        [self.delegate hideMBProcessListRegis];
        
    } errorHandler:^(NSError *error) {
        
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@- %@",@"GetToTalOTT",[error localizedDescription]]];
        [self.delegate hideMBProcessListRegis];
        
    }];
    
//    [shared.registrationFormProxy GetToTalOTT:shared.currentUser.userName OTTBoxCount:stringCount Completehander:^(id result, NSString *errorCode, NSString *message) {
//        
//        if ([message isEqualToString:@"het phien lam viec"]) {
//            [self ShowAlertErrorSession];
//            [self.delegate logOutWhenEndSession];
//
//            [self.delegate hideMBProcessListRegis];
//            return;
//        }
//        //vutt11
//        if ([message isEqualToString:@"Error Service: Unauthorized"]) {
//
//            [self.delegate hideMBProcessListRegis];
//            return;
//        }
//        
//        NSDictionary *dict = result;
//        amountOTT = [StringFormat(@"%@",[dict objectForKey:@"Amount"])intValue];
//        
//        [[RegistrationFormModel sharedInstance] setTotalFPTPlay:amountOTT];
//    
//        [self Total];
//
//        [self.delegate hideMBProcessListRegis];
//        
//        
//    } errorHandler:^(NSError *error) {
//        
//        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@- %@",@"GetToTalOTT",[error localizedDescription]]];
//        [self.delegate hideMBProcessListRegis];
//        
//    }];
    
}

// Get total IPTV amount
- (void)getTotalIPTV {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }

    [self.delegate showMBProcessListRegis];
    
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    
    KeyValueModel *selectedPackageIPTV = [[RegistrationFormModel sharedInstance] iptvPackageIPTV];
    CableStatus = [[RegistrationFormModel sharedInstance] cableStatus];
    InsCable = [[RegistrationFormModel sharedInstance] isIPTV];
    TypeDeployment = [[RegistrationFormModel sharedInstance] iptvRequest];
    TypeDrillWall = [[RegistrationFormModel sharedInstance] iptvWallDrilling];
    PromotionModel *selectedPromotionIPTV = [[RegistrationFormModel sharedInstance] iptvPromotionIPTV];
    //    NSString *iptvPackage = [[RegistrationFormModel sharedInstance] iptv]
    PromotionModel *selectedPromotionIPTVBoxOrder = [[RegistrationFormModel sharedInstance] iptvPromotionIPTV2];
    
    NSString *deviceBoxCount = [[RegistrationFormModel sharedInstance] iptvDeviceBoxCount] ?:@"0";
    NSString *devicePLCCount = [[RegistrationFormModel sharedInstance] iptvDevicePLCCount] ?:@"0";
    NSString *deviceSTBCount = [[RegistrationFormModel sharedInstance] iptvDeviceSTBCount] ?:@"0";
    NSString *iptvChargeTimes = [[RegistrationFormModel sharedInstance] iptvChargeTimes] ?:@"0";
    NSString *iptvFilmPlusChargeTime = [[RegistrationFormModel sharedInstance] iptvFilmPlusChargeTime] ?:@"0";
    NSString *iptvFilmPlusPrepaidMonth = [[RegistrationFormModel sharedInstance] iptvFilmPlusPrepaidMonth] ?:@"0";
    
    NSString *iptvFilmHotChargeTime = [[RegistrationFormModel sharedInstance] iptvFilmHotChargeTime] ?:@"0";
    NSString *iptvFilmHotPrepaidMonth = [[RegistrationFormModel sharedInstance] iptvFilmHotPrepaidMonth] ?:@"0";
    
    NSString *iptvFilmKPlusChargeTime = [[RegistrationFormModel sharedInstance] iptvFilmKPlusChargeTime] ?:@"0";
    NSString *iptvFilmKPLusPrepaidMonth = [[RegistrationFormModel sharedInstance] iptvFilmKPlusPrepaidMonth] ?:@"0";
    
    NSString *iptvFilmVTCChargeTime = [[RegistrationFormModel sharedInstance] iptvFilmVTCChargeTime] ?:@"0";
    NSString *iptvFilmVTCPrepaidMonth = [[RegistrationFormModel sharedInstance] iptvFilmVTCPrepaidMonth] ?:@"0";
    
    NSString *iptvFilmVTVChargeTime = [[RegistrationFormModel sharedInstance] iptvFilmVTVChargeTime] ?:@"0";
    NSString *iptvFilmVTVPrepaidMonth = [[RegistrationFormModel sharedInstance] iptvFilmVTVPrepaidMonth] ?:@"0";
    NSString *iPrepaid;
    
    ShareData *shared = [ShareData instance];
    [shared.registrationFormProxy GetIPTVTotal:shared.currentUser.userName sPackage:selectedPackageIPTV.Key iPromotionID:selectedPromotionIPTV.PromotionId iChargeTimes:iptvChargeTimes iPrepaid:iPrepaid iBoxCount:deviceBoxCount iPLCCount:devicePLCCount iReturnSTB:deviceSTBCount iVTVCabPrepaidMonth:iptvFilmVTVPrepaidMonth iVTVCabChargeTimes:iptvFilmVTVChargeTime iKPlusPrepaidMonth:iptvFilmKPLusPrepaidMonth iKPlusChargeTimes:iptvFilmKPlusChargeTime iVTCPrepaidMonth:iptvFilmVTCPrepaidMonth iVTCChargeTimes:iptvFilmVTCChargeTime iHBOPrepaidMonth: @"0" iHBOChargeTimes:@"0" IServiceType:@"1" iFimPlusChargeTimes:iptvFilmPlusChargeTime iFimPlusPrepaidMonth:iptvFilmPlusPrepaidMonth iFimHotChargeTimes:iptvFilmHotChargeTime iFimHotPrepaidMonth:iptvFilmHotPrepaidMonth iIPTVPromotionIDBoxOrder:selectedPromotionIPTVBoxOrder.PromotionId
                             IPTVPromotionType:selectedPromotionIPTV.Type ?:@"-1"
                     IPTVPromotionTypeBoxOrder:selectedPromotionIPTVBoxOrder.Type ?:@"-1"
                                      Contract:@""
                                       RegCode:@""
                                Completehander:^(id result, NSString *errorCode, NSString *message) {
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
           
            [self.delegate logOutWhenEndSession];

            [self.delegate hideMBProcessListRegis];
            return;
        }
        //vutt11
        if ([message isEqualToString:@"Error Service: Unauthorized"]) {
            [self.delegate hideMBProcessListRegis];
            return;
        }
        
        if(![errorCode isEqualToString:@"0"]){
            [self.delegate hideMBProcessListRegis];
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            return ;
        }
        NSDictionary *dict = result;
        amountIPTV = [ StringFormat(@"%@",[dict objectForKey:@"Total"]) intValue ];
        
        [[RegistrationFormModel sharedInstance] setTotalIPTV:[ StringFormat(@"%@",[dict objectForKey:@"Total"]) intValue]];
    
        DeviceTotal = StringFormat(@"%@",[dict objectForKey:@"DeviceTotal"]);
        PrepaidTotal = StringFormat(@"%@",[dict objectForKey:@"PrepaidTotal"]);

        [self Total];
        flagtotaliptv = TRUE;
        [self.delegate hideMBProcessListRegis];
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@- %@",@"GetIPTVTotal",[error localizedDescription]]];
        [self.delegate hideMBProcessListRegis];
    }];
    
}

#pragma mark - Total amount

-(void)Total {
    
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    //vutt11 fixed
    [self checkTotalOffice365];
    
    amountInternet = [[RegistrationFormModel sharedInstance] totalInternet];
    amountIPTV = [[RegistrationFormModel sharedInstance] totalIPTV];
    
    KeyValueModel *selectedDeposit = [[RegistrationFormModel sharedInstance] deposit];
    KeyValueModel *selectedDepositBlackPoint = [[RegistrationFormModel sharedInstance] depositBlackPoint];
    // vutt11 Fix (not total money of Deposit and DepositBlackPoint when Upadate)
    
    amountTotal = amountInternet + amountIPTV + [selectedDeposit.Key intValue] + [selectedDepositBlackPoint.Key intValue] + office365Total + amountOTT + [amountDevice intValue];
    

    [[RegistrationFormModel sharedInstance] setTotalAmount:amountTotal];
    
    [self configTableView];
    [myTableView reloadData];
    
}

//MARK: -ANITEXTFIELDDELEGATE
-(void)valueTextfield:(NSString *)value kTag:(NSInteger)tag {
    
    switch (tag) {
        case indoor: {
            [[RegistrationFormModel sharedInstance] setIndoor:value];
            break;
        }
        case outdoor: {
            [[RegistrationFormModel sharedInstance] setOutdoor:value];
            break;
        }
        default:
            break;
    }
    
}

#pragma mark - validate email

- (BOOL) validateEmail:(NSString *)candidate {
    NSString *string= candidate;
    NSError *error = NULL;
    NSRegularExpression *regex = nil;
    regex = [NSRegularExpression regularExpressionWithPattern:@"\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,6}"
                                                      options:NSRegularExpressionCaseInsensitive
                                                        error:&error];
    NSUInteger numberOfMatches = 0;
    numberOfMatches = [regex numberOfMatchesInString:string
                                             options:0
                                               range:NSMakeRange(0, [string length])];
    if(numberOfMatches != 0){
        return TRUE;
    }
    return FALSE;
}

#pragma mark - String Validate
-(NSString *)JSONString:(NSString *)aString {
    NSMutableString *s = [NSMutableString stringWithString:aString];
    [s replaceOccurrencesOfString:@"\"" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\n" withString:@"\\n" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\b" withString:@"\\b" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\f" withString:@"\\f" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\r" withString:@"\\r" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    [s replaceOccurrencesOfString:@"\t" withString:@"\\t" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    return [NSString stringWithString:s];
}

//Total ListDevice
- (void)getTotalDevice :(NSMutableArray *)arrayListDevice {
    
    //check network
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    [self.delegate showMBProcessListRegis];
    NSLog(@"%@",arrayListDevice);
    ShareData *shared = [ShareData instance];
    [shared.appProxy GetTotalDevice:arrayListDevice CustomerStatus:@"0" Handler:^(id result,NSString *errorCode,NSString *message){
        [self.delegate hideMBProcessListRegis];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self.delegate logOutWhenEndSession];
            return ;
        }
        if ([message isEqualToString:@"Error Service: Unauthorized"]) {
            NSLog(@"Error Service: Unauthorized -- %@",message);
            
            [self.delegate logOutWhenEndSession];
            
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            NSLog(@"isEqualToString");
            return ;
        }
        NSArray *arr = result;
        
        if (arr.count <= 0) {
            NSLog(@"arr.count <= 0");
            return;
            
        } else {
            amountDevice = StringFormat(@"%@",[arr valueForKey:@"Amount"]);
            NSCharacterSet *character = [NSCharacterSet whitespaceCharacterSet];
            
            amountDevice = [amountDevice stringByReplacingOccurrencesOfString:@"(" withString:@""];
            amountDevice = [amountDevice stringByReplacingOccurrencesOfString:@")" withString:@""];
            amountDevice = [amountDevice stringByTrimmingCharactersInSet:character];
            amountDevice = [amountDevice stringByReplacingOccurrencesOfString:@"\n" withString:@""];
            amountDevice = [amountDevice stringByReplacingOccurrencesOfString:@" " withString:@""];
            
            [[RegistrationFormModel sharedInstance] setTotalDevice:amountDevice];
            
            [self Total];
            
        }
        
        
        
    } errorHandler:^(NSError *error){
        
        [self.delegate hideMBProcessListRegis];
        
    }];
    
}

@end
