//
//  PaymentViewController.h
//  demoRegister
//
//  Created by Bored Ninjas on 11/24/16.
//  Copyright © 2016 com.StevenNguyen.DemoCreateRegisterForm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "UIPopoverListView.h"
#import "UIViewController+FPTCustom.h"
#import "TDDatePicker.h"
#import "RegistrationFormDetailRecord.h"
#import "EGOCache.h"

@protocol PaymentViewControllerDelegate <NSObject>

-(void) scrollToView:(NSInteger)tag tagButton:(NSInteger)tagButton;

-(void) scrollToViewValidate:(NSInteger)tag;

-(void) popViewController:(NSString *)resultID;

-(void) showMBProcessListRegis;

-(void) hideMBProcessListRegis;

-(void) logOutWhenEndSession;

@end

@interface PaymentViewController : TDDatePicker<UIPopoverListViewDataSource, UIPopoverListViewDelegate>

@property (nonatomic, weak) id <PaymentViewControllerDelegate> delegate;

@property (retain, nonatomic) NSMutableArray *arrDeposit;
@property (retain, nonatomic) NSMutableArray *arrDepositBlackPoint;
@property (retain, nonatomic) NSMutableArray *arrPaymentType;

@property (strong, nonatomic) RegistrationFormDetailRecord *rc;

@property (strong, nonatomic) UIImagePickerController *imagePickerController;

-(void)reloadDataViewController;

@property (strong, nonatomic) NSString *Id;

-(void)LoadDeposit:(NSString *)Id;

-(void)LoadDepositBlackPoint:(NSString *) Id;

- (void)loadPaymentType:(NSString *)Id;

- (void)getImageWithUrl:(NSString *)url title:(NSString *)title;

- (void)checkTotalOffice365;

- (void)getTotalDevice :(NSMutableArray *)arrayListDevice;

-(void)loadCachePaymentType:(NSString *)idPayment;


@end
