//
//  AddressViewController.m
//  MobiSale
//
//  Created by Bored Ninjas on 11/18/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "AddressViewController.h"
#import "AniTextFieldCell.h"
#import "AniPhoneNumberCell.h"
#import "AniTextViewCell.h"
#import "ButtonNextCell.h"

enum StyleCell{
    
    county = 0,
    ward = 1,
    typeOfHouse = 2,
    street = 3,
    housePosition = 4,
    numberOfHouse = 5,
    noteAddress = 6,
    notePlant = 7,
    button = 8
    
};

@interface AddressViewController ()<UITableViewDelegate, UITableViewDataSource> {
    
    __weak IBOutlet UITableView *myTableView;
    
}
@end

@implementation AddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configTableView];
    
}

//config and register cell for tableview.
- (void)configTableView {
    
    myTableView.delegate = self;
    myTableView.dataSource = self;
    
    UINib *cellTextField = [UINib nibWithNibName:@"AniTextFieldCell" bundle:nil];
    UINib *cellNote = [UINib nibWithNibName:@"AniTextViewCell" bundle:nil];
    UINib *cellButton = [UINib nibWithNibName:@"ButtonNextCell" bundle:nil];
    
    [myTableView registerNib:cellTextField forCellReuseIdentifier:@"cellTxtF"];
    [myTableView registerNib:cellNote forCellReuseIdentifier:@"cellNote"];
    [myTableView registerNib:cellButton forCellReuseIdentifier:@"cellButton"];
    
}

//MARK: -UITABLEVIEWDELEGATE

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == notePlant || indexPath.row == noteAddress) {
        return 95;
    } else {
        return 60;
    }
    
}

//MARK: -UITABLEVIEWDATASOURCE

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return button + 1;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    switch (indexPath.row) {
        case county: {
            
            AniTextFieldCell *cell = (AniTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTxtF" forIndexPath:indexPath];
            
            cell.lblTitle.text = @"Quận (Huyện)";
            cell.txtFValue.placeholder = @"Quận (Huyện)";
            
            return cell;
            
        }
            
        case ward: {
            
            AniTextFieldCell *cell = (AniTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTxtF" forIndexPath:indexPath];
            
            cell.lblTitle.text = @"Phường (Xã)";
            cell.txtFValue.placeholder = @"Phường (Xã)";
            
            return cell;
            
        }
            
        case typeOfHouse: {
            
            AniTextFieldCell *cell = (AniTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTxtF" forIndexPath:indexPath];
            
            cell.lblTitle.text = @"Loại nhà";
            cell.txtFValue.placeholder = @"Loại nhà";
            
            return cell;
            
        }
            
            
        case street: {
            
            AniTextFieldCell *cell = (AniTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTxtF" forIndexPath:indexPath];
            
            cell.lblTitle.text = @"Đường";
            cell.txtFValue.placeholder = @"Đường";
            
            return cell;
            
        }
            
            
        case housePosition: {
            
            AniTextFieldCell *cell = (AniTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTxtF" forIndexPath:indexPath];
            
            cell.lblTitle.text = @"Vị trí nhà";
            cell.txtFValue.placeholder = @"Vị trí nhà";
            
            return cell;
            
        }
            
            
        case numberOfHouse: {
            
            AniTextFieldCell *cell = (AniTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTxtF" forIndexPath:indexPath];
            
            cell.lblTitle.text = @"Số nhà";
            cell.txtFValue.placeholder = @"Số nhà";
            
            return cell;
            
        }
            
            
        case noteAddress: {
            
            AniTextViewCell *cell = (AniTextViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellNote" forIndexPath:indexPath];
            
            return cell;
            
        }
            
            
        case notePlant: {
            
            AniTextViewCell *cell = (AniTextViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellNote" forIndexPath:indexPath];
            
            return cell;
            
        }
            
        case button: {
            
            ButtonNextCell *cell = (ButtonNextCell *)[tableView dequeueReusableCellWithIdentifier:@"cellButton" forIndexPath:indexPath];
            
            
            
            return cell;
            
        }
            
            
        default: {
            
            AniTextFieldCell *cell = (AniTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTxtF" forIndexPath:indexPath];
            
            cell.lblTitle.text = @"Họ tên khách hàng";
            cell.txtFValue.placeholder = @"Nhập họ và tên";
            
            return cell;
            
        }
            
    }
    
}

@end
