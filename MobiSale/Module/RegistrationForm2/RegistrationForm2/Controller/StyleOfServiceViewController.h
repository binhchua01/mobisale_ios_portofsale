//
//  StyleOfServiceViewController.h
//  demoRegister
//
//  Created by Bored Ninjas on 11/23/16.
//  Copyright © 2016 com.StevenNguyen.DemoCreateRegisterForm. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIPopoverListView2.h"
#import "BaseViewController.h"
#import "ListDeviceViewController.h"
#import "ListDeviceTableCell.h"
#import "MobiSale-Swift.h"

@protocol StyleOfServiceViewControllerDelegate <NSObject>

-(void) loadViewControllerWithArray:(NSInteger)tag tagButton:(NSInteger)tagButton array:(NSMutableArray *)arr;

-(void) loadViewController:(NSInteger)tag tagButton:(NSInteger)tagButton;

-(void) loadViewController:(NSInteger)tag listDeviceVC:(ListDeviceViewController *)listDeviceVC delegate:(id<ListDeviceViewControllerDelegate>)delegateListDevice arrayDeviceChecked:(NSMutableArray *)arrayDeviceChecked;

-(void) loadOTTListDeviceOTTViewControllerViewController:(NSInteger)tag listDeviceVC:(ListDeviceOTTViewController *)listDeviceVC delegate:(id<ListDeviceOTTViewControllerDelegate>)delegateListDevice arrayDeviceChecked:(NSMutableArray *)arrayDeviceChecked;

-(void) loadScanViewController:(NSInteger)tag listDeviceVC:(CameraViewController *)listDeviceVC delegate:(id<CameraViewControllerDelegate>)delegateListDevice arrayDeviceChecked:(NSMutableArray *)arrayDeviceChecked;

-(void) dismissViewController;

-(void) scrollToView:(NSInteger)tag tagButton:(NSInteger)tagButton;

-(void) showMBProcessListRegis;

-(void) hideMBProcessListRegis;

-(void) logOutWhenEndSession;

@end

@interface StyleOfServiceViewController : BaseViewController<UIPopoverListView2DataSource, UIPopoverListView2Delegate>

@property (nonatomic, weak) id <StyleOfServiceViewControllerDelegate> delegate;
// Goi dich vu (LocalType)
@property (retain, nonatomic) NSMutableArray *arrSaveLocalType;
//
@property (retain, nonatomic) NSMutableArray *arrLocalType;
//
@property (retain, nonatomic) NSMutableArray *arrInternetService;
// Cau lenh khuyen mai internet
@property (retain, nonatomic) NSMutableArray *arrInternetPromotion;
// Combo info
@property (retain, nonatomic) NSMutableArray *arrCombo;
// Hinh thuc trien khai
@property (retain, nonatomic) NSMutableArray *arrDeployment;
//
@property (retain, nonatomic) NSMutableArray *arrPackageIPTV;
// Khuyen mai IPTV
@property (retain, nonatomic) NSMutableArray *arrPromotionIPTV;
// Khuyen mai thu box 2
@property (retain, nonatomic) NSMutableArray *arrPromotionIPTVBoxOrder;
// Array cau lenh khuyen mai cua thiet bi
@property (retain, nonatomic) NSMutableArray *arrayPomotionListDevice;
// Array add listdevice when update
@property (retain, nonatomic) NSMutableArray *arrayAddWhenUpdate;
// Id phieu dang ky
@property (strong, nonatomic) NSString *Id;

//Array OTT Device
@property (strong,nonatomic)NSMutableArray *arrayListOTT;

//Array OTT Device
@property (strong,nonatomic)NSMutableArray *arrayMACOTT;


-(void) reloadTableView;

-(void)LoadLocalTypePost:(NSString *)Id serverPackageID:(NSString *)serverPackageID PromotionId:(NSString *)promotionid;

-(void)LoadPackageIPTV:(NSString *)Id PromotionIPTV:(NSString *)promotionIPTV boxCount:(NSString *)boxCount;

-(void)LoadPromotionIPTV:(NSString *)Id Package:(NSString *)package boxCount:(NSString *)boxCount RegCode:(NSString *)regCode;

-(void)LoadDeployment:(NSString *) Id;

- (void)LoadCombo:(NSString *)Id;

-(void)LocaLocalType:(NSString *)promotionid Package:(NSString *)package Officetotal:(int) office365Total ArrayDeviceChecked:(NSMutableArray *)arrayDevice;

-(void) setPackageForInternet:(NSString *)localType promotionIPTV:(NSString *)promotionIPTV promotion:(NSString *)PromotionIDBoxOrder countBox:(NSString *)countBox;

- (void)upDateShowInfoListDevice:(NSMutableArray*)arrayListDevice ;

@end
