//
//  CustomersViewController.m
//  MobiSale
//
//  Created by Bored Ninjas on 11/18/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "CustomersViewController.h"
#import "AniTextFieldCell.h"
#import "AniPhoneNumberCell.h"
#import "AniCheckBoxCell.h"
#import "ButtonNextCell.h"
#import "AniDropDownCellTableViewCell.h"
#import "RegistrationFormModel.h"
#import "KeyValueModel.h"
#import "PromotionModel.h"
#import "Common_client.h"
#import "ShareData.h"

///
enum StyleCell{
    
    customers = 0,
    isp = 1,
    address = 2,
    solvency = 3,
    partnerCustomer = 4,
    entity = 5,
    cableStatus = 6,
    isIPTV = 7,
    button = 8,
    
};

@interface CustomersViewController ()<UITableViewDelegate, UITableViewDataSource, AniDropDownCellTableViewCellDelegate,AniCheckBoxCellDelegate,ButtonNextCellDelegate> {
    
    __weak IBOutlet UITableView *myTableView;
    
}


@end

@implementation CustomersViewController {
    
    BOOL isSelectedCustomer;
    BOOL isSelectedISP;
    BOOL isSelectedAddress;
    BOOL isSelectedSolvency;
    BOOL isSelectedPartnerCustomer;
    BOOL isSelectedEntity;
    
}
@synthesize delegate;
@synthesize arrCustomer;
@synthesize arrISP;
@synthesize arrAddress;
@synthesize arrSolvency;
@synthesize arrPartnerCustomer;
@synthesize arrEntity;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isSelectedCustomer = NO;
    isSelectedISP = NO;
    isSelectedAddress = NO;
    isSelectedSolvency = NO;
    isSelectedPartnerCustomer = NO;
    isSelectedEntity = NO;
    
    [self configTableView];
    
    [self LoadObjectCustomer:@"0"];
    [self LoadISP:@"0"];
    [self LoadCurrenPlace:@"0"];
    [self LoadSolveny:@"0"];
    [self LoadClientPartner:@"0"];
    [self LoadLegal:@"0"];
    
    if (self.Id.length <= 0 || [self.Id isEqualToString:@"0"]) {
        
        [self LoadObjectCustomer:@"0"];
        [self LoadISP:@"0"];
        [self LoadCurrenPlace:@"0"];
        [self LoadSolveny:@"0"];
        [self LoadClientPartner:@"0"];
        [self LoadLegal:@"0"];
        
    }
    
}

//config and register cell for tableview.
- (void)configTableView {
    
    myTableView.delegate = self;
    myTableView.dataSource = self;
    
    UINib *cellTextField = [UINib nibWithNibName:@"AniTextFieldCell" bundle:nil];
    UINib *cellCheckBox = [UINib nibWithNibName:@"AniCheckBoxCell" bundle:nil];
    UINib *cellButton = [UINib nibWithNibName:@"ButtonNextCell" bundle:nil];
    UINib *cellDropDown = [UINib nibWithNibName:@"AniDropDownCellTableViewCell" bundle:nil];
    
    [myTableView registerNib:cellTextField forCellReuseIdentifier:@"cellTxtF"];
    [myTableView registerNib:cellCheckBox forCellReuseIdentifier:@"cellCheckBox"];
    [myTableView registerNib:cellButton forCellReuseIdentifier:@"cellButton"];
    [myTableView registerNib:cellDropDown forCellReuseIdentifier:@"cellDropDown"];
    
}

/********************************************************/
//MARK: -LOADALL
/********************************************************/
-(void)LoadObjectCustomer:(NSString *) Id {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self.delegate hideMBProcessListRegis];
        return;
    }
    self.arrCustomer = [Common getPartNer];
    int i = 0;
    if([Id isEqualToString:@"0"]){
        [[RegistrationFormModel sharedInstance] setObjectCustomer:[self.arrCustomer objectAtIndex:0]];
    }else {
        for (i = 0; i< self.arrCustomer.count; i++) {
            KeyValueModel *selectedObjectCustomer = [self.arrCustomer objectAtIndex:i];
            if([Id isEqualToString:selectedObjectCustomer.Key]){
                [[RegistrationFormModel sharedInstance] setObjectCustomer:selectedObjectCustomer];
                isSelectedCustomer = YES;
                return;
            }
        }
    }
    
}

-(void)LoadISP:(NSString *) Id {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self.delegate hideMBProcessListRegis];
        return;
    }
    self.arrISP = [Common getISP];
    int i = 0;
    if([Id isEqualToString:@"0"]){
        [[RegistrationFormModel sharedInstance] setIsp:[self.arrISP objectAtIndex:0]];
       
    }else {
        for (i = 0; i< self.arrISP.count; i++) {
            KeyValueModel *selectedISP = [self.arrISP objectAtIndex:i];
            if([Id isEqualToString:selectedISP.Key]){
                [[RegistrationFormModel sharedInstance] setIsp:selectedISP];
                isSelectedISP = YES;
                return;
            }
        }
    }
    
}

-(void)LoadCurrenPlace:(NSString *) Id {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self.delegate hideMBProcessListRegis];
        return;
    }
    self.arrAddress = [Common getCurrentPlace];
    int i = 0;
    if([Id isEqualToString:@"0"]){
        [[RegistrationFormModel sharedInstance] setAddressNow:[self.arrAddress objectAtIndex:0]];
    }else {
        for (i = 0; i< self.arrAddress.count; i++) {
            KeyValueModel *selectedPlaceCurrent = [self.arrAddress objectAtIndex:i];
            if([Id isEqualToString:selectedPlaceCurrent.Key]){
                [[RegistrationFormModel sharedInstance] setAddressNow:selectedPlaceCurrent];
                isSelectedAddress = YES;
                return;
            }
        }
    }
    
}

-(void)LoadSolveny:(NSString *) Id {
    self.arrSolvency = [Common getSolvency];
    int i = 0;
    if([Id isEqualToString:@"0"]){
        [[RegistrationFormModel sharedInstance] setSolvency:[self.arrSolvency objectAtIndex:0]];
    }else {
        for (i = 0; i< self.arrSolvency.count; i++) {
            KeyValueModel *selectedSolvency = [self.arrSolvency objectAtIndex:i];
            if([Id isEqualToString:selectedSolvency.Key]){
                [[RegistrationFormModel sharedInstance] setSolvency:selectedSolvency];
                isSelectedSolvency = YES;
                return;
            }
        }
    }
    
}

-(void)LoadClientPartner:(NSString *) Id {
    self.arrPartnerCustomer = [Common getPartCustomer];
    int i = 0;
    if([Id isEqualToString:@"0"]){
        [[RegistrationFormModel sharedInstance] setPartnerCustomer:[self.arrPartnerCustomer objectAtIndex:0]];
    }else {
        for (i = 0; i< self.arrPartnerCustomer.count; i++) {
            KeyValueModel *selectedClientPartners = [self.arrPartnerCustomer objectAtIndex:i];
            if([Id isEqualToString:selectedClientPartners.Key]){
                [[RegistrationFormModel sharedInstance] setPartnerCustomer:selectedClientPartners];
                isSelectedPartnerCustomer = YES;
                return;
            }
        }
    }
    
}

-(void)LoadLegal:(NSString *) Id {
    self.arrEntity = [Common getLegal];
    int i = 0;
    if([Id isEqualToString:@"0"]){
        [[RegistrationFormModel sharedInstance] setEntity:[self.arrEntity objectAtIndex:0]];
    }else {
        for (i = 0; i< self.arrEntity.count; i++) {
            KeyValueModel *selectedLegal = [self.arrEntity objectAtIndex:i];
            if([Id isEqualToString:selectedLegal.Key]){
                [[RegistrationFormModel sharedInstance] setEntity:selectedLegal];
                isSelectedEntity = YES;
                return;
            }
        }
    }
    
}

-(void)reloadDataViewController {
    
    [myTableView reloadData];
    
}

/********************************************************/
//MARK: - ANIDROPDOWNCELLTABLEVIEWCELLDELEGATE
/********************************************************/
-(void) dropDownButton: (id)sender kTag:(NSInteger)tag {
    
    switch (tag) {
        case customers: {
            [self setupDropDownView:customers];
            isSelectedCustomer = YES;
            break;
        }
            
        case isp: {
            [self setupDropDownView:isp];
            isSelectedISP = YES;
            break;
        }
            
        case address: {
            [self setupDropDownView:address];
            isSelectedAddress = YES;
            break;
        }
            
        case solvency: {
            [self setupDropDownView:solvency];
            isSelectedSolvency = YES;
            break;
        }
            
        case partnerCustomer: {
            [self setupDropDownView:partnerCustomer];
            isSelectedPartnerCustomer = YES;
            break;
        }
            
        case entity: {
            [self setupDropDownView:entity];
            isSelectedEntity = YES;
            break;
        }
            
        default: {
            break;
            
        }
    }
    
}

/****************************/
//MARK: - UIPOPOVERLISTVIEW
/****************************/
- (void)setupDropDownView: (NSInteger) tag {
    [self.view endEditing:YES];
    
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.tag = tag;
    poplistview.listView.scrollEnabled = TRUE;
    switch (tag) {
        case customers:
            [poplistview setTitle:NSLocalizedString(@"Đối tượng khách hàng", @"")];
            break;
        case isp:
            [poplistview setTitle:NSLocalizedString(@"ISP", @"")];
            break;
        case address:
            [poplistview setTitle:NSLocalizedString(@"Nơi ở hiện tại", @"")];
            break;
        case solvency:
            [poplistview setTitle:NSLocalizedString(@"Khả năng thanh toán", @"")];
            break;
        case partnerCustomer:
            [poplistview setTitle:NSLocalizedString(@"Khách hàng của đối tác", @"")];
            break;
        case entity:
            [poplistview setTitle:NSLocalizedString(@"Pháp nhân", @"")];
            break;
        default:
            break;
    }
    
    [poplistview show];
    
}

- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

#pragma mark - UIPopoverListViewDataSource

- (void)setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell tag:(NSInteger)tag{
    KeyValueModel *model;
   // PromotionModel *promotionmodel;
    cell.textLabel.numberOfLines = 0;
    [cell.textLabel sizeToFit];
    switch (tag) {
        case customers:
            model = [self.arrCustomer objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case isp:
            model = [self.arrISP objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case address:
            model = [self.arrAddress objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case solvency:
            model = [self.arrSolvency objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case partnerCustomer:
            model = [self.arrPartnerCustomer objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case entity:
            model = [self.arrEntity objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        default:
            break;
            
    }
    
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView cellForIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row cell:cell tag:popoverListView.tag];
    cell.textLabel.font = [UIFont fontWithName:@"Arial" size:14];
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = 0;
    switch (popoverListView.tag) {
        case customers: {
            count = self.arrCustomer.count;
            break;
        }
        case isp: {
            count = self.arrISP.count;
            break;
        }
        case address:{
            count = self.arrAddress.count;
            break;
        }
        case solvency:{
            count = self.arrSolvency.count;
            break;
        }
        case partnerCustomer:{
            count = self.arrPartnerCustomer.count;
            break;
        }
        case entity:{
            count = self.arrEntity.count;
            break;
        }
    }
    return count;
    
}

#pragma mark - UIPopoverListViewDelegate

- (void) didDropDownSelected:(NSInteger)row tag:(NSInteger)tag {
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    switch (tag) {
        case customers:{
            NSLog(@"%ld",(long)row);
            [[RegistrationFormModel sharedInstance] setObjectCustomer:[self.arrCustomer objectAtIndex:row]];
            
            if (![[[self.arrCustomer objectAtIndex:row] Key]  isEqual: @"2"]) {
                [[RegistrationFormModel sharedInstance] setIsp:nil];
            }
            
            [myTableView reloadData];
            
            break;
            
        }
        case isp:{
            [[RegistrationFormModel sharedInstance] setIsp:[self.arrISP objectAtIndex:row]];
            [myTableView reloadData];
            
            break;
        }
        case address:{
            [[RegistrationFormModel sharedInstance] setAddressNow:[self.arrAddress objectAtIndex:row]];
            [myTableView reloadData];
            
            break;
        }
        case solvency:{
            [[RegistrationFormModel sharedInstance] setSolvency:[self.arrSolvency objectAtIndex:row]];
            [myTableView reloadData];
            
            break;
        }
        case partnerCustomer:{
            [[RegistrationFormModel sharedInstance] setPartnerCustomer:[self.arrPartnerCustomer objectAtIndex:row]];
            
            [myTableView reloadData];
            
            break;
        }
        case entity:{
            [[RegistrationFormModel sharedInstance] setEntity:[self.arrEntity objectAtIndex:row]];
            [myTableView reloadData];
            
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - UIPopoverListViewDelegate

- (void)popoverListView:(UIPopoverListView *)popoverListView didSelectIndexPath:(NSIndexPath *)indexPath {
    [self didDropDownSelected:indexPath.row tag:popoverListView.tag];
    [popoverListView  dismiss];
    
}

/*******************************/
//MARK: -ANICHECKBOXCELLDELEGATE
/*******************************/
-(void)valueTextfield:(NSString *)value kTag:(NSInteger)tag {
    
    switch (tag) {
        case cableStatus: {
            [[RegistrationFormModel sharedInstance] setCableStatus:value];
            break;
        }
        case isIPTV: {
            [[RegistrationFormModel sharedInstance] setIsIPTV:value];
            break;
        }
        default:
            break;
    }
    
}

/***************************/
//MARK: -UITABLEVIEWDELEGATE
/***************************/
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == cableStatus || indexPath.row == isIPTV) {
        return 80;
    } else {
        return 60;
    }
    
}

/*****************************/
//MARK: -UITABLEVIEWDATASOURCE
/*****************************/
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return button + 1;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //cellDropDown
    switch (indexPath.row) {
        case customers: {
            
            AniDropDownCellTableViewCell *cell = (AniDropDownCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellDropDown" forIndexPath:indexPath];
            
            NSString *str = [[RegistrationFormModel sharedInstance] objectCustomer].Values ?:@"";
            cell.delegate = self;
            if ( ![str  isEqualToString: @""] ) {
                cell.btnChoose.selected = YES;
                [cell configDropDownCell:@"Đối tượng khách hàng" titleButton:str kTag:customers];
            } else {
                cell.btnChoose.selected = NO;
                [cell configDropDownCell:@"Đối tượng khách hàng" titleButton:@"Đối tượng khách hàng" kTag:customers];
            }
            
            return cell;
            
        }
            
        case isp: {
            
            AniDropDownCellTableViewCell *cell = (AniDropDownCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellDropDown" forIndexPath:indexPath];
            
            NSString *str = [[RegistrationFormModel sharedInstance] isp].Values ?:@"";
            cell.delegate = self;
            cell.btnChoose.enabled = FALSE;
            if ( ![str  isEqualToString: @""] ) {
                cell.btnChoose.selected = YES;
                [cell configDropDownCell:@"ISP" titleButton:str kTag:isp];
            } else {
                cell.btnChoose.selected = NO;
                [cell configDropDownCell:@"ISP" titleButton:@"ISP" kTag:isp];
            }
            
            
            
            if ([[[RegistrationFormModel sharedInstance] objectCustomer].Key  isEqual: @"2"]) {
               
                cell.btnChoose.enabled = YES;
                
            } else {

            }
            
            
            return cell;
            
        }
            
        case address: {
            
            AniDropDownCellTableViewCell *cell = (AniDropDownCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellDropDown" forIndexPath:indexPath];
            
            NSString *str = [[RegistrationFormModel sharedInstance] addressNow].Values ?:@"";
            cell.delegate = self;
            if ( ![str  isEqualToString: @""] ) {
                cell.btnChoose.selected = YES;
                [cell configDropDownCell:@"Nơi ở hiện tại" titleButton:str kTag:address];
            } else {
                cell.btnChoose.selected = NO;
                [cell configDropDownCell:@"Nơi ở hiện tại" titleButton:@"Nơi ở hiện tại" kTag:address];
            }
            
            return cell;
            
        }
            
            
        case solvency: {
            
            AniDropDownCellTableViewCell *cell = (AniDropDownCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellDropDown" forIndexPath:indexPath];
            
            NSString *str = [[RegistrationFormModel sharedInstance] solvency].Values ?:@"";
            cell.delegate = self;
            if ( ![str  isEqualToString: @""] ) {
                cell.btnChoose.selected = YES;
                [cell configDropDownCell:@"Khả năng thanh toán" titleButton:str kTag:solvency];
            } else {
                cell.btnChoose.selected = NO;
                [cell configDropDownCell:@"Khả năng thanh toán" titleButton:@"Khả năng thanh toán" kTag:solvency];
            }
            
            return cell;
            
        }
            
            
        case partnerCustomer: {
            
            AniDropDownCellTableViewCell *cell = (AniDropDownCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellDropDown" forIndexPath:indexPath];
            
            NSString *str = [[RegistrationFormModel sharedInstance] partnerCustomer].Values ?:@"";
            cell.delegate = self;
            if ( ![str  isEqualToString: @""] ) {
                cell.btnChoose.selected = YES;
                [cell configDropDownCell:@"Khách hàng của đối tác" titleButton:str kTag:partnerCustomer];
            } else {
                cell.btnChoose.selected = NO;
                [cell configDropDownCell:@"Khách hàng của đối tác" titleButton:@"Khách hàng của đối tác" kTag:partnerCustomer];
            }
            
            return cell;
            
        }
            
            
        case entity: {
            
            AniDropDownCellTableViewCell *cell = (AniDropDownCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellDropDown" forIndexPath:indexPath];
            
            NSString *str = [[RegistrationFormModel sharedInstance] entity].Values ?:@"";
            cell.delegate = self;
            if ( ![str  isEqualToString: @""] ) {
                cell.btnChoose.selected = YES;
                [cell configDropDownCell:@"Pháp nhân" titleButton:str kTag:entity];
            } else {
                cell.btnChoose.selected = NO;
                [cell configDropDownCell:@"Pháp nhân" titleButton:@"Pháp nhân" kTag:entity];
            }
            
            return cell;
        }
            
        case cableStatus: {
            
            AniCheckBoxCell *cell = (AniCheckBoxCell *)[tableView dequeueReusableCellWithIdentifier:@"cellCheckBox" forIndexPath:indexPath];
            
            cell.delegate = self;
            NSString *str = [[RegistrationFormModel sharedInstance] cableStatus] ?:@"";
            [cell configCell:@"Tình trạng truyền hình cáp" kTag:cableStatus];
            if ( [str  isEqualToString: @""] ) {
                [cell configCellWithYesNo:@"2" SelectedNo:@"2" SelectedYes:@"1"];
            } else {
                [cell configCellWithYesNo:str SelectedNo:@"2" SelectedYes:@"1"];
            }
            
            return cell;
            
        }
            
        case isIPTV: {
            
            AniCheckBoxCell *cell = (AniCheckBoxCell *)[tableView dequeueReusableCellWithIdentifier:@"cellCheckBox" forIndexPath:indexPath];
            
            cell.delegate = self;
            NSString *str = [[RegistrationFormModel sharedInstance] isIPTV] ?:@"";
            [cell configCell:@"Nhu cầu lắp đặt truyền hình" kTag:isIPTV];
            if ( [str  isEqualToString: @""] ) {
                [cell configCellWithYesNo:@"2" SelectedNo:@"2" SelectedYes:@"1"];
            } else {
                [cell configCellWithYesNo:str SelectedNo:@"2" SelectedYes:@"1"];
            }
            
            
            
            return cell;
            
        }
            
        case button: {
            
            ButtonNextCell *cell = (ButtonNextCell *)[tableView dequeueReusableCellWithIdentifier:@"cellButton" forIndexPath:indexPath];
            
            cell.delegate = self;
            
            return cell;
            
        }
            
        default: {
            
            AniTextFieldCell *cell = (AniTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTxtF" forIndexPath:indexPath];
            
            cell.lblTitle.text = @"Họ tên khách hàng";
            cell.txtFValue.placeholder = @"Nhập họ và tên";
            
            return cell;
            
        }
            
    }
    
}

/***************************************/
//MARK: -ButtonNextCellDelegate
/***************************************/

-(void)pressedButton:(id)sender kTag:(NSInteger)tag ktagButton:(NSInteger)tagButton {
    
    switch (tagButton) {
        case 1:{
            //save
            [delegate scrollToView:2 tagButton:1];
            break;
        }
        case 2:{
            //back
            [delegate scrollToView:2 tagButton:2];
            break;
        }
        default:
            break;
    }
    
    
}


@end
