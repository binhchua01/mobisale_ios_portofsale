 //
//  StyleOfServiceViewController.m
//  demoRegister
//
//  Created by Bored Ninjas on 11/23/16.
//  Copyright © 2016 com.StevenNguyen.DemoCreateRegisterForm. All rights reserved.

#import "StyleOfServiceViewController.h"
#import "Office365Cell.h"
#import "InternetCell.h"
#import "IPTVCell.h"
#import "AniTextFieldCell.h"
#import "MobiSale-Swift.h"
#import "RegistrationFormModel.h"
#import "KeyValueModel.h"
#import "PromotionModel.h"
#import "Common_client.h"
#import "ShareData.h"
#import "SearchPromotionViewController.h"
#import "SuggestionsSearchViewController.h"
#import "AniDropDownCellTableViewCell.h"
#import "Office365ViewController.h"
#import "ButtonNextCell.h"
#import "TypeOfServiceCell.h"
#import "IPTVPrice.h"
#import "FPTPlayCell.h"
#import "UIImage+FPTCustom.h"
#import "PaymentViewController.h"
#import "ListRegisteredFormViewController.h"

//Define import add cell DeviceEquipment
#import "DeviceEquipmentCell.h"
#import "ListDeviceEquipmentCell.h"
#import "TableListDeviceEquipmentCell.h"

#import "ListDeviceModel.h"
#import "CacheDropDownCell.h"

//MARK: -StevenNguyen-FixError
#define kArrayServiceType(f) ([NSString stringWithFormat:@"arrayservicetype%@",f])

enum StyleOfService {
    
    office365 = 0,
    internet = 1,
    iptv = 2,
    fptPlay = 101,
    styleService = 99,
    button = 100,
    
    device = 102,
    tableListDevice = 103,
    listDevice = 104,
    
    internetService = 3,
    internetPromotion = 4,
    internetSuggest = 5,
    serviceBase = 6,
    deploy = 7,
    combo = 8,
    service = 9,
    promotion1 = 10,
    promotion2 = 11,
    filmPlus = 12,
    filmHot = 13,
    filmKPlus = 14,
    filmHD = 15,
    filmVTVCab = 16,
    styleServicePackage = 17
};

@interface StyleOfServiceViewController ()<UITableViewDelegate, UITableViewDataSource, InternetCellDelegate,SearchPromotionDelegate,SuggestionsDelegate,IPTVCellDelegate,AniDropDownCellTableViewCellDelegate,UIScrollViewDelegate,ButtonNextCellDelegate,TypeOfServiceCellDelegate,Office365CellDelegate,DeviceEquipmentCellDelegate,ListDeviceViewControllerDelegate,LisRegisteredFormViewControllerDelegate,ListDeviceEquipmentCellDelegate,ListDeviceTableCellDelegate, FPTPlayNewCellDelegate> {
    
    __weak IBOutlet UITableView *myTableView;
    
    ActionButton *btnFloatingAction;
    
    NSMutableArray *arrActionButtonItem;
    
    NSMutableArray *arrStyleLocalCell;
    
    NSArray<ActionButtonItem *> *arr;

    BOOL isEnablePackageService;
    
    IPTVPrice *iptvPrice;
    
    NSString *promotionID,*packedID,*packedInternet,*iptvIPTVPromotion,*iptvPromotionOrder,*iPTVBoxCount;
    
    BOOL isCheckDevices ;
    BOOL isCheckStype;  
    BOOL isCheckShowUpdateListDevice;
    int coutUpDate;
    int coutCheck;
    int coutCheckAdd;
    int coutCheckTableDeviceCell;
    
}
@property (strong, nonatomic)ListDeviceViewController *ls;
@property (strong, nonatomic)NSMutableArray *arraySelectedListDevies;
@property (strong, nonatomic)ListRegisteredFormViewController *lr;
@property (strong, nonatomic)NSMutableArray *arraySaveLocalCell;
@property (strong,   nonatomic)ListDeviceEquipmentCell *ldCell;
//ArrayAddListDeviced
@property (strong,nonatomic) NSMutableArray *arrayAddListDevices;
@property (strong,nonatomic) NSMutableDictionary *dict;
@property (strong,nonatomic) NSMutableDictionary *dictListDevice;
@property (strong,nonatomic) NSMutableArray *arrayListDevicesUpdate;
//Array list Device when update add
@property (strong,nonatomic) NSMutableArray *arrayUpdateListDeviceAdd;

//Array Device da chon show ra khi update
@property (strong,nonatomic) NSMutableArray *arrayDeviceChecked;

//CreadAdd Mode
@property (strong,nonatomic) NSMutableArray *arrayAddModeDevice;
//Array test
@property (strong,nonatomic)NSMutableArray *arrayListDevice;

////Array OTT Device
//@property (strong,nonatomic)NSMutableArray *arrayListOTT;
//
////Array OTT Device
//@property (strong,nonatomic)NSMutableArray *arrayMACOTT;

@end

@implementation StyleOfServiceViewController {
    
    BOOL isSelectedInternetService;
    BOOL isSelectedInternetPromotion;
    
    BOOL isDevies ;
    BOOL ischeckNotReload;
    
    PaymentViewController *payVC;
    ListDeviceModel *selectedListDeviceModel;
    
}

@synthesize regcode;
@synthesize delegate;
@synthesize arrSaveLocalType;
@synthesize arrLocalType;
@synthesize arrInternetService;
@synthesize arrInternetPromotion;
@synthesize arrCombo;
@synthesize arrPackageIPTV;
@synthesize arrPromotionIPTV;
@synthesize arrPromotionIPTVBoxOrder;

- (NSMutableArray*)arrayListDevice {
    if (!_arrayListDevice) {
        _arrayListDevice = [[NSMutableArray alloc] init];
    }
    return _arrayListDevice;
}
- (NSMutableDictionary *)dictListDevice {
    if (!_dictListDevice) {
        _dictListDevice = [[NSMutableDictionary alloc] init];
    }
    return _dictListDevice;
}

- (NSMutableDictionary *)dict {
    if (!_dict) {
        _dict = [[NSMutableDictionary alloc] init];
    }
    return _dict;
}

- (NSMutableArray *)arrayAddListDevices {
    if (!_arrayAddListDevices) {
        _arrayAddListDevices = [[NSMutableArray alloc] init];
    }
    return _arrayAddListDevices;
}

- (NSMutableArray *)arrayListDevicesUpdate {
    if (!_arrayListDevicesUpdate) {
        _arrayListDevicesUpdate = [[NSMutableArray alloc] init];
    }
    return _arrayListDevicesUpdate;
}

- (NSMutableArray *)arrayUpdateListDeviceAdd {
    if (!_arrayUpdateListDeviceAdd) {
        _arrayUpdateListDeviceAdd = [[NSMutableArray alloc] init];
    }
    return _arrayUpdateListDeviceAdd;
}
- (NSMutableArray *)arrayDeviceChecked {
    if (!_arrayDeviceChecked) {
        _arrayDeviceChecked = [[NSMutableArray alloc] init];
    }
    return _arrayDeviceChecked;
}
- (NSMutableArray *)arrayAddModeDevice {
    if (!_arrayAddModeDevice) {
        _arrayAddModeDevice = [[NSMutableArray alloc] init];
    }
    return _arrayAddModeDevice;
}
- (NSMutableArray *)arrayAddWhenUpdate {
    if (!_arrayAddWhenUpdate) {
        _arrayAddWhenUpdate = [[NSMutableArray alloc] init];
    }
    return _arrayAddWhenUpdate;
}

- (ListDeviceEquipmentCell *)ldCell {
    if (!_ldCell) {
        _ldCell = [[ListDeviceEquipmentCell alloc] init];
    }
    return _ldCell;
}
- (ListDeviceViewController *)ls {
    
    if (!_ls) {
        _ls = [[ListDeviceViewController alloc] init];
    }
    return _ls;
}

- (ListRegisteredFormViewController *)lr {
    if (!_lr) {
        _lr = [[ListRegisteredFormViewController alloc] init];
    }
    return _lr;
}

- (NSMutableArray *)arraySelectedListDevies {
    if (!_arraySelectedListDevies) {
        _arraySelectedListDevies = [[NSMutableArray alloc] init];
    }
    return _arraySelectedListDevies;
}

- (NSMutableArray *)arraySaveLocalCell {
    if (!_arraySaveLocalCell) {
        _arraySaveLocalCell = [[NSMutableArray alloc] init];
    }
    return _arraySaveLocalCell;
}

- (NSMutableArray *)arrayPomotionListDevice {
    if (!_arrayPomotionListDevice) {
        _arrayPomotionListDevice = [[NSMutableArray alloc] init];
    }
    return _arrayPomotionListDevice;
}

//Array OTT Device
//@property (strong,nonatomic)NSMutableArray *arrayMACOTT;
- (NSMutableArray *)arrayMACOTT {
    if (!_arrayMACOTT) {
        _arrayMACOTT = [[NSMutableArray alloc] init];
    }
    return _arrayMACOTT;
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    myTableView.estimatedRowHeight = 80;
    myTableView.rowHeight = UITableViewAutomaticDimension;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //init delegate of ListDeviceViewController
    ischeckNotReload = false;
    isDevies = false;
    isCheckDevices = false;
    coutCheck = 0;
    coutUpDate = 0;
   
    self.ls.delegate = self;
    self.ldCell.delegate = self;
    
    isSelectedInternetService = NO;
    isSelectedInternetPromotion = NO;
    isEnablePackageService = NO;
    
    arrSaveLocalType = [[NSMutableArray alloc] init];
    arrActionButtonItem = [[NSMutableArray alloc] init];
    arr = [[NSArray<ActionButtonItem *> alloc] init];
    // created Button Action Service(Internet,IPTV,Office 365, FPTPlay)
    [self initFloatingActionButton];
    [self configTableView];
    
    arrStyleLocalCell = [[NSMutableArray alloc] init];
    [arrStyleLocalCell removeAllObjects];
//    [self loadIPTVPrice];

    promotionID = @"0";
    packedID = @"0";
    packedInternet = @"0";
    
    if (self.Id.length <= 0 || [self.Id isEqualToString:@"0"]) {
        
//        [self LoadLocalTypePost:@"0" serverPackageID:@"0" PromotionId:@"0" Complete:^{}];
//        [self loadIPTVPrice:@"-1" contract:@"" RegCode:@""];
        [self loadIPTVPrice:@"-1" contract:@"" RegCode:@"" promotionID:@""];
        [self loadServiceType:@"0" ServicePackageID:@"0" PromotionID:@"0"];
        [self LoadCombo:@"0"];
        [self LoadDeployment:@"0"];
        [self LoadPackageIPTV:[[RegistrationFormModel sharedInstance] iptvPackageIPTV].Key PromotionIPTV:[[RegistrationFormModel sharedInstance] iptvPromotionIPTV].PromotionId boxCount:@"1" Complete:^{}];
        [self LocaLocalType:@"1" Package:@"" Officetotal:0 ArrayDeviceChecked:0];
        
    } else {
        
        if (self.regcode.length <= 0) {
//            [self LoadLocalTypePost:@"0" serverPackageID:@"0" PromotionId:@"0" Complete:^{}];
//            [self loadIPTVPrice:@"-1" contract:@"" RegCode:@""];
            [self loadIPTVPrice:@"-1" contract:@"" RegCode:@"" promotionID:@""];
            [self loadServiceType:@"0" ServicePackageID:@"0" PromotionID:@"0"];
            [self LoadCombo:@"0"];
            [self LoadDeployment:@"0"];
            [self LoadPackageIPTV:[[RegistrationFormModel sharedInstance] iptvPackageIPTV].Key PromotionIPTV:[[RegistrationFormModel sharedInstance] iptvPromotionIPTV].PromotionId boxCount:@"1" Complete:^{}];
            [self LocaLocalType:@"1" Package:@"" Officetotal:0 ArrayDeviceChecked:0];
            
        } else {
            
//            [self loadIPTVPrice:@"-1" contract:@"" RegCode:self.regcode ?: @""];
            [self loadIPTVPrice:@"-1" contract:@"" RegCode:self.regcode ?: @"" promotionID:@""];
            
        }
        
    }

}

-(void) reloadTableView {
    
    [myTableView reloadData];
    
}

- (void)initFloatingActionButton {
    
    btnFloatingAction = [[ActionButton alloc] initWithAttachedToView:self.view items: arrActionButtonItem];
    __weak typeof (self) weakSelf = self;
    [btnFloatingAction setAction:^(ActionButton * button) {
        
        //vutt fixed (Capturing 'self' strongly in this block is likely to lead to a retain cycle)
        
        [ weakSelf pressedLocalType];
        
    }];
    
    [btnFloatingAction setTitle:@"+" forState:UIControlStateNormal];
    btnFloatingAction.backgroundColor = [UIColor colorWithRed:238.0/255.0 green:130.0/255.0 blue:34.0/255.0 alpha:1.0];
    
}

- (void)loadServiceType:(NSString *)ID ServicePackageID:(NSString *)servicePackageID PromotionID:(NSString *)promotionID {
    
    NSMutableArray *arrCacheService = [[EGOCache globalCache] objectForKey:kArrayServiceType(ID)];
    
    if (arrCacheService.count > 0) {//Có dữ liệu từ Cache
        
        self.arrInternetService = arrCacheService;
        if([servicePackageID isEqualToString:@"0"]  ){
            [[RegistrationFormModel sharedInstance] setInternetService:[self.arrInternetService objectAtIndex:0]];
            
        }else {
            [[RegistrationFormModel sharedInstance] setInternetService:[self.arrInternetService objectAtIndex:0]];
            int i = 0;
            for (i = 0; i< self.arrInternetService.count; i++) {
                KeyValueModel *selectedServicePack = [self.arrInternetService objectAtIndex:i];
                if([servicePackageID isEqualToString:selectedServicePack.Key]){
                    isSelectedInternetService = YES;
                    [[RegistrationFormModel sharedInstance] setInternetService:selectedServicePack];
                    
                    [myTableView reloadData];
                    
                    break;
                }
            }
        }
        
        NSString * strKey = [[RegistrationFormModel sharedInstance] internetService].Key;
        [self LoadPromotion:promotionID LocalType:strKey];
        
    } else {//Không có dữ liệu nên request API
        
        [self LoadLocalTypePost:ID serverPackageID:servicePackageID PromotionId:promotionID Complete:^{}];
        
    }
    
}

-(void) saveCacheWith:(id)obj KeyCache:(NSString *)keyCache {
    
    dispatch_queue_t dispatchQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(dispatchQueue, ^ {
        [[EGOCache globalCache] setObject:obj forKey:keyCache];
    });
    
}

- (void)pressedLocalType {
    
    [self setupDropDownView:99];
    
}

- (void)pressedAppendToCell:(NSInteger) styleOfService {
    
    NSNumber *inter = [NSNumber numberWithInt:internet];
    NSNumber *offi = [NSNumber numberWithInt:office365];
    NSNumber *ipt = [NSNumber numberWithInt:iptv];
    NSNumber *fpt = [NSNumber numberWithInt:fptPlay];
    NSNumber *typeService = [NSNumber numberWithInt:styleService];
    NSNumber *numberButton = [NSNumber numberWithInt:button];
    NSNumber *devices = [NSNumber numberWithInt:device];
    NSNumber *tableListDevices = [NSNumber numberWithInt:tableListDevice];
    NSNumber *listDevices = [NSNumber numberWithInt:listDevice];
   //104 of listdevice
    //arrStyleLocalCell = self.arraySaveLocalCell;
    
    switch (styleOfService) {
            
        case listDevice:{
            
            [arrStyleLocalCell addObject:listDevices];
            [myTableView reloadData];
            break;
            
        }
        case device:{
            
            for (int i = 0; i < self.arrSaveLocalType.count; i++)
            {
                if (self.arrSaveLocalType.count == 2 && [[self.arrSaveLocalType[i]valueForKey:@"Key"]isEqualToString:@"3"]) {
                    
                    [self.arrSaveLocalType removeObjectAtIndex:1];
                    [myTableView reloadData];
                    break;
                    
                }
                else {
                    //cap nhat khi khong biet tai sao arrlocalSave lai doupble
                    if (![self.Id isEqualToString:@"(null)"]) {
                        coutCheckAdd ++;
                        if (coutCheckAdd <=  1) {
                            [arrStyleLocalCell addObject:devices];
                            [myTableView reloadData];
                            break;
                        }
                    }
                  if ([self.Id isEqualToString:@"(null)"])
                  {
                      [arrStyleLocalCell addObject:devices];
                      [myTableView reloadData];
                       break;
                  }
                }
            }
           
        }
        case tableListDevice: {
            
                coutCheckTableDeviceCell ++;
                if (coutCheckTableDeviceCell <= 1) {
                    [arrStyleLocalCell addObject:tableListDevices];
                    [myTableView reloadData];
                }
             break;
            
        }
            
        case office365: {
            
            [arrStyleLocalCell addObject:offi];
            [myTableView reloadData];
            break;
            
        }
        case internet: {
            
            [arrStyleLocalCell addObject: inter];
            [myTableView reloadData];
            break;
            
        }
        case iptv: {
            
            [arrStyleLocalCell addObject: ipt];
            [myTableView reloadData];
            break;
            
        }
        case fptPlay: {
            [arrStyleLocalCell removeAllObjects];
            [arrStyleLocalCell addObject: typeService];
            [arrStyleLocalCell addObject:fpt];
            [myTableView reloadData];
            break;
            
        }
        case styleService: {
            
            [arrStyleLocalCell addObject: typeService];
            [myTableView reloadData];
            isCheckStype = true;
            break;
            
        }
        case button: {
            
            [arrStyleLocalCell addObject: numberButton];
            [myTableView reloadData];
            break;
        }
        default:
            break;
    }
    
}

- (void)configTableView {
    
    myTableView.delegate = self;
    myTableView.dataSource = self;
    
    UINib *cellOffice = [UINib nibWithNibName:@"AniTextFieldCell" bundle:nil];
    UINib *cellTextField = [UINib nibWithNibName:@"Office365Cell" bundle:nil];
    UINib *cellPhoneNumber = [UINib nibWithNibName:@"InternetCell" bundle:nil];
    UINib *cellIPTV = [UINib nibWithNibName:@"IPTVCell" bundle:nil];
    UINib *cellDropDown = [UINib nibWithNibName:@"AniDropDownCellTableViewCell" bundle:nil];
    UINib *cellTypeOfService = [UINib nibWithNibName:@"TypeOfServiceCell" bundle:nil];
    UINib *cellButton = [UINib nibWithNibName:@"ButtonNextCell" bundle:nil];
    UINib *cellFPTPlay = [UINib nibWithNibName:@"FPTPlayNewCell" bundle:nil];
    
    //vutt11 add Cell DeviceEquipmentCell
    UINib *cellDeviceEquipment = [UINib nibWithNibName:@"DeviceEquipmentCell" bundle:nil];
    UINib *cellTableListDeviceEquipment = [UINib nibWithNibName:@"TableListDeviceEquipmentCell" bundle:nil];
    UINib *cellListDeviceEquipment = [UINib nibWithNibName:@"ListDeviceEquipmentCell" bundle:nil];
    
    UINib *cellListDeviceTable = [UINib nibWithNibName:@"ListDeviceTableCell" bundle:nil];
    
    [myTableView registerNib:cellListDeviceTable forCellReuseIdentifier:@"cellListDeviceTable"];
    
    [myTableView registerNib:cellListDeviceTable forCellReuseIdentifier:@"cellListDeviceTable"];
     [myTableView registerNib:cellListDeviceEquipment forCellReuseIdentifier:@"cellListDeviceEquipment"];
    [myTableView registerNib:cellDeviceEquipment forCellReuseIdentifier:@"cellDeviceEquipment"];
    [myTableView registerNib:cellTableListDeviceEquipment forCellReuseIdentifier:@"cellTableListDeviceEquipment"];
    
    [myTableView registerNib:cellOffice forCellReuseIdentifier:@"cellOffice2"];
    [myTableView registerNib:cellTextField forCellReuseIdentifier:@"cellOffice"];
    [myTableView registerNib:cellPhoneNumber forCellReuseIdentifier:@"cellInternet"];
    [myTableView registerNib:cellIPTV forCellReuseIdentifier:@"cellIPTV"];
    [myTableView registerNib:cellDropDown forCellReuseIdentifier:@"cellDropDown"];
    [myTableView registerNib:cellTypeOfService forCellReuseIdentifier:@"cellTypeOfService"];
    [myTableView registerNib:cellButton forCellReuseIdentifier:@"cellButton"];
    [myTableView registerNib:cellFPTPlay forCellReuseIdentifier:@"cellFPTPlay"];
    
}

//MARK: -LOADALL
-(void)LoadLocalTypePost:(NSString *)Id serverPackageID:(NSString *)serverPackageID PromotionId:(NSString *)promotionid Complete:(void (^) ())completed {
    
    [self.delegate showMBProcessListRegis];
    
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self.delegate hideMBProcessListRegis];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:Id forKey:@"ServiceType"];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    
    if (isSelectedInternetService == YES) {
        [self.delegate showMBProcessListRegis];
    }
    
    [shared.appProxy GetListLocalTypePost:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self.delegate logOutWhenEndSession];
            [self.delegate hideMBProcessListRegis];
            return;
        }
        //vutt11
        if ([message isEqualToString:@"Error Service: Unauthorized"]) {
            [self.delegate hideMBProcessListRegis];
            return;
        }
        
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self.delegate hideMBProcessListRegis];
            return ;
        }
        
        self.arrInternetService = result;
        int i = 0;
        
        if(self.arrInternetService.count > 0) {
            [self saveCacheWith:self.arrInternetService KeyCache:kArrayServiceType(Id)];
            if([serverPackageID isEqualToString:@"0"]  ){
                [[RegistrationFormModel sharedInstance] setInternetService:[self.arrInternetService objectAtIndex:0]];
                
            }else {
                
                [[RegistrationFormModel sharedInstance] setInternetService:[self.arrInternetService objectAtIndex:0]];
                for (i = 0; i< self.arrInternetService.count; i++) {
                    KeyValueModel *selectedServicePack = [self.arrInternetService objectAtIndex:i];
                    if([serverPackageID isEqualToString:selectedServicePack.Key]){
                        isSelectedInternetService = YES;
                        [[RegistrationFormModel sharedInstance] setInternetService:selectedServicePack];
                        
                        [myTableView reloadData];
                        
                        break;
                    }
                }
            }
            
            NSString * strKey = [[RegistrationFormModel sharedInstance] internetService].Key;
            [self.delegate hideMBProcessListRegis];
            [self LoadPromotion:promotionid LocalType:strKey];
            completed();
            return;
        }
        
        if (isSelectedInternetService == YES) {
            [self.delegate hideMBProcessListRegis];
        }
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetListLocalType",[error localizedDescription]]];
        [self.delegate hideMBProcessListRegis];

    }];
}

-(void)LoadDeployment:(NSString *) Id {

    self.arrDeployment = [Common getDeployment];
    int i = 0;
    if([Id isEqualToString:@"0"]){
        [[RegistrationFormModel sharedInstance] setIptvDeployment:[self.arrDeployment objectAtIndex:0]];
        
    }else {
        for (i = 0; i< self.arrDeployment.count; i++) {
            KeyValueModel *selectedDeployment = [self.arrDeployment objectAtIndex:i];
            if([Id isEqualToString:selectedDeployment.Key]){
                [[RegistrationFormModel sharedInstance] setIptvDeployment:selectedDeployment];
                return;
            }
        }
    }
}

-(void)LoadPromotion:(NSString *)Id LocalType:(NSString *)localtype {

    [self.delegate showMBProcessListRegis];
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];

        [self.delegate hideMBProcessListRegis];
        return;
    }
    if (isSelectedInternetService == YES) {

        [self.delegate showMBProcessListRegis];
    }
    ShareData *shared = [ShareData instance];
        [shared.appProxy GetListPromotion:localtype Complatehander:^(id result, NSString *errorCode, NSString *message) {
            if ([message isEqualToString:@"het phien lam viec"]) {
                [self ShowAlertErrorSession];
           
                [self.delegate logOutWhenEndSession];

                [self.delegate hideMBProcessListRegis];
                return;
            }
            //vutt11
            if ([message isEqualToString:@"Error Service: Unauthorized"]) {

                [self.delegate hideMBProcessListRegis];
                return;
            }
            if (errorCode.length < 1) {
                errorCode=@"<null>";
            }
            if(![errorCode isEqualToString:@"<null>"]){
                [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
//                [self hideMBProcess];
                [self.delegate hideMBProcessListRegis];

                return ;
            }
            self.arrInternetPromotion = result;
            
            if (self.arrInternetPromotion.count <= 0 || [Id isEqualToString:@"0"]) {

                [[RegistrationFormModel sharedInstance] setInternetPromotion:[[PromotionModel alloc] initWithName:@"0" description:@"[Chọn câu lệnh khuyến mãi]" Amount:@"0" Type:@""]];
                [myTableView reloadData];
            }
            
            if (self.arrInternetPromotion.count > 0 && ![Id isEqualToString:@"0"]) {
                int i = 0;
                for (i = 0; i< self.arrInternetPromotion.count; i++) {
                    
                    PromotionModel *selectedPromotion = [self.arrInternetPromotion objectAtIndex:i];
                    
                    if([Id isEqualToString:selectedPromotion.PromotionId]){
                        
                        if ([selectedPromotion.PromotionName isKindOfClass:[NSString class]]) {
                            
                            [[RegistrationFormModel sharedInstance] setInternetPromotion:selectedPromotion];
                            //vutt 11 fixed 
                            int amountInternet = [[[RegistrationFormModel sharedInstance] internetPromotion].Amount intValue];
                            [[RegistrationFormModel sharedInstance] setTotalInternet:amountInternet];
                            
                            break;
                            
                        }else{

                            [self.delegate hideMBProcessListRegis];
                            [self showAlertBox:@"Thông Báo" message:@"Dữ liệu không đúng"];
                            break ;
                        }
                        
                    }
                }
                [myTableView reloadData];
            }
            
            NSNumberFormatter *nf = [NSNumberFormatter new];
            nf.numberStyle = NSNumberFormatterDecimalStyle;
            
            [self.delegate hideMBProcessListRegis];
            
        } errorHandler:^(NSError *error) {

            [self.delegate hideMBProcessListRegis];

            [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetListPromotion",[error localizedDescription]]];
        }];
}

- (void)LoadCombo:(NSString *)Id {
    
    self.arrCombo = [Common getCombo];
    if([Id isEqualToString:@"0"]){
        
        [[RegistrationFormModel sharedInstance] setIptvCombo:[self.arrCombo objectAtIndex:0]];
        
        return;
    }
    if([Id isEqualToString:@"2"]){
        
        [[RegistrationFormModel sharedInstance] setIptvCombo:[self.arrCombo objectAtIndex:1]];
        
        return;
    }
    if([Id isEqualToString:@"3"]){
        
        [[RegistrationFormModel sharedInstance] setIptvCombo:[self.arrCombo objectAtIndex:2]];
        
        return;
    }

}

-(void)LoadPackageIPTV:(NSString *)Id PromotionIPTV:(NSString *)promotionIPTV boxCount:(NSString *)boxCount Complete:(void (^) () )completed{

    int i = 0;
    self.arrPackageIPTV = [Common getServicePackage];
    
    if([Id isEqualToString:@"0"] || [Id isEqualToString:@""] || Id == nil){
        
        [[RegistrationFormModel sharedInstance] setIptvPackageIPTV:[self.arrPackageIPTV objectAtIndex:0]];
        
    }else {
        for (i = 0; i< self.arrPackageIPTV.count; i++) {
            KeyValueModel *selectedPackageIPTV = [self.arrPackageIPTV objectAtIndex:i];
            if([Id isEqualToString:selectedPackageIPTV.Key]){
                [[RegistrationFormModel sharedInstance] setIptvPackageIPTV:selectedPackageIPTV];
                break;
            }
        }
    }
    //vutt11
    [self LoadPromotionIPTV:promotionIPTV Package:[[RegistrationFormModel sharedInstance] iptvPackageIPTV].Key boxCount:boxCount RegCode:self.regcode ?: @"" Complete:^{
        completed();
    }];
    
}

-(void)LoadPromotionIPTV:(NSString *)Id Package:(NSString *)package boxCount:(NSString *)boxCount RegCode:(NSString *)regCode Complete:(void (^) () )completed{

    // Nếu danh sách CLKM IPTV đã load trước đó thì không load lại
    if (self.arrPromotionIPTV.count > 0 && self.arrPromotionIPTVBoxOrder.count > 0) {
        return;
    }
    
    [self.delegate showMBProcessListRegis];
    
    // Kiểm tra kết nối internet
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
//        [self hideMBProcess];
        [self.delegate hideMBProcessListRegis];

        return;
    }

    ShareData *shared = [ShareData instance];
    
            [shared.appProxy GetPromotionIPTVList:package withBoxOrder:boxCount CustomerType:(NSString *)@"1" Contract:@"" RegCode:regCode Handler:^(id result, NSString *errorCode, NSString *message) {
                
                if ([message isEqualToString:@"het phien lam viec"]) {
                    [self ShowAlertErrorSession];
              
                    [self.delegate logOutWhenEndSession];

                    [self.delegate hideMBProcessListRegis];
                    return;
                }
                //vutt11
                if ([message isEqualToString:@"Error Service: Unauthorized"]) {

                    [self.delegate hideMBProcessListRegis];
                    return;
                }
                
                if(![errorCode isEqualToString:@"0"]){
                    [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];

                    [self.delegate hideMBProcessListRegis];
                    return ;
                }
                NSArray *arrData = result;
                if (arrData.count <= 0) {

                    [self.delegate hideMBProcessListRegis];
                    return ;
                }
                NSInteger count = [boxCount integerValue];
                
                if (count <= 1) { // Box đầu tiên hoặc là box mới
                    self.arrPromotionIPTV = [NSMutableArray arrayWithArray:arrData];
                    PromotionModel *selectedPromotionModel;
                    if (arrPromotionIPTV > 0) {
                        if ([Id isEqualToString:@"0"] || Id == nil) {
                            selectedPromotionModel = [[PromotionModel alloc] initWithName:@"0" description:@"[Chọn câu lệnh khuyến mãi]" Amount:@"0" Type:@""];
                        } else {
                            for (selectedPromotionModel in arrPromotionIPTV) {
                                if([Id isEqualToString:selectedPromotionModel.PromotionId]){
                                    [[RegistrationFormModel sharedInstance] setIptvPromotionIPTV:selectedPromotionModel];
                                    [self loadIPTVPrice:[[RegistrationFormModel sharedInstance] iptvPromotionIPTV].Type ?: @"-1" contract:@"" RegCode:self.regcode ?: @"" promotionID:[[RegistrationFormModel sharedInstance] iptvPromotionIPTV].PromotionId ?: @""];
                                    
                                    break;
                                }
                            }
                        }
                    }
                    [myTableView reloadData];
                    
                } else { // Box thứ 2 trở lên
                    self.arrPromotionIPTVBoxOrder = [NSMutableArray arrayWithArray:arrData];
                    PromotionModel *selectedPromotionModel;
                    
                    if (arrPromotionIPTVBoxOrder > 0) {
                        if ([Id isEqualToString:@"0"] || Id == nil) {
                            selectedPromotionModel = [[PromotionModel alloc] initWithName:@"0" description:@"[Chọn câu lệnh khuyến mãi 2]" Amount:@"0" Type:@""];
                            [[RegistrationFormModel sharedInstance] setIptvPromotionIPTV2:selectedPromotionModel];
                        } else {
                            NSString * idBox2;
                          
                            idBox2 = iptvPromotionOrder;
                          
                            for (selectedPromotionModel in arrPromotionIPTVBoxOrder) {
                                if([idBox2 isEqualToString:selectedPromotionModel.PromotionId]){
                                    [[RegistrationFormModel sharedInstance] setIptvPromotionIPTV2:selectedPromotionModel];
                                    [self loadIPTVPrice:[[RegistrationFormModel sharedInstance] iptvPromotionIPTV].Type ?: @"-1" contract:@"" RegCode:self.regcode ?: @"" promotionID:[[RegistrationFormModel sharedInstance] iptvPromotionIPTV].PromotionId ?:@""];
                                    break;
                                }
                            }
                        }
                    }
                    [myTableView reloadData];
                    
                }

                [self.delegate hideMBProcessListRegis];
                completed();

            } errorHandler:^(NSError *error) {
                [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetPromotionIPTVList",[error localizedDescription]]];

                [self.delegate hideMBProcessListRegis];

            }];
  
}

//StevenNguyen28/4
-(void) setPackageForInternet:(NSString *)localType promotionIPTV:(NSString *)promotionIPTV promotion:(NSString *)PromotionIDBoxOrder countBox:(NSString *)countBox {
    
    packedInternet = localType;
    iptvPromotionOrder = PromotionIDBoxOrder;
    iPTVBoxCount = countBox;
    iptvIPTVPromotion = promotionIPTV;
    
}

//MARK: -StevenNguyen: Kiểm tra khi load Loại Dịch vụ (Internet, FPT Play, ...)
-(void)LocaLocalType:(NSString *)promotionid Package:(NSString *)package Officetotal:(int) office365Total ArrayDeviceChecked:(NSMutableArray *)arrayDevice {

    self.arrLocalType = [Common getLocalType];
    promotionID = promotionid;
    packedID = package;
    
    if ([promotionid isEqualToString:@"4"] && [package isEqualToString:@"4"]) {
        //if have only FPT Play Box
        //chuyển packedID và promotionID lại = 0, do lúc gắn = 4 để check FPT Play Box...
        packedID = @"0";
        promotionID = @"0";
        packedInternet = @"0";
        [self selectedLocalType:[self.arrLocalType objectAtIndex:3] indexPath:nil];
    }else if(![promotionid isEqualToString:@"0"] && [package isEqualToString:@""]){
        //if have only Internet
        [self selectedLocalType:[self.arrLocalType objectAtIndex:0] indexPath:nil];
        
        if (![arrayDevice isEqual: [NSNull null]]) {
            if (arrayDevice.count > 0) {
                [self selectedLocalType:[self.arrLocalType objectAtIndex:4] indexPath:nil];
            }
        }
        
    }else if([promotionid isEqualToString:@"0"] && ![package isEqualToString:@""]){
        //if have only IPTV
        [self selectedLocalType:[self.arrLocalType objectAtIndex:1] indexPath:nil];
        if (![arrayDevice isEqual: [NSNull null]]) {
            if (arrayDevice.count > 0) {
                [self selectedLocalType:[self.arrLocalType objectAtIndex:4] indexPath:nil];
            }
        }
        
    }else if((![package isEqualToString:@""]) && (![promotionid isEqualToString:@"0"])){
        //if have Internet and IPTV
        [self selectedLocalType:[self.arrLocalType objectAtIndex:0] indexPath:nil];
        [self selectedLocalType:[self.arrLocalType objectAtIndex:1] indexPath:nil];
        
        if (![arrayDevice isEqual: [NSNull null]]) {
            if (arrayDevice.count > 0) {
                [self selectedLocalType:[self.arrLocalType objectAtIndex:4] indexPath:nil];
            }
        }
    }
    
    //vutt11
     if (office365Total > 0)
    {
        if ([promotionID isEqualToString: @"0"]) {
             packedInternet = @"0";
        }
       
        [self selectedLocalType:[self.arrLocalType objectAtIndex:2] indexPath:nil];
        
    }
    
   [self addLocalTypeWithEnum:arrSaveLocalType];
    
   
}

-(void)loadIPTVPrice:(NSString *)userName contract:(NSString *)contract RegCode:(NSString *)regCode promotionID:(NSString *)promotionID {


    ShareData *share = [ShareData instance];
//    NSMutableDictionary *dict = [NSDictionary dictionaryWithObject:share.currentUser.userName forKey:@"UserName"];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setValue:share.currentUser.userName forKeyPath:@"UserName"];
    [dict setValue:userName forKeyPath:@"IPTVPromotionType"];
    //IPTVPromotionID
    [dict setValue:promotionID forKeyPath:@"IPTVPromotionID"];
    [dict setValue:contract forKeyPath:@"Contract"];
    [dict setValue:regCode forKeyPath:@"RegCode"];
    
    [share.registrationFormProxy getIPTVPrice:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self.delegate logOutWhenEndSession];
            [self.delegate hideMBProcessListRegis];
            return;
        }
        //vutt11
        if ([message isEqualToString:@"Error Service: Unauthorized"]) {
            [self.delegate hideMBProcessListRegis];
            return;
        }
        
        if(![errorCode isEqualToString:@"0"]){
            return ;
        }
        
        iptvPrice = result;
        NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
        numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
        
        [[RegistrationFormModel sharedInstance] setIptvPriceFilmPlus:StringFormat(@"(%@ VNĐ)",[numberFormatter stringFromNumber:[NSNumber numberWithFloat:[iptvPrice.FimPlusPrice intValue]]])];
        
        [[RegistrationFormModel sharedInstance] setIptvPriceFilmHot:StringFormat(@"(%@ VNĐ)",[numberFormatter stringFromNumber:[NSNumber numberWithFloat:[iptvPrice.FimHotPrice intValue]]])];
        
        [[RegistrationFormModel sharedInstance] setIptvPriceFilmKPlus:StringFormat(@"(%@ VNĐ)",[numberFormatter stringFromNumber:[NSNumber numberWithFloat:[iptvPrice.KPlusPrice intValue]]])];
        
        [[RegistrationFormModel sharedInstance] setIptvPriceFilmHD:StringFormat(@"(%@ VNĐ)",[numberFormatter stringFromNumber:[NSNumber numberWithFloat:[iptvPrice.DacSacHDPrice intValue]]])];
        
        [[RegistrationFormModel sharedInstance] setIptvPriceFilmVTV:StringFormat(@"(%@ VNĐ)",[numberFormatter stringFromNumber:[NSNumber numberWithFloat:[iptvPrice.VTVCabPrice intValue]]])];
        
        [myTableView reloadData];
        
        
    } errorHandler:^(NSError *error) {
        NSLog(@"%@",[error description]);
    }];
}


//MARK: -UITABLEVIEWDELEGATE

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int numberStyle = [[arrStyleLocalCell objectAtIndex:indexPath.row] intValue];
    
    switch (numberStyle) {
        case tableListDevice:
            return UITableViewAutomaticDimension;
        default:
            return UITableViewAutomaticDimension;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int numberStyle = [[arrStyleLocalCell objectAtIndex:indexPath.row] intValue];
    
    switch (numberStyle) {
        case office365:
            return 105;
        case internet:
            return 210;
        case iptv:
            return 1450;
        case styleService:
            return 160;
        case button:
            return 60;
        case fptPlay:
            return UITableViewAutomaticDimension;
        case listDevice:
            return 50;
        case device:
            return 105;
        case tableListDevice:
            return UITableViewAutomaticDimension;
        default:
            return 60;
    }
}
//MARK: -UITABLEVIEWDATASOURCE

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrStyleLocalCell.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int numberStyle = [[arrStyleLocalCell objectAtIndex:indexPath.row] intValue];
    
    switch (numberStyle) {
            
        case listDevice: {
            
            TableListDeviceEquipmentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellTableListDeviceEquipment" forIndexPath:indexPath];
            return cell;
        
        }
            
        case tableListDevice:{
          
           ListDeviceTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellListDeviceTable" forIndexPath:indexPath];
            cell.delegate = self;
           
            cell.Id = self.Id;
            self.Id = [NSString stringWithFormat:@"%@",self.Id];
        
            if ( ![self.Id isEqualToString:@"(null)"]){
                
                cell.arrayData = [[RegistrationFormModel sharedInstance] listDevicePackagesArray];
                
                [cell.tableViewInfo reloadData];
                
            }
            // truong hop tao phieu dang ky 
            if ([self.Id isEqualToString:@"0"] || [self.Id isEqualToString:@"(null)"])
            
            {
                cell.arrayData = [[RegistrationFormModel sharedInstance] listDevicePackagesArray];
                [cell.tableViewInfo reloadData];
            
            }
            
            [cell reloadTableViewCell];
           
           return cell;

        }
        
            //Load Bán Thiết Bị .................
        case device:{
            
            DeviceEquipmentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellDeviceEquipment" forIndexPath:indexPath];
            
            [cell configDropDowCell:@"Chọn Loại Thiết Bị" PlaceTitle:@"Chọn Loại Thiết Bị"];
            cell.delegate = self;
            
            return cell;
            
        }
        case office365: {
            
             Office365Cell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellOffice" forIndexPath:indexPath];
            
            cell.delegate = self;
            [cell configDropDownCell:@"Office 365" titleButton:@"Office 365" kTag:office365];
            
            return cell;
            
        }
        case internet: {
            
            InternetCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellInternet" forIndexPath:indexPath];
            
            NSString *strService = [[RegistrationFormModel sharedInstance] internetService].Values;
            NSString *strPromotion = [[RegistrationFormModel sharedInstance] internetPromotion].PromotionName;
            
            cell.delegate = self;
            
            if (![strService  isEqual: @""] && ![strPromotion isEqual:@""]) {
                [cell configCell:strService titlePromotion:strPromotion stringService:strPromotion ktag:internet];
            }
            
            return cell;
            
        }case iptv: {
            
            IPTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellIPTV" forIndexPath:indexPath];
            
            NSString *strServiceBase = [[RegistrationFormModel sharedInstance] iptvServiceBase].Values ?:@"";
            NSString *strRequest = [[RegistrationFormModel sharedInstance] iptvRequest] ?:@"1";
            NSString *strDrillWall = [[RegistrationFormModel sharedInstance] iptvWallDrilling] ?:@"1";
            NSString *strCombo = [[RegistrationFormModel sharedInstance] iptvCombo].Values ?:@"";
            NSString *strDelopment = [[RegistrationFormModel sharedInstance] iptvDeployment].Values ?:@"";
            NSString *strPackageIPTV = [[RegistrationFormModel sharedInstance] iptvPackageIPTV].Values ?:@"";
            
            NSString *strPromotion1 = [[RegistrationFormModel sharedInstance] iptvPromotionIPTV].PromotionName ?:@"";
            
            NSString *strPromotion2 = [[RegistrationFormModel sharedInstance] iptvPromotionIPTV2].PromotionName ?:@"";
            
            NSString *strBoxCount = [[RegistrationFormModel sharedInstance] iptvDeviceBoxCount] ?:@"0";
            NSString *strPLCCount = [[RegistrationFormModel sharedInstance] iptvDevicePLCCount] ?:@"0";
            NSString *strSTBCount = [[RegistrationFormModel sharedInstance] iptvDeviceSTBCount] ?:@"0";
            NSString *strPostageCount = [[RegistrationFormModel sharedInstance] iptvChargeTimes] ?:@"0";
            
            NSString *strPlusCharge = [[RegistrationFormModel sharedInstance] iptvFilmPlusChargeTime] ?:@"0";
            NSString *strPlusPrepaid = [[RegistrationFormModel sharedInstance] iptvFilmPlusPrepaidMonth] ?:@"0";
            NSString *strHotCharge = [[RegistrationFormModel sharedInstance] iptvFilmHotChargeTime] ?:@"0";
            NSString *strHotPrepaid = [[RegistrationFormModel sharedInstance] iptvFilmHotPrepaidMonth] ?:@"0";
            NSString *strKPlusCharge = [[RegistrationFormModel sharedInstance] iptvFilmKPlusChargeTime] ?:@"0";
            NSString *strKPlusPrepaid = [[RegistrationFormModel sharedInstance] iptvFilmKPlusPrepaidMonth] ?:@"0";
            NSString *strVTCCharge = [[RegistrationFormModel sharedInstance] iptvFilmVTCChargeTime] ?:@"0";
            NSString *strVTCPrepaid = [[RegistrationFormModel sharedInstance] iptvFilmVTCPrepaidMonth] ?:@"0";
            NSString *strVTVCharge = [[RegistrationFormModel sharedInstance] iptvFilmVTVChargeTime] ?:@"0";
            NSString *strVTVPrepaid = [[RegistrationFormModel sharedInstance] iptvFilmVTVPrepaidMonth] ?:@"0";
            
            NSString *strPricePlus = [[RegistrationFormModel sharedInstance] iptvPriceFilmPlus] ?:@"";
            NSString *strPriceHot = [[RegistrationFormModel sharedInstance] iptvPriceFilmHot] ?:@"";
            NSString *strPriceKPlus = [[RegistrationFormModel sharedInstance] iptvPriceFilmKPlus] ?:@"";
            NSString *strPriceHD = [[RegistrationFormModel sharedInstance] iptvPriceFilmHD] ?:@"";
            NSString *strPriceVTV = [[RegistrationFormModel sharedInstance] iptvPriceFilmVTV] ?:@"";
                                                                                                
            cell.stepperBox.minimumValue = 1.0;
            cell.stepperPostage.minimumValue = 1.0;
            
            cell.delegate = self;
            [cell configPriceCell:strPricePlus
                          FilmHot:strPriceHot FilmKPlus:strPriceKPlus FilmHD:strPriceHD FilmVTV:strPriceVTV];
            
            [cell configCell:strServiceBase cbRequest:strRequest cbDrillWall:strDrillWall combo:strCombo delopment:strDelopment IPTVPackage:strPackageIPTV promotion1:strPromotion1 promotion2:strPromotion2];
            
            [cell configSteppersForCell:strBoxCount PLCCount:strPLCCount STBCount:strSTBCount PostageCount:strPostageCount];
            
            [cell configFilmForCell:strPlusCharge FilmPlusPrepaidMonth:strPlusPrepaid FilmHotChargeTime:strHotCharge FilmHotPrepaidMonth:strHotPrepaid FilmKPlusChargeTime:strKPlusCharge FilmKPlusPrepaidMonth:strKPlusPrepaid FilmVTCChargeTime:strVTCCharge FilmVTCPrepaidMonth:strVTCPrepaid FilmVTVChargeTime:strVTVCharge FilmVTVPrepaidMonth:strVTVPrepaid];
            
            return cell;
            
        }
        case fptPlay: {
            FPTPlayNewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellFPTPlay" forIndexPath:indexPath];
            
//            cell.arrTableView = self.arrayListOTT;
            cell.delegate = self;
            cell.arrMacCode = self.arrayMACOTT;
            [cell configCellWithDic:self.arrayListOTT indexPath:indexPath];
//            NSString *strBoxFPTPlay = [NSString stringWithFormat:@"%ld",(long)[[RegistrationFormModel sharedInstance] countFPTPlay] ];
//            [cell configFPTPlayBox:strBoxFPTPlay];
            
            
            return cell;
        }
            
        case styleService: {
            
            TypeOfServiceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellTypeOfService" forIndexPath:indexPath];
            
            NSString *strService = [[RegistrationFormModel sharedInstance] internetService].Values;
            NSMutableArray *arrLoca = [[RegistrationFormModel sharedInstance] localType];
            
            cell.delegate = self;
            cell.btnPackageService.enabled = YES;
            [cell configCell:@"Loại dịch vụ" titlePromotion:@"Gói dịch vụ" ktag:styleService];
            [cell configCellTitleButton:arrLoca titlePackage:strService];
            
            if (isEnablePackageService == YES) {
                cell.btnPackageService.enabled = NO;
                [cell.btnPackageService setTitle:@"Gói dịch vụ" forState:UIControlStateNormal];
            }
            
            return cell;
            
        }
        case button: {
            
            ButtonNextCell *cell= [tableView dequeueReusableCellWithIdentifier:@"cellButton" forIndexPath:indexPath];
            
            cell.delegate = self;
            
            return cell;
            
        }
        default: {
            
            IPTVCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellIPTV" forIndexPath:indexPath];
            
            return cell;
            
        }
    }
    
}
//MARK: - INTERNETCELLDELEGATE && TYPEOFSERVICECELLDELEGATE
-(void)dropDownButton:(id)sender kTag:(NSInteger)tag tagButton:(NSInteger)tagButton {
    
    switch (tag) {
            
        case internet: {
            
            if (tagButton == internetService) {
                //service
                [self setupDropDownView:internetService];

                break;
                
            }
            else if (tagButton == internetPromotion) {
                [delegate loadViewControllerWithArray:98 tagButton:1 array:arrInternetPromotion];

                
            } else if (tagButton == internetSuggest) {
            
                [delegate loadViewController:98 tagButton:2];
                
            }
            break;
        }
            
        case device:{
            break;
        }
            
        case styleService: {
            
            if (tagButton == 1) {
                //service
                [self setupDropDownView:styleService];
                
            } else if (tagButton == 2) {
                //package
                [self setupDropDownView:internetService];
                
                
            }
            
            break;
        }
            
        default:
            break;
    }
    
}

-(void)freshCompleted:(NSInteger)tag {
    
    switch (tag) {
        case styleService:
            [self addLocalTypeWithEnum:arrSaveLocalType];
            break;
        default:
            break;
    }
    
}

//MARK: -SearchPromotionDelegate
-(void)cancelSearchPromotionView:(PromotionModel *)selectedPromo {
    
    [[RegistrationFormModel sharedInstance] setInternetPromotion:selectedPromo];
    [myTableView reloadData];
    
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    
    int amountInternet = [[[RegistrationFormModel sharedInstance] internetPromotion].Amount intValue];
    
    [[RegistrationFormModel sharedInstance] setTotalInternet:amountInternet];
    
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
    
}

//MARK: -SuggestionsDelegate
- (void)CancelPopupSuggestions {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
}

//MARK: -IPTVCELDELEGATE

-(void)dropDownIPTVButton:(id)sender kTag:(NSInteger)tag tagButton:(NSInteger)tagButton {
    
    switch (tagButton) {
        case serviceBase: {
            [self setupDropDownView:serviceBase];
            break;
        }
        case deploy: {
            [self setupDropDownView:deploy];
            break;
        }
        case combo: {
            [self setupDropDownView:combo];
            break;
        }
        case service: {
            [self setupDropDownView:service];
            break;
        }
        case promotion1: {
            [self setupDropDownView:promotion1];
            break;
        }
        case promotion2: {
            [self setupDropDownView:promotion2];
            break;
        }
        case filmPlus: {
            
            break;
        }
        case filmHot: {
            
            break;
        }
        case filmKPlus: {
            
            break;
        }
        case filmHD: {
            
            break;
        }
        case filmVTVCab: {
            
            break;
        }
            
        default:
            break;
    }
    
}

/****************************/
//MARK: - UIPOPOVERLISTVIEW
/****************************/
- (void)setupDropDownView: (NSInteger) tag {
    [self.view endEditing:YES];
    
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView2 *poplistview = [[UIPopoverListView2 alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.tag = tag;
    poplistview.listView.scrollEnabled = TRUE;
    poplistview.userInteractionEnabled = TRUE;
    switch (tag) {
        case internetService:
            [poplistview setTitle:NSLocalizedString(@"Gói dịch vụ", @"")];
            break;
        case internetPromotion:
            [poplistview setTitle:NSLocalizedString(@"Gói dịch vụ", @"")];
            break;
        case serviceBase:
            [poplistview setTitle:NSLocalizedString(@"Gói dịch vụ", @"")];
            break;
        case deploy:
            [poplistview setTitle:NSLocalizedString(@"Hình thức triển khai", @"")];
            break;
        case combo:
            [poplistview setTitle:NSLocalizedString(@"Đăng ký combo", @"")];
            break;
        case service:
            [poplistview setTitle:NSLocalizedString(@"Gói dịch vụ", @"")];
            break;
        case promotion1:
            [poplistview setTitle:NSLocalizedString(@"Khuyến mãi Box đầu tiên", @"")];
            break;
        case promotion2:
            [poplistview setTitle:NSLocalizedString(@"Khuyến mãi Box thứ 2 trở đi", @"")];
            break;
        case styleService:
            [poplistview setTitle:NSLocalizedString(@"Loại dịch vụ", @"")];
            poplistview.multipleTouchEnabled = TRUE;
            break;
        default:
            break;
    }
    
    [poplistview show];
    
}

- (CGFloat)popoverListView:(UIPopoverListView2 *)popoverListView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

#pragma mark - UIPopoverListViewDataSource

- (void)setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell tag:(NSInteger)tag{
    KeyValueModel *model;
    PromotionModel *promotionmodel;
    cell.textLabel.numberOfLines = 0;
    [cell.textLabel sizeToFit];
    switch (tag) {
        case internetService:
            model = [self.arrInternetService objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case deploy:
            model = [self.arrDeployment objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case combo:
            model = [self.arrCombo objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case service:
            model = [self.arrPackageIPTV objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case promotion1:
            promotionmodel = [self.arrPromotionIPTV objectAtIndex:row];
            cell.textLabel.text = promotionmodel.PromotionName;
            break;
        case promotion2:
            promotionmodel = [self.arrPromotionIPTVBoxOrder objectAtIndex:row];
            cell.textLabel.text = promotionmodel.PromotionName;
            break;
        case styleService:
            model = [self.arrLocalType objectAtIndex:row];
            cell.textLabel.text = model.Values;
            
            for ( KeyValueModel *key in arrSaveLocalType ) {
                if (key.Key == model.Key) {
                    
                    cell.accessoryType = UITableViewCellAccessoryCheckmark;
                    break;
                }
            }
            break;
       default:
            break;
            
    }
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView2 *)popoverListView cellForIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row cell:cell tag:popoverListView.tag];
    cell.textLabel.font = [UIFont fontWithName:@"Arial" size:14];
    
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView2 *)popoverListView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = 0;
    switch (popoverListView.tag) {
        case internetService: {
            count = self.arrInternetService.count;
            break;
        }
        case deploy: {
            count = self.arrDeployment.count;
            break;
        }
        case combo: {
            count = self.arrCombo.count;
            break;
        }
        case service: {
            count = self.arrPackageIPTV.count;
            break;
        }
        case promotion1: {
            count = self.arrPromotionIPTV.count;
            break;
        }
        case promotion2: {
            count = self.arrPromotionIPTVBoxOrder.count;
            break;
        }
        case styleService: {
            count = self.arrLocalType.count;
            break;
        }

    }
    return count;
    
}

#pragma mark - UIPopoverListViewDelegate

//serviceBase = 6,
//deploy = 7,
//combo = 8,
//service = 9,
//promotion1 = 10,
//promotion2 = 11,
//filmPlus = 12,
//filmHot = 13,
//filmKPlus = 14,
//filmHD = 15,
//filmVTVCab = 16

- (void) didDropDownSelected:(NSInteger)row tag:(NSInteger)tag {
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    switch (tag) {
        case internetService:{
            NSLog(@"%ld",(long)row);
            
            KeyValueModel *modelInternet = [self.arrInternetService objectAtIndex:row];
            
            [[RegistrationFormModel sharedInstance] setInternetService:modelInternet];
            
            [self LoadPromotion:@"0" LocalType:modelInternet.Key];
            
            [myTableView reloadData];
            
            break;
        }
        case serviceBase: {
            
            break;
            
        }
        case deploy: {
            
            [[RegistrationFormModel sharedInstance] setIptvDeployment:[self.arrDeployment objectAtIndex:row]];
            [myTableView reloadData];
            break;
        }
            
        case combo: {
            
            [[RegistrationFormModel sharedInstance] setIptvCombo:[self.arrCombo objectAtIndex:row]];
            [myTableView reloadData];
            break;
            
        }
        case service: {
            
            [[RegistrationFormModel sharedInstance] setIptvPackageIPTV:[self.arrPackageIPTV objectAtIndex:row]];
            [myTableView reloadData];
            break;
            
        }
        case promotion1: {
            
            [[RegistrationFormModel sharedInstance] setIptvPromotionIPTV:[self.arrPromotionIPTV objectAtIndex:row]];
            [self loadIPTVPrice:[[RegistrationFormModel sharedInstance] iptvPromotionIPTV].Type ?: @"-1" contract:@"" RegCode:self.regcode ?: @"" promotionID:[[RegistrationFormModel sharedInstance] iptvPromotionIPTV].PromotionId ?:@""];
            [myTableView reloadData];
            break;
            
        }
        case promotion2: {
            ShareData *shared = [ShareData instance];
            [[RegistrationFormModel sharedInstance] setIptvPromotionIPTV2:[self.arrPromotionIPTVBoxOrder objectAtIndex:row]];
            //vutt11  Error not save promotion 2
             PromotionModel *selectedPromotion = [self.arrPromotionIPTVBoxOrder objectAtIndex:row];
            NSString *stringID = selectedPromotion.PromotionId;
            
            [[RegistrationFormModel sharedInstance] setPromotionIPTVBox2:stringID];
            shared.idBox2 = stringID;
            
//            [self loadIPTVPrice:[[RegistrationFormModel sharedInstance] iptvPromotionIPTV].Type ?: @"-1" contract:@"" RegCode:self.regcode ?: @""];
            
            [myTableView reloadData];
            break;
            
        }
        case styleService: {
            break;
        }
        default:
            break;
    }
}

-(BOOL)selectedLocalType:(KeyValueModel *)local indexPath:(NSIndexPath *)indexPath {
    
    for (KeyValueModel * key in arrSaveLocalType) {
        
        if (key.Key == local.Key) {
            //Remove KeyValueModel
            [self addLocalType:key isAdd:NO];
            //Return No đê bỏ checkmark
            return NO;
        }
        
    }
    //Add KeyValueModel
    [self addLocalType:local isAdd:YES];
    //Return Yes để checkmark
    return YES;
}

-(void)addLocalType:(KeyValueModel *)local isAdd:(BOOL)isAdd {
    
    
    if (isAdd == YES) {
    
            [arrSaveLocalType addObject:local];
            [[RegistrationFormModel sharedInstance] setLocalType:arrSaveLocalType];

    } else {
        
        //remove object
        for(KeyValueModel *key in arrSaveLocalType) {
            if (key.Key == local.Key) {
                [arrSaveLocalType removeObject:key];
                break;
            }
        }
        [[RegistrationFormModel sharedInstance] setLocalType:arrSaveLocalType];
    }
}

-(void)addLocalTypeWithEnum:(NSMutableArray *)arrLocal {
    
    NSArray *sortedArrLocal = [arrLocal sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        KeyValueModel *ob1 = (KeyValueModel *)obj1;
        KeyValueModel *ob2 = (KeyValueModel *)obj2;
        return [ob1.Key compare:ob2.Key options:NSNumericSearch];
    }];
    //inter, iptv. office. fpt play
    [arrStyleLocalCell removeAllObjects];
    [myTableView reloadData];
    
    coutCheckAdd = 0;
    coutCheckTableDeviceCell = 0;

    BOOL isInternet = false;
    BOOL isIptv = false;
    BOOL isOffice365 = false;
    BOOL isFPTPlay = false;
    BOOL isDevice = false;
    
    [self pressedAppendToCell:styleService];
    
    for (KeyValueModel *keyVa in sortedArrLocal) {
        
        if ([keyVa.Key  isEqual: @"0"]) {
            //internet
            [self pressedAppendToCell:internet];
            isInternet = true;
            
            
        } else if ([keyVa.Key isEqual:@"1"]) {
            //iptv
            [self pressedAppendToCell:iptv];
            isIptv = true;
            
        } else if ([keyVa.Key isEqual:@"3"]) {
            //office365
            [self pressedAppendToCell:office365];
            isOffice365 = true;
            
        }else if ([keyVa.Key isEqual:@"4"]) {
            //fptplay
            [self pressedAppendToCell:fptPlay];
            isFPTPlay = true;
        }
        //vutt 11 fix add checked Device
        
        else if ([keyVa.Key isEqual:@"5"]){
            [self pressedAppendToCell:device];
            isDevice = true;
        }
    }
    
    [self pressedAppendToCell:button];
    
    // luu lai 1 mang cell ban dau cho truong hop clicked nhieu lan
    for ( int i = 0; i<arrStyleLocalCell.count; i++) {
        NSString *string = StringFormat(@"%d",[arrStyleLocalCell[i] intValue]);
        
        if (![string isEqualToString:@"103"  ]) {
            
             [self.arraySaveLocalCell addObject:arrStyleLocalCell[i]];
        }
        
    }
    
    //truong hop cap nhat

    //check create new ListRegisterForm.
    if (self.Id.length <= 0 || [self.Id isEqualToString:@"0"]) {
        
        if (isInternet && !isIptv) {
//            [self LoadLocalTypePost:@"0" serverPackageID:@"0" PromotionId:@"0" Complete:^{}];
            [self loadServiceType:@"0" ServicePackageID:@"0" PromotionID:@"0"];
            isEnablePackageService = NO;
        } else if (!isInternet && isIptv) {
//            [self LoadLocalTypePost:@"1" serverPackageID:@"0" PromotionId:@"0" Complete:^{}];
            [self loadServiceType:@"1" ServicePackageID:@"0" PromotionID:@"0"];
            [self LoadPackageIPTV:[[RegistrationFormModel sharedInstance] iptvPackageIPTV].Key PromotionIPTV:[[RegistrationFormModel sharedInstance] iptvPromotionIPTV].PromotionId boxCount:@"1" Complete:^{}];
            isEnablePackageService = NO;
        } else if (isInternet && isIptv) {
//            [self LoadLocalTypePost:@"2" serverPackageID:@"0" PromotionId:@"0" Complete:^{}];
            [self loadServiceType:@"2" ServicePackageID:@"0" PromotionID:@"0"];
            [self LoadPackageIPTV:[[RegistrationFormModel sharedInstance] iptvPackageIPTV].Key PromotionIPTV:[[RegistrationFormModel sharedInstance] iptvPromotionIPTV].PromotionId boxCount:@"1" Complete:^{}];
            isEnablePackageService = NO;
        } else if (!isInternet && !isIptv && isOffice365) {
            isEnablePackageService = YES;
        } else if (isFPTPlay) {
            isEnablePackageService = YES;
        }
        
    } else if (regcode.length <= 0) {
        
        if (isInternet && !isIptv) {
//            [self LoadLocalTypePost:@"0" serverPackageID:@"0" PromotionId:@"0" Complete:^{}];
            [self loadServiceType:@"0" ServicePackageID:@"0" PromotionID:@"0"];
            isEnablePackageService = NO;
        } else if (!isInternet && isIptv) {
//            [self LoadLocalTypePost:@"1" serverPackageID:@"0" PromotionId:@"0" Complete:^{}];
            [self loadServiceType:@"1" ServicePackageID:@"0" PromotionID:@"0"];
            [self LoadPackageIPTV:[[RegistrationFormModel sharedInstance] iptvPackageIPTV].Key PromotionIPTV:[[RegistrationFormModel sharedInstance] iptvPromotionIPTV].PromotionId boxCount:@"1" Complete:^{}];
            isEnablePackageService = NO;
        } else if (isInternet && isIptv) {
//            [self LoadLocalTypePost:@"2" serverPackageID:@"0" PromotionId:@"0" Complete:^{}];
            [self loadServiceType:@"2" ServicePackageID:@"0" PromotionID:@"0"];
            [self LoadPackageIPTV:[[RegistrationFormModel sharedInstance] iptvPackageIPTV].Key PromotionIPTV:[[RegistrationFormModel sharedInstance] iptvPromotionIPTV].PromotionId boxCount:@"1" Complete:^{}];
            isEnablePackageService = NO;
        } else if (!isInternet && !isIptv && isOffice365) {
            isEnablePackageService = YES;
        } else if (isFPTPlay) {
            isEnablePackageService = YES;
        }
        
    } else {
        
        //StevenNguyen28/4
        if (isInternet && !isIptv) {
            isEnablePackageService = NO;
            [self LoadLocalTypePost:@"0" serverPackageID:packedInternet PromotionId:promotionID Complete:^{}];
        } else if (!isInternet && isIptv) {

            [self LoadLocalTypePost:@"1" serverPackageID:packedInternet PromotionId:promotionID Complete:^{
                
                [self LoadPackageIPTV:packedID PromotionIPTV:iptvIPTVPromotion boxCount:@"1" Complete:^{
                    
                    if (![iptvPromotionOrder isEqualToString:@""] && ![iptvPromotionOrder isEqualToString:@"0"]) {
                        
                        [self LoadPromotionIPTV:iptvPromotionOrder Package:packedID boxCount:iPTVBoxCount RegCode:self.regcode ?: @"" Complete:^{}];
                        
                    }
                    
                }];
                
            }];
            
            isEnablePackageService = NO;
        } else if (isInternet && isIptv) {
            isEnablePackageService = NO;

            [self LoadLocalTypePost:@"2" serverPackageID:packedInternet PromotionId:promotionID Complete:^{
                
                 [self LoadPackageIPTV:packedID PromotionIPTV:iptvIPTVPromotion boxCount:@"1" Complete:^{
                     
                     if (![iptvPromotionOrder isEqualToString:@""] && ![iptvPromotionOrder isEqualToString:@"0"]) {
                         
                         [self LoadPromotionIPTV:iptvPromotionOrder Package:packedID boxCount:iPTVBoxCount RegCode:self.regcode ?: @"" Complete:^{}];
                         
                     }
                     
                 }];
                
            }];
            
        } else if (!isInternet && !isIptv && isOffice365) {
            isEnablePackageService = YES;
        } else if (isFPTPlay) {
            isEnablePackageService = YES;
        }
    }
     
    [myTableView reloadData];
    
}

#pragma mark - UIPopoverListViewDelegate
//MARK: -checkbox muti
- (void)popoverListView:(UIPopoverListView2 *)popoverListView didSelectIndexPath:(NSIndexPath *)indexPath {
    
    switch (popoverListView.tag) {
        case styleService: {
            
            KeyValueModel *key = [self.arrLocalType objectAtIndex:indexPath.row];
            
            if ([key.Key isEqual: @"4"]) {
                
                NSArray *indexPaths = [popoverListView.listView indexPathsForVisibleRows];
                [arrSaveLocalType removeAllObjects];
                // remove di thiet bi da chon
                [self.arraySelectedListDevies removeAllObjects];
                [self.arrayDeviceChecked removeAllObjects];
                [self.ls.arraySelectedDevies removeAllObjects];
                [[RegistrationFormModel sharedInstance]setListDevicePackagesArray:self.arraySelectedListDevies];
                
                for (NSIndexPath *index in indexPaths) {
                        [[popoverListView.listView cellForRowAtIndexPath:index] setAccessoryType:UITableViewCellAccessoryNone];
                }
                if ([self selectedLocalType:key indexPath:indexPath]) {
                    [[popoverListView.listView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryCheckmark];
                }
                
                else {
                    [[popoverListView.listView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryNone];
                }
                
            } else if ([key.Key isEqual: @"5"]) {
                
                //So sánh xem đã có Key này trong mảng arrSaveLocalType
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.Key == %@ OR SELF.Key == %@",@"0",@"1"];
                NSArray *arrPredicated = [self.arrSaveLocalType filteredArrayUsingPredicate:predicate];
                
                //Log xem
                NSLog(@"HERE %@",arrPredicated);
                
                if (arrPredicated.count > 0) {
                    if ([self selectedLocalType:key indexPath:indexPath]) {/* Trường hợp Khi đã chọn Internet, IPTV */
                        [[popoverListView.listView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryCheckmark];
                    } else {/* Trường hợp Khi đã chọn Internet, IPTV. Nhưng không muốn mua Bán Thiết Bị */
                        [[popoverListView.listView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryNone];
                        
                        // remove di thiet bi da chon
                        [self.arraySelectedListDevies removeAllObjects];
                        [self.arrayDeviceChecked removeAllObjects];
                        [self.ls.arraySelectedDevies removeAllObjects];
                        [[RegistrationFormModel sharedInstance]setListDevicePackagesArray:self.arraySelectedListDevies];
                        
                    }

                } else {/* Trường hợp Khi chưa chọn Internet, IPTV */
                    [self showAlertBox:@"Thông báo" message:@"Vui lòng chọn Internet, IPTV"];
                    [[popoverListView.listView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryNone];
                }
                
                
            } else if ([key.Key isEqual: @"0"] || [key.Key isEqual: @"1"]) {
                
                //Kiểm tra trường hợp có FPT Play
                for (KeyValueModel *keyChild in arrSaveLocalType) {
                    if ([keyChild.Key isEqualToString:@"4"]) {
                        
                        // remove di thiet bi da chon
                        [self.arraySelectedListDevies removeAllObjects];
                        [self.arrayDeviceChecked removeAllObjects];
                        [self.ls.arraySelectedDevies removeAllObjects];
                        [[RegistrationFormModel sharedInstance]setListDevicePackagesArray:self.arraySelectedListDevies];
                        
                        [arrSaveLocalType removeAllObjects];
                        NSIndexPath *index = [NSIndexPath indexPathForItem:3 inSection:0];
                        [[popoverListView.listView cellForRowAtIndexPath:index] setAccessoryType:UITableViewCellAccessoryNone];
                    }
                }
                
                if ([self selectedLocalType:key indexPath:indexPath]) {
                    [[popoverListView.listView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryCheckmark];
                } else {
                    [[popoverListView.listView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryNone];
                }
                
                //So sánh xem đã có Key này trong mảng arrSaveLocalType
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.Key == %@ OR SELF.Key == %@",@"0",@"1"];
                NSArray *arrPredicated = [self.arrSaveLocalType filteredArrayUsingPredicate:predicate];
                
                //Log xem
                NSLog(@"HERE %@",arrPredicated);
                
                if (arrPredicated.count <= 0) {
                    KeyValueModel *keyDevice = [self.arrLocalType objectAtIndex:4];
                    NSIndexPath *indexPathDevice = [NSIndexPath indexPathForItem:4 inSection:0];
                    
                    //So sánh xem đã có Key(Bán thiết bị) này trong mảng arrSaveLocalType
                    NSPredicate *devicePredicate = [NSPredicate predicateWithFormat:@"SELF.Key == %@",@"5"];
                    NSArray *arrDevicePredicated = [self.arrSaveLocalType filteredArrayUsingPredicate:devicePredicate];
                    
                    if (arrDevicePredicated.count > 0) {
                        [[popoverListView.listView cellForRowAtIndexPath:indexPathDevice] setAccessoryType:UITableViewCellAccessoryNone];
                        [self addLocalType:keyDevice isAdd:NO];
                    } else {
                        NSLog(@"Qua day do");
                    }
                    
                }
                
            } else {
                
                //Kiểm tra trường hợp có FPT Play Box khi checkmark các trường Office ...
                for (KeyValueModel *keyChild in arrSaveLocalType) {
                    
                    if ([keyChild.Key isEqualToString:@"4"]) {
                        
                        [arrSaveLocalType removeAllObjects];
                        NSIndexPath *index = [NSIndexPath indexPathForItem:3 inSection:0];
                        [[popoverListView.listView cellForRowAtIndexPath:index] setAccessoryType:UITableViewCellAccessoryNone];
                    }
                }
                
                if ([self selectedLocalType:key indexPath:indexPath]) {
                    [[popoverListView.listView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryCheckmark];
                } else {
                    [[popoverListView.listView cellForRowAtIndexPath:indexPath] setAccessoryType:UITableViewCellAccessoryNone];
                }
                
                
            }
            
            [self addLocalTypeWithEnum:arrSaveLocalType];
            
            break;
            
        }
            
        default:
            [self didDropDownSelected:indexPath.row tag:popoverListView.tag];
            [popoverListView dismiss];
            break;
    }
}
//popoverListViewCancel
-(void)popoverListViewCancel:(UIPopoverListView2 *)popoverListView {
    
    if (popoverListView.tag == styleService) {
        
        if ([arrSaveLocalType count] <= 0) {
            
            [popoverListView dismiss];
            [self setupDropDownView:styleService];
            [self showAlertBox:@"Thông báo" message:@"Vui lòng chọn loại dịch vụ"];
        }
    }
    return;
}

//MARK: -OFFICE 365
-(void) dropDownButton: (id)sender kTag:(NSInteger)tag {
    
    switch (tag) {
        case office365:
            
            [delegate loadViewController:99 tagButton:99];
            
            break;
            
        default:
            break;
    }
}

//MARK: -UISCROLLVIEWDELEGATE
-(void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    float scrollViewHeight = scrollView.frame.size.height;
    float scrollContentSizeHeight = scrollView.contentSize.height;
    float scrollOffset = scrollView.contentOffset.y;
    
    if (scrollOffset == 0)
    {
        // then we are at the top
        [btnFloatingAction animationForButtonMoveBottomWithAnimation:NO];
    }
    else if (scrollOffset + scrollViewHeight == scrollContentSizeHeight)
    {
        [btnFloatingAction animationForButtonMoveBottomWithAnimation:YES];
        
    }
}

/***************************************/
//MARK: -ButtonNextCellDelegate
/***************************************/

-(void)pressedButton:(id)sender kTag:(NSInteger)tag ktagButton:(NSInteger)tagButton {
    
    switch (tagButton) {
        case 1:{
            //save
            [delegate scrollToView:3 tagButton:1];
            break;
        }
        case 2:{
            //back
            [delegate scrollToView:3 tagButton:2];
            break;
        }
        default:
            break;
    }
}

//MARK: -iptv
-(void) PressedBoxCount:(NSInteger)Count {
    
    KeyValueModel *selectedPackageIPTV = [[RegistrationFormModel sharedInstance] iptvPackageIPTV];
    
    NSString *strDeviceBoxCount = [[RegistrationFormModel sharedInstance] iptvDeviceBoxCount];
    
    [self LoadPromotionIPTV:@"0" Package:selectedPackageIPTV.Key boxCount:strDeviceBoxCount RegCode:self.regcode ?: @"" Complete:^{}];
    
    if (Count == 1) {
        [[RegistrationFormModel sharedInstance] setIptvPromotionIPTV2:nil];
        [myTableView reloadData];
    }
    
}
//vutt11
//DELEGATE OF DeviceEquipmentCell

- (void)scanFromScanViewController {
    
    //Load ScanViewController
    CameraViewController *scanVC = [[CameraViewController alloc] initWithNibName:@"ScanBarCodeViewController" bundle:nil];
    //            CameraViewController *scanVC = [[CameraViewController alloc] init];
    scanVC.delegate = self;
    [delegate loadScanViewController:101 listDeviceVC:scanVC delegate:self arrayDeviceChecked:nil];
//    [delegate loadViewController:101 tagButton:101];
    
}

//StevenNguyen CameraViewControllerDelegate
//func getMacOTTPlayBox(strMac:String)
-(void)getMacOTTPlayBoxWithStrMac:(NSString *)strMac {
    
    [delegate dismissViewController];
    
    if ([self checkFPTMacOTT:strMac] == NO) {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        [dic setValue:strMac forKey:@"MAC"];
        [self.arrayMACOTT addObject:dic];
        [myTableView reloadData];
    } else {
        [self showAlertBox:@"Thông báo " message:@"MAC đã có"];
    }
    
}

- (BOOL) checkFPTMacOTT:(NSString *)strMac {
    
    if (self.arrayMACOTT.count > 0) {
        for (NSDictionary *dictMacOTT in self.arrayMACOTT) {
            NSString *strMacOTT = [dictMacOTT valueForKey:@"MAC"];
            if ([strMac isEqualToString:strMacOTT]) {
                return YES;
            }
        }
        return NO;
    } else {
        return NO;
    }
    
}

-(void)dismissMacOTTPlayBox {
    [delegate dismissViewController];
}

-(void)changeMacFPTPlayOTTWithIndex:(NSInteger)index {
    
    [self.arrayMACOTT removeObjectAtIndex:index];
    [myTableView reloadData];
    
}

//Show List OTT ViewController
-(void)showListOTTViewController {
    
    ListDeviceOTTViewController *listOTTVC = [[ListDeviceOTTViewController alloc] initWithNibName:@"ListDeviceOTTViewController" bundle:nil];
    
//    [delegate loadViewController:111 listDeviceVC:listOTTVC delegate:self arrayDeviceChecked:self.arrayDeviceChecked];
    
    listOTTVC.arrDeviceChecked = self.arrayListOTT;
    listOTTVC.Id = self.Id;
    listOTTVC.delegate = self;
    [delegate loadOTTListDeviceOTTViewControllerViewController:111 listDeviceVC:listOTTVC delegate:self arrayDeviceChecked:self.arrayDeviceChecked];
    
}

- (void)changeValueFPTPlayOTTWithDic:(NSDictionary *)dic {
    
//    self.arrayListOTT
    self.arrayListOTT = dic;
    
}

//Delegate List Device OTT
- (void)cancelListDeviceOTTWithArrChecked:(NSArray<NSDictionary *> * _Nonnull)arrChecked {
    [self.delegate dismissViewController];
    
    self.arrayListOTT = arrChecked;
    
    [myTableView reloadData];
}

- (void)showListDevicesViewController {
  
    
   [delegate loadViewController:102 listDeviceVC:self.ls delegate:self arrayDeviceChecked:self.arrayDeviceChecked];
  
    self.ls.Id = self.Id;
   
    if ([[RegistrationFormModel sharedInstance]listDevicePackagesArray].count > 0) {
        
    }else {
         [self.ls.tableViewInfo reloadData];
    }
    
}

- (void)showListDevicesViewController2 {
    [delegate loadViewController:100 tagButton:100];
    
}

//DELEGATE OF ListDevieViewConller

- (void)cancelListDevices:(NSMutableArray *)arrayData {
    
    [self.delegate dismissViewController];
// truong hop cap nhat
   // if (isCheckShowUpdateListDevice == true && arrayData.count >0) {
    if (![self.Id isEqualToString:@"(null)"]){
        [self createdModelDevice:arrayData];
    }
   // }
// truong hop tao moi
    if ([self.Id isEqualToString:@"(null)"]) {
        [self cratedModelDeviceCreated:arrayData];
        
    }
   
    [myTableView reloadData];
    
    
}
//init Element Created 
- (void)cratedModelDeviceCreated:(NSMutableArray*)arrayData{
    [self.arraySelectedListDevies removeAllObjects];
    [[RegistrationFormModel sharedInstance]setListDevicePackagesArray:self.arraySelectedListDevies];
    
    for (int i = 0; i< arrayData.count; i++) {
        NSString *deviceName,*deviceID;
        deviceID = StringFormat(@"%@",[arrayData[i]valueForKey:@"DeviceID"]);
        deviceName = StringFormat(@"%@",[arrayData[i]valueForKey:@"DeviceName"]);
    
         [[RegistrationFormModel sharedInstance] setParamater:deviceID DeviceName:deviceName PromotionDeviceID:@"0" PromotionDeviceText:@"Vui lòng chọn CLKM" Number:@"1"];
       }
}

-(void)getNumberCount:(NSString *)stringNumberCount {
    [[RegistrationFormModel sharedInstance] setNumber:stringNumberCount];
    
}

- (void)upDateShowInfoListDevice:(NSMutableArray*)arrayListDevice {
  
    // lấy danh sách device đã chọn khi cập nhật để check table chọn
    self.arrayDeviceChecked = arrayListDevice;
    //lấy danh sách device đã chọn
    self.arrayListDevicesUpdate = arrayListDevice;
    isCheckShowUpdateListDevice = true;
    
   // truong hop vao cap nhat nhung ko chon gi ca
     [[RegistrationFormModel sharedInstance] setArrayListDevicesModel:arrayListDevice];
    // save lai danh sach thiet bi da chon roi chon them
    [[RegistrationFormModel sharedInstance] setArraySaveWhenUpdate:arrayListDevice];
    
    // Add listDevice get from API into sharedInstance
    
    for (int i = 0; i < arrayListDevice.count; i++) {
        NSString *number,*deviceID,*deviceName,*promotionDeviceID,*promotionDeviceText;
        number = [arrayListDevice[i] valueForKey:@"Number"];
        deviceID = [arrayListDevice[i]valueForKey:@"DeviceID"];
        deviceName = [arrayListDevice[i]valueForKey:@"DeviceName"];
        promotionDeviceID = [arrayListDevice[i]valueForKey:@"PromotionDeviceID"];
        promotionDeviceText = [arrayListDevice[i]valueForKey:@"PromotionDeviceText"];
        [[RegistrationFormModel sharedInstance] setParamater:deviceID DeviceName:deviceName PromotionDeviceID:promotionDeviceID PromotionDeviceText:promotionDeviceText Number:number];
        
    }
}
//Init element Update
- (void)createdModelDevice :(NSMutableArray *)array {
    
    [[RegistrationFormModel sharedInstance]setListDevicePackagesArray:array];
    
}


@end
