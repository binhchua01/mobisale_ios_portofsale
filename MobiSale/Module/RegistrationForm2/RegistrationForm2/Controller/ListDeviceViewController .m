//
//  ListDeviceViewController.m
//  MobiSale
//
//  Created by Tran Vu on 6/11/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import "ListDeviceViewController.h"
#import "KeyValueModel.h"
#import "Common_client.h"
#import "ListDeviceModel.h"
#import "ShareData.h"
#import "RegistrationFormModel.h"

@interface ListDeviceViewController ()<UITableViewDelegate,UITableViewDataSource>

//Danh sách thiết bị khi get về từ API
@property(nonatomic,strong) NSMutableArray *arrayData;

//Danh sách thiết bị người dùng chọn


@property (nonatomic ) BOOL isCheckShowUpdateListDevice;

@property (strong, nonatomic) NSMutableArray *arrayCheckMart;


@end

@implementation ListDeviceViewController{
    
    KeyValueModel *selectedDevies;
    ListDeviceModel *selectedListDevice;
    KeyValueModel *selectedListDeviceUpdate;
}
@synthesize delegate;

//init arrayData before using
- (NSMutableArray *)arrayData {
    if (!_arrayData) {
        _arrayData = [[NSMutableArray alloc] init];
    }
    return _arrayData;
}
- (NSMutableArray *)arrayCheckMart {
    if (!_arrayCheckMart) {
        _arrayCheckMart = [[NSMutableArray alloc] init];
    }
    return _arrayCheckMart;
}


- (NSMutableArray*)arraySelectedDevies {
    if (!_arraySelectedDevies) {
        _arraySelectedDevies = [[NSMutableArray alloc] init];
    }
    return _arraySelectedDevies;
}

- (NSMutableArray*)arrayDevieChecked {
    if (!_arrayDevieChecked) {
        _arrayDevieChecked = [[NSMutableArray alloc] init];
    }
    return _arrayDevieChecked;
}

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self loadListDevice:@"1"];
    
    [self setupTableview];
    
    [self style];
    
}

- (void)viewWillAppear:(BOOL)animated {
   // [self.tableViewInfo reloadData];
    
}

- (void)setupTableview {
    
    self.tableViewInfo.delegate = self;
    self.tableViewInfo.dataSource = self;
    self.tableViewInfo.allowsSelection = YES;
    
    [self.tableViewInfo setAllowsMultipleSelectionDuringEditing:YES];
    [self.tableViewInfo setAllowsSelectionDuringEditing:YES];
    [self.tableViewInfo setEditing:YES animated:YES];
    
}

- (void)style {
    
    self.btnClose.layer.cornerRadius = 5;
    self.btnClose.layer.cornerRadius = 5;
    
    self.btnClose.layer.shadowOffset = CGSizeMake(0, 3);
    self.btnClose.layer.shadowColor = [UIColor blackColor].CGColor;
    self.btnClose.layer.shadowRadius = 1.0;
    self.btnClose.layer.shadowOpacity = .5;
    
}


- (IBAction)btnCanCel_cliked:(id)sender {
    
    [self.delegate cancelListDevices:self.arraySelectedDevies];
    
}
- (IBAction)btnClose_clicked:(id)sender {
    [self.delegate cancelListDevices:self.arraySelectedDevies];
    
}

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
    
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
  return  self.arrayData.count;
    
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
    }
    
    if (self.arrayData.count > 0) {
        
        cell.textLabel.text = [self.arrayData [indexPath.row]valueForKey:@"DeviceName"];
    }
    
    //cap nhat
    NSString *deviceID = StringFormat(@"%@",[self.arrayData[indexPath.row] valueForKey:@"DeviceID"]);
    if (![self.Id isEqualToString:@"(null)"] && !(self.Id == nil)) {
        
        int i ;
        //vu fixed bo check khi cap nhat
        //self.arraySelectedDevies = self.arrayDevieChecked;
        if (![self.arrayDevieChecked isEqual: [NSNull null]]) {
            for (i = 0; i<self.arrayDevieChecked.count  ; i++){
                
                NSString *deviceIDUpdate = StringFormat(@"%@",[self.arrayDevieChecked[i] valueForKey:@"DeviceID"]);
                if (deviceIDUpdate == deviceID) {
                    
                    [self.tableViewInfo selectRowAtIndexPath:indexPath animated:TRUE scrollPosition:UITableViewScrollPositionNone];
                    [self.arraySelectedDevies addObject:self.arrayDevieChecked[i]];
                    
                    break;
                }
            }
        }
        
        
    }
    
    return cell;
}


- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 3;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //lấy DeviceID của mảng arrayData từ API
    NSString *deviceID = StringFormat(@"%@",[self.arrayData[indexPath.row] valueForKey:@"DeviceID"]);
    
    //So sánh xem đã có DeviceID này trong mảng arraySelectedDevies
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.DeviceID == %@",deviceID];
    NSArray *arrPredicated = [self.arraySelectedDevies filteredArrayUsingPredicate:predicate];
    
    //Log xem
    NSLog(@"HERE %@",arrPredicated);
    
    //nếu arrPredicated = 0 là chưa addObject, ngược lại...
    if (arrPredicated.count <= 0) {
        
        NSString *deviceID = StringFormat(@"%@",[self.arrayData[indexPath.row] valueForKey:@"DeviceID"]);
        NSString *deviceName = StringFormat(@"%@",[self.arrayData[indexPath.row] valueForKey:@"DeviceName"]);
        NSMutableDictionary *dict;
        dict = [[NSMutableDictionary alloc] init];
        
        [dict setValue:deviceID forKey:@"DeviceID"];
        [dict setValue:deviceName forKey:@"DeviceName"];
        
        //init
        [dict setValue:@"0" forKey:@"PromotionDeviceID"];
        [dict setValue:@"Vui lòng chọn CLKM" forKey:@"PromotionDeviceText"];
        [dict setValue:@"1" forKey:@"Number"];
        
        [self.arraySelectedDevies addObject:dict];
    }
   
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //lấy DeviceID của mảng arrayData từ API
    NSString *deviceID = StringFormat(@"%@",[self.arrayData[indexPath.row] valueForKey:@"DeviceID"]);
    
    //Search trong arraySelectedDevies Neu Co Remove...
    for (int i = 0; i<self.arraySelectedDevies.count; i++) {
        NSString *deviceIDUpdate = StringFormat(@"%@",[self.arraySelectedDevies[i] valueForKey:@"DeviceID"]);
        if (deviceIDUpdate == deviceID) {
            NSUInteger inter = i;
            [self.arraySelectedDevies removeObjectAtIndex:inter];
        }
    }
}

- (void)loadListDevice :(NSString *)type  {
    //kiem tra ket noi Internet
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    ShareData *shared = [ShareData instance];
    [shared.appProxy GetListDevice:type Handler:^(id result,NSString *errorCode,NSString *message){
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return ;
        }
        
        if ([message isEqualToString:@"Error Service: Unauthorized"]) {
             [self ShowAlertErrorSession];
             [self hideMBProcess];
            
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            
            return ;
        }
        
        NSArray *arrData = result;
        if (arrData.count <= 0) {
            [self hideMBProcess];
            return;
            
        } else {
            
            self.arrayData = [NSMutableArray arrayWithArray:arrData];
            [self.tableViewInfo reloadData];
        }
        
    } errorHandler:^(NSError *error){
        
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetListDevice",[error localizedDescription]]];
        [self hideMBProcess];
        
    }];
    
}


@end
