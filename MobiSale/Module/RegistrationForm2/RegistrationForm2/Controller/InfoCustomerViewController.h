//
//  InfoCustomerViewController.h
//  MobiSale
//
//  Created by Bored Ninjas on 11/18/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TDDatePicker.h"
#import "UIPopoverListView.h"
#import "EGOCache.h"

@protocol InfoCustomerViewControllerDelegate <NSObject>

-(void) showButton: (id)sender kTag:(NSInteger)tag;

-(void) scrollToView:(NSInteger)tag tagButton:(NSInteger)tagButton;

-(void) loadImageViewController;

-(void) showMBProcessListRegis;

-(void) hideMBProcessListRegis;

-(void) logOutWhenEndSession;

@end

@interface InfoCustomerViewController : TDDatePicker<UIPopoverListViewDataSource, UIPopoverListViewDelegate>

@property (nonatomic, weak) id <InfoCustomerViewControllerDelegate> delegate;

@property (strong, nonatomic) UIImagePickerController *imagePickerController;

@property (retain, nonatomic) NSMutableArray *arrCell;
@property (retain, nonatomic) NSMutableArray *arrType;
@property (retain, nonatomic) NSMutableArray *arrTypeCustomer;
@property (retain, nonatomic) NSMutableArray *arrDistrict;
@property (retain, nonatomic) NSMutableArray *arrWard;
@property (retain, nonatomic) NSMutableArray *arrStreet;
@property (retain, nonatomic) NSMutableArray *arrNameVilla;
@property (retain, nonatomic) NSMutableArray *arrTypeHouse;
@property (retain, nonatomic) NSMutableArray *arrTypePhone;
@property (retain, nonatomic) NSMutableArray *arrTypePhone2;
@property (retain, nonatomic) NSMutableArray *arrHousePosition;

@property (strong, nonatomic) NSString *Id;

@property (strong, nonatomic) NSString *NameVilla;

-(void)LoadPhone1:(NSString *) Id;

-(void)LoadPhone2:(NSString *) Id;

-(void)LoadTypeCustomer:(NSString *) Id;

-(void)LoadDistrict:(NSString *)Id WardId:(NSString *)wardid Street:(NSString *)street;

-(void)LoadTypeHouse:(NSString *) Id;

-(void)LoadPosition:(NSString *)Id;

-(void)LoadCusType:(NSString *) Id;

@end
