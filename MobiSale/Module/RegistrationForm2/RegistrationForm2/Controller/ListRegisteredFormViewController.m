//
//  ListRegisteredFormViewController.m
//  MobiSale
//
//  Created by Bored Ninjas on 11/18/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.

#import "ListRegisteredFormViewController.h"
#import "InfoCustomerViewController.h"
#import "CustomersViewController.h"
#import "AddressViewController.h"
#import "StyleOfServiceViewController.h"
#import "PaymentViewController.h"
#import "ReputationView.h"
#import "MobiSale-Swift.h"
#import "RegistrationFormModel.h"
#import "Office365ViewController.h"
#import "SuggestionsSearchViewController.h"
#import "SearchPromotionViewController.h"
#import "ListDeviceViewController.h"
#import "DetailRegisteredForm.h"
#import "ListRegistrationFormPageViewController.h"

@implementation UIColor (HexString)

+ (UIColor *)colorFromHexString:(NSString *)hexString {
    
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1];
    [scanner scanHexInt:&rgbValue];
    
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

@end

enum StyleCell{
    
    type,
    typeCustomer,
    name,
    isCard,
    date,
    address,
    taxCode,
    typePhone1,
    numberPhone1,
    typePhone2,
    numberPhone2,
    email,
    county,
    ward,
    street,
    typeOfHouse,
    regulationAddress,
    housePosition,
    nameApartment,
    apartment,
    numberOfHouse,
    noteAddress,
    notePlant,
    button
    
};

@interface ListRegisteredFormViewController ()<UIScrollViewDelegate,ABSteppedProgressBarDelegate, InfoCustomerViewControllerDelegate, StyleOfServiceViewControllerDelegate,CustomersViewControllerDelegate, PaymentViewControllerDelegate, Office365ViewControllerDelegate,SuggestionsDelegate,SearchPromotionDelegate,ListDeviceViewControllerDelegate,ListDeviceEquipmentCellDelegate, CameraViewControllerDelegate> {
    
    MBProgressHUD *HUD;
    
    __weak IBOutlet ABSteppedProgressBar *stepperView;
    
    __weak IBOutlet UILabel *lblTitle;
    
    __weak IBOutlet UILabel *lblStepOf;
    
    //list ViewController...
    NSMutableArray *arrReputationView;
}
@property (strong, nonatomic) Office365ViewController *office365ViewController;
@property (strong, nonatomic) dispatch_queue_t serialQueue;
@property (strong, nonatomic) ListDeviceViewController *ls;
@property (strong, nonatomic) NSMutableArray *arrayData;

@end

@implementation ListRegisteredFormViewController {
    
    InfoCustomerViewController *infoCustomerVC;
    CustomersViewController *customersVC;
    StyleOfServiceViewController *styleVC;
    PaymentViewController *payVC;
    RegistrationFormDetailRecord * rc;
    int nbInfo, nbCustomer, nbStyle, nbPayment;
    NSInteger office365Total;
    
    
}
@synthesize rc;
@synthesize delegate;

- (NSMutableArray *)arrayData {
    
    if (!_arrayData) {
        _arrayData = [[NSMutableArray alloc] init];
    }
    return _arrayData;
    
}

- (ListDeviceViewController *)ls {
    
    if (!_ls) {
        _ls = [[ListDeviceViewController alloc] init];
    }
    return _ls;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.regcode = rc.RegCode;
    office365Total = 0;
    self.ls.delegate = self;
    
    //vutt11
    //Using Observer from ListDeviceEquipmentCell (why not OOP BaseViewController )
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(showMBProcessListRegis) name:@"ShowMBProcessListRegis" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(hideMBProcessListRegis) name:@"HideMBProcessListRegis" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(logOutWhenEndSession) name:@"LogOutWhenEndSession" object:nil];
    
    UIFont *font = [UIFont fontWithName:@"Avenir Next" size:18];
    UIColor *colorWhite = [UIColor whiteColor];
    NSDictionary *settingTextAttributes = @{
                                            NSForegroundColorAttributeName : colorWhite,
                                            NSFontAttributeName : font
                                            };
    
    self.navigationController.navigationBar.titleTextAttributes = settingTextAttributes;
    self.navigationController.navigationBar.barTintColor = [UIColor colorFromHexString:@"22B2A8"];
    self.serialQueue = dispatch_queue_create("com.vutt11.queue", DISPATCH_QUEUE_SERIAL);
    
    if (self.Id.length <= 0 || [self.Id isEqualToString:@"0"]) {
        
        self.title = @"Tạo phiếu đăng ký";
        [RegistrationFormModel selfDestruct];
        [self dataStatic];
        [self setupScrollView];
        [self configStepperView];
        
    } else {
        
        if (self.regcode.length <= 0) {
            
            self.title = @"Tạo phiếu đăng ký";
            [RegistrationFormModel selfDestruct];
            [self dataStatic];
            [self setupScrollView];
            [self configStepperView];
            
        } else {
            
            [RegistrationFormModel selfDestruct];
            
            [self dataStatic];
            [self setupScrollView];
            [self configStepperView];
            self.title  = @"CẬP NHẬT PHIẾU ĐK";
            office365Total = self.rc.office365Total;
            [[RegistrationFormModel sharedInstance] setTotalOffice365:office365Total];
            
        }
        
    }
}

-(void) loadUpdateWithPage:(NSInteger)page {
    
    if (self.Id.length <= 0 || [self.Id isEqualToString:@"0"] || [self.Id isEqualToString:@"(null)"] ) {
        
        return;
    }
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;

    if (page == 0) {
        
        if (nbInfo == 1) {
            return;
        }
        //vutt11 fixed
        //Get data from Model
        nbInfo = 1;
        
        [[RegistrationFormModel sharedInstance] setDate:[self convertDateToString:[self convertStringToDate:self.rc.Birthday]]];
        [[RegistrationFormModel sharedInstance] setAddress:self.rc.AddressPassport];
        [[RegistrationFormModel sharedInstance] setIndoor:self.rc.InDoor];
        [[RegistrationFormModel sharedInstance] setOutdoor:self.rc.OutDoor];
        
        [[RegistrationFormModel sharedInstance] setContact1:self.rc.Contract];
        [[RegistrationFormModel sharedInstance] setName:self.rc.FullName];
        [[RegistrationFormModel sharedInstance] setIsCard:self.rc.Passport];
        [[RegistrationFormModel sharedInstance] setTaxCode:self.rc.TaxId];
        
        [[RegistrationFormModel sharedInstance] setNumberPhone1:self.rc.Phone_1];
        [[RegistrationFormModel sharedInstance] setContact1:self.rc.Contact_1];
        
        [[RegistrationFormModel sharedInstance] setNumberPhone2:self.rc.Phone_2];
        [[RegistrationFormModel sharedInstance] setContact2:self.rc.Contact_2];
        
        [[RegistrationFormModel sharedInstance] setNotePlant:self.rc.DescriptionIBB];
        [[RegistrationFormModel sharedInstance] setEmail:self.rc.Email];
        
        [[RegistrationFormModel sharedInstance] setNumberOfHouse:self.rc.BillTo_Number];
        [[RegistrationFormModel sharedInstance] setLot:self.rc.Lot];
        [[RegistrationFormModel sharedInstance] setFloor:self.rc.Floor];
        [[RegistrationFormModel sharedInstance] setRoom:self.rc.Room];
        [[RegistrationFormModel sharedInstance] setNoteAddress:self.rc.Note];
        [[RegistrationFormModel sharedInstance] setIptvChargeTimes:self.rc.IPTVChargeTimes];
        
        [[RegistrationFormModel sharedInstance] setTotalIPTV:[self.rc.IPTVTotal intValue]];
        [[RegistrationFormModel sharedInstance] setTotalInternet:[self.rc.InternetTotal intValue]];
        
        [[RegistrationFormModel sharedInstance] setTotalAmount:[self.rc.Total intValue]];
        
        [[RegistrationFormModel sharedInstance] setIptvFilmPlusChargeTime:self.rc.IPTVFimPlusChargeTimes];
        [[RegistrationFormModel sharedInstance] setIptvFilmPlusPrepaidMonth:self.rc.IPTVFimPlusPrepaidMonth];
        [[RegistrationFormModel sharedInstance] setIptvFilmHotChargeTime:self.rc.IPTVFimHotChargeTimes];
        [[RegistrationFormModel sharedInstance] setIptvFilmHotPrepaidMonth:self.rc.IPTVFimHotPrepaidMonth];
        [[RegistrationFormModel sharedInstance] setIptvFilmKPlusChargeTime:self.rc.IPTVKPlusChargeTimes];
        [[RegistrationFormModel sharedInstance] setIptvFilmKPlusPrepaidMonth:self.rc.IPTVKPlusPrepaidMonth];
        [[RegistrationFormModel sharedInstance] setIptvFilmVTCChargeTime:self.rc.IPTVVTCChargeTimes];
        [[RegistrationFormModel sharedInstance] setIptvFilmVTCPrepaidMonth:self.rc.IPTVVTCPrepaidMonth];
        [[RegistrationFormModel sharedInstance] setIptvFilmVTVChargeTime:self.rc.IPTVVTVChargeTimes];
        [[RegistrationFormModel sharedInstance] setIptvFilmVTVPrepaidMonth:self.rc.IPTVVTVPrepaidMonth];
        [[RegistrationFormModel sharedInstance] setIptvDeviceBoxCount:self.rc.IPTVBoxCount];
        [[RegistrationFormModel sharedInstance] setIptvDevicePLCCount:self.rc.IPTVPLCCount];
        [[RegistrationFormModel sharedInstance] setIptvDeviceSTBCount:self.rc.IPTVReturnSTBCount];
        [[RegistrationFormModel sharedInstance] setIptvFilmPlusChargeTime:self.rc.IPTVFimPlusChargeTimes];
        [[RegistrationFormModel sharedInstance] setIptvFilmHotChargeTime:self.rc.IPTVFimHotChargeTimes];
        [[RegistrationFormModel sharedInstance] setIptvFilmKPlusChargeTime:self.rc.IPTVKPlusChargeTimes];
        [[RegistrationFormModel sharedInstance] setIptvFilmVTVChargeTime:self.rc.IPTVVTVChargeTimes];
        [[RegistrationFormModel sharedInstance] setIptvFilmVTCChargeTime:self.rc.IPTVVTCChargeTimes];
        [[RegistrationFormModel sharedInstance] setCableStatus:self.rc.CableStatus];
        [[RegistrationFormModel sharedInstance] setIsIPTV:self.rc.INSCable];
        [[RegistrationFormModel sharedInstance] setIptvRequest:self.rc.IPTVRequestSetUp];
        [[RegistrationFormModel sharedInstance] setIptvWallDrilling:self.rc.IPTVRequestDrillWall];
        [[RegistrationFormModel sharedInstance] setImgInfo:self.rc.imageInfo];
        
        //InfoCustomerViewController
        [infoCustomerVC LoadPhone1:self.rc.Type_1];
        [infoCustomerVC LoadPhone2:self.rc.Type_2];
        [infoCustomerVC LoadTypeCustomer:self.rc.CusTypeDetail];
        [infoCustomerVC LoadDistrict:self.rc.BillTo_District WardId:self.rc.BillTo_Ward Street:self.rc.BillTo_Street];
        infoCustomerVC.NameVilla = self.rc.NameVilla;
        [infoCustomerVC LoadTypeHouse:self.rc.TypeHouse];
        [infoCustomerVC LoadPosition:self.rc.Position];
        
        if ([self.rc.CusType  isEqualToString: @""] || self.rc.CusType == nil) {
            [infoCustomerVC LoadCusType:@"0"];
        } else {
            [infoCustomerVC LoadCusType:self.rc.CusType];
        }
        
        [payVC getImageWithUrl:self.rc.imageInfo title:self.rc.imageInfo];
        
        
    } else if (page == 1) {
        
        if (nbCustomer == 1) {
            return;
        }
        nbCustomer = 1;
        //CustomerViewController
        
        [customersVC LoadObjectCustomer:self.rc.ObjectType];
        [customersVC LoadISP:self.rc.ISPType];
        [customersVC LoadCurrenPlace:self.rc.CurrentHouse];
        [customersVC LoadSolveny:self.rc.PaymentAbility];
        [customersVC LoadClientPartner:self.rc.PartnerID];
        [customersVC LoadLegal:self.rc.LegalEntity];
        [customersVC reloadDataViewController];
        
        if (self.regcode.length <= 0) {
            return;
        }
        //StyleOfServiceViewController
        NSMutableArray *arrayListDevice = rc.ListDevice;
        [[RegistrationFormModel sharedInstance] setArrayListDevicesUpdate:arrayListDevice];
        if (![arrayListDevice isEqual: [NSNull null] ]) {
            
            [styleVC upDateShowInfoListDevice:arrayListDevice];
        }
        NSMutableArray *arrLocalType = [[RegistrationFormModel sharedInstance] localType];
        NSInteger boxCount = [self.rc.OTTBoxCount integerValue];
        
        [[RegistrationFormModel sharedInstance] setCountFPTPlay:boxCount];
        
        //StevenNguyen28/4
        [styleVC setPackageForInternet:self.rc.LocalType promotionIPTV:self.rc.IPTVPromotionID promotion:self.rc.IPTVPromotionIDBoxOrder countBox:self.rc.IPTVBoxCount];
        
        //load Deployment and combo
        [styleVC LoadDeployment:self.rc.IPTVStatus];
        [styleVC LoadCombo:self.rc.IPTVUseCombo];
        
        // FPT Play Box New
        if (rc.ListPlayBoxOTT.count > 0) {
            styleVC.arrayListOTT = rc.ListPlayBoxOTT;
            styleVC.arrayMACOTT = rc.ListMACPlayBoxOTT;
            [styleVC LocaLocalType:@"4" Package:@"4" Officetotal:0 ArrayDeviceChecked:0];
            return;
        } else {
            
            [styleVC LocaLocalType:self.rc.PromotionID Package:self.rc.IPTVPackage Officetotal:office365Total ArrayDeviceChecked:arrayListDevice];
            
        }
        
//        // FPT Play Box
//        if (boxCount > 0) {
//            
//            [styleVC LocaLocalType:@"4" Package:@"4" Officetotal:0 ArrayDeviceChecked:0];
//            return;
//            
//        } else {
//        
//            [styleVC LocaLocalType:self.rc.PromotionID Package:self.rc.IPTVPackage Officetotal:office365Total ArrayDeviceChecked:arrayListDevice];
//            
//        }
       
        BOOL isInternet = NO;
        BOOL isIPTV = NO;
        
        for ( KeyValueModel *key in arrLocalType ) {
            if ([key.Key  isEqual: @"0"]) {
                isInternet = YES;
            } else if ([key.Key isEqual:@"1"]) {
                isIPTV = YES;
            }
        }
        [styleVC reloadTableView];
        
    } else if (page == 2) {
        
        if (nbStyle == 1) {
            return;
        }
        nbStyle = 1;
        [styleVC reloadTableView];

    } else {
        if (nbPayment == 1) {
            return;
        }
        nbPayment = 1;
        
        //MARK: -StevenNguyen-FixError
        [payVC loadCachePaymentType:self.rc.Payment];
//       [payVC loadPaymentType:self.rc.Payment];
        
       [payVC LoadDeposit:self.rc.Deposit];
       [payVC LoadDepositBlackPoint:self.rc.DepositBlackPoint];
        
        }
}

- (void)loadUpdate {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    [[RegistrationFormModel sharedInstance] setDate:[self convertDateToString:[self convertStringToDate:self.rc.Birthday]]];
    [[RegistrationFormModel sharedInstance] setAddress:self.rc.AddressPassport];
    [[RegistrationFormModel sharedInstance] setIndoor:self.rc.InDoor];
    [[RegistrationFormModel sharedInstance] setOutdoor:self.rc.OutDoor];
    
    [styleVC LocaLocalType:self.rc.PromotionID Package:self.rc.IPTVPackage Officetotal:office365Total ArrayDeviceChecked:0];
    [[RegistrationFormModel sharedInstance] setContact1:self.rc.Contract];
    [[RegistrationFormModel sharedInstance] setName:self.rc.FullName];
    [[RegistrationFormModel sharedInstance] setIsCard:self.rc.Passport];
    [[RegistrationFormModel sharedInstance] setTaxCode:self.rc.TaxId];
    [infoCustomerVC LoadPhone1:self.rc.Type_1];
    
    [[RegistrationFormModel sharedInstance] setNumberPhone1:self.rc.Phone_1];
    [[RegistrationFormModel sharedInstance] setContact1:self.rc.Contact_1];
    [infoCustomerVC LoadPhone2:self.rc.Type_2];
    [[RegistrationFormModel sharedInstance] setNumberPhone2:self.rc.Phone_2];
    [[RegistrationFormModel sharedInstance] setContact2:self.rc.Contact_2];
    
    [[RegistrationFormModel sharedInstance] setNotePlant:self.rc.DescriptionIBB];
    [[RegistrationFormModel sharedInstance] setEmail:self.rc.Email];
    [customersVC LoadObjectCustomer:self.rc.ObjectType];
    [customersVC LoadISP:self.rc.ISPType];
    [infoCustomerVC LoadTypeCustomer:self.rc.CusTypeDetail];
    [customersVC LoadCurrenPlace:self.rc.CurrentHouse];
    [customersVC LoadSolveny:self.rc.PaymentAbility];
    [customersVC LoadClientPartner:self.rc.PartnerID];
    [customersVC LoadLegal:self.rc.LegalEntity];
    [infoCustomerVC LoadDistrict:self.rc.BillTo_District WardId:self.rc.BillTo_Ward Street:self.rc.BillTo_Street];
    infoCustomerVC.NameVilla = self.rc.NameVilla;
    [infoCustomerVC LoadTypeHouse:self.rc.TypeHouse];
    [infoCustomerVC LoadPosition:self.rc.Position];
    [[RegistrationFormModel sharedInstance] setNumberOfHouse:self.rc.BillTo_Number];
    [[RegistrationFormModel sharedInstance] setLot:self.rc.Lot];
    [[RegistrationFormModel sharedInstance] setFloor:self.rc.Floor];
    [[RegistrationFormModel sharedInstance] setRoom:self.rc.Room];
    [[RegistrationFormModel sharedInstance] setNoteAddress:self.rc.Note];
    [[RegistrationFormModel sharedInstance] setIptvChargeTimes:self.rc.IPTVChargeTimes];
    
    [[RegistrationFormModel sharedInstance] setTotalIPTV:[self.rc.IPTVTotal intValue]];
    [[RegistrationFormModel sharedInstance] setTotalInternet:[self.rc.InternetTotal intValue]];
    
    [[RegistrationFormModel sharedInstance] setTotalAmount:[self.rc.Total intValue]];
    
    [[RegistrationFormModel sharedInstance] setIptvFilmPlusChargeTime:self.rc.IPTVFimPlusChargeTimes];
    [[RegistrationFormModel sharedInstance] setIptvFilmPlusPrepaidMonth:self.rc.IPTVFimPlusPrepaidMonth];
    [[RegistrationFormModel sharedInstance] setIptvFilmHotChargeTime:self.rc.IPTVFimHotChargeTimes];
    [[RegistrationFormModel sharedInstance] setIptvFilmHotPrepaidMonth:self.rc.IPTVFimHotPrepaidMonth];
    [[RegistrationFormModel sharedInstance] setIptvFilmKPlusChargeTime:self.rc.IPTVKPlusChargeTimes];
    [[RegistrationFormModel sharedInstance] setIptvFilmKPlusPrepaidMonth:self.rc.IPTVKPlusPrepaidMonth];
    [[RegistrationFormModel sharedInstance] setIptvFilmVTCChargeTime:self.rc.IPTVVTCChargeTimes];
    [[RegistrationFormModel sharedInstance] setIptvFilmVTCPrepaidMonth:self.rc.IPTVVTCPrepaidMonth];
    [[RegistrationFormModel sharedInstance] setIptvFilmVTVChargeTime:self.rc.IPTVVTVChargeTimes];
    [[RegistrationFormModel sharedInstance] setIptvFilmVTVPrepaidMonth:self.rc.IPTVVTVPrepaidMonth];
    [[RegistrationFormModel sharedInstance] setIptvDeviceBoxCount:self.rc.IPTVBoxCount];
    [[RegistrationFormModel sharedInstance] setIptvDevicePLCCount:self.rc.IPTVPLCCount];
    [[RegistrationFormModel sharedInstance] setIptvDeviceSTBCount:self.rc.IPTVReturnSTBCount];
    
    NSMutableArray *arrLocalType = [[RegistrationFormModel sharedInstance] localType];
    
    BOOL isInternet = NO;
    BOOL isIPTV = NO;
    
    for ( KeyValueModel *key in arrLocalType ) {
        if ([key.Key  isEqual: @"0"]) {
            isInternet = YES;
        } else if ([key.Key isEqual:@"1"]) {
            isIPTV = YES;
        }
    }
    
    if (isInternet == YES) {
        [styleVC LoadLocalTypePost:@"0" serverPackageID:self.rc.LocalType PromotionId:self.rc.PromotionID];
    } else {
        [styleVC LoadLocalTypePost:@"1" serverPackageID:self.rc.LocalType PromotionId:self.rc.PromotionID];
    }
    
    if(![self.rc.IPTVPackage isEqualToString:@""]){
        
        [styleVC LoadPackageIPTV:self.rc.IPTVPackage PromotionIPTV:self.rc.IPTVPromotionID boxCount:@"1"];
        
        if (![self.rc.IPTVPromotionIDBoxOrder isEqualToString:@""] && ![self.rc.IPTVPromotionIDBoxOrder isEqualToString:@"0"]) {
            
            [styleVC LoadPromotionIPTV:self.rc.IPTVPromotionIDBoxOrder Package:self.rc.IPTVPackage boxCount:self.rc.IPTVBoxCount RegCode:self.regcode];
//            [styleVC LoadPromotionIPTV:self.rc.IPTVPromotionIDBoxOrder Package:self.rc.IPTVPackage boxCount:self.rc.IPTVBoxCount];
            
            
        }
    }
    
    [styleVC LoadDeployment:self.rc.IPTVStatus];
    
    [infoCustomerVC LoadCusType:self.rc.CusType];
    [styleVC LoadCombo:self.rc.IPTVUseCombo];
    [[RegistrationFormModel sharedInstance] setIptvFilmPlusChargeTime:self.rc.IPTVFimPlusChargeTimes];
    [[RegistrationFormModel sharedInstance] setIptvFilmHotChargeTime:self.rc.IPTVFimHotChargeTimes];
    [[RegistrationFormModel sharedInstance] setIptvFilmKPlusChargeTime:self.rc.IPTVKPlusChargeTimes];
    [[RegistrationFormModel sharedInstance] setIptvFilmVTVChargeTime:self.rc.IPTVVTVChargeTimes];
    [[RegistrationFormModel sharedInstance] setIptvFilmVTCChargeTime:self.rc.IPTVVTCChargeTimes];
    
    //MARK: -StevenNguyen-FixError
    [payVC loadCachePaymentType:self.rc.Payment];
    [payVC loadPaymentType:self.rc.Payment];
    [payVC LoadDeposit:self.rc.Deposit];
    [payVC LoadDepositBlackPoint:self.rc.DepositBlackPoint];
    
    [[RegistrationFormModel sharedInstance] setCableStatus:self.rc.CableStatus];
    [[RegistrationFormModel sharedInstance] setIsIPTV:self.rc.INSCable];
    [[RegistrationFormModel sharedInstance] setIptvRequest:self.rc.IPTVRequestSetUp];
    [[RegistrationFormModel sharedInstance] setIptvWallDrilling:self.rc.IPTVRequestDrillWall];
    
    [[RegistrationFormModel sharedInstance] setImgInfo:self.rc.imageInfo];
    
    [payVC getImageWithUrl:self.rc.imageInfo title:self.rc.imageInfo];
    
    int boxCount = [self.rc.OTTBoxCount intValue];
    
    [[RegistrationFormModel sharedInstance] setCountFPTPlay:boxCount];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)dataStatic {
    
    infoCustomerVC = [[InfoCustomerViewController alloc] initWithNibName:@"InfoCustomerViewController" bundle:nil];
    infoCustomerVC.delegate = self;
    infoCustomerVC.Id = self.Id;
    
    customersVC = [[CustomersViewController alloc] initWithNibName:@"CustomersViewController" bundle:nil];
    customersVC.delegate = self;
    customersVC.Id = self.Id;
    
    styleVC = [[StyleOfServiceViewController alloc] initWithNibName:@"StyleOfServiceViewController" bundle:nil];
    styleVC.delegate = self;
    styleVC.Id = self.Id;
    styleVC.regcode = self.regcode;
    
    payVC = [[PaymentViewController alloc] initWithNibName:@"PaymentViewController" bundle:nil];
    payVC.delegate = self;
    payVC.Id = self.Id;
    payVC.regcode = self.regcode;
    payVC.rc = self.rc;
    
    infoCustomerVC.delegate = self;
    ReputationView *repu1 = [[ReputationView alloc] init];
    repu1.title = @"Thông Tin Khách Hàng";
    repu1.viewController = infoCustomerVC;
    
    ReputationView *repu2 = [[ReputationView alloc] init];
    repu2.title = @"Đối Tượng Khách Hàng";
    repu2.viewController = customersVC;
    
    ReputationView *repu3 = [[ReputationView alloc] init];
    repu3.title = @"Loại Dịch Vụ";
    repu3.viewController = styleVC;
    
    ReputationView *repu4 = [[ReputationView alloc] init];
    repu4.title = @"Thanh Toán";
    repu4.viewController = payVC;
    
    arrReputationView = [[NSMutableArray alloc] init];
    [arrReputationView addObject:repu1];
    [arrReputationView addObject:repu2];
    [arrReputationView addObject:repu3];
    [arrReputationView addObject:repu4];
    
}

- (void)setupScrollView {
    
    self.scrollView.delegate = self;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    NSLog(@"%f",screenRect.size.width);
    for (int i = 0; i < arrReputationView.count; i++) {
        
        CGFloat xRect = screenRect.size.width * (CGFloat)i;
        CGFloat wRect = screenRect.size.width;
        CGFloat hRect = screenRect.size.height - (64 + 60 + 34);
        CGRect rect = CGRectMake(xRect, 0, wRect, hRect );
        [self addViewControllerAsChildScrollView:[arrReputationView[i] viewController] rect:rect];
    }
    
    CGFloat countEntriesReputation = (CGFloat)arrReputationView.count;
    //self.scrollView.frame.size.height
    CGSize sizeScrollView = CGSizeMake(screenRect.size.width * countEntriesReputation, screenRect.size.height - (64 + 60 + 34));
    
    [self.scrollView setContentSize:sizeScrollView];
    
}

- (void)configStepperView {
    
    stepperView.delegate = self;
    stepperView.numberOfPoints = arrReputationView.count;
    [self configStepSlider:0];
    
}

- (void)configStepSlider:(NSInteger)stepped {
    
    stepperView.currentIndex = stepped;
    lblTitle.text = [arrReputationView[stepped] title];
    lblStepOf.text = [NSString stringWithFormat:@"Bước %ld/%lu",stepped + 1, (unsigned long)arrReputationView.count];
    
    [self loadUpdateWithPage:stepped];
}
//vutt11 fixed crasch 
-(void)addViewControllerAsChildScrollView:(UIViewController*) viewController rect:(CGRect) rect {
    
    NSLog(@"%f",self.view.frame.size.width);
    
    CGRect rectView = CGRectMake(rect.origin.x, rect.origin.y, rect.size.width, self.view.frame.size.height );
    
    UIView *view = [[UIView alloc] initWithFrame:rectView];
    [view addSubview:viewController.view];
    viewController.view.frame = CGRectMake(0, 0, rect.size.width, rect.size.height );
    
    [self.scrollView addSubview:view];
    
}

//MARK: -UISCROLLVIEWDELEGATE

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    NSLog(@"scrollViewDidScroll");
    
}

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    
    NSLog(@"scrollViewWillBeginDecelerating");
    [self.scrollView setScrollEnabled:NO];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    NSLog(@"scrollViewDidEndDecelerating");
    CGFloat pageCGFloat = scrollView.contentOffset.x / scrollView.contentSize.width * (CGFloat)arrReputationView.count;
    [self configStepSlider:pageCGFloat];
    if (pageCGFloat + 1 == (CGFloat)arrReputationView.count) {
        
        [payVC reloadDataViewController];
    }
    [self.view endEditing:YES];
    [self.scrollView setScrollEnabled:YES];
}

//MARK: -ABSTEPPEDPROGRESSBARDELEGATE

-(BOOL)progressBar:(ABSteppedProgressBar *)progressBar canSelectItemAtIndex:(NSInteger)index {
    
    return NO;
}

-(NSString *)progressBar:(ABSteppedProgressBar *)progressBar textAtIndex:(NSInteger)index {
    
    NSString *indexString = [NSString stringWithFormat:@"%ld",index + 1];
    return indexString;
    
}

//MARK: -InfoCustomerViewControllerDelegate
-(void)showButton:(id)sender kTag:(NSInteger)tag {
    
    switch (tag) {
        case regulationAddress: {
            SuggestionsSearchViewController *vc = [[SuggestionsSearchViewController alloc] init];
            vc.delegate = self;
            vc.type = SuggestionsFillHouseNumber;
            [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomTop];
            break;
        }
        case date:
            self.buttonDate = sender;
            
            [self showDatePicker];
            [self limitDate:[NSDate date] minYear:-99 maxYear:0];
            break;
        default:
            break;
    }
}

-(void)setSelectedDateInField {
    [super setSelectedDateInField];
    
    //vutt11
    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:currentDate];
    NSInteger day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    //
    NSDateComponents * componentsBirthDay = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:self.selectedDate];
    
    NSInteger dayBirth = [componentsBirthDay day];
    NSInteger monthBirth = [componentsBirthDay month];
    NSInteger yearbirth = [componentsBirthDay year];
    
    [[RegistrationFormModel sharedInstance] setDate:[self convertDateToString: [self convertStringToDate:self.buttonDate.titleLabel.text]]];
    
    if (year - yearbirth < 15) {
        [[RegistrationFormModel sharedInstance] setDate:@""];
        [self showAlertBox:@"Thông báo" message:@" Khách hàng chưa đủ tuổi để tạo phiếu đăng ký này"];
        [self.buttonDate setTitle:@"Chọn Ngày Sinh" forState:UIControlStateNormal];
    }
    if (year - yearbirth == 15) {
        
        if (monthBirth > month  ) {
            [[RegistrationFormModel sharedInstance] setDate:@""];
            [self showAlertBox:@"Thông báo" message:@" Khách hàng chưa đủ tuổi để tạo phiếu đăng ký này"];
            [self.buttonDate setTitle:@"Chọn Ngày Sinh" forState:UIControlStateNormal];
            
        }
        if (month == monthBirth)
        {
            if (dayBirth > day ) {
                [[RegistrationFormModel sharedInstance] setDate:@""];
                [self showAlertBox:@"Thông báo" message:@" Khách hàng chưa đủ tuổi để tạo phiếu đăng ký này"];
                [self.buttonDate setTitle:@"Chọn Ngày Sinh" forState:UIControlStateNormal];
            }
        }
    }
}

-(void) loadImageViewController {
    
    infoCustomerVC.imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:infoCustomerVC.imagePickerController animated:YES completion:nil];
}

-(void)showMBProcessListRegis {
    
    [HUD hide:YES];
    if (self.navigationController.view == nil) {
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    } else {
        HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    }
    //[self.view addSubview:HUD];
    HUD.delegate = (id<MBProgressHUDDelegate>)self;
    HUD.labelText = @"Loading";
    [HUD show:YES];
    
}

-(void)hideMBProcessListRegis {
    
    [HUD hide:YES];
    
}

-(void)logOutWhenEndSession {
    
    [self LogOut];
    
}

//Delegate of StyleOfServiceViewController

-(void) loadViewControllerWithArray:(NSInteger)tag tagButton:(NSInteger)tagButton array:(NSMutableArray *)arr {
    
    switch (tag) {
        case 98:
            if (tagButton == 1) {
                //promotion
                SearchPromotionViewController *vc = [[SearchPromotionViewController alloc] init];
                vc.delegate = self;
                
                [[RegistrationFormModel sharedInstance] setInternetPromotion:vc.promotionModel];
                
                [[RegistrationFormModel sharedInstance] setTotalInternet:[[[RegistrationFormModel sharedInstance] internetPromotion].Amount intValue]];
                vc.arrData = [self pareArrayForKeyWith:arr];
                [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomTop];
                
            }
            break;
            
        default:
            break;
    }
    
}

//DELEGATE LOADING OTT
-(void) loadOTTListDeviceOTTViewControllerViewController:(NSInteger)tag listDeviceVC:(ListDeviceOTTViewController *)listDeviceVC delegate:(id<ListDeviceOTTViewControllerDelegate>)delegateListDevice arrayDeviceChecked:(NSMutableArray *)arrayDeviceChecked {
    
    switch (tag) {
        case 111: {
            
//            ListDeviceOTTViewController *listOTTVC = [[ListDeviceOTTViewController alloc] initWithNibName:@"ListDeviceOTTViewController" bundle:nil];
//            listOTTVC.delegate = self;
            
            [self presentViewController:listDeviceVC animated:YES completion:nil];
            
            break;
            
        }
            
        default:
            break;
    }
    
}

-(void) loadScanViewController:(NSInteger)tag listDeviceVC:(CameraViewController *)listDeviceVC delegate:(id<CameraViewControllerDelegate>)delegateListDevice arrayDeviceChecked:(NSMutableArray *)arrayDeviceChecked {
    
    [self presentViewController:listDeviceVC animated:YES completion:nil];
    
}

//DELEGATE LOADING THIET BI
-(void) loadViewController:(NSInteger)tag listDeviceVC:(ListDeviceViewController *)listDeviceVC delegate:(id<ListDeviceViewControllerDelegate>)delegateListDevice arrayDeviceChecked:(NSMutableArray *)arrayDeviceChecked {

    switch (tag) {
        case 102: {
            
            self.Id = StringFormat(@"%@",self.Id);
            self.ls.Id = self.Id;
            // lay nhung thiet bi da check o phan tao khi cap nhat lai phieu dang ky
            if (![self.Id isEqualToString:@"(null)"] ||![self.Id isEqualToString:@"0"]) {
                listDeviceVC.arrayDevieChecked = arrayDeviceChecked;
            }
            [self presentViewController:listDeviceVC animated:YES completion:nil];
            
            break;
        }
        default:
            break;
    }
    
}

// Dismiss danh sách thiết bị
-(void) dismissViewController {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

//MARK: -STYLEOFSERVICEVIEWCONTROLLERDELEGATE
-(void) loadViewController:(NSInteger)tag tagButton:(NSInteger)tagButton {
    
    switch (tag) {
        case 98: {
            
            if (tagButton == 2) {
                
                [self.view endEditing:YES];
                SuggestionsSearchViewController *vc = [[SuggestionsSearchViewController alloc] init];
                vc.delegate = self;
                vc.type = suggestionsSearchPromotionStatement;
                [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomTop];
                break;
            }
            
        }
        case 99: {
            _office365ViewController = [[Office365ViewController alloc] init];
            _office365ViewController.delegate = self;
            _office365ViewController.registerID = self.Id;
            _office365ViewController.amountTotal = office365Total;
            [self presentViewController:_office365ViewController animated:YES completion:nil];
            break;
        }
            
        case 100: {
            
            [self presentViewController:self.ls animated:YES completion:nil];
            
        }
        case 101: {
            
            CameraViewController *scanVC = [[CameraViewController alloc] initWithNibName:@"ScanBarCodeViewController" bundle:nil];
//            CameraViewController *scanVC = [[CameraViewController alloc] init];
            scanVC.delegate = self;
            [self presentViewController:scanVC animated:YES completion:nil];
            
        }
            
        default:
            break;
    }
    
}

//StevenNguyen CameraViewControllerDelegate
//func getMacOTTPlayBox(strMac:String)
-(void)getMacOTTPlayBoxWithStrMac:(NSString *)strMac {
    
    [self dismissViewController];
    
}

-(void)dismissMacOTTPlayBox {
    [self dismissViewController];
}

- (NSMutableArray *)pareArrayForKeyWith:(NSArray*)arrInput {
    NSMutableArray *data = [[NSMutableArray alloc] init];
    
    int i = 0;
    PromotionModel *temp;
    for (i = 0; i < arrInput.count; i++) {
        temp = [arrInput objectAtIndex:i];
        [data addObject:[NSDictionary dictionaryWithObjectsAndKeys:temp.PromotionName,@"DisplayText",temp,@"PromotionModel", nil]];
    }
    return data;
}

- (void)cancelOffice365View
{
    office365Total = _office365ViewController.amountTotal;
    [[RegistrationFormModel sharedInstance] setTotalOffice365:_office365ViewController.amountTotal];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

//MARK: -SearchPromotionDelegate
-(void)cancelSearchPromotionView:(PromotionModel *)selectedPromo {
    
    [[RegistrationFormModel sharedInstance] setInternetPromotion:selectedPromo];
    
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    
    [styleVC reloadTableView];
   
    
    int amountInternet = [[[RegistrationFormModel sharedInstance] internetPromotion].Amount intValue];
    
    [[RegistrationFormModel sharedInstance] setTotalInternet:amountInternet];
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
    
}

//MARK: -SuggestionsDelegate
- (void)CancelPopupSuggestions {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
}

-(void)scrollToPage:(NSInteger)page animationFor:(BOOL)animated {
    
    CGRect frameScrollView = self.scrollView.frame;
    frameScrollView.origin.x = frameScrollView.size.width * page;
    frameScrollView.origin.y = 0;
    
    [self.scrollView scrollRectToVisible:frameScrollView animated:animated];
    
    [self configStepSlider:page];
    
    if (page + 1 == (CGFloat)arrReputationView.count) {
        
        [payVC reloadDataViewController];
        
    }
    
    [self.view endEditing:YES];
    
}

//MARK: -InfoCustomerViewControllerDelegate
-(void)scrollToView:(NSInteger)tag tagButton:(NSInteger)tagButton {
    
    switch (tag) {
        case 1: {
            
            //InfoCustomerViewController
            if (tagButton == 1) {
                
                //Button Save
                [self scrollToPage:1 animationFor:YES];
                
                
            } else if (tagButton == 2){
                
                //Button Back
                
            }
            
            break;
        }
        case 2: {
            
            //CustomersViewController
            if (tagButton == 1) {
                
                //Button Save
                [self scrollToPage:2 animationFor:YES];
                
            } else if (tagButton == 2){
                
                //Button Back
                [self scrollToPage:0 animationFor:YES];
            }
            break;
        }
        case 3: {
            
            //StyleOfServiceViewController
            if (tagButton == 1) {
                
                //Button Save
                [self scrollToPage:3 animationFor:YES];
                
            } else if (tagButton == 2){
                
                //Button Back
                [self scrollToPage:1 animationFor:YES];
            }
            
            break;
        }
        case 4: {
            
            //PaymentViewController
            if (tagButton == 1) {
                
                //Button Save
                [self scrollToPage:4 animationFor:YES];
                
            } else if (tagButton == 2){
                
                //Button Back
                [self scrollToPage:2 animationFor:YES];
            }
            
            break;
        }
        default:
            break;
    }
    
}
//Delegate - Protocol of PaymentViewController


-(void) scrollToViewValidate:(NSInteger)tag {
    
    switch (tag) {
        case -1: {
            [self showAlertBox:@"Thông Báo" message:@"Vui lòng chọn loại hình"];
            [self scrollToPage:0 animationFor:YES];
            break;
        }
        case 0: {
            [self showAlertBox:@"Thông Báo" message:@"Vui lòng chọn loại khách hàng"];
            [self scrollToPage:0 animationFor:YES];
            break;
        }
        case 1: {
            [self showAlertBox:@"Thông Báo" message:@"Vui lòng điền tên khách hàng"];
            [self scrollToPage:0 animationFor:YES];
            break;
        }
        case 2: {
            [self showAlertBox:@"Thông Báo" message:@"Vui lòng điền số CMND"];
            [self scrollToPage:0 animationFor:YES];
            break;
        }
        case 3: {
            [self showAlertBox:@"Thông Báo" message:@"Số CMND không hợp lệ"];
            [self scrollToPage:0 animationFor:YES];
            break;
        }
        case 4: {
            [self showAlertBox:@"Thông Báo" message:@"Số CMND không hợp lệ"];
            [self scrollToPage:0 animationFor:YES];
            break;
        }
        case 5: {
            [self showAlertBox:@"Thông Báo" message:@"Vui lòng điền địa chỉ trên CMND"];
            [self scrollToPage:0 animationFor:YES];
            break;
        }
        case 6: {
            [self showAlertBox:@"Thông báo" message:@"Vui lòng chọn ngày sinh"];
            [self scrollToPage:0 animationFor:YES];
            break;
        }
        case 7: {
            [self showAlertBox:@"Thông Báo" message:@"Vui lòng điền mã số thuế"];
            [self scrollToPage:0 animationFor:YES];
            break;
        }
        case 8: {
            [self showAlertBox:@"Thông Báo" message:@"Vui lòng điền số điện thoại liên hệ 1"];
            [self scrollToPage:0 animationFor:YES];
            break;
        }
        case 9: {
            [self showAlertBox:@"Thông Báo" message:@"Số điện thoại liên hệ 1 không hợp lệ"];
            [self scrollToPage:0 animationFor:YES];
            break;
        }
        case 10: {
            [self showAlertBox:@"Thông Báo" message:@"Vui lòng điền tên liên hệ"];
            [self scrollToPage:0 animationFor:YES];
            break;
        }
        case 11: {
            [self showAlertBox:@"Thông Báo" message:@"Email không hợp lệ"];
            [self scrollToPage:0 animationFor:YES];
            break;
        }
        case 12: {
            [self showAlertBox:@"Thông Báo" message:@"Vui lòng chọn quận/huyện "];
            [self scrollToPage:0 animationFor:YES];
            break;
        }
        case 13: {
            [self showAlertBox:@"Thông Báo" message:@"Vui lòng chọn phường/xã "];
            [self scrollToPage:0 animationFor:YES];
            break;
        }
        case 14: {
            [self showAlertBox:@"Thông Báo" message:@"Vui lòng chọn đường"];
            [self scrollToPage:0 animationFor:YES];
            break;
        }
        case 15: {
            [self showAlertBox:@"Thông Báo" message:@"Vui lòng điền số nhà"];
            [self scrollToPage:0 animationFor:YES];
            break;
        }
        case 16: {
            [self showAlertBox:@"Thông Báo" message:@"Vui lòng chọn chung cư"];
            [self scrollToPage:0 animationFor:YES];
            break;
        }
        case 17: {
            [self showAlertBox:@"Thông Báo" message:@"Vui lòng điền số nhà"];
            [self scrollToPage:0 animationFor:YES];
            break;
        }
        case 18: {
            [self showAlertBox:@"Thông Báo" message:@"Vui lòng chọn vị trí nhà"];
            [self scrollToPage:0 animationFor:YES];
            break;
        }
        case 19: {
            [self showAlertBox:@"Thông Báo" message:@"Quận/Huyện không hợp lệ"];
            [self scrollToPage:0 animationFor:YES];
            break;
        }
        case 20: {
            [self showAlertBox:@"Thông Báo" message:@"Phường/Xã không hợp lệ"];
            [self scrollToPage:0 animationFor:YES];
            break;
        }
        case 21: {
            [self showAlertBox:@"Thông Báo" message:@"Chưa nhập tên đường"];
            [self scrollToPage:0 animationFor:YES];
            break;
        }
        case 22: {
            [self showAlertBox:@"Thông báo" message:@"Vui lòng chọn hình thức thanh toán"];
            [self scrollToPage:3 animationFor:YES];
            break;
        }
        case 23: {
            [self showAlertBox:@"Thông báo " message:@"Vui lòng chọn Câu lệnh khuyến mãi dịch vụ internet"];
            [self scrollToPage:2 animationFor:YES];
            break;
        }
        case 24: {
            [self showAlertBox:@"Thông báo " message:@"Vui lòng chọn Câu lệnh khuyến mãi IPTV"];
            [self scrollToPage:2 animationFor:YES];
            break;
        }
       
        case 25: {
            [self showAlertBox:@"Thông báo " message:@"Vui lòng chọn Combo cho dịch vụ IPTV"];
            [self scrollToPage:2 animationFor:YES];
            break;
        }
        case 26: {
            [self showAlertBox:@"Thông báo " message:@"Vui lòng chọn đối tượng khách hàng"];
            [self scrollToPage:1 animationFor:YES];
            break;
        }
        case 27: {
            [self showAlertBox:@"Thông báo " message:@"Vui lòng chọn ISP"];
            [self scrollToPage:1 animationFor:YES];
            break;
        }
        case 28: {
            [self showAlertBox:@"Thông báo " message:@"Vui lòng chọn nơi ở hiện tại"];
            [self scrollToPage:1 animationFor:YES];
            break;
        }
        case 29: {
            [self showAlertBox:@"Thông báo " message:@"Vui lòng chọn khả năng thanh toán"];
            [self scrollToPage:1 animationFor:YES];
            break;
        }
        case 30: {
            [self showAlertBox:@"Thông báo " message:@"Vui lòng chọn khách hàng của đối tác"];
            [self scrollToPage:1 animationFor:YES];
            break;
        }
        case 31: {
            [self showAlertBox:@"Thông báo " message:@"Vui lòng chọn pháp nhân"];
            [self scrollToPage:1 animationFor:YES];
            break;
        }
        case 32: {
            [self showAlertBox:@"Thông báo " message:@"Vui lòng Đăng ký thông tin dịch vụ Office 365"];
            [self scrollToPage:2 animationFor:YES];
            break;
        }
        case 33: {
            [self showAlertBox:@"Thông báo " message:@"Vui chọn số lượng Box khi bán FPTPlay"];
            [self scrollToPage:2 animationFor:YES];
            break;
        }
        case 34: {
            [self showAlertBox:@"Thông báo " message:@"Vui lòng chọn Câu lệnh khuyến mãi 2 IPTV"];
            [self scrollToPage:2 animationFor:YES];
            break;
        }
        case 35: {
            [self showAlertBox:@"Thông báo " message:@"Vui lòng chọn lại Dịch vụ Internet"];
            [self scrollToPage:2 animationFor:YES];
            break;
        }
       
        case 36: {
            
            [self showAlertBox:@"Thông báo " message:@"Vui lòng chọn Câu lệnh khuyến mãi cho thiết bị"];
            [self scrollToPage:2 animationFor:YES];
            break;
        }
        case 37: {
            [self showAlertBox:@"Thông báo " message:@"Vui lòng chọn thiết bị để bán"];
            [self scrollToPage:2 animationFor:YES];
            break;
        }
        case 408: {//Chua Chon Promotion
            [self showAlertBox:@"Thông báo " message:@"Vui lòng chọn Khuyến mãi cho FPT Play Box"];
            [self scrollToPage:2 animationFor:YES];
            break;
        }
        case 409: {//Chua Chon Box
            [self showAlertBox:@"Thông báo " message:@"Vui lòng chọn Box FPT Play"];
            [self scrollToPage:2 animationFor:YES];
            break;
        }
        case 411: {//Box Lon Hon MAC
            [self showAlertBox:@"Thông báo " message:@"Vui lòng chọn thêm số lượng MAC"];
            [self scrollToPage:2 animationFor:YES];
            break;
        }
        case 412: {//MAC Lon Hon Box
            [self showAlertBox:@"Thông báo " message:@"Vui lòng chọn thêm số lượng Box"];
            [self scrollToPage:2 animationFor:YES];
            break;
        }
            
        default:
            break;
    }
    
}

//MARK: -PaymentViewControllerDelegate
-(void)popViewController:(NSString *)resultID {
    
    [self hideMBProcessListRegis];
    
    //List phiếu đk
    ListRegistrationFormPageViewController *listvc = [[ListRegistrationFormPageViewController alloc] init];
    
    //Chi tiết phiếu đk
    NSString *nibName = @"DetailRegisteredForm";
    DetailRegisteredForm *vc = [[DetailRegisteredForm alloc]initWithNibName:nibName bundle:nil];
    vc.Id = resultID;       //ID PĐK
    
    //Lấy danh sách màn hình trong Navigation hiện tại
    NSArray *arrViewControllerMain = [[NSArray alloc] initWithArray:[self.navigationController viewControllers]];
    
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    [arr addObject:arrViewControllerMain[0]];   //Màn hình main
    [arr addObject:listvc];                     //Màn hình list PĐK
    [arr addObject:vc];                         //Màn hình chi tiết PĐK
    
    //Sử lý Send màng hình khi Success...
    [self.navigationController setViewControllers:arr];
    
}


@end
