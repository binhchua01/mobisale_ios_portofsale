
//  InfoCustomerViewController.m
//  MobiSale
//
//  Created by Bored Ninjas on 11/18/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "InfoCustomerViewController.h"
#import "AniTextFieldCell.h"
#import "AniPhoneNumberCell.h"
#import "AniTextViewCell.h"
#import "ButtonNextCell.h"
#import "RegistrationFormModel.h"
#import "AniDropDownCellTableViewCell.h"
#import "AniApartmentCell.h"
#import "PromotionModel.h"
#import "Common_client.h"
#import "ShareData.h"
#import "ButtonTitleCell.h"
#import "UploadImageCell.h"
#import "UIImage+FPTCustom.h"
#import "AppDelegate.h"
#import "CacheDropDownCell.h"

//MARK: -StevenNguyen-FixError
#define kArrayDistrict @"arraydistrict"

#define kArrayWard(f) ([NSString stringWithFormat:@"arrayward%@",f])

enum StyleCell{
    
    type,
    typeCustomer,
    name,
    isCard,
    date,
    address,
    taxCode,
    typePhone1,
    numberPhone1,
    typePhone2,
    numberPhone2,
    email,
    county,
    ward,
    street,
    typeOfHouse,
    regulationAddress,
    housePosition,
    nameApartment,
    apartment,
    numberOfHouse,
    noteAddress,
    notePlant,
    chooseImage,
    button
    
};

@interface InfoCustomerViewController ()<UITableViewDelegate, UITableViewDataSource, AniTextFieldCellDelegate,AniDropDownCellTableViewCellDelegate,AniPhoneNumberCellDelegate,AniApartmentCellDelegate,AniTextViewCellDelegate,ButtonNextCellDelegate,ButtonTitleCellDelegate,UploadImageCellDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate> {
    
    __weak IBOutlet UITableView *myTableView;
    
    NSString *imageData;
    
}

@end

@implementation InfoCustomerViewController {
    
    BOOL isSelectedDate;
    BOOL isSelectedType;
    BOOL isSelectedTypeCustomer;
    BOOL isSelectedCounty;
    BOOL isSelectedWard;
    BOOL isSelectedTypeOfHouse;
    BOOL isSelectedStreet;
    BOOL isSelectedTypePhone1;
    BOOL isSelectedTypePhone2;
    BOOL isSelectedNameVilla;
    BOOL isSelectedHousePosition;
}

@synthesize delegate;
@synthesize arrCell;
@synthesize arrType;
@synthesize arrTypeCustomer;
@synthesize arrDistrict;
@synthesize arrWard;
@synthesize arrStreet;
@synthesize arrTypeHouse;
@synthesize arrNameVilla;
@synthesize arrTypePhone;
@synthesize arrTypePhone2;
@synthesize arrHousePosition;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isSelectedDate = NO;
    isSelectedType = NO;
    isSelectedTypeCustomer = NO;
    isSelectedCounty = NO;
    isSelectedWard = NO;
    isSelectedTypeOfHouse = NO;
    isSelectedStreet = NO;
    isSelectedTypePhone1 = NO;
    isSelectedTypePhone2 = NO;
    isSelectedNameVilla = NO;
    isSelectedHousePosition = NO;
    
    [self addArrayCell];
    [self configTableView];
    
    if (self.Id.length <= 0 || [self.Id isEqualToString:@"0"]) {
        
        [self LoadCusType:@"0"];
        [self LoadPhone1:@"0"];
        [self LoadPhone2:@"0"];
        [self LoadTypeCustomer:@"0"];
        [self loadDistrictAndCache];
        [self LoadTypeHouse:@"0"];
        [self LoadPosition:@"0"];
        
    }
    
}

//MARK: -StevenNguyen-FixError
-(void) loadDistrictAndCache {
    
    NSMutableArray *arrCacheDistrict = [[EGOCache globalCache] objectForKey:kArrayDistrict];
    
    NSLog(@"%@", kArrayWard(@"Sang"));
    
    if (arrCacheDistrict.count > 0) {//Có dữ liệu từ Cache
        
        self.arrDistrict = arrCacheDistrict;
        
//        int i = 0;
//        if([Id isEqualToString:@"0"]){
//            KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"0" description:@""];
//            [[RegistrationFormModel sharedInstance] setCounty:s1];
//            isSelectedCounty = YES;
//            dispatch_async(dispatch_get_main_queue(), ^{
//                [self.tableView reloadData];
//            });
//            
//        }
//        else {
//            isSelectedCounty = YES;
//            for (i = 0; i< self.arrDistrict.count; i++) {
//                
//                KeyValueModel *s1 = [self.arrDistrict objectAtIndex:i];
//                if([Id isEqualToString:s1.Key]){
//                    [[RegistrationFormModel sharedInstance] setCounty:s1];
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        [self.tableView reloadData];
//                    });
//                    break;
//                }
//            }
//        }
        
        [myTableView reloadData];
        
    } else {//Không có dữ liệu nên request API
        
        [self LoadDistrict:@"0" WardId:@"0" Street:@"0"];
        
    }
    
    
}

//MARK: -StevenNguyen-FixError
-(void) loadWardAndCache:(NSString *)keyWardCache IDWard:(NSString *)idWard IDStreet:(NSString *)idStreet {
    
    NSMutableArray *arrCacheWard = [[EGOCache globalCache] objectForKey:keyWardCache];
    if (arrCacheWard.count > 0) {//Có dữ liệu từ Cache

        self.arrWard = arrCacheWard;
        
        if([idWard isEqualToString:@"0"] || idWard == nil){
            
            [[RegistrationFormModel sharedInstance] setWard:[self.arrWard objectAtIndex:0]];
            [self.tableView reloadData];
        }else {
            int i = 0;
            for (i = 0; i< self.arrWard.count; i++) {
                KeyValueModel *selectedWard = [self.arrWard objectAtIndex:i];
                if([idWard isEqualToString:selectedWard.Key]){
                    [[RegistrationFormModel sharedInstance] setWard:selectedWard];
                    break;
                }
            }
            [self.tableView reloadData];
        }
        [self LoadStreet:idStreet];
        
    } else {//Không có dữ liệu nên request API
        
        [self LoadWard:idWard Street:idStreet];
        
    }
    
}

-(void) saveCacheWith:(id)obj KeyCache:(NSString *)keyCache {
    
    dispatch_queue_t dispatchQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(dispatchQueue, ^ {
        [[EGOCache globalCache] setObject:obj forKey:keyCache];
    });
    
}


- (void) addArrayCell {

    NSNumber *numberType = [NSNumber numberWithInteger:type];
    NSNumber *numberTypeCustomer = [NSNumber numberWithInteger:typeCustomer];
    NSNumber *numberName = [NSNumber numberWithInteger:name];
    NSNumber *numberisCard = [NSNumber numberWithInteger:isCard];
    NSNumber *numberdate = [NSNumber numberWithInteger:date];
    NSNumber *numberaddress = [NSNumber numberWithInteger:address];
    NSNumber *numbertaxCode = [NSNumber numberWithInteger:taxCode];
    NSNumber *numberTypePhone1 = [NSNumber numberWithInteger:typePhone1];
    NSNumber *numbernumberPhone1 = [NSNumber numberWithInteger:numberPhone1];
    NSNumber *numberTypePhone2 = [NSNumber numberWithInteger:typePhone2];
    NSNumber *numbernumberPhone2 = [NSNumber numberWithInteger:numberPhone2];
    NSNumber *numberemail = [NSNumber numberWithInteger:email];
    NSNumber *numbercounty = [NSNumber numberWithInteger:county];
    NSNumber *numberWard = [NSNumber numberWithInteger:ward];
    NSNumber *numberstreet = [NSNumber numberWithInteger:street];
    NSNumber *numbertypeOfHouse = [NSNumber numberWithInteger:typeOfHouse];
    NSNumber *numberhousePosition = [NSNumber numberWithInteger:housePosition];
    NSNumber *numberNameAparment = [NSNumber numberWithInteger:nameApartment];
    NSNumber *numberApartment = [NSNumber numberWithInteger:apartment];
    NSNumber *numbernumberOfHouse = [NSNumber numberWithInteger:numberOfHouse];
    NSNumber *numbernoteAddress = [NSNumber numberWithInteger:noteAddress];
    NSNumber *numbernotePlant = [NSNumber numberWithInteger:notePlant];
    NSNumber *numberbutton = [NSNumber numberWithInteger:button];
    NSNumber *numberRegulationAddress = [NSNumber numberWithInteger:regulationAddress];
    NSNumber *numberChooseImage = [NSNumber numberWithInteger:chooseImage];
    
    NSString *str = [[RegistrationFormModel sharedInstance] typeOfHouse].Key;
    if ([str  isEqual: @"1"]) {
        arrCell = nil;
        arrCell = [[NSMutableArray alloc] initWithObjects:numberType,numberTypeCustomer,numberName,numberisCard,numberdate,numberaddress,numbertaxCode,numberTypePhone1,numbernumberPhone1,numberTypePhone2,numbernumberPhone2,numberemail,numbercounty,numberWard,numberstreet,numbertypeOfHouse,numbernumberOfHouse,numberRegulationAddress,numbernoteAddress,numbernotePlant,numberChooseImage,numberbutton, nil];
        
    } else if ([str  isEqual: @"2"]) {
        
        arrCell = nil;
        arrCell = [[NSMutableArray alloc] initWithObjects:numberType,numberTypeCustomer,numberName,numberisCard,numberdate,numberaddress,numbertaxCode,numberTypePhone1,numbernumberPhone1,numberTypePhone2,numbernumberPhone2,numberemail,numbercounty,numberWard,numberstreet,numbertypeOfHouse,numberApartment,numberNameAparment,numbernoteAddress,numbernotePlant,numberChooseImage,numberbutton, nil];
        
    } else if ([str  isEqual: @"3"]) {
        
        arrCell = nil;
        arrCell = [[NSMutableArray alloc] initWithObjects:numberType,numberTypeCustomer,numberName,numberisCard,numberdate,numberaddress,numbertaxCode,numberTypePhone1,numbernumberPhone1,numberTypePhone2,numbernumberPhone2,numberemail,numbercounty,numberWard,numberstreet,numbertypeOfHouse,numberhousePosition,numbernumberOfHouse,numberRegulationAddress,numbernoteAddress,numbernotePlant,numberChooseImage,numberbutton, nil];
        
    } else {
        arrCell = nil;
        arrCell = [[NSMutableArray alloc] initWithObjects:numberType,numberTypeCustomer,numberName,numberisCard,numberdate,numberaddress,numbertaxCode,numberTypePhone1,numbernumberPhone1,numberTypePhone2,numbernumberPhone2,numberemail,numbercounty,numberWard,numberstreet,numbertypeOfHouse,numbernumberOfHouse,numberRegulationAddress,numbernoteAddress,numbernotePlant,numberChooseImage,numberbutton, nil];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

//config and register cell for tableview.
- (void)configTableView {
    
    myTableView.delegate = self;
    myTableView.dataSource = self;
    
    UINib *cellTextField = [UINib nibWithNibName:@"AniTextFieldCell" bundle:nil];
    UINib *cellPhoneNumber = [UINib nibWithNibName:@"AniPhoneNumberCell" bundle:nil];
    UINib *cellNote = [UINib nibWithNibName:@"AniTextViewCell" bundle:nil];
    UINib *cellButton = [UINib nibWithNibName:@"ButtonNextCell" bundle:nil];
    UINib *cellDropDown = [UINib nibWithNibName:@"AniDropDownCellTableViewCell" bundle:nil];
    UINib *cellApartment = [UINib nibWithNibName:@"AniApartmentCell" bundle:nil];
    UINib *cellButtonTitle = [UINib nibWithNibName:@"ButtonTitleCell" bundle:nil];
    UINib *cellChoosImage = [UINib nibWithNibName:@"UploadImageCell" bundle:nil];
    UINib *cellCache = [UINib nibWithNibName:@"CacheDropDownCell" bundle:nil];
    
    [myTableView registerNib:cellButtonTitle forCellReuseIdentifier:@"cellButtonTitle123"];
    [myTableView registerNib:cellTextField forCellReuseIdentifier:@"cellTxtF"];
    [myTableView registerNib:cellPhoneNumber forCellReuseIdentifier:@"cellPhoneNumber"];
    [myTableView registerNib:cellNote forCellReuseIdentifier:@"cellNote"];
    [myTableView registerNib:cellButton forCellReuseIdentifier:@"cellButton"];
    [myTableView registerNib:cellDropDown forCellReuseIdentifier:@"cellDropDown"];
    [myTableView registerNib:cellApartment forCellReuseIdentifier:@"cellApartment"];
    [myTableView registerNib:cellChoosImage forCellReuseIdentifier:@"cellChooseImage"];
    [myTableView registerNib:cellCache forCellReuseIdentifier:@"CacheDropDownCell"];
    
}

-(NSString *)convertStringDateToStringDateNew:(NSString *)strDate {
    
    NSDate *dateFromString;
    
    @try {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"GMT"];
        [dateFormatter setTimeZone:timeZone];
        dateFromString = [[NSDate alloc] init];
        dateFromString = [dateFormatter dateFromString:strDate];
    }
    @catch (NSException *exception) {
        [self showAlertBox:@"Lỗi" message:@"Bạn đã nhập sai định dạng thời gian cần tìm"];
        
         [CrashlyticsKit recordCustomExceptionName:@"InfoCustomerViewController - funtion (saveReceiveRemoteNotification)-Error handle Bạn đã nhập sai định dạng thời gian cần tìm" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
    }
    
    @try {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"GMT+7"];
        [dateFormatter setTimeZone:timeZone];
        NSString *stringDate = [dateFormatter stringFromDate:dateFromString];
        return stringDate;
    }
    @catch (NSException *exception) {
        [self showAlertBox:@"Lỗi" message:@"Bạn đã nhập sai định dạng thời gian cần tìm"];
        
        [CrashlyticsKit recordCustomExceptionName:@"InfoCustomerViewcontroller - funtion (saveReceiveRemoteNotification)-Error handle Bạn đã nhập sai định dạng thời gian cần tìm" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
    }
    
}


/***************************************************************/
/***************************************************************/
//MARK: -LOAD ALL VALUE...
/***************************************************************/
/***************************************************************/
-(void)LoadCusType:(NSString *) Id {
    self.arrType = [Common getCusType];
    int i = 0;
    if([Id isEqualToString:@"0"]){
        [[RegistrationFormModel sharedInstance] setType:[self.arrType objectAtIndex:0]];
    }else {
        for (i = 0; i< self.arrType.count; i++) {
            KeyValueModel *selectedCusType = [self.arrType objectAtIndex:i];
            if([Id isEqualToString:selectedCusType.Key]){
                [[RegistrationFormModel sharedInstance] setType:selectedCusType];
                isSelectedType = YES;
                return;
            }
        }
    }
}

-(void)LoadTypeCustomer:(NSString *) Id {
    self.arrTypeCustomer = [Common getCustomerType];
    int i = 0;
    if([Id isEqualToString:@"0"]){
        // Default load "Hộ gia đình"
        [[RegistrationFormModel sharedInstance] setTypeCustomer:[self.arrTypeCustomer objectAtIndex:2]];
        
    }else {
        for (i = 0; i< self.arrTypeCustomer.count; i++) {
            KeyValueModel *selectedTypeCustomer = [self.arrTypeCustomer objectAtIndex:i];
            if([Id isEqualToString:selectedTypeCustomer.Key]){
                [[RegistrationFormModel sharedInstance] setTypeCustomer:selectedTypeCustomer];
                isSelectedTypeCustomer = YES;
                return;
            }
        }
    }
    
}

//Load District
-(void)LoadDistrict:(NSString *)Id WardId:(NSString *)wardid Street:(NSString *)street {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self.delegate hideMBProcessListRegis];
        return;
    }
    ShareData *shared = [ShareData instance];
    NSString *locationid = shared.currentUser.LocationID;
    [shared.appProxy GetDistrictList:locationid completionHandler:^(id result, NSString *errorCode, NSString *message) {
        self.arrDistrict = result;
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self.delegate logOutWhenEndSession];
            [self.delegate hideMBProcessListRegis];
            return;
        }
        if ([message isEqualToString:@"Error Service: Unauthorized"]) {

            [self.delegate hideMBProcessListRegis];
            return;
        }
        
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self.delegate hideMBProcessListRegis];
            return ;
        }
       
        
        if (self.arrDistrict.count <= 0) {
            [[RegistrationFormModel sharedInstance] setCounty:@""];
            [myTableView reloadData];
            return;
        }
        
        if(self.arrDistrict.count > 0){
            int i = 0;
            
            //MARK: -StevenNguyen-FixError
            [self saveCacheWith:self.arrDistrict KeyCache:kArrayDistrict];
            
            if([Id isEqualToString:@"0"]){
                KeyValueModel *s1 = [[KeyValueModel alloc] initWithName:@"0" description:@""];
                [[RegistrationFormModel sharedInstance] setCounty:s1];
                isSelectedCounty = YES;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.tableView reloadData];
                });
                
            }
            else {
                isSelectedCounty = YES;
                for (i = 0; i< self.arrDistrict.count; i++) {
                   
                    KeyValueModel *s1 = [self.arrDistrict objectAtIndex:i];
                    if([Id isEqualToString:s1.Key]){
                        [[RegistrationFormModel sharedInstance] setCounty:s1];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.tableView reloadData];
                        });
                        break;
                    }
                }
            }
            
            if ([[[RegistrationFormModel sharedInstance] county].Key length] >0 ) {

                //MARK: -StevenNguyen-FixError
                NSString *strKeyDistrict = [[RegistrationFormModel sharedInstance] county].Key;
                [self loadWardAndCache:kArrayWard(strKeyDistrict) IDWard:wardid IDStreet:street];
                
            }
        }
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@- %@",@"GetDistrictList",[error localizedDescription]]];
        [self.delegate hideMBProcessListRegis];
    }];
}

//Load Ward
-(void)LoadWard:(NSString *)Id Street:(NSString *)street {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];

        [self.delegate hideMBProcessListRegis];
        isSelectedCounty = NO;
        return;
    }
    if (isSelectedCounty == YES) {

        [self.delegate showMBProcessListRegis];
    }
    
    ShareData *shared = [ShareData instance];
    [shared.appProxy GetWardList:shared.currentUser.LocationID DistrictName:[[RegistrationFormModel sharedInstance] county].Key completionHandler:^(id result, NSString *errorCode, NSString *message) {
        self.arrWard = result;
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            
            [self.delegate logOutWhenEndSession];

            [self.delegate hideMBProcessListRegis];
            return;
        }
        //vutt11
        if ([message isEqualToString:@"Error Service: Unauthorized"]) {
            [self.delegate hideMBProcessListRegis];
            return;
        }
        
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self.delegate hideMBProcessListRegis];
            isSelectedCounty = NO;
            self.arrWard = nil;
            self.arrStreet = nil;
            self.arrNameVilla = nil;
            [[RegistrationFormModel sharedInstance] setWard:nil];
            [[RegistrationFormModel sharedInstance] setStreet:nil];
            [[RegistrationFormModel sharedInstance] setNameAparment:nil];
            [myTableView reloadData];
            
            return ;
        }
        
        if (self.arrWard.count <= 0) {
            if (isSelectedCounty == YES) {
                [self.delegate hideMBProcessListRegis];
            }
            isSelectedCounty = NO;
            self.arrWard = nil;
            self.arrStreet = nil;
            self.arrNameVilla = nil;
            [[RegistrationFormModel sharedInstance] setWard:nil];
            [[RegistrationFormModel sharedInstance] setStreet:nil];
            [[RegistrationFormModel sharedInstance] setNameAparment:nil];
            [myTableView reloadData];
            return;
        }
        
        if(self.arrWard.count > 0){
            int i = 0;
            
            //MARK: -StevenNguyen-FixError
            [self saveCacheWith:self.arrWard KeyCache:kArrayWard([[RegistrationFormModel sharedInstance] county].Key)];
            
            if([Id isEqualToString:@"0"] || Id == nil){

                [[RegistrationFormModel sharedInstance] setWard:[self.arrWard objectAtIndex:0]];
                [self.tableView reloadData];
            }else {
                for (i = 0; i< self.arrWard.count; i++) {
                    KeyValueModel *selectedWard = [self.arrWard objectAtIndex:i];
                    if([Id isEqualToString:selectedWard.Key]){
                        [[RegistrationFormModel sharedInstance] setWard:selectedWard];
                        break;
                    }
                }
                [self.tableView reloadData];
            }
            [self LoadStreet:street];
            
        }
        
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@- %@",@"GetWardList",[error localizedDescription]]];
        [self.delegate hideMBProcessListRegis];
        self.arrWard = nil;
        self.arrStreet = nil;
        self.arrNameVilla = nil;
        [[RegistrationFormModel sharedInstance] setWard:nil];
        [[RegistrationFormModel sharedInstance] setStreet:nil];
        [[RegistrationFormModel sharedInstance] setNameAparment:nil];
        
    }];
}

//Load Street
-(void)LoadStreet:(NSString *)Id {
    
    [self.delegate showMBProcessListRegis];
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self.delegate hideMBProcessListRegis];
        return;
    }
    if (isSelectedWard == YES) {
        [self.delegate showMBProcessListRegis];
    }
    NSString *District=[[RegistrationFormModel sharedInstance] county].Key;
    NSString *selWard=[[RegistrationFormModel sharedInstance] ward].Key;
    ShareData *shared = [ShareData instance];
    [shared.appProxy GetStreetOrCondominiumList:shared.currentUser.LocationID DistrictName:District Ward:selWard Type:@"0" completionHandler:^(id result, NSString *errorCode, NSString *message) {
        self.arrStreet = result;
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self.delegate logOutWhenEndSession];
            [self.delegate hideMBProcessListRegis];
            return;
        }
        if(errorCode.length <1){
            errorCode=@"<null>";
        }
        //vutt11
        if ([message isEqualToString:@"Error Service: Unauthorized"]) {
            [self.delegate hideMBProcessListRegis];
            return;
        }
        
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self.delegate hideMBProcessListRegis];
            self.arrNameVilla = nil;
            [[RegistrationFormModel sharedInstance] setStreet:nil];
            [[RegistrationFormModel sharedInstance] setNameAparment:nil];
            [myTableView reloadData];
            return ;
        }
        
        if (self.arrStreet.count <= 0) {
            if (isSelectedCounty == YES || isSelectedWard == YES) {
                [self.delegate hideMBProcessListRegis];
            }
            self.arrNameVilla = nil;
            [[RegistrationFormModel sharedInstance] setStreet:nil];
            [[RegistrationFormModel sharedInstance] setNameAparment:nil];
            [myTableView reloadData];

            return;
        }
        
        if(self.arrStreet.count > 0){
            int i = 0;
            if([Id isEqualToString:@"0"]){
                [[RegistrationFormModel sharedInstance] setStreet:[self.arrStreet objectAtIndex:0]];
                [self.tableView reloadData];
            }else {
                for (i = 0; i< self.arrStreet.count; i++) {
                    KeyValueModel *selectedStreet = [self.arrStreet objectAtIndex:i];
                    if([Id isEqualToString:selectedStreet.Key]){
                        [[RegistrationFormModel sharedInstance] setStreet:selectedStreet];
                        break;
                    }
                }
                [self.tableView reloadData];
            }
        }
        [self.delegate hideMBProcessListRegis];
        if (self.Id.length <= 0 || [self.Id isEqualToString:@"0"]) {
            [self LoadNameVilla:@"0"];
        
        } else {
            [self LoadNameVilla:self.NameVilla];
        
        }
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetStreetOrCondominiumList-LoadStreet",[error localizedDescription]]];
        [self.delegate hideMBProcessListRegis];
        [[RegistrationFormModel sharedInstance] setStreet:nil];
        [[RegistrationFormModel sharedInstance] setNameAparment:nil];
        self.arrNameVilla = nil;

    }];
    
}

-(void)LoadTypeHouse:(NSString *) Id {
    self.arrTypeHouse = [Common getTypeHouse];
    int i = 0;
    if([Id isEqualToString:@"0"]){
        [[RegistrationFormModel sharedInstance] setTypeOfHouse:[self.arrTypeHouse objectAtIndex:0]];
    }else {
        for (i = 0; i< self.arrTypeHouse.count; i++) {
            KeyValueModel *selectedTypeHouse = [self.arrTypeHouse objectAtIndex:i];
            if([Id isEqualToString:selectedTypeHouse.Key]){
                [[RegistrationFormModel sharedInstance] setTypeOfHouse:selectedTypeHouse];
                isSelectedTypeOfHouse = YES;
                break;
            }
        }
    }
    [self addArrayCell];
    
}

-(void)LoadPosition:(NSString *)Id {
    int i = 0;
    self.arrHousePosition = [Common getHouseCoordinate];
    if([Id isEqualToString:@"0"]){
        [[RegistrationFormModel sharedInstance] setHousePosition:[self.arrHousePosition objectAtIndex:0]];
    }else {
        for (i = 0; i< self.arrHousePosition.count; i++) {
            KeyValueModel *selectedPosition = [self.arrHousePosition objectAtIndex:i];
            if([Id isEqualToString:selectedPosition.Key]){
                [[RegistrationFormModel sharedInstance] setHousePosition:selectedPosition];
                isSelectedHousePosition = YES;
                return;
            }
        }
    }
    
}

-(void)LoadNameVilla:(NSString *)Id {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self.delegate hideMBProcessListRegis];
        return;
    }
    if (isSelectedStreet == YES) {
        [self.delegate showMBProcessListRegis];
    }
    if (Id.length <1) {
        Id = @"0";
    }
    ShareData *shared = [ShareData instance];
    
    [shared.appProxy GetStreetOrCondominiumList:shared.currentUser.LocationID DistrictName:[[RegistrationFormModel sharedInstance] county].Key Ward:[[RegistrationFormModel sharedInstance] ward].Key Type:@"1" completionHandler:^(id result, NSString *errorCode, NSString *message) {
        
        self.arrNameVilla = result;
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self.delegate logOutWhenEndSession];
            [self.delegate hideMBProcessListRegis];
            return;
        }
        //vutt11
        if ([message isEqualToString:@"Error Service: Unauthorized"]) {
            [self.delegate hideMBProcessListRegis];
            return;
        }
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self.delegate hideMBProcessListRegis];
            [[RegistrationFormModel sharedInstance] setNameAparment: nil];
            [myTableView reloadData];
            return ;
        }
        
        if (self.arrNameVilla.count <= 0) {
            [[RegistrationFormModel sharedInstance] setNameAparment: nil];
            [myTableView reloadData];
        }
        
        if(self.arrNameVilla.count > 0){
            if([Id isEqualToString:@"0"]){
                [[RegistrationFormModel sharedInstance] setNameAparment:[self.arrNameVilla objectAtIndex:0]];
                [myTableView reloadData];
            }else {
                KeyValueModel *selectedNameVilla = [self.arrNameVilla objectAtIndex:0];
                for (selectedNameVilla in self.arrNameVilla) {
                    if([Id isEqualToString:selectedNameVilla.Key]){
                        [[RegistrationFormModel sharedInstance] setNameAparment:selectedNameVilla];
                        [myTableView reloadData];
                        break;
                    }
                }
            }
        }
    
        if (isSelectedStreet == YES || isSelectedCounty == YES || isSelectedWard == YES) {
            [self.delegate hideMBProcessListRegis];

            return;
        }
        
    } errorHandler:^(NSError *error) {
        [self.delegate hideMBProcessListRegis];
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetStreetOrCondominiumList",[error localizedDescription]]];
    }];
}

-(void)LoadPhone1:(NSString *) Id {
    self.arrTypePhone = [Common getPhoneType];
    int i = 0;
    if([Id isEqualToString:@"0"]){
        [[RegistrationFormModel sharedInstance] setTypePhone1:[self.arrTypePhone objectAtIndex:3]];
    }else {
        for (i = 0; i< self.arrTypePhone.count; i++) {
            KeyValueModel *selectedPhone1 = [self.arrTypePhone objectAtIndex:i];
            if([Id isEqualToString:selectedPhone1.Key]){
                isSelectedTypePhone1 = YES;
                [[RegistrationFormModel sharedInstance] setTypePhone1:selectedPhone1];
                return;
            }
        }
    }
}

-(void)LoadPhone2:(NSString *) Id {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];

        [self.delegate hideMBProcessListRegis];
        return;
    }
    self.arrTypePhone2 = [Common getPhoneType];
    int i = 0;
    if([Id isEqualToString:@"0"]){
        [[RegistrationFormModel sharedInstance] setTypePhone2:[self.arrTypePhone2 objectAtIndex:3]];
    }else {
        for (i = 0; i< self.arrTypePhone2.count; i++) {
            KeyValueModel *selectedPhone2 = [self.arrTypePhone2 objectAtIndex:i];
            if([Id isEqualToString:selectedPhone2.Key]){
                isSelectedTypePhone2 = YES;
                [[RegistrationFormModel sharedInstance] setTypePhone2:selectedPhone2];
                return;
            }
        }
    }
}

/****************************/
//MARK: - UIPOPOVERLISTVIEW
/****************************/
- (void)setupDropDownView: (NSInteger) tag {
    [self.view endEditing:YES];
    
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.tag = tag;
    poplistview.listView.scrollEnabled = TRUE;
    switch (tag) {
        case type:
            [poplistview setTitle:NSLocalizedString(@"Loại hình", @"")];
            break;
        case typeCustomer:
            [poplistview setTitle:NSLocalizedString(@"Loại khách hàng", @"")];
            break;
        case county:
            [poplistview setTitle:NSLocalizedString(@"Quận/Huyện", @"")];
            break;
        case ward:
            [poplistview setTitle:NSLocalizedString(@"Phường/Xã", @"")];
            break;
        case typeOfHouse:
            [poplistview setTitle:NSLocalizedString(@"Loại nhà", @"")];
            break;
        case street:
            [poplistview setTitle:NSLocalizedString(@"Đường", @"")];
            break;
        case nameApartment:
            [poplistview setTitle:NSLocalizedString(@"Tên chung cư", @"")];
            break;
        case typePhone1:
            [poplistview setTitle:NSLocalizedString(@"Di Động", @"")];
            break;
        case typePhone2:
            [poplistview setTitle:NSLocalizedString(@"Di Động", @"")];
            break;
        case housePosition:
            [poplistview setTitle:NSLocalizedString(@"Vị trí nhà", @"")];
            break;
        default:
            break;
    }
    
    [poplistview show];
    
}


- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

#pragma mark - UIPopoverListViewDataSource

- (void)setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell tag:(NSInteger)tag{
    KeyValueModel *model;
  
    cell.textLabel.numberOfLines = 0;
    [cell.textLabel sizeToFit];
    switch (tag) {
        case type:
            model = [self.arrType objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case typeCustomer:
            model = [self.arrTypeCustomer objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case county:
            model = [self.arrDistrict objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case ward:
            model = [self.arrWard objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case typeOfHouse:
            model = [self.arrTypeHouse objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case street:
            model = [self.arrStreet objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case nameApartment:
            model = [self.arrNameVilla objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case typePhone1:
            model = [self.arrTypePhone objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case typePhone2:
            model = [self.arrTypePhone objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case housePosition:
            model = [self.arrHousePosition objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        default:
            break;
    }
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView cellForIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row cell:cell tag:popoverListView.tag];
    cell.textLabel.font = [UIFont fontWithName:@"Arial" size:14];
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = 0;
    switch (popoverListView.tag) {
        case type: {
            count = self.arrType.count;
            break;
        }
        case typeCustomer: {
            count = self.arrTypeCustomer.count;
            break;
        }
        case county:{
            count = self.arrDistrict.count;
            break;
        }
        case ward:{
            count = self.arrWard.count;
            break;
        }
        case typeOfHouse:{
            count = self.arrTypeHouse.count;
            break;
        }
        case street:{
            count = self.arrStreet.count;
            break;
        }
        case nameApartment: {
            count = self.arrNameVilla.count;
            break;
        }
        case typePhone1:{
            count = self.arrTypePhone.count;
            break;
        }
        case typePhone2:{
            count = self.arrTypePhone.count;
            break;
        }
        case housePosition: {
            count = self.arrHousePosition.count;
            break;
        }
    }
    
    NSLog(@"------ StevenNguuyen ------ Count %ld", (long)count);
    
    return count;
}

#pragma mark - UIPopoverListViewDelegate

- (void) didDropDownSelected:(NSInteger)row tag:(NSInteger)tag {
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    switch (tag) {
        case type:{
            NSLog(@"%ld",(long)row);
            [[RegistrationFormModel sharedInstance] setType:[self.arrType objectAtIndex:row]];
            [myTableView reloadData];
            break;
        }
        case typeCustomer:{
            [[RegistrationFormModel sharedInstance] setTypeCustomer:[self.arrTypeCustomer objectAtIndex:row]];
            [myTableView reloadData];
            break;
        }
        case county:{
            [[RegistrationFormModel sharedInstance] setCounty:[self.arrDistrict objectAtIndex:row]];
            [myTableView reloadData];
            if ([[[RegistrationFormModel sharedInstance] county].Key length] > 0) {
                isSelectedCounty = YES;
                
                //MARK: -StevenNguyen-FixError
                NSString *strKeyDistrict = [[RegistrationFormModel sharedInstance] county].Key;
                [self loadWardAndCache:kArrayWard(strKeyDistrict) IDWard:@"0" IDStreet:@"0"];
                
                break;
            }
            break;
        }
        case ward:{
            [[RegistrationFormModel sharedInstance] setWard:[self.arrWard objectAtIndex:row]];
            [myTableView reloadData];
            isSelectedCounty = YES;
            [self LoadStreet:@"0"];
            break;
        }
        case typeOfHouse:{
            [[RegistrationFormModel sharedInstance] setTypeOfHouse:[self.arrTypeHouse objectAtIndex:row]];
            // Refesh lai so nha khi co thay doi
            
            [[RegistrationFormModel sharedInstance] setNumberOfHouse:@""];
            [self addArrayCell];
            [myTableView reloadData];
            break;
        }
        case street:{
            
            NSLog(@"------ StevenNguuyen ------ ROW %ld", (long)row);
            
            [[RegistrationFormModel sharedInstance] setStreet:[self.arrStreet objectAtIndex:row]];
            [myTableView reloadData];
            [self LoadNameVilla:@"0"];
            break;
        }
        case nameApartment:{
            [[RegistrationFormModel sharedInstance] setNameAparment:[self.arrNameVilla objectAtIndex:row]];
            [myTableView reloadData];
            break;
        }
        case typePhone1: {
            [[RegistrationFormModel sharedInstance] setTypePhone1:[self.arrTypePhone objectAtIndex:row]];
            [myTableView reloadData];
            break;
        }
        case typePhone2: {
            [[RegistrationFormModel sharedInstance] setTypePhone2:[self.arrTypePhone objectAtIndex:row]];
            [myTableView reloadData];
            break;
        }
        case housePosition: {
            [[RegistrationFormModel sharedInstance] setHousePosition:[self.arrHousePosition objectAtIndex:row]];
            [myTableView reloadData];
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - UIPopoverListViewDelegate

- (void)popoverListView:(UIPopoverListView *)popoverListView didSelectIndexPath:(NSIndexPath *)indexPath {
    [self didDropDownSelected:indexPath.row tag:popoverListView.tag];
    [popoverListView dismiss];
    
}

/****************************************/
//MARK: -UITABLEVIEWDELEGATE
/****************************************/
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int numberStyle = [[arrCell objectAtIndex:indexPath.row] intValue];
    
    if (numberStyle == numberPhone1 || numberStyle == numberPhone2) {
        return 60;
    }
    else if (numberStyle == notePlant || numberStyle == noteAddress) {
        return 95;
    }
    else if (numberStyle == chooseImage) {
        return 110;
    }
    else {
        return 60;
    }
    
}

/*********************************************/
//MARK: -UITABLEVIEWDATASOURCE
/*********************************************/
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrCell.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int numberStyle = [[arrCell objectAtIndex:indexPath.row] intValue];
    
    switch (numberStyle) {
        case name: {
            AniTextFieldCell *cell = (AniTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTxtF" forIndexPath:indexPath];
            cell.delegate = self;
            cell.txtFValue.text = [[RegistrationFormModel sharedInstance] name];
            [cell.txtFValue setKeyboardType:UIKeyboardTypeDefault];
            [cell configCell:@"Họ tên khách hàng" placeHolderTextField:@"Nhập họ và tên"];
            [cell configCellWithTag:name];
            
            return cell;
            
        }
            
        case isCard: {
            
            AniTextFieldCell *cell = (AniTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTxtF" forIndexPath:indexPath];
            cell.delegate = self;
            cell.txtFValue.text = [[RegistrationFormModel sharedInstance] isCard];
            [cell.txtFValue setKeyboardType:UIKeyboardTypeNamePhonePad];
            [cell configCell:@"CMND" placeHolderTextField:@"Nhập số CMND"];
            [cell configCellWithTag:isCard];
        
            return cell;
        }
            
        case date: {
            
            AniDropDownCellTableViewCell *cell = (AniDropDownCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellDropDown" forIndexPath:indexPath];
            NSString *str = [[RegistrationFormModel sharedInstance] date] ?:@"";
            cell.delegate = self;
            cell.btnChoose.enabled = YES;
            if ( ![str  isEqualToString: @""] ) {
                cell.btnChoose.selected = YES;
                NSString *stringDate = [self convertStringDateToStringDateNew:str];
                [cell configDropDownCell:@"Ngày Sinh" titleButton:stringDate kTag:date];
            }
            else {
                cell.btnChoose.selected = NO;
                [cell configDropDownCell:@"Ngày Sinh" titleButton:@"Chọn Ngày Sinh" kTag:date];
            }
            return cell;
        }
            
        case address: {
            
            AniTextFieldCell *cell = (AniTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTxtF" forIndexPath:indexPath];
            cell.delegate = self;
            cell.txtFValue.text = [[RegistrationFormModel sharedInstance] address];
            [cell.txtFValue setKeyboardType:UIKeyboardTypeDefault];
            [cell configCell:@"Địa chỉ trên CMND" placeHolderTextField:@"Nhập địa chỉ trên CMND"];
            [cell configCellWithTag:address];
            return cell;
        }
            
        case taxCode: {
            
            AniTextFieldCell *cell = (AniTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTxtF" forIndexPath:indexPath];
            cell.delegate = self;
            cell.txtFValue.text = [[RegistrationFormModel sharedInstance] taxCode];
            [cell.txtFValue setKeyboardType:UIKeyboardTypeEmailAddress];
            [cell configCell:@"Mã số thuế" placeHolderTextField:@"Nhập mã số thuế"];
            [cell configCellWithTag:taxCode];
            return cell;
            
        }
            
        case typePhone1: {
            AniDropDownCellTableViewCell *cell = (AniDropDownCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellDropDown" forIndexPath:indexPath];
            NSString *str = [[RegistrationFormModel sharedInstance] typePhone1].Values ?:@"";
            cell.delegate = self;
            cell.btnChoose.enabled = YES;
            if ( ![str  isEqualToString: @""] ) {
                cell.btnChoose.selected = YES;
                [cell configDropDownCell:@"Di Động" titleButton:str kTag:typePhone1];
            } else {
                cell.btnChoose.selected = NO;
                [cell configDropDownCell:@"Di Động" titleButton:@"Chọn Di Động" kTag:typePhone1];
            }
            return cell;
            
        }
            
        case numberPhone1: {
            
            AniPhoneNumberCell *cell = (AniPhoneNumberCell *)[tableView dequeueReusableCellWithIdentifier:@"cellPhoneNumber" forIndexPath:indexPath];
            NSString *strNumber = [[RegistrationFormModel sharedInstance] numberPhone1];
            NSString *strUser = [[RegistrationFormModel sharedInstance] contact1];
            cell.delegate = self;
            [cell configCellWithTag:numberPhone1];
            [cell configCell:@"Số điện thoại" placeHolderPhoneNumber:@"Số điện thoại" titleUserName:@"Người liên hệ" placeHolderUserName:@"Người liên hệ"];
            //UIKeyboardTypeNamePhonePad
            [cell.txtfNameUser setKeyboardType:UIKeyboardTypeDefault];
            [cell configCellWhenSetText:strNumber username:strUser];
            return cell;
        }
            
        case typePhone2: {
            AniDropDownCellTableViewCell *cell = (AniDropDownCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellDropDown" forIndexPath:indexPath];
            NSString *str = [[RegistrationFormModel sharedInstance] typePhone2].Values ?:@"";
            cell.delegate = self;
            cell.btnChoose.enabled = YES;
            if ( ![str  isEqualToString: @""] ) {
                cell.btnChoose.selected = YES;
                [cell configDropDownCell:@"Di Động" titleButton:str kTag:typePhone2];
            } else {
                cell.btnChoose.selected = NO;
                [cell configDropDownCell:@"Di Động" titleButton:@"Chọn Di Động" kTag:typePhone2];
            }
            return cell;
            
        }
        case numberPhone2: {
            AniPhoneNumberCell *cell = (AniPhoneNumberCell *)[tableView dequeueReusableCellWithIdentifier:@"cellPhoneNumber" forIndexPath:indexPath];
            NSString *strNumber = [[RegistrationFormModel sharedInstance] numberPhone2];
            NSString *strUser = [[RegistrationFormModel sharedInstance] contact2];
            cell.delegate = self;
            [cell configCellWithTag:numberPhone2];
            [cell configCell:@"Số điện thoại" placeHolderPhoneNumber:@"Số điện thoại" titleUserName:@"Người liên hệ" placeHolderUserName:@"Người liên hệ"];
            [cell.txtfNameUser setKeyboardType:UIKeyboardTypeDefault];
            [cell configCellWhenSetText:strNumber username:strUser];
            return cell;
            
        }
            
        case email: {
            AniTextFieldCell *cell = (AniTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTxtF" forIndexPath:indexPath];
            cell.delegate = self;
            cell.txtFValue.text = [[RegistrationFormModel sharedInstance] email];
            [cell.txtFValue setKeyboardType:UIKeyboardTypeEmailAddress];
            [cell configCell:@"Email" placeHolderTextField:@"Email"];
            [cell configCellWithTag:email];
            return cell;
            
        }
            
        case county: {
            
            CacheDropDownCell *cell = (CacheDropDownCell *)[tableView dequeueReusableCellWithIdentifier:@"CacheDropDownCell" forIndexPath:indexPath];
            
            NSString *str = [[RegistrationFormModel sharedInstance] county].Values ?:@"";
            cell.delegate = self;
            cell.btnChoose.enabled = YES;
            if ( ![str  isEqualToString: @""] ) {
                cell.btnChoose.selected = YES;
                [cell configDropDownCell:@"Quận (Huyện)" titleButton:str kTag:county];
            } else {
                cell.btnChoose.selected = NO;
                [cell configDropDownCell:@"Quận (Huyện)" titleButton:@"Chọn Quận (Huyện)" kTag:county];
            }
            return cell;
            
        }
            
        case ward: {
            
            CacheDropDownCell *cell = (CacheDropDownCell *)[tableView dequeueReusableCellWithIdentifier:@"CacheDropDownCell" forIndexPath:indexPath];
            NSString *str = [[RegistrationFormModel sharedInstance] ward].Values ?:@"";
            NSString *strCounty = [[RegistrationFormModel sharedInstance] county].Values ?:@"";
            cell.delegate = self;
            cell.btnChoose.enabled = YES;
            if ( ![str  isEqualToString: @""] ) {
                cell.btnChoose.selected = YES;
                [cell configDropDownCell:@"Phường(Xã)" titleButton:str kTag:ward];
            } else {
                cell.btnChoose.selected = NO;
                [cell configDropDownCell:@"Phường(Xã)" titleButton:@"Chọn Phường(Xã)" kTag:ward];
            }
            if ([strCounty  isEqualToString: @""]) {
                cell.btnChoose.enabled = NO;
            }
            return cell;
        }
            
        case typeOfHouse: {
            
            AniDropDownCellTableViewCell *cell = (AniDropDownCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellDropDown" forIndexPath:indexPath];
            NSString *str = [[RegistrationFormModel sharedInstance] typeOfHouse].Values ?:@"";
            cell.delegate = self;
            cell.btnChoose.enabled = YES;
            if ( ![str  isEqualToString: @""] ) {
                cell.btnChoose.selected = YES;
                [cell configDropDownCell:@"Loại Nhà" titleButton:str kTag:typeOfHouse];
            } else {
                cell.btnChoose.selected = NO;
                [cell configDropDownCell:@"Loại Nhà" titleButton:@"Chọn loại nhà" kTag:typeOfHouse];
            }
            return cell;
            
        }
            
        case street: {
            
            AniDropDownCellTableViewCell *cell = (AniDropDownCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellDropDown" forIndexPath:indexPath];
            NSString *str = [[RegistrationFormModel sharedInstance] street].Values ?:@"";
            NSString *strCounty = [[RegistrationFormModel sharedInstance] county].Values ?:@"";
            cell.delegate = self;
            cell.btnChoose.enabled = YES;
            if ( ![str  isEqualToString: @""] ) {
                cell.btnChoose.selected = YES;
                [cell configDropDownCell:@"Đường" titleButton:str kTag:street];
            } else {
                cell.btnChoose.selected = NO;
                [cell configDropDownCell:@"Đường" titleButton:@"Chọn Đường" kTag:street];
            }
            
            if ([strCounty  isEqualToString: @""]) {
                cell.btnChoose.enabled = NO;
            }
            
            return cell;
            
        }
            
        case housePosition: {
            
            AniDropDownCellTableViewCell *cell = (AniDropDownCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellDropDown" forIndexPath:indexPath];
            NSString *str = [[RegistrationFormModel sharedInstance] housePosition].Values ?:@"";
            cell.delegate = self;
            if ( ![str  isEqualToString: @""] ) {
                cell.btnChoose.selected = YES;
                [cell configDropDownCell:@"Vị trí nhà" titleButton:str kTag:housePosition];
            } else {
                cell.btnChoose.selected = NO;
                [cell configDropDownCell:@"Vị trí nhà" titleButton:@"Chọn Vị trí nhà" kTag:housePosition];
            }
            
            return cell;
            
        }
            
        case numberOfHouse: {
            
            AniTextFieldCell *cell = (AniTextFieldCell *)[tableView dequeueReusableCellWithIdentifier:@"cellTxtF" forIndexPath:indexPath];
            cell.delegate = self;
            cell.txtFValue.text = [[RegistrationFormModel sharedInstance] numberOfHouse];
            [cell.txtFValue setKeyboardType:UIKeyboardTypeDefault];
            [cell configCell:@"Số nhà" placeHolderTextField:@"Số nhà"];
            [cell configCellWithTag:numberOfHouse];
            
            return cell;
            
        }
            
        case regulationAddress: {
            
            ButtonTitleCell *cell = (ButtonTitleCell *)[tableView dequeueReusableCellWithIdentifier:@"cellButtonTitle123" forIndexPath:indexPath];
            cell.delegate = self;
            [cell configCell:@"Qui định nhập địa chỉ" ktag:regulationAddress];
            
            return cell;
        }
            
        case apartment: {
            
            AniApartmentCell *cell = (AniApartmentCell *)[tableView dequeueReusableCellWithIdentifier:@"cellApartment" forIndexPath:indexPath];
            NSString *strLot = [[RegistrationFormModel sharedInstance] lot];
            NSString *strFloor = [[RegistrationFormModel sharedInstance] floor];
            NSString *strRoom = [[RegistrationFormModel sharedInstance] room];
            cell.delegate = self;
            [cell.txtfLot setKeyboardType:UIKeyboardTypeNamePhonePad];
            [cell.txtfFloor setKeyboardType:UIKeyboardTypeNamePhonePad];
            [cell.txtfRoom setKeyboardType:UIKeyboardTypeNamePhonePad];
            [cell configCellWhenSetText:strLot Floor:strFloor Room:strRoom];
            [cell configCellWithTag:apartment];
            
            return cell;
            
        }
            
        case nameApartment: {
            
            AniDropDownCellTableViewCell *cell = (AniDropDownCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellDropDown" forIndexPath:indexPath];
            NSString *str = [[RegistrationFormModel sharedInstance] nameAparment].Values ?:@"";
            cell.delegate = self;
            cell.btnChoose.enabled = YES;
            if ( ![str  isEqualToString: @""] ) {
                cell.btnChoose.selected = YES;
                [cell configDropDownCell:@"Tên chung cư" titleButton:str kTag:nameApartment];
            } else {
                cell.btnChoose.selected = NO;
                [cell configDropDownCell:@"Tên chung cư" titleButton:@"Chọn tên chung cư" kTag:nameApartment];
            }
            
            return cell;
            
        }
            
        case noteAddress: {
            
            AniTextViewCell *cell = (AniTextViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellNote" forIndexPath:indexPath];
            NSString *strNote = [[RegistrationFormModel sharedInstance] noteAddress];
            cell.delegate = self;
            cell.lblTitle.text = @"Ghi chú địa chỉ";
            [cell.txtvTitle setKeyboardType:UIKeyboardTypeDefault];
            [cell configCell:strNote kTag:noteAddress];
            
            return cell;
            
        }
            
        case notePlant: {
            
            AniTextViewCell *cell = (AniTextViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellNote" forIndexPath:indexPath];
            NSString *strNote = [[RegistrationFormModel sharedInstance] notePlant];
            cell.delegate = self;
            cell.lblTitle.text = @"Ghi chú triển khai";
            [cell.txtvTitle setKeyboardType:UIKeyboardTypeDefault];
            [cell configCell:strNote kTag:notePlant];
            
            return cell;
            
        }
            
        case type: {
            
            AniDropDownCellTableViewCell *cell = (AniDropDownCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellDropDown" forIndexPath:indexPath];
            
            NSString *str = [[RegistrationFormModel sharedInstance] type].Values ?:@"";
            cell.delegate = self;
            cell.btnChoose.enabled = YES;
            if ( ![str  isEqualToString: @""] ) {
                cell.btnChoose.selected = YES;
                [cell configDropDownCell:@"Loại hình" titleButton:str kTag:type];
            } else {
                cell.btnChoose.selected = NO;
                [cell configDropDownCell:@"Loại hình" titleButton:@"Chọn loại hình" kTag:type];
            }
            return cell;
            
        }
            
        case typeCustomer: {
            
            AniDropDownCellTableViewCell *cell = (AniDropDownCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"cellDropDown" forIndexPath:indexPath];
            
            NSString *str = [[RegistrationFormModel sharedInstance] typeCustomer].Values ?:@"";
            cell.delegate = self;
            cell.btnChoose.enabled = YES;
            if ( ![str  isEqualToString: @""] ) {
                cell.btnChoose.selected = YES;
                [cell configDropDownCell:@"Loại khách hàng" titleButton:str kTag:typeCustomer];
            } else {
                cell.btnChoose.selected = NO;
                [cell configDropDownCell:@"Loại khách hàng" titleButton:@"Chọn loại khách hàng" kTag:typeCustomer];
            }
            return cell;
            
        }
            
        case chooseImage: {
            
            UploadImageCell *cell = (UploadImageCell *)[tableView dequeueReusableCellWithIdentifier:@"cellChooseImage" forIndexPath:indexPath];
            UIImage *img = [[RegistrationFormModel sharedInstance] imageUpload];
            if (img == NULL) {
                
            } else {
                cell.imgView.image = img;
            }
            cell.delegate = self;
            return cell;
            
        }
        case button: {
            
            ButtonNextCell *cell = (ButtonNextCell *)[tableView dequeueReusableCellWithIdentifier:@"cellButton" forIndexPath:indexPath];
            cell.delegate = self;
            [cell hiddenButtonBack];
            return cell;
            
        }
            
        default: {
            
            ButtonNextCell *cell = (ButtonNextCell *)[tableView dequeueReusableCellWithIdentifier:@"cellButton" forIndexPath:indexPath];
            [cell hiddenButtonBack];
            return cell;
            
        }
            
    }
    
}

/***************************************/
//MARK: -ANITEXTFIELDCELLDELEGATE, -
/***************************************/
-(void)valueTextfield:(NSString *)value kTag:(NSInteger)tag {
    
    switch (tag) {
        case name: {
            [[RegistrationFormModel sharedInstance] setName:value];
            break;
            
        }
        case isCard: {
            [[RegistrationFormModel sharedInstance] setIsCard:value];
            break;
            
        }
            
        case date: {
            [self showDatePicker];
            [self limitDate:[NSDate date] minYear:-99 maxYear:0];
            break;
            
        }
        case address: {
            [[RegistrationFormModel sharedInstance] setAddress:value];
            break;
            
        }
        case taxCode: {
            [[RegistrationFormModel sharedInstance] setTaxCode:value];
            break;
            
        }
        case numberPhone1: {
            [[RegistrationFormModel sharedInstance] setName:value];
            break;
            
        }
        case numberPhone2: {
            [[RegistrationFormModel sharedInstance] setName:value];
            break;
            
        }
            
        case email: {
            [[RegistrationFormModel sharedInstance] setEmail:value];
            break;
            
        }
            
        case county: {
            break;
            
        }
            
        case ward: {
            break;
            
        }
            
        case typeOfHouse: {
            break;
            
        }
            
        case street: {
            break;
            
        }
            
        case housePosition: {
            [[RegistrationFormModel sharedInstance] setHousePosition:value];
            break;
            
        }
            
        case numberOfHouse: {
            [[RegistrationFormModel sharedInstance] setNumberOfHouse:value];
            break;
            
        }
            
        case noteAddress: {
            [[RegistrationFormModel sharedInstance] setNoteAddress:value];
            break;
            
        }
            
        case notePlant: {
            [[RegistrationFormModel sharedInstance] setNotePlant:value];
            break;
            
        }
            
        case type: {
            break;
            
        }
            
        case typeCustomer: {
            break;
            
        }
            
        case button: {
            [[RegistrationFormModel sharedInstance] setName:value];
            break;
        }
        default: {
            [[RegistrationFormModel sharedInstance] setName:value];
            break;
        }
    }
}

//Delegate - Protocol AniDropDownCellTableViewCell

-(void)dropDownButton:(id)sender kTag:(NSInteger)tag {
    
    switch (tag) {
        case name: {
           
            break;
            
        }
            
        case isCard: {
            
            break;
            
        }
            
            
        case date: {
            
            [delegate showButton:sender kTag:date];
            
            break;
            
        }
            
            
        case address: {
        
            break;
            
        }
            
            
        case taxCode: {
           
            break;
            
        }
            
        case typePhone1: {
            [self setupDropDownView:typePhone1];
            break;
        }
            
        case numberPhone1: {
       
            break;
            
        }
            
        case typePhone2: {
            [self setupDropDownView:typePhone2];
            break;
        }
            
        case numberPhone2: {
            
            break;
            
        }
        case email: {
           
            break;
            
        }
        case county: {
          
            [self setupDropDownView:county];
            break;
            
        }
            
        case ward: {
           
            [self setupDropDownView:ward];
            break;
            
        }
            
        case typeOfHouse: {
            
            [self setupDropDownView:typeOfHouse];
            break;
            
        }
            
        case street: {
         
            [self setupDropDownView:street];
            break;
            
        }
            
        case housePosition: {
            [self setupDropDownView:housePosition];
            break;
            
        }
            
        case numberOfHouse: {
            break;
            
        }
            
        case noteAddress: {
        
            break;
            
        }
            
        case notePlant: {
            break;
            
        }
            
        case type: {
            [self setupDropDownView:type];
            break;
            
        }
            
        case typeCustomer: {
            [self setupDropDownView:typeCustomer];
            break;
            
        }
            
        case nameApartment: {
            
            [self setupDropDownView:nameApartment];
            break;
            
        }
            
        case regulationAddress: {
            
            [delegate showButton:sender kTag:regulationAddress];
            break;
            
        }
            
        case button: {
         
            break;
            
        }
            
        default: {
            
            break;
            
        }
    }
}

-(void)freshCompleted:(NSInteger)tag {
    
    switch (tag) {
        case county:
            [self loadDistrictAndCache];
            break;
        case ward:
            if ([[[RegistrationFormModel sharedInstance] county].Key length] > 0) {
                isSelectedCounty = YES;
                //MARK: -StevenNguyen-FixError
                NSString *strKeyDistrict = [[RegistrationFormModel sharedInstance] county].Key;
                [self loadWardAndCache:kArrayWard(strKeyDistrict) IDWard:@"0" IDStreet:@"0"];
            } else {
                [self loadWardAndCache:@"0" IDWard:@"0" IDStreet:@"0"];
            }
            break;
        default:
            break;
    }
    
}

/***************************************/
//MARK: -AniPhoneNumberCellDelegate
/***************************************/

-(void)valueTextfield:(NSString *)value kTag:(NSInteger)tag tagTextField:(NSInteger)tagTextField {
    
    switch (tag) {
        case numberPhone1: {
            //Phone Number
            if (tagTextField == 1) {
                [[RegistrationFormModel sharedInstance] setNumberPhone1:value];
                
            } else if (tagTextField == 2) {
                //userName
                [[RegistrationFormModel sharedInstance] setContact1:value];
                
            }
            
            break;
        }
        case numberPhone2: {
            //Phone Number
            if (tagTextField == 1) {
                [[RegistrationFormModel sharedInstance] setNumberPhone2:value];
                
            } else if (tagTextField == 2) {
                //userName
                [[RegistrationFormModel sharedInstance] setContact2:value];
                
            }
            break;
        }
        case apartment: {
            
            //Phone Number
            if (tagTextField == 1) {
                //lot
                [[RegistrationFormModel sharedInstance] setLot:value];
                
            } else if (tagTextField == 2) {
                //floor
                [[RegistrationFormModel sharedInstance] setFloor:value];
                
            } else if (tagTextField == 3) {
                //room
                [[RegistrationFormModel sharedInstance] setRoom:value];
                
            }
            break;
            
        }
        default:
            break;
    }
    
}

/***************************************/
//MARK: -AniApartmentCellDelegate
/***************************************/

/***************************************/
//MARK: -ButtonNextCellDelegate
/***************************************/

-(void)pressedButton:(id)sender kTag:(NSInteger)tag ktagButton:(NSInteger)tagButton {
    
    switch (tagButton) {
        case 1:{
            //save
            [delegate scrollToView:1 tagButton:1];
            break;
        }
        case 2:{
            //back
            [delegate scrollToView:1 tagButton:2];
            break;
        }
        default:
            break;
    }
}

//MARK: -UPLOADIMAGEDELEGATE
-(void) ButtonUploadImage {
    
    [self.delegate loadImageViewController];
}

#pragma mark - Custom Accessors
- (UIImagePickerController *) imagePickerController {
    if (!_imagePickerController) {
        _imagePickerController = [[UIImagePickerController alloc] init];
        _imagePickerController.allowsEditing = NO;
        _imagePickerController.delegate = self;
        
    }
    return _imagePickerController;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo: (NSDictionary *)info {
    imageData = @"";
    UIImage *originalImage, *editedImage, *imageToUse;
    
    // Handle a still image picked from a photo album
    editedImage = (UIImage *)[info objectForKey:UIImagePickerControllerEditedImage];
    
    originalImage = (UIImage *)[info objectForKey:UIImagePickerControllerOriginalImage];
    
    if (editedImage) {
        imageToUse = editedImage;
    } else {
        imageToUse = originalImage;
        
    }
    
    CGFloat scale = imageToUse.size.width / 300;
    imageToUse = [UIImage scaleImage:imageToUse toSize:CGSizeMake(imageToUse.size.width/scale, imageToUse.size.height/scale)];
    
 [[RegistrationFormModel sharedInstance] setImageUpload:imageToUse];
    
   [myTableView reloadData];
    
    [self upDateImage];
    
    [[picker presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)upDateImage{
    
    UIImage *img = [[RegistrationFormModel sharedInstance] imageUpload];
    
    NSData *imageToUseData = UIImageJPEGRepresentation(img, 0.2);
    
    imageData = [self base64forData:imageToUseData];

    [self.delegate showMBProcessListRegis];
    
    ShareData *shared  = [ShareData instance];
    [shared.registrationFormProxy upDateImage:shared.currentUser.userName Image:imageData RegCode:@"" Type:1 Completehander:^(id result,NSString *errorCode,NSString *message) {
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self.delegate logOutWhenEndSession];
            [self.delegate hideMBProcessListRegis];
            return;
        }
        //vutt11
        if ([message isEqualToString:@"Error Service: Unauthorized"]) {
            [self.delegate hideMBProcessListRegis];
            return;
        }
        
        if (message.length <10) {
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self.delegate hideMBProcessListRegis];
            return;
        }
        
        [[RegistrationFormModel sharedInstance] setImgInfo:message];
        
        [self.delegate hideMBProcessListRegis];
        
    }errorHandler:^(NSError *error){
        
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self.delegate hideMBProcessListRegis];
        return;
        
    }];
}

-(NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger i;
    for ( i=0; i < length; i += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = i; j < (i + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (i / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (i + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (i + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] ;
}


@end
