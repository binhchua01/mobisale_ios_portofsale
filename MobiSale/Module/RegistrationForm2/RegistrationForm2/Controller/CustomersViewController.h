//
//  CustomersViewController.h
//  MobiSale
//
//  Created by Bored Ninjas on 11/18/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIPopoverListView.h"
#import "BaseViewController.h"

@protocol CustomersViewControllerDelegate <NSObject>

-(void) scrollToView:(NSInteger)tag tagButton:(NSInteger)tagButton;

-(void) showMBProcessListRegis;

-(void) hideMBProcessListRegis;

-(void) logOutWhenEndSession;

@end

@interface CustomersViewController : BaseViewController<UIPopoverListViewDelegate,UIPopoverListViewDataSource>

@property (nonatomic, weak) id <CustomersViewControllerDelegate> delegate;

@property (retain, nonatomic) NSMutableArray *arrCustomer;
@property (retain, nonatomic) NSMutableArray *arrISP;
@property (retain, nonatomic) NSMutableArray *arrAddress;
@property (retain, nonatomic) NSMutableArray *arrSolvency;
@property (retain, nonatomic) NSMutableArray *arrPartnerCustomer;
@property (retain, nonatomic) NSMutableArray *arrEntity;

@property (strong, nonatomic) NSString *Id;

-(void)LoadObjectCustomer:(NSString *) Id;

-(void)LoadISP:(NSString *) Id;

-(void)LoadCurrenPlace:(NSString *) Id;

-(void)LoadSolveny:(NSString *) Id;

-(void)LoadClientPartner:(NSString *) Id;

-(void)LoadLegal:(NSString *) Id;

-(void)reloadDataViewController;

@end
