//
//  IPTVPrice.m
//  MobiSale
//
//  Created by ISC-DanTT on 5/25/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "IPTVPrice.h"

@implementation IPTVPrice

@synthesize DacSacHDPrice, FimHotPrice, FimPlusPrice, HDBoxPrice, KPlusPrice, PLCPrice, STBPrice, VTCHDPrice, VTVCabPrice;

@end
