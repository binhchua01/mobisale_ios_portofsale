//
//  BoxRecord.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 2/2/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BoxRecord : NSObject

@property (nonatomic,retain) NSString *BoxType;
@property (nonatomic,retain) NSString *Count;
@property (nonatomic,retain) NSString *ID;
@property (nonatomic,retain) NSString *Package;
@property (nonatomic,retain) NSString *PromotionID;
@property (nonatomic,retain) NSString *RegID;


@end
