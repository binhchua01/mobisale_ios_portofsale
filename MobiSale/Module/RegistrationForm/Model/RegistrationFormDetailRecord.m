//
//  RegistrationFormDetailRecord.m
//  MobiSale
//
//  Created by HIEUPC on 1/27/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "RegistrationFormDetailRecord.h"
#import "AppDelegate.h"
#import "ShareData.h"

@implementation NSString(RegistrationFormDetailRecord)

//check string "<null>" when server return.
-(NSString *)checkNull {
    
    @try {
        if ([self  isEqualToString: @"<null>"]) {
            return @"";
        }
        return self;
    } @catch (NSException *exception) {
        NSLog(@"%@", exception);
        [CrashlyticsKit recordCustomExceptionName:@"RegistrationFormDetailRecord - funtion (checkNull)-Error handle Parse JSON" reason:[[ShareData instance].currentUser.userName lowercaseString]frameArray:@[]];
        return @"";
    }
    
}

@end


@implementation RegistrationFormDetailRecord

@synthesize Address;
@synthesize AddressOrigin;
@synthesize BillTo_City;
@synthesize BillTo_District;
@synthesize BillTo_Number;
@synthesize BillTo_Street;
@synthesize BillTo_Ward;
@synthesize BookPortType;
@synthesize BranchCode;
@synthesize CableStatus;
@synthesize Contact;
@synthesize Contact_1;
@synthesize Contact_2;
@synthesize Contract;
@synthesize CreateBy;
@synthesize CreateDate;
@synthesize CurrentHouse;
@synthesize CusType;
@synthesize CusTypeDetail;
@synthesize CustomerType;
@synthesize Deposit;
@synthesize DepositBlackPoint;
@synthesize Description;
@synthesize DescriptionIBB;
@synthesize DivisionID;
@synthesize Email;
@synthesize EoC;
@synthesize Floor;
@synthesize FullName;
@synthesize ID;
@synthesize INSCable;
@synthesize IPTVBoxCount;
@synthesize IPTVChargeTimes;
@synthesize IPTVDeviceTotal;
@synthesize IPTVHBOChargeTimes;
@synthesize IPTVHBOPrepaidMonth;
@synthesize IPTVFimPlusChargeTimes;
@synthesize IPTVFimPlusPrepaidMonth;
@synthesize IPTVFimHotChargeTimes;
@synthesize IPTVFimHotPrepaidMonth;
@synthesize IPTVKPlusChargeTimes;
@synthesize IPTVKPlusPrepaidMonth;
@synthesize IPTVPLCCount;
@synthesize IPTVPackage;
@synthesize IPTVPrepaid;
@synthesize IPTVPrepaidTotal;
@synthesize IPTVPromotionID;
@synthesize IPTVPromotionIDBoxOrder;
@synthesize IPTVRequestDrillWall;
@synthesize IPTVRequestSetUp;
@synthesize IPTVReturnSTBCount;
@synthesize IPTVStatus;
@synthesize IPTVTotal;
@synthesize IPTVUseCombo;
@synthesize IPTVVTCChargeTimes;
@synthesize IPTVVTCPrepaidMonth;
@synthesize IPTVVTVChargeTimes;
@synthesize IPTVVTVPrepaidMonth;
@synthesize ISPType;
@synthesize Image;
@synthesize InDType;
@synthesize InDoor;
@synthesize InternetTotal;
@synthesize Latlng;
@synthesize LegalEntity;
@synthesize LocalType;
@synthesize LocalTypeName;
@synthesize LocationID;
@synthesize Lot;
@synthesize MapCode;
@synthesize Modem;
@synthesize NameVilla;
@synthesize Note;
@synthesize ODCCableType;
@synthesize ObjID;
@synthesize ObjectType;
@synthesize OutDType;
@synthesize OutDoor;
@synthesize PartnerID;
@synthesize Passport;
@synthesize PaymentAbility;
@synthesize PhoneNumber;
@synthesize Phone_1;
@synthesize Phone_2;
@synthesize Position;
@synthesize PromotionID;
@synthesize PromotionName;
@synthesize RegCode;
@synthesize Room;
@synthesize StatusDeposit;
@synthesize Supporter;
@synthesize TaxId;
@synthesize Total;
@synthesize TypeHouse;
@synthesize Type_1;
@synthesize Type_2;
@synthesize UserName;
@synthesize TDName;
@synthesize TDAddress;
@synthesize TDLatLng;
@synthesize PotentialID;
@synthesize AddressPassport;
@synthesize Payment;
@synthesize Birthday;
@synthesize promotionComboID, promotionComboName, regType, contractServiceType, contractServiceTypeName, contractLocalType, contractPromotionID,contractComboStatus;
@synthesize OTTBoxCount,OTTTotal,imageSignature,imageInfo,office365Total;
@synthesize ListDevice;
@synthesize DeviceTotal;
@synthesize ListPlayBoxOTT;
@synthesize ListMACPlayBoxOTT;

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        self.Address = [StringFormat(@"%@",[dict objectForKey:@"Address"]) checkNull];
        //AddressOrigin Use AutoBookPort
        self.AddressOrigin = [StringFormat(@"%@",[dict objectForKey:@"AddressOrigin"])checkNull];
        /*-----------------------------*/
        self.BillTo_City = [StringFormat(@"%@",[dict objectForKey:@"BillTo_City"])checkNull];
        self.BillTo_District = [StringFormat(@"%@",[dict objectForKey:@"BillTo_District"]) checkNull];
        self.BillTo_Number = [StringFormat(@"%@",[dict objectForKey:@"BillTo_Number"]) checkNull];
        self.BillTo_Street = [StringFormat(@"%@",[dict objectForKey:@"BillTo_Street"]) checkNull];
        self.BillTo_Ward = [StringFormat(@"%@",[dict objectForKey:@"BillTo_Ward"]) checkNull];
        self.BookPortType = [StringFormat(@"%@",[dict objectForKey:@"BookPortType"]) checkNull];
        self.BranchCode = [StringFormat(@"%@",[dict objectForKey:@"BranchCode"]) checkNull];
        self.CableStatus = [StringFormat(@"%@",[dict objectForKey:@"CableStatus"]) checkNull];
        self.Contact = [StringFormat(@"%@",[dict objectForKey:@"Contact"]) checkNull];
        self.Contact_1 = [StringFormat(@"%@",[dict objectForKey:@"Contact_1"]) checkNull];
        self.Contact_2 = [StringFormat(@"%@",[dict objectForKey:@"Contact_2"]) checkNull];
        self.Contract = [StringFormat(@"%@",[dict objectForKey:@"Contract"]) checkNull];
        self.CreateBy = [StringFormat(@"%@",[dict objectForKey:@"CreateBy"]) checkNull];
        self.CreateDate = [StringFormat(@"%@",[dict objectForKey:@"CreateDate"]) checkNull];
        self.CurrentHouse = [StringFormat(@"%@",[dict objectForKey:@"CurrentHouse"]) checkNull];
        self.CusType = [StringFormat(@"%@",[dict objectForKey:@"CusType"]) checkNull];
        self.CusTypeDetail = [StringFormat(@"%@",[dict objectForKey:@"CusTypeDetail"]) checkNull];
        self.CustomerType = [StringFormat(@"%@",[dict objectForKey:@"CustomerType"]) checkNull];
        self.Deposit = [StringFormat(@"%@",[dict objectForKey:@"Deposit"]) checkNull];
        self.DepositBlackPoint = [StringFormat(@"%@",[dict objectForKey:@"DepositBlackPoint"]) checkNull];
        self.Description = [StringFormat(@"%@",[dict objectForKey:@"Description"]) checkNull];
        self.DescriptionIBB = [StringFormat(@"%@",[dict objectForKey:@"DescriptionIBB"]) checkNull];
        self.DivisionID = [StringFormat(@"%@",[dict objectForKey:@"DivisionID"]) checkNull];
        self.Email = [StringFormat(@"%@",[dict objectForKey:@"Email"]) checkNull];
        self.EoC = [StringFormat(@"%@",[dict objectForKey:@"EoC"]) checkNull];
        self.Floor = [StringFormat(@"%@",[dict objectForKey:@"Floor"]) checkNull];
        self.FullName = [StringFormat(@"%@",[dict objectForKey:@"FullName"]) checkNull];
        self.ID = [StringFormat(@"%@",[dict objectForKey:@"ID"]) checkNull];
        self.INSCable = [StringFormat(@"%@",[dict objectForKey:@"INSCable"]) checkNull];
        self.IPTVBoxCount = [StringFormat(@"%@",[dict objectForKey:@"IPTVBoxCount"]) checkNull];
        self.IPTVChargeTimes = [StringFormat(@"%@",[dict objectForKey:@"IPTVChargeTimes"]) checkNull];
        self.IPTVDeviceTotal = [StringFormat(@"%@",[dict objectForKey:@"IPTVDeviceTotal"]) checkNull];
        self.IPTVHBOChargeTimes = [StringFormat(@"%@",[dict objectForKey:@"IPTVHBOChargeTimes"]) checkNull];
        self.IPTVHBOPrepaidMonth = [StringFormat(@"%@",[dict objectForKey:@"IPTVHBOPrepaidMonth"]) checkNull];
        
        self.IPTVFimPlusPrepaidMonth = [StringFormat(@"%@",[dict objectForKey:@"IPTVFimPlusPrepaidMonth"]) checkNull];
        self.IPTVFimPlusChargeTimes = [StringFormat(@"%@",[dict objectForKey:@"IPTVFimPlusChargeTimes"]) checkNull];
        self.IPTVFimHotPrepaidMonth = [StringFormat(@"%@",[dict objectForKey:@"IPTVFimHotPrepaidMonth"]) checkNull];
        self.IPTVFimHotChargeTimes = [StringFormat(@"%@",[dict objectForKey:@"IPTVFimHotChargeTimes"]) checkNull];
        
        self.IPTVKPlusChargeTimes = [StringFormat(@"%@",[dict objectForKey:@"IPTVKPlusChargeTimes"]) checkNull];
        self.IPTVKPlusPrepaidMonth = [StringFormat(@"%@",[dict objectForKey:@"IPTVKPlusPrepaidMonth"]) checkNull];
        self.IPTVPLCCount = [StringFormat(@"%@",[dict objectForKey:@"IPTVPLCCount"]) checkNull];
        self.IPTVPackage = [StringFormat(@"%@",[dict objectForKey:@"IPTVPackage"]) checkNull];
        self.IPTVPrepaid = [StringFormat(@"%@",[dict objectForKey:@"IPTVPrepaid"]) checkNull];
        self.IPTVPrepaidTotal = [StringFormat(@"%@",[dict objectForKey:@"IPTVPrepaidTotal"]) checkNull];
        self.IPTVPromotionID = [StringFormat(@"%@",[dict objectForKey:@"IPTVPromotionID"]) checkNull];
        self.IPTVPromotionIDBoxOrder = [StringFormat(@"%@",[dict objectForKey:@"IPTVPromotionIDBoxOrder"]) checkNull];
        
        self.IPTVRequestDrillWall = [StringFormat(@"%@",[dict objectForKey:@"IPTVRequestDrillWall"]) checkNull];
        self.IPTVRequestSetUp = [StringFormat(@"%@",[dict objectForKey:@"IPTVRequestSetUp"]) checkNull];
        self.IPTVReturnSTBCount = [StringFormat(@"%@",[dict objectForKey:@"IPTVReturnSTBCount"]) checkNull];
        self.IPTVStatus = [StringFormat(@"%@",[dict objectForKey:@"IPTVStatus"]) checkNull];
        self.IPTVTotal = [StringFormat(@"%@",[dict objectForKey:@"IPTVTotal"]) checkNull];
        self.IPTVUseCombo = [StringFormat(@"%@",[dict objectForKey:@"IPTVUseCombo"]) checkNull];
        self.IPTVVTCChargeTimes = [StringFormat(@"%@",[dict objectForKey:@"IPTVVTCChargeTimes"]) checkNull];
        self.IPTVVTCPrepaidMonth = [StringFormat(@"%@",[dict objectForKey:@"IPTVVTCPrepaidMonth"]) checkNull];
        self.IPTVVTVChargeTimes = [StringFormat(@"%@",[dict objectForKey:@"IPTVVTVChargeTimes"]) checkNull];
        self.IPTVVTVPrepaidMonth = [StringFormat(@"%@",[dict objectForKey:@"IPTVVTVPrepaidMonth"]) checkNull];
        self.ISPType = [StringFormat(@"%@",[dict objectForKey:@"ISPType"]) checkNull];
        self.Image = [StringFormat(@"%@",[dict objectForKey:@"Image"]) checkNull];
        self.InDType = [StringFormat(@"%@",[dict objectForKey:@"InDType"]) checkNull];
        self.InDoor = [StringFormat(@"%@",[dict objectForKey:@"InDoor"]) checkNull];
        self.InternetTotal = [StringFormat(@"%@",[dict objectForKey:@"InternetTotal"]) checkNull];
        self.Latlng = [StringFormat(@"%@",[dict objectForKey:@"Latlng"]) checkNull];
        self.LegalEntity = [StringFormat(@"%@",[dict objectForKey:@"LegalEntity"]) checkNull];
        self.LocalType = [StringFormat(@"%@",[dict objectForKey:@"LocalType"]) checkNull];
        self.LocalTypeName = [StringFormat(@"%@",[dict objectForKey:@"LocalTypeName"]) checkNull];
        self.LocationID = [StringFormat(@"%@",[dict objectForKey:@"LocationID"]) checkNull];
        self.Lot = [StringFormat(@"%@",[dict objectForKey:@"Lot"]) checkNull];
        self.MapCode = [StringFormat(@"%@",[dict objectForKey:@"MapCode"]) checkNull];
        self.Modem = [StringFormat(@"%@",[dict objectForKey:@"Modem"]) checkNull];
        self.NameVilla = [StringFormat(@"%@",[dict objectForKey:@"NameVilla"]) checkNull];
        self.Note = [StringFormat(@"%@",[dict objectForKey:@"Note"]) checkNull];
        self.ODCCableType = [StringFormat(@"%@",[dict objectForKey:@"ODCCableType"]) checkNull];
        self.ObjID = [StringFormat(@"%@",[dict objectForKey:@"ObjID"]) checkNull];
        self.ObjectType = [StringFormat(@"%@",[dict objectForKey:@"ObjectType"]) checkNull];
        self.OutDType = [StringFormat(@"%@",[dict objectForKey:@"OutDType"]) checkNull];
        self.OutDoor = [StringFormat(@"%@",[dict objectForKey:@"OutDoor"]) checkNull];
        self.PartnerID = [StringFormat(@"%@",[dict objectForKey:@"PartnerID"]) checkNull];
        self.Passport = [StringFormat(@"%@",[dict objectForKey:@"Passport"]) checkNull];
        self.PaymentAbility = [StringFormat(@"%@",[dict objectForKey:@"PaymentAbility"]) checkNull];
        self.PhoneNumber = [StringFormat(@"%@",[dict objectForKey:@"PhoneNumber"]) checkNull];
        self.Phone_1 = [StringFormat(@"%@",[dict objectForKey:@"Phone_1"]) checkNull];
        self.Phone_2 = [StringFormat(@"%@",[dict objectForKey:@"Phone_2"]) checkNull];
        self.Position = [StringFormat(@"%@",[dict objectForKey:@"Position"]) checkNull];
        self.PromotionID = [StringFormat(@"%@",[dict objectForKey:@"PromotionID"]) checkNull];
        self.PromotionName = [StringFormat(@"%@",[dict objectForKey:@"PromotionName"]) checkNull];
        self.PotentialID = @"";
        self.RegCode = [StringFormat(@"%@",[dict objectForKey:@"RegCode"]) checkNull];
        self.Room = [StringFormat(@"%@",[dict objectForKey:@"Room"]) checkNull];
        self.StatusDeposit = [StringFormat(@"%@",[dict objectForKey:@"StatusDeposit"]) checkNull];
        self.Supporter = [StringFormat(@"%@",[dict objectForKey:@"Supporter"]) checkNull];
        self.TaxId = [StringFormat(@"%@",[dict objectForKey:@"TaxId"]) checkNull];
        self.Total = [StringFormat(@"%@",[dict objectForKey:@"Total"]) checkNull];
        self.TypeHouse = [StringFormat(@"%@",[dict objectForKey:@"TypeHouse"]) checkNull];
        self.Type_1 = [StringFormat(@"%@",[dict objectForKey:@"Type_1"]) checkNull];
        self.Type_2 = [StringFormat(@"%@",[dict objectForKey:@"Type_2"]) checkNull];
        self.UserName = [StringFormat(@"%@",[dict objectForKey:@"UserName"]) checkNull];
        self.TDName = [StringFormat(@"%@",[dict objectForKey:@"TDName"]) checkNull];
        self.TDAddress = [StringFormat(@"%@",[dict objectForKey:@"TDAddress"]) checkNull];
        self.TDLatLng = [StringFormat(@"%@",[dict objectForKey:@"TDLatLng"]) checkNull];
        self.Payment = [StringFormat(@"%@",[dict objectForKey:@"Payment"]) checkNull];
        self.AddressPassport = [StringFormat(@"%@",[dict objectForKey:@"AddressPassport"]) checkNull];
        self.Birthday = [StringFormat(@"%@",[dict objectForKey:@"Birthday"]) checkNull];
        self.PaymentName = [StringFormat(@"%@",[dict objectForKey:@"PaymentName"]) checkNull];
       
        
        self.promotionComboID = [StringFormat(@"%@",[dict objectForKey:@"PromotionComboID"]) checkNull];
        self.promotionComboName = [StringFormat(@"%@",[dict objectForKey:@"PromotionComboName"]) checkNull];
        self.regType = [StringFormat(@"%@",[dict objectForKey:@"RegType"]) checkNull];
        self.contractServiceType = [StringFormat(@"%@",[dict objectForKey:@"ContractServiceType"]) checkNull];
        self.contractServiceTypeName = [StringFormat(@"%@",[dict objectForKey:@"ContractServiceTypeName"]) checkNull];
        self.contractLocalType = [StringFormat(@"%@",[dict objectForKey:@"ContractLocalType"]) checkNull];
        self.contractPromotionID = [StringFormat(@"%@",[dict objectForKey:@"ContractPromotionID"]) checkNull];
        self.contractPromotionName = [StringFormat(@"%@",[dict objectForKey:@"ContractPromotionName"]) checkNull];
        self.contractComboStatus = [StringFormat(@"%@",[dict objectForKey:@"ContractComboStatus"]) checkNull];
        //IMAGE
        
        self.imageInfo = [StringFormat(@"%@",[dict objectForKey:@"ImageInfo"]) checkNull];
        self.imageSignature = [StringFormat(@"%@",[dict objectForKey:@"ImageSignature"]) checkNull];
        //OTT
        self.OTTBoxCount = [StringFormat(@"%@",[dict objectForKey:@"OTTBoxCount"]) checkNull];
        self.OTTTotal = [dict[@"OTTTotal"]integerValue];
        // Office 365.
        NSString *strOffice365 = StringFormat(@"%@",[dict objectForKey:@"Office365Total"]);
        
        //ArrayListDevice
        
        self.ListDevice = [dict objectForKey:@"ListDevice"];
        self.DeviceTotal = StringFormat(@"%@",[dict objectForKey:@"DeviceTotal"]);
        
        //Array FPT Play Box OTT
        NSDictionary *dictPlayBoxOTT = [dict objectForKey:@"ListDeviceMACOTT"];
        self.ListPlayBoxOTT = [dictPlayBoxOTT objectForKey:@"listDeviceOTT"];
        self.ListMACPlayBoxOTT = [dictPlayBoxOTT objectForKey:@"listMACOTT"];
//        if ( [dictPlayBoxOTT count] > 0 ) {
//            self.ListPlayBoxOTT = [dictPlayBoxOTT objectForKey:@"listDeviceOTT"];
//            self.ListMACPlayBoxOTT = [dictPlayBoxOTT objectForKey:@"listMACOTT"];
//        }
        
//        if (arrDictMACOTT.count > 0) {
//            
//        }
        
//        self.ListPlayBoxOTT = [dict objectForKey:@"ListDeviceMACOTT"];

    
        if ([strOffice365  isEqualToString: @"<null>"]) {
            self.office365Total = 0;
        } else {
            self.office365Total = [dict[@"Office365Total"] integerValue];
        }
        
        @try {
            //vutt11
            self.office365Model = [[Office365Model sharedManager] initWithDictionary:dict];

        } @catch (NSException *exception) {
            NSLog(@"Error: Parse data Office 365");
              [CrashlyticsKit recordCustomExceptionName:@"RegistrationFormDetailRecord - funtion (saveReceiveRemoteNotification)-Error handle Parse data Office 365" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
        }

    }
    return self;
}

@end
