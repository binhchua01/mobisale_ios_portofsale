//
//  RegistrationFormRecord.h
//  MobiSale
//
//  Created by HIEUPC on 1/27/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RegistrationFormRecord : NSObject

@property (nonatomic,retain) NSString *Address;
@property (nonatomic,retain) NSString *Contact;
@property (nonatomic,retain) NSString *CurrentPage;
@property (nonatomic,retain) NSString *FullName;
@property (nonatomic,retain) NSString *ID;
@property (nonatomic,retain) NSString *LocalType;
@property (nonatomic,retain) NSString *RegCode;
@property (nonatomic,retain) NSString *RowNumber;
@property (nonatomic,retain) NSString *TotalPage;
@property (nonatomic,retain) NSString *TotalRow;
@property (nonatomic,retain) NSString *isBilling;
@property (nonatomic,retain) NSString *isBookport;
@property (nonatomic,retain) NSString *isInVest;
@property (nonatomic,retain) NSString *StatusDeposit;
@property (nonatomic,retain) NSString *AutoContractStatus;
@property (nonatomic,retain) NSString *AutoContractStatusDesc;
@property (nonatomic,retain) NSString *Contract;
@property (nonatomic,retain) NSString *strStatusBookPort;
//PaidStatusName
@property (nonatomic,retain) NSString *strPaidStatusName;
@end
