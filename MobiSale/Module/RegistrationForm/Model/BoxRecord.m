//
//  BoxRecord.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 2/2/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BoxRecord.h"

@implementation BoxRecord

@synthesize BoxType;
@synthesize Count;
@synthesize ID;
@synthesize Package;
@synthesize PromotionID;
@synthesize RegID;

@end
