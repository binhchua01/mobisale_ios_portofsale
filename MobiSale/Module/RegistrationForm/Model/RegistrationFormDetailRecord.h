//
//  RegistrationFormDetailRecord.h
//  MobiSale
//
//  Created by HIEUPC on 1/27/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Office365Model.h"

@interface RegistrationFormDetailRecord : NSObject

@property (nonatomic,retain) NSString *Address;
@property (nonatomic,retain) NSString *AddressOrigin;
@property (nonatomic,retain) NSString *AddressPassport;
@property (nonatomic,retain) NSString *BillTo_City;
@property (nonatomic,retain) NSString *BillTo_District;
@property (nonatomic,retain) NSString *BillTo_Number;
@property (nonatomic,retain) NSString *BillTo_Street;
@property (nonatomic,retain) NSString *BillTo_Ward;
@property (nonatomic,retain) NSString *Birthday;
@property (nonatomic,retain) NSString *BookPortType;
@property (nonatomic,retain) NSString *BranchCode;
@property (nonatomic,retain) NSString *CableStatus;
@property (nonatomic,retain) NSString *Contact;
@property (nonatomic,retain) NSString *Contact_1;
@property (nonatomic,retain) NSString *Contact_2;
@property (nonatomic,retain) NSString *Contract;
@property (nonatomic,retain) NSString *CreateBy;
@property (nonatomic,retain) NSString *CreateDate;
@property (nonatomic,retain) NSString *CurrentHouse;
@property (nonatomic,retain) NSString *CusType;
@property (nonatomic,retain) NSString *CusTypeDetail;
@property (nonatomic,retain) NSString *CustomerType;
@property (nonatomic,retain) NSString *Deposit;
@property (nonatomic,retain) NSString *DepositBlackPoint;
@property (nonatomic,retain) NSString *Description;
@property (nonatomic,retain) NSString *DescriptionIBB;
@property (nonatomic,retain) NSString *DivisionID;
@property (nonatomic,retain) NSString *Email;
@property (nonatomic,retain) NSString *EoC;
@property (nonatomic,retain) NSString *Floor;
@property (nonatomic,retain) NSString *FullName;
@property (nonatomic,retain) NSString *ID;
@property (nonatomic,retain) NSString *INSCable;
@property (nonatomic,retain) NSString *IPTVBoxCount;
@property (nonatomic,retain) NSString *IPTVChargeTimes;
@property (nonatomic,retain) NSString *IPTVDeviceTotal;
@property (nonatomic,retain) NSString *IPTVHBOChargeTimes;
@property (nonatomic,retain) NSString *IPTVHBOPrepaidMonth;
@property (nonatomic,retain) NSString *IPTVFimPlusChargeTimes;  //Số lần tính cước Fim+
@property (nonatomic,retain) NSString *IPTVFimPlusPrepaidMonth; //Số tháng trả trước Fim+
@property (nonatomic,retain) NSString *IPTVFimHotChargeTimes;   //Số lần tính cước Fim Hot
@property (nonatomic,retain) NSString *IPTVFimHotPrepaidMonth;  //Số tháng trả trước Fim Hot
@property (nonatomic,retain) NSString *IPTVKPlusChargeTimes;
@property (nonatomic,retain) NSString *IPTVKPlusPrepaidMonth;
@property (nonatomic,retain) NSString *IPTVPLCCount;
@property (nonatomic,retain) NSString *IPTVPackage;
@property (nonatomic,retain) NSString *IPTVPrepaid;
@property (nonatomic,retain) NSString *IPTVPrepaidTotal;
@property (nonatomic,retain) NSString *IPTVPromotionID;
@property (nonatomic,retain) NSString *IPTVPromotionIDBoxOrder; // ID CLKM IPTV của Box thứ 2 trở đi
@property (nonatomic,retain) NSString *IPTVRequestDrillWall;
@property (nonatomic,retain) NSString *IPTVRequestSetUp;
@property (nonatomic,retain) NSString *IPTVReturnSTBCount;
@property (nonatomic,retain) NSString *IPTVStatus;
@property (nonatomic,retain) NSString *IPTVTotal;
@property (nonatomic,retain) NSString *IPTVUseCombo;
@property (nonatomic,retain) NSString *IPTVVTCChargeTimes;
@property (nonatomic,retain) NSString *IPTVVTCPrepaidMonth;
@property (nonatomic,retain) NSString *IPTVVTVChargeTimes;
@property (nonatomic,retain) NSString *IPTVVTVPrepaidMonth;
@property (nonatomic,retain) NSString *ISPType;
@property (nonatomic,retain) NSString *Image;
@property (nonatomic,retain) NSString *InDType;
@property (nonatomic,retain) NSString *InDoor;
@property (nonatomic,retain) NSString *InternetTotal;
@property (nonatomic,retain) NSString *Latlng;
@property (nonatomic,retain) NSString *LegalEntity;
@property (nonatomic,retain) NSString *LocalType;
@property (nonatomic,retain) NSString *LocalTypeName;
@property (nonatomic,retain) NSString *LocationID;
@property (nonatomic,retain) NSString *Lot;
@property (nonatomic,retain) NSString *MapCode;
@property (nonatomic,retain) NSString *Modem;
@property (nonatomic,retain) NSString *NameVilla;
@property (nonatomic,retain) NSString *Note;
@property (nonatomic,retain) NSString *ODCCableType;
@property (nonatomic,retain) NSString *ObjID;
@property (nonatomic,retain) NSString *ObjectType;
@property (nonatomic,retain) NSString *OutDType;
@property (nonatomic,retain) NSString *OutDoor;
@property (nonatomic,retain) NSString *PartnerID;
@property (nonatomic,retain) NSString *Passport;
@property (nonatomic,retain) NSString *Payment;
@property (nonatomic,retain) NSString *PaymentAbility;
@property (nonatomic,retain) NSString *PaymentName;
@property (nonatomic,retain) NSString *PhoneNumber;
@property (nonatomic,retain) NSString *Phone_1;
@property (nonatomic,retain) NSString *Phone_2;
@property (nonatomic,retain) NSString *Position;
@property (nonatomic,retain) NSString *PromotionID;
@property (nonatomic,retain) NSString *PromotionName;
@property (nonatomic,retain) NSString *RegCode;
@property (nonatomic,retain) NSString *Room;
@property (nonatomic,retain) NSString *StatusDeposit;
@property (nonatomic,retain) NSString *Supporter;
@property (nonatomic,retain) NSString *TaxId;
@property (nonatomic,retain) NSString *Total;
@property (nonatomic,retain) NSString *TypeHouse;
@property (nonatomic,retain) NSString *Type_1;
@property (nonatomic,retain) NSString *Type_2;
@property (nonatomic,retain) NSString *UserName;
@property (nonatomic,retain) NSString *TDName;
@property (nonatomic,retain) NSString *TDAddress;
@property (nonatomic,retain) NSString *TDLatLng;
@property (nonatomic,retain) NSString *PotentialID;
// Thông tin PĐK bán thêm (Bán thêm dịch vụ)
// ID CLKM combo
@property (nonatomic, strong) NSString *promotionComboID;
// Tên CLKM combo
@property (nonatomic, strong) NSString *promotionComboName;
// Loại PĐK (0: PĐK bán khách hàng mới, < >0: PĐK bán thêm dịch vụ cho khách hàng cũ)
@property (nonatomic, strong) NSString *regType;

// Loại dịch vụ hợp đồng đã đăng ký (0: internet only, 1: IPTV only, 2: internet+IPTV)
@property (nonatomic, strong) NSString *contractServiceType;
@property (nonatomic, strong) NSString *contractServiceTypeName;
// ID Gói dịch vụ internet đã đăng ký (F2, F3,...) của hợp đồng
@property (nonatomic, strong) NSString *contractLocalType;
// ID CLKM Internet của hợp đồng
@property (nonatomic, strong) NSString *contractPromotionID;
// Tên CLKM Internet của hợp đồng
@property (nonatomic, strong) NSString *contractPromotionName;
// Tình trạng đã chọn CLKM combo Internet (0: chưa chọn, <>0: đã chọn) của hợp đồng
@property (nonatomic, strong) NSString *contractComboStatus;
// Office 365
@property (strong, nonatomic) NSString *listPackage; // Danh sách gói Office 365.
@property (assign, nonatomic) NSInteger office365Total; // Tổng tiền Office 365.
@property (strong, nonatomic) Office365Model *office365Model;
//FPT PlayBox
@property (strong, nonatomic) NSString *OTTBoxCount;
@property (assign ,nonatomic) NSInteger OTTTotal;
//Image
@property (strong, nonatomic) NSString *imageInfo;
@property (strong, nonatomic) NSString *imageSignature;
//ArrayListDevice
@property (strong, nonatomic) NSMutableArray *ListDevice;

//ArrayListFPTPlayBoxOTT
@property (strong, nonatomic) NSMutableArray *ListPlayBoxOTT;

//ArrayListMACFPTOTT
@property (strong, nonatomic) NSMutableArray *ListMACPlayBoxOTT;

//Tong tien Device
@property (strong ,nonatomic) NSString *DeviceTotal;

- (instancetype)initWithDictionary:(NSDictionary *)dict;


@end
