//
//  RegistrationFormRecord.m
//  MobiSale
//
//  Created by HIEUPC on 1/27/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "RegistrationFormRecord.h"

@implementation RegistrationFormRecord

@synthesize Address;
@synthesize Contact;
@synthesize CurrentPage;
@synthesize FullName;
@synthesize ID;
@synthesize LocalType;
@synthesize RegCode;
@synthesize RowNumber;
@synthesize TotalPage;
@synthesize TotalRow;
@synthesize isBilling;
@synthesize isBookport;
@synthesize isInVest;
@synthesize StatusDeposit;
@synthesize AutoContractStatus;
@synthesize AutoContractStatusDesc;
@synthesize Contract;
@synthesize strStatusBookPort;

@synthesize strPaidStatusName;
@end
