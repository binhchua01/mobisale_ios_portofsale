//
//  IPTVPrice.h
//  MobiSale
//
//  Created by ISC-DanTT on 5/25/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IPTVPrice : NSObject

@property (strong, nonatomic) NSString *DacSacHDPrice;
@property (strong, nonatomic) NSString *FimHotPrice;
@property (strong, nonatomic) NSString *FimPlusPrice;
@property (strong, nonatomic) NSString *HDBoxPrice;
@property (strong, nonatomic) NSString *KPlusPrice;
@property (strong, nonatomic) NSString *PLCPrice;
@property (strong, nonatomic) NSString *STBPrice;
@property (strong, nonatomic) NSString *VTCHDPrice;   // cước VTC HD 
@property (strong, nonatomic) NSString *VTVCabPrice;  // cước VTV Cab

@end








