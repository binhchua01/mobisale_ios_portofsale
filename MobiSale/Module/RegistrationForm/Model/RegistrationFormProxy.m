    //
//  RegistrationFormProxy.m
//  MobiSale
//
//  Created by HIEUPC on 1/27/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "RegistrationFormProxy.h"
#import "ShareData.h"
#import "RegistrationFormRecord.h"
#import "RegistrationFormDetailRecord.h"
#import "BoxRecord.h"
#import "KeyValueModel.h"
#import "IPTVPrice.h"
#import "ConstructionVoteDetailModel.h"
#import "FPTUtility.h"

#define mGetListRegistration                @"GetListRegistration"
#define mGetRegistrationDetail              @"GetRegistrationDetail"
#define mUpdateRegistration                 @"UpdateRegistration"
#define mGetIPTVTotal                       @"GetIPTVTotal"
#define mUpdateDeposit                      @"UpdateDeposit"
#define mCreateObject                       @"CreateObject"
#define mGetListRegistrationPost            @"GetListRegistrationPost"
#define mCheckAddressNumber                 @"CheckAddressNumber"
#define mGetContractDepositList             @"GetContractDepositList"
#define mUpdateReceiptInternet              @"UpdateReceiptInternet"
#define mGetPaymentType                     @"GetPaymentType"
#define mWriteLogMPos                       @"WriteLogMPos"
#define mUpdateDepositmPOS                  @"UpdateDepositmPOS"
#define mCheckForDeposit                    @"CheckForDeposit"
#define mGetIPTVPrice                       @"GetIPTVPrice"
#define mGetPTCDetail                       @"GetPTCDetail"
#define mChangePTCStatusForNotApproval      @"ChangePTCStatusForNotApproval"
#define mGetPackage365                      @"GetPackage365"
#define mGetTotalOffice365                  @"GetTotalOffice365"
#define mGetTotalOTT                        @"GetTotalOTT"
#define mGetUploadImageInvest               @"UploadImageInvest"
@implementation RegistrationFormProxy

#pragma mark - Get List RegistrationForm
- (void)GetListRegistration:(NSString*)PageNum Completehander:(DidGetResultBlock)handler
               errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    
    //NSString *deviceIMEI = [OpenUDID value];358239055770376
    NSString *username = [ShareData instance].currentUser.userName;
    NSString *pagenum = PageNum;
    
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@/%@/%@",urlAPI, mGetListRegistration,username,pagenum)];
    //NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetListRegistration)]; // server test for POST
    
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    // server test, mới sử dụng POST
    /*
     NSString *postString = @"";
     NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
     
     [dict setObject:pagenum forKey:@"PageNumber"];
     [dict setObject:username forKey:@"UserName"];
     
     postString = [dict JSONRepresentation];
     
     [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
     [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
     */
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    //[(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"GET"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetListRegistration:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
    
}
- (void)endGetListRegistration:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSArray *arr = [jsonDict objectForKey:@"GetListRegistrationResult"]?:nil;
    
    if(arr.count <= 0){
        handler(nil, @"", @"Danh sách trống");
        return;
    }
    
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    if(![ErrorService isEqualToString:@"<null>"]){
        //NSString *message = StringFormat(@"%@",[root objectForKey:@"Message"]); ;
        handler(nil, ErrorService, @"Có lỗi, vui lòng thử lại!");
        return;
    }
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        RegistrationFormRecord *p = [self ParseRecordList:d];
        [mArray addObject:p];
    }
    handler(mArray, ErrorService, @"");
    
    
}

#pragma mark - Get List RegistrationForm AutoContract
- (void)GetListRegistrationPost:(NSString*)userName pageNumber:(NSString*)pageNumber agent:(NSString*)agent agentName:(NSString*)agentName Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetListRegistrationPost)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:userName    forKey:@"UserName"];
    [dict setObject:pageNumber  forKey:@"PageNumber"];
    [dict setObject:agent       forKey:@"Agent"];
    [dict setObject:agentName   forKey:@"AgentName"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetListRegistrationPost:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
    
}

- (void)endGetListRegistrationPost:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSArray *arr = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(arr.count <= 0){
        handler(nil, @"", @"Danh sách trống");
        return;
    }
    
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    if(![ErrorService isEqualToString:@"<null>"]){
        handler(nil, ErrorService, @"Có lỗi, vui lòng thử lại!");
        return;
    }
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        RegistrationFormRecord *p = [self ParseRecordList:d];
        [mArray addObject:p];
    }
    handler(mArray, ErrorService, @"");
    
}

-(RegistrationFormRecord *) ParseRecordList:(NSDictionary *)dict{
    RegistrationFormRecord *rc = [[RegistrationFormRecord alloc]init];
    
    rc.Address          = StringFormat(@"%@",[dict objectForKey:@"Address"]);
    rc.Contact          = StringFormat(@"%@",[dict objectForKey:@"Contact"]);
    rc.CurrentPage      = StringFormat(@"%@",[dict objectForKey:@"CurrentPage"]);
    rc.FullName         = StringFormat(@"%@",[dict objectForKey:@"FullName"]);
    rc.ID               = StringFormat(@"%@",[dict objectForKey:@"ID"]);
    rc.LocalType        = StringFormat(@"%@",[dict objectForKey:@"LocalType"]);
    rc.RegCode          = StringFormat(@"%@",[dict objectForKey:@"RegCode"]);
    rc.RowNumber        = StringFormat(@"%@",[dict objectForKey:@"RowNumber"]);
    rc.TotalPage        = StringFormat(@"%@",[dict objectForKey:@"TotalPage"]);
    rc.TotalRow         = StringFormat(@"%@",[dict objectForKey:@"TotalRow"]);
    rc.isBilling        = StringFormat(@"%@",[dict objectForKey:@"isBilling"]);
    rc.isBookport       = StringFormat(@"%@",[dict objectForKey:@"isBookport"]);
    rc.isInVest         = StringFormat(@"%@",[dict objectForKey:@"isInVest"]);
    rc.StatusDeposit    = StringFormat(@"%@",[dict objectForKey:@"StatusDeposit"]);
    rc.Contract         = StringFormat(@"%@",[dict objectForKey:@"Contract"]);
    rc.AutoContractStatus      = StringFormat(@"%@",[dict objectForKey:@"AutoContractStatus"]);
    rc.AutoContractStatusDesc  = StringFormat(@"%@",[dict objectForKey:@"AutoContractStatusDesc"]);
    rc.strStatusBookPort = StringFormat(@"%@",[dict objectForKey:@"strStatusBookPort"]);
    rc.strPaidStatusName = StringFormat(@"%@",[dict objectForKey:@"PaidStatusName"]);

    return rc;
}
//vutt11
#pragma mark - Get List Detail
- (void)GetDetailRegistration:(NSString*)Id Completehander:(DidGetResultBlock)handler
                 errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetRegistrationDetail)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //NSString *deviceIMEI = [OpenUDID value];358239055770376
    NSString *username = [ShareData instance].currentUser.userName;
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:Id forKey:@"ID"];
    [dict setObject:username forKey:@"UserName"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetDetailRegistration:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}

- (void)endGetDetailRegistration:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    
    NSDictionary *root = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(root == nil) return;
    
    NSString *ErrorCode = StringFormat(@"%@",[root objectForKey:@"ErrorCode"]?:nil);
    NSString *message;
    
    if(![ErrorCode isEqualToString:@"0"]){
        message = StringFormat(@"%@",[root objectForKey:@"Error"]);
        handler(nil, ErrorCode, message);
        return;
    }
    
    NSMutableArray *mArray = [NSMutableArray array];
    NSArray *arr = [root objectForKey:@"ListObject"]?:nil;
    
    if(arr.count <= 0){
        handler(nil, ErrorCode, @"");
        return;
    }
    for(NSDictionary *d in arr) {
        RegistrationFormDetailRecord *p = [[RegistrationFormDetailRecord alloc] initWithDictionary:d];

        [mArray addObject:p];
    }
    handler(mArray, ErrorCode, message);
    
}

-(RegistrationFormDetailRecord *) ParseRecordDetail:(NSDictionary *)dict{
    RegistrationFormDetailRecord *rc = [[RegistrationFormDetailRecord alloc]init];
    
    rc.Address = StringFormat(@"%@",[dict objectForKey:@"Address"]);
    rc.BillTo_City = StringFormat(@"%@",[dict objectForKey:@"BillTo_City"]);
    rc.BillTo_District = StringFormat(@"%@",[dict objectForKey:@"BillTo_District"]);
    rc.BillTo_Number = StringFormat(@"%@",[dict objectForKey:@"BillTo_Number"]);
    rc.BillTo_Street = StringFormat(@"%@",[dict objectForKey:@"BillTo_Street"]);
    rc.BillTo_Ward = StringFormat(@"%@",[dict objectForKey:@"BillTo_Ward"]);
    rc.BookPortType = StringFormat(@"%@",[dict objectForKey:@"BookPortType"]);
    rc.BranchCode = StringFormat(@"%@",[dict objectForKey:@"BranchCode"]);
    rc.CableStatus = StringFormat(@"%@",[dict objectForKey:@"CableStatus"]);
    rc.Contact = StringFormat(@"%@",[dict objectForKey:@"Contact"]);
    rc.Contact_1 = StringFormat(@"%@",[dict objectForKey:@"Contact_1"]);
    rc.Contact_2 = StringFormat(@"%@",[dict objectForKey:@"Contact_2"]);
    rc.Contract = StringFormat(@"%@",[dict objectForKey:@"Contract"]);
    rc.CreateBy = StringFormat(@"%@",[dict objectForKey:@"CreateBy"]);
    rc.CreateDate = StringFormat(@"%@",[dict objectForKey:@"CreateDate"]);
    rc.CurrentHouse = StringFormat(@"%@",[dict objectForKey:@"CurrentHouse"]);
    rc.CusType = StringFormat(@"%@",[dict objectForKey:@"CusType"]);
    rc.CusTypeDetail = StringFormat(@"%@",[dict objectForKey:@"CusTypeDetail"]);
    rc.CustomerType = StringFormat(@"%@",[dict objectForKey:@"CustomerType"]);
    rc.Deposit = StringFormat(@"%@",[dict objectForKey:@"Deposit"]);
    rc.DepositBlackPoint = StringFormat(@"%@",[dict objectForKey:@"DepositBlackPoint"]);
    rc.Description = StringFormat(@"%@",[dict objectForKey:@"Description"]);
    rc.DescriptionIBB = StringFormat(@"%@",[dict objectForKey:@"DescriptionIBB"]);
    rc.DivisionID = StringFormat(@"%@",[dict objectForKey:@"DivisionID"]);
    rc.Email = StringFormat(@"%@",[dict objectForKey:@"Email"]);
    rc.EoC = StringFormat(@"%@",[dict objectForKey:@"EoC"]);
    rc.Floor = StringFormat(@"%@",[dict objectForKey:@"Floor"]);
    rc.FullName = StringFormat(@"%@",[dict objectForKey:@"FullName"]);
    rc.ID = StringFormat(@"%@",[dict objectForKey:@"ID"]);
    rc.INSCable = StringFormat(@"%@",[dict objectForKey:@"INSCable"]);
    rc.IPTVBoxCount = StringFormat(@"%@",[dict objectForKey:@"IPTVBoxCount"]);
    rc.IPTVChargeTimes = StringFormat(@"%@",[dict objectForKey:@"IPTVChargeTimes"]);
    rc.IPTVDeviceTotal = StringFormat(@"%@",[dict objectForKey:@"IPTVDeviceTotal"]);
    rc.IPTVHBOChargeTimes = StringFormat(@"%@",[dict objectForKey:@"IPTVHBOChargeTimes"]);
    rc.IPTVHBOPrepaidMonth = StringFormat(@"%@",[dict objectForKey:@"IPTVHBOPrepaidMonth"]);
    
    rc.IPTVFimPlusPrepaidMonth = StringFormat(@"%@",[dict objectForKey:@"IPTVFimPlusPrepaidMonth"]);
    rc.IPTVFimPlusChargeTimes = StringFormat(@"%@",[dict objectForKey:@"IPTVFimPlusChargeTimes"]);
    rc.IPTVFimHotPrepaidMonth = StringFormat(@"%@",[dict objectForKey:@"IPTVFimHotPrepaidMonth"]);
    rc.IPTVFimHotChargeTimes = StringFormat(@"%@",[dict objectForKey:@"IPTVFimHotChargeTimes"]);
    
    rc.IPTVKPlusChargeTimes = StringFormat(@"%@",[dict objectForKey:@"IPTVKPlusChargeTimes"]);
    rc.IPTVKPlusPrepaidMonth = StringFormat(@"%@",[dict objectForKey:@"IPTVKPlusPrepaidMonth"]);
    rc.IPTVPLCCount = StringFormat(@"%@",[dict objectForKey:@"IPTVPLCCount"]);
    rc.IPTVPackage = StringFormat(@"%@",[dict objectForKey:@"IPTVPackage"]);
    rc.IPTVPrepaid = StringFormat(@"%@",[dict objectForKey:@"IPTVPrepaid"]);
    rc.IPTVPrepaidTotal = StringFormat(@"%@",[dict objectForKey:@"IPTVPrepaidTotal"]);
    rc.IPTVPromotionID = StringFormat(@"%@",[dict objectForKey:@"IPTVPromotionID"]);
    rc.IPTVPromotionIDBoxOrder = StringFormat(@"%@",[dict objectForKey:@"IPTVPromotionIDBoxOrder"]);

    rc.IPTVRequestDrillWall = StringFormat(@"%@",[dict objectForKey:@"IPTVRequestDrillWall"]);
    rc.IPTVRequestSetUp = StringFormat(@"%@",[dict objectForKey:@"IPTVRequestSetUp"]);
    rc.IPTVReturnSTBCount = StringFormat(@"%@",[dict objectForKey:@"IPTVReturnSTBCount"]);
    rc.IPTVStatus = StringFormat(@"%@",[dict objectForKey:@"IPTVStatus"]);
    rc.IPTVTotal = StringFormat(@"%@",[dict objectForKey:@"IPTVTotal"]);
    rc.IPTVUseCombo = StringFormat(@"%@",[dict objectForKey:@"IPTVUseCombo"]);
    rc.IPTVVTCChargeTimes = StringFormat(@"%@",[dict objectForKey:@"IPTVVTCChargeTimes"]);
    rc.IPTVVTCPrepaidMonth = StringFormat(@"%@",[dict objectForKey:@"IPTVVTCPrepaidMonth"]);
    rc.IPTVVTVChargeTimes = StringFormat(@"%@",[dict objectForKey:@"IPTVVTVChargeTimes"]);
    rc.IPTVVTVPrepaidMonth = StringFormat(@"%@",[dict objectForKey:@"IPTVVTVPrepaidMonth"]);
    rc.ISPType = StringFormat(@"%@",[dict objectForKey:@"ISPType"]);
    rc.Image = StringFormat(@"%@",[dict objectForKey:@"Image"]);
    rc.InDType = StringFormat(@"%@",[dict objectForKey:@"InDType"]);
    rc.InDoor = StringFormat(@"%@",[dict objectForKey:@"InDoor"]);
    rc.InternetTotal = StringFormat(@"%@",[dict objectForKey:@"InternetTotal"]);
    rc.Latlng = StringFormat(@"%@",[dict objectForKey:@"Latlng"]);
    rc.LegalEntity = StringFormat(@"%@",[dict objectForKey:@"LegalEntity"]);
    rc.LocalType = StringFormat(@"%@",[dict objectForKey:@"LocalType"]);
    rc.LocalTypeName = StringFormat(@"%@",[dict objectForKey:@"LocalTypeName"]);
    rc.LocationID = StringFormat(@"%@",[dict objectForKey:@"LocationID"]);
    rc.Lot = StringFormat(@"%@",[dict objectForKey:@"Lot"]);
    rc.MapCode = StringFormat(@"%@",[dict objectForKey:@"MapCode"]);
    rc.Modem = StringFormat(@"%@",[dict objectForKey:@"Modem"]);
    rc.NameVilla = StringFormat(@"%@",[dict objectForKey:@"NameVilla"]);
    rc.Note = StringFormat(@"%@",[dict objectForKey:@"Note"]);
    rc.ODCCableType = StringFormat(@"%@",[dict objectForKey:@"ODCCableType"]);
    rc.ObjID = StringFormat(@"%@",[dict objectForKey:@"ObjID"]);
    rc.ObjectType = StringFormat(@"%@",[dict objectForKey:@"ObjectType"]);
    rc.OutDType = StringFormat(@"%@",[dict objectForKey:@"OutDType"]);
    rc.OutDoor = StringFormat(@"%@",[dict objectForKey:@"OutDoor"]);
    rc.PartnerID = StringFormat(@"%@",[dict objectForKey:@"PartnerID"]);
    rc.Passport = StringFormat(@"%@",[dict objectForKey:@"Passport"]);
    rc.PaymentAbility = StringFormat(@"%@",[dict objectForKey:@"PaymentAbility"]);
    rc.PhoneNumber = StringFormat(@"%@",[dict objectForKey:@"PhoneNumber"]);
    rc.Phone_1 = StringFormat(@"%@",[dict objectForKey:@"Phone_1"]);
    rc.Phone_2 = StringFormat(@"%@",[dict objectForKey:@"Phone_2"]);
    rc.Position = StringFormat(@"%@",[dict objectForKey:@"Position"]);
    rc.PromotionID = StringFormat(@"%@",[dict objectForKey:@"PromotionID"]);
    rc.PromotionName = StringFormat(@"%@",[dict objectForKey:@"PromotionName"]);
    rc.PotentialID = @"";
    rc.RegCode = StringFormat(@"%@",[dict objectForKey:@"RegCode"]);
    rc.Room = StringFormat(@"%@",[dict objectForKey:@"Room"]);
    rc.StatusDeposit = StringFormat(@"%@",[dict objectForKey:@"StatusDeposit"]);
    rc.Supporter = StringFormat(@"%@",[dict objectForKey:@"Supporter"]);
    rc.TaxId = StringFormat(@"%@",[dict objectForKey:@"TaxId"]);
    rc.Total = StringFormat(@"%@",[dict objectForKey:@"Total"]);
    rc.TypeHouse = StringFormat(@"%@",[dict objectForKey:@"TypeHouse"]);
    rc.Type_1 = StringFormat(@"%@",[dict objectForKey:@"Type_1"]);
    rc.Type_2 = StringFormat(@"%@",[dict objectForKey:@"Type_2"]);
    rc.UserName = StringFormat(@"%@",[dict objectForKey:@"UserName"]);
    rc.TDName = StringFormat(@"%@",[dict objectForKey:@"TDName"]);
    rc.TDAddress = StringFormat(@"%@",[dict objectForKey:@"TDAddress"]);
    rc.TDLatLng = StringFormat(@"%@",[dict objectForKey:@"TDLatLng"]);
    rc.Payment = StringFormat(@"%@",[dict objectForKey:@"Payment"]);
    rc.AddressPassport = StringFormat(@"%@",[dict objectForKey:@"AddressPassport"]);
    rc.Birthday = StringFormat(@"%@",[dict objectForKey:@"Birthday"]);
    rc.PaymentName = StringFormat(@"%@",[dict objectForKey:@"PaymentName"]);

    rc.promotionComboID = StringFormat(@"%@",[dict objectForKey:@"PromotionComboID"]);
    rc.promotionComboName = StringFormat(@"%@",[dict objectForKey:@"PromotionComboName"]);
    rc.regType = StringFormat(@"%@",[dict objectForKey:@"RegType"]);
    rc.contractServiceType = StringFormat(@"%@",[dict objectForKey:@"ContractServiceType"]);
    rc.contractServiceTypeName = StringFormat(@"%@",[dict objectForKey:@"ContractServiceTypeName"]);
    rc.contractLocalType = StringFormat(@"%@",[dict objectForKey:@"ContractLocalType"]);
    rc.contractPromotionID = StringFormat(@"%@",[dict objectForKey:@"ContractPromotionID"]);
    rc.contractPromotionName = StringFormat(@"%@",[dict objectForKey:@"ContractPromotionName"]);
    rc.contractComboStatus = StringFormat(@"%@",[dict objectForKey:@"ContractComboStatus"]);
// OTT
    rc.OTTBoxCount = StringFormat(@"%@",[dict objectForKey:@"OTTBoxCount"]);
//IMAGE
    rc.imageInfo   = StringFormat(@"%@",[dict objectForKey:@"ImageInfo"]);
    rc.imageSignature = StringFormat(@"%@",[dict objectForKey:@"ImageSignature"]);
    rc.ListDevice = [dict objectForKey:@"ListDevice"];
    // get total Device
    rc.DeviceTotal = StringFormat(@"%@",[dict objectForKey:@"DeviceTotal"]);
    
    return rc;
}
//vutt11
#pragma mark - Create Registered form
- (void)UpdateRegistration:(NSString *)Id ObjID:(NSString *)objid Contract:(NSString *)contract LocationID:(NSString *)locationid LocalType:(NSString *)localtype FullName:(NSString *)fullname Contact:(NSString *)contact BillTo_City:(NSString *)billto_city     BillTo_District:(NSString *)billto_district BillTo_Ward:(NSString *)billto_ward TypeHouse:(NSString *)typehouse BillTo_Street:(NSString *)billto_street Lot:(NSString *)lot Floor:(NSString *)floor Room:(NSString *)room NameVilla:(NSString *)namevilla Position:(NSString *)position BillTo_Number:(NSString *)billto_number Address:(NSString *)address Note:(NSString *)note Passport:(NSString *)passport TaxId:(NSString *)taxid Phone_1:(NSString *)phone_1 Phone_2:(NSString *)phone_2 Type_1:(NSString *)type_1 Type_2:(NSString *)type_2 Contact_1:(NSString *)contact_1 Contact_2:(NSString *)contact_2 ISPType:(NSString *)isptype CusTypeDetail:(NSString *)custypedetail CurrentHouse:(NSString *)currenthouse PaymentAbility:(NSString *)paymentability PartnerID:(NSString *)partnerid LegalEntity:(NSString *)legalentity Supporter:(NSString *)supporter PromotionID:(NSString *)promotionid Total:(NSString *)total UserName:(NSString *)username Deposit:(NSString *)deposit DepositBlackPoint:(NSString *)depositblackpoint ObjectType:(NSString *)objecttype CableStatus:(NSString *)cablestatus InsCable:(NSString *)inscable CusType:(NSString *)custype IPTVUseCombo:(NSString *)iptvusecombo IPTVRequestSetUp:(NSString *)iptvrequestsetup IPTVRequestDrillWall:(NSString *)iptvrequestdrillwall IPTVStatus:(NSString *)iptvstatus Email:(NSString *)email IPTVPromotionID:(NSString *)iptvpromotionid IPTVPackage:(NSString *)iptvpackage IPTVBoxCount:(NSString *)iptvboxcount IPTVPLCCount:(NSString *)iptvplccount IPTVReturnSTBCount:(NSString *)iptvreturnstbcount IPTVChargeTimes:(NSString *)iptvchargetimes IPTVPrepaid:(NSString *)iptvprepaid IPTVHBOPrepaidMonth:(NSString *)iptvhboprepaidmonth IPTVHBOChargeTimes:(NSString *)iptvhbochargetimes IPTVVTVPrepaidMonth:(NSString *)iptvvtvprepaidmonth IPTVVTVChargeTimes:(NSString *)iptvvtvchargetimes IPTVKPlusPrepaidMonth:(NSString *)iptvkplusprepaidmonth IPTVKPlusChargeTimes:(NSString *) iptvkpluscharegetimes IPTVVTCPrepaidMonth:(NSString *)iptvvtcprepaidmonth IPTVVTCChargeTimes:(NSString *)iptvvtcchargetimes IPTVDeviceTotal:(NSString *)iptvdevicetotal IPTVPrepaidTotal:(NSString *)iptvprepaidtotal IPTVTotal:(NSString *)iptvtotal InternetTotal:(NSString *)internettotal DescriptionIBB:(NSString *)descriptionIBB potentialID:(NSString *)potentialID indoor:(NSString *)indoor outdoor:(NSString *)outdoor paymentType:(NSString *)paymentType addressPassport:(NSString *)addressPassport birthday:(NSString *)birthday IPTVFimPlusPrepaidMonth:(NSString *)IPTVFimPlusPrepaidMonth IPTVFimPlusChargeTimes:(NSString *)IPTVFimPlusChargeTimes IPTVFimHotPrepaidMonth:(NSString *)IPTVFimHotPrepaidMonth IPTVFimHotChargeTimes:(NSString *)IPTVFimHotChargeTimes IPTVPromotionIDBoxOrder:(NSString *)IPTVPromotionIDBoxOrder OTTBoxCount:(NSString *)OTTBoxCount IPTVPromotionType:(NSString *)iptvPromotionType IPTVPromotionTypeBoxOrder:(NSString *) iptvPromotionTypeBoxOrder completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mUpdateRegistration)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    //create json
    //NSString *deviceKey = [OpenUDID value];358239055770376
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    if(Id == nil) Id = @"0";
    if(namevilla == nil) namevilla = @"0";
    if (indoor == nil) {
        indoor = @"0";
    }
    if (outdoor == nil) {
        outdoor = @"0";
    }
    
    //[dict setObject:username forKey:@"UserName"];
    [dict setObject:Id?:@"0" forKey:@"ID"];
    [dict setObject:objid?:@"0" forKey:@"ObjID"];
    [dict setObject:contract?:@"" forKey:@"Contract"];
    [dict setObject:locationid?:@"0" forKey:@"LocationID"];
    [dict setObject:localtype?:@"0" forKey:@"LocalType"];
    [dict setObject:fullname?:@"" forKey:@"FullName"];
    [dict setObject:contact?:@"" forKey:@"Contact"];
    [dict setObject:billto_city?:@"0" forKey:@"BillTo_City"];
    [dict setObject:billto_district?:@"0" forKey:@"BillTo_District"];
    [dict setObject:billto_ward?:@"0" forKey:@"BillTo_Ward"];
    [dict setObject:typehouse?:@"0" forKey:@"TypeHouse"];
    [dict setObject:billto_street?:@"0" forKey:@"BillTo_Street"];
    [dict setObject:lot?:@"0" forKey:@"Lot"];
    [dict setObject:floor?:@"0" forKey:@"Floor"];
    [dict setObject:room?:@"0" forKey:@"Room"];
    [dict setObject:namevilla?:@"" forKey:@"NameVilla"];
    [dict setObject:position?:@"0" forKey:@"Position"];
    [dict setObject:billto_number?:@"0" forKey:@"BillTo_Number"];
    [dict setObject:address?:@"" forKey:@"Address"];
    [dict setObject:note?:@"" forKey:@"Note"];
    [dict setObject:passport?:@"" forKey:@"Passport"];
    [dict setObject:taxid?:@"" forKey:@"TaxId"];
    [dict setObject:phone_1?:@"" forKey:@"Phone_1"];
    [dict setObject:phone_2?:@"" forKey:@"Phone_2"];
    [dict setObject:type_1?:@"0" forKey:@"Type_1"];
    [dict setObject:type_2?:@"0" forKey:@"Type_2"];
    [dict setObject:contact_1?:@"" forKey:@"Contact_1"];
    [dict setObject:contact_2?:@"" forKey:@"Contact_2"];
    [dict setObject:isptype?:@"0" forKey:@"ISPType"];
    [dict setObject:custypedetail?:@"" forKey:@"CusTypeDetail"];
    [dict setObject:currenthouse?:@"" forKey:@"CurrentHouse"];
    [dict setObject:paymentability?:@"0" forKey:@"PaymentAbility"];
    [dict setObject:partnerid?:@"0" forKey:@"PartnerID"];
    [dict setObject:legalentity?:@"0" forKey:@"LegalEntity"];
    [dict setObject:supporter?:@"" forKey:@"Supporter"];
    [dict setObject:promotionid?:@"0" forKey:@"PromotionID"];
    [dict setObject:total?:@"0" forKey:@"Total"];
    [dict setObject:username forKey:@"UserName"];
    [dict setObject:deposit?:@"0" forKey:@"Deposit"];
    [dict setObject:depositblackpoint?:@"0" forKey:@"DepositBlackPoint"];
    [dict setObject:objecttype?:@"0" forKey:@"ObjectType"];
    [dict setObject:cablestatus?:@"0" forKey:@"CableStatus"];
    [dict setObject:inscable?:@"0" forKey:@"InsCable"];
    [dict setObject:custype?:@"0" forKey:@"CusType"];
    [dict setObject:iptvusecombo?:@"0" forKey:@"IPTVUseCombo"];
    [dict setObject:iptvrequestsetup?:@"0" forKey:@"IPTVRequestSetUp"];
    [dict setObject:iptvrequestdrillwall?:@"0" forKey:@"IPTVRequestDrillWall"];
    [dict setObject:iptvstatus?:@"0" forKey:@"IPTVStatus"];
    [dict setObject:email?:@"" forKey:@"Email"];
    [dict setObject:iptvpromotionid?:@"0" forKey:@"IPTVPromotionID"];
    [dict setObject:iptvpackage?:@"0" forKey:@"IPTVPackage"];
    [dict setObject:iptvboxcount?:@"0" forKey:@"IPTVBoxCount"];
    [dict setObject:iptvplccount?:@"0" forKey:@"IPTVPLCCount"];
    [dict setObject:iptvreturnstbcount?:@"0" forKey:@"IPTVReturnSTBCount"];
    [dict setObject:iptvchargetimes?:@"0" forKey:@"IPTVChargeTimes"];
    [dict setObject:iptvprepaid?:@"0" forKey:@"IPTVPrepaid"];
    [dict setObject:iptvhboprepaidmonth?:@"0" forKey:@"IPTVHBOPrepaidMonth"];
    [dict setObject:iptvhbochargetimes?:@"0" forKey:@"IPTVHBOChargeTimes"];
    [dict setObject:iptvvtvprepaidmonth?:@"0" forKey:@"IPTVVTVPrepaidMonth"];
    [dict setObject:iptvvtvchargetimes?:@"0" forKey:@"IPTVVTVChargeTimes"];
    [dict setObject:iptvkplusprepaidmonth?:@"0" forKey:@"IPTVKPlusPrepaidMonth"];
    [dict setObject:iptvkpluscharegetimes?:@"0" forKey:@"IPTVKPlusChargeTimes"];
    [dict setObject:iptvvtcprepaidmonth?:@"0" forKey:@"IPTVVTCPrepaidMonth"];
    [dict setObject:iptvvtcchargetimes?:@"0" forKey:@"IPTVVTCChargeTimes"];
    [dict setObject:iptvdevicetotal?:@"0" forKey:@"IPTVDeviceTotal"];
    [dict setObject:iptvprepaidtotal?:@"0" forKey:@"IPTVPrepaidTotal"];
    [dict setObject:iptvtotal?:@"0" forKey:@"IPTVTotal"];
    [dict setObject:internettotal?:@"0" forKey:@"InternetTotal"];
    [dict setObject:descriptionIBB?:@"" forKey:@"DescriptionIBB"];
    [dict setObject:potentialID?:@"" forKey:@"PotentialID"];
    [dict setObject:indoor  ?:@"" forKey:@"InDoor"];
    [dict setObject:outdoor ?:@"" forKey:@"OutDoor"];
    [dict setObject:paymentType?:@"" forKey:@"Payment"];
    [dict setObject:addressPassport  ?:@"" forKey:@"AddressPassport"];
    [dict setObject:birthday ?:@"" forKey:@"Birthday"];
    
    [dict setObject:IPTVFimPlusPrepaidMonth ?:@"0" forKey:@"IPTVFimPlusPrepaidMonth"];
    [dict setObject:IPTVFimPlusChargeTimes?:@"0" forKey:@"IPTVFimPlusChargeTimes"];
    [dict setObject:IPTVFimHotPrepaidMonth  ?:@"0" forKey:@"IPTVFimHotPrepaidMonth"];
    [dict setObject:IPTVFimHotChargeTimes ?:@"0" forKey:@"IPTVFimHotChargeTimes"];
    [dict setObject:IPTVPromotionIDBoxOrder ?:@"0" forKey:@"IPTVPromotionIDBoxOrder"];
    [dict setObject:OTTBoxCount ?:@"0" forKey:@"OTTBoxCount"];
    
    //StevenNguyen IPTV new
    [dict setObject:OTTBoxCount ?:@"-1" forKey:@"IPTVPromotionType"];
    [dict setObject:OTTBoxCount ?:@"-1" forKey:@"IPTVPromotionTypeBoxOrder"];
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endUpdateRegistration:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endUpdateRegistration:(NSData *)result response:(NSURLResponse *)response
            completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *root = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    NSString *ErrorCode = StringFormat(@"%@",[root objectForKey:@"ErrorCode"]);
    NSString *message = StringFormat(@"%@",[root objectForKey:@"Error"]);
    
    if(root == nil){
        handler(nil,ErrorCode , message);
        return;
    }
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode,message);
        return;
    }
    
    NSArray *arr = [root objectForKey:@"ListObject"]?:nil;
    NSString *Result = StringFormat(@"%@",[[arr objectAtIndex:0]  objectForKey:@"Result"]);
    NSString *ResultID = StringFormat(@"%@",[[arr objectAtIndex:0]  objectForKey:@"ResultID"]);
    
    [arr setValue:Result forKey:@"Result"];
    [arr setValue:ResultID forKey:@"ResultID"];
    handler(arr, ResultID, Result);
}
//vutt11
#pragma mark - Get List Detail
- (void)GetIPTVTotal:(NSString*)username sPackage:(NSString *)spackage iPromotionID:(NSString *)ipromotionid iChargeTimes:(NSString *)ichargetimes iPrepaid:(NSString *)iprepaid iBoxCount:(NSString *)iboxcount iPLCCount:(NSString *)iplccount iReturnSTB:(NSString *)ireturnstb iVTVCabPrepaidMonth:(NSString *)ivtvcabprepaidmonth iVTVCabChargeTimes:(NSString *)ivtvcabchargetimes iKPlusPrepaidMonth:(NSString *)ikplusprepaidmonth iKPlusChargeTimes:(NSString *)ikpluschargetimes iVTCPrepaidMonth:(NSString *)ivtcprepaidmonnth iVTCChargeTimes:(NSString *)ivtcchargetimes iHBOPrepaidMonth:(NSString *)ihboprepaidmonth iHBOChargeTimes:(NSString *)ihbocharegetimes IServiceType:(NSString *)iServiceType iFimPlusChargeTimes:(NSString *)iFimPlusChargeTimes iFimPlusPrepaidMonth:(NSString *)iFimPlusPrepaidMonth iFimHotChargeTimes:(NSString *)iFimHotChargeTimes iFimHotPrepaidMonth:(NSString *)iFimHotPrepaidMonth iIPTVPromotionIDBoxOrder:(NSString *)iIPTVPromotionIDBoxOrder IPTVPromotionType:(NSString *)iPTVPromotionType IPTVPromotionTypeBoxOrder:(NSString *)iPTVPromotionTypeBoxOrder Contract:(NSString *)contract RegCode:(NSString *)regCode Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetIPTVTotal)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:username forKey:@"sUserName"];
    [dict setObject:spackage?:@"" forKey:@"sPackage"];
    [dict setObject:ipromotionid?:@"0" forKey:@"iPromotionID"];
    [dict setObject:ichargetimes?:@"0" forKey:@"iChargeTimes"];
    [dict setObject:iprepaid?:@"0" forKey:@"iPrepaid"];
    [dict setObject:iboxcount?:@"0" forKey:@"iBoxCount"];
    [dict setObject:iplccount?:@"0" forKey:@"iPLCCount"];
    [dict setObject:ireturnstb?:@"0" forKey:@"iReturnSTB"];
    [dict setObject:ivtvcabprepaidmonth?:@"0" forKey:@"iVTVCabPrepaidMonth"];
    [dict setObject:ivtvcabchargetimes?:@"0" forKey:@"iVTVCabChargeTimes"];
    [dict setObject:ikplusprepaidmonth?:@"0" forKey:@"iKPlusPrepaidMonth"];
    [dict setObject:ikpluschargetimes?:@"0" forKey:@"iKPlusChargeTimes"];
    [dict setObject:ivtcprepaidmonnth?:@"0" forKey:@"iVTCPrepaidMonth"];
    [dict setObject:ivtcchargetimes?:@"0" forKey:@"iVTCChargeTimes"];
    [dict setObject:ihboprepaidmonth?:@"0" forKey:@"iHBOPrepaidMonth"];
    [dict setObject:ihbocharegetimes?:@"0" forKey:@"iHBOChargeTimes"];
    [dict setObject:iServiceType?:@"0" forKey:@"iServiceType"];
    
    [dict setObject:iFimPlusChargeTimes?:@"0" forKey:@"iFimPlusChargeTimes"];
    [dict setObject:iFimPlusPrepaidMonth?:@"0" forKey:@"iFimPlusPrepaidMonth"];
    [dict setObject:iFimHotChargeTimes?:@"0" forKey:@"iFimHotChargeTimes"];
    [dict setObject:iFimHotPrepaidMonth?:@"0" forKey:@"iFimHotPrepaidMonth"];
    [dict setObject:iIPTVPromotionIDBoxOrder ?:@"0" forKey:@"iIPTVPromotionIDBoxOrder"];
    
    [dict setObject:iPTVPromotionType ?:@"-1" forKey:@"IPTVPromotionType"];
    
    if ([iPTVPromotionTypeBoxOrder  isEqual: @""]) {
        [dict setObject:@"-1" forKey:@"IPTVPromotionTypeBoxOrder"];
    } else {
        [dict setObject:iPTVPromotionTypeBoxOrder ?:@"-1" forKey:@"IPTVPromotionTypeBoxOrder"];
    }
    
//    [dict setObject:iPTVPromotionTypeBoxOrder ?:@"-1" forKey:@"IPTVPromotionTypeBoxOrder"];
    
//    [dict setObject:iPTVPromotionType ?:@"-1" forKey:@"IPTVPromotionType"];
//    [dict setObject:iPTVPromotionTypeBoxOrder ?:@"-1" forKey:@"IPTVPromotionTypeBoxOrder"];
    
    [dict setObject:contract ?:@"" forKey:@"Contract"];
    [dict setObject:regCode ?:@"" forKey:@"RegCode"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetIPTVTotal:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
    
}

- (void)endGetIPTVTotal:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *root = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(root == nil) return;
    
    NSString *ErrorCode = StringFormat(@"%@",[root objectForKey:@"ErrorCode"]?:nil);
    NSString *message;
    
    if(![ErrorCode isEqualToString:@"0"]){
        message = StringFormat(@"%@",[root objectForKey:@"Error"]);
        handler(nil, ErrorCode, message);
        return;
    }
    
    NSArray *arr = [root objectForKey:@"ListObject"]?:nil;
    handler([arr objectAtIndex:0], ErrorCode, message);
    
    
}

//vutt11
#pragma mark - OTT
- (void)GetToTalOTT:(NSString*)userName OTTBoxCount:(NSString *)oTTBoxCount dicJSON:(NSArray *)dicDeviceOTT Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI,mGetTotalOTT)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:userName forKey:@"UserName"];
    [dict setObject:dicDeviceOTT forKey:@"lOtt"];
//    [dict setObject:oTTBoxCount forKey:@"OTTBoxCount"];
    
    postString = [dict JSONRepresentation];
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetToTalOTT:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
    
}

- (void)endGetToTalOTT:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *root = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(root == nil) return;
    
    NSString *ErrorCode = StringFormat(@"%@",[root objectForKey:@"ErrorCode"]?:nil);
    NSString *message;
    
    if(![ErrorCode isEqualToString:@"0"]){
        message = StringFormat(@"%@",[root objectForKey:@"Error"]);
        handler(nil, ErrorCode, message);
        return;
    }
    
    NSArray *arr = [root objectForKey:@"ListObject"]?:nil;
    handler([arr objectAtIndex:0], ErrorCode, message);

}

#pragma mark - Update Deposit
- (void)UpdateDeposit:(NSString*)username RegCode:(NSString *)regcode SBI_Internet:(NSString *)sbi_internet SBI_IPTV:(NSString *)sbi_iptv Total:(NSString *)total ImageSignature:(NSString *)imageSignature PaidType:(NSString *)paidType Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI,mUpdateDeposit)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:username                forKey:@"Username"];
    [dict setObject:regcode         ?:@""   forKey:@"RegCode"];
    [dict setObject:sbi_internet    ?:@"0"  forKey:@"SBI_Internet"];
    [dict setObject:sbi_iptv        ?:@"0"  forKey:@"SBI_IPTV"];
    [dict setObject:total           ?:@"0"  forKey:@"Total"];
    [dict setObject:imageSignature  ?:@"0"  forKey:@"ImageSignature"];
    [dict setObject:paidType        ?:@"0"  forKey:@"PaidType"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endUpdateDeposit:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
    
}

- (void)endUpdateDeposit:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *root = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(root == nil) return;
    
    NSString *ErrorCode = StringFormat(@"%@",[root objectForKey:@"ErrorCode"]?:nil);
    NSString *Error     = StringFormat(@"%@",[root objectForKey:@"Error"]?:nil);

    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, Error);
        return;
    }
    
    NSArray *arr = [root objectForKey:@"ListObject"]?:nil;
    NSString *Result = [[arr objectAtIndex:0] objectForKey:@"Result"]?:@"";
    NSString *ResultID = [[arr objectAtIndex:0] objectForKey:@"ResultID"]?:@"";
    handler(nil, ResultID, Result);
    
    
}

#pragma mark - Create contract
- (void)CreateObject:(NSString*)username RegCode:(NSString *)regcode Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI,mCreateObject)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:username forKey:@"UserName"];
    [dict setObject:regcode?:@"" forKey:@"RegCode"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endCreateObject:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}

- (void)endCreateObject:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *root = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(root == nil) return;
    
    NSString *ErrorCode = StringFormat(@"%@",[root objectForKey:@"ErrorCode"]?:nil);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, @"");
        return;
    }
    
    NSArray *arr = [root objectForKey:@"ListObject"]?:nil;
    NSString *Result = [[arr objectAtIndex:0] objectForKey:@"Result"]?:@"";
    NSString *ResultID = [[arr objectAtIndex:0] objectForKey:@"ResultID"]?:@"";
    handler(nil, ResultID, Result);

}

- (void)checkAddressNumber:(NSString *)username locationParent:(NSString *)locationParent locationID:(NSString *)locationID houseNumber:(NSString *)houseNumber completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI,mCheckAddressNumber)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = @"";
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:username        forKey:@"UserName"];
    [dict setObject:locationParent  forKey:@"LocationParent"];
    [dict setObject:locationID      forKey:@"LocationID"];
    [dict setObject:houseNumber     forKey:@"HouseNumber"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    
    NSString * strUTF8 = @"";
    NSMutableString *s = [NSMutableString stringWithString:postString];
    [s replaceOccurrencesOfString:@"\\" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    strUTF8 = [NSString stringWithString:s];

    
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:strUTF8];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endCheckAddressNumber:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endCheckAddressNumber:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *root = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(root == nil) return;
    
    NSString *ErrorCode = StringFormat(@"%@",[root objectForKey:@"ErrorCode"]?:nil);
    
    if(![ErrorCode isEqualToString:@"0"]) {
        handler(nil, ErrorCode, @"");
        return;
    }
    
    NSArray *arr = [root objectForKey:@"ListObject"]?:nil;
    NSDictionary *dict = [arr objectAtIndex:0];
    handler(dict, @"", @"");
    
}

#pragma mark - Get contract deposit list 
- (void)getContractDepositList:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetContractDepositList)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    
    NSString *postString = [dict JSONRepresentation];
    
    //vutt11
    NSString * strUTF8 = @"";
    NSMutableString *s = [NSMutableString stringWithString:postString];
    [s replaceOccurrencesOfString:@"\\" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    strUTF8 = [NSString stringWithString:s];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:strUTF8];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetContractDepositList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}

- (void)endGetContractDepositList:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
   
    
    /*Parse json response*/
    
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    NSString *error = StringFormat(@"%@",[dict objectForKey:@"Error"]);
    
    if(![error isEqualToString:@"<null>"]){
        handler(nil, error, error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    
    if(arr.count <= 0){
        return;
    }
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        NSString *Name = StringFormat(@"%@",[d objectForKey:@"Title"]);
        NSString *Id = StringFormat(@"%@",[d objectForKey:@"Total"]);
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:Id description:Name];
        
        [mArray addObject:s];
    }
    handler(mArray, error, @"");
    
}

#pragma mark - Update Receipt Internet
- (void)updateReceiptInternet:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mUpdateReceiptInternet)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endUpdateReceiptInternet:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}

- (void)endUpdateReceiptInternet:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
        NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
        if (![headerErrorCode isEqualToString:@"0"]) {
            handler (nil, headerErrorCode, @"het phien lam viec");
            return;
        }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    NSString *error = StringFormat(@"%@",[dict objectForKey:@"Error"]);
    
    if(![error isEqualToString:@"<null>"]){
        handler(nil, error, error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    
    if(arr.count <= 0){
        return;
    }
    
    NSDictionary *d = [arr objectAtIndex:0];
    NSString *resultMessage = StringFormat(@"%@",[d objectForKey:@"Result"]);
    NSString *resultID = StringFormat(@"%@",[d objectForKey:@"ResultID"]);
    
    handler(d, resultID, resultMessage);
    
    return;

}

#pragma mark - Get Payment Type List
- (void)getPaymentType:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetPaymentType)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    NSString * strUTF8 = @"";
    NSMutableString *s = [NSMutableString stringWithString:postString];
    [s replaceOccurrencesOfString:@"\\" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    strUTF8 = [NSString stringWithString:s];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:strUTF8];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetPaymentType:result response:res completionHandler:handler errorHandler:errHandler];
        
        
};
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}

- (void)endGetPaymentType:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response*/
    /*Parse json response header*/
    
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    NSString *error = StringFormat(@"%@",[dict objectForKey:@"Error"]);
    
    if(![error isEqualToString:@"<null>"]){
        handler(nil, error, error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    
    if(arr.count <= 0){
        return;
    }
    
    NSMutableArray *arrData = [NSMutableArray array];
    for (NSDictionary *d in arr) {
        NSString *ID = StringFormat(@"%@",[d objectForKey:@"ID"]);
        NSString *name = StringFormat(@"%@",[d objectForKey:@"Name"]);
        KeyValueModel *keyValue = [[KeyValueModel alloc] initWithName:ID description:name];
        [arrData addObject:keyValue];
    }
   
    
    handler(arrData, @"", @"Get list payment type success");
    
    return;
    
}

#pragma mark - WriteLogMPos
- (void)writeLogMPos:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mWriteLogMPos)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    //[Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endWriteLogMPos:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
   
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}

- (void)endWriteLogMPos:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    NSString *error = StringFormat(@"%@",[dict objectForKey:@"Error"]);
    
    if(![error isEqualToString:@"<null>"]){
        handler(nil, error, error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    
    if(arr.count <= 0){
        return;
    }
    
    NSString *resultID  = [arr[0] objectForKey:@"ResultID"];
    NSString *resultStr = [arr[0] objectForKey:@"Result"];

    handler(arr, resultID, resultStr);
    
    return;
    
}

#pragma mark - Update Deposit mPOS
- (void)updateDepositmPOS:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mUpdateDepositmPOS)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endUpdateDepositmPOS:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}

- (void)endUpdateDepositmPOS:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    NSString *error = StringFormat(@"%@",[dict objectForKey:@"Error"]);
    
    if(![error isEqualToString:@"<null>"]){
        handler(nil, error, error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    
    if(arr.count <= 0){
        return;
    }
    
    NSString *resultID  = [arr[0] objectForKey:@"ResultID"];
    NSString *resultStr = [arr[0] objectForKey:@"Result"];
    
    handler(arr, resultID, resultStr);

    return;
    
}

#pragma mark - check UpdateImage

- (void)upDateImage:(NSString *)userName Image:(NSString *)image RegCode:(NSString *)regCode Type:(int)type Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler{
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPIIMAGE, mGetUploadImageInvest)];
    //urlimage
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:userName    forKey:@"UserName"];
    [dict setObject:image       forKey:@"image"];
    [dict setObject:regCode     forKey:@"Regcode"];
    [dict setObject:StringFormat(@"%i",type)        forKey:@"Type"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endUpdateImage:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];

}

- (void)endUpdateImage:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *root = [jsonDict objectForKey:@"UploadImageInvestResult"]?:nil;
    
    if(root == nil){
        handler(nil,@"" , @"Upload ảnh thất bại");
        return;
    }
    
    NSString *ErrorService = StringFormat(@"%@",[root objectForKey:@"ErrorService"]);
    
    if(![ErrorService isEqualToString:@"<null>"]){
        handler(nil, ErrorService, @"Upload ảnh thất bại");
        return;
    }
    
    NSString *Result = StringFormat(@"%@",[root objectForKey:@"Result"]);
    
    handler(nil,Result , Result);

}

#pragma mark - check Deposit mPOS
- (void)checkForDeposit:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mCheckForDeposit)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endCheckForDeposit:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}

- (void)endCheckForDeposit:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
   
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    NSString *error = StringFormat(@"%@",[dict objectForKey:@"Error"]);
    
    if(![error isEqualToString:@"<null>"]){
        handler(nil, error, error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    
    if(arr.count <= 0){
        return;
    }
    
    NSString *resultID  = [arr[0] objectForKey:@"ResultID"];
    NSString *resultStr = [arr[0] objectForKey:@"Result"];
    
    handler(arr, resultID, resultStr);
    
    
    return;
    
}

#pragma mark - Lấy giá thiết bị IPTV và giá đăng ký các gói Extra IPTV
- (void)getIPTVPrice:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetIPTVPrice)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetIPTVPrice:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}

- (void)endGetIPTVPrice:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
   
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    NSString *error = StringFormat(@"%@",[dict objectForKey:@"Error"]);
    NSString *errorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]);
    
    if(![error isEqualToString:@"<null>"]){
        handler(nil, errorCode, error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    
    if(arr.count <= 0){
        return;
    }
    
    IPTVPrice *price = [self pareDataIPTVPrice:arr[0]];
    
    handler(price, errorCode, error);
    
    return;
    
}

- (IPTVPrice *)pareDataIPTVPrice:(NSDictionary *)dict {
    IPTVPrice *price = [[IPTVPrice alloc] init];
    
    price.DacSacHDPrice = StringFormat(@"%@",[dict objectForKey:@"DacSacHD"]);
    price.FimHotPrice = StringFormat(@"%@",[dict objectForKey:@"FimHot"]);
    price.FimPlusPrice = StringFormat(@"%@",[dict objectForKey:@"FimPlus"]);
    price.HDBoxPrice = StringFormat(@"%@",[dict objectForKey:@"HDBox"]);
    price.KPlusPrice = StringFormat(@"%@",[dict objectForKey:@"KPlus"]);
    price.PLCPrice = StringFormat(@"%@",[dict objectForKey:@"PLC"]);
    price.STBPrice = StringFormat(@"%@",[dict objectForKey:@"STB"]);
    price.VTCHDPrice = StringFormat(@"%@",[dict objectForKey:@"VTCHD"]);
    price.VTVCabPrice = StringFormat(@"%@",[dict objectForKey:@"VTVCap"]);
    
    return price;
}

#pragma mark - Lấy thông tin Phiếu thi công hiện tại của hợp đồng - API 68
- (void)getPTCDetail:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetPTCDetail)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetPTCDetail:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
   
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}

- (void)endGetPTCDetail:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    NSString *error = StringFormat(@"%@",[dict objectForKey:@"Error"]);
    NSString *errorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]);
    
    if(![error isEqualToString:@"<null>"]){
        handler(nil, errorCode, error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    
    if(arr.count <= 0){
        handler(nil, errorCode, error);
        return;
    }
    
    ConstructionVoteDetailModel *model = [ConstructionVoteDetailModel constructionVoteDetailModelWithDict:arr[0]];
    
    handler(model, errorCode, error);
    
    return;
    
}

#pragma mark - Chuyển tình trạng PTC của hợp đồng chưa duyệt

- (void)changePTCStatusForNotApproval:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mChangePTCStatusForNotApproval)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endChangePTCStatusForNotApproval:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}

- (void)endChangePTCStatusForNotApproval:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    NSString *error = StringFormat(@"%@",[dict objectForKey:@"Error"]);
    
    if(![error isEqualToString:@"<null>"]){
        handler(nil, error, error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    
    if(arr.count <= 0){
        return;
    }
    
    NSString *resultID  = [arr[0] objectForKey:@"ResultID"];
    NSString *resultStr = [arr[0] objectForKey:@"Result"];
    
    handler(arr, resultID, resultStr);
    
    return;
    
}

#pragma mark - Update Registration Form 
- (void)updateRegistration:(NSDictionary *)parameters completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSString *urlString = StringFormat(@"%@/%@",urlAPI, mUpdateRegistration);
    [self requestAPIWithURLString:urlString withParameters:parameters completionHandler:handler errorHandler:errHandler];

}

#pragma mark - Office 365 API
// Lấy thông tin các gói đăng ký Office 365
- (void)getPackage365:(NSDictionary *)parameters completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSString *urlString = StringFormat(@"%@/%@",urlAPI, mGetPackage365);
    [self requestAPIWithURLString:urlString withParameters:parameters completionHandler:handler errorHandler:errHandler];
}

// Tính tổng tiền Office 365
- (void)getTotalOffice365:(NSDictionary *)parameters completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSString *urlString = StringFormat(@"%@/%@",urlAPI, mGetTotalOffice365);
    [self requestAPIWithURLString:urlString withParameters:parameters completionHandler:handler errorHandler:errHandler];
}

/*----------------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------- Setup Request API ------------------------------------------------------------*/
#pragma Mark - Setup Request API
// Start request API
- (void)requestAPIWithURLString:(NSString *)urlString withParameters:(NSDictionary *)parameters completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:urlString];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
  
    NSString *postString = [parameters JSONRepresentation];
    
    NSString * strUTF8 = @"";
    NSMutableString *s = [NSMutableString stringWithString:postString];
    [s replaceOccurrencesOfString:@"\\" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    strUTF8 = [NSString stringWithString:s];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
   // NSString *str = [[NSString alloc] initWithData:[postString dataUsingEncoding:NSUTF8StringEncoding] encoding:NSUTF8StringEncoding];
    
    NSString *base64 = [FPTUtility base64:strUTF8];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endRequestAPI:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

// End Request API
- (void)endRequestAPI:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"] || [headerErrorCode isEqualToString:@"(null)"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *reponseData = jsonDict[@"ResponseResult"] ?:nil;
    
    NSString *error     = reponseData[@"Error"];
    NSString *errorCode = reponseData[@"ErrorCode"];
    NSArray  *arrayData = reponseData[@"ListObject"];
    
    if([errorCode integerValue] != 0 || arrayData.count <= 0){
        handler(nil, errorCode, error);
        return;
    }
    
    handler(arrayData, errorCode, error);
    
    return;
    
}

@end
