//
//  RegistrationFormProxy.h
//  MobiSale
//
//  Created by HIEUPC on 1/27/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BaseProxy.h"

@interface RegistrationFormProxy : BaseProxy

- (void)GetListRegistration:(NSString*)PageNum Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)GetListRegistrationPost:(NSString*)userName pageNumber:(NSString*)pageNumber agent:(NSString*)agent agentName:(NSString*)agentName Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)GetDetailRegistration:(NSString*)Id Completehander:(DidGetResultBlock)handler
                 errorHandler:(ErrorBlock)errHandler;

//- (void)UpdateRegistration:(NSString *)Id ObjID:(NSString *)objid Contract:(NSString *)contract LocationID:(NSString *)locationid LocalType:(NSString *)localtype FullName:(NSString *)fullname Contact:(NSString *)contact BillTo_City:(NSString *)billto_city     BillTo_District:(NSString *)billto_district BillTo_Ward:(NSString *)billto_ward TypeHouse:(NSString *)typehouse BillTo_Street:(NSString *)billto_street Lot:(NSString *)lot Floor:(NSString *)floor Room:(NSString *)room NameVilla:(NSString *)namevilla Position:(NSString *)position BillTo_Number:(NSString *)billto_number Address:(NSString *)address Note:(NSString *)note Passport:(NSString *)passport TaxId:(NSString *)taxid Phone_1:(NSString *)phone_1 Phone_2:(NSString *)phone_2 Type_1:(NSString *)type_1 Type_2:(NSString *)type_2 Contact_1:(NSString *)contact_1 Contact_2:(NSString *)contact_2 ISPType:(NSString *)isptype CusTypeDetail:(NSString *)custypedetail CurrentHouse:(NSString *)currenthouse PaymentAbility:(NSString *)paymentability PartnerID:(NSString *)partnerid LegalEntity:(NSString *)legalentity Supporter:(NSString *)supporter PromotionID:(NSString *)promotionid Total:(NSString *)total UserName:(NSString *)username Deposit:(NSString *)deposit DepositBlackPoint:(NSString *)depositblackpoint ObjectType:(NSString *)objecttype CableStatus:(NSString *)cablestatus InsCable:(NSString *)inscable CusType:(NSString *)custype IPTVUseCombo:(NSString *)iptvusecombo IPTVRequestSetUp:(NSString *)iptvrequestsetup IPTVRequestDrillWall:(NSString *)iptvrequestdrillwall IPTVStatus:(NSString *)iptvstatus Email:(NSString *)email IPTVPromotionID:(NSString *)iptvpromotionid IPTVPackage:(NSString *)iptvpackage IPTVBoxCount:(NSString *)iptvboxcount IPTVPLCCount:(NSString *)iptvplccount IPTVReturnSTBCount:(NSString *)iptvreturnstbcount IPTVChargeTimes:(NSString *)iptvchargetimes IPTVPrepaid:(NSString *)iptvprepaid IPTVHBOPrepaidMonth:(NSString *)iptvhboprepaidmonth IPTVHBOChargeTimes:(NSString *)iptvhbochargetimes IPTVVTVPrepaidMonth:(NSString *)iptvvtvprepaidmonth IPTVVTVChargeTimes:(NSString *)iptvvtvchargetimes IPTVKPlusPrepaidMonth:(NSString *)iptvkplusprepaidmonth IPTVKPlusChargeTimes:(NSString *) iptvkpluscharegetimes IPTVVTCPrepaidMonth:(NSString *)iptvvtcprepaidmonth IPTVVTCChargeTimes:(NSString *)iptvvtcchargetimes IPTVDeviceTotal:(NSString *)iptvdevicetotal IPTVPrepaidTotal:(NSString *)iptvprepaidtotal IPTVTotal:(NSString *)iptvtotal InternetTotal:(NSString *)internettotal DescriptionIBB:(NSString *)descriptionIBB potentialID:(NSString *)potentialID indoor:(NSString *)indoor outdoor:(NSString *)outdoor paymentType:(NSString *)paymentType addressPassport:(NSString *)addressPassport birthday:(NSString *)birthday IPTVFimPlusPrepaidMonth:(NSString *)IPTVFimPlusPrepaidMonth IPTVFimPlusChargeTimes:(NSString *)IPTVFimPlusChargeTimes IPTVFimHotPrepaidMonth:(NSString *)IPTVFimHotPrepaidMonth IPTVFimHotChargeTimes:(NSString *)IPTVFimHotChargeTimes IPTVPromotionIDBoxOrder:(NSString *)IPTVPromotionIDBoxOrder completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
    
    - (void)UpdateRegistration:(NSString *)Id ObjID:(NSString *)objid Contract:(NSString *)contract LocationID:(NSString *)locationid LocalType:(NSString *)localtype FullName:(NSString *)fullname Contact:(NSString *)contact BillTo_City:(NSString *)billto_city     BillTo_District:(NSString *)billto_district BillTo_Ward:(NSString *)billto_ward TypeHouse:(NSString *)typehouse BillTo_Street:(NSString *)billto_street Lot:(NSString *)lot Floor:(NSString *)floor Room:(NSString *)room NameVilla:(NSString *)namevilla Position:(NSString *)position BillTo_Number:(NSString *)billto_number Address:(NSString *)address Note:(NSString *)note Passport:(NSString *)passport TaxId:(NSString *)taxid Phone_1:(NSString *)phone_1 Phone_2:(NSString *)phone_2 Type_1:(NSString *)type_1 Type_2:(NSString *)type_2 Contact_1:(NSString *)contact_1 Contact_2:(NSString *)contact_2 ISPType:(NSString *)isptype CusTypeDetail:(NSString *)custypedetail CurrentHouse:(NSString *)currenthouse PaymentAbility:(NSString *)paymentability PartnerID:(NSString *)partnerid LegalEntity:(NSString *)legalentity Supporter:(NSString *)supporter PromotionID:(NSString *)promotionid Total:(NSString *)total UserName:(NSString *)username Deposit:(NSString *)deposit DepositBlackPoint:(NSString *)depositblackpoint ObjectType:(NSString *)objecttype CableStatus:(NSString *)cablestatus InsCable:(NSString *)inscable CusType:(NSString *)custype IPTVUseCombo:(NSString *)iptvusecombo IPTVRequestSetUp:(NSString *)iptvrequestsetup IPTVRequestDrillWall:(NSString *)iptvrequestdrillwall IPTVStatus:(NSString *)iptvstatus Email:(NSString *)email IPTVPromotionID:(NSString *)iptvpromotionid IPTVPackage:(NSString *)iptvpackage IPTVBoxCount:(NSString *)iptvboxcount IPTVPLCCount:(NSString *)iptvplccount IPTVReturnSTBCount:(NSString *)iptvreturnstbcount IPTVChargeTimes:(NSString *)iptvchargetimes IPTVPrepaid:(NSString *)iptvprepaid IPTVHBOPrepaidMonth:(NSString *)iptvhboprepaidmonth IPTVHBOChargeTimes:(NSString *)iptvhbochargetimes IPTVVTVPrepaidMonth:(NSString *)iptvvtvprepaidmonth IPTVVTVChargeTimes:(NSString *)iptvvtvchargetimes IPTVKPlusPrepaidMonth:(NSString *)iptvkplusprepaidmonth IPTVKPlusChargeTimes:(NSString *) iptvkpluscharegetimes IPTVVTCPrepaidMonth:(NSString *)iptvvtcprepaidmonth IPTVVTCChargeTimes:(NSString *)iptvvtcchargetimes IPTVDeviceTotal:(NSString *)iptvdevicetotal IPTVPrepaidTotal:(NSString *)iptvprepaidtotal IPTVTotal:(NSString *)iptvtotal InternetTotal:(NSString *)internettotal DescriptionIBB:(NSString *)descriptionIBB potentialID:(NSString *)potentialID indoor:(NSString *)indoor outdoor:(NSString *)outdoor paymentType:(NSString *)paymentType addressPassport:(NSString *)addressPassport birthday:(NSString *)birthday IPTVFimPlusPrepaidMonth:(NSString *)IPTVFimPlusPrepaidMonth IPTVFimPlusChargeTimes:(NSString *)IPTVFimPlusChargeTimes IPTVFimHotPrepaidMonth:(NSString *)IPTVFimHotPrepaidMonth IPTVFimHotChargeTimes:(NSString *)IPTVFimHotChargeTimes IPTVPromotionIDBoxOrder:(NSString *)IPTVPromotionIDBoxOrder OTTBoxCount:(NSString *)OTTBoxCount IPTVPromotionType:(NSString *)iptvPromotionType IPTVPromotionTypeBoxOrder:(NSString *) iptvPromotionTypeBoxOrder completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

//- (void)GetIPTVTotal:(NSString*)username sPackage:(NSString *)spackage iPromotionID:(NSString *)ipromotionid iChargeTimes:(NSString *)ichargetimes iPrepaid:(NSString *)iprepaid iBoxCount:(NSString *)iboxcount iPLCCount:(NSString *)iplccount iReturnSTB:(NSString *)ireturnstb iVTVCabPrepaidMonth:(NSString *)ivtvcabprepaidmonth iVTVCabChargeTimes:(NSString *)ivtvcabchargetimes iKPlusPrepaidMonth:(NSString *)ikplusprepaidmonth iKPlusChargeTimes:(NSString *)ikpluschargetimes iVTCPrepaidMonth:(NSString *)ivtcprepaidmonnth iVTCChargeTimes:(NSString *)ivtcchargetimes iHBOPrepaidMonth:(NSString *)ihboprepaidmonth iHBOChargeTimes:(NSString *)ihbocharegetimes IServiceType:(NSString *)iServiceType iFimPlusChargeTimes:(NSString *)iFimPlusChargeTimes iFimPlusPrepaidMonth:(NSString *)iFimPlusPrepaidMonth iFimHotChargeTimes:(NSString *)iFimHotChargeTimes iFimHotPrepaidMonth:(NSString *)iFimHotPrepaidMonth iIPTVPromotionIDBoxOrder:(NSString *)iIPTVPromotionIDBoxOrder  Completehander:(DidGetResultBlock)handler
//        errorHandler:(ErrorBlock)errHandler;
- (void)GetIPTVTotal:(NSString*)username sPackage:(NSString *)spackage iPromotionID:(NSString *)ipromotionid iChargeTimes:(NSString *)ichargetimes iPrepaid:(NSString *)iprepaid iBoxCount:(NSString *)iboxcount iPLCCount:(NSString *)iplccount iReturnSTB:(NSString *)ireturnstb iVTVCabPrepaidMonth:(NSString *)ivtvcabprepaidmonth iVTVCabChargeTimes:(NSString *)ivtvcabchargetimes iKPlusPrepaidMonth:(NSString *)ikplusprepaidmonth iKPlusChargeTimes:(NSString *)ikpluschargetimes iVTCPrepaidMonth:(NSString *)ivtcprepaidmonnth iVTCChargeTimes:(NSString *)ivtcchargetimes iHBOPrepaidMonth:(NSString *)ihboprepaidmonth iHBOChargeTimes:(NSString *)ihbocharegetimes IServiceType:(NSString *)iServiceType iFimPlusChargeTimes:(NSString *)iFimPlusChargeTimes iFimPlusPrepaidMonth:(NSString *)iFimPlusPrepaidMonth iFimHotChargeTimes:(NSString *)iFimHotChargeTimes iFimHotPrepaidMonth:(NSString *)iFimHotPrepaidMonth iIPTVPromotionIDBoxOrder:(NSString *)iIPTVPromotionIDBoxOrder IPTVPromotionType:(NSString *)iPTVPromotionType IPTVPromotionTypeBoxOrder:(NSString *)iPTVPromotionTypeBoxOrder Contract:(NSString *)contract RegCode:(NSString *)regCode Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

//- (void)UpdateDeposit:(NSString*)username RegCode:(NSString *)regcode SBI_Internet:(NSString *)sbi_internet SBI_IPTV:(NSString *)sbi_iptv Total:(NSString *)total ImageSignature:(NSString *)imageSignature Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)UpdateDeposit:(NSString*)username RegCode:(NSString *)regcode SBI_Internet:(NSString *)sbi_internet SBI_IPTV:(NSString *)sbi_iptv Total:(NSString *)total ImageSignature:(NSString *)imageSignature PaidType:(NSString *)paidType Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)CreateObject:(NSString*)username RegCode:(NSString *)regcode Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)checkAddressNumber:(NSString *)username locationParent:(NSString *)locationParent locationID:(NSString *)locationID houseNumber:(NSString *)houseNumber completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

//Get contract deposit list
- (void)getContractDepositList:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

// Update Receipt Internet
- (void)updateReceiptInternet:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

// Get Payment Type
- (void)getPaymentType:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

// WriteLogMPos
- (void)writeLogMPos:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

// Update Deposit mPOS
- (void)updateDepositmPOS:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

// Check Deposit mPOS
- (void)checkForDeposit:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

// Lấy giá thiết bị IPTV và giá đăng ký các gói Extra IPTV
- (void)getIPTVPrice:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

// Lấy thông tin Phiếu thi công hiện tại của hợp đồng
- (void)getPTCDetail:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

// Chuyển tình trạng PTC của hợp đồng chưa duyệt
- (void)changePTCStatusForNotApproval:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

#pragma mark - Update Registration Form
- (void)updateRegistration:(NSDictionary *)parameters completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHand;

#pragma mark - Office 365 API
// Lấy thông tin các gói đăng ký Office 365
- (void)getPackage365:(NSDictionary *)parameters completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Tính tổng tiền Office 365
- (void)getTotalOffice365:(NSDictionary *)parameters completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Tính tổng tiền OTT
//- (void)GetToTalOTT:(NSString*)userName OTTBoxCount:(NSString *)oTTBoxCount Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

// Tính tổng tiền OTT New
- (void)GetToTalOTT:(NSString*)userName OTTBoxCount:(NSString *)oTTBoxCount dicJSON:(NSDictionary *)dicDeviceOTT Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

// updateimage
- (void)upDateImage:(NSString *)userName Image:(NSString *)image RegCode:(NSString*)regCode Type:(int)type Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;



@end
