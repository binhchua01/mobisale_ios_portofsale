//
//  UpdateReceiptViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 4/6/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "UpdateReceiptViewController.h"
#import "ShareData.h"
#import "KeyValueModel.h"
#import "PromotionModel.h"
#import "Common.h"
#import "NIDropDown.h"
#import "NSString+GetStringHeight.h"
#import "AppDelegate.h"

enum actionSheetTagType {
    back = 1,
    save = 2,
};

enum alertViewTagType {
    CheckUpdate = 1,
    UpdateSuccess = 2,
    UpdateError = 3,
    Error = 4
};

@interface UpdateReceiptViewController () <NIDropDownDelegate, UIAlertViewDelegate>

@end

@implementation UpdateReceiptViewController {
    
    NSNumberFormatter *nf;
    
    NSInteger newMoneyTotal;
    
    NSInteger MoneyDifference;
    
    KeyValueModel *selectedServicePackage, *selectedDepositBlackPoint, *selectedDepositContract;
    
    PromotionModel *selectedPromotion;
    
    NSArray *arrServicePackage, *arrPromotion, *arrDepositBlackPoint, *arrDepositContract;
    
    NIDropDown *dropDown;
    UIButton *btnClicked;
    
}

@synthesize record, serviceType;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"CẬP NHẬT TỔNG TIỀN";
    self.screenName = @"CẬP NHẬT PHIẾU THU ĐIỆN TỬ";
    
    nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    
    self.btnPromotion.titleLabel.numberOfLines = 0;
    self.btnPromotion.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    self.txtNewMoneyTotal.userInteractionEnabled = NO;
    
    // add right bar button save
    self.navigationItem.rightBarButtonItem = [self rightBarButtonItem:[UIImage imageNamed:@"save_32_2"]];
    
    self.navigationItem.leftBarButtonItem = [self backBarButtonItem:[UIImage imageNamed:@"ic_pre"]];
    
    // handle hide list drop down view when tap on screen
    UITapGestureRecognizer *tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    
    [self.view addGestureRecognizer:tapper];
    
    // load data
    [self loadData];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - custom UIBarButtonItem

- (UIBarButtonItem *)backBarButtonItem:(UIImage *)icon
{
    UIImage *image = [SiUtils imageWithImageHeight:[icon imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] height:30];
    return [[UIBarButtonItem alloc]
            initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered
            target:self
            action:@selector(backButtonPressed:)];
    
}

- (UIBarButtonItem *)rightBarButtonItem:(UIImage *)icon
{
    UIImage *image = [SiUtils imageWithImageHeight:[icon imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] height:30];
    return [[UIBarButtonItem alloc]
            initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered
            target:self
            action:@selector(rightBarButton_Clicked:)];
    
}

- (void)rightBarButton_Clicked:(id)sender
{
    NSLog(@"update receipt");
    
    UIActionSheet *noticeSave = [[UIActionSheet alloc] initWithTitle:@"Bạn chắc chắn muốn cập nhật Phiếu thu điện tử?" delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
    noticeSave.tag = save;
    [noticeSave showInView:[UIApplication sharedApplication].keyWindow];
}

- (void)backButtonPressed:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Bạn chắc chắn muốn thoát?" delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
    actionSheet.tag = back;
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}

#pragma mark - action sheet delegate method
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        switch (actionSheet.tag) {
            case back:
                [self.navigationController popViewControllerAnimated:YES];
                break;
            case save:
                [self checkUpdateReceipt];
                break;
            default:
                break;
        }
    }
    
}

- (IBAction)btnDepositBlackPoint_Clicked:(id)sender
{
    [self showDropDownView:sender data:[self pareArrayForKeyWith:arrDepositBlackPoint button:sender]];
}

- (IBAction)btnPromotion_Clicked:(id)sender
{
    [self showDropDownView:sender data:[self pareArrayForKeyWith:arrPromotion button:sender]];
}

- (IBAction)btnDepositContract_Clicked:(id)sender
{
    [self showDropDownView:sender data:[self pareArrayForKeyWith:arrDepositContract button:sender]];
}

- (IBAction)btnServicePackage_Clicked:(id)sender
{
    [self showDropDownView:sender data:[self pareArrayForKeyWith:arrServicePackage button:sender]];
}

/*
 * load data
 */
- (void)loadData
{

    self.txtNewMoneyTotal.text = @"0";
    self.lblMoneyDifference.text = @"0";
    self.lblRegCode.text = record.RegCode;
    self.lblPhoneNumber.text = record.Phone_1;
    self.lblCurrentMoneyTotal.text = StringFormat(@"%@ VNĐ",[nf stringFromNumber:[NSNumber numberWithFloat:[record.Total intValue]]]);
    
    [self loadDepositContract:record.Deposit];
    [self loadDepositBlackPoint:record.DepositBlackPoint];
    [self loadLocalTypePost:serviceType serverPackageID:record.LocalType PromotionID:record.PromotionID];
    
}

/*
 * load list service package with service type: internet or internet-IPTV
 */

- (void)loadLocalTypePost:(NSString *)Id serverPackageID:(NSString *)serverPackageID PromotionID:(NSString *)promotionID
{
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    ShareData *shared = [ShareData instance];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:Id forKey:@"ServiceType"];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    
    [self showMBProcess];
    
    [shared.appProxy GetListLocalTypePost:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        // [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        
        arrServicePackage = result;
        
        if(arrServicePackage.count > 0) {
            
            if([serverPackageID isEqualToString:@"0"]){
                
                selectedServicePackage = [arrServicePackage objectAtIndex:0];
                [self.btnServicePackage setTitle:selectedServicePackage.Values forState:UIControlStateNormal];
                
            }else {
                
                for (selectedServicePackage in arrServicePackage) {
                    if([serverPackageID isEqualToString:selectedServicePackage.Key]){
                        [self.btnServicePackage setTitle:selectedServicePackage.Values forState:UIControlStateNormal];
                        break;
                    }
                }
            }
            
            [self loadPromotion:promotionID LocalType:selectedServicePackage.Key];
            return;
        }
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetListLocalTypePost",[error localizedDescription]]];
        [self hideMBProcess];
        
    }];
    
}

/*
 * load list promotion statement with service package id
 */

-(void)loadPromotion:(NSString *)Id LocalType:(NSString *)localtype
{
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    [shared.appProxy GetListPromotion:localtype Complatehander:^(id result, NSString *errorCode, NSString *message) {
        //[self hideMBProcess];//bbbbbb
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if (errorCode.length < 1) {
            errorCode=@"<null>";
        }
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        arrPromotion = result;
        
        if (arrPromotion.count <= 0 || [Id isEqualToString:@"0"]) {
            selectedPromotion = [[PromotionModel alloc] initWithName:@"0" description:@"[Chọn câu lệnh khuyến mãi]" Amount:@"0" Type:@""];
            [self.btnPromotion setTitle:selectedPromotion.PromotionName forState:UIControlStateNormal];
        }
        
        if (arrPromotion.count > 0 && ![Id isEqualToString:@"0"]) {
            for (selectedPromotion in arrPromotion) {
                if([Id isEqualToString:selectedPromotion.PromotionId]){
                    if ([selectedPromotion.PromotionName isKindOfClass:[NSString class]]) {
                        [self.btnPromotion setTitle:selectedPromotion.PromotionName forState:UIControlStateNormal];
                        break;
                        
                    }else {
                        [self showAlertBox:@"Thông Báo" message:@"Dữ liệu không đúng"];
                        break ;
                    }
                }
            }
        }
        [self setFrameButton:self.btnPromotion withTitle:selectedPromotion.PromotionName];
        [self calculatedMoney];
        
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetListPromotion",[error localizedDescription]]];
    }];
    
}

/* load list deposite black point
 *
 */
- (void)loadDepositBlackPoint:(NSString *)ID
{
    arrDepositBlackPoint = [Common getDepositBlackPoint];
    for ( selectedDepositBlackPoint in arrDepositBlackPoint ) {
        if([ID isEqualToString:selectedDepositBlackPoint.Key]){
            [self.btnDepositBlackPoint setTitle:selectedDepositBlackPoint.Values forState:UIControlStateNormal];
            return;
        }
    }
    
}

/*
 * load list deposit contract
 */
- (void)loadDepositContract:(NSString *)ID
{
    
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    ShareData *shared = [ShareData instance];
    if (self.regcode.length <= 0) {
        self.regcode = @"";
    }
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    [dict setObject:record.RegCode forKey:@"RegCode"];
    
    [shared.registrationFormProxy getContractDepositList:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if (errorCode.length < 1) {
            errorCode=@"<null>";
        }
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        arrDepositContract = result;
        
        for ( selectedDepositContract in arrDepositContract ) {
            if([ID isEqualToString:selectedDepositContract.Key]){
                [self.btnDepositContract setTitle:selectedDepositContract.Values forState:UIControlStateNormal];
                return;
            }
        }
                
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetContractDepositList",[error localizedDescription]]];
    }];
}

/*
 * calculated new money total
 */

- (void)calculatedMoney
{
    
    newMoneyTotal   = 0;
    MoneyDifference = 0;
    NSInteger totalOffice365;
    totalOffice365 = record.office365Total ;
    newMoneyTotal   = [selectedPromotion.Amount integerValue] + [selectedDepositBlackPoint.Key integerValue] + [selectedDepositContract.Key integerValue] + [record.IPTVTotal integerValue] + totalOffice365  +[record.DeviceTotal integerValue];
    
    MoneyDifference = newMoneyTotal - [record.Total integerValue];
    
    self.txtNewMoneyTotal.text = StringFormat(@"%@ VNĐ", [nf stringFromNumber:[NSNumber numberWithFloat:newMoneyTotal]]);
    
    self.lblMoneyDifference.text = StringFormat(@"%@ VNĐ", [nf stringFromNumber:[NSNumber numberWithFloat:MoneyDifference]]);
    
}

/*
 * update receipt
 */
- (void)checkUpdateReceipt
{
    if (MoneyDifference > 0) {
        [self showAlertViewWithTitle:@"Thông báo" message:@"Số tiền cập nhật lớn hơn số tiền hiện tại trên Phiếu đăng ký. Hệ thống sẽ gửi SMS tới khách hàng. \nBạn có muốn cập nhật?" tag:CheckUpdate];
        return;
    }
    [self updateReceipt];
}

/*
 * update receipt
 */
- (void)updateReceipt
{
    
    ShareData *shared = [ShareData instance];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];

    @try {
        [dict setObject:shared.currentUser.userName forKey:@"UserName"];
        [dict setObject:record.RegCode forKey:@"RegCode"];
        [dict setObject:selectedServicePackage.Key forKey:@"NewLocalType"];
        [dict setObject:selectedPromotion.PromotionId forKey:@"NewPromotionID"];
        [dict setObject:selectedDepositBlackPoint.Key forKey:@"DepositBlackPoint"];
        [dict setObject:selectedDepositContract.Key forKey:@"DepositContract"];
        [dict setObject:StringFormat(@"%li",(long)newMoneyTotal) forKey:@"Total"];
    }
    
    @catch (NSException *exception) {
        NSLog(@"update Receipt error: %@",[exception description]);
         [CrashlyticsKit recordCustomExceptionName:@"UpdateReceiptViewController - funtion - Error updateReceipt" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
    }
    
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    [self showMBProcess];
    
    [shared.registrationFormProxy updateReceiptInternet:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        NSInteger ID = [errorCode integerValue];
        if (ID > 0) {
            [self showAlertViewWithTitle:@"Thông báo" message:message tag:UpdateSuccess];
        } else {
            [self showAlertViewWithTitle:@"Thông báo" message:message tag:UpdateError];
        }
        
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetListPromotion",[error localizedDescription]]];
    }];
}

/*
 * handle hide list drop down view when tap on screen
 */
- (void)handleSingleTap:(UITapGestureRecognizer *) sender
{
    [self.view endEditing:YES];
    if (dropDown !=nil) {
        [dropDown hideDropDown:btnClicked];
        [self rel];
    }
}

/*
 * get data for drop down view
 */
- (NSMutableArray *)pareArrayForKeyWith:(NSArray*)arrInput button:(UIButton *)btn
{
    
    NSMutableArray *data = [[NSMutableArray alloc] init];
    
    if (btn == self.btnPromotion) {
        for (PromotionModel *temp in arrInput) {
            [data addObject:temp.PromotionName];
        }
        return data;
    }
    for (KeyValueModel *temp in arrInput) {
        [data addObject:temp.Values];
    }
    return data;

}

/*
 * show drop down view
 */
- (void)showDropDownView:(UIButton *)btn data:(NSArray *)data
{
    if (btn != btnClicked) {
        [dropDown hideDropDown:btn];
        [self rel];
        btnClicked = btn;
    }
    
    if (dropDown == nil) {
        CGFloat height = 239;
        CGFloat width = 0;
        if (data.count <= 7) {
            height = data.count*40;
        }
        dropDown = [[NIDropDown alloc] showDropDownAtButton:btn height:&height width:&width data:data images:nil animation:@"down"];
        dropDown.delegate = self;
        return;
    }
    
    [dropDown hideDropDown:btn];
    [self rel];
}

#pragma mark - NIDropDown Delegate method

- (void)dropDownListView:(NIDropDown *)dropDownListView didSelectIndexPath:(NSIndexPath *)indexPath atButton:(UIButton *)button
{
    
    if (button == self.btnServicePackage) {
        selectedServicePackage = arrServicePackage[indexPath.row];
        [self showMBProcess];
        [self loadPromotion:@"0" LocalType:selectedServicePackage.Key];
        return;
    }
    if (button == self.btnPromotion) {
        selectedPromotion = arrPromotion[indexPath.row];
        [self setFrameButton:button withTitle:selectedPromotion.PromotionName];
        [self calculatedMoney];
        return;
    }
    if (button == self.btnDepositBlackPoint) {
        selectedDepositBlackPoint = arrDepositBlackPoint[indexPath.row];
        selectedDepositContract = [arrDepositContract objectAtIndex:0];
        [self.btnDepositContract setTitle:selectedDepositContract.Values forState:UIControlStateNormal];
        [self calculatedMoney];
        return;
    }
    if (button == self.btnDepositContract) {
        selectedDepositContract = arrDepositContract[indexPath.row];
        selectedDepositBlackPoint = [arrDepositBlackPoint objectAtIndex:0];
        [self.btnDepositBlackPoint setTitle:selectedDepositBlackPoint.Values forState:UIControlStateNormal];
        [self calculatedMoney];
        return;
    }
}

- (void)niDropDownDelegateMethod:(NIDropDown *)sender
{
    [self rel];
}

- (void)rel {
    dropDown = nil;
}

#pragma mark - Implement alert View 

- (void) showAlertViewWithTitle:(NSString *)title message:(NSString *)message tag:(NSInteger)tag
{
    UIAlertView *alertView;
    
    switch (tag) {
        case UpdateSuccess:
        case UpdateError:
            alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            break;
        case CheckUpdate:
            alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"Có" otherButtonTitles:@"Không", nil];
            break;
        default:
            break;
    }
    alertView.tag = tag;
    [alertView show];
}

// UIAlert View Delegate method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        switch (alertView.tag) {
            case CheckUpdate:
                [self updateReceipt];
                break;
            case UpdateSuccess:
                [self.navigationController popViewControllerAnimated:YES];
                break;
            default:
                break;
        }
    }
   
}

/*
 * set height for button when title of button changed
 */

- (void)setFrameButton:(UIButton *)button withTitle:(NSString *)title
{
    button.translatesAutoresizingMaskIntoConstraints = YES;
    CGSize constrant = CGSizeMake(button.frame.size.width, 2000.0f);
    CGFloat height = [title getStringHeightWithFontSize:15 withSizeMake:constrant];
    //or whatever font you're using
    CGRect btnFrame = button.frame;
    btnFrame.size.height = height;
    [button setFrame:btnFrame];
    [UIView animateWithDuration:0.25
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];}

@end
