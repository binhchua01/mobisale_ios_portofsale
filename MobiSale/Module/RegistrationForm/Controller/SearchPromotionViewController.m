//
//  SearchPromotionViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 4/4/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "SearchPromotionViewController.h"
#import "SiUtils.h"

@interface SearchPromotionViewController () <UIScrollViewDelegate>

@end

@implementation SearchPromotionViewController {
    NSArray *arrSearchResult;
    PromotionModel *selectedPromotion;
    CGFloat _currentKeyboardHeight;
}

@synthesize arrData, promotionModel;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.lblTitle.text = @"CHỌN CLKM INTERNET";
    arrSearchResult = arrData;
    [self.resultTableView reloadData];
    [self.searchBar becomeFirstResponder];
    [self.searchBar setValue:@"Đóng" forKey:@"_cancelButtonText"];
    
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.view.frame = CGRectMake(0,30,[SiUtils getScreenFrameSize].width,[SiUtils getScreenFrameSize].height-30);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrSearchResult.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dataForRowAtIndexPath = arrSearchResult[indexPath.row];
    
    NSString *text = [dataForRowAtIndexPath objectForKey:@"DisplayText"];
    CGSize constraint = CGSizeMake(210, 20000.0f);
    
    // constratins the size of the table row according to the text
    CGRect textRect = [text boundingRectWithSize:constraint options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Light" size:14]} context:nil];
    
    CGFloat height = MAX(textRect.size.height,49);
    
    return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSDictionary *dataForRowAtIndexPath = arrSearchResult[indexPath.row];
    [cell setBackgroundColor:[UIColor clearColor]];
    [cell.textLabel setNumberOfLines:0];
    [cell.textLabel sizeToFit];
    [cell.textLabel setText:[dataForRowAtIndexPath objectForKey:@"DisplayText"]];
    [cell.textLabel setTextColor:[UIColor blackColor]];
    [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica-Light" size:14]];
    
    return cell;
}

#pragma mark - UITableViewDelegate 

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dataForRowAtIndexPath = arrSearchResult[indexPath.row];
    selectedPromotion = [dataForRowAtIndexPath objectForKey:@"PromotionModel"];
    
    if (self.delegate) {
        [self.delegate cancelSearchPromotionView:selectedPromotion];
    }
}

#pragma mark -  Implement our UISearchBarDelegate methods

- (NSArray *)filterContentForSearchText:(NSString *)searchText {
    
    NSArray *arrStrFilter = [searchText componentsSeparatedByString:@"*"];
    NSArray *arrFiltered;
    NSPredicate *predicate;
    NSMutableArray *arrPredicate = [NSMutableArray array];
    
    if (arrStrFilter.count > 1) {
        for (NSString *strFilter in arrStrFilter) {
            if (![strFilter isEqualToString: @""]) {
                
                predicate = [NSPredicate predicateWithFormat:@"DisplayText CONTAINS[cd] %@",strFilter];
                
                [arrPredicate addObject:predicate];
                
            } else {
                break;
            }
        }
        
        predicate = [NSCompoundPredicate andPredicateWithSubpredicates:arrPredicate];
        
        arrFiltered = [NSArray arrayWithArray:[arrData filteredArrayUsingPredicate:predicate]];

        return arrFiltered;
    }
    predicate = [NSPredicate predicateWithFormat:@"DisplayText CONTAINS[cd] %@", searchText];
    arrFiltered = [NSArray arrayWithArray:[arrData filteredArrayUsingPredicate:predicate]];
    return arrFiltered;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (searchText.length > 0) {
        arrSearchResult =  [self filterContentForSearchText:searchText];
        
    } else {
        arrSearchResult = arrData;
    }
    
    [self.resultTableView reloadData];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = NO;
    searchBar.text = @"";
    [searchBar resignFirstResponder];
    
    arrSearchResult = arrData;

    [self.resultTableView reloadData];

}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = NO;
    [searchBar resignFirstResponder];
}

- (IBAction)btnCancel_Clicked:(id)sender {
    if (self.delegate) {
        
        if (promotionModel.PromotionId == nil ) {
            
        }
        
        if (![promotionModel.PromotionId isEqualToString:@"0"]) {
            selectedPromotion = promotionModel;
            
        } else {
            selectedPromotion = [[PromotionModel alloc] initWithName:@"0" description:@"[Chọn câu lệnh khuyến mãi]" Amount:@"0" Type:@""];
        }
        
        [self.delegate cancelSearchPromotionView:selectedPromotion];
    }
}

#pragma mark - Scroll view delegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    self.searchBar.showsCancelButton = NO;
    [self.searchBar resignFirstResponder];
}

#pragma mark - Implement keyboardWillShow to react to the current change in keyboard height
- (void)keyboardWillShow:(NSNotification*)notification {
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    // Write code to adjust views accordingly using deltaHeight
    _currentKeyboardHeight = kbSize.height;
    
    [self setFrameWithHeightKeyBoard:YES];

}

- (void)keyboardWillHide:(NSNotification*)notification {
    // Write code to adjust views accordingly using kbSize.height
    //_currentKeyboardHeight = 0.0f;
    
    [self setFrameWithHeightKeyBoard:NO];

}

- (void)setFrameWithHeightKeyBoard:(BOOL)isShow {

    if (isShow == YES) {
        self.constraintBottomTableView.constant += _currentKeyboardHeight;
        
    } else {
        self.constraintBottomTableView.constant = 0;

    }
    
    [UIView commitAnimations];

}

@end
