//
//  ListRegistrationFormPageViewController.m
//  MobiSale
//
//  Created by ISC on 8/23/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

/*
 Phân loại PĐK theo từng tab:
  - Danh sách PĐK dành cho khách hàng mới.
  - Danh sách PĐK bán thêm dịch vụ cho khách hàng cũ.
  - Danh sách PĐK đã lên hợp đồng chưa duyệt: bao gồm PĐK bán mới và bán cũ đã thu tiền.
 */

static NSString *PhieuDangKyMoi = @"PHIẾU ĐĂNG KÝ MỚI";
static NSString *PhieuDangKyBanThem = @"PHIẾU ĐĂNG KÝ BÁN THÊM";
static NSString *PhieuDangKyChuaDuyet = @"PĐK CHƯA ĐƯỢC DUYỆT";

#import "ListRegistrationFormPageViewController.h"
#import "ListRegisteredFormViewController.h"

@interface ListRegistrationFormPageViewController ()<ViewPagerDelegate, ViewPagerDataSource>

@end

@implementation ListRegistrationFormPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.delegate = self;
    self.dataSource = self;
    self.title = @"DANH SÁCH PĐK";
    // add right bar button
    self.navigationItem.rightBarButtonItem = [self rightBarButtonItem];
    /*
     *  Set data for tab
     */
    
    KeyValueModel *item1 = [[KeyValueModel alloc] initWithName:@"0" description:PhieuDangKyMoi];
    KeyValueModel *item2 = [[KeyValueModel alloc] initWithName:@"1" description:PhieuDangKyChuaDuyet];
    KeyValueModel *item3 = [[KeyValueModel alloc] initWithName:@"2" description:PhieuDangKyBanThem];
    self.tabArray = [NSMutableArray arrayWithObjects:item1, item3, item2, nil];
    
    [self selectTabAtIndex:0];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

#pragma mark - ViewPagerDataSource methods
- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager {
    return self.tabArray.count;
}

- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index {
    KeyValueModel *item = self.tabArray[index];
    // Create Title label
    UILabel *titleLabel = [UILabel new];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont systemFontOfSize:14.0];
    titleLabel.text = item.Values;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.numberOfLines = 0;
    [titleLabel sizeToFit];
    
    return titleLabel;
    
}

- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index {
    
    KeyValueModel *item = self.tabArray[index];
    NSString *nibName = @"ListRegisteredFormController";
    ListRegisteredFormController *vc = [[ListRegisteredFormController alloc] initWithNibName:nibName bundle:nil];
    vc.viewType = item;
    vc.title = @"DANH SÁCH PĐK";

    return vc;
}

- (CGFloat)viewPager:(ViewPagerController *)viewPager valueForOption:(ViewPagerOption)option withDefault:(CGFloat)value {
    switch (option) {
        case ViewPagerOptionStartFromSecondTab:
            return 0.0;
        case ViewPagerOptionCenterCurrentTab:
            return 1.0;
        case ViewPagerOptionTabLocation:
            return 1.0;
        case ViewPagerOptionTabHeight:
            return 39.0;
        case ViewPagerOptionTabOffset:
            return 36.0;
        case ViewPagerOptionTabWidth:
            return 200.0;
        case ViewPagerOptionFixFormerTabsPositions:
            return 1.0;
        case ViewPagerOptionFixLatterTabsPositions:
            return 1.0;
        default:
            return value;
    }
}

- (UIColor *)viewPager:(ViewPagerController *)viewPager colorForComponent:(ViewPagerComponent)component withDefault:(UIColor *)color {
    switch (component) {
        case ViewPagerIndicator:
            return [UIColor orangeColor];
        case ViewPagerTabsView:
            return [[UIColor colorWithRed:0.133 green:0.698 blue:0.569 alpha:1] colorWithAlphaComponent:1.0];
        case ViewPagerContent:
            return [UIColor whiteColor];
        default:
            return color;
    }
}

#pragma mark - ViewPagerDelegate methods
- (void)viewPager:(ViewPagerController *)viewPager didChangeTabToIndex:(NSUInteger)index {
    self.tabCurrentView = self.tabArray[index];
}

#pragma mark - Set right button bar

- (UIBarButtonItem *)rightBarButtonItem
{
    UIImage *image = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"ic_create_potentail_obj"] height:30];
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered target:self action:@selector(rightButtonPressed:)];
    return rightBarButton;
}

- (void)rightButtonPressed:(id)sender
{
    if ([self.tabCurrentView.Key isEqual:@"0"]) {
       
        ListRegisteredFormViewController *vc = [[ListRegisteredFormViewController alloc] initWithNibName:@"ListRegisteredFormViewController" bundle:nil];
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
    
    if ([self.tabCurrentView.Key isEqual:@"1"] || [self.tabCurrentView.Key isEqual:@"2"]) {
        SaleMoreServiceViewController *vc = [[SaleMoreServiceViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
}

@end
