//
//  RegisteredFormController.h
//  MobiSales
//
//  Created by HIEUPC on 1/22/15.
//  Copyright (c) 2015 Aryan Ghassemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TDDatePicker.h"
#import "RadioButton.h"
#import "UIPopoverListView.h"
#import "RegistrationFormDetailRecord.h"
#import "MPGTextField.h"
#import "Office365ViewController.h"

@class UICheckbox;

@interface RegisteredFormController : TDDatePicker<UIPopoverListViewDataSource, UIPopoverListViewDelegate, MPGTextFieldDelegate, UITextFieldDelegate, UIActionSheetDelegate, Office365ViewControllerDelegate>

@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
// Office 365
@property (weak, nonatomic) IBOutlet UIButton *office365Button;

@property (strong, nonatomic) IBOutlet RadioButton *rdYesRequest;
@property (strong, nonatomic) IBOutlet RadioButton *rdNoRequest;
@property (strong, nonatomic) IBOutlet RadioButton *rdYesWallDrilling;
@property (strong, nonatomic) IBOutlet RadioButton *rdNoWallDrilling;
@property (strong, nonatomic) IBOutlet RadioButton *rdYesCableStatus;
@property (strong, nonatomic) IBOutlet RadioButton *rdNoCableStatus;
@property (strong, nonatomic) IBOutlet RadioButton *rdYesInsCable;
@property (strong, nonatomic) IBOutlet RadioButton *rdNoInsCable;

@property (weak, nonatomic) IBOutlet UIButton *cbFimPlus;
@property (weak, nonatomic) IBOutlet UIButton *cbFimHot;
@property (assign, nonatomic) IBOutlet UIButton *cbKPlus;
@property (assign, nonatomic) IBOutlet UIButton *cbVTC;
@property (assign, nonatomic) IBOutlet UIButton *cbVTV;

@property (strong, nonatomic) IBOutlet UITextField *txtFullName;
@property (strong, nonatomic) IBOutlet UITextField *txtCMND;
@property (strong, nonatomic) IBOutlet UITextField *txtMST;
@property (strong, nonatomic) IBOutlet UITextField *txtPhone1;
@property (strong, nonatomic) IBOutlet UITextField *txtPersonContact1;
@property (strong, nonatomic) IBOutlet UITextField *txtPhone2;
@property (strong, nonatomic) IBOutlet UITextField *txtPersonContact2;
@property (strong, nonatomic) IBOutlet UITextField *txtPersonContact;
@property (strong, nonatomic) IBOutlet UITextField *txtBilltoNumber;
@property (strong, nonatomic) IBOutlet UITextField *txtLot;
@property (strong, nonatomic) IBOutlet UITextField *txtFloor;
@property (strong, nonatomic) IBOutlet UITextField *txtRoom;
@property (strong, nonatomic) IBOutlet UITextField *txtDeviceBoxCount;
@property (strong, nonatomic) IBOutlet UITextField *txtDevicePLCCount;
@property (strong, nonatomic) IBOutlet UITextField *txtDeviceSTBCount;
@property (strong, nonatomic) IBOutlet UITextField *txtIPTVChargeTimes;
@property (strong, nonatomic) IBOutlet UITextField *txtIPTVPrepaid;

// Fim +
@property (weak, nonatomic) IBOutlet UITextField *txtIPTVFimPlusChargeTimes;
@property (weak, nonatomic) IBOutlet UITextField *txtIPTVFimPlusPrepaidMonth;

// Fim Hot
@property (weak, nonatomic) IBOutlet UITextField *txtIPTVFimHotChargeTimes;
@property (weak, nonatomic) IBOutlet UITextField *txtIPTVFimHotPrepaidMonth;

// K +
@property (strong, nonatomic) IBOutlet UITextField *txtIPTVKPlusChargeTimes;
@property (strong, nonatomic) IBOutlet UITextField *txtIPTVKPlusPrepaidMonth;

// Đặc sắc HD
@property (strong, nonatomic) IBOutlet UITextField *txtIPTVVTCChargeTimes;
@property (strong, nonatomic) IBOutlet UITextField *txtIPTVVTCPrepaidMonth;

// VTV Cab
@property (strong, nonatomic) IBOutlet UITextField *txtIPTVVTVChargeTimes;
@property (strong, nonatomic) IBOutlet UITextField *txtIPTVVTVPrepaidMonth;

@property (weak, nonatomic) IBOutlet UITextField *txtIPTVMonthlyTotal;   // cước sử dụng IPTV tháng sau.
@property (strong, nonatomic) IBOutlet UITextField *txtIPTVTotal;
@property (strong, nonatomic) IBOutlet UITextField *txtInternetTotal;
@property (strong, nonatomic) IBOutlet UITextField *txtTotal;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;

@property (strong, nonatomic) IBOutlet MPGTextField *textFieldPromotionInternet;

@property (strong, nonatomic) IBOutlet UITextView *txtNote;
@property (strong, nonatomic) IBOutlet UITextView *txtDescriptionIBB;
@property (strong, nonatomic) IBOutlet UITextView *tvPromotionInternet;
@property (strong, nonatomic) IBOutlet UITextView *txtPromotionInternet;
@property (strong, nonatomic) IBOutlet UITextView *tvPromotionIPTV; // CLKM Box đầu tiên
@property (weak, nonatomic) IBOutlet UITextView *tvPromotionIPTVBoxOrder; // CLKM Box thứ 2 trở đi 

@property (strong, nonatomic) IBOutlet UITextField *txtIndoor;
@property (strong, nonatomic) IBOutlet UITextField *txtOutdoor;
@property (strong, nonatomic) IBOutlet UITextField *txtAddressIdentifyCard;

@property (strong, nonatomic) IBOutlet UIButton *btnPaymentType;
@property (strong, nonatomic) IBOutlet UIButton *btnBirthday;
@property (strong, nonatomic) IBOutlet UIButton *btnUpdate;
@property (strong, nonatomic) IBOutlet UIButton *btnPhone1;
@property (strong, nonatomic) IBOutlet UIButton *btnPhone2;
@property (strong, nonatomic) IBOutlet UIButton *btnObjectCustomer;
@property (strong, nonatomic) IBOutlet UIButton *btnISP;
@property (strong, nonatomic) IBOutlet UIButton *btnTypeCustomer;
@property (strong, nonatomic) IBOutlet UIButton *btnPlaceCurrent;
@property (strong, nonatomic) IBOutlet UIButton *btnSolvency;
@property (strong, nonatomic) IBOutlet UIButton *btnClientPartners;
@property (strong, nonatomic) IBOutlet UIButton *btnLegal;
@property (strong, nonatomic) IBOutlet UIButton *btnDistrict;
@property (strong, nonatomic) IBOutlet UIButton *btnWard;
@property (strong, nonatomic) IBOutlet UIButton *btnTypeHouse;
@property (strong, nonatomic) IBOutlet UIButton *btnStreet;
@property (strong, nonatomic) IBOutlet UIButton *btnServicePack;
@property (strong, nonatomic) IBOutlet UIButton *btnDeployment;
@property (strong, nonatomic) IBOutlet UIButton *btnPromotion;
@property (strong, nonatomic) IBOutlet UIButton *btnNameVilla;
@property (strong, nonatomic) IBOutlet UIButton *btnPosition;
@property (strong, nonatomic) IBOutlet UIButton *btnDeviceBoxCountPlus;
@property (strong, nonatomic) IBOutlet UIButton *btnDeviceBoxCountMinus;
@property (strong, nonatomic) IBOutlet UIButton *btnDevicePLCCountPlus;
@property (strong, nonatomic) IBOutlet UIButton *btnDevicePLCCountMinus;
@property (strong, nonatomic) IBOutlet UIButton *btnDeviceSTBCountPlus;
@property (strong, nonatomic) IBOutlet UIButton *btnDeviceSTBCountMinus;
@property (strong, nonatomic) IBOutlet UIButton *btnIPTVChargeTimesPlus;
@property (strong, nonatomic) IBOutlet UIButton *btnIPTVChargeTimesMinus;

// Fim +
@property (weak, nonatomic) IBOutlet UIButton *btnIPTVFimPlusChargeTimesPlus;
@property (weak, nonatomic) IBOutlet UIButton *btnIPTVFimPlusChargeTimesMinus;
@property (weak, nonatomic) IBOutlet UIButton *btnIPTVFimPlusPrepaidMonthPlus;
@property (weak, nonatomic) IBOutlet UIButton *btnIPTVFimPlusPrepaidMonthMinus;

// Fim Hot
@property (weak, nonatomic) IBOutlet UIButton *btnIPTVFimHotPrepaidMonthPlus;
@property (weak, nonatomic) IBOutlet UIButton *btnIPTVFimHotPrepaidMonthMinus;
@property (weak, nonatomic) IBOutlet UIButton *btnIPTVFimHotChargeTimesMinus;
@property (weak, nonatomic) IBOutlet UIButton *btnIPTVFimHotChargeTimesPlus;

// K +
@property (strong, nonatomic) IBOutlet UIButton *btnIPTVKPlusChargeTimesPlus;
@property (strong, nonatomic) IBOutlet UIButton *btnIPTVKPlusChargeTimesMinus;
@property (strong, nonatomic) IBOutlet UIButton *btnIPTVKPlusPrepaidMonthPlus;
@property (strong, nonatomic) IBOutlet UIButton *btnIPTVKPlusPrepaidMonthMinus;

// Đặc sắc HD
@property (strong, nonatomic) IBOutlet UIButton *btnIPTVVTCChargeTimesPlus;
@property (strong, nonatomic) IBOutlet UIButton *btnIPTVVTCChargeTimesMinus;
@property (strong, nonatomic) IBOutlet UIButton *btnIPTVVTCPrepaidMonthPlus;
@property (strong, nonatomic) IBOutlet UIButton *btnIPTVVTCPrepaidMonthMinus;

// VTV Cab
@property (strong, nonatomic) IBOutlet UIButton *btnIPTVVTVChargeTimesPlus;
@property (strong, nonatomic) IBOutlet UIButton *btnIPTVVTVChargeTimesMinus;
@property (strong, nonatomic) IBOutlet UIButton *btnIPTVVTVPrepaidMonthPlus;
@property (strong, nonatomic) IBOutlet UIButton *btnIPTVVTVPrepaidMonthMinus;

@property (strong, nonatomic) IBOutlet UIButton *btnDepositBlackPoint;
@property (strong, nonatomic) IBOutlet UIButton *btnDeposit;
@property (strong, nonatomic) IBOutlet UIButton *btnCusType;
@property (strong, nonatomic) IBOutlet UIButton *btnLocalType;
@property (strong, nonatomic) IBOutlet UIButton *btnIPTVPackage;
@property (strong, nonatomic) IBOutlet UIButton *btnIPTVPromotionID;    // Khuyến mãi Box đầu tiên
@property (weak, nonatomic) IBOutlet UIButton *btnIPTVPromotionIDBoxOrder; // Khuyến mãi từ Box thứ 2 trở đi
@property (strong, nonatomic) IBOutlet UIButton *btnCombo;
@property (strong, nonatomic) IBOutlet UIButton *btnGetTotalIPTV;
@property (strong, nonatomic) IBOutlet UIButton *btnSuggestionSearch;
@property (strong, nonatomic) IBOutlet UIButton *btnSuggestionAddress;

@property (weak, nonatomic) IBOutlet UILabel *lblIPTVPromotionID; // Tiền CLKM Box đầu tiên
@property (weak, nonatomic) IBOutlet UILabel *lblIPTVPromotionIDBoxOrder; // Tiền CLKM Box thứ 2 trở đi
@property (strong, nonatomic) IBOutlet UILabel *lblPositionHouse;
@property (strong, nonatomic) IBOutlet UILabel *lblNumberHouse;
@property (strong, nonatomic) IBOutlet UILabel *lblLot;
@property (strong, nonatomic) IBOutlet UILabel *lblFloor;
@property (strong, nonatomic) IBOutlet UILabel *lblRoom;
@property (strong, nonatomic) IBOutlet UILabel *lblNameVilla;
@property (weak, nonatomic) IBOutlet UILabel *lblFimPlusPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblFimHotPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblKPlusPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblDacSacHDPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblVTVCabPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblIPTVMonthlyTotal;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintPositionHouse;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintNumberHouse;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintLot;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintNote;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopAmountTotal;
// Đăng ký Office 365
@property (weak, nonatomic) IBOutlet UISwitch *registedOffice365Switch;
@property (weak, nonatomic) IBOutlet UITextField *office365Amount;
- (IBAction)registedOffice365ChangeValue:(UISwitch *)sender;
- (IBAction)office365ButtonPressed:(id)sender;
// Đăng ký OTT
@property (weak, nonatomic) IBOutlet UISwitch *registedOTT;
@property (weak, nonatomic) IBOutlet UIView *viewOTT;
@property (weak, nonatomic) IBOutlet UIButton *btnMaxBoxOTT;
@property (weak, nonatomic) IBOutlet UIButton *btnMinBoxOTT;
@property (weak, nonatomic) IBOutlet UITextField *txtOTTTotal;
@property (weak, nonatomic) IBOutlet UITextField *txtOTTBoxCount;
@property (nonatomic) NSInteger minimum;
@property (nonatomic) NSInteger maximum;
@property (nonatomic) NSInteger count;


- (IBAction)btnBirthday_Clicked:(id)sender;
- (IBAction)btnUpdate_clicked:(id)sender;
- (IBAction)btnPhone1_clicked:(id)sender;
- (IBAction)btnPhone2_clicked:(id)sender;
- (IBAction)btnObjectCustomer_clicked:(id)sender;
- (IBAction)btnISP_clicked:(id)sender;
- (IBAction)btnTypeCustomer_clicked:(id)sender;
- (IBAction)btnPlaceCurrent_clicked:(id)sender;
- (IBAction)btnSolvency_clicked:(id)sender;
- (IBAction)btnClientPartners_clicked:(id)sender;
- (IBAction)btnLegal_clicked:(id)sender;
- (IBAction)btnDistrict_clicked:(id)sender;
- (IBAction)btnWard_clicked:(id)sender;
- (IBAction)btnTypeHouse_clicked:(id)sender;
- (IBAction)btnStreet_clicked:(id)sender;
- (IBAction)btnServicePack_clicked:(id)sender;
- (IBAction)btnDeployment_clicked:(id)sender;
- (IBAction)btnPromotion_cliked:(id)sender;
- (IBAction)btnNameVilla_cliked:(id)sender;
- (IBAction)btnPosition_cliked:(id)sender;
- (IBAction)btnMinus_cliked:(id)sender;
- (IBAction)btnPlus_cliked:(id)sender;
- (IBAction)btnDepositBlackPoint_cliked:(id)sender;
- (IBAction)btnDeposit_cliked:(id)sender;
- (IBAction) RadioButtonDeployment:(RadioButton*)sender;
- (IBAction) RadioButtonDrillWall:(RadioButton*)sender;
- (IBAction) RadioButtonCableStatus:(RadioButton*)sender;
- (IBAction) RadioButtonInsCable:(RadioButton*)sender;
- (IBAction)cbKPlus_Clicked:(UIButton *)sender;
- (IBAction)cbVTV_Clicked:(UIButton *)sender;
- (IBAction)cbVTC_Clicked:(UIButton *)sender;
- (IBAction)btnCusType_Clicked:(UIButton *)sender;
- (IBAction)btnLocalType_Clicked:(UIButton *)sender;
- (IBAction)btnIPTVPackage_Clicked:(UIButton *)sender;
- (IBAction)btnIPTVPromotionID_Clicked:(UIButton *)sender;
- (IBAction)btnGetTotalIPTV_Clicked:(UIButton *)sender;
- (IBAction)btnSuggestionSearch_Clicked:(id)sender;
- (IBAction)btnSuggestionAddress_Clicked:(id)sender;
- (IBAction)btnPaymentType_Clicked:(id)sender;
- (IBAction)btnRefreshPaymentType_Clicked:(id)sender;

@property (retain, nonatomic) NSMutableArray *arrPhone1;
@property (retain, nonatomic) NSMutableArray *arrPhone2;
@property (retain, nonatomic) NSMutableArray *arrObjectCustomer;
@property (retain, nonatomic) NSMutableArray *arrISP;
@property (retain, nonatomic) NSMutableArray *arrTypeCustomer;
@property (retain, nonatomic) NSMutableArray *arrPlaceCurrent;
@property (retain, nonatomic) NSMutableArray *arrSolvency;
@property (retain, nonatomic) NSMutableArray *arrClientPartners;
@property (retain, nonatomic) NSMutableArray *arrLegal;
@property (retain, nonatomic) NSMutableArray *arrDistrict;
@property (retain, nonatomic) NSMutableArray *arrWard;
@property (retain, nonatomic) NSMutableArray *arrTypeHouse;
@property (retain, nonatomic) NSMutableArray *arrStreet;
@property (retain, nonatomic) NSMutableArray *arrServicePack;
@property (retain, nonatomic) NSMutableArray *arrCombo;
@property (retain, nonatomic) NSMutableArray *arrDeployment;
@property (retain, nonatomic) NSMutableArray *arrDeposit;
@property (retain, nonatomic) NSMutableArray *arrPromotion;
@property (retain, nonatomic) NSMutableArray *arrBonusBox;
@property (retain, nonatomic) NSMutableArray *arrNameVilla;
@property (retain, nonatomic) NSMutableArray *arrPosition;
@property (retain, nonatomic) NSMutableArray *arrDepositBlackPoint;
@property (retain, nonatomic) NSMutableArray *arrCusType;
@property (retain, nonatomic) NSMutableArray *arrPromotionIPTV;
@property (retain, nonatomic) NSMutableArray *arrPromotionIPTVBoxOrder;
@property (retain, nonatomic) NSMutableArray *arrPackageIPTV;
@property (retain, nonatomic) NSMutableArray *arrLocalType;
@property (retain, nonatomic) NSMutableArray *arrPaymentType;
@property (retain, nonatomic) NSArray *arrayListPacketOld;
@property (strong, nonatomic) NSString *Id;
@property (strong, nonatomic) RegistrationFormDetailRecord *rc;

@end
