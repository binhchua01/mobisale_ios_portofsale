//
//  SuggestionsSearchViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 3/11/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "SuggestionsSearchViewController.h"
#import "SiUtils.h"

@interface SuggestionsSearchViewController ()

@end

@implementation SuggestionsSearchViewController

@synthesize webViewContent;

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.type == suggestionsSearchPromotionStatement) {
        self.lblTitle.text = @"CÚ PHÁP TÌM KIẾM CLKM";
        [self loadWebViewContent:@"keyword"];
        return;
    }
    if (self.type == SuggestionsFillHouseNumber) {
        self.lblTitle.text = @"QUY ĐỊNH NHẬP ĐỊA CHỈ";
        [self loadWebViewContent:@"house_number_format"];
        return;
    }
    // Do any additional setup after loading the view from its nib.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.view.frame = CGRectMake(0,62,[SiUtils getScreenFrameSize].width,[SiUtils getScreenFrameSize].height - 62);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)loadWebViewContent:(NSString *)nameFileHTML {
    NSString *htmlFile   = [[NSBundle mainBundle] pathForResource:nameFileHTML ofType:@"html"];
    NSString* htmlString = [NSString stringWithContentsOfFile:htmlFile encoding:NSUTF8StringEncoding error:nil];
    [webViewContent loadHTMLString:htmlString baseURL:nil];
}

- (IBAction)btnBack_Clicked:(id)sender {
    if (self.delegate) {
        [self.delegate CancelPopupSuggestions];
    }
}
@end
