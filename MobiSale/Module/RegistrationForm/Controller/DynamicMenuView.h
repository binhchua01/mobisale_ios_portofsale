//
//  DynamicMenuView.h
//  MobiSale
//
//  Created by Nguyen Sang on 7/11/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DynamicMenuCell.h"

@protocol DynamicMenuViewDelegate <NSObject>

-(void)pressedButtonDynamicInMenu:(SNDynamicMenuView)dynamic;

@end

@interface DynamicMenuView : UIView<UITableViewDataSource, UITableViewDelegate, DynamicMenuCellDelegate>

@property (retain ,nonatomic) id<DynamicMenuViewDelegate> delegate;

@property (strong, nonatomic) IBOutlet UITableView *myTableView;

-(void)configView:(NSMutableArray *)arrDynamicEnum;

@end
