//
//  ConstructionVoteDetailModel.h
//  MobiSale
//
//  Created by ISC on 6/24/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConstructionVoteDetailModel : NSObject

@property (nonatomic, assign) NSInteger PTCId;          // ID Phiếu thi công (PTC)
@property (nonatomic, assign) NSInteger PTCStatus;      // Tình trạng (PTC)
@property (nonatomic, copy)   NSString  *createDate;    // Ngày tạo PTC
@property (nonatomic, copy)   NSString  *createBy;      // Người tạo PTC
@property (nonatomic, copy)   NSString  *updateDate;    // Ngày cập nhật PTC
@property (nonatomic, copy)   NSString  *updateBy;      // Người cập nhật PTC
@property (nonatomic, copy)   NSString  *PTCStatusName; // Tên tình trạng PTC
@property (nonatomic, copy)   NSString  *staffName;     // Tên nhân viên/ tổ thi công
@property (nonatomic, copy)   NSString  *staffEmail;    // Địa chỉ nhân viên/ tổ thi công
@property (nonatomic, copy)   NSString  *staffPhone;    // Số điện thoại nhân viên/ tổ thi công
@property (nonatomic, copy)   NSString  *contract;      // Số hợp đồng
@property (nonatomic, copy)   NSString  *fullName;      // Họ tên khách hàng
@property (nonatomic, copy)   NSString  *address;       // Địa chỉ khách hàng
@property (nonatomic, copy)   NSString  *email;         // Địa chỉ khách hàng
@property (nonatomic, copy)   NSString  *customerPhone; // Số điện thoại khách hàng

+ (id)constructionVoteDetailModelWithDict:(NSDictionary *)dict;

@end
