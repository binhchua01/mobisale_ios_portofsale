//
//  ConstructionVoteDetailModel.m
//  MobiSale
//
//  Created by ISC on 6/24/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ConstructionVoteDetailModel.h"

@implementation ConstructionVoteDetailModel

+ (id)constructionVoteDetailModelWithDict:(NSDictionary *)dict {
    ConstructionVoteDetailModel *model = [[ConstructionVoteDetailModel alloc] init];
    
    model.PTCId       = [dict[@"PTCId"] integerValue];
    model.PTCStatus   = [dict[@"PTCStatus"]   integerValue];
    model.createDate  = StringFormat(@"%@",dict[@"CreateDate"]);
    model.createBy    = StringFormat(@"%@",dict[@"CreateBy"]);
    model.updateDate  = StringFormat(@"%@",dict[@"UpdateDate"]);
    model.updateBy    = StringFormat(@"%@",dict[@"UpdateBy"]);
    model.PTCStatusName = StringFormat(@"%@",dict[@"PTCStatusName"]);
    model.staffName     = StringFormat(@"%@",dict[@"StaffName"]);
    model.staffEmail    = StringFormat(@"%@",dict[@"StaffEmail"]);
    model.staffPhone    = StringFormat(@"%@",dict[@"StaffPhone"]);
    model.contract      = StringFormat(@"%@",dict[@"Contract"]);
    model.fullName      = StringFormat(@"%@",dict[@"FullName"]);
    model.email      = StringFormat(@"%@",dict[@"Email"]);
    model.address       = StringFormat(@"%@",dict[@"Address"]);
    model.customerPhone    = StringFormat(@"%@",dict[@"CustomerPhone"]);
    
    return model;
}
@end
