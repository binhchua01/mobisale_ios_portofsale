//
//  ConstructionVoteDetailViewController.m
//  MobiSale
//
//  Created by ISC on 6/24/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ConstructionVoteDetailViewController.h"
#import "ShareData.h"
#import "ConstructionVoteDetailModel.h"

@interface ConstructionVoteDetailViewController ()<UIActionSheetDelegate, UIAlertViewDelegate>

@end

@implementation ConstructionVoteDetailViewController {
    NSArray *arrSection, *arrInfoCustomerRow, *arrConstructionVoteRow, *arrConstructionTeamRow;
    ConstructionVoteDetailModel *PTCModel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = self.screenName = @"THÔNG TIN PTC";
    
    // Set border and radius for button
    [self.btnChangePTCStatus styleButtonUpdate];
    [self.btnChangePTCStatus setShadow];
    
    arrSection = [NSArray arrayWithObjects:@"THÔNG TIN KHÁCH HÀNG", @"PHIẾU THI CÔNG", @"TỔ THI CÔNG", nil];
    arrInfoCustomerRow = [NSArray arrayWithObjects:@"", @"Số hợp đồng:", @"Điện thoại:", @"Địa chỉ:", nil];
    arrConstructionVoteRow = [NSArray arrayWithObjects:@"Tình trạng PTC:", @"Ngày tạo:", @"Người tạo:", nil];
    arrConstructionTeamRow = [NSArray arrayWithObjects:@"Tên tổ TC:", @"Email:", @"Điện thoại:", nil];
    
    [self getPTCDetail];
    // add refresh control when scroll top of tableview
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.detailTableView addSubview:refreshControl];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma refresh table view
- (void)refresh:(UIRefreshControl *)refreshControl {
    [self getPTCDetail];
    [refreshControl endRefreshing];
}


#pragma mark - UITableView datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return arrSection.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
           return  arrInfoCustomerRow.count;
        case 1:
            return arrConstructionVoteRow.count;
        case 2:
            return arrConstructionTeamRow.count;
        case 3:
            return 1;
        default:
            break;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (PTCModel == nil) {
        return 25;
    }
    
    CGSize constraint = CGSizeMake(tableView.contentSize.width, 20000.0f);
    
    NSString *text;
    
    UIFont *font = [UIFont fontWithName:@"Helvetica-Light" size:15];
    
    switch (indexPath.section) {
        case 0:
            switch (indexPath.row) {
                case 0:
                    text = PTCModel.fullName;
                    font = [UIFont fontWithName:@"Helvetica-Light" size:20];
                    break;
                    
                case 3:
                    text = PTCModel.address;
                    break;
                    
                default:
                    break;
            }
            
            break;
            
        case 1:
            switch (indexPath.row) {
                case 0:
                    text = PTCModel.PTCStatusName;
                    break;
                    
                case 1:
                    text = PTCModel.createDate;
                    break;
                    
                case 2:
                    text = PTCModel.createBy;
                    break;
                    
                default:
                    break;
            }
            
            break;
            
        case 2:
            switch (indexPath.row) {
                case 0:
                    text = PTCModel.staffName;
                    break;
                    
                case 1:
                    text = PTCModel.staffEmail;
                    break;
                    
                case 2:
                    text = PTCModel.staffPhone;
                    break;
                    
                default:
                    break;
            }
            
            break;
            
        default:
            break;
    }

    // constratins the size of the table row according to the text
    CGRect textRect = [text boundingRectWithSize:constraint options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{NSFontAttributeName:font} context:nil];
    
    CGFloat height = MAX(textRect.size.height,24);
    
    return 25 + height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 39;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return arrSection[section];
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor colorMain];
    
    UILabel *customLabel = [[UILabel alloc] initWithFrame: CGRectMake(8, 8, tableView.frame.size.width, 25.0)];
    customLabel.text = [self tableView:tableView titleForHeaderInSection:section];
    customLabel.textColor = [UIColor whiteColor];
    
    [headerView addSubview:customLabel];
    
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reuseIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
    }
    cell.detailTextLabel.textColor  = [UIColor colorMain];
    
    UIFont *font = [UIFont fontWithName:@"Helvetica-Light" size:15];

    cell.textLabel.font = font;
    cell.detailTextLabel.font = font;


    NSString *textLabel;
    NSString *detailTextLabel;
    
    switch (indexPath.section) {
        case 0:
            textLabel = arrInfoCustomerRow[indexPath.row];
            switch (indexPath.row) {
                case 0:
                    detailTextLabel = PTCModel.fullName;
                    cell.detailTextLabel.font = [UIFont systemFontOfSize:20.0f];

                    break;
                    
                case 1:
                    detailTextLabel = PTCModel.contract;
                    break;
                    
                case 2:
                    detailTextLabel = PTCModel.customerPhone;
                    break;
                    
                case 3:
                    detailTextLabel = PTCModel.address;
                    break;
                    
                default:
                    break;
            }
            
            break;
            
        case 1:
            textLabel = arrConstructionVoteRow[indexPath.row];
            switch (indexPath.row) {
                case 0:
                    detailTextLabel = PTCModel.PTCStatusName;
                    break;
                    
                case 1:
                    detailTextLabel = PTCModel.createDate;
                    break;
                    
                case 2:
                    detailTextLabel = PTCModel.createBy;
                    break;
                    
                default:
                    break;
            }
            
            break;
            
        case 2:
            textLabel = arrConstructionTeamRow[indexPath.row];
            switch (indexPath.row) {
                case 0:
                    detailTextLabel = PTCModel.staffName;
                    break;
                    
                case 1:
                    detailTextLabel = PTCModel.staffEmail;
                    break;
                    
                case 2:
                    detailTextLabel = PTCModel.staffPhone;
                    break;
                    
                default:
                    break;
            }

            break;
            
        default:
            break;
    }
    
    [cell.detailTextLabel setNumberOfLines:0];
    [cell.detailTextLabel sizeToFit];
    cell.textLabel.text =textLabel;
    cell.detailTextLabel.text = detailTextLabel;
    
    return cell;
}

#pragma mark - UITableView delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

}

#pragma mark - UIActionSheetDelegate methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag == 1) {
        if (buttonIndex == 0) {
            [self changePTCStatusForNotApproval];
            return;
        }
    }
}

- (void)showActionSheetWithTitle:(NSString *)title andTag:(NSInteger)tag {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:title delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
    actionSheet.tag = tag;
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}

#pragma mark - UIAlertViewDelegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 1) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)showAlertViewWithTitle:(NSString *)title message:(NSString *)message andTag:(NSInteger)tag {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alertView.tag = 1;
    [alertView show];
}

// Load dữ liệu Phiếu thi công
- (void)getPTCDetail {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    [dict setObject:self.contract forKey:@"Contract"];
    
    [self showMBProcess];
    
    [shared.registrationFormProxy getPTCDetail:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        
        if(![errorCode isEqualToString:@"0"] || result == nil) {
            [self showAlertViewWithTitle:@"Thông báo" message:@"Không có dữ liệu trả về!" andTag:1];
            [self.btnChangePTCStatus setEnabled:NO];
            [self hideMBProcess];
            
            return ;
        }
        
        PTCModel = result;
        [self.btnChangePTCStatus setEnabled:YES];
        self.detailTableView.hidden = NO;
        [self.detailTableView reloadData];
        
        [self hideMBProcess];

    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
        [self hideMBProcess];
    }] ;
}

- (void)changePTCStatusForNotApproval {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    
    NSString *status = @"0";
    if (PTCModel.PTCStatus == 0) {
        status = @"1";
    } else {
        status = @"2";
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    [dict setObject:self.contract forKey:@"Contract"];
    [dict setObject:StringFormat(@"%li", (long)PTCModel.PTCId) forKey:@"PTCId"];
    [dict setObject:status forKey:@"Status"];
    
    [self showMBProcess];
    
    [shared.registrationFormProxy changePTCStatusForNotApproval:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        [self showAlertBoxWithDelayDismiss:@"Thông báo" message:message];
        [self hideMBProcess];
        
        if ([errorCode intValue] != 0) {
            [self getPTCDetail];
        }
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
        [self hideMBProcess];
    }] ;
}

- (IBAction)btnChangePTCStatus_Clicked:(id)sender {
    [self showActionSheetWithTitle:@"Bạn có muốn chuyển tình trạng Phiếu thi công?" andTag:1];
}
@end
