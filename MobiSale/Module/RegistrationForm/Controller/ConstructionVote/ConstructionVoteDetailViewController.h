//
//  ConstructionVoteDetailViewController.h
//  MobiSale
//
//  Created by ISC on 6/24/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ConstructionVoteDetailViewController :BaseViewController

@property (weak, nonatomic) IBOutlet UITableView *detailTableView;
@property (weak, nonatomic) IBOutlet UIButton *btnChangePTCStatus;

@property (nonatomic, copy) NSString *contract;

@end
