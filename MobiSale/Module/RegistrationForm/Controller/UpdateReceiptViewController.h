//
//  UpdateReceiptViewController.h
//  MobiSale
//
//  Created by ISC-DanTT on 4/6/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RegistrationFormDetailRecord.h"
#import "BaseViewController.h"

@interface UpdateReceiptViewController : BaseViewController

@property (strong, nonatomic) RegistrationFormDetailRecord *record;

@property (strong, nonatomic) NSString *serviceType;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@property (strong, nonatomic) IBOutlet UILabel *lblRegCode;

@property (strong, nonatomic) IBOutlet UILabel *lblPhoneNumber;

@property (strong, nonatomic) IBOutlet UIButton *btnServicePackage;

@property (strong, nonatomic) IBOutlet UIButton *btnPromotion;

@property (strong, nonatomic) IBOutlet UIButton *btnDepositBlackPoint;

@property (strong, nonatomic) IBOutlet UIButton *btnDepositContract;

@property (strong, nonatomic) IBOutlet UITextField *txtNewMoneyTotal;

@property (strong, nonatomic) IBOutlet UILabel *lblCurrentMoneyTotal;

@property (strong, nonatomic) IBOutlet UILabel *lblMoneyDifference;

- (IBAction)btnDepositBlackPoint_Clicked:(id)sender;

- (IBAction)btnPromotion_Clicked:(id)sender;

- (IBAction)btnDepositContract_Clicked:(id)sender;

- (IBAction)btnServicePackage_Clicked:(id)sender;

@end
