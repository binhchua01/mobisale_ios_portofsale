//
//  DetailRegisteredForm.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 1/25/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BaseViewController.h"
#import "RegistrationFormDetailRecord.h"
#import "PayViewController.h"
#import "RightMenu.h"
#import "../../Map/Controller/Survey.h"
#import "DynamicMenuView.h"

@interface DetailRegisteredForm : BaseViewController<PayDelegate,SurveyDelegate, UIActionSheetDelegate, DynamicMenuViewDelegate>

@property (strong, nonatomic) NSString *Id;

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *mViewMenu;
@property (strong, nonatomic) IBOutlet UIView *updateReceiptMenu;
@property (strong, nonatomic) IBOutlet UIView *notUpdateReceiptMenuView;
@property (strong, nonatomic) IBOutlet UIView *bookportMenuView;
@property (strong, nonatomic) IBOutlet UIView *registrationContractMenuView;
@property (strong, nonatomic) IBOutlet UIView *menuContructVote;
@property (strong, nonatomic) IBOutlet UIView *menuUpcontract;


@property (strong, nonatomic) IBOutlet UILabel *lblFullName;
@property (strong, nonatomic) IBOutlet UILabel *lblId;
@property (strong, nonatomic) IBOutlet UILabel *lblContract;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblCMND;
@property (strong, nonatomic) IBOutlet UILabel *lblMST;
@property (strong, nonatomic) IBOutlet UILabel *lblPhone;
@property (strong, nonatomic) IBOutlet UILabel *lblIPTVReturnSTBCount;
@property (strong, nonatomic) IBOutlet UILabel *lblLocalType;
@property (strong, nonatomic) IBOutlet UILabel *lblIPTVBoxCount;
@property (strong, nonatomic) IBOutlet UILabel *lblIPTVPLCCount;
// ngày sinh
@property (weak, nonatomic) IBOutlet UILabel *lblBirthday;
// Địa chỉ trên CMND
@property (weak, nonatomic) IBOutlet UILabel *lblIDAddress;
// Ghi chú triển khai
@property (weak, nonatomic) IBOutlet UITextView *txtDeploymentNote;
// Hình thức thanh toán
@property (weak, nonatomic) IBOutlet UILabel *lblPaymentForm;

@property (strong, nonatomic) IBOutlet UILabel *lblTD;
@property (strong, nonatomic) IBOutlet UITextView *txtTDAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblIndoor;
@property (strong, nonatomic) IBOutlet UILabel *lblOutdoor;

@property (strong, nonatomic) IBOutlet UILabel *lblDeposit;
@property (strong, nonatomic) IBOutlet UILabel *lblDepositBlack;
@property (strong, nonatomic) IBOutlet UILabel *lblTotal;

@property (strong, nonatomic) IBOutlet UITextView *tvNote;
@property (strong, nonatomic) IBOutlet UITextView *lblBonus;

@property (weak, nonatomic) IBOutlet UIButton *btnThutien;
@property (strong, nonatomic) IBOutlet UIButton *btnUpdate;
@property (strong, nonatomic) IBOutlet UIButton *btnBookPort;
@property (strong, nonatomic) IBOutlet UIButton *btnSurvey;
@property (strong, nonatomic) IBOutlet UIButton *btnRecieve;
@property (strong, nonatomic) IBOutlet UILabel *lblInternetTotal;
@property (strong, nonatomic) IBOutlet UILabel *lblIPTVTotal;
@property (weak, nonatomic) IBOutlet UILabel *lblOffice365Total;
@property (weak, nonatomic) IBOutlet UITextView *packetServiceOffice365;

//TichHenPoup
@property (strong, nonatomic) IBOutlet UIButton *btnTichHen;

@property (strong, nonatomic) IBOutlet UIButton *btnUpdateReceipt;
// PHIẾU THI CÔNG 
@property (weak, nonatomic) IBOutlet UIButton *btnConstructionVote;

@property (strong, nonatomic) IBOutlet UIImageView *imageSurvey;
// Dịch vụ bán thêm
@property (strong, nonatomic) IBOutlet UILabel *contractServiceTypeLabel;

@property (strong, nonatomic) IBOutlet UILabel *paymentStatusLabel;
//vutt11
@property (weak, nonatomic) IBOutlet UILabel *oTTBoxCount;
@property (weak, nonatomic) IBOutlet UILabel *lblOTTToTal;

@property (weak, nonatomic) IBOutlet UITextView *packetListDevice;

@property (weak, nonatomic) IBOutlet UILabel *lblTotalDevice;

- (IBAction)btnUpdateReceipt_Clicked:(id)sender;

-(IBAction)btnTichHen_clicked:(id)sender;
- (IBAction)btn_DSTichHen_Clicked:(id)sender;

-(IBAction)btnUpdate_clicked:(id)sender;
-(IBAction)btnBookPort_clicked:(id)sender;
-(IBAction)btnSurvey_clicked:(id)sender;
-(IBAction)btnCancel_clicked:(id)sender;
-(IBAction)btnRecieve_clicked:(id)sender;


@property (strong, nonatomic) RegistrationFormDetailRecord *record;


@end
