//
//  Office365Model.h
//  MobiSale
//
//  Created by ISC on 10/6/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Package365Model.h"

static NSString* kOffice365ManagerAddedContentNotification = @"FPT.ISC.TECH.MobiSale.Office365ManagerAddedContent";
static NSString* kOffice365ManagerSetParameterNotification = @"FPT.ISC.TECH.MobiSale.Office365ManagerSetParameter";

@interface Office365Model : NSObject

@property (strong, nonatomic) NSString * emailAdmin; //Email của chủ hợp đồng.
@property (strong, nonatomic) NSString * domainName; //Thông tin tiên miền.
@property (strong, nonatomic) NSString * techName; //Nhân viên kỹ thuật.
@property (strong, nonatomic) NSString * techPhoneNumber; //Số điện thoại nhân viên kỹ thuật.
@property (strong, nonatomic) NSString * techEmail; //Email nhân viên kỹ thuật.

- (instancetype)initWithDictionary:(NSDictionary* )dict;

+ (instancetype)sharedManager;
- (NSArray *)listPackages;
- (NSArray *)listPackagesParameter;
- (void)addPackage365:(Package365Model*)package365Model;
- (NSMutableDictionary *)setParamater;
- (void)setParamaterListPackage;
- (void)resetData;

@end
