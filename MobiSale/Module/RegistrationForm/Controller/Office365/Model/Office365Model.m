//
//  Office365Model.m
//  MobiSale
//
//  Created by ISC on 10/6/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.


#import "Office365Model.h"
#import "Office365ViewController.h"
@interface Office365Model()

@property (strong, nonatomic) dispatch_queue_t concurrentOffice365Queue;
@property (strong, nonatomic) NSMutableArray *listPackagesArray;
@property (strong, nonatomic) NSMutableArray *listPackagesParamater;

@end

@implementation Office365Model

//vutt11 config
- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        self.emailAdmin         = dict[@"EmailAdmin"];
        self.domainName         = dict[@"DomainName"];
        self.techName           = dict[@"TechName"];
        self.techEmail          = dict[@"TechEmail"];
        self.techPhoneNumber    = dict[@"TechPhoneNumber"];
        NSArray *listPackageArray        = dict[@"ListPackage"];
        if (listPackageArray.count > 0) {
            [_listPackagesArray removeAllObjects];
            for (NSDictionary *dict in listPackageArray) {
                Package365Model *model = [[Package365Model alloc] initWithDictionary:dict];
                [self addPackage365:model];
            }
        }
    }
    return self;
}
// note by vutt
+ (instancetype)sharedManager
{
    static Office365Model *shareOffice365Manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        shareOffice365Manager = [[Office365Model alloc] init];
        shareOffice365Manager.listPackagesArray = [NSMutableArray array];
        shareOffice365Manager.listPackagesParamater = [NSMutableArray array];
        shareOffice365Manager.concurrentOffice365Queue = dispatch_queue_create("FPT.ISC.TECH.MobiSale.package365Queue", DISPATCH_QUEUE_CONCURRENT);
        
    });
    return shareOffice365Manager;
}

- (NSArray *)listPackages
{
    __block NSMutableArray *array;
    dispatch_sync(self.concurrentOffice365Queue, ^{
        
        array = _listPackagesArray;
    });

    return array;
}
// fix bug when update have office 365
- (NSArray *)listPackagesParameter
{
    __block NSArray *array;
    dispatch_sync(self.concurrentOffice365Queue, ^{
        array = _listPackagesParamater;
    });
    
    return array;
}
//vutt
- (void)addPackage365:(Package365Model *)package365Model
{
    
    if (package365Model) {
        dispatch_barrier_sync(self.concurrentOffice365Queue, ^{
            
            [_listPackagesArray addObject:package365Model];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self postContentAddedNotification];
                NSLog(@"log listcontract");
                
            });
        });
    }
}

//vutt11
- (void)resetData
{
    self.emailAdmin         = @"";
    self.domainName         = @"";
    self.techName           = @"";
    self.techEmail          = @"";
    self.techPhoneNumber    = @"";
    [_listPackagesArray removeAllObjects];
}

#pragma Mark - Setup pramaters for APIs
- (NSMutableDictionary *)setParamater
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    if (self) {
        [dict setObject:_emailAdmin      ?:@"" forKey:@"EmailAdmin"];
        [dict setObject:_domainName      ?:@"" forKey:@"DomainName"];
        [dict setObject:_techName        ?:@"" forKey:@"TechName"];
        [dict setObject:_techPhoneNumber ?:@"" forKey:@"TechPhoneNumber"];
        [dict setObject:_techEmail       ?:@"" forKey:@"TechEmail"];
        
        NSArray *listPackageParameter = [self listPackagesParameter];
        [dict setObject:listPackageParameter forKey:@"ListPackage"];

    }
    
    return dict;
    
}
//vutt11
- (void)setParamaterListPackage
{
    [_listPackagesParamater removeAllObjects];

    for (Package365Model *package in _listPackagesArray) {
        dispatch_barrier_sync(self.concurrentOffice365Queue, ^{
            NSMutableDictionary *dict = [NSMutableDictionary dictionary];
            dict = [self setParamaterPackage:package];
            [_listPackagesParamater addObject:dict];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self postSetParameterNotification];
            });
        });
    }

}
- (NSMutableDictionary *)setParamaterPackage:(Package365Model *)package
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:StringFormat(@"%li", (long)package.packageID)  forKey:@"PackageID"];
    [dict setObject:StringFormat(@"%li", (long)package.price)      forKey:@"Price"];
    [dict setObject:StringFormat(@"%li", (long)package.monthly)    forKey:@"Monthly"];
    [dict setObject:StringFormat(@"%li", (long)package.seat)       forKey:@"Seat"];
    [dict setObject:StringFormat(@"%li", (long)package.seatAdd)    forKey:@"SeatAdd"];
    [dict setObject:StringFormat(@"%li", (long)package.monthlyAdd) forKey:@"MonthlyAdd"];
    [dict setObject:StringFormat(@"%li", (long)package.priceAdd)   forKey:@"PriceAdd"];
    [dict setObject:@"1"     forKey:@"Status"];
    [dict setObject:package.packageName ?:@""                      forKey:@"PackageName"];
    return dict;
}

- (void)postContentAddedNotification
{
    
    static NSNotification *notification = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        notification = [NSNotification notificationWithName:kOffice365ManagerAddedContentNotification object:nil];
    });
    
    [[NSNotificationQueue defaultQueue]enqueueNotification:notification postingStyle:NSPostASAP coalesceMask:NSNotificationCoalescingOnName forModes:nil];
}

- (void)postSetParameterNotification
{
    static NSNotification *notification = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        notification = [NSNotification notificationWithName:kOffice365ManagerSetParameterNotification object:nil];
    });
    
    [[NSNotificationQueue defaultQueue]enqueueNotification:notification postingStyle:NSPostASAP coalesceMask:NSNotificationCoalescingOnName forModes:nil];
}

@end
