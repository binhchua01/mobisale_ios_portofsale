//
//  Package365Model.m
//  MobiSale
//
//  Created by ISC on 10/6/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "Package365Model.h"

@implementation Package365Model

- (instancetype)initWithDictionary:(NSDictionary *)dict
{
    if (self = [super init]) {
        self.packageID   = [dict[@"PackageID"] integerValue];
        self.packageName =  dict[@"PackageName"];
        self.price       = [dict[@"Price"] integerValue];
        self.priceAdd    = [dict[@"PriceAdd"] integerValue];
        
        self.monthly     = [dict[@"Monthly"] integerValue];
        self.monthlyAdd  = [dict[@"MonthlyAdd"] integerValue];
        self.seat        = [dict[@"Seat"] integerValue];
        self.seatAdd     = [dict[@"SeatAdd"] integerValue];
        self.status      = [dict[@"Status"] integerValue];

    }
    return self;
}

@end
