//
//  Package365Model.h
//  MobiSale
//
//  Created by ISC on 10/6/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Package365Model : NSObject

@property (assign, nonatomic) NSInteger packageID; //id bảng Package
@property (strong, nonatomic) NSString *packageName ; //tên gói cước
@property (assign, nonatomic) NSInteger price; //giá gói
@property (assign, nonatomic) NSInteger priceAdd; //đơn giá phụ

@property (assign, nonatomic) NSInteger monthly; // Số tháng
@property (assign, nonatomic) NSInteger monthlyAdd; // Số tháng phụ
@property (assign, nonatomic) NSInteger seat; // Số lượng
@property (assign, nonatomic) NSInteger seatAdd; // Số lượng phụ

@property (assign, nonatomic) NSInteger status; // "Status":"1", Tình trạng đăng ký mới gói Office

- (instancetype)initWithDictionary:(NSDictionary* )dict;

@end
