//
//  PayViewController.h
//  MobiSale
//
//  Created by HIEUPC on 4/16/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../../RegistrationForm/Model/RegistrationFormDetailRecord.h"
#import "BaseViewController.h"
#import "MAMPosAPIHelper.h"

@protocol PayDelegate <NSObject>

-(void)CancelPopup;

@end

@interface PayViewController : BaseViewController <MAMPosAPIHelperDelegate>

@property (strong ,nonatomic) NSMutableArray *arrSBIInternet;
@property (strong ,nonatomic) NSMutableArray *arrSBIIPTV;
@property (strong ,nonatomic) NSMutableArray *arrList;
@property (strong ,nonatomic) NSString *Regcode;
@property (strong ,nonatomic) RegistrationFormDetailRecord *record;

@property (retain ,nonatomic) id<PayDelegate> delegate;


// MPOST
@property (strong, nonatomic) NSString *result_TransactionKey;
@property (strong, nonatomic) NSString *result_ApprovedCode;
@property (strong, nonatomic) NSString *result_ReferenceNbr;
@property (strong, nonatomic) NSString *result_CardHolderName;
@property (strong, nonatomic) NSString *result_MaskedCardNumber;
@property (strong, nonatomic) NSString *result_ResponseCode;
@property (strong, nonatomic) NSString *result_ErrDesc;
@property (strong, nonatomic) NSString *result_StatusDesc;
@property (strong, nonatomic) NSString *result_TerminalId;
@property (strong, nonatomic) NSString *result_MerchantId;
@property (strong, nonatomic) NSString *result_TransactionDate;

@property (weak, nonatomic) IBOutlet UIButton *btnSBIInernet;
@property (weak, nonatomic) IBOutlet UIButton *btnSBIIPTV;
@property (weak, nonatomic) IBOutlet UIButton *btnPaymentType;
@property (weak, nonatomic) IBOutlet UIButton *btnSignature;
@property (strong, nonatomic) IBOutlet UIButton *btnUpdate;

@property (weak, nonatomic) IBOutlet UIImageView *imgSignature;

@property (weak, nonatomic) IBOutlet UILabel *lblFullName;
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
@property (weak, nonatomic) IBOutlet UILabel *lblContract;
@property (weak, nonatomic) IBOutlet UILabel *lblTotal;

-(IBAction) btnCancel_clicked:(id)sender;
-(IBAction) btnSBIInernet_clicked:(id)sender;
-(IBAction) btnSBIIPTV_clicked:(id)sender;
-(IBAction) btnUpdate_clicked:(id)sender;
-(IBAction) btnPaymentType_clicked:(id)sender;



@end
