//
//  SignatureViewController.m
//  MobiSale
//
//  Created by TranTuanVu on 12/14/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "SignatureViewController.h"
#import "TESignatureView.h"
#import "UIViewController+MJPopupViewController.h"
#import "ShareData.h"
@interface SignatureViewController ()

{
    __weak IBOutlet TESignatureView *signtureView;
    
}

@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;
@property (weak, nonatomic) IBOutlet UIButton *btnRefesh;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation SignatureViewController
@synthesize Regcode;
@synthesize delegate;

- (void)viewDidLoad {
   
    [super viewDidLoad];
    
    signtureView.backgroundColor = [UIColor whiteColor];
    
    self.btnRefesh.layer.cornerRadius = 5;
    self.btnConfirm.layer.cornerRadius = 5;
    
    
    self.btnRefesh.layer.shadowOffset = CGSizeMake(0, 3);
    self.btnRefesh.layer.shadowColor = [UIColor blackColor].CGColor;
    self.btnRefesh.layer.shadowRadius = 1.0;
    self.btnRefesh.layer.shadowOpacity = .5;

    self.btnConfirm.layer.shadowOffset = CGSizeMake(0, 3);
    self.btnConfirm.layer.shadowColor = [UIColor blackColor].CGColor;
    self.btnConfirm.layer.shadowRadius = 1.0;
    self.btnConfirm.layer.shadowOpacity = .5;
    
    ShareData *shared = [ShareData instance];
    shared.imageView = NULL;
   
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (IBAction)btnRefesh_clicked:(id)sender {
    
     [signtureView clearSignature];
    
}
//vutt11
- (IBAction)btnConfirm_clicked:(id)sender {
    
    ShareData *shared = [ShareData instance];
   UIImageWriteToSavedPhotosAlbum([signtureView getSignatureImage], nil, nil, nil);
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            self.imageView.image = shared.imageView;
            
        });
        
    });
    
    [self dismissViewControllerAnimated:true completion:nil];
    
    //check imageData
    
    if (shared.imageData == NULL || [shared.imageData  isEqual: @""] || shared.imageView == nil) {
        return;
    }
    
    [self.delegate dismissSignatureView:shared.imageView];
   
        
         [self updateImage];
        
    
   

}

- (IBAction)btnCancel_clicked:(id)sender {
   
   
    
    if (self.delegate) {
        [self.delegate CancelSignatureView];
    }
}

- (void) updateImage {
    
    ShareData *shared = [ShareData instance];
    [self showMBProcess];
    
    if (shared.imageData == NULL || [shared.imageData  isEqual: @""]) {
        [self hideMBProcess];
        return;
    }
    
    [shared.registrationFormProxy upDateImage:shared.currentUser.userName Image:shared.imageData RegCode:self.Regcode Type:2 Completehander:^(id result, NSString *errorCode, NSString *message ){
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        
        if (message.length <10) {
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return;
        }
        shared.imageSignature = message;
        [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",@"Xác nhận chữ ký thành công !!!"]];
        
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error){
        
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
        return;
    }];
    
}

@end
