
//  DetailRegisteredForm.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 1/25/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.

#import "DetailRegisteredForm.h"
#import "ShareData.h"
#import "BoxRecord.h"
#import "RegisteredFormController.h"
#import "MainMap.h"
#import "TichHenPoup.h"
#import "Common_client.h"
#import "ListTichHenController.h"
#import "UpdateReceiptViewController.h"
#import "ConstructionVoteDetailViewController.h"
#import <MWPhotoBrowser/MWPhoto.h>
#import <MWPhotoBrowser/MWPhotoBrowser.h>
#import "RegistrationFormSaleMorePageViewController.h"
#import "ListRegisteredFormViewController.h"
#import "PaymentViewController.h"
#import "DynamicMenuView.h"
#import "DynamicMenuCell.h"

#import "MobiSale-Swift.h"

enum {
    
    INTERNET = 0,
    IPTV = 1,
    INTERNET_IPTV = 2,
    OTHER = 3,
    OTT = 4
    
};

@interface DetailRegisteredForm () <MWPhotoBrowserDelegate>
@property(strong,nonatomic) NSArray *arraylistPacket;
@property(strong,nonatomic) NSMutableArray *arraylistPacketShow;
@property(strong,nonatomic) NSMutableArray *arraylistPacketString;
@property(strong,nonatomic) NSMutableArray *arrayListpacketDevice;
@property(strong,nonatomic) NSMutableArray *arratListpacketDeviceString;
@property(strong,nonatomic) NSString *seatAdd;
@property(strong,nonatomic) NSString *seat;
@property (strong,nonatomic)NSMutableString *listDevice;
@property (strong,nonatomic)NSMutableString *addlistDevice;

@property (weak, nonatomic) IBOutlet UIImageView *imageInfo;
@property (weak, nonatomic) IBOutlet UIImageView *imageSignature;

@end

@implementation DetailRegisteredForm{
    NSString *a, *statusDeposit;
    int statusmenu;
    RegistrationFormDetailRecord *rc;
    NSInteger selectedLocalType;
    UIView *menuView;
    NSInteger amountOTTTotal;
    //StevenNguyen -photo for image
    NSMutableArray *arrPhoto;
    MWPhoto *photoImage;
}

- (NSMutableArray *)arrayListpacketDevice {
    if (!_arrayListpacketDevice) {
        _arrayListpacketDevice = [[NSMutableArray alloc] init];
        
    }
    return _arrayListpacketDevice;
}
- (NSMutableArray *)arratListpacketDeviceString {
    if (!_arratListpacketDeviceString) {
        _arratListpacketDeviceString = [[NSMutableArray alloc] init];
        
    }
    return _arratListpacketDeviceString;
}


- (NSArray *)arraylistPacket {
    if (!_arraylistPacket) {
        _arraylistPacket = [[NSMutableArray alloc] init];
        
    }
    return _arraylistPacket;
}

- (NSMutableArray *)arraylistPacketShow {
    if (!_arraylistPacketShow) {
        _arraylistPacketShow = [[NSMutableArray alloc] init];
        
    }
    return _arraylistPacketShow;
}
- (NSMutableArray *)arraylistPacketString {
    if (!_arraylistPacketString) {
        _arraylistPacketString = [[NSMutableArray alloc] init];
        
    }
    return _arraylistPacketString;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //vutt11
    self.title = @"CHI TIẾT PĐK";
    self.screenName = self.title;
    
    [self setType];
    
    //Chỉnh lại Button
    CGRect mainRect = [[UIScreen mainScreen] bounds];
    CGSize sizeButton = self.btnRecieve.frame.size;
    CGRect frameButton = CGRectMake(mainRect.size.width - (sizeButton.width) - 8, mainRect.size.height - (sizeButton.height) - 8, sizeButton.width, sizeButton.height);
    [self.btnRecieve setFrame:frameButton];
    
    //Create ScrollView
    [self.scrollView setBackgroundColor:[UIColor clearColor]];
    [self.scrollView setCanCancelContentTouches:NO];
    self.scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    [self.scrollView setScrollEnabled:YES];
    self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
    
    //Set handler tap screen add by dantt2
    UITapGestureRecognizer *tapScreem = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnScreen:)];
    [self.view addGestureRecognizer:tapScreem];
    
    // tap ảnh bản vẽ khảo sát
    UITapGestureRecognizer *tapImageSurvey = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(brownserImage:)];
    [self.imageSurvey addGestureRecognizer:tapImageSurvey];
    
    //tap anh chu ky
    UITapGestureRecognizer *tapImageSignature = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(brownserImage:)];
    [self.imageSignature addGestureRecognizer:tapImageSignature];
    
    //tap thong tin
    UITapGestureRecognizer *tapImageInfo = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(brownserImage:)];
    [self.imageInfo addGestureRecognizer:tapImageInfo];
    
    // add refresh control when scroll top of scrollview
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.scrollView addSubview:refreshControl];
}

-(void)viewDidLayoutSubviews{
    if(IS_IPHONE4){
        [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 3000)];
        return;
    }
    //2799
    if(IS_IPHONE5){
        [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 3000)];
        return;
    }
    [self.scrollView setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 2999)];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //Set menu view frame
    statusmenu = 1;
    
    [self rightSideMenuButtonPressed:NULL];
    
    [self LoadData];
    [self.menuContainerViewController setRightMenuWidth:0];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.menuContainerViewController.rightMenuWidth = 0;
    if (statusmenu == 1) {
        [self rightSideMenuButtonPressed:NULL];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - load data
-(void)LoadData {
    
    [self showMBProcess];
    ShareData *shared = [ShareData instance];
    [shared.registrationFormProxy GetDetailRegistration:self.Id Completehander:^(id result, NSString *errorCode, NSString *message) {
        //[self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self hideMBProcess];
            [self LogOut];
            return;
        }
        
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            
            [self hideMBProcess];
            return ;
        }
        
        if(result == nil){
            [self showAlertBox:@"Thông Báo" message:Mesage_DataEmpty];
            [self hideMBProcess];
            
        }else {
            if([errorCode isEqualToString:@"0"]){
                [self hideMBProcess];
                self.record = [result objectAtIndex:0];
                rc = self.record;
                [self setDataWhenLoadView];
                
            }
        }
        
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        if ([[error localizedDescription ]isEqualToString:@"The request timed out"]) {
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        
        [self hideMBProcess];
    }];
}
- (void)setDataWhenLoadView {
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    [self.arraylistPacketShow removeAllObjects];
    [self.arraylistPacketString removeAllObjects];
    
    self.lblId.text         = rc.RegCode;
    self.lblContract.text   = rc.Contract;
    self.lblFullName.text   = rc.FullName;
    self.lblPhone.text      = rc.Phone_1;
    self.lblCMND.text       = rc.Passport;
    self.lblIDAddress.text  = rc.AddressPassport;
    self.lblBirthday.text   = rc.Birthday;
    self.lblPaymentForm.text = rc.PaymentName;
    self.txtDeploymentNote.text = rc.DescriptionIBB;
    self.lblAddress.text    = rc.Address;
    self.tvNote.text        = rc.Note;
    self.lblMST.text        = rc.TaxId;
    self.lblLocalType.text  = rc.LocalTypeName;
    NSString *contract = StringFormat(@"%@",rc.Contract);
    if (![contract isEqualToString:@""]) {
        
        self.lblBonus.text = rc.contractPromotionName;
        
    }
    else {
        
        self.lblBonus.text      = rc.PromotionName;
    }
    
    self.regcode            = rc.RegCode;
    self.lblIPTVPLCCount.text = rc.IPTVPLCCount;
    self.lblIPTVReturnSTBCount.text = rc.IPTVReturnSTBCount;
    self.lblIPTVBoxCount.text   = rc.IPTVBoxCount;
    self.lblTD.text             = rc.TDName;
    self.txtTDAddress.text      = rc.TDAddress;
    self.lblIndoor.text         = rc.InDoor;
    self.lblOutdoor.text        = rc.OutDoor;
    self.contractServiceTypeLabel.text = rc.contractServiceTypeName;
    
    // Tiền đặt cọc thuê bao.
    self.lblDeposit.text    = StringFormat(@"%@ VNĐ",[nf stringFromNumber:[NSNumber numberWithFloat:[rc.Deposit intValue]]]);
    // Tiền đặt cọc điểm đen.
    self.lblDepositBlack.text   = StringFormat(@"%@ VNĐ",[nf stringFromNumber:[NSNumber numberWithFloat:[rc.DepositBlackPoint intValue]]]);
    // Tổng tiền Internet.
    self.lblInternetTotal.text  = StringFormat(@"%@ VNĐ",[nf stringFromNumber:[NSNumber numberWithFloat:[rc.InternetTotal intValue]]]);
    // Tổng tiền IPTV.
    self.lblIPTVTotal.text      = StringFormat(@"%@ VNĐ",[nf stringFromNumber:[NSNumber numberWithFloat:[rc.IPTVTotal intValue]]]);
    // Tổng tiền Office 365
    self.lblOffice365Total.text = StringFormat(@"%@ VNĐ",[nf stringFromNumber:[NSNumber numberWithFloat:rc.office365Total]]);
    // Tổng tiền Device
    self.lblTotalDevice.text = StringFormat(@"%@ VNĐ",[nf stringFromNumber:[NSNumber numberWithFloat:[rc.DeviceTotal intValue]]]);
    
    // TỔNG TIỀN.
    self.lblTotal.text      = StringFormat(@"%@ VNĐ",[nf stringFromNumber:[NSNumber numberWithFloat:[rc.Total intValue]]]);
    //vutt11
    //GÓI DỊCH VỤ
    //OTT
    amountOTTTotal = rc.OTTTotal;
    self.oTTBoxCount.text = rc.OTTBoxCount;
    self.lblOTTToTal.text = StringFormat(@"%@ VNĐ", [nf stringFromNumber:[NSNumber numberWithFloat:amountOTTTotal]]);
    //vutt11
    //Fix bug Update  have not office 365 but show info of office 365
    if (rc.office365Total > 0) {
        
        self.arraylistPacket = rc.office365Model.listPackages;
        if (self.arraylistPacket.count > 0) {
            
            for (int i = 0; i< self.arraylistPacket.count; i++) {
                
                //self.seatAdd = [NSString stringWithFormat:@"%@",[self.arraylistPacket[i] valueForKey:@"seatAdd"] ];
                self.seat = [NSString stringWithFormat:@"%@",[self.arraylistPacket[i] valueForKey:@"seat"]];
                
                if (![self.seat isEqualToString:@"0"]) {
                    [self.arraylistPacketShow addObject:self.arraylistPacket[i]];
                    
                }
            }
            
            for (int i = 0 ; i<self.arraylistPacketShow.count; i++) {
                
                if ([[self.arraylistPacketShow[i] valueForKey:@"packageName"] isEqualToString:@"Mail Pro"]||[[self.arraylistPacketShow[i] valueForKey:@"packageName"] isEqualToString:@"Mail Plus"]) {
                    
                    self.seatAdd = [NSString stringWithFormat:@"%@",[self.arraylistPacketShow[i] valueForKey:@"seatAdd"] ];
                    self.seat = [NSString stringWithFormat:@"%@",[self.arraylistPacketShow[i] valueForKey:@"seat"]];
                    
                    [self.arraylistPacketString addObject:[NSString stringWithFormat:@"%@/SL:%d,%@thang",[self.arraylistPacketShow[i] valueForKey:@"packageName"],[self.seatAdd intValue]+ [self.seat intValue],[self.arraylistPacketShow[i] valueForKey:@"monthly"]]];
                }
                
                else {
                    
                    [self.arraylistPacketString addObject:[NSString stringWithFormat:@"%@/SL:%@,%@thang",[self.arraylistPacketShow[i] valueForKey:@"packageName"],[self.arraylistPacketShow[i]valueForKey:@"seat"],[self.arraylistPacketShow[i] valueForKey:@"monthly"]]];
                }
                
            }
            
            //Deleted ky tu dat biet cau mang
            for (int  i = 0 ; i< self.arraylistPacketString.count; i++) {
                
                self.packetServiceOffice365.text = [ NSString stringWithFormat:@"%@" ,self.arraylistPacketString];
                self.packetServiceOffice365.text = [self.packetServiceOffice365.text stringByReplacingOccurrencesOfString:@"\"" withString: @" "];
                self.packetServiceOffice365.text = [[self.packetServiceOffice365.text stringByReplacingOccurrencesOfString:@"(" withString:@""] stringByReplacingOccurrencesOfString:@")" withString:@""];
            }
            
        }
    }
    else{
        self.packetServiceOffice365.text = @"";
    }
    [self.btnRecieve setHidden:YES];
    if([rc.StatusDeposit isEqualToString:@"0"]){
        self.paymentStatusLabel.text = @"Chưa thu tiền";
    }else {
        self.paymentStatusLabel.text = @"Đã thu tiền";
    }
    
    selectedLocalType = [self loadLocalType:rc.PromotionID Package:rc.IPTVPackage];
    
    statusDeposit = rc.StatusDeposit ;
    
    if (rc.Image.length <= 0) {
        
        self.imageSurvey.userInteractionEnabled = NO;
        
        [self hideMBProcess];
        
    } else {
        
        self.imageSurvey.userInteractionEnabled = YES;
        
        [self getImageWithUrl:rc.Image title:rc.Image];
        [self hideMBProcess];
    }
    
    if (![rc.ListDevice isEqual:[NSNull null]] && rc.ListDevice.count > 0) {
        self.arratListpacketDeviceString = nil;
        self.arrayListpacketDevice = rc.ListDevice;
        for (int i = 0; i < self.arrayListpacketDevice.count; i++) {
            
            NSString *name,*number,*promotion;
            name = StringFormat(@"Tên:%@",[self.arrayListpacketDevice[i]valueForKey:@"DeviceName"]);
            number = StringFormat(@"SL:%@",[self.arrayListpacketDevice[i]valueForKey:@"Number"]);
            promotion = StringFormat(@"CLKM:%@\n",[self.arrayListpacketDevice[i]valueForKey:@"PromotionDeviceText"]);
            
            [self.arratListpacketDeviceString addObject:StringFormat(@"%@,%@,%@",name,number,promotion)];
            
        }
        
        if (self.arratListpacketDeviceString.count > 0) {
            self.packetListDevice.text = [self.arratListpacketDeviceString componentsJoinedByString:@""];
        }else {
            self.packetListDevice.text = @"";
        }
        
    }
    
    
    
    [self loadViewDynamic];
    
    
}

//MARK: -SETUP MENU
-(void)loadViewDynamic {
    
    //load View
    NSArray *nibView = [[NSBundle mainBundle] loadNibNamed:@"DynamicMenuView" owner:self options:nil];
    DynamicMenuView* myView = [nibView lastObject];
    
    NSMutableArray *arrDynamicShow = [self checkShowMenu];
    
    [myView setFrame:CGRectMake(0, [SiUtils getScreenFrameSize].height, self.notUpdateReceiptMenuView.frame.size.width, 31 * arrDynamicShow.count)];
    
    myView.delegate = self;
    [myView configView:arrDynamicShow];
    
    menuView = [[UIView alloc] init];
    menuView = myView;
    
}

-(NSMutableArray *)checkShowMenu {
    
    NSMutableArray *arrDynamicView = [[NSMutableArray alloc] init];
    
    ShareData *shared = [ShareData instance];
    
    int statusDesposit = [rc.StatusDeposit intValue] ?: 0;
    
    int regType = [rc.regType intValue] ?: 0;
    
    int inDoor = [rc.InDoor intValue] ?: 0;
    
    int outDoor = [rc.OutDoor intValue] ?: 0;
    
    int inDType = [rc.InDType intValue] ?: 0;
    
    int outDType = [rc.OutDType intValue] ?: 0;
    
    int promotionID = [rc.PromotionID intValue] ?: 0;
    
    int iptvTotal = [rc.IPTVTotal intValue] ?: 0;
    
    int total = [rc.Total intValue] ?: 0;
    
    /*******************************************************************************************************/
    /************************************ Điều kiện cho Button Thu Tiền ************************************/
    /*******************************************************************************************************/
    [self.btnRecieve addTarget:self action:@selector(pressedChangedAction:) forControlEvents:UIControlEventTouchUpInside];
    //Check xem hiễn thị thu tiền hay lên họp đồng
    if (statusDesposit > 0) {/* đã thu tiền */
        
        // nếu  user có quyền lên hợp đồng và số HD ko có
        if (shared.currentUser.IsCreateContract > 0 && [rc.Contract isEqualToString:@""]) {
            // hiển thị nút lên HD
            // Ẩn nút thu tiền
            NSLog(@"Qua Day");
            [self.btnRecieve setTitle:@"Lên hợp đồng" forState:UIControlStateNormal];
            [self.btnRecieve setTag:1];
            [self.btnRecieve setHidden:NO];
        }
        
    } else {/* chưa thu tiền */
        
        // Kiểm tra phải là PDK bán thêm hay không	 ?
        if (regType > 0)/* Trường họp này là bán thêm  */ {
            // Hiển thị nút thu tiền . Thu tiền cho bán thêm
            NSLog(@"Qua Day");
            [self.btnRecieve setTitle:@"Thu Tiền" forState:UIControlStateNormal];
            [self.btnRecieve setTag:1];
            [self.btnRecieve setHidden:NO];
            
        } else {/* Đây là trường họp bán mới */
            
            if ([rc.ODCCableType isEqualToString:@""] || rc.ODCCableType.length <= 0) {/* PDK chưa book port */
                //Hiển thị là nút book port
                NSLog(@"Qua Day");
                [self.btnRecieve setTitle:@"BookPort" forState:UIControlStateNormal];
                [self.btnRecieve setTag:2];
                [self.btnRecieve setHidden:NO];
                
            } else if (inDoor <= 0 || inDType <= 0 || outDoor <= 0 || outDType <= 0) {/* PDK chưa khảo sát */
                
                // Hiển thị là nút khảo sát
                NSLog(@"Qua Day");
                [self.btnRecieve setTitle:@"Khảo sát" forState:UIControlStateNormal];
                [self.btnRecieve setTag:3];
                [self.btnRecieve setHidden:NO];
                
            } else {
                
                // Hiển thị là nút thu tiền
                NSLog(@"Qua Day");
                [self.btnRecieve setTitle:@"Thu Tiền" forState:UIControlStateNormal];
                [self.btnRecieve setTag:1];
                [self.btnRecieve setHidden:NO];
                
            }
            
        }
    }
    
    /**************************************************************************************************/
    /************************************ Điều kiện cho Menu Right ************************************/
    /**************************************************************************************************/
    
    //Kiểm tra PDK có phải là Office 365 , FPT Play hay không ?
    if (promotionID == 0 && iptvTotal == 0 && total > 0) {/* đây là pdk office 365 or FPT Play */
        
        if (statusDesposit <= 0) {/* chưa thu tiền */
            // hiển thị menu thu tiền
            // hiển thị menu cập PDK
            NSLog(@"Qua Day");
            [arrDynamicView addObject:[NSNumber numberWithInteger:SNDynamicMenuViewUpdate]];
            [arrDynamicView addObject:[NSNumber numberWithInteger:SNDynamicMenuViewEarningMoney]];
            
            
        } else {/* đã thu tiền */
            
            if (promotionID > 0) {/* Nếu có bán ineternet thì cho phép cập nhật lại tổng tiền bằng cách chọn lại CLKM */
                
                // Hiển thị menu cập nhật lại tổng tiền
                NSLog(@"Qua Day");
                [arrDynamicView addObject:[NSNumber numberWithInteger:SNDynamicMenuViewUpdateAmount]];
                
            }
            
        }
        
    } else if ([rc.Contract isEqualToString:@""]) {/* PDK chưa lên HD */
        
        // Hiển thị menu book port ( có thể book port mới or cập nhật port khác)
        [arrDynamicView addObject:[NSNumber numberWithInteger:SNDynamicMenuViewBookPort]];
        
        
        if (![rc.ODCCableType isEqualToString:@""] || rc.ODCCableType.length > 0) {/* Nếu đã book port rồi */
            
            NSLog(@"Qua Day");
            
            // Hiển thị menu khảo sát
            [arrDynamicView addObject:[NSNumber numberWithInteger:SNDynamicMenuViewSurvey]];
            
            // Hiển thị menu “Hẹn triển khai”
            [arrDynamicView addObject:[NSNumber numberWithInteger:SNDynamicMenuViewAppointment]];
        }
        
        if (statusDesposit <= 0) {/* Nếu chưa thu tiền */
            // Hiển thị thu tiền
            // Cập nhật PDK
            NSLog(@"Qua Day");
            [arrDynamicView addObject:[NSNumber numberWithInteger:SNDynamicMenuViewUpdate]];
            [arrDynamicView addObject:[NSNumber numberWithInteger:SNDynamicMenuViewEarningMoney]];
            
        } else {/* Đã thu tiền rồi */
            
            if (promotionID > 0) {/* Nếu có bán ineternet thì cho phép cập nhật lại tổng tiền bằng cách chọn lại CLKM */
                // Hiển thị menu cập nhật lại tổng tiền
                NSLog(@"Qua Day");
                [arrDynamicView addObject:[NSNumber numberWithInteger:SNDynamicMenuViewUpdateAmount]];
            }
        }
        
    } else {/* // PDK đã có Họp đồng */
        
        if (regType > 0 && statusDesposit == 0) {/* Đây là bán thêm chưa thu tiền  */
            // Hiển thị menu cập nhật PDK
            // Hiển thị menu thu tiền
            NSLog(@"Qua Day");
            [arrDynamicView addObject:[NSNumber numberWithInteger:SNDynamicMenuViewUpdate]];
            [arrDynamicView addObject:[NSNumber numberWithInteger:SNDynamicMenuViewEarningMoney]];
            
        } else {/* PDK bán mới đã có Họp đồng */
            
            // Hiên thị menu “ Phiếu thi công”
            NSLog(@"Qua Day");
            //SNDynamicMenuViewConstructVote
            // vu fix hien thi phieu thi cong
            [arrDynamicView addObject:[NSNumber numberWithInteger:SNDynamicMenuViewConstructVote]];
            if (promotionID > 0) {/* Nếu có bán ineternet thì cho phép cập nhật lại tổng tiền bằng cách chọn lại CLKM  */
                // Hiển thị menu cập nhật lại tổng tiền
                NSLog(@"Qua Day");
                [arrDynamicView addObject:[NSNumber numberWithInteger:SNDynamicMenuViewUpdateAmount]];
                
            }
        }
    }
    
    //thêm nút Huỷ
    [arrDynamicView addObject:[NSNumber numberWithInteger:SNDynamicMenuViewCancel]];
    return arrDynamicView;
    
}

-(IBAction)pressedChangedAction:(id)sender {
    
    //Thu tien
    if (self.btnRecieve.tag == 1) {
        [self btnRecieve_clicked:sender];
    } else if (self.btnRecieve.tag == 2) {
        //Book port
        [self btnBookPort_clicked:sender];
    } else if (self.btnRecieve.tag == 3) {
        //Khao sat
        [self btnSurvey_clicked:sender];
    }
    
}


- (BOOL)checkedOffice365Only
{
    NSInteger promotionID = [self.record.PromotionID integerValue];
    NSString *iptvPackage = self.record.IPTVPackage;
    
    if (promotionID != 0 || iptvPackage.length > 0) {
        return NO;
    }
    return YES;
}

#pragma mark - load image
- (void)getImageWithUrl:(NSString *)url title:(NSString *)title {
    
    [self showHUDWithMessage:@"Đang tải ảnh!!!"];
    ShareData *shared = [ShareData instance];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    NSString *str = StringFormat(@"Images/%@",url);
    [dict setObject:str forKey:@"Path"];
    //urlimage
    [shared.promotionProgramProxy getImage:dict completeionHander:^(id result, NSString *errorCode, NSString *message) {
        
        [self hideMBProcess];
        [self hideHUD];
        if(![errorCode isEqualToString:@"0"] || result == nil ) {
            [self hideMBProcess];
            return ;
        }
        [self hideMBProcess];
        NSString *strData = StringFormat(@"%@",[result objectForKey:@"Image"]);
        if ([strData isEqualToString:@"<null>"]) {
            return;
        }
        
        //StevenNguyen - compare String's title & String's Image or Image Info or Image Signature
        if (title == rc.Image) {
            self.imageSurvey.image = [self decodeBase64ToImage:strData title:title];
        } else if (title == rc.imageInfo) {
            self.imageInfo.image = [self decodeBase64ToImage:strData title:title];
        } else if (title == rc.imageSignature) {
            self.imageSignature.image = [self decodeBase64ToImage:strData title:title];
        }
        
    } errorHandler:^(NSError *error) {
        
    }];
    [self hideMBProcess];
    [self hideHUD];
}

#pragma mark - get image from binary string
- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData title:(NSString *)title{
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    
    return [UIImage imageWithData:data];
    
}

-(NSInteger)loadLocalType:(NSString *)promotionID Package:(NSString *)package {
    if(![promotionID isEqualToString:@"0"]){
        return 0;
    }
    if(![package isEqualToString:@""] && [promotionID isEqualToString:@"0"]){
        return 1;
    }
    if((![package isEqualToString:@""]) && (![promotionID isEqualToString:@"0"])){
        return 2;
    }
    if ([package isEqualToString:@""] && ([promotionID isEqualToString:@"0"])) {
        return 4;
    }
    return 3;
}

#pragma mark - event button click
-(IBAction)btnUpdate_clicked:(id)sender {
    //vutt11
    [self btnCancel_clicked:self];
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    if([rc.StatusDeposit isEqualToString:@"1"]){
        [self showAlertBox:@"Thông Báo" message:@"Phiếu đăng ký đã thu tiền"];
        return;
        
    }
    // Nếu là PĐK bán thêm thì show view cập nhật PĐK bán thêm
    if (![rc.regType isEqual:@"0"]) {
        RegistrationFormSaleMorePageViewController *vc = [[RegistrationFormSaleMorePageViewController alloc] init];
        vc.viewType = UpdateRegisterForm;
        vc.updateRegistrationContractModel = [[UpdateRegistrationContractModel alloc] initWithRegistrationFormDetailRecord:rc];
        
        [self.navigationController pushViewController:vc animated:YES];
        // Nếu là PĐK mới thì show view cập nhật PĐK mới
        
    }else {
        
        ListRegisteredFormViewController *vc = [[ListRegisteredFormViewController alloc] initWithNibName:@"ListRegisteredFormViewController" bundle:nil];
        
        
        vc.rc = self.record;
        vc.Id = self.Id;
        
        vc.arrayListPacketOld = self.arraylistPacket;
        
        UINavigationController *nav = [((UINavigationController *)self.menuContainerViewController.centerViewController) initWithRootViewController:vc ];
        self.menuContainerViewController.centerViewController = nav;
        
    }
}

- (IBAction)btnUpdateReceipt_Clicked:(id)sender {
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"UpdateReceipt" bundle:nil];
    UpdateReceiptViewController *vc = [[UpdateReceiptViewController alloc] init];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"UpdateReceipt"];
    vc.record  = self.record;
    vc.serviceType = StringFormat(@"%li",(long)selectedLocalType);
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)btnConstructionVote_Clicked:(id)sender {
    ConstructionVoteDetailViewController *vc = [[ConstructionVoteDetailViewController alloc] init];
    vc.contract = rc.Contract;
    [self.navigationController pushViewController:vc animated:YES];
}

-(IBAction)btnTichHen_clicked:(id)sender {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    // add by DanTT2
    if(![rc.ODCCableType isEqualToString:@""] || rc.ODCCableType.length > 0){
        [self ShowTichHen];
        [self btnCancel_clicked:self];
        return;
    }
    [self showAlertBox:@"Thông Báo" message:@"Phiếu đăng ký chưa book port nên không tích hẹn được"];
}

-(void)ShowTichHen {
    ShareData*share =[ShareData instance];
    share.RegCode =self.lblId.text;
    [self showMBProcess];
    [share.appProxy GetSubTeamID:share.currentUser.userName  RegCode:share.RegCode completionHandler:^(id result, NSString *errorCode, NSString *message) {
        
        //[self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self hideMBProcess];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi: %@",message]];
            [self hideMBProcess];
            return ;
        }
        if ([[result objectForKey:@"ListObject"] count] <1) {
            [Common_client showAlertMessage:@"Không có dữ liệu trả về từ server"];
            [self hideMBProcess];
            return;
        }
        
        TichHenPoup *vc= [[TichHenPoup alloc]initWithNibName:@"TichHenPoup" bundle:nil];
        vc.delegate = (id)self;
        vc.arrList = [result objectForKey:@"ListObject"]?:nil;
        vc.RegCode =self.lblId.text;
        
        [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomTop];
        [self hideMBProcess];
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}

- (IBAction)btn_DSTichHen_Clicked:(id)sender {
    [self btnCancel_clicked:self];
    [self showMBProcess];
    [self btnCancel_clicked:self];
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    
    ShareData *share=[ShareData instance];
    //share.RegCode =self.lblId.text;
    NSMutableDictionary* dataInput = [NSMutableDictionary dictionary];
    [dataInput setValue: share.currentUser.userName forKey:@"UserName"];
    //[dataInput setValue: share.RegCode forKey:@"RegCode"];
    [dataInput setValue: self.record.RegCode forKey:@"RegCode"];
    
    [share.appProxy GetDeployAppointmentList:dataInput completionHandler:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        if ([result count] >0) {
            ListTichHenController *vc = [[ListTichHenController alloc]initWithNibName:@"ListTichHenController" bundle:nil];
            UINavigationController *nav = [((UINavigationController *)self.menuContainerViewController.centerViewController) initWithRootViewController:vc ];
            vc.arrList =result;
            self.menuContainerViewController.centerViewController = nav;
            
        } else {
            [Common_client showAlertMessage:@"Không có danh sách hẹn "];
        }
        
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}

-(void)UpdateDeployAppointment:(NSMutableDictionary *)dic {
    [self showMBProcess];
    ShareData*shared =[ShareData instance];
    [self CancelTichHenPoup];
    [shared.appProxy UpdateDeployAppointment:dic completionHandler:^(id result, NSString *errorCode, NSString *message) {
        //[self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        NSString *mess =[result[0] objectForKey:@"Result"];
        [Common_client showAlertMessage:mess];
        [self hideMBProcess];
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",[error localizedDescription]]];
    }];
}

-(void)CancelTichHenPoup {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
    
}

//btn Book Port
-(IBAction)btnBookPort_clicked:(id)sender {
    [self showMBProcess];
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    
    if ([ShareData instance].currentUser.autoBookPort) {
        
        AutoBookPortViewController *autoVC = [[AutoBookPortViewController alloc] initWithNibName:@"AutoBookPortViewController" bundle:nil];
        autoVC.router.dataDestination.registrationFromDetailRecord = self.record;
        autoVC.router.dataDestination.txtLocation = rc.TDName;
        [self hideMBProcess];
//        [self presentViewController:autoVC animated:TRUE completion:^{}];
        [self.navigationController pushViewController:autoVC animated:TRUE];
        
    } else {
        
        MainMap *vc = [[MainMap alloc]initWithNibName:@"MainMap" bundle:nil];
        vc.record = self.record;
        vc.Flat = @"1";
        vc.regcode = self.record.RegCode;
        vc.arrRecord = self.record;
        vc.txtLocationPB=rc.TDName;
        [self hideMBProcess];
        [self.navigationController pushViewController:vc animated:TRUE];
        
    }

}

//btn khảo sát
-(IBAction)btnSurvey_clicked:(id)sender {
    //vutt11
    [self showMBProcess];
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    //if(![rc.TDName isEqualToString:@""]){
    if(![rc.ODCCableType isEqualToString:@""] || rc.ODCCableType.length >0){
        Survey *vc   = [[Survey alloc]initWithNibName:@"Survey" bundle:nil];
        vc.delegate  = (id)self;
        vc.arrRecord = self.record;
        vc.image     = self.imageSurvey.image;
        [self hideMBProcess];
        [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomTop];
        [self btnCancel_clicked:self];
        return;
    }
    [self hideMBProcess];
    [self showAlertBox:@"Thông Báo" message:@"Phiếu đăng ký chưa book port nên không được khảo sát"];
}

#pragma mark - Navigation
-(void)setType{
    
    self.btnRecieve.layer.cornerRadius = 5;
    self.tvNote.layer.borderColor= [UIColor colorWithRed:0.133 green:0.698 blue:0.569 alpha:1].CGColor;
    self.tvNote.layer.borderWidth = 1.f;
    self.tvNote.layer.cornerRadius = 6.0f;
    
    self.txtDeploymentNote.layer.borderColor= [UIColor colorWithRed:0.133 green:0.698 blue:0.569 alpha:1].CGColor;
    self.txtDeploymentNote.layer.borderWidth = 1.f;
    self.txtDeploymentNote.layer.cornerRadius = 6.0f;
    
    self.lblBonus.layer.borderColor= [UIColor colorWithRed:0.133 green:0.698 blue:0.569 alpha:1].CGColor;
    self.lblBonus.layer.borderWidth = 1.f;
    self.lblBonus.layer.cornerRadius = 6.0f;
    
    self.txtTDAddress.layer.borderColor= [UIColor colorWithRed:0.133 green:0.698 blue:0.569 alpha:1].CGColor;
    self.txtTDAddress.layer.borderWidth = 1.f;
    self.txtTDAddress.layer.cornerRadius = 6.0f;
    self.txtTDAddress.text = @"";
    
    //vutt11
    self.packetServiceOffice365.layer.borderColor= [UIColor colorWithRed:0.133 green:0.698 blue:0.569 alpha:1].CGColor;
    self.packetServiceOffice365.layer.borderWidth = 1.f;
    self.packetServiceOffice365.layer.cornerRadius = 6.0f;
    self.packetServiceOffice365.text = @"";
    
}

#pragma mark - menu

- (void)rightSideMenuButtonPressed:(id)sender {
    
    if(statusmenu  == 0){
        [menuView setShadow];
        [self.view addSubview:menuView];
        [UIView animateWithDuration:0.4
                              delay:0.1
                            options: UIViewAnimationOptionTransitionNone
                         animations:^{
                             
                             menuView.frame = CGRectMake(0, [SiUtils getScreenFrameSize].height - menuView.frame.size.height, menuView.frame.size.width, menuView.frame.size.height);
                         }
                         completion:^(BOOL finished){
                             
                         }];
        
        statusmenu = 1;
        
        return;
    }
    
    if(statusmenu == 1){
        [UIView animateWithDuration:0.4
                              delay:0.1
                            options: UIViewAnimationOptionTransitionNone
                         animations:^{
                             
                             menuView.frame = CGRectMake(0, [SiUtils getScreenFrameSize].height, menuView.frame.size.width, menuView.frame.size.height);
                         }
                         completion:^(BOOL finished){
                             
                         }];
        
        statusmenu = 0;
    }
    
}
#pragma mark - event button

-(IBAction)btnCancel_clicked:(id)sender{
    
    [UIView animateWithDuration:0.4
                          delay:0.1
                        options: UIViewAnimationOptionTransitionNone
                     animations:^{
                         
                         menuView.frame = CGRectMake(0, [SiUtils getScreenFrameSize].height, menuView.frame.size.width, menuView.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         
                     }];
    
    statusmenu = 0;
    
}

//Thu tiên bán thêm, thu tiền bán mơi, lên hợp đồng.
-(IBAction)btnRecieve_clicked:(id)sender{
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    if([statusDeposit isEqualToString: @"1"]){
        if (self.record.Contract.length <= 0 && shared.currentUser.IsCreateContract > 0) {
            if([rc.ODCCableType isEqualToString:@""] || rc.ODCCableType.length <= 0){
                [self showAlertBox:@"Thông Báo" message:@"Phiếu đăng ký chưa book port nên không được lên hợp đồng"];
                return;
            }
            UIActionSheet *noticeSave = [[UIActionSheet alloc] initWithTitle:@"Bạn chắc chắn muốn lên hợp đồng cho phiếu đăng ký này?" delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
            [noticeSave showInView:[UIApplication sharedApplication].keyWindow];
            return;
        }
        [self showAlertBox:@"Thông báo" message:@"Hợp đồng đã được thanh toán"];
        
    } else {
        PayViewController *vc = [[PayViewController alloc]initWithNibName:@"PayViewController" bundle:nil];
        vc.delegate = (id)self;
        vc.Regcode = self.lblId.text;
        vc.record = rc;
        // [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomTop];
        [self presentViewController:vc animated:TRUE completion:nil];
    }
}
#pragma mark - Lên hợp đồng
- (void) createContract {
    ShareData *shared = [ShareData instance];
    [self showMBProcess];
    [shared.registrationFormProxy CreateObject:shared.currentUser.userName RegCode:self.lblId.text Completehander:^(id result, NSString *errorCode, NSString *message) {
        [self showAlertBox:@"Thông Báo" message:message];
        [self hideMBProcess];
        if (errorCode > 0) {
            [self LoadData];
        }
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Lỗi" message:[error description]];
        [self hideMBProcess];
    }];
}

#pragma refresh scroll view
- (void)refresh:(UIRefreshControl *)refreshControl {
    // load data
    [self LoadData];
    [refreshControl endRefreshing];
}

#pragma mark - delegate
-(void)CancelPopup{
    //vutt11 fixed
    //[self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
    [self dismissViewControllerAnimated:TRUE completion:nil];
    [self tapOnScreen:nil];
    [self LoadData];
}

-(void)CancelPopupSurvey{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
    [self tapOnScreen:nil];
    [self LoadData];
}

// add by DanTT2

- (void)tapOnScreen:(UITapGestureRecognizer *)sender{
    [UIView animateWithDuration:0.4
                          delay:0.1
                        options: UIViewAnimationOptionTransitionNone
                     animations:^{
                         
                         menuView.frame = CGRectMake(0, [SiUtils getScreenFrameSize].height, menuView.frame.size.width, menuView.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         
                     }];
    
    statusmenu = 0;
    
    
}

#pragma mark - UIActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == 0) {
        [self createContract];
    }
    
}

- (void)brownserImage:(UIGestureRecognizer *)gestureItem {
    //vutt11
    
    //check gestureRecognizer
    UIView* viewGesture = [gestureItem view];
    
    //StevenNguyen - check UIImageView's tag
    if (viewGesture.tag == self.imageInfo.tag) {
        NSLog(@"imageInfo");
        photoImage = [[MWPhoto alloc] initWithImage:self.imageInfo.image];
        photoImage.caption = rc.Image;
        arrPhoto = [[NSMutableArray alloc] init];
        [arrPhoto addObject:photoImage];
        
        
    } else if (viewGesture.tag == self.imageSignature.tag) {
        NSLog(@"imageSignature");
        photoImage = [[MWPhoto alloc] initWithImage:self.imageSignature.image];
        photoImage.caption = rc.Image;
        arrPhoto = [[NSMutableArray alloc] init];
        [arrPhoto addObject:photoImage];
        
        
    } else if (viewGesture.tag == self.imageSurvey.tag) {
        NSLog(@"imageSurvey");
        photoImage = [[MWPhoto alloc] initWithImage:self.imageSurvey.image];
        photoImage.caption = rc.Image;
        arrPhoto = [[NSMutableArray alloc] init];
        [arrPhoto addObject:photoImage];
        
    }
    
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithPhotos:arrPhoto];
    //    browser.delegate = self;
    
    // Set options
    browser.displayActionButton = YES; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    // Optionally set the current visible photo before displaying
    //    [browser setCurrentPhotoIndex:indexItem];
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    // Manipulate
    [browser showNextPhotoAnimated:YES];
    [browser showPreviousPhotoAnimated:YES];
    //vutt11
    [browser setCurrentPhotoIndex:4];
    
    
}

- (IBAction)btnImageInfo_clicked:(id)sender {
    
    [self getImageWithUrl:rc.imageInfo title:rc.imageInfo];
    
    
}

- (IBAction)btnImageSignature_clicked:(id)sender {
    
    if ([rc.imageSignature  isEqualToString: @""]) {
        return;
    }
    
    [self getImageWithUrl:rc.imageSignature title:rc.imageSignature];
    
    
}

//MARK: -DynamicMenuViewDelegate
-(void)pressedButtonDynamicInMenu:(SNDynamicMenuView)dynamic {
    
    switch (dynamic) {
        case SNDynamicMenuViewBookPort: {
            [self btnBookPort_clicked:nil];
            break;
        }
        case SNDynamicMenuViewSurvey: {
            [self btnSurvey_clicked:nil];
            break;
        }
        case SNDynamicMenuViewAppointment: {
            [self btnTichHen_clicked:nil];
            break;
        }
        case SNDynamicMenuViewUpdate: {
            [self btnUpdate_clicked:nil];
            break;
        }
        case SNDynamicMenuViewEarningMoney: {
            [self btnRecieve_clicked:nil];
            break;
        }
        case SNDynamicMenuViewUpdateAmount: {
            [self btnUpdateReceipt_Clicked:nil];
            break;
        }
        case SNDynamicMenuViewConstructVote: {
            [self btnConstructionVote_Clicked:nil];
            break;
        }
        default: {/* Huy */
            [self btnCancel_clicked:nil];
            break;
        }
    }
    
}


@end
