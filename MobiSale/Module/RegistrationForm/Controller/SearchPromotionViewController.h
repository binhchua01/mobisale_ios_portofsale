//
//  SearchPromotionViewController.h
//  MobiSale
//
//  Created by ISC-DanTT on 4/4/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PromotionModel.h"

@protocol SearchPromotionDelegate <NSObject>

@required

- (void)cancelSearchPromotionView:(PromotionModel *)selectedPromo;

@end

@interface SearchPromotionViewController : UIViewController <UISearchBarDelegate,UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@property (strong, nonatomic) IBOutlet UITableView *resultTableView;

@property (nonatomic, strong) NSArray *arrData;

@property (nonatomic, strong) PromotionModel *promotionModel;

@property (nonatomic, retain) id<SearchPromotionDelegate> delegate;

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintBottomTableView;

- (IBAction)btnCancel_Clicked:(id)sender;

@end
