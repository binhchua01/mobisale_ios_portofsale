//
//  ListRegistrationFormPageViewController.h
//  MobiSale
//
//  Created by ISC on 8/23/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "KeyValueModel.h"
#import "ViewPagerController.h"
#import "ListRegisteredFormController.h"
#import "RegisteredFormController.h"
#import "SaleMoreServiceViewController.h"

@interface ListRegistrationFormPageViewController : ViewPagerController

// Tiêu đề của các tab
@property (strong, nonatomic) NSMutableArray *tabArray;
@property (strong, nonatomic) KeyValueModel *tabCurrentView;


@end
