//
//  RegisteredFormController.m
//  MobiSales
//
//  Created by HIEUPC on 1/22/15.
//  Copyright (c) 2015 Aryan Ghassemi. All rights reserved.
//

#import "RegisteredFormController.h"
#import "KeyValueModel.h"
#import "PromotionModel.h"
#import "Common.h"
#import "ShareData.h"
#import "BoxRecord.h"
#import "DetailRegisteredForm.h"
#import "ListRegisteredFormController.h"
#import "ListRegistrationFormPageViewController.h"
#import "Common_client.h"
#import "SuggestionsSearchViewController.h"
#import "SearchPromotionViewController.h"
#import "IPTVPrice.h"
#import "UIImage+FPTCustom.h"


@interface RegisteredFormController () <SuggestionsDelegate, SearchPromotionDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (strong, nonatomic) Office365ViewController *office365ViewController;
@property (strong, nonatomic) UIImagePickerController *imagePickerController;
@property (weak, nonatomic) IBOutlet UIImageView *img;
@property (strong, nonatomic) UIImage *image;
@property (strong, nonatomic) NSString *imageInfo;

@end

@implementation RegisteredFormController {
    
    IPTVPrice *iptvPrice;
    BOOL getIPTVPriceSuccess;
    BOOL isGetIPTVTotal;
    CGFloat ConstraintTopAmountTotal;
    NSString *imageData;
    
    KeyValueModel
    *selectedPhone1 ,
    *selectedPhone2 ,
    *selectedObjectCustomer ,
    *selectedISP ,
    *selectedTypeCustomer ,
    *selectedPlaceCurrent ,
    *selectedSolvency ,
    *selectedClientPartners ,
    *selectedLegal ,
    *selectedDistrict ,
    *selectedWard ,
    *selectedTypeHouse ,
    *selectedStreet ,
    *selectedCombo ,
    *selectedDeployment ,
    *selectedDeposit,
    *selectedDepositBlackPoint,
    *selectedServicePack,
    *selectedNameVilla,
    *selectedCusType,
    *selectedLocalType,
    *selectedPackageIPTV,
    *selectedPosition,
    *selectedPaymentType,
    *registedOffice;
    
    
    PromotionModel
    *selectedPromotionIPTV,
    *selectedPromotionIPTVBoxOrder,
    *selectedPromotion;
    
    NSString *TypeDeployment, *TypeDrillWall, *Address, *BoxId1, *BoxId2, *BoxId3, *BoxId4, *CableStatus, *InsCable, *Contract , *BoxQuanlity, *PrepaidTotal, *DeviceTotal;
    NSString *stringCount;
    NSInteger deposit, amountInternet, amountIPTV, amountTotal, office365Total,amountOTT;
    bool flagtotaliptv,flagAlert;
    NSString *ID;
    
    UIAlertView * updateCheck, *updateSuccess;
    
    //MPGTextField *txtPromotionNet;
    
    UIButton *btnSuggestionHouseNumber;
    int i;
    
 
}

enum actionSheetTagType {
    back = 1,
    save = 2,
    datePicker = 3
};

enum tableSourceStype {
    Phone1 = 1,
    Phone2 = 2,
    ObjectCustomer = 3,
    ISP = 4,
    TypeCustomer = 5,
    PlaceCurrent = 6,
    Solvency = 7,
    ClientPartners = 8,
    Legal = 9,
    District = 10,
    Ward = 11,
    TypeHouse = 12,
    Street = 13,
    ServicePack = 14,
    Combo = 15,
    Deployment = 16,
    Deposit = 17,
    Promotion = 18,
    NameVilla = 19,
    Position = 20,
    CusType = 21,
    DepositBlackPoint = 22,
    PackageIPTV = 23,
    PromotionIPTV = 24,
    LocalType = 25,
    PaymentType = 26,
    PromotionIPTVBoxOrder = 27
};

@synthesize arrPhone1;
@synthesize arrPhone2;
@synthesize arrObjectCustomer;
@synthesize arrISP;
@synthesize arrTypeCustomer;
@synthesize arrPlaceCurrent;
@synthesize arrSolvency;
@synthesize arrClientPartners;
@synthesize arrLegal;
@synthesize arrDistrict;
@synthesize arrWard;
@synthesize arrTypeHouse;
@synthesize arrStreet;
@synthesize arrServicePack;
@synthesize arrCombo;
@synthesize arrDeployment;
@synthesize arrDeposit;
@synthesize arrPromotion;
@synthesize arrBonusBox;
@synthesize arrNameVilla;
@synthesize arrPosition;
@synthesize arrCusType;
@synthesize arrDepositBlackPoint;
@synthesize arrPackageIPTV;
@synthesize arrPromotionIPTV;
@synthesize arrPromotionIPTVBoxOrder;
@synthesize arrLocalType;
@synthesize arrayListPacketOld;

UIView *contentView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.btnUpdate.layer.cornerRadius = 5;
    registedOffice = [[KeyValueModel alloc] init];
    iptvPrice = [[IPTVPrice alloc] init];
    getIPTVPriceSuccess = NO;
    
    flagtotaliptv = TRUE;
    flagAlert = FALSE;
    TypeDeployment = @"0";
    TypeDrillWall  = @"0";
    CableStatus    = @"2";
    InsCable       = @"2";
    //OTT numberBOX
    self.maximum = 100;
    self.minimum = 0;
    [self.registedOTT setOn:NO];
    [self.registedOTT setEnabled:NO];
    [self.btnMaxBoxOTT setEnabled:NO];
    [self.btnMinBoxOTT setEnabled:NO];
    //Office 365
    [self.registedOffice365Switch setOn:NO];
   // self.viewOTT.backgroundColor = [UIColor groupTableViewBackgroundColor];
    //Create ScrollView
    [self.scrollView setBackgroundColor:[UIColor clearColor]];
    [self.scrollView setCanCancelContentTouches:NO];
    self.scrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    
    [self.scrollView setScrollEnabled:YES];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(office365AmountChanged:) name:kAmountOfficeChangedNotification object:nil];
    /*
     *add date picker
     */
    
    self.dateFormat = @"yyyy-MM-dd";
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnView:)];
    
    [self.view addGestureRecognizer:tap];
    self.btnISP.enabled = NO;
    
    // add right bar button save
    UIImage *image = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"save_32_2"] height:30];
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc]
                                       initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered
                                       target:self
                                       action:@selector(rightBarButton_Clicked:)];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    
    self.navigationItem.leftBarButtonItem = [self backBarButtonItem];
    
    [self setType];
    
    if(self.Id.length <= 0 || [self.Id isEqualToString:@"0"]){
        self.title  = @"TẠO PHIẾU ĐĂNG KÝ";
        // Disable office 365
        [self.registedOffice365Switch setOn:NO];
        self.office365Button.enabled = NO;
        [self registedOffice365:NO];
        
        self.rdNoCableStatus.selected = YES;
        self.rdNoInsCable.selected = YES;
        
        self.rdNoRequest.selected = YES;
        self.rdNoWallDrilling.selected = YES;
        
        [self LoadLocalTypePost:@"0" serverPackageID:@"0" PromotionId:@"0"];
        [self LoadCurrenPlace:@"0"];
        [self LoadLegal:@"0"];
        if (self.rc.ISPType.length > 0 && ![self.rc.ISPType isEqualToString:@"0"]) {
            [self LoadObjectCustomer:@"2"];
            self.btnISP.enabled = YES;
        } else {
            [self LoadObjectCustomer:@"0"];
        }
        [self LoadPhone1:@"4"];
        [self LoadPhone2:@"4"];
        [self LoadSolveny:@"0"];
        [self LoadClientPartner:@"0"];
        [self LoadDeployment:@"0"];
        [self LoadDeposit:@"0"];
        [self LoadDepositBlackPoint:@"0"];
        [self LocaLocalType:@"0" Package:@""];
        [self LoadCusType:@"0"];
        [self LoadCombo:@"0"];
        [self.btnCombo setEnabled:NO];
        if (self.rc.PotentialID.length > 0 && ![self.rc.PotentialID isEqualToString:@"0"]) {
            [self showMBProcess];
            self.txtFullName.text = self.rc.FullName;
            self.txtCMND.text = self.rc.Passport;
            self.txtMST.text = self.rc.TaxId;
            self.txtPhone1.text = self.rc.Phone_1;
            self.txtPersonContact1.text = self.rc.Contact_1;
            self.txtPhone2.text = self.rc.Phone_2;
            self.txtPersonContact2.text = self.rc.Contact_2;
            self.txtPersonContact.text = self.rc.Contact;
            self.txtEmail.text = self.rc.Email;
            
            [self LoadISP:self.rc.ISPType];
            [self LoadDistrict:self.rc.BillTo_District WardId:self.rc.BillTo_Ward Street:self.rc.BillTo_Street];
            [self LoadTypeHouse:self.rc.TypeHouse];
            [self LoadPosition:self.rc.Position];
            [self LoadTypeCustomer:self.rc.CusTypeDetail];
            
            [self loadPaymentType:@"0"];
            
            self.txtBilltoNumber.text = self.rc.BillTo_Number;
            self.txtLot.text = self.rc.Lot;
            self.txtFloor.text = self.rc.Floor;
            self.txtRoom.text = self.rc.Room;
            self.txtNote.text = self.rc.Note;
            [self.btnNameVilla setTitle:self.rc.NameVilla forState:UIControlStateNormal];
        } else {
            [self showMBProcess];
            [self LoadISP:@"0"];
            [self LoadDistrict:@"0" WardId:@"0" Street:@"0"];
            [self LoadTypeHouse:@"0"];
            [self LoadPosition:@"0"];
            [self LoadTypeCustomer:@"12"];
        }
        
    } else {
        self.title  = @"CẬP NHẬT PHIẾU ĐK";
        // Load office 365 data.
        office365Total = self.rc.office365Total;

        if (office365Total > 0) {
            // Enable office 365
            [self.registedOffice365Switch setOn:YES];
            [self registedOffice365:YES];
            i = 3;

        }
        amountOTT = self.rc.OTTTotal;
        if (amountOTT > 0) {
            
            [self.registedOTT setOn:YES];
            [self.registedOTT setEnabled:YES];
            [self.btnMaxBoxOTT setEnabled:YES];
            [self.btnMinBoxOTT setEnabled:YES];
            i = 4;
        }
        // Load data.
        [self LoadData];
        
    }
    
    [self ShowHideSelecttypeHouse];
    
    self.txtIPTVPrepaid.delegate = (id)self;
    self.screenName = self.title;
    
    
    self.img.image       = self.image;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)tapOnView:(UITapGestureRecognizer *)sender {
    [self.scrollView endEditing:YES];
}

-(void)viewDidLayoutSubviews{
    
    //[self.scrollView setContentSize:CGSizeMake([SiUtils getScreenFrameSize].width, 3720)];
    [self.scrollView setScrollEnabled:TRUE];
    
}

- (UIBarButtonItem *)backBarButtonItem{
    UIImage *image = [SiUtils imageWithImageHeight:[[UIImage imageNamed:@"ic_pre"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] height:30];
    return [[UIBarButtonItem alloc]
            initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered
            target:self
            action:@selector(backButtonPressed:)];
}

#pragma mark - UIBarButtonItem Callbacks
- (void)backButtonPressed:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Bạn chắc chắn muốn thoát?" delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
    actionSheet.tag = back;
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}

#pragma Mark - Office 365 Amount changed notification.
- (void)office365AmountChanged:(NSNotificationCenter *)notification
{
    office365Total = _office365ViewController.amountTotal;
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    self.office365Amount.text = [nf stringFromNumber:[NSNumber numberWithFloat: office365Total]];
    // Tính tổng tiền PĐK.
    [self Total];
    // Set đã đăng ký Office 365.
    [self registedOffice365:YES];
}

- (void)registedOffice365:(BOOL)status
{
    if (status) {
        registedOffice.Key    = @"1";
        registedOffice.Values = @"[Xem thông tin Office đã đăng ký]";
    } else {
        registedOffice.Key    = @"0";
        registedOffice.Values = @"[Vui lòng đăng ký Office]";
    }
    
    [self.office365Button setTitle:registedOffice.Values forState:UIControlStateNormal];
}
// Office365ViewControllerDelegate methods
- (void)cancelOffice365View
{
   
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - DropDown view

- (void)setupDropDownView: (NSInteger) tag {
    [self.view endEditing:YES];
    
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.tag = tag;
    poplistview.listView.scrollEnabled = TRUE;
    switch (tag) {
        case Phone1:
            [poplistview setTitle:NSLocalizedString(@"Loại điện thoại", @"")];
            break;
        case Phone2:
            [poplistview setTitle:NSLocalizedString(@"Loại điện thoại", @"")];
            break;
        case ObjectCustomer:
            [poplistview setTitle:NSLocalizedString(@"Đối tượng khách hàng", @"")];
            break;
        case ISP:
            [poplistview setTitle:NSLocalizedString(@"ISP", @"")];
            break;
        case TypeCustomer:
            [poplistview setTitle:NSLocalizedString(@"Loại khách hàng", @"")];
            break;
        case PlaceCurrent:
            [poplistview setTitle:NSLocalizedString(@"Nơi ở hiện tại", @"")];
            break;
        case Solvency:
            [poplistview setTitle:NSLocalizedString(@"Khả năng thanh toán", @"")];
            break;
        case ClientPartners:
            [poplistview setTitle:NSLocalizedString(@"Khách hàng của đối tác", @"")];
            break;
        case Legal:
            [poplistview setTitle:NSLocalizedString(@"Pháp nhân", @"")];
            break;
        case District:
            [poplistview setTitle:NSLocalizedString(@"Quận/Huyện", @"")];
            break;
        case Ward:
            [poplistview setTitle:NSLocalizedString(@"Phường/Xã", @"")];
            break;
        case TypeHouse:
            [poplistview setTitle:NSLocalizedString(@"Loại nhà", @"")];
            break;
        case Street:
            [poplistview setTitle:NSLocalizedString(@"Đường", @"")];
            break;
        case ServicePack:
            [poplistview setTitle:NSLocalizedString(@"Gói dịch vụ", @"")];
            break;
        case Combo:
            [poplistview setTitle:NSLocalizedString(@"Combo", @"")];
            break;
        case Deployment:
            [poplistview setTitle:NSLocalizedString(@"Hình thức triển khai", @"")];
            break;
        case Deposit:
            [poplistview setTitle:NSLocalizedString(@"Đặt cọc",@"")];
            break;
        case Promotion:
            [poplistview setTitle:NSLocalizedString(@"Chọn khuyến mãi",@"")];
            break;
        case NameVilla:
            [poplistview setTitle:NSLocalizedString(@"Chọn chung cư",@"")];
            break;
        case Position:
            [poplistview setTitle:NSLocalizedString(@"Chọn vị trí nhà",@"")];
            break;
        case CusType:
            [poplistview setTitle:NSLocalizedString(@"Chọn loại hình",@"")];
            break;
        case DepositBlackPoint:
            [poplistview setTitle:NSLocalizedString(@"Chọn Đặt cọc điểm đen",@"")];
            break;
        case PackageIPTV:
            [poplistview setTitle:NSLocalizedString(@"Chọn gói dịch vụ",@"")];
            break;
        case PromotionIPTV:
            [poplistview setTitle:NSLocalizedString(@"Chọn khuyến mãi",@"")];
            break;
        case PromotionIPTVBoxOrder:
            [poplistview setTitle:NSLocalizedString(@"Chọn khuyến mãi",@"")];
            break;
        case LocalType:
            [poplistview setTitle:NSLocalizedString(@"Chọn loại dịch vụ",@"")];
            break;
        case PaymentType:
            [poplistview setTitle:NSLocalizedString(@"Chọn hình thức thanh toán", @"")];
            break;
        default:
            break;
    }
    
    [poplistview show];
    
}

- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

#pragma mark - UIPopoverListViewDataSource

- (void)setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell tag:(NSInteger)tag{
    KeyValueModel *model;
    PromotionModel *promotionmodel;
    cell.textLabel.numberOfLines = 0;
    [cell.textLabel sizeToFit];
    switch (tag) {
        case Phone1:
            model = [self.arrPhone1 objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Phone2:
            model = [self.arrPhone2 objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case ObjectCustomer:
            model = [self.arrObjectCustomer objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case ISP:
            model = [self.arrISP objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case TypeCustomer:
            model = [self.arrTypeCustomer objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case PlaceCurrent:
            model = [self.arrPlaceCurrent objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Solvency:
            model = [self.arrSolvency objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case ClientPartners:
            model = [self.arrClientPartners objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Legal:
            model = [self.arrLegal objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case District:
            model = [self.arrDistrict objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Ward:
            model = [self.arrWard objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case TypeHouse:
            model = [self.arrTypeHouse objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Street:
            model = [self.arrStreet objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case ServicePack:
            model = [self.arrServicePack objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Combo:
            model = [self.arrCombo objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Deployment:
            model = [self.arrDeployment objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Deposit:
            model = [self.arrDeposit objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Promotion:
            promotionmodel = [self.arrPromotion objectAtIndex:row];
            cell.textLabel.text = promotionmodel.PromotionName;
            cell.textLabel.font = [UIFont fontWithName:@"Arial" size:13];
            break;
        case NameVilla:
            model = [self.arrNameVilla objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Position:
            model = [self.arrPosition objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case CusType:
            model = [self.arrCusType objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case DepositBlackPoint:
            model = [self.arrDepositBlackPoint objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case PackageIPTV:
            model = [self.arrPackageIPTV objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case PromotionIPTV:
            promotionmodel = [self.arrPromotionIPTV objectAtIndex:row];
            cell.textLabel.text = promotionmodel.PromotionName;
            break;
        case PromotionIPTVBoxOrder:
            promotionmodel = [self.arrPromotionIPTVBoxOrder objectAtIndex:row];
            cell.textLabel.text = promotionmodel.PromotionName;
            break;
        case LocalType:
            model = [self.arrLocalType objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case PaymentType:
            model = [self.arrPaymentType objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        default:
            break;
            
    }
    
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView cellForIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row cell:cell tag:popoverListView.tag];
    cell.textLabel.font = [UIFont fontWithName:@"Arial" size:14];
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = 0;
    switch (popoverListView.tag) {
        case Phone1:
            count = self.arrPhone1.count;
            break;
        case Phone2:
            count = self.arrPhone2.count;
            break;
        case ObjectCustomer:
            count = self.arrObjectCustomer.count;
            break;
        case ISP:
            count = self.arrISP.count;
            break;
        case TypeCustomer:
            count = self.arrTypeCustomer.count;
            break;
        case PlaceCurrent:
            count = self.arrPlaceCurrent.count;
            break;
        case Solvency:
            count = self.arrSolvency.count;
            break;
        case ClientPartners:
            count = self.arrClientPartners.count;
            break;
        case Legal:
            count = self.arrLegal.count;
            break;
        case District:
            count = self.arrDistrict.count;
            break;
        case Ward:
            count = self.arrWard.count;
            break;
        case TypeHouse:
            count = self.arrTypeHouse.count;
            break;
        case Street:
            count = self.arrStreet.count;
            break;
        case ServicePack:
            count = self.arrServicePack.count;
            break;
        case Combo:
            count = self.arrCombo.count;
            break;
        case Deployment:
            count = self.arrDeployment.count;
            break;
        case Deposit:
            count = self.arrDeposit.count;
            break;
        case Promotion:
            count = self.arrPromotion.count;
            break;
        case NameVilla:
            count = self.arrNameVilla.count;
            break;
        case Position:
            count = self.arrPosition.count;
            break;
        case CusType:
            count = self.arrCusType.count;
            break;
        case DepositBlackPoint:
            count = self.arrDepositBlackPoint.count;
            break;
        case PackageIPTV:
            count = self.arrPackageIPTV.count;
            break;
        case PromotionIPTV:
            count = self.arrPromotionIPTV.count;
            break;
        case PromotionIPTVBoxOrder:
            count = self.arrPromotionIPTVBoxOrder.count;
            break;
        case LocalType:
            count = self.arrLocalType.count;
            break;
        case PaymentType:
            count = self.arrPaymentType.count;
            break;
        default:
            break;
    }
    return count;
    
}

#pragma mark - UIPopoverListViewDelegate

- (void) didDropDownSelected:(NSInteger)row tag:(NSInteger)tag {
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    
   
    switch (tag) {
        case Phone1:
            selectedPhone1 = [self.arrPhone1 objectAtIndex:row];
            [self.btnPhone1 setTitle:selectedPhone1.Values forState:UIControlStateNormal];
            break;
        case Phone2:
            selectedPhone2 = [self.arrPhone2 objectAtIndex:row];
            [self.btnPhone2 setTitle:selectedPhone2.Values forState:UIControlStateNormal];
            break;
        case ObjectCustomer:
            selectedObjectCustomer = [self.arrObjectCustomer objectAtIndex:row];
            [self.btnObjectCustomer setTitle:selectedObjectCustomer.Values forState:UIControlStateNormal];
            if([selectedObjectCustomer.Key isEqualToString:@"2"]){
                self.btnISP.enabled = YES;
            }else{
                self.btnISP.enabled = NO;
            }
            break;
        case ISP:
            selectedISP = [self.arrISP objectAtIndex:row];
            [self.btnISP setTitle:selectedISP.Values forState:UIControlStateNormal];
            break;
        case TypeCustomer:
            selectedTypeCustomer = [self.arrTypeCustomer objectAtIndex:row];
            [self.btnTypeCustomer setTitle:selectedTypeCustomer.Values forState:UIControlStateNormal];
            break;
        case PlaceCurrent:
            selectedPlaceCurrent = [self.arrPlaceCurrent objectAtIndex:row];
            [self.btnPlaceCurrent setTitle:selectedPlaceCurrent.Values forState:UIControlStateNormal];
            break;
        case Solvency:
            selectedSolvency = [self.arrSolvency objectAtIndex:row];
            [self.btnSolvency setTitle:selectedSolvency.Values forState:UIControlStateNormal];
            break;
        case ClientPartners:
            selectedClientPartners = [self.arrClientPartners objectAtIndex:row];
            [self.btnClientPartners setTitle:selectedClientPartners.Values forState:UIControlStateNormal];
            break;
        case Legal:
            selectedLegal = [self.arrLegal objectAtIndex:row];
            [self.btnLegal setTitle:selectedLegal.Values forState:UIControlStateNormal];
            break;
        case District:
            selectedDistrict = [self.arrDistrict objectAtIndex:row];
            [self.btnDistrict setTitle:selectedDistrict.Values forState:UIControlStateNormal];
            self.btnDistrict.selected = YES;
            if ([selectedDistrict.Key length] >0 ) {
                [self LoadWard:@"0" Street:@"0"];
                break;
            }
            self.btnDistrict.selected = NO;
            [self.btnWard setTitle:@"" forState:UIControlStateNormal];
            [self.btnStreet setTitle:@"" forState:UIControlStateNormal];
            [self.btnNameVilla setTitle:@"" forState:UIControlStateNormal];
            selectedNameVilla = nil;
            selectedWard = nil;
            selectedStreet = nil;
            selectedNameVilla = nil;
            self.arrWard = nil;
            self.arrStreet = nil;
            self.arrNameVilla = nil;
            return;
            break;
        case Ward:
            selectedWard = [self.arrWard objectAtIndex:row];
            [self.btnWard setTitle:selectedWard.Values forState:UIControlStateNormal];
            self.btnWard.selected = YES;
            [self LoadStreet:@"0"];
            break;
        case Street:
            selectedStreet = [self.arrStreet objectAtIndex:row];
            [self.btnStreet setTitle:selectedStreet.Values forState:UIControlStateNormal];
            self.btnStreet.selected = YES;
            [self LoadNameVilla:@"0"];
            break;
        case ServicePack:
            selectedServicePack = [self.arrServicePack objectAtIndex:row];
            [self.btnServicePack setTitle:selectedServicePack.Values forState:UIControlStateNormal];
            self.btnServicePack.selected = YES;
            [self LoadPromotion:@"0" LocalType:selectedServicePack.Key];
            break;
        case Deployment:
            selectedDeployment = [self.arrDeployment objectAtIndex:row];
            [self.btnDeployment setTitle:selectedDeployment.Values forState:UIControlStateNormal];
            break;
        case Deposit:
            selectedDeposit = [self.arrDeposit objectAtIndex:row];
            [self.btnDeposit setTitle:selectedDeposit.Values forState:UIControlStateNormal];
            selectedDepositBlackPoint = [self.arrDepositBlackPoint objectAtIndex:0];
            [self.btnDepositBlackPoint setTitle:selectedDepositBlackPoint.Values forState:UIControlStateNormal];
            [self Total];
            break;
        case TypeHouse:
            selectedTypeHouse = [self.arrTypeHouse objectAtIndex:row];
            [self.btnTypeHouse setTitle:selectedTypeHouse.Values forState:UIControlStateNormal];
            if(![selectedTypeHouse.Key isEqualToString:@"2"]){
                self.txtFloor.text = @"";
                self.txtRoom.text = @"";
                self.txtLot.text = @"";
            }else if([selectedTypeHouse.Key isEqualToString:@"2"]){
                self.txtFloor.text = self.rc.Floor;
                self.txtRoom.text = self.rc.Room;
                self.txtLot.text = self.rc.Lot;
            }
            
            [self ShowHideSelecttypeHouse];
            break;
        case Promotion:
            selectedPromotion = [self.arrPromotion objectAtIndex:row];
            [self.btnPromotion setTitle:selectedPromotion.PromotionName forState:UIControlStateNormal];
            amountInternet = [selectedPromotion.Amount intValue];
            self.txtInternetTotal.text = [nf stringFromNumber:[NSNumber numberWithFloat:amountInternet]];
            self.txtPromotionInternet.text = selectedPromotion.PromotionName;
            [self Total];
            break;
        case NameVilla:
            selectedNameVilla = [self.arrNameVilla objectAtIndex:row];
            [self.btnNameVilla setTitle:selectedNameVilla.Values forState:UIControlStateNormal];
            break;
        case Position:
            selectedPosition = [self.arrPosition objectAtIndex:row];
            [self.btnPosition setTitle:selectedPosition.Values forState:UIControlStateNormal];
            break;
        case CusType:
            selectedCusType = [self.arrCusType objectAtIndex:row];
            [self.btnCusType setTitle:selectedCusType.Values forState:UIControlStateNormal];
            break;
        case DepositBlackPoint:
            selectedDepositBlackPoint = [self.arrDepositBlackPoint objectAtIndex:row];
            [self.btnDepositBlackPoint setTitle:selectedDepositBlackPoint.Values forState:UIControlStateNormal];
            selectedDeposit = [self.arrDeposit objectAtIndex:0];
            [self.btnDeposit setTitle:selectedDeposit.Values forState:UIControlStateNormal];
            [self Total];
            break;
        case PackageIPTV:
            selectedPackageIPTV = [self.arrPackageIPTV objectAtIndex:row];
            [self.btnIPTVPackage setTitle:selectedPackageIPTV.Values forState:UIControlStateNormal];
            self.btnIPTVPackage.selected = YES;
            
            self.rc.IPTVPackage = selectedPackageIPTV.Key;
            
            [self LoadPromotionIPTV:@"0" Package:selectedPackageIPTV.Key boxCount:self.txtDeviceBoxCount.text];
            
            flagtotaliptv = FALSE;
            
            break;
        case PromotionIPTV:
            selectedPromotionIPTV = [self.arrPromotionIPTV objectAtIndex:row];
            [self.btnIPTVPromotionID setTitle:selectedPromotionIPTV.PromotionName forState:UIControlStateNormal];
            self.tvPromotionIPTV.text = selectedPromotionIPTV.PromotionName;
            self.rc.IPTVPromotionID = selectedPromotionIPTV.PromotionId;
            //Kiểm trả số tiền trả trước khi chọn câu lệnh khuyến mãi
            [self CheckPromotionIPTV];
            flagtotaliptv = FALSE;
            break;
        case PromotionIPTVBoxOrder:
            selectedPromotionIPTVBoxOrder = [self.arrPromotionIPTVBoxOrder objectAtIndex:row];
            [self.btnIPTVPromotionIDBoxOrder setTitle:selectedPromotionIPTVBoxOrder.PromotionName forState:UIControlStateNormal];
            self.tvPromotionIPTVBoxOrder.text = selectedPromotionIPTVBoxOrder.PromotionName;
            self.rc.IPTVPromotionIDBoxOrder = selectedPromotionIPTVBoxOrder.PromotionId;
            //Kiểm trả số tiền trả trước khi chọn câu lệnh khuyến mãi
            [self CheckPromotionIPTV];
            flagtotaliptv = FALSE;
            break;
            
        case LocalType:
            
            selectedLocalType = [self.arrLocalType objectAtIndex:row];
            [self.btnLocalType setTitle:selectedLocalType.Values forState:UIControlStateNormal];
           
            self.btnLocalType.selected = YES;
           
            if ([selectedLocalType.Key isEqualToString:@"3"]) {
                // Enable Office365 service
                [self.registedOffice365Switch setOn:YES];
                self.office365Button.enabled = YES;
                // Disable Internet service.
                self.btnServicePack.enabled = NO;
                self.btnPromotion.enabled   = NO;
                [self.registedOTT setEnabled:NO];
            

            } else {
                // Enable Internet service.
                self.btnServicePack.enabled = YES;
                self.btnPromotion.enabled   = YES;
                
            }
            
            [self LoadLocalTypePost:selectedLocalType.Key serverPackageID:@"0" PromotionId:@"0"];
            
            if(![selectedLocalType.Key isEqualToString:@"0"]){
                
                selectedDeployment = [self.arrDeployment objectAtIndex:0];
                
                [self LoadPackageIPTV:selectedPackageIPTV.Key PromotionIPTV:selectedPromotionIPTV.PromotionId boxCount:self.txtDeviceBoxCount.text];
                
            } else {
                selectedDeployment = [[KeyValueModel alloc] initWithName:@"0" description:@""];
            }
            
            if ([selectedLocalType.Key isEqualToString:@"2"]) {
                [self.btnCombo setEnabled:YES];
               [self.registedOTT setEnabled:NO];
                
            } else {
                [self.btnCombo setEnabled:NO];
            }
            
            [self LoadCombo:@"0"];
            
            [self CheckRegisterIPTV];
            
            // Kiểm tra số lượng Box để load CLKM box thứ 2 trở đi
            if ([self.txtDeviceBoxCount.text integerValue] > 1) {
                [self enablePromotionIPTVIDBoxOrder:YES];
            } else {
                [self enablePromotionIPTVIDBoxOrder:NO];
            }
            
            //Kiểm tra gói dịch vụ internet ẩn câu lệnh khuyến mãi
            [self CheckPromotionInternet];
            flagtotaliptv = FALSE;
            
            //vutt11
            //Kiểm tra bật dịch vụ OTT
            if ([selectedLocalType.Key isEqualToString:@"4"]) {
                
                [self.registedOTT setOn:YES];
                [self.registedOTT setEnabled:YES];
                [self.btnMaxBoxOTT setEnabled:YES];
                [self.btnMinBoxOTT setEnabled:YES];
                
                // Disable Internet service.
                self.btnServicePack.enabled = NO;
                self.btnPromotion.enabled   = NO;
                
            } else
            {
                [self.registedOTT setOn:NO];
                self.btnServicePack.enabled = YES;
                self.btnPromotion.enabled   = YES;
            }
            
            break;
            
        case Combo:
            selectedCombo = [self.arrCombo objectAtIndex:row];
            [self.btnCombo setTitle:selectedCombo.Values forState:UIControlStateNormal];
            break;
            
        case PaymentType:
            selectedPaymentType = [self.arrPaymentType objectAtIndex:row];
            [self.btnPaymentType setTitle:selectedPaymentType.Values forState:UIControlStateNormal];
            break;
        default:
            break;
    }
}

#pragma mark - UIPopoverListViewDelegate

- (void)popoverListView:(UIPopoverListView *)popoverListView didSelectIndexPath:(NSIndexPath *)indexPath {
    [self didDropDownSelected:indexPath.row tag:popoverListView.tag];
    
}

#pragma mark - event button

- (IBAction)btnPhone1_clicked:(id)sender {
    [self setupDropDownView:Phone1];
}

- (IBAction)btnPhone2_clicked:(id)sender {
    [self setupDropDownView:Phone2];
}

- (IBAction)btnObjectCustomer_clicked:(id)sender {
    [self setupDropDownView:ObjectCustomer];
}

- (IBAction)btnISP_clicked:(id)sender {
    [self setupDropDownView:ISP];
}

- (IBAction)btnTypeCustomer_clicked:(id)sender {
    [self setupDropDownView:TypeCustomer];
}

- (IBAction)btnPlaceCurrent_clicked:(id)sender {
    [self setupDropDownView:PlaceCurrent];
}

- (IBAction)btnSolvency_clicked:(id)sender {
    [self setupDropDownView:Solvency];
}

- (IBAction)btnClientPartners_clicked:(id)sender {
    [self setupDropDownView:ClientPartners];
}

- (IBAction)btnLegal_clicked:(id)sender {
    [self setupDropDownView:Legal];
}

- (IBAction)btnDistrict_clicked:(id)sender {
    [self setupDropDownView:District];
}

- (IBAction)btnWard_clicked:(id)sender {
    [self setupDropDownView:Ward];
}

- (IBAction)btnTypeHouse_clicked:(id)sender {
    [self setupDropDownView:TypeHouse];
}

- (IBAction)btnStreet_clicked:(id)sender {
    [self setupDropDownView:Street];
}

- (IBAction)btnServicePack_clicked:(id)sender {
    [self setupDropDownView:ServicePack];
}

- (IBAction)btnCombo_clicked:(id)sender {
    [self setupDropDownView:Combo];
}

- (IBAction)btnDeployment_clicked:(id)sender {
    [self setupDropDownView:Deployment];
}

- (IBAction)btnDeposit_cliked:(id)sender {
    [self setupDropDownView:Deposit];
    
}

-(IBAction)btnDepositBlackPoint_cliked:(id)sender {
    [self setupDropDownView:DepositBlackPoint];
}

- (IBAction)btnPromotion_cliked:(id)sender {
    // [self setupDropDownView:Promotion];
    SearchPromotionViewController *vc = [[SearchPromotionViewController alloc] init];
    vc.delegate = self;
    //vc.promotionModel = [[PromotionModel alloc] init];
    vc.promotionModel = selectedPromotion;
    vc.arrData = [self pareArrayForKeyWith:arrPromotion];
    [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomTop];
}

- (void)cancelSearchPromotionView:(PromotionModel *)selectedPromo {
    selectedPromotion = selectedPromo;
    
    [self.btnPromotion setTitle:selectedPromotion.PromotionName forState:UIControlStateNormal];
    self.txtPromotionInternet.text = selectedPromotion.PromotionName;
    /*
     * calculated amount internet after choose promotion statemant
     */
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    amountInternet = [selectedPromotion.Amount intValue];
    self.txtInternetTotal.text = [nf stringFromNumber:[NSNumber numberWithFloat:amountInternet]];
    [self Total];
    
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
}

- (IBAction)btnNameVilla_cliked:(id)sender {
    [self setupDropDownView:NameVilla];
}

- (IBAction)btnPosition_cliked:(id)sender {
    [self setupDropDownView:Position];
}

-(IBAction)btnCusType_Clicked:(UIButton *)sender {
    [self setupDropDownView:CusType];
}

-(IBAction)btnIPTVPackage_Clicked:(UIButton *)sender {
    [self setupDropDownView:PackageIPTV];
}

-(IBAction)btnIPTVPromotionID_Clicked:(id)sender {
    [self setupDropDownView:PromotionIPTV];
}

-(IBAction)btnIPTVPromotionIDBoxOrder_Clicked:(id)sender {
    [self setupDropDownView:PromotionIPTVBoxOrder];
}

-(IBAction)btnLocalType_Clicked:(UIButton *)sende {
    [self setupDropDownView:LocalType];
}

- (IBAction)cbFimPlus_Clicked:(id)sender {
    if(self.cbFimPlus.selected == YES){
        self.cbFimPlus.selected = NO;
        self.txtIPTVFimPlusChargeTimes.text = @"0";
        self.txtIPTVFimPlusPrepaidMonth.text = @"0";
        [self.btnIPTVFimPlusChargeTimesMinus setEnabled:NO];
        [self.btnIPTVFimPlusChargeTimesPlus setEnabled:NO];
        [self.btnIPTVFimPlusPrepaidMonthMinus setEnabled:NO];
        [self.btnIPTVFimPlusPrepaidMonthPlus setEnabled:NO];
        
    }else {
        self.cbFimPlus.selected  = YES;
        
        if(self.rc.IPTVFimPlusChargeTimes != nil && ![self.rc.IPTVFimPlusChargeTimes isEqualToString:@"0"]){
            self.txtIPTVFimPlusChargeTimes.text = self.rc.IPTVFimPlusChargeTimes;
            
        }else {
            self.txtIPTVFimPlusChargeTimes.text = @"1";
        }
        
        if(self.rc.IPTVFimPlusPrepaidMonth != nil){
            self.txtIPTVFimPlusPrepaidMonth.text = self.rc.IPTVFimPlusPrepaidMonth;
            
        }else {
            self.txtIPTVFimPlusPrepaidMonth.text = @"0";
        }
        
        [self.btnIPTVFimPlusChargeTimesMinus setEnabled:YES];
        [self.btnIPTVFimPlusChargeTimesPlus setEnabled:YES];
        [self.btnIPTVFimPlusPrepaidMonthMinus setEnabled:YES];
        [self.btnIPTVFimPlusPrepaidMonthPlus setEnabled:YES];
    }
    flagtotaliptv = FALSE;
}

- (IBAction)cbFimHot_Clicked:(id)sender {
    if(self.cbFimHot.selected == YES){
        self.cbFimHot.selected = NO;
        self.txtIPTVFimHotChargeTimes.text = @"0";
        self.txtIPTVFimHotPrepaidMonth.text = @"0";
        [self.btnIPTVFimHotChargeTimesMinus setEnabled:NO];
        [self.btnIPTVFimHotChargeTimesPlus setEnabled:NO];
        [self.btnIPTVFimHotPrepaidMonthMinus setEnabled:NO];
        [self.btnIPTVFimHotPrepaidMonthPlus setEnabled:NO];
        
    }else {
        self.cbFimHot.selected  = YES;
        
        if(self.rc.IPTVFimHotChargeTimes != nil && ![self.rc.IPTVFimHotChargeTimes isEqualToString:@"0"]){
            self.txtIPTVFimHotChargeTimes.text = self.rc.IPTVFimHotChargeTimes;
            
        }else {
            self.txtIPTVFimHotChargeTimes.text = @"1";
        }
        
        if(self.rc.IPTVFimHotPrepaidMonth != nil){
            self.txtIPTVFimHotPrepaidMonth.text = self.rc.IPTVFimHotPrepaidMonth;
            
        }else {
            self.txtIPTVFimHotPrepaidMonth.text = @"0";
        }
        
        [self.btnIPTVFimHotChargeTimesMinus setEnabled:YES];
        [self.btnIPTVFimHotChargeTimesPlus setEnabled:YES];
        [self.btnIPTVFimHotPrepaidMonthMinus setEnabled:YES];
        [self.btnIPTVFimHotPrepaidMonthPlus setEnabled:YES];
    }
    flagtotaliptv = FALSE;
}

-(IBAction)cbKPlus_Clicked:(UIButton *)sender {
    if(self.cbKPlus.selected == YES){
        self.cbKPlus.selected = NO;
        self.txtIPTVKPlusChargeTimes.text = @"0";
        self.txtIPTVKPlusPrepaidMonth.text = @"0";
        [self.btnIPTVKPlusChargeTimesMinus setEnabled:NO];
        [self.btnIPTVKPlusChargeTimesPlus setEnabled:NO];
        [self.btnIPTVKPlusPrepaidMonthMinus setEnabled:NO];
        [self.btnIPTVKPlusPrepaidMonthPlus setEnabled:NO];
        
    }else {
        self.cbKPlus.selected  = YES;
        
        if(self.rc.IPTVKPlusChargeTimes != nil && ![self.rc.IPTVKPlusChargeTimes isEqualToString:@"0"]){
            self.txtIPTVKPlusChargeTimes.text = self.rc.IPTVKPlusChargeTimes;
            
        }else {
            self.txtIPTVKPlusChargeTimes.text = @"1";
        }
        
        if(self.rc.IPTVKPlusPrepaidMonth != nil){
            self.txtIPTVKPlusPrepaidMonth.text = self.rc.IPTVKPlusPrepaidMonth;
            
        }else {
            self.txtIPTVKPlusPrepaidMonth.text = @"0";
        }
        
        [self.btnIPTVKPlusChargeTimesMinus setEnabled:YES];
        [self.btnIPTVKPlusChargeTimesPlus setEnabled:YES];
        [self.btnIPTVKPlusPrepaidMonthMinus setEnabled:YES];
        [self.btnIPTVKPlusPrepaidMonthPlus setEnabled:YES];
    }
    flagtotaliptv = FALSE;
}

-(IBAction)cbVTC_Clicked:(UIButton *)sender {
    if(self.cbVTC.selected == YES){
        self.cbVTC.selected = NO;
        self.txtIPTVVTCChargeTimes.text = @"0";
        self.txtIPTVVTCPrepaidMonth.text = @"0";
        [self.btnIPTVVTCChargeTimesMinus setEnabled:NO];
        [self.btnIPTVVTCChargeTimesPlus setEnabled:NO];
        [self.btnIPTVVTCPrepaidMonthMinus setEnabled:NO];
        [self.btnIPTVVTCPrepaidMonthPlus setEnabled:NO];
    }else {
        self.cbVTC.selected  = YES;
        
        if(self.rc.IPTVVTCChargeTimes != nil && ![self.rc.IPTVVTCChargeTimes isEqualToString:@"0"]){
            self.txtIPTVVTCChargeTimes.text = self.rc.IPTVVTCChargeTimes;
            
        }else {
            self.txtIPTVVTCChargeTimes.text = @"1";
        }
        
        if(self.rc.IPTVVTCPrepaidMonth != nil){
            self.txtIPTVVTCPrepaidMonth.text = self.rc.IPTVVTCPrepaidMonth;
            
        }else {
            self.txtIPTVVTCPrepaidMonth.text = @"0";
        }
        
        [self.btnIPTVVTCChargeTimesMinus setEnabled:YES];
        [self.btnIPTVVTCChargeTimesPlus setEnabled:YES];
        [self.btnIPTVVTCPrepaidMonthMinus setEnabled:YES];
        [self.btnIPTVVTCPrepaidMonthPlus setEnabled:YES];
    }
    flagtotaliptv = FALSE;
    
}

-(IBAction)cbVTV_Clicked:(UIButton *)sender {
    if(self.cbVTV.selected == YES){
        self.cbVTV.selected = NO;
        self.txtIPTVVTVChargeTimes.text = @"0";
        self.txtIPTVVTVPrepaidMonth.text = @"0";
        [self.btnIPTVVTVChargeTimesMinus setEnabled:NO];
        [self.btnIPTVVTVChargeTimesPlus setEnabled:NO];
        [self.btnIPTVVTVPrepaidMonthMinus setEnabled:NO];
        [self.btnIPTVVTVPrepaidMonthPlus setEnabled:NO];
        
    }else {
        self.cbVTV.selected  = YES;
        
        if(self.rc.IPTVVTVChargeTimes != nil && ![self.rc.IPTVVTVChargeTimes isEqualToString:@"0"]){
            self.txtIPTVVTVChargeTimes.text = self.rc.IPTVVTVChargeTimes;
            
        }else {
            self.txtIPTVVTVChargeTimes.text = @"1";
        }
        
        if(self.rc.IPTVVTVPrepaidMonth != nil){
            self.txtIPTVVTVPrepaidMonth.text = self.rc.IPTVVTVPrepaidMonth;
            
        }else {
            self.txtIPTVVTVPrepaidMonth.text = @"0";
        }
        
        [self.btnIPTVVTVChargeTimesMinus setEnabled:YES];
        [self.btnIPTVVTVChargeTimesPlus setEnabled:YES];
        [self.btnIPTVVTVPrepaidMonthMinus setEnabled:YES];
        [self.btnIPTVVTVPrepaidMonthPlus setEnabled:YES];
    }
    flagtotaliptv = FALSE;
}

-(IBAction)RadioButtonDeployment:(RadioButton *)sender {
    if(sender == self.rdYesRequest){
        TypeDeployment = @"1";
    }else {
        TypeDeployment = @"0";
    }
}

-(IBAction)RadioButtonCableStatus:(RadioButton *)sender {
    if(sender == self.rdYesCableStatus){
        CableStatus = @"1";
    }else {
        CableStatus = @"2";
    }
}

-(IBAction)RadioButtonInsCable:(RadioButton *)sender {
    if(sender == self.rdYesInsCable){
        InsCable = @"1";
    }else {
        InsCable = @"2";
    }
}

-(IBAction)RadioButtonDrillWall:(RadioButton *)sender {
    if(sender == self.rdYesWallDrilling){
        TypeDrillWall = @"1";
    }else {
        TypeDrillWall = @"0";
    }
}

// Đăng ký Office 365
- (IBAction)registedOffice365ChangeValue:(UISwitch *)sender {
    if (sender.isOn) {
        self.office365Button.enabled = YES;
        
    } else {
        self.office365Button.enabled = NO;
        [self registedOffice365:NO];
        office365Total = 0;
        self.office365Amount.text = @"0";
        [self Total];
    }
    
}
// Đăng ký OTT

- (IBAction)registedOTTChangeValue:(UISwitch *)sender {
    
    if (sender.isOn) {
        self.btnMinBoxOTT.enabled = YES;
        self.btnMaxBoxOTT.enabled = YES;
    }
    else{
        self.btnMaxBoxOTT.enabled = NO;
        self.btnMinBoxOTT.enabled = NO;
    }
}



- (IBAction)office365ButtonPressed:(id)sender {
    [self.view endEditing:YES];
    
    _office365ViewController = [[Office365ViewController alloc] init];
    _office365ViewController.delegate   = self;
    _office365ViewController.registerID = self.Id;
    _office365ViewController.amountTotal = office365Total;
    _office365ViewController.arrayListPacketOld = self.arrayListPacketOld;
    [self presentViewController:_office365ViewController animated:YES completion:nil];
    
}

- (IBAction)btnBirthday_Clicked:(id)sender {
    self.buttonDate = sender;
    [self showDatePicker];
    [self limitDate:[NSDate date] minYear:-99 maxYear:0];
    
}

-(IBAction)btnUpdate_clicked:(id)sender {
    [self CheckRegistration];
    
}
/*
 - (void)UpdateRegistrationGo {
 if (![Common_client isNetworkAvailable]) {
 [Common_client showAlertMessage:Mesage_Network];
 [self hideMBProcess];
 return;
 }
 NSString *promotionid = @"0";
 NSString *iptvPackage = @"";
 NSString *fullname = @"";
 NSString *fullname1 = @"";
 
 if([self.txtPersonContact1.text isEqualToString:@""] && ![self.txtPersonContact.text isEqualToString:@""]){
 fullname = self.txtPersonContact.text;
 fullname1 = self.txtPersonContact.text;
 }
 if(![self.txtPersonContact1.text isEqualToString:@""] && [self.txtPersonContact.text isEqualToString:@""]){
 fullname = self.txtPersonContact1.text;
 fullname1 = self.txtPersonContact1.text;
 }
 if(![self.txtPersonContact1.text isEqualToString:@""] && ![self.txtPersonContact.text isEqualToString:@""]){
 fullname = self.txtPersonContact.text;
 fullname1 = self.txtPersonContact1.text;
 }
 
 NSString *LocationName = [ShareData instance].currentUser.LocationName;
 if([selectedTypeHouse.Key isEqualToString:@"1"]){
 Address = [NSString stringWithFormat:@"%@ ,%@, %@, %@, %@", self.txtBilltoNumber.text, selectedStreet.Values, selectedWard.Values, selectedDistrict.Values, LocationName];
 }else if([selectedTypeHouse.Key isEqualToString:@"2"]) {
 Address = [NSString stringWithFormat:@"L.%@ ,T.%@, P.%@, %@, %@, %@, %@, %@", self.txtLot.text, self.txtFloor.text, self.txtRoom.text, selectedNameVilla.Values, selectedStreet.Values, selectedWard.Values, selectedDistrict.Values, LocationName];
 }else {
 Address = [NSString stringWithFormat:@"%@ ,%@, %@, %@, %@", self.txtBilltoNumber.text, selectedStreet.Values, selectedWard.Values, selectedDistrict.Values, [ShareData instance].currentUser.LocationName];
 }
 //NSString *IPTVUseComboHR=@"2";
 NSString *IPTVUseComboHR=@"0";
 if([selectedLocalType.Key isEqualToString:@"0"]){
 promotionid = selectedPromotion.PromotionId;
 iptvPackage = @"";
 
 }else if([selectedLocalType.Key isEqualToString:@"1"]){
 iptvPackage = selectedPackageIPTV.Key;
 }else if([selectedLocalType.Key isEqualToString:@"2"]){
 promotionid = selectedPromotion.PromotionId;
 iptvPackage = selectedPackageIPTV.Key;
 //IPTVUseComboHR =@"3";
 IPTVUseComboHR = selectedCombo.Key;
 }
 
 NSString *birthday = [self convertDateToString: [self convertStringToDate:self.btnBirthday.titleLabel.text]];
 
 NSString *addressPassport = self.txtAddressIdentifyCard.text;
 if (addressPassport.length <= 0) {
 addressPassport = @"";
 }
 
 ShareData *shared = [ShareData instance];
 [shared.registrationFormProxy UpdateRegistration:self.rc.ID ObjID:self.rc.ObjID Contract:self.rc.Contract LocationID:shared.currentUser.LocationID LocalType:selectedServicePack.Key FullName:self.txtFullName.text Contact:fullname BillTo_City:shared.currentUser.LocationName BillTo_District:selectedDistrict.Key BillTo_Ward:selectedWard.Key TypeHouse:selectedTypeHouse.Key BillTo_Street:selectedStreet.Key Lot:self.txtLot.text Floor:self.txtFloor.text Room:self.txtRoom.text NameVilla:selectedNameVilla.Key Position:selectedPosition.Key BillTo_Number:self.txtBilltoNumber.text Address:Address Note:self.txtNote.text Passport:self.txtCMND.text TaxId:self.txtMST.text Phone_1:self.txtPhone1.text Phone_2:self.txtPhone2.text Type_1:selectedPhone1.Key Type_2:selectedPhone1.Key Contact_1:fullname1 Contact_2:self.txtPersonContact2.text ISPType:selectedISP.Key CusTypeDetail:selectedTypeCustomer.Key CurrentHouse:selectedPlaceCurrent.Key PaymentAbility:selectedSolvency.Key PartnerID:selectedClientPartners.Key LegalEntity:selectedLegal.Key Supporter:self.rc.Supporter PromotionID:promotionid Total:[NSString stringWithFormat:@"%d", amountTotal] UserName:shared.currentUser.userName Deposit:selectedDeposit.Key DepositBlackPoint:selectedDepositBlackPoint.Key ObjectType:selectedObjectCustomer.Key CableStatus:CableStatus InsCable:InsCable CusType:selectedCusType.Key IPTVUseCombo:IPTVUseComboHR IPTVRequestSetUp:TypeDeployment IPTVRequestDrillWall:TypeDrillWall IPTVStatus:selectedDeployment.Key Email:self.txtEmail.text IPTVPromotionID:selectedPromotionIPTV.PromotionId IPTVPackage:iptvPackage IPTVBoxCount:self.txtDeviceBoxCount.text IPTVPLCCount:self.txtDevicePLCCount.text IPTVReturnSTBCount:self.txtDeviceSTBCount.text IPTVChargeTimes:self.txtIPTVChargeTimes.text IPTVPrepaid:self.txtIPTVPrepaid.text IPTVHBOPrepaidMonth:@"0" IPTVHBOChargeTimes:@"0" IPTVVTVPrepaidMonth:self.txtIPTVVTVPrepaidMonth.text IPTVVTVChargeTimes:self.txtIPTVVTVChargeTimes.text IPTVKPlusPrepaidMonth:self.txtIPTVKPlusPrepaidMonth.text IPTVKPlusChargeTimes:self.txtIPTVKPlusChargeTimes.text IPTVVTCPrepaidMonth:self.txtIPTVVTCPrepaidMonth.text IPTVVTCChargeTimes:self.txtIPTVVTCChargeTimes.text IPTVDeviceTotal:DeviceTotal IPTVPrepaidTotal:PrepaidTotal IPTVTotal:[NSString stringWithFormat:@"%d", amountIPTV] InternetTotal:[NSString stringWithFormat:@"%d", amountInternet] DescriptionIBB:self.txtDescriptionIBB.text  potentialID:self.rc.PotentialID indoor:self.txtIndoor.text outdoor:self.txtOutdoor.text paymentType:selectedPaymentType.Key addressPassport:addressPassport birthday:birthday IPTVFimPlusPrepaidMonth:self.txtIPTVFimPlusPrepaidMonth.text IPTVFimPlusChargeTimes:self.txtIPTVFimPlusChargeTimes.text IPTVFimHotPrepaidMonth:self.txtIPTVFimHotPrepaidMonth.text IPTVFimHotChargeTimes:self.txtIPTVFimHotChargeTimes.text IPTVPromotionIDBoxOrder:selectedPromotionIPTVBoxOrder.PromotionId completeHandler:^(id result, NSString *errorCode, NSString *message) {
 
 if ([message isEqualToString:@"het phien lam viec"]) {
 [self ShowAlertErrorSession];
 [self LogOut];
 [self hideMBProcess];
 return;
 }
 if(![errorCode isEqualToString:@"0"]){
 [self hideMBProcess];
 [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
 ID = errorCode;
 flagAlert = TRUE;
 [self.navigationController popViewControllerAnimated:YES];
 return ;
 }
 
 [self showAlertBox:@"Thông Báo" message:message];
 flagAlert = FALSE;
 [self hideMBProcess];
 } errorHandler:^(NSError *error) {
 [self showAlertBox:@"Lỗi" message:[NSString stringWithFormat:@"%@ - /n%@",@"UpdateRegistration",[error localizedDescription]]];
 flagAlert = FALSE;
 [self hideMBProcess];
 }];
 
 }
 */
- (void)UpdateRegistrationGo {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters = [self setDataUpdateRegistration];
    
    ShareData *shareData = [ShareData instance];
    [shareData.registrationFormProxy updateRegistration:parameters
                                      completionHandler:^(id result, NSString *errorCode, NSString *message) {
                                         NSArray *arrData = result;
                                         if (arrData.count <= 0) {
                                             // Cập nhật PĐK thất bại.
                                             /// Show thông báo.
                                             [self showAlertBox:@"Thông báo!" message:message tag:ErrorTag];
                                             
                                         } else {
                                             NSString *result = StringFormat(@"%@",[arrData[0]  objectForKey:@"Result"]);
                                             NSString *resultID = StringFormat(@"%@",[arrData[0]  objectForKey:@"ResultID"]);
                                             
                                             [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",result]];
                                             
                                             // Cập nhật PĐK thành công
                                             if(![resultID isEqualToString:@"0"]){
                                                 ID = resultID;
                                                 flagAlert = TRUE;
                                                 [self.navigationController popViewControllerAnimated:YES];
                                             }
                                         }
                                         
                                         [self hideMBProcess];
                                         return;
                                         
                                     } errorHandler:^(NSError *error) {
                                         [self showAlertBox:@"Lỗi!" message:[error localizedDescription] tag:ErrorTag];
                                         [self hideMBProcess];
                                     }];
    
}

// Set parameter for Update registration API.
- (NSMutableDictionary *)setDataUpdateRegistration
{
    NSString *promotionid = @"0";
    NSString *iptvPackage = @"";
    NSString *fullname = @"";
    NSString *fullname1 = @"";
    
    if([self.txtPersonContact1.text isEqualToString:@""] && ![self.txtPersonContact.text isEqualToString:@""]){
        fullname = self.txtPersonContact.text;
        fullname1 = self.txtPersonContact.text;
    }
    if(![self.txtPersonContact1.text isEqualToString:@""] && [self.txtPersonContact.text isEqualToString:@""]){
        fullname = self.txtPersonContact1.text;
        fullname1 = self.txtPersonContact1.text;
    }
    if(![self.txtPersonContact1.text isEqualToString:@""] && ![self.txtPersonContact.text isEqualToString:@""]){
        fullname = self.txtPersonContact.text;
        fullname1 = self.txtPersonContact1.text;
    }
    
    NSString *LocationName = [ShareData instance].currentUser.LocationName;
    if([selectedTypeHouse.Key isEqualToString:@"1"]){
        Address = [NSString stringWithFormat:@"%@ ,%@, %@, %@, %@", self.txtBilltoNumber.text, selectedStreet.Values, selectedWard.Values, selectedDistrict.Values, LocationName];
    }else if([selectedTypeHouse.Key isEqualToString:@"2"]) {
        Address = [NSString stringWithFormat:@"L.%@ ,T.%@, P.%@, %@, %@, %@, %@, %@", self.txtLot.text, self.txtFloor.text, self.txtRoom.text, selectedNameVilla.Values, selectedStreet.Values, selectedWard.Values, selectedDistrict.Values, LocationName];
    }else {
        Address = [NSString stringWithFormat:@"%@ ,%@, %@, %@, %@", self.txtBilltoNumber.text, selectedStreet.Values, selectedWard.Values, selectedDistrict.Values, [ShareData instance].currentUser.LocationName];
    }
    //NSString *IPTVUseComboHR=@"2";
    NSString *IPTVUseComboHR=@"0";
    if([selectedLocalType.Key isEqualToString:@"0"]){
        promotionid = selectedPromotion.PromotionId;
        iptvPackage = @"";
        
    }else if([selectedLocalType.Key isEqualToString:@"1"]){
        iptvPackage = selectedPackageIPTV.Key;
    }else if([selectedLocalType.Key isEqualToString:@"2"]){
        promotionid = selectedPromotion.PromotionId;
        iptvPackage = selectedPackageIPTV.Key;
        //IPTVUseComboHR =@"3";
        IPTVUseComboHR = selectedCombo.Key;
    }
    
    NSString *birthday = [self convertDateToString: [self convertStringToDate:self.btnBirthday.titleLabel.text]];
    
    NSString *addressPassport = self.txtAddressIdentifyCard.text;
    if (addressPassport.length <= 0) {
        addressPassport = @"";
    }
    
    ShareData *shared = [ShareData instance];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    // Set parameter office 365 for Update registration API
    if (self.registedOffice365Switch.isOn && office365Total > 0) {
        NSDictionary *officeParameterDict = [[Office365Model sharedManager] setParamater];
        [dict setDictionary:officeParameterDict];

    } else {
        [dict setObject:@"" forKey:@"EmailAdmin"];
        [dict setObject:@"" forKey:@"DomainName"];
        [dict setObject:@"" forKey:@"TechName"];
        [dict setObject:@"" forKey:@"TechPhoneNumber"];
        [dict setObject:@"" forKey:@"TechEmail"];
        [dict setObject:@[] forKey:@"ListPackage"];
    }
    // Nếu là Office 365 only, truyền LocalType = 208, LocalTypeNam = @"Microsoft365".
    if ([selectedLocalType.Key isEqualToString:@"3"]) {
        [dict setObject:@"208" forKey:@"LocalType"];
        
    } else {
        [dict setObject:selectedServicePack.Key ?:@"0" forKey:@"LocalType"];

    }
    // Nếu là OTT , truyền LocalType = 210,LocalTypeName = @"FPT Play"
    //vutt11
    if ([selectedLocalType.Key isEqualToString:@"4"]) {
        [dict setObject:@"210" forKey:@"LocalType"];
    }
    else
    {
        [dict setObject:selectedServicePack.Key?:@"0" forKey:@"LocalType"];
    }
    
    [dict setObject:self.rc.ID       ?:@"0" forKey:@"ID"];
    [dict setObject:self.rc.ObjID    ?:@"0" forKey:@"ObjID"];
    [dict setObject:self.rc.Contract ?:@"" forKey:@"Contract"];
    [dict setObject:shared.currentUser.LocationID ?:@"0" forKey:@"LocationID"];
    [dict setObject:self.txtFullName.text         ?:@"" forKey:@"FullName"];
    [dict setObject:fullname         ?:@"" forKey:@"Contact"];
    [dict setObject:shared.currentUser.LocationName ?:@"0" forKey:@"BillTo_City"];
    [dict setObject:selectedDistrict.Key    ?:@"0" forKey:@"BillTo_District"];
    [dict setObject:selectedWard.Key        ?:@"0" forKey:@"BillTo_Ward"];
    [dict setObject:selectedTypeHouse.Key   ?:@"0" forKey:@"TypeHouse"];
    [dict setObject:selectedStreet.Key      ?:@"0" forKey:@"BillTo_Street"];
    [dict setObject:self.txtLot.text        ?:@"0" forKey:@"Lot"];
    [dict setObject:self.txtFloor.text      ?:@"0" forKey:@"Floor"];
    [dict setObject:self.txtRoom.text       ?:@"0" forKey:@"Room"];
    [dict setObject:selectedNameVilla.Key   ?:@"" forKey:@"NameVilla"];
    [dict setObject:selectedPosition.Key    ?:@"0" forKey:@"Position"];
    [dict setObject:self.txtBilltoNumber.text ?:@"0" forKey:@"BillTo_Number"];
    [dict setObject:Address ?:@"" forKey:@"Address"];
    [dict setObject:self.txtNote.text   ?:@"" forKey:@"Note"];
    [dict setObject:self.txtCMND.text   ?:@"" forKey:@"Passport"];
    [dict setObject:self.txtMST.text    ?:@"" forKey:@"TaxId"];
    [dict setObject:self.txtPhone1.text ?:@"" forKey:@"Phone_1"];
    [dict setObject:self.txtPhone2.text ?:@"" forKey:@"Phone_2"];
    [dict setObject:selectedPhone1.Key  ?:@"0" forKey:@"Type_1"];
    [dict setObject:selectedPhone2.Key  ?:@"0" forKey:@"Type_2"];
    [dict setObject:fullname1 ?:@"" forKey:@"Contact_1"];
    [dict setObject:self.txtPersonContact2.text ?:@"" forKey:@"Contact_2"];
    [dict setObject:selectedISP.Key ?:@"0" forKey:@"ISPType"];
    [dict setObject:selectedTypeCustomer.Key    ?:@"" forKey:@"CusTypeDetail"];
    [dict setObject:selectedPlaceCurrent.Key    ?:@"" forKey:@"CurrentHouse"];
    [dict setObject:selectedSolvency.Key        ?:@"0" forKey:@"PaymentAbility"];
    [dict setObject:selectedClientPartners.Key  ?:@"0" forKey:@"PartnerID"];
    [dict setObject:selectedLegal.Key ?:@"0" forKey:@"LegalEntity"];
    [dict setObject:self.rc.Supporter ?:@"" forKey:@"Supporter"];
    [dict setObject:promotionid       ?:@"0" forKey:@"PromotionID"];
    [dict setObject:[NSString stringWithFormat:@"%d", amountTotal] ?:@"0" forKey:@"Total"];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    [dict setObject:selectedDeposit.Key         ?:@"0" forKey:@"Deposit"];
    [dict setObject:selectedDepositBlackPoint.Key   ?:@"0" forKey:@"DepositBlackPoint"];
    [dict setObject:selectedObjectCustomer.Key      ?:@"0" forKey:@"ObjectType"];
    [dict setObject:CableStatus ?:@"0" forKey:@"CableStatus"];
    [dict setObject:InsCable    ?:@"0" forKey:@"InsCable"];
    [dict setObject:selectedCusType.Key ?:@"0" forKey:@"CusType"];
    [dict setObject:IPTVUseComboHR  ?:@"0" forKey:@"IPTVUseCombo"];
    [dict setObject:TypeDeployment  ?:@"0" forKey:@"IPTVRequestSetUp"];
    [dict setObject:TypeDrillWall   ?:@"0" forKey:@"IPTVRequestDrillWall"];
    [dict setObject:selectedDeployment.Key  ?:@"0" forKey:@"IPTVStatus"];
    [dict setObject:self.txtEmail.text      ?:@"" forKey:@"Email"];
    [dict setObject:selectedPromotionIPTV.PromotionId  ?:@"0" forKey:@"IPTVPromotionID"];
    [dict setObject:iptvPackage ?:@"0" forKey:@"IPTVPackage"];
    [dict setObject:self.txtDeviceBoxCount.text  ?:@"0" forKey:@"IPTVBoxCount"];
    [dict setObject:self.txtDevicePLCCount.text  ?:@"0" forKey:@"IPTVPLCCount"];
    [dict setObject:self.txtDeviceSTBCount.text  ?:@"0" forKey:@"IPTVReturnSTBCount"];
    [dict setObject:self.txtIPTVChargeTimes.text ?:@"0" forKey:@"IPTVChargeTimes"];
    [dict setObject:self.txtIPTVPrepaid.text     ?:@"0" forKey:@"IPTVPrepaid"];
    [dict setObject:@"0" forKey:@"IPTVHBOPrepaidMonth"];
    [dict setObject:@"0" forKey:@"IPTVHBOChargeTimes"];
    [dict setObject:self.txtIPTVVTVPrepaidMonth.text   ?:@"0" forKey:@"IPTVVTVPrepaidMonth"];
    [dict setObject:self.txtIPTVVTVChargeTimes.text    ?:@"0" forKey:@"IPTVVTVChargeTimes"];
    [dict setObject:self.txtIPTVKPlusPrepaidMonth.text ?:@"0" forKey:@"IPTVKPlusPrepaidMonth"];
    [dict setObject:self.txtIPTVKPlusChargeTimes.text  ?:@"0" forKey:@"IPTVKPlusChargeTimes"];
    [dict setObject:self.txtIPTVVTCPrepaidMonth.text   ?:@"0" forKey:@"IPTVVTCPrepaidMonth"];
    [dict setObject:self.txtIPTVVTCChargeTimes.text    ?:@"0" forKey:@"IPTVVTCChargeTimes"];
    [dict setObject:DeviceTotal ?:@"0" forKey:@"IPTVDeviceTotal"];
    [dict setObject:PrepaidTotal ?:@"0" forKey:@"IPTVPrepaidTotal"];
    [dict setObject:[NSString stringWithFormat:@"%d", amountIPTV] ?:@"0" forKey:@"IPTVTotal"];
    [dict setObject:[NSString stringWithFormat:@"%d", amountInternet] ?:@"0" forKey:@"InternetTotal"];
    [dict setObject:self.txtDescriptionIBB.text ?:@"" forKey:@"DescriptionIBB"];
    [dict setObject:self.rc.PotentialID  ?:@"" forKey:@"PotentialID"];
    [dict setObject:self.txtIndoor.text  ?:@"" forKey:@"InDoor"];
    [dict setObject:self.txtOutdoor.text ?:@"" forKey:@"OutDoor"];
    [dict setObject:selectedPaymentType.Key ?:@"" forKey:@"Payment"];
    [dict setObject:addressPassport  ?:@"" forKey:@"AddressPassport"];
    [dict setObject:birthday ?:@"" forKey:@"Birthday"];
    [dict setObject:self.txtIPTVFimPlusPrepaidMonth.text ?:@"0" forKey:@"IPTVFimPlusPrepaidMonth"];
    [dict setObject:self.txtIPTVFimPlusChargeTimes.text  ?:@"0" forKey:@"IPTVFimPlusChargeTimes"];
    [dict setObject:self.txtIPTVFimHotPrepaidMonth.text  ?:@"0" forKey:@"IPTVFimHotPrepaidMonth"];
    [dict setObject:self.txtIPTVFimHotChargeTimes.text   ?:@"0" forKey:@"IPTVFimHotChargeTimes"];
    [dict setObject:selectedPromotionIPTVBoxOrder.PromotionId ?:@"0" forKey:@"IPTVPromotionIDBoxOrder"];
    //vutt11
    [dict setObject:[NSString stringWithFormat:@"%ld",(long)self.count] ?:@"0" forKey:@"OTTBoxCount"];
    [dict setObject:[NSString stringWithFormat:@"%@",self.imageInfo] forKey:@"ImageInfo"];
    
    return dict;
    
}

//vutt11
- (void)CheckRegistration {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    if([self.txtFullName.text isEqualToString:@""]){
        [self showAlertBox:@"Thông Báo" message:@"Vui lòng điền tên khách hàng"];
        [self.txtFullName becomeFirstResponder];
        return;
    }
    
    if(![selectedTypeCustomer.Key isEqualToString:@"10"]){
        if([self.txtCMND.text isEqualToString:@""]){
            [self showAlertBox:@"Thông Báo" message:@"Vui lòng điền số CMND"];
            [self.txtCMND becomeFirstResponder];
            return;
        }
        
        int len = (int)[[self.txtCMND.text stringByReplacingOccurrencesOfString:@" " withString:@""] length];
        if(len < 9 || len > 15){
            [self showAlertBox:@"Thông Báo" message:@"Số CMND không hợp lệ"];
            [self.txtCMND becomeFirstResponder];
            return;
        }
        
        if (![[self.txtCMND.text stringByTrimmingCharactersInSet:[NSCharacterSet alphanumericCharacterSet]] isEqualToString:@""]) {
            [self showAlertBox:@"Thông Báo" message:@"Số CMND không hợp lệ"];
            [self.txtCMND becomeFirstResponder];
            return;
        }
        
        // check address identify card not empty
        if ([self.txtAddressIdentifyCard.text isEqualToString:@""]) {
            [self showAlertBox:@"Thông Báo" message:@"Vui lòng điền địa chỉ trên CMND"];
            [self.txtAddressIdentifyCard becomeFirstResponder];
            return;
        }
    }
    
    // check birthday choosed yes or no ?
    if ([self.btnBirthday.titleLabel.text isEqualToString:@"[Chọn ngày sinh]"] || [self.btnBirthday.titleLabel.text isEqualToString:@""] ) {
        [self showAlertBox:@"Thông báo" message:@"Vui lòng chọn ngày sinh"];
        return;
    }
    
    // Obligatory enter tax code when customer type is company
    if([selectedTypeCustomer.Key isEqualToString:@"10"]){
        if([self.txtMST.text isEqualToString:@""]){
            [self showAlertBox:@"Thông Báo" message:@"Vui lòng điền mã số thuế"];
            [self.txtMST becomeFirstResponder];
            return;
        }
    }
    
    if([self.txtPhone1.text isEqualToString:@""]){
        [self showAlertBox:@"Thông Báo" message:@"Vui lòng điền số điện thoại liên hệ 1"];
        [self.txtPhone1 becomeFirstResponder];
        
        return;
        
    }else {
        int len = (int)[self.txtPhone1.text length];
        if(len < 10 || len > 11){
            [self showAlertBox:@"Thông Báo" message:@"Số điện thoại liên hệ 1 không hợp lệ"];
            [self.txtPhone1 becomeFirstResponder];
            return;
        }else {
            NSString* num = [self.txtPhone1.text substringToIndex:1];
            if(![num isEqualToString:@"0"]){
                [self showAlertBox:@"Thông Báo" message:@"Số điện thoại liên hệ 1 không hợp lệ"];
                [self.txtPhone1 becomeFirstResponder];
                return;
            }
        }
    }
    
    if([self.txtPersonContact1.text isEqualToString:@""]){
        if( [self.txtPersonContact.text isEqualToString:@""]){
            [self showAlertBox:@"Thông Báo" message:@"Vui lòng điền tên liên hệ"];
            [self.txtPersonContact1 becomeFirstResponder];
            return;
        }
    }
    
    if(![self.txtEmail.text isEqualToString:@""]){
        if(![self validateEmail:self.txtEmail.text]){
            [self showAlertBox:@"Thông Báo" message:@"Email không hợp lệ"];
            [self.txtEmail becomeFirstResponder];
            return;
        }
    }
    
    if ([selectedDistrict.Key isEqualToString:@""] || selectedDistrict.Key == nil) {
        [self showAlertBox:@"Thông Báo" message:@"Vui lòng chọn quận/huyện "];
        return;
    }
    
    if ([selectedWard.Key isEqualToString:@""] || selectedWard.Key == nil) {
        [self showAlertBox:@"Thông Báo" message:@"Vui lòng chọn phường/xã "];
        return;
    }
    
    if([selectedStreet.Key isEqualToString:@""] || selectedStreet.Key == nil){
        [self showAlertBox:@"Thông Báo" message:@"Vui lòng chọn đường"];
        return;
    }
    
    if([selectedTypeHouse.Key isEqualToString:@"1"]){
        if([self.txtBilltoNumber.text isEqualToString:@""]){
            [self showAlertBox:@"Thông Báo" message:@"Vui lòng điền số nhà"];
            [self.txtBilltoNumber becomeFirstResponder];
            return;
        }
    }
    
    if([selectedTypeHouse.Key isEqualToString:@"2"]){
        if([selectedNameVilla.Key isEqualToString:@""] || selectedNameVilla.Key == nil){
            [self showAlertBox:@"Thông Báo" message:@"Vui lòng chọn chung cư"];
            return;
        }
    }
    
    if ([selectedTypeHouse.Key isEqualToString:@"3"]) {
        if([self.txtBilltoNumber.text isEqualToString:@""]){
            [self showAlertBox:@"Thông Báo" message:@"Vui lòng điền số nhà"];
            [self.txtBilltoNumber becomeFirstResponder];
            return;
        }
        if ([selectedPosition.Key isEqualToString:@"0"]) {
            [self showAlertBox:@"Thông Báo" message:@"Vui lòng chọn vị trí nhà"];
            return;
        }
    }
    
    if (selectedDistrict.Values.length <1) {
        [self showAlertBox:@"Thông Báo" message:@"Quận/Huyện không hợp lệ"];
        return;
    }
    
    if (selectedWard.Values.length <1) {
        [self showAlertBox:@"Thông Báo" message:@"Phường/Xã không hợp lệ"];
        return;
    }
    
    if (selectedStreet.Values.length < 3) {
        [self showAlertBox:@"Thông Báo" message:@"Chưa nhập tên đường"];
        return;
    }
    
    // check payment type choose yes ? or
    if ([selectedPaymentType.Key isEqualToString:@"0"] || [self.btnPaymentType.titleLabel.text isEqualToString:@"[Chọn hình thức thanh toán]"]) {
        [self showAlertBox:@"Thông báo" message:@"Vui lòng chọn hình thức thanh toán"];
        return;
    }
    //Check OTT
    if ([selectedLocalType.Key isEqualToString:@"4"]) {
        if (self.count == 0) {
            
            [self showAlertBox:@"Thông báo " message:@"Vui lòng chọn số lượng Box cho FPT Play Box"];
            return;
        }
    }
    
    
    // Check Registed Office 365
    if ([selectedLocalType.Key isEqualToString:@"3"]) {
        if ([registedOffice.Key isEqualToString:@"0"]) {
            [self showAlertBox:@"Thông báo " message:@"Vui lòng Đăng ký thông tin dịch vụ Office 365"];
            return;

        }
        
        [self showActionSheetUpdate];
        return;
    }
    
    /*
     * check seleted promotion internet if local type not IPTV
     */
    
    //vutt11 !!
    if ([selectedLocalType.Key isEqualToString:@"0"]) {
        if ([selectedPromotion.PromotionId isEqualToString:@"0"]) {
            [self showAlertBox:@"Thông báo " message:@"Vui lòng chọn Câu lệnh khuyến mãi dịch vụ internet"];
            return;
        }
    }
    //!+
    if ([selectedLocalType.Key isEqualToString:@"1"]) {
        if ([selectedPromotionIPTV.PromotionId isEqualToString:@"0"]) {
            [self showAlertBox:@"Thông báo " message:@"Vui lòng chọn Câu lệnh khuyến mãi IPTV"];
            return;
        }
    }
    
    // check selected IPTVCombo if local type is net & IPTV
    if ([selectedLocalType.Key isEqualToString:@"2"]) {
        if ([selectedCombo.Key isEqualToString:@"0"]) {
            [self showAlertBox:@"Thông báo " message:@"Vui lòng chọn Combo cho dịch vụ IPTV"];
            return;
        }
    }
    
    
    
    if(![[NSString stringWithFormat:@"%@",selectedLocalType.Key] isEqualToString:@"0"]){
        if(flagtotaliptv == FALSE){
            isGetIPTVTotal = YES;
            [self getTotalIPTV];
            return;
        }
    }
    
    [self showActionSheetUpdate];
    
}

- (void)checkAddressNumber {
    ShareData *shared = [ShareData instance];
    [shared.registrationFormProxy checkAddressNumber:shared.currentUser.userName locationParent:shared.currentUser.LocationParent locationID:shared.currentUser.LocationID houseNumber:self.txtBilltoNumber.text completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if (result == nil) {
            return;
        }
        NSString *strIgnoreAction = [result objectForKey:@"IgnoreAction"];
        NSString *strResultID     = [result objectForKey:@"ResultID"];
        NSString *strResult       = [result objectForKey:@"Result"];
        if ([strResultID intValue] > 0) {
            self.txtBilltoNumber.text = strResult;
            //   [self checkRegistrationAPI];
            [self checkRegistration2];
            
            return;
        }
        if ([strIgnoreAction intValue] <= 0) {
            [self showAlertBox:@"Lỗi" message:strResult];
            [self hideMBProcess];
            return;
        }
        if ([strIgnoreAction intValue] > 0) {
            [self showCheckReg:@"Số nhà không hợp lệ"];
            updateCheck.tag = 1;
        }
        
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@",[error localizedDescription]]];
        [self.btnUpdate setEnabled:YES];
    }];
}

- (void)checkRegistrationAPI {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:self.txtCMND.text           forKey:@"Passport"];
    [dict setObject:self.txtMST.text            forKey:@"TaxID"];
    [dict setObject:selectedStreet.Key ?:@""    forKey:@"Street"];
    [dict setObject:selectedWard.Key   ?:@""    forKey:@"Ward"];
    [dict setObject:selectedDistrict.Key ?:@""  forKey:@"District"];
    
    if ([selectedTypeHouse.Key isEqualToString:@"1"]) {
        [dict setObject:self.txtBilltoNumber.text forKey:@"Number"];
    }
    
    if ([selectedTypeHouse.Key isEqualToString:@"2"]) {
        [dict setObject:self.txtLot.text    forKey:@"Lot"];
        [dict setObject:self.txtFloor.text  forKey:@"Floor"];
        [dict setObject:self.txtRoom.text   forKey:@"Room"];
        [dict setObject:selectedNameVilla.Key ?:@"" forKey:@"NameVilla"];
        
    }
    
    if ([selectedTypeHouse.Key isEqualToString:@"3"]) {
        [dict setObject:self.txtBilltoNumber.text   forKey:@"Number"];
        [dict setObject:selectedPosition.Key ?:@""  forKey:@"HousePosition"];
    }
    
    [self.btnUpdate setEnabled:NO];
    
    ShareData *share = [ShareData instance];
    
    [share.appProxy CheckRegistration:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(message.length >0){
            [self showCheckReg:message];
            updateCheck.tag = 2;
            [self.btnUpdate setEnabled:YES];
            
        } else {
            [self UpdateRegistrationGo];
            [self.btnUpdate setEnabled:YES];
        }
        
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@",[error localizedDescription]]];
        [self.btnUpdate setEnabled:YES];
    }];
}
//vutt11
- (void)checkRegistration2 {
    
    if (self.regcode.length <= 0) {
        self.regcode = @"";
    }
    
    if (![selectedTypeHouse.Key isEqualToString:@"2"]) {
        selectedNameVilla   = nil;
        self.txtLot.text    = @"";
        self.txtFloor.text  = @"";
        self.txtRoom.text   = @"";
    }
    
    if (![selectedTypeHouse.Key isEqualToString:@"3"]) {
        selectedPosition = nil;
    }
    
    ShareData *share = [ShareData instance];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:self.txtCMND.text           forKey:@"Passport"];
    [dict setObject:self.txtMST.text            forKey:@"TaxID"];
    [dict setObject:self.regcode                forKey:@"RegCode"];
    [dict setObject:share.currentUser.userName  forKey:@"UserName"];
    [dict setObject:self.txtPhone1.text         forKey:@"PhoneNumber"];
    
    [dict setObject:selectedStreet.Key ?:@""    forKey:@"Street"];
    [dict setObject:selectedWard.Key   ?:@""    forKey:@"Ward"];
    [dict setObject:selectedDistrict.Key ?:@""  forKey:@"District"];
    [dict setObject:selectedTypeHouse.Key ?:@"" forKey:@"TypeHouse"];
    [dict setObject:@""                         forKey:@"HousePosition"];
    [dict setObject:@""                         forKey:@"Number"];
    [dict setObject:@""                         forKey:@"NameVilla"];
    [dict setObject:@""                         forKey:@"Lot"];
    [dict setObject:@""                         forKey:@"Floor"];
    [dict setObject:@""                         forKey:@"Room"];
    
    [dict setObject:selectedDeployment.Key ?:@""            forKey:@"PayTVStatus"];
    [dict setObject:selectedServicePack.Key ?:@""           forKey:@"LocalType"];
    [dict setObject:selectedPromotion.PromotionId ?:@""     forKey:@"PromotionID"];
    [dict setObject:selectedPackageIPTV.Key ?:@""           forKey:@"IPTVPackpage"];
    [dict setObject:selectedPromotionIPTV.PromotionId ?:@"" forKey:@"IPTVPromotionID"];
    [dict setObject:[NSString stringWithFormat:@"%ld",(long)self.count]?:@"" forKey:@"OTTBoxCount"];
    
    [dict setObject:[NSString stringWithFormat:@"%@",self.imageInfo]?:@"" forKey:@"ImageInfo"];
    
    
    if ([selectedTypeHouse.Key isEqualToString:@"1"]) {
        [dict setObject:self.txtBilltoNumber.text forKey:@"Number"];
    }
    
    if ([selectedTypeHouse.Key isEqualToString:@"2"]) {
        [dict setObject:self.txtLot.text    forKey:@"Lot"];
        [dict setObject:self.txtFloor.text  forKey:@"Floor"];
        [dict setObject:self.txtRoom.text   forKey:@"Room"];
        [dict setObject:selectedNameVilla.Key ?:@"" forKey:@"NameVilla"];
        
        self.txtBilltoNumber.text = @"";
    }
    
    if ([selectedTypeHouse.Key isEqualToString:@"3"]) {
        [dict setObject:self.txtBilltoNumber.text   forKey:@"Number"];
        [dict setObject:selectedPosition.Key ?:@""  forKey:@"HousePosition"];
    }
    
    [self.btnUpdate setEnabled:NO];
    
    
    
    [share.appProxy CheckRegistration2:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        NSArray *arrData = result;
        NSInteger ResultID = [[[arrData objectAtIndex:0] objectForKey:@"ResultID"] integerValue];
        
        if (ResultID != 0) {
            
            NSString *msg = [[arrData objectAtIndex:0] objectForKey:@"Messages"];
            NSString *dupID = [[arrData objectAtIndex:0] objectForKey:@"DupID"];
            // save notice on server
            [self updateHasShowDuplicateWithDupID:dupID];
            // show alert update continue yes or no when duplicate info
            [self showCheckReg:msg];
            
            updateCheck.tag = 2;
            [self.btnUpdate setEnabled:YES];
            
            return;
        }
        
        [self UpdateRegistrationGo];
        [self.btnUpdate setEnabled:YES];
        
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@",[error localizedDescription]]];
        [self.btnUpdate setEnabled:YES];
    }];
}

// this function not show info for user
- (void)updateHasShowDuplicateWithDupID:(NSString *)Id {
    
    ShareData *share = [ShareData instance];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    [dict setObject:share.currentUser.userName forKey:@"UserName"];
    [dict setObject:Id forKey:@"ID"];
    
    [share.appProxy updateHasShowDuplicate:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        NSString *resultMsg = [[result objectAtIndex:0] objectForKey:@"Result"];
        NSLog(@"-------Update has show duplicate status: %@", resultMsg);
        return ;
        
    } errorHandler:^(NSError *error) {
        NSLog(@"-------Update has show duplicate status: %@", [error description]);
        
    }];
    
}

- (void)showCheckReg:(NSString*)message {
    NSString *messageAlert = StringFormat(@"%@ \nBạn có muốn tiếp tục cập nhật không? ",message);
    updateCheck = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:messageAlert delegate:self cancelButtonTitle:@"Không" otherButtonTitles:@"Có", nil];
    [updateCheck show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView == updateCheck) {
        switch (buttonIndex) {
            case 0:
                [self hideMBProcess];
                break;
            case 1: {
                if (updateCheck.tag == 1) {
                    //    [self checkRegistrationAPI];
                    [self checkRegistration2];
                }
                if (updateCheck.tag == 2) {
                    [self UpdateRegistrationGo];
                }
            }
                break;
            default:
                break;
        }
    }
    //    if (alertView == updateSuccess) {
    //        [self.navigationController popViewControllerAnimated:YES];
    //    }
}

#pragma mark - LoadData

-(void)LoadData {
    [self showMBProcess];
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    //vutt11
    
    [self.btnBirthday setTitle:self.rc.Birthday forState:UIControlStateNormal];
    self.txtAddressIdentifyCard.text = self.rc.AddressPassport;
    
    self.txtIndoor.text  = self.rc.InDoor;
    self.txtOutdoor.text = self.rc.OutDoor;
    
    [self LocaLocalType:self.rc.PromotionID Package:self.rc.IPTVPackage];
    Contract = self.rc.Contract;
    self.txtFullName.text = self.rc.FullName;
    self.txtCMND.text = self.rc.Passport;
    self.txtMST.text = self.rc.TaxId;
    [self LoadPhone1:self.rc.Type_1];
    self.txtPhone1.text = self.rc.Phone_1;
    self.txtPersonContact1.text = self.rc.Contact_1;
    [self LoadPhone2:self.rc.Type_2];
    self.txtPhone2.text = self.rc.Phone_2;
    self.txtPersonContact2.text = self.rc.Contact_2;
    self.txtPersonContact.text = self.rc.Contact;
    [self LoadObjectCustomer:self.rc.ObjectType];
    self.txtDescriptionIBB.text = self.rc.DescriptionIBB;
    self.txtEmail.text = self.rc.Email;
    //Kiểm tra loại khách hàng
    
    if([self.rc.ObjectType isEqualToString:@"2"]){
        self.btnISP.enabled = YES;
    }
    
    [self LoadISP:self.rc.ISPType];
    [self LoadTypeCustomer:self.rc.CusTypeDetail];
    [self LoadCurrenPlace:self.rc.CurrentHouse];
    [self LoadSolveny:self.rc.PaymentAbility];
    [self LoadClientPartner:self.rc.PartnerID];
    [self LoadLegal:self.rc.LegalEntity];
    [self LoadDistrict:self.rc.BillTo_District WardId:self.rc.BillTo_Ward Street:self.rc.BillTo_Street];
    [self LoadTypeHouse:self.rc.TypeHouse];
    [self LoadPosition:self.rc.Position];
    self.txtBilltoNumber.text = self.rc.BillTo_Number;
    self.txtLot.text = self.rc.Lot;
    self.txtFloor.text = self.rc.Floor;
    self.txtRoom.text = self.rc.Room;
    self.txtNote.text = self.rc.Note;
    self.txtIPTVChargeTimes.text = self.rc.IPTVChargeTimes;
    
    //Tổng tiền Internet, IPTV, Tổng cộng
    amountIPTV = [self.rc.IPTVTotal intValue];
    amountInternet =[self.rc.InternetTotal intValue];
    amountTotal = [self.rc.Total intValue];
    DeviceTotal = self.rc.IPTVDeviceTotal;
    PrepaidTotal = self.rc.IPTVPrepaidTotal;
    amountOTT = self.rc.OTTTotal;
    self.office365Amount.text = [nf stringFromNumber:[NSNumber numberWithFloat:office365Total]];
    self.txtIPTVTotal.text =[nf stringFromNumber:[NSNumber numberWithFloat:amountIPTV]];
    self.txtTotal.text =[nf stringFromNumber:[NSNumber numberWithFloat:amountTotal]];
    //vutt11
    //OTT
    self.txtOTTBoxCount.text = self.rc.OTTBoxCount;
    self.txtOTTTotal.text = [nf stringFromNumber:[NSNumber numberWithFloat:amountOTT]];
    
    self.txtIPTVFimPlusChargeTimes.text = self.rc.IPTVFimPlusChargeTimes;
    self.txtIPTVFimPlusPrepaidMonth.text =self.rc.IPTVFimPlusPrepaidMonth;
    self.txtIPTVFimHotChargeTimes.text = self.rc.IPTVFimHotChargeTimes;
    self.txtIPTVFimHotPrepaidMonth.text =self.rc.IPTVFimHotPrepaidMonth;
    self.txtIPTVKPlusChargeTimes.text = self.rc.IPTVKPlusChargeTimes;
    self.txtIPTVKPlusPrepaidMonth.text =self.rc.IPTVKPlusPrepaidMonth;
    self.txtIPTVPrepaid.text = self.rc.IPTVPrepaid;
    self.txtIPTVVTCChargeTimes.text = self.rc.IPTVVTCChargeTimes;
    self.txtIPTVVTCPrepaidMonth.text = self.rc.IPTVVTCPrepaidMonth;
    self.txtIPTVVTVChargeTimes.text = self.rc.IPTVVTVChargeTimes;
    self.txtIPTVVTVPrepaidMonth.text = self.rc.IPTVVTVPrepaidMonth;
    self.txtDeviceBoxCount.text = self.rc.IPTVBoxCount;
    self.txtDevicePLCCount.text = self.rc.IPTVPLCCount;
    self.txtDeviceSTBCount.text = self.rc.IPTVReturnSTBCount;
    
    [self LoadLocalTypePost:selectedLocalType.Key serverPackageID:self.rc.LocalType PromotionId:self.rc.PromotionID];
    
    if(![self.rc.IPTVPackage isEqualToString:@""]){
        
        [self LoadPackageIPTV:self.rc.IPTVPackage PromotionIPTV:self.rc.IPTVPromotionID boxCount:@"1"];
        
        if (![self.rc.IPTVPromotionIDBoxOrder isEqualToString:@""] && ![self.rc.IPTVPromotionIDBoxOrder isEqualToString:@"0"]) {
            
            [self LoadPromotionIPTV:self.rc.IPTVPromotionIDBoxOrder Package:self.rc.IPTVPackage boxCount:self.rc.IPTVBoxCount];
            
        }
        
    }
    [self LoadISP:self.rc.ISPType];
    [self LoadDeployment:self.rc.IPTVStatus];
    [self LoadCusType:self.rc.CusType];
    [self LoadCombo:self.rc.IPTVUseCombo];
    
    if(![self.rc.IPTVFimPlusChargeTimes isEqualToString:@"0"]){
        self.cbFimPlus.selected = YES;
        [self.btnIPTVFimPlusChargeTimesMinus setEnabled:YES];
        [self.btnIPTVFimPlusChargeTimesPlus setEnabled:YES];
        [self.btnIPTVFimPlusPrepaidMonthMinus setEnabled:YES];
        [self.btnIPTVFimPlusPrepaidMonthPlus setEnabled:YES];
    }
    if(![self.rc.IPTVFimHotChargeTimes isEqualToString:@"0"]){
        self.cbFimHot.selected = YES;
        [self.btnIPTVFimHotChargeTimesMinus setEnabled:YES];
        [self.btnIPTVFimHotChargeTimesPlus setEnabled:YES];
        [self.btnIPTVFimHotPrepaidMonthMinus setEnabled:YES];
        [self.btnIPTVFimHotPrepaidMonthPlus setEnabled:YES];
    }
    if(![self.rc.IPTVKPlusChargeTimes isEqualToString:@"0"]){
        self.cbKPlus.selected = YES;
        [self.btnIPTVKPlusChargeTimesMinus setEnabled:YES];
        [self.btnIPTVKPlusChargeTimesPlus setEnabled:YES];
        [self.btnIPTVKPlusPrepaidMonthMinus setEnabled:YES];
        [self.btnIPTVKPlusPrepaidMonthPlus setEnabled:YES];
    }
    if(![self.rc.IPTVVTVChargeTimes isEqualToString:@"0"]){
        self.cbVTV.selected = YES;
        [self.btnIPTVVTVChargeTimesMinus setEnabled:YES];
        [self.btnIPTVVTVChargeTimesPlus setEnabled:YES];
        [self.btnIPTVVTVPrepaidMonthMinus setEnabled:YES];
        [self.btnIPTVVTVPrepaidMonthPlus setEnabled:YES];
        
    }
    if(![self.rc.IPTVVTCChargeTimes isEqualToString:@"0"]){
        self.cbVTC.selected = YES;
        [self.btnIPTVVTCChargeTimesMinus setEnabled:YES];
        [self.btnIPTVVTCChargeTimesPlus setEnabled:YES];
        [self.btnIPTVVTCPrepaidMonthMinus setEnabled:YES];
        [self.btnIPTVVTCPrepaidMonthPlus setEnabled:YES];
    }
    
    [self LoadDeposit:self.rc.Deposit];
    [self LoadDepositBlackPoint:self.rc.DepositBlackPoint];
    
    if([self.rc.IPTVRequestSetUp isEqualToString:@"0"]){
        self.rdNoRequest.selected = YES;
        TypeDeployment = @"0";
    }
    if([self.rc.IPTVRequestDrillWall isEqualToString:@"0"]){
        self.rdNoWallDrilling.selected = YES;
        TypeDrillWall = @"0";
    }
    if([self.rc.CableStatus isEqualToString:@"2"]){
        self.rdNoCableStatus.selected = YES;
        CableStatus = @"2";
    }
    if([self.rc.INSCable isEqualToString:@"2"]){
        self.rdNoInsCable.selected = YES;
        InsCable = @"2";
    }
    [self LoadDeposit:self.rc.Deposit];
    
}

-(IBAction)btnMinus_cliked:(id)sender {
    flagtotaliptv = FALSE;
    if(sender == self.btnDeviceBoxCountMinus){
        if(![self.txtDeviceBoxCount.text isEqualToString:@"1"]){
            int boxDeviceCount = [self.txtDeviceBoxCount.text intValue];
            self.txtDeviceBoxCount.text =[NSString stringWithFormat:@"%d",boxDeviceCount - 1] ;
            
            [self checkExtraIPTVPackagesChargeTimesWithBoxDeviceCount:boxDeviceCount - 1];
            
            // Enable Khuyến mãi Box thứ 2 trở đi nếu số lượng Box > 1
            int deviceBox = [self.txtDeviceBoxCount.text intValue];
            if (deviceBox > 1) {
                return;
            }
            
            [self enablePromotionIPTVIDBoxOrder:NO];
            
            return;
        }
    }
    if(sender == self.btnDevicePLCCountMinus){
        if(![self.txtDevicePLCCount.text isEqualToString:@"0"]){
            self.txtDevicePLCCount.text =[NSString stringWithFormat:@"%d",[self.txtDevicePLCCount.text intValue] - 1] ;
            return;
        }
    }
    if(sender == self.btnDeviceSTBCountMinus){
        if(![self.txtDeviceSTBCount.text isEqualToString:@"0"]){
            self.txtDeviceSTBCount.text =[NSString stringWithFormat:@"%d",[self.txtDeviceSTBCount.text intValue] - 1] ;
            return;
        }
    }
    if(sender == self.btnIPTVChargeTimesMinus){
        if(![self.txtIPTVChargeTimes.text isEqualToString:@"1"]){
            self.txtIPTVChargeTimes.text =[NSString stringWithFormat:@"%d",[self.txtIPTVChargeTimes.text intValue] - 1] ;
            return;
        }
    }
    if(sender == self.btnIPTVFimPlusChargeTimesMinus){
        if(![self.txtIPTVFimPlusChargeTimes.text isEqualToString:@"1"]){
            self.txtIPTVFimPlusChargeTimes.text =[NSString stringWithFormat:@"%d",[self.txtIPTVFimPlusChargeTimes.text intValue] - 1] ;
            return;
        }
    }
    if(sender == self.btnIPTVFimPlusPrepaidMonthMinus){
        if(![self.txtIPTVFimPlusPrepaidMonth.text isEqualToString:@"0"]){
            self.txtIPTVFimPlusPrepaidMonth.text =[NSString stringWithFormat:@"%d",[self.txtIPTVFimPlusPrepaidMonth.text intValue] - 1] ;
            return;
        }
    }
    if(sender == self.btnIPTVFimHotChargeTimesMinus){
        if(![self.txtIPTVFimHotChargeTimes.text isEqualToString:@"1"]){
            self.txtIPTVFimHotChargeTimes.text =[NSString stringWithFormat:@"%d",[self.txtIPTVFimHotChargeTimes.text intValue] - 1] ;
            return;
        }
    }
    if(sender == self.btnIPTVFimHotPrepaidMonthMinus){
        if(![self.txtIPTVFimHotPrepaidMonth.text isEqualToString:@"0"]){
            self.txtIPTVFimHotPrepaidMonth.text =[NSString stringWithFormat:@"%d",[self.txtIPTVFimHotPrepaidMonth.text intValue] - 1] ;
            return;
        }
    }
    if(sender == self.btnIPTVKPlusChargeTimesMinus){
        if(![self.txtIPTVKPlusChargeTimes.text isEqualToString:@"1"]){
            self.txtIPTVKPlusChargeTimes.text =[NSString stringWithFormat:@"%d",[self.txtIPTVKPlusChargeTimes.text intValue] - 1] ;
            return;
        }
    }
    if(sender == self.btnIPTVKPlusPrepaidMonthMinus){
        if(![self.txtIPTVKPlusPrepaidMonth.text isEqualToString:@"0"]){
            self.txtIPTVKPlusPrepaidMonth.text =[NSString stringWithFormat:@"%d",[self.txtIPTVKPlusPrepaidMonth.text intValue] - 1] ;
            return;
        }
    }
    if(sender == self.btnIPTVVTCChargeTimesMinus){
        if(![self.txtIPTVVTCChargeTimes.text isEqualToString:@"1"]){
            self.txtIPTVVTCChargeTimes.text =[NSString stringWithFormat:@"%d",[self.txtIPTVVTCChargeTimes.text intValue] - 1] ;
            return;
        }
    }
    if(sender == self.btnIPTVVTCPrepaidMonthMinus){
        if(![self.txtIPTVVTCPrepaidMonth.text isEqualToString:@"0"]){
            self.txtIPTVVTCPrepaidMonth.text =[NSString stringWithFormat:@"%d",[self.txtIPTVVTCPrepaidMonth.text intValue] - 1] ;
            return;
        }
    }
    if(sender == self.btnIPTVVTVChargeTimesMinus){
        if(![self.txtIPTVVTVChargeTimes.text isEqualToString:@"1"]){
            self.txtIPTVVTVChargeTimes.text =[NSString stringWithFormat:@"%d",[self.txtIPTVVTVChargeTimes.text intValue] - 1] ;
            return;
        }
    }
    if(sender == self.btnIPTVVTVPrepaidMonthMinus){
        if(![self.txtIPTVVTVPrepaidMonth.text isEqualToString:@"0"]){
            self.txtIPTVVTVPrepaidMonth.text =[NSString stringWithFormat:@"%d",[self.txtIPTVVTVPrepaidMonth.text intValue] - 1] ;
            return;
        }
    }
    
}

// Cho phép chọn Khuyễn mãi Box thứ 2 trở lên
- (void)enablePromotionIPTVIDBoxOrder:(BOOL)status {
    [self.btnIPTVPromotionIDBoxOrder setEnabled:status];
    if (status == NO) {
        selectedPromotionIPTVBoxOrder = nil;
        self.tvPromotionIPTVBoxOrder.text = @"";
        self.lblIPTVPromotionIDBoxOrder.text = @"";
        [self.btnIPTVPromotionIDBoxOrder setTitle:@"" forState:UIControlStateNormal];
        return;
    }
    if (self.arrPromotionIPTVBoxOrder.count > 0) {
        if (selectedPromotionIPTVBoxOrder == nil) {
            selectedPromotionIPTVBoxOrder = [self.arrPromotionIPTVBoxOrder objectAtIndex:0];
        }
        self.tvPromotionIPTVBoxOrder.text = selectedPromotionIPTVBoxOrder.PromotionName;
        [self.btnIPTVPromotionIDBoxOrder setTitle:selectedPromotionIPTVBoxOrder.PromotionName forState:UIControlStateNormal];
        
        NSNumberFormatter *nf = [NSNumberFormatter new];
        nf.numberStyle = NSNumberFormatterDecimalStyle;
        self.lblIPTVPromotionIDBoxOrder.text = StringFormat(@"(%@ VNĐ)",[nf stringFromNumber:[NSNumber numberWithFloat:[selectedPromotionIPTVBoxOrder.Amount intValue]]]);
        
        return;
        
    } else {
        self.btnIPTVPackage.selected = YES;
        [self LoadPromotionIPTV:@"0" Package:selectedPackageIPTV.Key boxCount:self.txtDeviceBoxCount.text];
        
    }
}

-(IBAction)btnPlus_cliked:(id)sender {
    flagtotaliptv = FALSE;
    if(sender == self.btnDeviceBoxCountPlus){
        self.txtDeviceBoxCount.text =[NSString stringWithFormat:@"%d",[self.txtDeviceBoxCount.text intValue] + 1] ;
        
        if ([self.txtDeviceBoxCount.text intValue] > 2) {
            return;
        }
        [self enablePromotionIPTVIDBoxOrder:YES];
        
        return;
    }
    if(sender == self.btnDevicePLCCountPlus){
        self.txtDevicePLCCount.text =[NSString stringWithFormat:@"%d",[self.txtDevicePLCCount.text intValue] + 1] ;
        return;
    }
    if(sender == self.btnDeviceSTBCountPlus){
        self.txtDeviceSTBCount.text =[NSString stringWithFormat:@"%d",[self.txtDeviceSTBCount.text intValue] + 1] ;
        return;
    }
    
    int deviceBox = [self.txtDeviceBoxCount.text intValue];
    
    if(sender == self.btnIPTVChargeTimesPlus){
        // kiểm tra số lần tính cước phải <= số lượng Box
        if ([self.txtIPTVChargeTimes.text intValue] < deviceBox) {
            self.txtIPTVChargeTimes.text =[NSString stringWithFormat:@"%d",[self.txtIPTVChargeTimes.text intValue] + 1] ;
            return;
        }
        return;
    }
    
    if(sender == self.btnIPTVFimPlusChargeTimesPlus){
        // kiểm tra số lần tính cước phải <= số lượng Box
        if ([self.txtIPTVFimPlusChargeTimes.text intValue] < deviceBox) {
            self.txtIPTVFimPlusChargeTimes.text =[NSString stringWithFormat:@"%d",[self.txtIPTVFimPlusChargeTimes.text intValue] + 1] ;
            return;
        }
        return;
    }
    if(sender == self.btnIPTVFimPlusPrepaidMonthPlus){
        self.txtIPTVFimPlusPrepaidMonth.text =[NSString stringWithFormat:@"%d",[self.txtIPTVFimPlusPrepaidMonth.text intValue] + 1] ;
        return;
    }
    if(sender == self.btnIPTVFimHotChargeTimesPlus){
        // kiểm tra số lần tính cước phải <= số lượng Box
        if ([self.txtIPTVFimHotChargeTimes.text intValue] < deviceBox) {
            self.txtIPTVFimHotChargeTimes.text =[NSString stringWithFormat:@"%d",[self.txtIPTVFimHotChargeTimes.text intValue] + 1] ;
            return;
        }
        return;
    }
    if(sender == self.btnIPTVFimHotPrepaidMonthPlus){
        self.txtIPTVFimHotPrepaidMonth.text =[NSString stringWithFormat:@"%d",[self.txtIPTVFimHotPrepaidMonth.text intValue] + 1] ;
        return;
    }
    if(sender == self.btnIPTVKPlusChargeTimesPlus){
        // kiểm tra số lần tính cước phải <= số lượng Box
        if ([self.txtIPTVKPlusChargeTimes.text intValue] < deviceBox) {
            self.txtIPTVKPlusChargeTimes.text =[NSString stringWithFormat:@"%d",[self.txtIPTVKPlusChargeTimes.text intValue] + 1] ;
            return;
        }
        return;
    }
    if(sender == self.btnIPTVKPlusPrepaidMonthPlus){
        self.txtIPTVKPlusPrepaidMonth.text =[NSString stringWithFormat:@"%d",[self.txtIPTVKPlusPrepaidMonth.text intValue] + 1] ;
        return;
    }
    if(sender == self.btnIPTVVTCChargeTimesPlus){
        // kiểm tra số lần tính cước phải <= số lượng Box
        if ([self.txtIPTVVTCChargeTimes.text intValue] < deviceBox) {
            self.txtIPTVVTCChargeTimes.text =[NSString stringWithFormat:@"%d",[self.txtIPTVVTCChargeTimes.text intValue] + 1] ;
            return;
        }
        return;
    }
    if(sender == self.btnIPTVVTCPrepaidMonthPlus){
        self.txtIPTVVTCPrepaidMonth.text =[NSString stringWithFormat:@"%d",[self.txtIPTVVTCPrepaidMonth.text intValue] + 1] ;
        return;
    }
    if(sender == self.btnIPTVVTVChargeTimesPlus){
        // kiểm tra số lần tính cước phải <= số lượng Box
        if ([self.txtIPTVVTVChargeTimes.text intValue] < deviceBox) {
            self.txtIPTVVTVChargeTimes.text =[NSString stringWithFormat:@"%d",[self.txtIPTVVTVChargeTimes.text intValue] + 1] ;
            return;
        }
        return;
    }
    if(sender == self.btnIPTVVTVPrepaidMonthPlus){
        self.txtIPTVVTVPrepaidMonth.text =[NSString stringWithFormat:@"%d",[self.txtIPTVVTVPrepaidMonth.text intValue] + 1] ;
        return;
    }
}

// kiểm tra lại số lần tính cước khi thay đổi số lượng box
- (void)checkExtraIPTVPackagesChargeTimesWithBoxDeviceCount:(int)boxDeviceCount {
    
    [self setIPTVChargeTimesValueWithPackage:self.txtIPTVChargeTimes andBoxDeviceCount:boxDeviceCount];
    
    // Fim Plus
    [self setIPTVChargeTimesValueWithPackage:self.txtIPTVFimPlusChargeTimes andBoxDeviceCount:boxDeviceCount];
    
    // Fim Hot
    [self setIPTVChargeTimesValueWithPackage:self.txtIPTVFimHotChargeTimes andBoxDeviceCount:boxDeviceCount];
    
    // K Plus
    [self setIPTVChargeTimesValueWithPackage:self.txtIPTVKPlusChargeTimes andBoxDeviceCount:boxDeviceCount];
    
    // VTC cab
    [self setIPTVChargeTimesValueWithPackage:self.txtIPTVVTCChargeTimes andBoxDeviceCount:boxDeviceCount];
    
    // VTV cab
    [self setIPTVChargeTimesValueWithPackage:self.txtIPTVVTVChargeTimes andBoxDeviceCount:boxDeviceCount];
    
}

- (void)setIPTVChargeTimesValueWithPackage:(UITextField *)iptvPackage andBoxDeviceCount:(int)boxDeviceCount {
    int decrease = 0;
    int IPTVChargeTimes = 0;
    
    IPTVChargeTimes = [iptvPackage.text intValue];
    if (IPTVChargeTimes > boxDeviceCount) {
        decrease = IPTVChargeTimes - boxDeviceCount;
        iptvPackage.text =[NSString stringWithFormat:@"%d",IPTVChargeTimes - decrease] ;
    }
    
}
//vutt11
-(IBAction)btnGetTotalIPTV_Clicked:(UIButton *)sender {
    isGetIPTVTotal = NO;
    [self getTotalIPTV];
    [self getToTalOTT];
  
}

// Get total IPTV amount
- (void)getTotalIPTV {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    [self.btnGetTotalIPTV setEnabled:NO];
    if([selectedLocalType.Key isEqualToString:@"0"]){
        [self.btnGetTotalIPTV setEnabled:YES];
        return;
    }
    //vutt11
    [self showMBProcess];
    
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    
    ShareData *shared = [ShareData instance];
    
    [shared.registrationFormProxy GetIPTVTotal:shared.currentUser.userName
                                      sPackage:selectedPackageIPTV.Key
                                  iPromotionID:selectedPromotionIPTV.PromotionId
                                  iChargeTimes:self.txtIPTVChargeTimes.text
                                      iPrepaid:self.txtIPTVPrepaid.text
                                     iBoxCount:self.txtDeviceBoxCount.text
                                     iPLCCount:self.txtDeviceBoxCount.text
                                    iReturnSTB:self.txtDeviceSTBCount.text
                           iVTVCabPrepaidMonth:self.txtIPTVVTVPrepaidMonth.text
                            iVTVCabChargeTimes:self.txtIPTVVTVChargeTimes.text
                            iKPlusPrepaidMonth:self.txtIPTVKPlusPrepaidMonth.text
                             iKPlusChargeTimes:self.txtIPTVKPlusChargeTimes.text
                              iVTCPrepaidMonth:self.txtIPTVVTCPrepaidMonth.text
                               iVTCChargeTimes:self.txtIPTVVTCChargeTimes.text
                              iHBOPrepaidMonth:@"0"
                               iHBOChargeTimes:@"0"
                                  IServiceType:selectedLocalType.Key
                           iFimPlusChargeTimes:self.txtIPTVFimPlusChargeTimes.text
                          iFimPlusPrepaidMonth:self.txtIPTVFimPlusPrepaidMonth.text
                            iFimHotChargeTimes:self.txtIPTVFimHotChargeTimes.text
                           iFimHotPrepaidMonth:self.txtIPTVFimHotPrepaidMonth.text
                      iIPTVPromotionIDBoxOrder:selectedPromotionIPTVBoxOrder.PromotionId
                             IPTVPromotionType:@"-2"
                     IPTVPromotionTypeBoxOrder:@"-1"
                                      Contract:@""
                                       RegCode:@""
                                Completehander:^(id result, NSString *errorCode, NSString *message) {
        
                                    if ([message isEqualToString:@"het phien lam viec"]) {
                                        [self ShowAlertErrorSession];
                                        [self LogOut];
                                        [self hideMBProcess];
                                        return;
                                    }
                                    if(![errorCode isEqualToString:@"0"]){
                                        [self hideMBProcess];
                                        [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
                                        [self.btnGetTotalIPTV setEnabled:YES];
                                        return ;
                                    }
                                    NSDictionary *dict = result;
                                    amountIPTV = [ StringFormat(@"%@",[dict objectForKey:@"Total"]) intValue];
                                    self.txtIPTVTotal.text =[nf stringFromNumber:[NSNumber numberWithFloat:amountIPTV]];
                                    DeviceTotal = StringFormat(@"%@",[dict objectForKey:@"DeviceTotal"]);
                                    PrepaidTotal = StringFormat(@"%@",[dict objectForKey:@"PrepaidTotal"]);
                                    self.txtIPTVPrepaid.text = StringFormat(@"%@",PrepaidTotal);
                                    
                                    self.txtIPTVTotal.text =[nf stringFromNumber:[NSNumber numberWithFloat:amountIPTV]];
                                    self.txtIPTVMonthlyTotal.text = StringFormat(@"%@ VNĐ",[nf stringFromNumber:[NSNumber numberWithFloat:[[dict objectForKey:@"MonthlyTotal"] intValue]]]);
                                    
                                    [self Total];
                                    flagtotaliptv = TRUE;
                                    [self.btnGetTotalIPTV setEnabled:YES];
                                    
                                    [self hideMBProcess];
                                    
                                    if (isGetIPTVTotal == YES) {
                                        isGetIPTVTotal = NO;
                                        [self showActionSheetUpdate];
                                    }
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@- %@",@"GetIPTVTotal",[error localizedDescription]]];
        [self hideMBProcess];
        [self.btnGetTotalIPTV setEnabled:YES];
    }];
    
//    [shared.registrationFormProxy GetIPTVTotal:shared.currentUser.userName
//                                      sPackage:selectedPackageIPTV.Key
//                                  iPromotionID:selectedPromotionIPTV.PromotionId
//                                  iChargeTimes:self.txtIPTVChargeTimes.text
//                                      iPrepaid:self.txtIPTVPrepaid.text
//                                     iBoxCount:self.txtDeviceBoxCount.text
//                                     iPLCCount:self.txtDevicePLCCount.text
//                                    iReturnSTB:self.txtDeviceSTBCount.text
//                           iVTVCabPrepaidMonth:self.txtIPTVVTVPrepaidMonth.text
//                            iVTVCabChargeTimes:self.txtIPTVVTVChargeTimes.text
//                            iKPlusPrepaidMonth:self.txtIPTVKPlusPrepaidMonth.text
//                             iKPlusChargeTimes:self.txtIPTVKPlusChargeTimes.text
//                              iVTCPrepaidMonth:self.txtIPTVVTCPrepaidMonth.text
//                               iVTCChargeTimes:self.txtIPTVVTCChargeTimes.text
//                              iHBOPrepaidMonth: @"0"
//                               iHBOChargeTimes:@"0"
//                                  IServiceType:selectedLocalType.Key
//                           iFimPlusChargeTimes:self.txtIPTVFimPlusChargeTimes.text
//                          iFimPlusPrepaidMonth:self.txtIPTVFimPlusPrepaidMonth.text
//                            iFimHotChargeTimes:self.txtIPTVFimHotChargeTimes.text
//                           iFimHotPrepaidMonth:self.txtIPTVFimHotPrepaidMonth.text
//                      iIPTVPromotionIDBoxOrder:selectedPromotionIPTVBoxOrder.PromotionId
//                                Completehander:^(id result, NSString *errorCode, NSString *message) {
//        
//        if ([message isEqualToString:@"het phien lam viec"]) {
//            [self ShowAlertErrorSession];
//            [self LogOut];
//            [self hideMBProcess];
//            return;
//        }
//        if(![errorCode isEqualToString:@"0"]){
//            [self hideMBProcess];
//            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
//            [self.btnGetTotalIPTV setEnabled:YES];
//            return ;
//        }
//        NSDictionary *dict = result;
//        amountIPTV = [ StringFormat(@"%@",[dict objectForKey:@"Total"]) intValue];
//        self.txtIPTVTotal.text =[nf stringFromNumber:[NSNumber numberWithFloat:amountIPTV]];
//        DeviceTotal = StringFormat(@"%@",[dict objectForKey:@"DeviceTotal"]);
//        PrepaidTotal = StringFormat(@"%@",[dict objectForKey:@"PrepaidTotal"]);
//        self.txtIPTVPrepaid.text = StringFormat(@"%@",PrepaidTotal);
//        
//        self.txtIPTVTotal.text =[nf stringFromNumber:[NSNumber numberWithFloat:amountIPTV]];
//        self.txtIPTVMonthlyTotal.text = StringFormat(@"%@ VNĐ",[nf stringFromNumber:[NSNumber numberWithFloat:[[dict objectForKey:@"MonthlyTotal"] intValue]]]);
//        
//        [self Total];
//        flagtotaliptv = TRUE;
//        [self.btnGetTotalIPTV setEnabled:YES];
//        
//        [self hideMBProcess];
//        
//        if (isGetIPTVTotal == YES) {
//            isGetIPTVTotal = NO;
//            [self showActionSheetUpdate];
//        }
//        
//    } errorHandler:^(NSError *error) {
//        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@- %@",@"GetIPTVTotal",[error localizedDescription]]];
//        [self hideMBProcess];
//        [self.btnGetTotalIPTV setEnabled:YES];
//    }];
}

- (void)getToTalOTT {
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    
    stringCount = [NSString stringWithFormat:@"%ld",(long)self.count];
     //[self showMBProcess];
     ShareData *shared = [ShareData instance];
    
//    [shared.registrationFormProxy GetToTalOTT:<#(NSString *)#> OTTBoxCount:<#(NSString *)#> dicJSON:<#(NSDictionary *)#> Completehander:^(id result, NSString *errorCode, NSString *message) {
//        
//        NSDictionary *dict = result;
//        amountOTT = [StringFormat(@"%@",[dict objectForKey:@"Amount"])intValue];
//        
//        self.txtOTTTotal.text =[nf stringFromNumber:[NSNumber numberWithFloat:amountOTT]];
//        
//        
//        [self Total];
//        [self hideMBProcess];
//        
//    } errorHandler:^(NSError *error) {
//        
//        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@- %@",@"GetToTalOTT",[error localizedDescription]]];
//        [self hideMBProcess];
//        
//    }];
    
//   [shared.registrationFormProxy GetToTalOTT:shared.currentUser.userName OTTBoxCount:stringCount Completehander:^(id result, NSString *errorCode, NSString *message) {
//       
//       NSDictionary *dict = result;
//       amountOTT = [StringFormat(@"%@",[dict objectForKey:@"Amount"])intValue];
//       
//        self.txtOTTTotal.text =[nf stringFromNumber:[NSNumber numberWithFloat:amountOTT]];
//       
//
//        [self Total];
//        [self hideMBProcess];
//       
//       
//   } errorHandler:^(NSError *error) {
//    
//       [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@- %@",@"GetToTalOTT",[error localizedDescription]]];
//       [self hideMBProcess];
//       
//   }];
    
}




/*
 * SHOW SUGGESTIONS VIEW
 */
- (IBAction)btnSuggestionSearch_Clicked:(id)sender {
    [self.view endEditing:YES];
    
    SuggestionsSearchViewController *vc = [[SuggestionsSearchViewController alloc] init];
    vc.delegate = self;
    vc.type = suggestionsSearchPromotionStatement;
    [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomTop];
    
}

- (IBAction)btnSuggestionAddress_Clicked:(id)sender {
    [self.view endEditing:YES];
    
    SuggestionsSearchViewController *vc = [[SuggestionsSearchViewController alloc] init];
    vc.delegate = self;
    vc.type = SuggestionsFillHouseNumber;
    [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomTop];
    
}

- (IBAction)btnPaymentType_Clicked:(id)sender {
    [self setupDropDownView:PaymentType];
}

- (IBAction)btnRefreshPaymentType_Clicked:(id)sender {
    [self showMBProcess];
    self.btnNameVilla.selected = YES;
    if (self.Id.length <= 0 || [self.Id isEqualToString:@"0"]) {
        [self loadPaymentType:@"0"];
    } else {
        [self loadPaymentType:self.rc.Payment];
    }
}

//vutt11
//Chon so luong box cho OTT

- (IBAction)btnMaxBoxOTT_clicked:(id)sender {
    
    if ([self.registedOTT isOn]) {
        [self.btnMaxBoxOTT setEnabled:YES];
       [self changedValueCount:sender];
    }
    else
    {
        [self.btnMaxBoxOTT setEnabled:NO];
    }
    
    
}

- (IBAction)btnMinBoxOTT_clicked:(id)sender {
    if ([self.registedOTT isOn]) {
        
        [self changedValueCount:sender];
        [self.btnMinBoxOTT setEnabled:YES];
    }
    else{
        [self.btnMinBoxOTT setEnabled:NO];
    }
    
}


- (void)CancelPopupSuggestions {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
}

#pragma mark - load dropdownlist

-(void)LoadPhone1:(NSString *) Id {
    self.arrPhone1 = [Common getPhoneType];
    int k = 0;
    if([Id isEqualToString:@"0"]){
        selectedPhone1 = [self.arrPhone1 objectAtIndex:0];
        [self.btnPhone1 setTitle:selectedPhone1.Values forState:UIControlStateNormal];
    }else {
        for (k = 0; k< self.arrPhone1.count; k++) {
            selectedPhone1 = [self.arrPhone1 objectAtIndex:k];
            if([Id isEqualToString:selectedPhone1.Key]){
                [self.btnPhone1 setTitle:selectedPhone1.Values forState:UIControlStateNormal];
                return;
            }
        }
    }
}

-(void)LoadPhone2:(NSString *) Id {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    self.arrPhone2 = [Common getPhoneType];
    int k = 0;
    if([Id isEqualToString:@"0"]){
        selectedPhone2 = [self.arrPhone2 objectAtIndex:0];
        [self.btnPhone2 setTitle:selectedPhone2.Values forState:UIControlStateNormal];
    }else {
        for (k = 0; k< self.arrPhone2.count; k++) {
            selectedPhone2 = [self.arrPhone2 objectAtIndex:k];
            if([Id isEqualToString:selectedPhone2.Key]){
                [self.btnPhone2 setTitle:selectedPhone2.Values forState:UIControlStateNormal];
                return;
            }
        }
    }
    
}

-(void)LoadObjectCustomer:(NSString *) Id {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    self.arrObjectCustomer = [Common getPartNer];
    int k = 0;
    if([Id isEqualToString:@"0"]){
        selectedObjectCustomer = [self.arrObjectCustomer objectAtIndex:0];
        [self.btnObjectCustomer setTitle:selectedObjectCustomer.Values forState:UIControlStateNormal];
    }else {
        for (k = 0; k< self.arrObjectCustomer.count; k++) {
            selectedObjectCustomer = [self.arrObjectCustomer objectAtIndex:k];
            if([Id isEqualToString:selectedObjectCustomer.Key]){
                [self.btnObjectCustomer setTitle:selectedObjectCustomer.Values forState:UIControlStateNormal];
                return;
            }
        }
    }
    
}

-(void)LoadISP:(NSString *) Id {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    self.arrISP = [Common getISP];
    int k = 0;
    if([Id isEqualToString:@"0"]){
        selectedISP = [self.arrISP objectAtIndex:0];
        [self.btnISP setTitle:selectedISP.Values forState:UIControlStateNormal];
    }else {
        for (k = 0; k< self.arrISP.count; k++) {
            selectedISP = [self.arrISP objectAtIndex:k];
            if([Id isEqualToString:selectedISP.Key]){
                [self.btnISP setTitle:selectedISP.Values forState:UIControlStateNormal];
                return;
            }
        }
    }
    
}

-(void)LoadDeposit:(NSString *)Id {
    
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    ShareData *shared = [ShareData instance];
    if (self.regcode.length <= 0) {
        self.regcode = @"";
    }
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    [dict setObject:self.regcode forKey:@"RegCode"];
    
    [shared.registrationFormProxy getContractDepositList:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if (errorCode.length < 1) {
            errorCode=@"<null>";
        }
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        self.arrDeposit = result;
        
        for ( selectedDeposit in self.arrDeposit ) {
            if([Id isEqualToString:selectedDeposit.Key]){
                [self.btnDeposit setTitle:selectedDeposit.Values forState:UIControlStateNormal];
                return;
            }
        }
        
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetContractDepositList",[error localizedDescription]]];
    }];
    
}

-(void)LoadDepositBlackPoint:(NSString *) Id {
    self.arrDepositBlackPoint = [Common getDepositBlackPoint];
    int k = 0;
    if([Id isEqualToString:@"0"]){
        selectedDepositBlackPoint= [self.arrDepositBlackPoint objectAtIndex:0];
        [self.btnDepositBlackPoint setTitle:selectedDepositBlackPoint.Values forState:UIControlStateNormal];
    }else {
        for (k = 0; k< self.arrDepositBlackPoint.count; k++) {
            selectedDepositBlackPoint = [self.arrDepositBlackPoint objectAtIndex:k];
            if([Id isEqualToString:selectedDepositBlackPoint.Key]){
                [self.btnDepositBlackPoint setTitle:selectedDepositBlackPoint.Values forState:UIControlStateNormal];
                return;
            }
        }
    }
    
}

-(void)LoadTypeCustomer:(NSString *) Id {
    self.arrTypeCustomer = [Common getCustomerType];
    int k = 0;
    if([Id isEqualToString:@"0"]){
        // Default load "Hộ gia đình"
        selectedTypeCustomer = [self.arrTypeCustomer objectAtIndex:2];
        [self.btnTypeCustomer setTitle:selectedTypeCustomer.Values forState:UIControlStateNormal];
    }else {
        for (k = 0; k< self.arrTypeCustomer.count; k++) {
            selectedTypeCustomer = [self.arrTypeCustomer objectAtIndex:k];
            if([Id isEqualToString:selectedTypeCustomer.Key]){
                [self.btnTypeCustomer setTitle:selectedTypeCustomer.Values forState:UIControlStateNormal];
                return;
            }
        }
    }
    
}

-(void)LoadCurrenPlace:(NSString *) Id {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    self.arrPlaceCurrent = [Common getCurrentPlace];
    int k = 0;
    if([Id isEqualToString:@"0"]){
        selectedPlaceCurrent = [self.arrPlaceCurrent objectAtIndex:0];
        [self.btnPlaceCurrent setTitle:selectedPlaceCurrent.Values forState:UIControlStateNormal];
    }else {
        for (k = 0; k< self.arrPlaceCurrent.count; k++) {
            selectedPlaceCurrent = [self.arrPlaceCurrent objectAtIndex:k];
            if([Id isEqualToString:selectedPlaceCurrent.Key]){
                [self.btnPlaceCurrent setTitle:selectedPlaceCurrent.Values forState:UIControlStateNormal];
                return;
            }
        }
    }
    
}

-(void)LoadClientPartner:(NSString *) Id {
    self.arrClientPartners = [Common getPartCustomer];
    int k = 0;
    if([Id isEqualToString:@"0"]){
        selectedClientPartners = [self.arrClientPartners objectAtIndex:0];
        [self.btnClientPartners setTitle:selectedClientPartners.Values forState:UIControlStateNormal];
    }else {
        for (k = 0; k< self.arrClientPartners.count; k++) {
            selectedClientPartners = [self.arrClientPartners objectAtIndex:k];
            if([Id isEqualToString:selectedClientPartners.Key]){
                [self.btnClientPartners setTitle:selectedClientPartners.Values forState:UIControlStateNormal];
                return;
            }
        }
    }
    
}

-(void)LoadSolveny:(NSString *) Id {
    self.arrSolvency = [Common getSolvency];
    int k = 0;
    if([Id isEqualToString:@"0"]){
        selectedSolvency = [self.arrSolvency objectAtIndex:0];
        [self.btnSolvency setTitle:selectedSolvency.Values forState:UIControlStateNormal];
    }else {
        for (k = 0; k< self.arrSolvency.count; k++) {
            selectedSolvency = [self.arrSolvency objectAtIndex:k];
            if([Id isEqualToString:selectedSolvency.Key]){
                [self.btnSolvency setTitle:selectedSolvency.Values forState:UIControlStateNormal];
                return;
            }
        }
    }
    
}

-(void)LoadLegal:(NSString *) Id {
    self.arrLegal = [Common getLegal];
    int k = 0;
    if([Id isEqualToString:@"0"]){
        selectedLegal = [self.arrLegal objectAtIndex:0];
        [self.btnLegal setTitle:selectedLegal.Values forState:UIControlStateNormal];
    }else {
        for (k = 0; k< self.arrLegal.count; k++) {
            selectedLegal = [self.arrLegal objectAtIndex:k];
            if([Id isEqualToString:selectedLegal.Key]){
                [self.btnLegal setTitle:selectedLegal.Values forState:UIControlStateNormal];
                return;
            }
        }
    }
    
}

-(void)LoadCusType:(NSString *) Id {
    self.arrCusType = [Common getCusType];
    int k = 0;
    if([Id isEqualToString:@"0"]){
        selectedCusType = [self.arrCusType objectAtIndex:0];
        [self.btnCusType setTitle:selectedCusType.Values forState:UIControlStateNormal];
    }else {
        for (k = 0; k< self.arrCusType.count; k++) {
            selectedCusType = [self.arrCusType objectAtIndex:k];
            if([Id isEqualToString:selectedCusType.Key]){
                [self.btnCusType setTitle:selectedCusType.Values forState:UIControlStateNormal];
                return;
            }
        }
    }
    
}

-(void)LoadTypeHouse:(NSString *) Id {
    self.arrTypeHouse = [Common getTypeHouse];
    int k = 0;
    if([Id isEqualToString:@"0"]){
        selectedTypeHouse = [self.arrTypeHouse objectAtIndex:0];
        [self.btnTypeHouse setTitle:selectedTypeHouse.Values forState:UIControlStateNormal];
    }else {
        for (k = 0; k< self.arrTypeHouse.count; k++) {
            selectedTypeHouse = [self.arrTypeHouse objectAtIndex:k];
            if([Id isEqualToString:selectedTypeHouse.Key]){
                [self.btnTypeHouse setTitle:selectedTypeHouse.Values forState:UIControlStateNormal];
                return;
            }
        }
    }
    
}

-(void)LoadDeployment:(NSString *) Id {
    self.arrDeployment = [Common getDeployment];
    int k = 0;
    if([Id isEqualToString:@"0"]){
        selectedDeployment = [self.arrDeployment objectAtIndex:0];
        [self.btnDeployment setTitle:selectedDeployment.Values forState:UIControlStateNormal];
    }else {
        for (k = 0; k< self.arrDeployment.count; k++) {
            selectedDeployment = [self.arrDeployment objectAtIndex:k];
            if([Id isEqualToString:selectedDeployment.Key]){
                [self.btnDeployment setTitle:selectedDeployment.Values forState:UIControlStateNormal];
                return;
            }
        }
    }
    
}

- (void)LoadCombo:(NSString *)Id {
    self.arrCombo = [Common getCombo];
    if([Id isEqualToString:@"0"]){
        selectedCombo = [self.arrCombo objectAtIndex:0];
        [self.btnCombo setTitle:selectedCombo.Values forState:UIControlStateNormal];
        return;
    }
    if([Id isEqualToString:@"2"]){
        selectedCombo = [self.arrCombo objectAtIndex:1];
        [self.btnCombo setTitle:selectedCombo.Values forState:UIControlStateNormal];
        return;
    }
    if([Id isEqualToString:@"3"]){
        selectedCombo = [self.arrCombo objectAtIndex:2];
        [self.btnCombo setTitle:selectedCombo.Values forState:UIControlStateNormal];
        return;
    }
}

-(void)LoadLocalType:(NSString *) Id PromotionId:(NSString *) promotionid {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    ShareData *shared = [ShareData instance];
    [shared.appProxy GetListLocalType:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        self.arrServicePack = result;
        int k = 0;
        if(self.arrServicePack.count > 0){
            if([Id isEqualToString:@"0"]){
                selectedServicePack = [self.arrServicePack objectAtIndex:0];
                [self.btnServicePack setTitle:selectedServicePack.Values forState:UIControlStateNormal];
            }else {
                for (k = 0; k< self.arrServicePack.count; k++) {
                    selectedServicePack = [self.arrServicePack objectAtIndex:k];
                    if([Id isEqualToString:selectedServicePack.Key]){
                        [self.btnServicePack setTitle:selectedServicePack.Values forState:UIControlStateNormal];
                        break;
                    }
                }
            }
            [self LoadPromotion:promotionid LocalType:selectedServicePack.Key];
        }
        
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetListLocalType",[error localizedDescription]]];
        [self hideMBProcess];
    }];
    
}

-(void)LoadLocalTypePost:(NSString *)Id serverPackageID:(NSString *)serverPackageID PromotionId:(NSString *)promotionid {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        self.btnLocalType.selected = NO;
        [self hideMBProcess];
        return;
    }
    ShareData *shared = [ShareData instance];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:Id forKey:@"ServiceType"];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    
    if (self.btnLocalType.selected == YES) {
        [self showMBProcess];
    }
    
    [shared.appProxy GetListLocalTypePost:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            self.btnLocalType.selected = NO;
            return ;
        }
        
        self.arrServicePack = result;
        int k = 0;
        
        if(self.arrServicePack.count > 0) {
            
            if([serverPackageID isEqualToString:@"0"] || self.btnLocalType.selected == YES ){
                
                selectedServicePack = [self.arrServicePack objectAtIndex:0];
                [self.btnServicePack setTitle:selectedServicePack.Values forState:UIControlStateNormal];
                
            }else {
                
                for (k = 0; k< self.arrServicePack.count; k++) {
                    selectedServicePack = [self.arrServicePack objectAtIndex:k];
                    if([serverPackageID isEqualToString:selectedServicePack.Key]){
                        [self.btnServicePack setTitle:selectedServicePack.Values forState:UIControlStateNormal];
                        break;
                    }
                }
            }
            
            [self LoadPromotion:promotionid LocalType:selectedServicePack.Key];
            
            return;
        }
        
        if (self.btnLocalType.selected == YES) {
            [self hideMBProcess];
            self.btnLocalType.selected = NO;
        }
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetListLocalType",[error localizedDescription]]];
        [self hideMBProcess];
        self.btnLocalType.selected = NO;
        
    }];
    
}

-(void)LoadPromotion:(NSString *)Id LocalType:(NSString *)localtype {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        self.btnLocalType.selected = NO;
        self.btnServicePack.selected = NO;
        return;
    }
    if (self.btnServicePack.selected == YES) {
        [self showMBProcess];
    }
    ShareData *shared = [ShareData instance];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [shared.appProxy GetListPromotion:localtype Complatehander:^(id result, NSString *errorCode, NSString *message) {
            if ([message isEqualToString:@"het phien lam viec"]) {
                [self ShowAlertErrorSession];
                [self LogOut];
                [self hideMBProcess];
                return;
            }
            if (errorCode.length < 1) {
                errorCode=@"<null>";
            }
            if(![errorCode isEqualToString:@"<null>"]){
                [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
                [self hideMBProcess];
                self.btnLocalType.selected = NO;
                self.btnServicePack.selected = NO;
                return ;
            }
            self.arrPromotion = result;
            
            if (self.arrPromotion.count <= 0 || [Id isEqualToString:@"0"]) {
                selectedPromotion = [[PromotionModel alloc] initWithName:@"0" description:@"[Chọn câu lệnh khuyến mãi]" Amount:@"0" Type:@""];
                [self.btnPromotion setTitle:selectedPromotion.PromotionName forState:UIControlStateNormal];
                self.txtPromotionInternet.text = selectedPromotion.PromotionName;
            }
            
            if (self.arrPromotion.count > 0 && ![Id isEqualToString:@"0"]) {
                int k = 0 ;
                for (k = 0; k< self.arrPromotion.count; k++) {
                    selectedPromotion = [self.arrPromotion objectAtIndex:k];
                    if([Id isEqualToString:selectedPromotion.PromotionId]){
                        
                        if ([selectedPromotion.PromotionName isKindOfClass:[NSString class]]) {
                            
                            [self.btnPromotion setTitle:selectedPromotion.PromotionName forState:UIControlStateNormal];
                            
                            self.txtPromotionInternet.text = selectedPromotion.PromotionName;
                            
                            break;
                            
                        }else{
                            [self hideMBProcess];
                            [self showAlertBox:@"Thông Báo" message:@"Dữ liệu không đúng"];
                            break ;
                        }
                        
                    }
                }
            }
            
            NSNumberFormatter *nf = [NSNumberFormatter new];
            nf.numberStyle = NSNumberFormatterDecimalStyle;
            if([selectedLocalType.Key isEqualToString:@"1"]){
                amountInternet = 0;
                
            }else {
                amountInternet = [selectedPromotion.Amount intValue];
            }
            
            self.txtInternetTotal.text = [nf stringFromNumber:[NSNumber numberWithFloat:amountInternet]];
            [self Total];
            
            if (self.btnLocalType.selected == YES) {
                self.btnLocalType.selected = NO;
                [self hideMBProcess];
                return;
            }
            
            if (self.btnServicePack.selected == YES) {
                [self hideMBProcess];
                self.btnServicePack.selected = NO;
                return;
            }
            
            [self hideMBProcess];
            
        } errorHandler:^(NSError *error) {
            [self hideMBProcess];
            self.btnLocalType.selected = NO;
            self.btnServicePack.selected = NO;
            [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetListPromotion",[error localizedDescription]]];
        }];
        
    });
    
}

-(void)LoadDistrict:(NSString *)Id WardId:(NSString *)wardid Street:(NSString *)street {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    ShareData *shared = [ShareData instance];
    NSString *locationid = shared.currentUser.LocationID;
    [shared.appProxy GetDistrictList:locationid completionHandler:^(id result, NSString *errorCode, NSString *message) {
        self.arrDistrict = result;
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        
        if (self.arrDistrict.count <= 0) {
            [self.btnDistrict setTitle:@"" forState:UIControlStateNormal];
            return;
        }
        
        if(self.arrDistrict.count > 0){
            int k = 0;
            if([Id isEqualToString:@"0"]){
                [self.btnDistrict setTitle:@"[Vui lòng chọn quận/huyện]" forState:UIControlStateNormal];
            }else {
                for (k = 0; k< self.arrDistrict.count; k++) {
                    selectedDistrict = [self.arrDistrict objectAtIndex:k];
                    if([Id isEqualToString:selectedDistrict.Key]){
                        [self.btnDistrict setTitle:selectedDistrict.Values forState:UIControlStateNormal];
                        break;
                    }
                }
                
            }
            if ([selectedDistrict.Key length] >0 ) {
                [self LoadWard:wardid Street:street];
            }
        }
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@- %@",@"GetDistrictList",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}

-(void)LoadWard:(NSString *)Id Street:(NSString *)street {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        self.btnDistrict.selected = NO;
        return;
    }
    if (self.btnDistrict.selected == YES) {
        [self showMBProcess];
    }
    
    ShareData *shared = [ShareData instance];
    [shared.appProxy GetWardList:shared.currentUser.LocationID DistrictName:selectedDistrict.Key completionHandler:^(id result, NSString *errorCode, NSString *message) {
        self.arrWard = result;
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            self.btnDistrict.selected = NO;
            [self.btnWard setTitle:@"" forState:UIControlStateNormal];
            self.arrStreet = nil;
            [self.btnStreet setTitle:@"" forState:UIControlStateNormal];
            self.arrNameVilla = nil;
            [self.btnNameVilla setTitle:@"" forState:UIControlStateNormal];
            
            selectedWard = nil;
            selectedStreet = nil;
            selectedNameVilla = nil;
            
            return ;
        }
        
        if (self.arrWard.count <= 0) {
            if (self.btnDistrict.selected == YES) {
                [self hideMBProcess];
            }
            self.btnDistrict.selected = NO;
            [self.btnWard setTitle:@"" forState:UIControlStateNormal];
            self.arrStreet = nil;
            [self.btnStreet setTitle:@"" forState:UIControlStateNormal];
            self.arrNameVilla = nil;
            [self.btnNameVilla setTitle:@"" forState:UIControlStateNormal];
            
            selectedWard = nil;
            selectedStreet = nil;
            selectedNameVilla = nil;
            
            return;
        }
        
        if(self.arrWard.count > 0){
            int k = 0;
            if([Id isEqualToString:@"0"] || Id == nil){
                selectedWard = [self.arrWard objectAtIndex:0];
                [self.btnWard setTitle:selectedWard.Values forState:UIControlStateNormal];
            }else {
                for (k = 0; k< self.arrWard.count; k++) {
                    selectedWard = [self.arrWard objectAtIndex:k];
                    if([Id isEqualToString:selectedWard.Key]){
                        [self.btnWard setTitle:selectedWard.Values forState:UIControlStateNormal];
                        break;
                    }
                }
            }
            
            [self LoadStreet:street];
            
        }
        
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@- %@",@"GetWardList",[error localizedDescription]]];
        [self hideMBProcess];
        self.btnDistrict.selected = NO;
        self.arrWard = nil;
        [self.btnWard setTitle:@"" forState:UIControlStateNormal];
        self.arrStreet = nil;
        [self.btnStreet setTitle:@"" forState:UIControlStateNormal];
        self.arrNameVilla = nil;
        [self.btnNameVilla setTitle:@"" forState:UIControlStateNormal];
        selectedWard = nil;
        selectedStreet = nil;
        selectedNameVilla = nil;
        
    }];
}

-(void)LoadStreet:(NSString *)Id {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        [self resetButtonSelected];
        return;
    }
    if (self.btnWard.selected == YES) {
        [self showMBProcess];
    }
    NSString *District=selectedDistrict.Key;
    NSString *selWard=selectedWard.Key;
    
    ShareData *shared = [ShareData instance];
    [shared.appProxy GetStreetOrCondominiumList:shared.currentUser.LocationID DistrictName:District Ward:selWard Type:@"0" completionHandler:^(id result, NSString *errorCode, NSString *message) {
        
        self.arrStreet = result;
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(errorCode.length <1){
            errorCode=@"<null>";
        }
        
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            [self resetButtonSelected];
            [self.btnStreet setTitle:@"" forState:UIControlStateNormal];
            self.arrNameVilla = nil;
            [self.btnNameVilla setTitle:@"" forState:UIControlStateNormal];
            
            selectedStreet = nil;
            selectedNameVilla = nil;
            return ;
        }
        
        if (self.arrStreet.count <= 0) {
            if (self.btnDistrict.selected == YES || self.btnWard.selected == YES) {
                [self hideMBProcess];
            }
            [self resetButtonSelected];
            [self.btnStreet setTitle:@"" forState:UIControlStateNormal];
            self.arrNameVilla = nil;
            [self.btnNameVilla setTitle:@"" forState:UIControlStateNormal];
            selectedStreet = nil;
            selectedNameVilla = nil;
            
            return;
        }
        
        if(self.arrStreet.count > 0){
            int k = 0;
            if([Id isEqualToString:@"0"]){
                selectedStreet = [self.arrStreet objectAtIndex:0];
                [self.btnStreet setTitle:selectedStreet.Values forState:UIControlStateNormal];
            }else {
                for (k = 0; k< self.arrStreet.count; k++) {
                    selectedStreet = [self.arrStreet objectAtIndex:k];
                    if([Id isEqualToString:selectedStreet.Key]){
                        [self.btnStreet setTitle:selectedStreet.Values forState:UIControlStateNormal];
                        break;
                    }
                }
            }
        }
        
        if (self.Id.length <= 0 || [self.Id isEqualToString:@"0"]) {
            [self LoadNameVilla:@"0"];
            
        } else {
            [self LoadNameVilla:self.rc.NameVilla];
            
        }
        
    } errorHandler:^(NSError *error) {
        [self resetButtonSelected];
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetStreetOrCondominiumList-LoadStreet",[error localizedDescription]]];
        [self hideMBProcess];
        [self.btnStreet setTitle:@"" forState:UIControlStateNormal];
        self.arrNameVilla = nil;
        [self.btnNameVilla setTitle:@"" forState:UIControlStateNormal];
    }];
    
}

-(void)LoadPosition:(NSString *)Id {
    int k = 0;
    self.arrPosition = [Common getHouseCoordinate];
    if([Id isEqualToString:@"0"]){
        selectedPosition = [self.arrPosition objectAtIndex:0];
        [self.btnPosition setTitle:selectedPosition.Values forState:UIControlStateNormal];
    }else {
        for (k = 0; k< self.arrPosition.count; k++) {
            selectedPosition = [self.arrPosition objectAtIndex:k];
            if([Id isEqualToString:selectedPosition.Key]){
                [self.btnPosition setTitle:selectedPosition.Values forState:UIControlStateNormal];
                return;
            }
        }
    }
    
}

-(void)LoadNameVilla:(NSString *)Id {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        [self resetButtonSelected];
        return;
    }
    if (self.btnStreet.selected == YES) {
        [self showMBProcess];
    }
    if (Id.length <1) {
        Id = @"0";
    }
    ShareData *shared = [ShareData instance];
    [shared.appProxy GetStreetOrCondominiumList:shared.currentUser.LocationID DistrictName:selectedDistrict.Key Ward:selectedWard.Key Type:@"1" completionHandler:^(id result, NSString *errorCode, NSString *message) {
        
        self.arrNameVilla = result;
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            [self.btnNameVilla setTitle:@"" forState:UIControlStateNormal];
            selectedNameVilla = nil;
            [self resetButtonSelected];
            
            return ;
        }
        
        if (self.arrNameVilla.count <= 0) {
            [self.btnNameVilla setTitle:@"" forState:UIControlStateNormal];
            selectedNameVilla = nil;
        }
        
        if(self.arrNameVilla.count > 0){
            if([Id isEqualToString:@"0"]){
                selectedNameVilla = [self.arrNameVilla objectAtIndex:0];
                
            }else {
                for (selectedNameVilla in self.arrNameVilla) {
                    if([Id isEqualToString:selectedNameVilla.Key]){
                        break;
                    }
                }
            }
            
            [self.btnNameVilla setTitle:selectedNameVilla.Values forState:UIControlStateNormal];
            
        }
        
        if (self.Id.length <= 0 || [self.Id isEqualToString:@"0"]) {
            [self loadPaymentType:@"0"];
            
        } else {
            [self loadPaymentType:self.rc.Payment];
            
        }
        
        if (self.btnStreet.selected == YES || self.btnDistrict.selected == YES || self.btnWard.selected == YES) {
            [self hideMBProcess];
            [self resetButtonSelected];
            return;
        }
        
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetStreetOrCondominiumList",[error localizedDescription]]];
        [self.btnNameVilla setTitle:@"" forState:UIControlStateNormal];
        selectedNameVilla = nil;
        [self resetButtonSelected];
    }];
}

- (void)loadPaymentType:(NSString *)Id {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self resetButtonSelected];
        [self hideMBProcess];
        return;
    }
    
    if (self.arrPaymentType.count > 0) {
        for ( selectedPaymentType in self.arrPaymentType ) {
            if([Id isEqualToString:selectedPaymentType.Key]){
                [self.btnPaymentType setTitle:selectedPaymentType.Values forState:UIControlStateNormal];
                [self resetButtonSelected];
                [self hideMBProcess];
                return;
            }
        }
    }
    
    if (selectedDistrict == nil) {
        selectedDistrict = [[KeyValueModel alloc]initWithName:@"" description:@""];
    }
    
    if (selectedWard == nil) {
        selectedWard = [[KeyValueModel alloc]initWithName:@"" description:@""];
    }
    
    if (selectedStreet == nil) {
        selectedStreet = [[KeyValueModel alloc]initWithName:@"" description:@""];
    }
    
    if (selectedNameVilla == nil) {
        selectedNameVilla = [[KeyValueModel alloc]initWithName:@"" description:@""];
    }
    
    ShareData *shared = [ShareData instance];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:shared.currentUser.userName     forKey:@"UserName"];
    [dict setObject:shared.currentUser.LocationName forKey:@"BillTo_City"];
    [dict setObject:selectedDistrict.Key            forKey:@"BillTo_Dictrict"];
    [dict setObject:selectedWard.Key                forKey:@"BillTo_Ward"];
    [dict setObject:selectedStreet.Key              forKey:@"BillTo_Street"];
    [dict setObject:selectedNameVilla.Key           forKey:@"BillTo_NameVilla"];
    [dict setObject:self.txtBilltoNumber.text       forKey:@"BillTo_Number"];
    
    [shared.registrationFormProxy getPaymentType:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        
        if (errorCode.length < 1) {
            errorCode=@"<null>";
        }
        
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self resetButtonSelected];
            [self hideMBProcess];
            return ;
        }
        
        self.arrPaymentType = result;
        
        for ( selectedPaymentType in self.arrPaymentType ) {
            if([Id isEqualToString:selectedPaymentType.Key]){
                [self.btnPaymentType setTitle:selectedPaymentType.Values forState:UIControlStateNormal];
                break;
            }
        }
        
        if (self.btnStreet.selected == YES || self.btnDistrict.selected == YES || self.btnWard.selected == YES || self.btnNameVilla.selected == YES) {
            [self hideMBProcess];
            [self resetButtonSelected];
            return;
        }
        
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetContractDepositList",[error localizedDescription]]];
    }];
}

-(void)LoadPromotionIPTV:(NSString *)Id Package:(NSString *)package boxCount:(NSString *)boxCount {
    // Nếu danh sách CLKM IPTV đã load trước đó thì không load lại
    if (self.arrPromotionIPTV.count > 0 && self.arrPromotionIPTVBoxOrder.count > 0) {
        return;
    }
    
    // Kiểm tra kết nối internet
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        self.btnIPTVPackage.selected = NO;
        self.btnLocalType.selected = NO;
        return;
    }
    if (self.btnIPTVPackage.selected == YES) {
        [self showMBProcess];
    }
    ShareData *shared = [ShareData instance];
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [shared.appProxy GetPromotionIPTVList:package withBoxOrder:boxCount CustomerType:@"1" Contract:@"" RegCode:@"" Handler:^(id result, NSString *errorCode, NSString *message) {
                
                //        if ([message isEqualToString:@"het phien lam viec"]) {
                //            [self ShowAlertErrorSession];
                //            [self LogOut];
                //            [self hideMBProcess];
                //            return;
                //        }
                if(![errorCode isEqualToString:@"0"]){
                    [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
                    [self hideMBProcess];
                    self.btnIPTVPackage.selected = NO;
                    self.btnLocalType.selected = NO;
                    return ;
                }
                
                NSArray *arrData = result;
                if (arrData.count <= 0) {
                    [self hideMBProcess];
                    self.btnIPTVPackage.selected = NO;
                    self.btnLocalType.selected = NO;
                    return ;
                }
                NSInteger count = [boxCount integerValue];
                if (count <= 1) {
                    self.arrPromotionIPTV = [NSMutableArray arrayWithArray:arrData];
                    [self setPromotionIPTVInterfaceWithData:self.arrPromotionIPTV id:Id promotionModel:selectedPromotionIPTV buttonPromotion:self.btnIPTVPromotionID textViewPromotion:self.tvPromotionIPTV labelPromotionAmount:self.lblIPTVPromotionID];
                } else {
                    self.arrPromotionIPTVBoxOrder = [NSMutableArray arrayWithArray:arrData];
                    [self setPromotionIPTVInterfaceWithData:self.arrPromotionIPTVBoxOrder id:Id promotionModel:selectedPromotionIPTVBoxOrder buttonPromotion:self.btnIPTVPromotionIDBoxOrder textViewPromotion:self.tvPromotionIPTVBoxOrder labelPromotionAmount:self.lblIPTVPromotionIDBoxOrder];
                }
                
                [self CheckPromotionIPTV];
                if (self.btnIPTVPackage.selected == YES) {
                    [self hideMBProcess];
                    self.btnIPTVPackage.selected = NO;
                    
                }
                
            } errorHandler:^(NSError *error) {
                
                [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetPromotionIPTVList",[error localizedDescription]]];
                [self hideMBProcess];
                self.btnIPTVPackage.selected = NO;
                self.btnLocalType.selected = NO;
                
            }];
            
//            [shared.appProxy GetPromotionIPTVList:package withBoxOrder:boxCount CustomerType:(NSString *)@"1" Handler:^(id result, NSString *errorCode, NSString *message) {
//                //        if ([message isEqualToString:@"het phien lam viec"]) {
//                //            [self ShowAlertErrorSession];
//                //            [self LogOut];
//                //            [self hideMBProcess];
//                //            return;
//                //        }
//                if(![errorCode isEqualToString:@"0"]){
//                    [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
//                    [self hideMBProcess];
//                    self.btnIPTVPackage.selected = NO;
//                    self.btnLocalType.selected = NO;
//                    return ;
//                }
//                
//                NSArray *arrData = result;
//                if (arrData.count <= 0) {
//                    [self hideMBProcess];
//                    self.btnIPTVPackage.selected = NO;
//                    self.btnLocalType.selected = NO;
//                    return ;
//                }
//                NSInteger count = [boxCount integerValue];
//                if (count <= 1) {
//                    self.arrPromotionIPTV = [NSMutableArray arrayWithArray:arrData];
//                    [self setPromotionIPTVInterfaceWithData:self.arrPromotionIPTV id:Id promotionModel:selectedPromotionIPTV buttonPromotion:self.btnIPTVPromotionID textViewPromotion:self.tvPromotionIPTV labelPromotionAmount:self.lblIPTVPromotionID];
//                } else {
//                    self.arrPromotionIPTVBoxOrder = [NSMutableArray arrayWithArray:arrData];
//                    [self setPromotionIPTVInterfaceWithData:self.arrPromotionIPTVBoxOrder id:Id promotionModel:selectedPromotionIPTVBoxOrder buttonPromotion:self.btnIPTVPromotionIDBoxOrder textViewPromotion:self.tvPromotionIPTVBoxOrder labelPromotionAmount:self.lblIPTVPromotionIDBoxOrder];
//                }
//                
//                [self CheckPromotionIPTV];
//                if (self.btnIPTVPackage.selected == YES) {
//                    [self hideMBProcess];
//                    self.btnIPTVPackage.selected = NO;
//                    
//                }
//                
//            } errorHandler:^(NSError *error) {
//                [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetPromotionIPTVList",[error localizedDescription]]];
//                [self hideMBProcess];
//                self.btnIPTVPackage.selected = NO;
//                self.btnLocalType.selected = NO;
//            }];
        });
    });
    
}

- (void)setPromotionIPTVInterfaceWithData:(NSArray *)arrData id:(NSString *)Id promotionModel:(PromotionModel *)selectedPromotionModel buttonPromotion:(UIButton *)btnPromotion textViewPromotion:(UITextView *)tvPromotion labelPromotionAmount:(UILabel *)lblPromotionAmount{
    
    if(arrData > 0){
        if([Id isEqualToString:@"0"] || Id == nil){
            //            selectedPromotionModel = [arrData objectAtIndex:0];
            selectedPromotionModel = [[PromotionModel alloc] initWithName:@"0" description:@"[Chọn câu lệnh khuyến mãi]" Amount:@"0" Type:@""];
            
        } else {
            for (selectedPromotionModel in arrData) {
                if([Id isEqualToString:selectedPromotionModel.PromotionId]){
                    break;
                }
            }
        }
        
        if (btnPromotion == self.btnIPTVPromotionID) {
            selectedPromotionIPTV = selectedPromotionModel;
            
        } else if (btnPromotion == self.btnIPTVPromotionIDBoxOrder) {
            selectedPromotionIPTVBoxOrder = selectedPromotionModel;
        }
        
        [btnPromotion setTitle:selectedPromotionModel.PromotionName forState:UIControlStateNormal];
        tvPromotion.text = selectedPromotionModel.PromotionName;
        
        NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
        numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
        
        lblPromotionAmount.text = StringFormat(@"(%@ VNĐ)",[numberFormatter stringFromNumber:[NSNumber numberWithFloat:[selectedPromotionModel.Amount intValue]]]);
    }
}

-(void)loadIPTVPrice {
    if (getIPTVPriceSuccess == YES) {
        return;
    }
    ShareData *share = [ShareData instance];
    NSDictionary *dict = [NSDictionary dictionaryWithObject:share.currentUser.userName forKey:@"UserName"];
    [share.registrationFormProxy getIPTVPrice:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        if(![errorCode isEqualToString:@"0"]){
            return ;
        }
        
        getIPTVPriceSuccess = YES;
        
        iptvPrice = result;
        NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
        numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
        
        self.lblFimPlusPrice.text = StringFormat(@"(%@ VNĐ)",[numberFormatter stringFromNumber:[NSNumber numberWithFloat:[iptvPrice.FimPlusPrice intValue]]]);
        
        self.lblFimHotPrice.text  = StringFormat(@"(%@ VNĐ)",[numberFormatter stringFromNumber:[NSNumber numberWithFloat:[iptvPrice.FimHotPrice intValue]]]);
        self.lblKPlusPrice.text   = StringFormat(@"(%@ VNĐ)",[numberFormatter stringFromNumber:[NSNumber numberWithFloat:[iptvPrice.KPlusPrice intValue]]]);
        self.lblDacSacHDPrice.text= StringFormat(@"(%@ VNĐ)",[numberFormatter stringFromNumber:[NSNumber numberWithFloat:[iptvPrice.DacSacHDPrice intValue]]]);
        self.lblVTVCabPrice.text  = StringFormat(@"(%@ VNĐ)",[numberFormatter stringFromNumber:[NSNumber numberWithFloat:[iptvPrice.VTVCabPrice intValue]]]);
        
    } errorHandler:^(NSError *error) {
        NSLog(@"%@",[error description]);
    }];
}

-(void)LoadPackageIPTV:(NSString *)Id PromotionIPTV:(NSString *)promotionIPTV boxCount:(NSString *)boxCount{
    int k = 0;
    self.arrPackageIPTV = [Common getServicePackage];
    if([Id isEqualToString:@"0"] || [Id isEqualToString:@""] || Id == nil){
        selectedPackageIPTV = [self.arrPackageIPTV objectAtIndex:0];
        [self.btnIPTVPackage setTitle:selectedPackageIPTV.Values forState:UIControlStateNormal];
    }else {
        for (k = 0; k< self.arrPackageIPTV.count; k++) {
            selectedPackageIPTV = [self.arrPackageIPTV objectAtIndex:k];
            if([Id isEqualToString:selectedPackageIPTV.Key]){
                [self.btnIPTVPackage setTitle:selectedPackageIPTV.Values forState:UIControlStateNormal];
                break;
            }
        }
        
    }
    
    [self LoadPromotionIPTV:promotionIPTV Package:selectedPackageIPTV.Key boxCount:boxCount];
}
//vutt11
-(void)LocaLocalType:(NSString *)promotionid Package:(NSString *)package {
    //int i = 3;
    self.arrLocalType = [Common getLocalType];
    if(![promotionid isEqualToString:@"0"]){
        i = 0;
        self.txtIPTVPrepaid.text = @"0";
    }
    if(![package isEqualToString:@""] && [promotionid isEqualToString:@"0"]){
        i = 1;
        self.btnPromotion.enabled = YES;
    }
    if((![package isEqualToString:@""]) && (![promotionid isEqualToString:@"0"])){
        i = 2;
    }
    
    if(i != 0){
        
    }
    if (i != 2) {
        [self.btnCombo setEnabled:NO];
    }
    
    // Nếu PĐK là Office 365 only thì ẩn Internet và IPTV.
    if (i == 3) {
        self.btnServicePack.enabled = NO;
        self.btnPromotion.enabled   = NO;
    }
    // Nếu OTT thì ẩn Internet và IPTV.
    if (i== 4) {
        self.btnServicePack.enabled = NO;
        self.btnPromotion.enabled   = NO;
    }
    //vutt11
//    if ([package isEqualToString:@""] && [promotionid isEqualToString:@"0"]) {
//        i = 4;
//    }
    
    selectedLocalType = [self.arrLocalType objectAtIndex:i];
    [self.btnLocalType setTitle:selectedLocalType.Values forState:UIControlStateNormal];
    
    [self CheckRegisterIPTV];
    
}

#pragma  mark - hide address

-(void)ShowHideSelecttypeHouse {
    if([selectedTypeHouse.Key isEqualToString:@"1"]){
        [self.btnSuggestionAddress setHidden:NO];
        [self.txtBilltoNumber setEnabled:YES];
        [self.txtLot setEnabled:NO];
        [self.txtFloor setEnabled:NO];
        [self.txtRoom setEnabled:NO];
        [self.btnNameVilla setEnabled:NO];
        [self.btnPosition setEnabled:NO];
        
        [self.txtBilltoNumber setHidden:NO];
        [self.btnPosition setHidden:YES];
        [self.txtLot setHidden:YES];
        [self.txtFloor setHidden:YES];
        [self.txtRoom setHidden:YES];
        [self.btnNameVilla setHidden:YES];
        [self.lblLot setHidden:YES];
        [self.lblFloor setHidden:YES];
        [self.lblRoom setHidden:YES];
        [self.lblNameVilla setHidden:YES];
        [self.lblPositionHouse setHidden:YES];
        [self.lblNumberHouse setHidden:NO];
        
        self.constraintNumberHouse.constant = 56;
        self.constraintNote.constant = 137;
        [UIView animateWithDuration:0.25
                         animations:^{
                             [self.view layoutIfNeeded];
                         }];
        
    }else if([selectedTypeHouse.Key isEqualToString:@"2"]){
        [self.btnSuggestionAddress setHidden:YES];
        
        [self.txtBilltoNumber setEnabled:NO];
        [self.txtLot setEnabled:YES];
        [self.txtFloor setEnabled:YES];
        [self.txtRoom setEnabled:YES];
        [self.btnNameVilla setEnabled:YES];
        [self.btnPosition setEnabled:NO];
        
        [self.txtBilltoNumber setHidden:YES];
        [self.btnPosition setHidden:YES];
        [self.txtLot setHidden:NO];
        [self.txtFloor setHidden:NO];
        [self.txtRoom setHidden:NO];
        [self.btnNameVilla setHidden:NO];
        [self.lblLot setHidden:NO];
        [self.lblFloor setHidden:NO];
        [self.lblRoom setHidden:NO];
        [self.lblNameVilla setHidden:NO];
        [self.lblNumberHouse setHidden:YES];
        [self.lblPositionHouse setHidden:YES];
        
        self.constraintLot.constant = 56;
        self.constraintNote.constant = 179;
        [UIView animateWithDuration:0.25
                         animations:^{
                             [self.view layoutIfNeeded];
                         }];
        
    }else if([selectedTypeHouse.Key isEqualToString:@"3"]){
        [self.btnSuggestionAddress setHidden:NO];
        
        [self.txtBilltoNumber setEnabled:YES];
        [self.txtLot setEnabled:NO];
        [self.txtFloor setEnabled:NO];
        [self.txtRoom setEnabled:NO];
        [self.btnNameVilla setEnabled:NO];
        [self.btnPosition setEnabled:YES];
        
        [self.txtBilltoNumber setHidden:NO];
        [self.btnPosition setHidden:NO];
        [self.txtLot setHidden:YES];
        [self.txtFloor setHidden:YES];
        [self.txtRoom setHidden:YES];
        [self.btnNameVilla setHidden:YES];
        [self.lblLot setHidden:YES];
        [self.lblFloor setHidden:YES];
        [self.lblRoom setHidden:YES];
        [self.lblNameVilla setHidden:YES];
        [self.lblNumberHouse setHidden:NO];
        [self.lblPositionHouse setHidden:NO];
        
        self.constraintNumberHouse.constant = 137;
        self.constraintNote.constant = 199;
        [UIView animateWithDuration:0.25
                         animations:^{
                             [self.view layoutIfNeeded];
                         }];
    }
}

#pragma mark - set type button

- (NSArray *)dataForPopoverInSearchDisplay:(UISearchDisplayController *)searchDisplayController {
    return [self pareArrayForKeyWith:arrPromotion];
}

-(void)setType {
    ConstraintTopAmountTotal = self.constraintTopAmountTotal.constant;
    
    self.lblDacSacHDPrice.text = self.lblFimHotPrice.text = self.lblFimPlusPrice.text = self.lblKPlusPrice.text = self.lblVTVCabPrice.text = @"";
    
    self.lblIPTVPromotionID.text = self.lblIPTVPromotionIDBoxOrder.text = @"";
    
    self.txtNote.layer.borderColor= [UIColor colorWithRed:0 green:0.173 blue:0.294 alpha:1].CGColor;
    self.txtNote.layer.borderWidth = 1.f;
    self.txtNote.layer.cornerRadius = 6.0f;
    
    self.txtDescriptionIBB.layer.borderColor= [UIColor colorWithRed:0 green:0.173 blue:0.294 alpha:1].CGColor;
    self.txtDescriptionIBB.layer.borderWidth = 1.f;
    self.txtDescriptionIBB.layer.cornerRadius = 6.0f;
    
    self.textFieldPromotionInternet.hidden = YES;
    
    self.txtPromotionInternet.layer.borderColor= [UIColor colorWithRed:0 green:0.173 blue:0.294 alpha:1].CGColor;
    self.txtPromotionInternet.layer.borderWidth = 1.f;
    self.txtPromotionInternet.layer.cornerRadius = 6.0f;
    
    self.tvPromotionIPTV.layer.borderColor= [UIColor colorWithRed:0 green:0.173 blue:0.294 alpha:1].CGColor;
    self.tvPromotionIPTV.layer.borderWidth = 1.f;
    self.tvPromotionIPTV.layer.cornerRadius = 6.0f;
    
    self.tvPromotionIPTVBoxOrder.layer.borderColor= [UIColor colorWithRed:0 green:0.173 blue:0.294 alpha:1].CGColor;
    self.tvPromotionIPTVBoxOrder.layer.borderWidth = 1.f;
    self.tvPromotionIPTVBoxOrder.layer.cornerRadius = 6.0f;
    
    self.btnDeviceBoxCountMinus.layer.cornerRadius = 15.0f;
    self.btnDeviceBoxCountPlus.layer.cornerRadius = 15.0f;
    self.btnDevicePLCCountMinus.layer.cornerRadius = 15.0f;
    self.btnDevicePLCCountPlus.layer.cornerRadius = 15.0f;
    self.btnDeviceSTBCountMinus.layer.cornerRadius = 15.0f;
    self.btnDeviceSTBCountPlus.layer.cornerRadius = 15.0f;
    self.btnIPTVFimPlusChargeTimesMinus.layer.cornerRadius = 15.0f;
    self.btnIPTVFimPlusChargeTimesPlus.layer.cornerRadius = 15.0f;
    self.btnIPTVFimPlusPrepaidMonthMinus.layer.cornerRadius = 15.0f;
    self.btnIPTVFimPlusPrepaidMonthPlus.layer.cornerRadius = 15.0f;
    self.btnIPTVFimHotChargeTimesMinus.layer.cornerRadius = 15.0f;
    self.btnIPTVFimHotChargeTimesPlus.layer.cornerRadius = 15.0f;
    self.btnIPTVFimHotPrepaidMonthMinus.layer.cornerRadius = 15.0f;
    self.btnIPTVFimHotPrepaidMonthPlus.layer.cornerRadius = 15.0f;
    self.btnIPTVKPlusChargeTimesMinus.layer.cornerRadius = 15.0f;
    self.btnIPTVKPlusChargeTimesPlus.layer.cornerRadius = 15.0f;
    self.btnIPTVKPlusPrepaidMonthMinus.layer.cornerRadius = 15.0f;
    self.btnIPTVKPlusPrepaidMonthPlus.layer.cornerRadius = 15.0f;
    self.btnIPTVChargeTimesMinus.layer.cornerRadius = 15.0f;
    self.btnIPTVChargeTimesPlus.layer.cornerRadius = 15.0f;
    self.btnIPTVVTCChargeTimesMinus.layer.cornerRadius = 15.0f;
    self.btnIPTVVTCChargeTimesPlus.layer.cornerRadius = 15.0f;
    self.btnIPTVVTCPrepaidMonthMinus.layer.cornerRadius = 15.0f;
    self.btnIPTVVTCPrepaidMonthPlus.layer.cornerRadius = 15.0f;
    self.btnIPTVVTVChargeTimesMinus.layer.cornerRadius = 15.0f;
    self.btnIPTVVTVChargeTimesPlus.layer.cornerRadius = 15.0f;
    self.btnIPTVVTVPrepaidMonthMinus.layer.cornerRadius = 15.0f;
    self.btnIPTVVTVPrepaidMonthPlus.layer.cornerRadius = 15.0f;
}

-(void)CheckPromotionIPTV {
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    
    [self.txtIPTVPrepaid setEnabled:NO];
    if ([self.txtDeviceBoxCount.text integerValue] < 2) {
        [self.btnIPTVPromotionIDBoxOrder setEnabled:NO];
        
    } else {
        [self.btnIPTVPromotionIDBoxOrder setEnabled:YES];
        
    }
    
    if([selectedPromotionIPTV.Type isEqualToString:@"1"]){
        self.lblIPTVPromotionID.text = StringFormat(@"(%@ VNĐ)",[nf stringFromNumber:[NSNumber numberWithFloat:[selectedPromotionIPTV.Amount intValue]]]);
    }
    if([selectedPromotionIPTV.Type isEqualToString:@"0"]){
        self.lblIPTVPromotionID.text = @"0";
    }
    
    if([selectedPromotionIPTVBoxOrder.Type isEqualToString:@"1"]){
        self.lblIPTVPromotionIDBoxOrder.text = StringFormat(@"(%@ VNĐ)",[nf stringFromNumber:[NSNumber numberWithFloat:[selectedPromotionIPTVBoxOrder.Amount intValue]]]);
    }
    if([selectedPromotionIPTVBoxOrder.Type isEqualToString:@"0"]){
        self.lblIPTVPromotionIDBoxOrder.text = @"0";
    }
    
    if([selectedPromotionIPTV.Type isEqualToString:@"1"] && [selectedPromotionIPTV.Amount isEqualToString:@"0"]){
        self.txtIPTVPrepaid.text = @"0";
        [self.txtIPTVPrepaid setEnabled:YES];
        if([selectedPromotionIPTV.Amount isEqualToString:@"0"]){
            if(![self.rc.IPTVPrepaid isEqualToString:@"0"]){
                self.txtIPTVPrepaid.text = self.rc.IPTVPrepaid;
            }
        }else {
            self.txtIPTVPrepaid.text = selectedPromotionIPTV.Amount;
        }
    }
    // Lấy giá thiết bị IPTV và giá đăng ký các gói Extra IPTV
    [self loadIPTVPrice];
}

-(void)CheckRegisterIPTV {
    if([selectedLocalType.Key isEqualToString:@"0"] || [selectedLocalType.Key isEqualToString:@"3"]){
        
        self.txtIPTVChargeTimes.text = @"0";
        self.txtDeviceBoxCount.text = @"0";
        self.txtDevicePLCCount.text = @"0";
        self.txtDeviceSTBCount.text = @"0";
        self.txtIPTVFimHotChargeTimes.text = @"0";
        self.txtIPTVFimHotPrepaidMonth.text = @"0";
        self.txtIPTVFimPlusChargeTimes.text = @"0";
        self.txtIPTVFimPlusPrepaidMonth.text = @"0";
        self.txtIPTVKPlusChargeTimes.text = @"0";
        self.txtIPTVKPlusPrepaidMonth.text = @"0";
        self.txtIPTVVTCChargeTimes.text = @"0";
        self.txtIPTVVTCPrepaidMonth.text = @"0";
        self.txtIPTVVTVChargeTimes.text = @"0";
        self.txtIPTVVTVPrepaidMonth.text = @"0";
        [self.rdYesRequest setEnabled:NO];
        [self.rdNoRequest setEnabled:NO];
        [self.rdYesWallDrilling setEnabled:NO];
        [self.rdNoWallDrilling setEnabled:NO];
        [self.btnDeployment setEnabled:NO];
        [self.btnDeviceBoxCountMinus setEnabled:NO];
        [self.btnDeviceBoxCountPlus setEnabled:NO];
        [self.btnDevicePLCCountMinus setEnabled:NO];
        [self.btnDevicePLCCountPlus setEnabled:NO];
        [self.btnDeviceSTBCountMinus setEnabled:NO];
        [self.btnDeviceSTBCountPlus setEnabled:NO];
        [self.btnIPTVPackage setEnabled:NO];
        [self.btnIPTVPromotionID setEnabled:NO];
        [self.btnIPTVPromotionIDBoxOrder setEnabled:NO];
        [self.btnIPTVPromotionIDBoxOrder setEnabled:NO];
        [self.btnIPTVChargeTimesMinus setEnabled:NO];
        [self.btnIPTVChargeTimesPlus setEnabled:NO];
        
        [self.cbFimPlus setEnabled:NO];
        [self.cbFimHot setEnabled:NO];
        [self.cbKPlus setEnabled:NO];
        [self.cbVTC setEnabled:NO];
        [self.cbVTV setEnabled:NO];
        
    }else {
        if(self.rc.ID != nil){
            self.txtIPTVFimPlusChargeTimes.text = self.rc.IPTVFimPlusChargeTimes;
            self.txtIPTVFimPlusPrepaidMonth.text =self.rc.IPTVFimPlusPrepaidMonth;
            self.txtIPTVFimHotChargeTimes.text = self.rc.IPTVFimHotChargeTimes;
            self.txtIPTVFimHotPrepaidMonth.text =self.rc.IPTVFimHotPrepaidMonth;
            self.txtIPTVKPlusChargeTimes.text = self.rc.IPTVKPlusChargeTimes;
            self.txtIPTVKPlusPrepaidMonth.text =self.rc.IPTVKPlusPrepaidMonth;
            self.txtIPTVPrepaid.text = self.rc.IPTVPrepaid;
            self.txtIPTVVTCChargeTimes.text = self.rc.IPTVVTCChargeTimes;
            self.txtIPTVVTCPrepaidMonth.text = self.rc.IPTVVTCPrepaidMonth;
            self.txtIPTVVTVChargeTimes.text = self.rc.IPTVVTVChargeTimes;
            self.txtIPTVVTVPrepaidMonth.text = self.rc.IPTVVTVPrepaidMonth;
            if([self.rc.IPTVBoxCount isEqualToString:@"0"]){
                self.txtDeviceBoxCount.text = @"1";
            }else {
                self.txtDeviceBoxCount.text = self.rc.IPTVBoxCount;
            }
            
            self.txtDevicePLCCount.text = self.rc.IPTVPLCCount;
            self.txtDeviceSTBCount.text = self.rc.IPTVReturnSTBCount;
            if([self.rc.IPTVChargeTimes isEqualToString:@"0"]){
                self.txtIPTVChargeTimes.text = @"1";
            }else {
                self.txtIPTVChargeTimes.text = self.rc.IPTVChargeTimes;
            }
            
        }else {
            self.txtIPTVChargeTimes.text = @"1";
            self.txtDeviceBoxCount.text = @"1";
        }
        self.txtIPTVPrepaid.text = @"0";
        [self.rdYesRequest setEnabled:YES];
        [self.rdNoRequest setEnabled:YES];
        [self.rdYesWallDrilling setEnabled:YES];
        [self.rdNoWallDrilling setEnabled:YES];
        [self.btnDeployment setEnabled:YES];
        [self.btnDeviceBoxCountMinus setEnabled:YES];
        [self.btnDeviceBoxCountPlus setEnabled:YES];
        [self.btnDevicePLCCountMinus setEnabled:YES];
        [self.btnDevicePLCCountPlus setEnabled:YES];
        [self.btnDeviceSTBCountMinus setEnabled:YES];
        [self.btnDeviceSTBCountPlus setEnabled:YES];
        [self.btnIPTVPackage setEnabled:YES];
        
        if([selectedLocalType.Key isEqualToString:@"1"] || [selectedLocalType.Key isEqualToString:@"3"]){
            [self.btnPromotion setEnabled:NO];
            
        }else {
            [self.btnPromotion setEnabled:YES];
        }
        
        [self.btnIPTVPromotionID setEnabled:YES];
        [self.btnIPTVPromotionIDBoxOrder setEnabled:YES];
        [self.btnIPTVChargeTimesMinus setEnabled:YES];
        [self.btnIPTVChargeTimesPlus setEnabled:YES];
        [self.cbFimPlus setEnabled:YES];
        [self.cbFimHot setEnabled:YES];
        [self.cbKPlus setEnabled:YES];
        [self.cbVTC setEnabled:YES];
        [self.cbVTV setEnabled:YES];
        
    }
}

-(void)CheckPromotionInternet {
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    if([selectedLocalType.Key isEqualToString:@"1"] || [selectedLocalType.Key isEqualToString:@"3"]){
        self.btnPromotion.enabled = NO;
        self.txtInternetTotal.text = @"";
        self.txtTotal.text = @"0";
        
    }else {
        self.btnPromotion.enabled = YES;
        amountInternet = [selectedPromotion.Amount intValue];
        self.txtInternetTotal.text = [nf stringFromNumber:[NSNumber numberWithFloat:amountInternet]];
        self.txtIPTVPrepaid.text = @"0";
        
        [self Total];
    }
    if(![selectedLocalType.Key isEqualToString:@"0"]){
        self.txtIPTVPrepaid.text =selectedPromotionIPTV.Amount;
    }
    
}

-(void)alertView:(UIAlertView *)alert_view didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if(flagAlert == TRUE){
        
        ListRegistrationFormPageViewController *vc = [[ListRegistrationFormPageViewController alloc] init];
        UINavigationController *nav = [((UINavigationController *)self.menuContainerViewController.centerViewController) initWithRootViewController:vc ];
        
        self.menuContainerViewController.centerViewController = nav;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    }
}

#pragma mark - Total amount

-(void)Total {
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    if([selectedLocalType.Key isEqualToString:@"0"]){
        amountIPTV = 0;
        self.txtIPTVTotal.text = @"0";
    }
    if([selectedLocalType.Key isEqualToString:@"1"]){
        amountInternet = 0;
        self.txtInternetTotal.text = @"0";
    }
    
    //vutt11
    amountTotal = amountInternet + amountIPTV + [selectedDeposit.Key intValue] + [selectedDepositBlackPoint.Key intValue] +office365Total + amountOTT;
    self.txtTotal.text =[nf stringFromNumber:[NSNumber numberWithFloat:amountTotal]];
}

#pragma mark - delegate Textfield

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    flagtotaliptv = FALSE;
}

#pragma mark MPGTextField Delegate Methods

- (NSArray *)dataForPopoverInTextField:(MPGTextField *)textField {
    return [self pareArrayForKeyWith:arrPromotion];
}

- (BOOL)textFieldShouldSelect:(MPGTextField *)textField {
    return YES;
}

- (void)textField:(MPGTextField *)textField didEndEditingWithSelection:(NSDictionary *)result {
    
    if ([[result objectForKey:@"PromotionModel"] isKindOfClass:[NSString class]] && [[result objectForKey:@"PromotionModel"] isEqualToString:@"NEW"]) {
        
        selectedPromotion = [[PromotionModel alloc] initWithName:@"0" description:@"" Amount:@"0" Type:@""];
        
    } else {
        
        selectedPromotion = [result objectForKey:@"PromotionModel"];
        
    }
    
    self.txtPromotionInternet.text = selectedPromotion.PromotionName;
    
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    amountInternet = [selectedPromotion.Amount intValue];
    self.txtInternetTotal.text = [nf stringFromNumber:[NSNumber numberWithFloat:amountInternet]];
    [self Total];
}

- (NSMutableArray *)pareArrayForKeyWith:(NSArray*)arrInput {
    NSMutableArray *data = [[NSMutableArray alloc] init];
    
    int k = 0;
    PromotionModel *temp;
    for (k = 0; k < arrInput.count; k++) {
        temp = [arrInput objectAtIndex:k];
        [data addObject:[NSDictionary dictionaryWithObjectsAndKeys:temp.PromotionName,@"DisplayText",temp,@"PromotionModel", nil]];
    }
    return data;
}

#pragma mark - validate email

- (BOOL) validateEmail:(NSString *)candidate {
    NSString *string= candidate;
    NSError *error = NULL;
    NSRegularExpression *regex = nil;
    regex = [NSRegularExpression regularExpressionWithPattern:@"\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,6}"
                                                      options:NSRegularExpressionCaseInsensitive
                                                        error:&error];
    NSUInteger numberOfMatches = 0;
    numberOfMatches = [regex numberOfMatchesInString:string
                                             options:0
                                               range:NSMakeRange(0, [string length])];
    // NSLog(@"numberOfMatches is: %lu", numberOfMatches);
    if(numberOfMatches != 0){
        return TRUE;
    }
    return FALSE;
}

- (void) resetButtonSelected {
    self.btnStreet.selected = NO;
    self.btnWard.selected = NO;
    self.btnDistrict.selected = NO;
    self.btnNameVilla.selected = NO;
}

#pragma mark - UIBarButtonItem save
- (void)rightBarButton_Clicked:(id)sender {
    [self CheckRegistration];
}

- (void)showActionSheetUpdate {
    NSLog(@"----- Đang Lưu thông tin Phiếu đăng ký...");
    UIActionSheet *noticeSave = [[UIActionSheet alloc] initWithTitle:@"Bạn chắc chắn muốn cập nhật thông tin Phiếu đăng ký?" delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
    noticeSave.tag = save;
    [noticeSave showInView:[UIApplication sharedApplication].keyWindow];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        switch (actionSheet.tag) {
            case back:
                [self.navigationController popViewControllerAnimated:YES];
                break;
            case save: {
                // [self CheckRegistration];
                [self showMBProcess];
                if (![selectedTypeHouse.Key isEqualToString:@"2"]) {
                    [self checkAddressNumber];
                } else {
                    //    [self checkRegistrationAPI];
                    [self checkRegistration2];
                }
            }
                break;
            case datePicker:
                [self setSelectedDateInField];
                break;
            default:
                break;
        }
    }
    
}

// hide or show IPTV Monthly total with local type service
- (void)setHideIPTVMonthlyTotal:(BOOL)status {
    [self.txtIPTVMonthlyTotal setHidden:status];
    [self.lblIPTVMonthlyTotal setHidden:status];
    
    self.constraintTopAmountTotal.constant = ConstraintTopAmountTotal; // default value
    
    if (status == YES) {
        self.constraintTopAmountTotal.constant += 30; // new value
        
    }
    
}

//Chon so luong Box trong dich vu OTT
- (void)changedValueCount:(UIButton *)sender {
    
    self.count = [self.txtOTTBoxCount.text integerValue];
    
    if (sender == self.btnMinBoxOTT) {
        self.count --;
        if (self.count < self.minimum) {
            self.count = self.minimum;
        }
    }
    if (sender == self.btnMaxBoxOTT) {
        self.count ++;
        if (self.count > self.maximum) {
            self.count = self.maximum;
        }
    }
    
    [self changeValueCountTextField:StringFormat(@"%li",(long)self.count)];
    
}

- (void)changeValueCountTextField:(NSString *)text {
    [self.txtOTTBoxCount setText:text];
    [self.txtOTTBoxCount layoutIfNeeded];
}

- (IBAction)btnUpdateImage_clicked:(id)sender {
    self.imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    [self presentViewController:self.imagePickerController animated:YES completion:nil];
    
}

#pragma mark - Custom Accessors
- (UIImagePickerController *) imagePickerController {
    if (!_imagePickerController) {
        _imagePickerController = [[UIImagePickerController alloc] init];
        _imagePickerController.allowsEditing = NO;
        _imagePickerController.delegate = self;
        
    }
    return _imagePickerController;
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo: (NSDictionary *)info {
    imageData = @"";
    UIImage *originalImage, *editedImage, *imageToUse;
    
    // Handle a still image picked from a photo album
    editedImage = (UIImage *)[info objectForKey:UIImagePickerControllerEditedImage];
    
    originalImage = (UIImage *)[info objectForKey:UIImagePickerControllerOriginalImage];
    
    if (editedImage) {
        imageToUse = editedImage;
    } else {
        imageToUse = originalImage;
    }
    //560
    CGFloat scale = imageToUse.size.width / 300;
    imageToUse = [UIImage scaleImage:imageToUse toSize:CGSizeMake(imageToUse.size.width/scale, imageToUse.size.height/scale)];
    
      NSLog(@"dataSize 1: %@",[NSByteCountFormatter stringFromByteCount:imageData.length countStyle:NSByteCountFormatterCountStyleFile]);
    
    
    self.img.image = imageToUse;
    
    [self upDateImage];

    [[picker presentingViewController] dismissViewControllerAnimated:YES completion:nil];
    
    
}

- (void)upDateImage{
    
    NSData *imageToUseData = UIImageJPEGRepresentation(self.img.image, 0.1);
    imageData = [self base64forData:imageToUseData];

    [self showMBProcess];
    
    ShareData *shared  = [ShareData instance];
    [shared.registrationFormProxy upDateImage:shared.currentUser.userName Image:imageData RegCode:@"" Type:1 Completehander:^(id result,NSString *errorCode,NSString *message) {
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if (message.length <10) {
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return;
        }
        self.imageInfo = message;
        [self hideMBProcess];
        
    }errorHandler:^(NSError *error){
        
        
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
        return;
        
        
    }];
    
    
}

-(NSString*)base64forData:(NSData*)theData {
    const uint8_t* input = (const uint8_t*)[theData bytes];
    NSInteger length = [theData length];
    
    static char table[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    
    NSMutableData* data = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    uint8_t* output = (uint8_t*)data.mutableBytes;
    
    NSInteger k;
    for ( k=0; k < length; k += 3) {
        NSInteger value = 0;
        NSInteger j;
        for (j = k; j < (k + 3); j++) {
            value <<= 8;
            
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        NSInteger theIndex = (k / 3) * 4;
        output[theIndex + 0] =                    table[(value >> 18) & 0x3F];
        output[theIndex + 1] =                    table[(value >> 12) & 0x3F];
        output[theIndex + 2] = (k + 1) < length ? table[(value >> 6)  & 0x3F] : '=';
        output[theIndex + 3] = (k + 2) < length ? table[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding] ;
}


@end
