//
//  SuggestionsSearchViewController.h
//  MobiSale
//
//  Created by ISC-DanTT on 3/11/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    suggestionsSearchPromotionStatement = 1,
    SuggestionsFillHouseNumber
    
} typeSuggestion;

@protocol SuggestionsDelegate <NSObject>

-(void)CancelPopupSuggestions;

@end

@interface SuggestionsSearchViewController : UIViewController

@property (assign) typeSuggestion type;

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@property (strong, nonatomic) IBOutlet UIWebView *webViewContent;

@property (retain, nonatomic) id<SuggestionsDelegate> delegate;

- (IBAction)btnBack_Clicked:(id)sender;

@end
