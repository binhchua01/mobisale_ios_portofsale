//
//  DynamicMenuCell.m
//  MobiSale
//
//  Created by Nguyen Sang on 7/11/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import "DynamicMenuCell.h"

typedef void (^completionCell)(SNDynamicMenuView dynamic);

@implementation DynamicMenuCell {
    
    SNDynamicMenuView dynamicView;
}

@synthesize delegate;

//action
- (IBAction)pressedAction:(id)sender {
    
    //kiểm tra có delegate không ?
    if (!self.delegate) {
        return;
    }
    
    switch (dynamicView) {
        case SNDynamicMenuViewBookPort: {
            [self.delegate pressedButtonDynamicInMenu:SNDynamicMenuViewBookPort];
            break;
        }
        case SNDynamicMenuViewSurvey: {
            [self.delegate pressedButtonDynamicInMenu:SNDynamicMenuViewSurvey];
            break;
        }
        case SNDynamicMenuViewAppointment: {
            [self.delegate pressedButtonDynamicInMenu:SNDynamicMenuViewAppointment];
            break;
        }
        case SNDynamicMenuViewUpdate: {
            [self.delegate pressedButtonDynamicInMenu:SNDynamicMenuViewUpdate];
            break;
        }
        case SNDynamicMenuViewEarningMoney: {
            [self.delegate pressedButtonDynamicInMenu:SNDynamicMenuViewEarningMoney];
            break;
        }
        case SNDynamicMenuViewUpdateAmount: {
            [self.delegate pressedButtonDynamicInMenu:SNDynamicMenuViewUpdateAmount];
            break;
        }
        case SNDynamicMenuViewConstructVote: {
            [self.delegate pressedButtonDynamicInMenu:SNDynamicMenuViewConstructVote];
            break;
        }
        default: {
            [self.delegate pressedButtonDynamicInMenu:SNDynamicMenuViewCancel];
            break;
        }
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    
    
}

- (void)configCell:(NSString *)nameButton enumDynamic:(SNDynamicMenuView)enumDynamic {
    
    [self.btnChildMenu setTitle:nameButton forState:UIControlStateNormal];
    
    dynamicView = enumDynamic;
    
}

@end
