//
//  ListRegisteredFormController.m
//  MobiSales
//
//  Created by Nguyen Tan Tho on 1/23/15.
//  Copyright (c) 2015 Aryan Ghassemi. All rights reserved.
//

#import "ListRegisteredFormController.h"
#import "ListRegisteredFormCell.h"
#import "DetailRegisteredForm.h"
#import "ShareData.h"
#import "RegistrationFormRecord.h"
#import "UIPopoverListView.h"
#import "KeyValueModel.h"
#import "RegistrationFormDetailRecord.h"
#import "Common_client.h"
#import "DemoTableFooterView.h"
#import "LoginViewController.h"
#import "RegisteredFormController.h"

#import "ListRegisteredFormViewController.h"

@interface ListRegisteredFormController (){
    NSMutableArray *arrData;
    NSInteger totalPage;
    NSInteger currentPage;
    DemoTableFooterView *footerView;
}

@property NSMutableArray *arrList;

@end

@implementation ListRegisteredFormController{
    KeyValueModel *selectedPage;
}

@dynamic tableView;
@synthesize arrPage;

- (void)viewDidLoad {
    //vutt11
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.screenName = self.title;
    arrData = [[NSMutableArray alloc]init];
    // set the custom view for "load more". See DemoTableFooterView.xib.
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DemoTableFooterView" owner:self options:nil];
    footerView = (DemoTableFooterView *)[nib objectAtIndex:0];
    
    // add refresh control when scroll top of tableview
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    
    // add right bar button
    if ([self.title isEqualToString:@"DANH SÁCH PĐK"]) {
    UIImage *image = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"ic_create_potentail_obj"] height:30];
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc]
                                       initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered
                                       target:self
                                       action:@selector(addNewRegisteredForm:)];
    self.navigationItem.rightBarButtonItem = rightBarButton;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}
- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    //load data
    [arrData removeAllObjects];
    footerView.infoLabel.hidden = YES;
    [self LoadData:@"1"];
}

#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if(arrData.count > 0 )
        return arrData.count;
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.title isEqualToString:@"DANH SÁCH PĐK"]) {
        if ([self.viewType.Key integerValue] == 0) {
            return  280;
        }
        if ([self.viewType.Key integerValue] == 1) {
            return  300;
        }
        if ([self.viewType.Key integerValue] == 2) {
            return  295;
        }
    }
    if ([self.title isEqualToString:@"DANH SÁCH PĐK/HĐ CHƯA DUYỆT"]) {
        return 270;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor  colorWithRed:0.133 green:0.698 blue:0.569 alpha:1];
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"ListRegisteredFormCell";
    ListRegisteredFormCell *cell = (ListRegisteredFormCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ListRegisteredFormCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    if(arrData.count > 0){
        RegistrationFormRecord *rc = [arrData objectAtIndex:indexPath.section];
        cell.lblContract.text = StringFormat(@"Số hợp đồng: %@",rc.Contract);
        cell.lblSTT.text = [NSString stringWithFormat:@"%d", [rc.RowNumber intValue]];
        cell.lblId.text = rc.RegCode;
        cell.lblFullName.text = rc.FullName;
        cell.lblAdress.text = rc.Address;
        cell.lblTypeSerivce.text = rc.LocalType;
        if ([self.title isEqualToString:@"DANH SÁCH PĐK"]) {
            if ([self.viewType.Key integerValue] == 0 || [self.viewType.Key integerValue] == 2 ) {
                if ([self.viewType.Key integerValue] == 0) {
                    cell.lblIDConstraintTopSpace.constant  = 0;
                    cell.lblContract.hidden      = YES;

                } else if ([self.viewType.Key integerValue] == 2) {
                    cell.lblIDConstraintTopSpace.constant  = 29;
                    cell.lblContract.hidden      = NO;
                }
                
                cell.lblSTTConstraintTopSpace.constant = 135;

                cell.lblIsBillling.hidden    = NO;
                cell.lblStatusDeposit.hidden = NO;
                
                // vutt11
                
//                if([rc.isBookport isEqualToString:@"0"]){
//                    cell.lblStatusPort.text = @"Chưa book port";
//                }else {
//                    cell.lblStatusPort.text = @"Đã book port";
//                }
//
                if ([rc.strStatusBookPort isEqualToString:@"(null)"]) {
                    cell.lblStatusPort.text = @"";
                }
                else {
                    cell.lblStatusPort.text = rc.strStatusBookPort;
                }
                
                if([rc.isBilling isEqualToString:@"0"]){
                    cell.lblIsBillling.text = @"Chưa xuất phiếu thu";
                }else {
                    cell.lblIsBillling.text = @"Đã xuất phiếu thu";
                }
                
                
                if([rc.isInVest isEqualToString:@"0"]){
                    cell.lblisInVest.text = @"Chưa khảo sát";
                }else {
                    cell.lblisInVest.text = @"Đã khảo sát";
                }
                
                if([rc.StatusDeposit isEqualToString:@"0"]){
                    cell.lblStatusDeposit.text = @"Chưa thu tiền";
                }else {
                    cell.lblStatusDeposit.text = @"Đã thu tiền";
                }
                [cell.lblStatusDeposit setFont:[UIFont boldSystemFontOfSize:15]];

                return cell;
            }
        }
        if ([self.title isEqualToString:@"DANH SÁCH PĐK/HĐ CHƯA DUYỆT"] || [self.viewType.Key integerValue] == 1 ) {
            cell.lblIDConstraintTopSpace.constant = 29;
            cell.lblSTTConstraintTopSpace.constant = 106;
            
            cell.lblContract.hidden      = NO;
            cell.lblIsBillling.hidden    = YES;
            cell.lblStatusDeposit.hidden = YES;
          
            if([rc.isBilling isEqualToString:@"0"]){
                cell.lblStatusPort.text = @"Chưa xuất phiếu thu";
            }else {
                cell.lblStatusPort.text = @"Đã xuất phiếu thu";
            }
            
            if ([rc.strPaidStatusName  isEqual: @"<null>"] || [rc.strPaidStatusName  isEqual: @""] || rc.strPaidStatusName == [NSNull null] || [rc.strPaidStatusName  isEqual: @"(null)"]) {
                
                cell.lblisInVest.attributedText = [self stringWithBoldSuffy:@"" suffix:rc.AutoContractStatusDesc];
                
            } else {
                NSString *format = [NSString stringWithFormat:@"%@\n\n", rc.strPaidStatusName];
//                cell.lblisInVest.text = format;
                cell.lblisInVest.attributedText = [self stringWithBoldSuffy:format suffix:rc.AutoContractStatusDesc];
            }
            
            
//            [cell.lblisInVest setFont:[UIFont boldSystemFontOfSize:15]];
            
            return cell;
        }
    }
    
    return cell;
}

#pragma mark BoldSuffy
- (NSMutableAttributedString *) stringWithBoldSuffy:(NSString *)prefix suffix:(NSString *)suffix {
    
    NSDictionary *dicAttri = @{
                              NSFontAttributeName : [UIFont boldSystemFontOfSize:15]
                              };
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:suffix attributes:dicAttri];
    
    NSMutableAttributedString *strBold = [[NSMutableAttributedString alloc] initWithString:prefix];
    
    [strBold appendAttributedString:attributedString];
    
    return strBold;
    
}

#pragma mark Table DidSelected
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *nibName = @"DetailRegisteredForm";
    RegistrationFormDetailRecord *list = [arrData objectAtIndex:indexPath.section];
    
    DetailRegisteredForm *vc = [[DetailRegisteredForm alloc]initWithNibName:nibName bundle:nil];
    vc.Id = list.ID;
    [self.navigationController pushViewController:vc animated:YES];
    
    
}

#pragma mark - DropDown view

- (void)setupDropDownView {
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.listView.scrollEnabled = TRUE;
    [poplistview setTitle:NSLocalizedString(@"Chọn trang", @"")];
    [poplistview show];
}

- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.0f;
}

#pragma mark - UIPopoverListViewDataSource

- (void) setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell{
    KeyValueModel *model;
    model = [self.arrPage objectAtIndex:row];
    cell.textLabel.text = model.Values;
    
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView cellForIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row cell:cell];
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = self.arrPage.count;
    return count;
}

#pragma mark - UIPopoverListViewDelegate

- (void)didDropDownSelected:(NSInteger)row {
    selectedPage = [self.arrPage objectAtIndex:row];
    [self LoadData:StringFormat(@"%li",(long)row +1)];
}


#pragma mark - UIPopoverListViewDelegate

- (void)popoverListView:(UIPopoverListView *)popoverListView didSelectIndexPath:(NSIndexPath *)indexPath {
    [self didDropDownSelected:indexPath.row];
    
}

#pragma mark - Scroll view delegate
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    if (maximumOffset - currentOffset < 0) {
        [self loadMore];
    }
}

#pragma mark - LoadData
-(void)LoadData:(NSString *)pagenum {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    [self showMBProcess];
    if ([self.title isEqualToString:@"DANH SÁCH PĐK"]) {
//        [self loadDataNorm:pagenum];
        [self loadListRegistrationFormWithPageNumber:pagenum agent:self.viewType.Key agenName:@""];
    } else if ([self.title isEqualToString:@"DANH SÁCH PĐK/HĐ CHƯA DUYỆT"]) {
        [self loadListRegistrationFormWithPageNumber:pagenum agent:@"1" agenName:@""];
    }
}

- (void)loadDataNorm:(NSString*)pageNumber {
    ShareData *shared = [ShareData instance];
    [shared.registrationFormProxy GetListRegistration:pageNumber Completehander:^(id result, NSString *errorCode, NSString *message) {
        self.arrList = result;
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(self.arrList <= 0){
            [self ShowAlertBoxEmpty];
            [self hideMBProcess];
            [self.tableView setHidden:YES];
            return;
        }
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        if([errorCode isEqualToString:@"<null>"]){
            [arrData addObjectsFromArray:self.arrList];
            RegistrationFormRecord *rc = [self.arrList objectAtIndex:0];
            totalPage   = [rc.TotalPage integerValue];
            currentPage = [rc.CurrentPage integerValue];
            [self.tableView reloadData];
            [self hideMBProcess];
            [self loadMoreCompleted];
        }
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
        return;
    }];
}

- (void)loadListRegistrationFormWithPageNumber:(NSString*)pageNumber agent:(NSString*)agent agenName:(NSString*)agentName {
    ShareData *shared = [ShareData instance];
    [shared.registrationFormProxy GetListRegistrationPost:shared.currentUser.userName pageNumber:pageNumber agent:agent agentName:agentName Completehander:^(id result, NSString *errorCode, NSString *message) {
        self.arrList = result;
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(self.arrList <= 0){
            [self ShowAlertBoxEmpty];
            [self hideMBProcess];
            [self.tableView setHidden:YES];
            return;
        }
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        if([errorCode isEqualToString:@"<null>"]){
            [arrData addObjectsFromArray:self.arrList];
            RegistrationFormRecord *rc = [self.arrList objectAtIndex:0];
            totalPage   = [rc.TotalPage integerValue];
            currentPage = [rc.CurrentPage integerValue];
            [self.tableView reloadData];
            [self hideMBProcess];
            [self loadMoreCompleted];
        }
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
        return;
    }];
}

#pragma refresh table view
- (void)refresh:(UIRefreshControl *)refreshControl {
    [arrData removeAllObjects];
    [self LoadData:@"1"];
    [refreshControl endRefreshing];
}

#pragma mark - Load More
//
// The method -loadMore was called and will begin fetching data for the next page (more).
// Do custom handling of -footerView if you need to.
//
- (void) willBeginLoadingMore {
    [footerView.activityIndicator startAnimating];
}

// Do UI handling after the "load more" process was completed. In this example, -footerView will
// show a "No more items to load" text.
//
- (void)loadMoreCompleted {
    [footerView.activityIndicator stopAnimating];
}

- (void)loadMore {
    // Do your async loading here
    self.tableView.tableFooterView = footerView;
    currentPage ++;
    if (currentPage <= totalPage) {
        [self willBeginLoadingMore];
        [self LoadData:StringFormat(@"%li",(long)currentPage)];
        return;
    }
    footerView.infoLabel.hidden = NO;
}

#pragma mark - Load Page
-(void)LoadPage:(NSInteger)page {
    totalPage = page;
    self.arrPage = [NSMutableArray array];
    int i = 1;
    for (i = 1; i <= page; i++) {
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",i] description:[NSString stringWithFormat:@"%d",i]];
        [self.arrPage addObject: s];
    }
    selectedPage = [self.arrPage objectAtIndex:0];
}

#pragma mark - event button click
-(IBAction)btnPage_clicked:(id)sender{
    [self setupDropDownView];
}

- (void)addNewRegisteredForm:(id)sender {
    
    ListRegisteredFormViewController *vc = [[ListRegisteredFormViewController alloc] initWithNibName:@"ListRegisteredFormViewController" bundle:nil];
    
//   RegisteredFormController *vc = [[RegisteredFormController alloc]initWithNibName:@"RegisteredFormController" bundle:nil];
    
    [self.navigationController pushViewController:vc animated:YES];

}

@end
