//
//  DynamicMenuCell.h
//  MobiSale


#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SNDynamicMenuView) {

    SNDynamicMenuViewBookPort,          //Book Port
    SNDynamicMenuViewSurvey,            //Khảo sát
    SNDynamicMenuViewAppointment,       //Hẹn triển khai
    SNDynamicMenuViewUpdate,            //Cập nhật
    SNDynamicMenuViewEarningMoney,      //thu tiền
    SNDynamicMenuViewUpdateAmount,      //Cập nhật tổng tiền
    SNDynamicMenuViewConstructVote,     //Phiếu thi công
    SNDynamicMenuViewCancel             //Huỷ
};

@protocol DynamicMenuCellDelegate <NSObject>

-(void)pressedButtonDynamicInMenu:(SNDynamicMenuView)dynamic;

@end

@interface DynamicMenuCell : UITableViewCell

@property (retain ,nonatomic) id<DynamicMenuCellDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *btnChildMenu;

- (void)configCell:(NSString *)nameButton enumDynamic:(SNDynamicMenuView)enumDynamic;

@end
