//
//  PayViewController.m
//  MobiSale
//
//  Created by HIEUPC on 4/16/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "PayViewController.h"
#import "../../../Models/KeyValueModel.h"
#import "UIPopoverListView.h"
#import "SBIRecord.h"
#import "ShareData.h"
#import "Common_client.h"
#import "NSUserDefaults+Notification.h"
#import "SignatureViewController.h"
#import "AppDelegate.h"

typedef enum {
    SMSPayment  = 1,
    mPOSPayment = 2,
    hiFPT       = 3
    
}actionSheetTag;

@interface PayViewController () <UIActionSheetDelegate, UIAlertViewDelegate, SignatureViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageSignature;

@end

@implementation PayViewController{
    SBIRecord *selectedSBIInternet, *selectedSBIIPTV;
    int typeSBI;
    
    NSArray *arrPaymentType;
    NSDictionary *selectedPaymentType;
    
    // Log for mPOS
    int tontien;
    NSString *ObjID,*ReferenceID,*Amount,*SEND_MPOS_CUSNAME,*paymentDescription ,*Step,*Status;
    NSInteger result_ErrCode, step;
    RegistrationFormDetailRecord *record;
   

}
@synthesize record;
enum tableSourceStype {
    Internet = 1,
    IPTV = 2,
    PaymentType = 3
};

- (void)viewDidLoad {
    [super viewDidLoad];
    //reset Data
    [self resetData];
    self.btnUpdate.layer.cornerRadius = 5;
    
    step = 0;
    typeSBI = 0;
    self.lblContract.text = self.record.RegCode;
    self.lblFullName.text = self.record.FullName;
    self.lblPhone.text = self.record.Phone_1;
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    self.lblTotal.text = StringFormat(@"%@ VNĐ", [nf stringFromNumber:[NSNumber numberWithFloat:[self.record.Total intValue]]]);
    [self LoadData];
    // Nếu là PĐK mới
    if ([self.record.regType isEqual:@"0"]) {
        if([self.record.PromotionID isEqualToString:@"0"] || [self.record.PromotionID isEqualToString:@""]){
            self.btnSBIInernet.enabled = NO;
//            typeSBI = 1;
        }
        
    // Nếu là PĐK bán thêm
    } else {
        self.btnSBIInernet.enabled = YES;
//        typeSBI = 1;
    }

//    if([self.record.IPTVPackage isEqualToString:@""] ){
//        self.btnSBIIPTV.enabled = NO;
//        typeSBI = 2;
//    }
//    
    /*
     // Setting to receive notification
     [[NSNotificationCenter defaultCenter] addObserver:self
     selector:@selector(didReceiveSuccessNotification:)
     name:kMAMPosAPICallBackSuccessURLNotification
     object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self
     selector:@selector(didReceiveErrorNotification:)
     name:kMAMPosAPICallBackErrorURLNotification
     object:nil];
     [[NSNotificationCenter defaultCenter] addObserver:self
     selector:@selector(didReceiveCancelNotification:)
     name:kMAMPosAPICallBackCancelURLNotification
     object:nil];
     */
    // setup payment helper
    [MAMPosAPIHelper sharedAPIHelper].delegate = self;
    
    ShareData *shared = [ShareData instance];
    
    // setup payment type list
    if ([shared.currentUser.UseMPOS integerValue] > 0) {
        arrPaymentType = [NSArray arrayWithObjects:[NSDictionary dictionaryWithObject:@"mPOS" forKey:@"2"],nil];
        [self.btnPaymentType setEnabled:NO];
    } else {
        if ([self.record.regType isEqual:@"0"]) {//MOi
            arrPaymentType = [NSArray arrayWithObjects:[NSDictionary dictionaryWithObject:@"Thanh toán tiền mặt + gửi SMS" forKey:@"1"],[NSDictionary dictionaryWithObject:@"Thanh toán online HiFPT/Members" forKey:@"3"],nil];
            [self.btnPaymentType setEnabled:YES];
        } else {
            arrPaymentType = [NSArray arrayWithObjects:[NSDictionary dictionaryWithObject:@"Thanh toán tiền mặt + gửi SMS" forKey:@"1"],[NSDictionary dictionaryWithObject:@"Thanh toán online HiFPT/Members" forKey:@"3"],nil];
            [self.btnPaymentType setEnabled:YES];
        }
        
    }
    
    selectedPaymentType = arrPaymentType[0];
    [self.btnPaymentType setTitle:[[selectedPaymentType allValues] objectAtIndex:0] forState:UIControlStateNormal];
    
    // Do any additional setup after loading the view.
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self writeLogmPOSToServer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    self.view.frame = CGRectMake(0, 30, self.view.frame.size.width, self.view.frame.size.height);
}

- (void) resetData {
    
    ShareData *shared = [ShareData instance];
    shared.imageSignature = @"";
}

#pragma mark - DropDown view

- (void)setupDropDownView:(NSInteger) tag
{
    
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.tag = tag;
    poplistview.listView.scrollEnabled = TRUE;
    switch (tag) {
        case Internet:
            [poplistview setTitle:NSLocalizedString(@"Chọn SBI", @"")];
            break;
        case IPTV:
            [poplistview setTitle:NSLocalizedString(@"Chọn SBI IPTV", @"")];
            break;
        case PaymentType:
            [poplistview setTitle:NSLocalizedString(@"Chọn hình thức thanh toán", @"")];
        default:
            break;
    }
    
    [poplistview show];
    
}
- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35.0f;
}

#pragma mark - UIPopoverListViewDataSource

- (void) setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell tag:(NSInteger)tag
{
    SBIRecord *model;
    switch (tag) {
        case Internet:
            model = [self.arrSBIInternet objectAtIndex:row];
            cell.textLabel.text = model.SBI;
            break;
        case IPTV:
            model = [self.arrSBIIPTV objectAtIndex:row];
            cell.textLabel.text = model.SBI;
            break;
        case PaymentType:
            cell.textLabel.text = [[arrPaymentType[row] allValues] objectAtIndex:0];
            break;
        default:
            break;
    }
    
    
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row cell:cell tag:popoverListView.tag];
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    switch (popoverListView.tag) {
        case Internet:
            count = self.arrSBIInternet.count;
            break;
        case IPTV:
            count = self.arrSBIIPTV.count;
            break;
        case PaymentType:
            count = arrPaymentType.count;
            break;
        default:
            break;
            
    }
    return count;
}

#pragma mark - UIPopoverListViewDelegate

- (void) didDropDownSelected:(NSInteger)row tag:(NSInteger)tag
{
    
    switch (tag) {
        case Internet:
            selectedSBIInternet = [self.arrSBIInternet objectAtIndex:row];
            [self.btnSBIInernet setTitle:selectedSBIInternet.SBI forState:UIControlStateNormal];
            break;
        case IPTV:
            selectedSBIIPTV = [self.arrSBIIPTV objectAtIndex:row];
            [self.btnSBIIPTV setTitle:selectedSBIIPTV.SBI forState:UIControlStateNormal];
            break;
        case PaymentType:
            selectedPaymentType = arrPaymentType[row];
            [self.btnPaymentType setTitle:[[selectedPaymentType allValues] objectAtIndex:0] forState:UIControlStateNormal];
            break;
        default:
            break;
    }
}

#pragma mark - UIPopoverListViewDelegate

- (void)popoverListView:(UIPopoverListView *)popoverListView
     didSelectIndexPath:(NSIndexPath *)indexPath
{
    [self didDropDownSelected:indexPath.row tag:popoverListView.tag];
    
}

#pragma mark - event button

-(IBAction)btnSBIInernet_clicked:(id)sender
{
    [self setupDropDownView:Internet];
}

-(IBAction)btnSBIIPTV_clicked:(id)sender
{
    [self setupDropDownView:IPTV];
}

-(IBAction) btnPaymentType_clicked:(id)sender
{
    [self setupDropDownView:PaymentType];
    
}

#pragma mark - loaddata

-(void)LoadData
{
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    ShareData *shared = [ShareData instance];
    [shared.appProxy GetSBIDetail:shared.currentUser.userName RegCode:self.Regcode Status:@"1" completionHandler:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        self.arrList = result;
        NSMutableArray *arrInternet = [NSMutableArray array];
        NSMutableArray *arrIPTV = [NSMutableArray array];
        if(self.arrList.count > 0){
            for (int i = 0; i < self.arrList.count; i++) {
                SBIRecord *rc = [self.arrList objectAtIndex:i];
                if([rc.Type isEqualToString:@"8"]){
                    [arrIPTV addObject:[self.arrList objectAtIndex:i]];
                }else if([rc.Type isEqualToString:@"1"] || [rc.Type isEqualToString:@"2"]){
                    [arrInternet addObject:[self.arrList objectAtIndex:i]];
                }
            }
            
            self.arrSBIIPTV = arrIPTV;
            self.arrSBIInternet = arrInternet;
            [self hideMBProcess];
        }
        
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}

-(IBAction)btnUpdate_clicked:(id)sender {
    
    
    [self checkUpdatePay];
    
}

-(IBAction)btnCancel_clicked:(id)sender{
    
    if(self.delegate){
        
        [self.delegate CancelPopup];
    }
}

- (void)checkUpdatePay
{

    //Kiểm tra số tiền
    if([self.lblTotal.text isEqualToString:@""]){
        [self showAlertBox:@"Thông Báo" message:@"Số tiền thu chưa có"];
        return;
    }
    
    NSString *title =@"";
    
    NSInteger paymenTypeInt = [[[selectedPaymentType allKeys] objectAtIndex:0] integerValue];
    switch (paymenTypeInt) {
            // Kiểm tra hình thức thanh toán
        case 0:
            [self showAlertBox:@"Thông Báo" message:@"Bạn chưa chọn hình thức thanh toán"];
            break;
            
            // Thanh toán bằng tiền mặt
        case 1:
            title = StringFormat(@"Hệ thống sẽ gửi tin nhắn tới số điện thoại: %@ \n Bạn có muốn cập nhật?",self.lblPhone.text);
            [self showActionSheetWithTitle:title andTag:SMSPayment];
            
            break;
            
            // Thanh toán qua mPOS
        case 2:
            // Nếu là PĐK bán thếm thì gọi API check PĐK trước khi thanh toàn qua mPOS
            if (![self.record.regType isEqual:@"0"]) {
                
                [self checkDepositRegisterContract];
                
            } else {
                
                [self checkForDeposit];
                
            }
            break;
        case 3:
            
            //Thanh toán thông qua HiFPT
            if ([self.record.regType isEqual:@"0"]) {//Bán mới
                
                title = StringFormat(@"Hệ thống sẽ gửi tin nhắn tới số điện thoại: %@ \n Bạn có muốn cập nhật?",self.lblPhone.text);
                [self showActionSheetWithTitle:title andTag:SMSPayment];
                
            } else {
                //Bán Thêm
                
                title = StringFormat(@"Hệ thống sẽ gửi tin nhắn tới số điện thoại: %@ \n Bạn có muốn cập nhật?",self.lblPhone.text);
                [self showActionSheetWithTitle:title andTag:SMSPayment];
                
            }
            
        default:
            break;
    }
    
}

- (void)showActionSheetWithTitle:(NSString *)title andTag:(actionSheetTag)tag
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:title delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
    actionSheet.tag = tag;
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}

// Thanh toán PĐK bằng tiền mặt + SMS
- (void)updatePay
{
    
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    
    [self showMBProcess];
    //selectedPaymentType
//    [selectedPaymentType objec]
    NSInteger paymenTypeInt = [[[selectedPaymentType allKeys] objectAtIndex:0] integerValue];
    NSString *paidType = [NSString stringWithFormat:@"%ld",(long)paymenTypeInt];
    
    [shared.registrationFormProxy UpdateDeposit:shared.currentUser.userName RegCode:self.record.RegCode SBI_Internet:selectedSBIInternet.SBI SBI_IPTV:selectedSBIIPTV.SBI Total:self.record.Total ImageSignature:shared.imageSignature PaidType:paidType Completehander:^(id result, NSString *errorCode, NSString *message) {
        
        [self hideMBProcess];
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if([errorCode intValue] >= 0){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            if(self.delegate){
                [self.delegate CancelPopup];
            }
            return;
        }
        
        [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
        return ;
        
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
    
//    [shared.registrationFormProxy UpdateDeposit:shared.currentUser.userName RegCode:self.record.RegCode SBI_Internet:selectedSBIInternet.SBI SBI_IPTV:selectedSBIIPTV.SBI Total:self.record.Total ImageSignature:shared.imageSignature Completehander:^(id result, NSString *errorCode, NSString *message) {
//        
//        [self hideMBProcess];
//        
//        if ([message isEqualToString:@"het phien lam viec"]) {
//            [self ShowAlertErrorSession];
//            [self LogOut];
//             [self hideMBProcess];
//            return;
//        }
//        if([errorCode intValue] >= 0){
//            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
//             [self hideMBProcess];
//            if(self.delegate){
//                [self.delegate CancelPopup];
//            }
//            return;
//        }
//        
//        [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
//        return ;0
//        
//    } errorHandler:^(NSError *error) {
//        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
//        [self hideMBProcess];
//    }];
}

// Kiểm tra PĐK trước khi thanh toán mPOS
- (void)checkForDeposit
{
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    [dict setObject:shared.currentUser.userName     forKey:@"UserName"];
    [dict setObject:self.record.RegCode      ?:@""  forKey:@"RegCode"];
    [dict setObject:selectedSBIInternet.SBI  ?:@"0" forKey:@"sbiInternet"];
    [dict setObject:selectedSBIIPTV.SBI      ?:@"0" forKey:@"sbiIPTV"];
    [dict setObject:self.record.Total        ?:@"0" forKey:@"Total"];
   
    
    [self showMBProcess];
    
    [shared.registrationFormProxy checkForDeposit:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        
        [self hideMBProcess];
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
             [self hideMBProcess];
            return;
        }
        
        // Registration form OK, enable payment by mPOS
        if ([errorCode intValue] == 1) {
            NSArray *arrData = result;
            paymentDescription = [arrData[0] objectForKey:@"Message"];
            [self connectTomPosApp];
             [self hideMBProcess];
            return;
        }
        
        // Registration form not OK, disenable payment by mPOS
        if([errorCode intValue] <= 0){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
             [self hideMBProcess];
            return;
        }
        
        [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
        return ;
        
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
    
    
}

#pragma mark - Payment with registration contract sale more
// Thanh toán PĐK bán thêm bằng tiền mặt + SMS
- (void)depositRegisterContract
{
    
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    [dict setObject:shared.currentUser.userName     forKey:@"UserName"];
    [dict setObject:self.record.RegCode      ?:@""  forKey:@"RegCode"];
    [dict setObject:selectedSBIInternet.SBI  ?:@"0" forKey:@"SBI"];
    [dict setObject:self.record.Total        ?:@"0" forKey:@"Total"];
    [dict setObject:shared.imageSignature             forKey:@"ImageSignature"];
    
    NSInteger paymenTypeInt = [[[selectedPaymentType allKeys] objectAtIndex:0] integerValue];
    NSString *paidType = [NSString stringWithFormat:@"%ld",(long)paymenTypeInt];
    [dict setObject:paidType             forKey:@"PaidType"];
    
    [self showMBProcess];
    
    [shared.saleMoreServiceProxy depositRegisterContract:dict
                                         completeHandler:^(id result, NSString *errorCode, NSString *message) {
                                             
                                             [self hideMBProcess];
                                             
                                             if ([message isEqualToString:@"het phien lam viec"]) {
                                                 [self ShowAlertErrorSession];
                                                 [self LogOut];
                                                 return;
                                             }
                                             if([errorCode intValue] >= 0){
                                                 [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
                                                 if(self.delegate){
                                                     [self.delegate CancelPopup];
                                                 }
                                                 return;
                                             }
                                             
                                             [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
                                             return ;
                                             
                                         } errorHandler:^(NSError *error) {
                                             [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
                                             [self hideMBProcess];
                                         }];
}

// Kiểm tra PĐK trước khi thanh toán mPOS
- (void)checkDepositRegisterContract
{
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    [dict setObject:shared.currentUser.userName     forKey:@"UserName"];
    [dict setObject:self.record.RegCode      ?:@""  forKey:@"RegCode"];
    [dict setObject:selectedSBIInternet.SBI  ?:@"0" forKey:@"SBI"];
    
    [self showMBProcess];
    
    [shared.saleMoreServiceProxy checkDepositRegisterContract:dict
                                              completeHandler:^(id result, NSString *errorCode, NSString *message) {
                                                  
                                                  [self hideMBProcess];
                                                  
                                                  if ([message isEqualToString:@"het phien lam viec"]) {
                                                      [self ShowAlertErrorSession];
                                                      [self LogOut];
                                                       [self hideMBProcess];
                                                      return;
                                                  }
                                                  
                                                  // Registration form OK, enable payment by mPOS
                                                  if ([errorCode intValue] == 1) {
                                                      NSArray *arrData = result;
                                                      paymentDescription = [arrData[0] objectForKey:@"Message"];
                                                      [self connectTomPosApp];
                                                       [self hideMBProcess];
                                                      return;
                                                  }
                                                  
                                                  // Registration form not OK, disenable payment by mPOS
                                                  if([errorCode intValue] <= 0){
                                                      [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
                                                       [self hideMBProcess];
                                                      return;
                                                  }
                                                   [self hideMBProcess];
                                                  [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
                                                  return ;
                                                  
                                              } errorHandler:^(NSError *error) {
                                                  [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
                                                  [self hideMBProcess];
                                              }];
}

// Thu tiền PĐK bán thêm sau khi thanh toàn bằng mPOS thành công
- (void)depositRegisterContractmPOS
{
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    [dict setObject:self.record.RegCode     ?:@"" forKey:@"RegCode"];
    [dict setObject:selectedSBIInternet.SBI ?:@"0" forKey:@"SBI"];
    [dict setObject:self.record.Total       ?:@"0" forKey:@"Total"];
    
    [self showMBProcess];
    
    [shared.saleMoreServiceProxy depositRegisterContractmPOS:dict
                                             completeHandler:^(id result, NSString *errorCode, NSString *message) {
                                                 
                                                 [self hideMBProcess];
                                                 
                                                 if ([message isEqualToString:@"het phien lam viec"]) {
                                                     [self ShowAlertErrorSession];
                                                     [self LogOut];
                                                      [self hideMBProcess];
                                                     return;
                                                 }
                                                 if([errorCode intValue] >= 0){
                                                     [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
                                                      [self hideMBProcess];
                                                     if(self.delegate){
                                                         [self.delegate CancelPopup];
                                                     }
                                                     return;
                                                 }
                                                 
                                                 [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
                                                  [self hideMBProcess];
                                                 return ;
                                                 
                                                 
                                             } errorHandler:^(NSError *error) {
                                                 [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
                                                 [self hideMBProcess];
                                             }];
}

#pragma mark - UIActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        switch (actionSheet.tag) {
            case SMSPayment:
                // Nếu là PĐK bán thêm thì gọi API thu tiền PĐK bán thêm
                if (![self.record.regType isEqual:@"0"]) {
                    [self depositRegisterContract];
                } else {//Phiếu mới
                    [self updatePay];

                }
                break;
                
            case mPOSPayment:
                [self sendPaymentInfoToMPosApp:MARequestOperatorPay];
                break;
                
            default:
                break;
        }
    }
}

#pragma mark - UIAlertView delegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        switch (alertView.tag) {
            case 0:
                [[UIApplication sharedApplication] openURL: [NSURL URLWithString:@"itms-apps://itunes.com/app/TPBankmPOS"]];
                break;
                
            default:
                break;
        }
    }
}

#pragma mark - Payment helper delegate.
- (void)onCallBackXSuccessWithHandleStatus:(MAStatus)status requestOperator:(MARequestOperator)requestOperator transaction:(MATransaction *)transaction
{
    
    NSLog(@"===Handle x-success url===");
    Status = @"0";
    if (status == MAStatusSuccess) {
        Status = @"1";
        self.result_StatusDesc = @"Thanh toán mPOS thành công.";
        // Nếu là PĐK bán thêm thì gọi API thu tiền PĐK bán thêm
        if (![self.record.regType isEqual:@"0"]) {
            [self depositRegisterContractmPOS];
            
        } else {
            [self updateDepositmPOS];

        }
    }
    [self getMAStatus:status andMATransaction:transaction];
    
}

- (void)onCallBackXErrorWithHandleStatus:(MAStatus)status requestOperator:(MARequestOperator)requestOperator error:(MAError *)error transaction:(MATransaction *)transaction
{
    
    NSLog(@"===Handle x-error url===");
    Status = @"0";
    
    if (status == MAStatusSuccess) {
        self.result_StatusDesc = @"Thanh toán mPOS bị lỗi.";
    }
    
    [self getMAError:error];
    
    [self getMAStatus:status andMATransaction:transaction];
}

- (void)onCallBackXCancelWithHandleStatus:(MAStatus)status requestOperator:(MARequestOperator)requestOperator transaction:(MATransaction *)transaction
{
    NSLog(@"===Handle x-cancel url===");
    Status = @"0";
    if (status == MAStatusSuccess) {
        self.result_StatusDesc = @"Thanh toán mPOS đã huỷ.";
        [Common_client showAlertMessage:self.result_StatusDesc];
    }
    
    [self getMAStatus:status andMATransaction:transaction];
    
}

/*
 * setup content Log mPOS
 */
- (void)setUpdateMPOSPaymentLogWithStep:(NSString *)SStep status:(NSString *)status andTransaction:(MATransaction *)transaction
{
    
    ShareData *shared = [ShareData instance];
    
    NSMutableDictionary *toReturn = [NSMutableDictionary dictionary];
    
    if ([shared.currentUser.UseMPOS integerValue] > 0 ) {
        if([SStep isEqualToString:@"0"]){
            result_ErrCode = 0;
            self.result_StatusDesc      = StringFormat(@"Gửi thông tin thanh toán qua mPOS");
            self.result_ErrDesc         = @"";
            self.result_TerminalId      = @"";
            self.result_MerchantId      = @"";
            self.result_CardHolderName  = @"";
            self.result_MaskedCardNumber= @"";
            self.result_ReferenceNbr    = @"";
            self.result_ResponseCode    = @"";
            self.result_TransactionKey  = @"";
            self.result_ApprovedCode    = @"";
            self.result_TransactionDate = @"";
            
        }
        //Vutt11
        if (transaction) {
            self.result_TerminalId       = transaction.terminalId;
            self.result_MerchantId       = transaction.merchantId;
            self.result_CardHolderName   = transaction.cardHolder;
            self.result_MaskedCardNumber = transaction.maskedCardNumber;
            self.result_ReferenceNbr     = transaction.referenceId;
            self.result_ResponseCode     = transaction.responseCode;
            self.result_TransactionKey   = transaction.transactionKey;
            self.result_ApprovedCode     = transaction.approvedCode;
            //
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"dd:MM:yyyy"];

            //[self.btnFromDate setTitle:[dateFormatter stringFromDate:[NSDate date]] forState:UIControlStateNormal];
            self.result_TransactionDate = [dateFormatter stringFromDate:[NSDate date]];
            
            //self.result_TransactionDate  = [self convertDateToString:transaction.transactionDate andDateFormat:@"yyyy/MM/dd"];
            
        }
        
        NSNumber *Result_Mpost_ErrCode = [NSNumber numberWithInteger: result_ErrCode];
        
        [toReturn setValue: shared.currentUser.userName  forKey:@"UserName"];               // tên Sale
        [toReturn setValue: self.record.RegCode          forKey:@"RegCode"];                // mã PĐK
        [toReturn setValue: self.record.RegCode          forKey:@"ReferenceID"];
        [toReturn setValue: self.record.Total            forKey:@"Amount"];                 // số tiền thanh toán
        
        [toReturn setValue: @""                          forKey:@"paymentType"];
        [toReturn setValue: self.result_TransactionDate  forKey:@"transactionDate"];
        [toReturn setValue: @"0"                         forKey:@"transactionAlreadyPaid"]; //Tình trạng PĐK đã thanh toán trước đó hay chưa: 0-Chưa, 1-Có
        
        [toReturn setValue: self.record.Email            forKey:@"SEND_MPOS_EMAIL"];        // email khách hàng
        [toReturn setValue: self.record.FullName         forKey:@"SEND_MPOS_CUSNAME"];      // họ tên khách hàng
        [toReturn setValue: self.record.Phone_1          forKey:@"SEND_MPOS_PHONE_NUMBER"]; // số điện thoại khách hàng
        
        [toReturn setValue: paymentDescription           forKey:@"MPOS_PAYMENT_DESCRIPTION"]; // Lý do thanh toán
        [toReturn setValue: @""                          forKey:@"MPOS_PAYMENT_STEP"]; // “1”: cho phép sửa thông tin giao dịch trên mPOS; “2”:
        [toReturn setValue: @""                          forKey:@"MPOS_TRANSACTION_KEY"]; // “0”: thanh toán bằng thẻ; “1” thanh toán bằng phương thức chuyển khoản
        
        [toReturn setValue: Result_Mpost_ErrCode         forKey:@"Result_ErrCode"];         // mã lỗi trả về từ app mPOS
        [toReturn setValue: self.result_ErrDesc          forKey:@"Result_ErrDesc"];         // Mô tả lỗi tra về từ app mPos
        [toReturn setValue: self.result_TerminalId       forKey:@"Result_TerminalId"];      // Terminal ID trả về từ ngân hàng
        [toReturn setValue: self.result_MerchantId       forKey:@"Result_MerchantId"];      // Merchant ID trả về từ ngân hàng
        [toReturn setValue: self.result_CardHolderName   forKey:@"Result_CardHolderName"];  //Tên chủ thẻ đã thanh toán
        [toReturn setValue: self.result_MaskedCardNumber forKey:@"Result_MaskedCardNumber"];//Số thẻ đã được che 1 phần, gồm 6 số đầu và 4 số cuối
        [toReturn setValue: self.result_ReferenceNbr     forKey:@"Result_ReferenceNbr"];    //Reference number trả về từ ngân hàng
        [toReturn setValue: self.result_ResponseCode     forKey:@"Result_ResponseCode"];    //Mã mô tả kết quả giao dịch trả về từ ngân hàng
        [toReturn setValue: self.result_TransactionKey   forKey:@"Result_TransactionKey"];  //Mã định danh giao dịch trả về từ ngân hàng
        [toReturn setValue: self.result_ApprovedCode     forKey:@"Result_ApprovedCode"];    //Mã dành cho thanh toán thẻ VISA trả về từ ngân hàng
        
        [toReturn setValue:SStep    forKey:@"Step"];// Bước đang thực hiện (0: gửi dữ liệu từ MobiSale -> ứng dụng TPBank mPos, 1: Kết quả từ TPBank mPos trả về MobiSale)
        [toReturn setValue:StringFormat(@"%@. %@", SStep, self.result_StatusDesc)   forKey:@"StepDesc"];// Mô tả các bước
        
        [toReturn setValue:status   forKey:@"Status"];// Tình trạng thanh toán bước hiện tại: 1-Thành công, 0- Thất bại
        
        [toReturn setValue:[self convertDateToString:[NSDate date] andDateFormat:@"yyyy-MM-dd HH-mm-ss"]   forKey:@"ClientDate"];// Ngày ghi log phía client (format: yyyy-MM-dd)
        [toReturn setValue:[self createClientID]   forKey:@"ClientID"];// Do client tự sinh, dùng để update tình trạng cập nhật log phía client
    }
    
    NSString *userName = [shared.currentUser.userName lowercaseString];
    NSString *keyUserDefautls = StringFormat(@"%@_%@",userName, LOG_MPOS_KEY);
    
    [NSUserDefaults saveLogMPOS:toReturn withKey:keyUserDefautls];
    
}

- (NSString *)convertDateToString:(NSDate*)dateInput andDateFormat:(NSString *)dateFormat
{
    @try {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:dateFormat];
        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"GMT+7"];
        [dateFormatter setTimeZone:timeZone];
        NSString *stringDate = [dateFormatter stringFromDate:dateInput];
        return stringDate;
    }
    @catch (NSException *exception) {
        
          [CrashlyticsKit recordCustomExceptionName:@"PayViewController- funtion (saveReceiveRemoteNotification)-Error handle conver date to string" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
        
    }
}

// Create ID for every Log mPOS, ID = reg code + date;
- (NSString *)createClientID
{
    NSString *str = StringFormat(@"%@_%@", self.record.RegCode, [self convertDateToString:[NSDate date] andDateFormat:@"yyyyMMddHHmmssSSS"]);
    
    return str;
}

/*
 * Write content Log mPOS to server when end payment handle
 */
- (void)writeLogmPOSToServer
{
    ShareData *shared = [ShareData instance];
    
    NSString *userName = [shared.currentUser.userName lowercaseString];
    NSString *keyUserDefautls = StringFormat(@"%@_%@",userName, LOG_MPOS_KEY);
    
    NSMutableArray *arrLogs = [NSMutableArray array];
    
    arrLogs = [[[NSUserDefaults standardUserDefaults] arrayForKey:keyUserDefautls] mutableCopy];
    
    if (arrLogs.count <= 0) {
        return;
    }
    
    NSDictionary *dict = [NSDictionary dictionaryWithObject:arrLogs forKey:@"Logs"];
    
    NSLog(@"------ Writing Logs mPOS to Server -------");
    
    [shared.registrationFormProxy writeLogMPos:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        
        if ([errorCode integerValue] > 0) {
            // if write log mPOS tp web server success, delete all logs mPOS were written
            NSLog(@"------ Writed Logs mPOS to Server success -------");
            [arrLogs removeAllObjects];
            [[NSUserDefaults standardUserDefaults] setObject:arrLogs forKey:keyUserDefautls];
            [[NSUserDefaults standardUserDefaults] synchronize];
            return;
        }
        
    } errorHandler:^(NSError *error) {
        
    }];
    
}

/*
 * Payment registration form when pay by mPOS successed
 */
- (void)updateDepositmPOS
{
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    [dict setObject:self.record.RegCode     ?:@"" forKey:@"RegCode"];
    [dict setObject:selectedSBIInternet.SBI ?:@"0" forKey:@"SBI_Internet"];
    [dict setObject:selectedSBIIPTV.SBI     ?:@"0" forKey:@"SBI_IPTV"];
    [dict setObject:self.record.Total       ?:@"0" forKey:@"Total"];
    
    [self showMBProcess];
    
    [shared.registrationFormProxy updateDepositmPOS:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        
        [self hideMBProcess];
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
             [self hideMBProcess];
            return;
        }
        if([errorCode intValue] >= 0){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            if(self.delegate){
                [self.delegate CancelPopup];
                 [self hideMBProcess];
            }
            return;
        }
        
        [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
        return ;
         [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}

/*
 * Send request payment to Payment App
 */
- (void)sendPaymentInfoToMPosApp:(MARequestOperator)requestOperator
{
    // Handle
    [[MAMPosAPIHelper sharedAPIHelper] reset];
    
    //    MAStatus status;
    MAPaymentInfo *paymentInfo;
    
    if (requestOperator == MARequestOperatorPay) {
        // Call payment app.
        paymentInfo = [MAPaymentInfo paymentInfoWithReferenceId:self.record.RegCode
                                                         amount:[self.record.Total intValue] //100
                                                   customerName:self.record.FullName
                                                 customerMobile:self.record.Phone_1
                                                  customerEmail:self.record.Email
                                             paymentDescription:paymentDescription
                                                  paymentMethod:MAPaymentMethodDefault
                                                    paymentStep:MAPaymentStepDefault
                                               loggedInUsername:NULL];
        
        paymentInfo.checkTransactionAlreadyPaid = YES;
        [[MAMPosAPIHelper sharedAPIHelper] payWithPaymentInfo:paymentInfo];
        //        status = [[MAMPosAPIHelper sharedAPIHelper] payWithPaymentInfo:paymentInfo];
        
        //        self.btnUpdate.enabled = NO;
        
        //        [self getMAStatus:status andMATransaction:NULL];
        step = 0;
        [self setUpdateMPOSPaymentLogWithStep:@"0"status:@"0" andTransaction:NULL];
    }
}

- (void)getMAStatus:(MAStatus)status andMATransaction:(MATransaction *)transaction
{
    switch (status) {
        case MAStatusSuccess:
            //   self.result_StatusDesc = @"Xử lý thành công.";
            break;
            
        case MAStatusAPIOperationError:
            self.result_StatusDesc = @"Lỗi mPOS vui lòng liên hệ TPBank";
            break;
            
        case MAStatusNotSetAuthenticationParams:
            self.result_StatusDesc = @"Các tham số cho chứng thực (Credential Key/Credential Password) chưa được set giá trị.";
            break;
            
        case MAStatusNotSetXCallbackParams:
            self.result_StatusDesc = @"Các tham số cho chuẩn x-callback (x-success, x-error, x-cancel) chưa được set giá trị.";
            break;
            
        case MAStatusInvalidReferenceId:
            self.result_StatusDesc = @"Lỗi mã hoá đơn/ mã phiếu thu không đúng. (phải khác rỗng và chiều dài tối đa 32 kí tự).";
            break;
            
        case MAStatusInvalidAmount:
            self.result_StatusDesc = @"Số tiền thanh toán không hợp lệ. (Số tiền phải lớn hơn 0 và nhỏ hơn 1.000.000.000.000.000).";
            break;
            
        case MAStatusInvalidCustomerName:
            self.result_StatusDesc = @"Tên khách hàng không đúng(Chứa kí tự đặc biệt hoặc trên 50 kí tự).";
            break;
            
        case MAStatusInvalidCustomerMobile:
            self.result_StatusDesc = @"Số điện thoại di động không đúng (bắt đầu không phải là 0 hoặc dấu +, độ dài không phải là từ 10 đến 20 số).";
            break;
            
        case MAStatusInvalidCustomerEmail:
            self.result_StatusDesc = @"Email không đúng(Không đúng định dạng hoặc quá 255 kí tự).";
            break;
            
        case MAStatusCanNotOpenPaymentApp:
            self.result_StatusDesc = @"Không thể mở ứng dụng mPOS TPBank.";
            break;
            
        case MAStatusBadURL:
            self.result_StatusDesc = @"URL cần xử lý không phải là URL đã đăng ký với API.";
            break;
            
        case MAStatusCanNotReadResponseData:
            self.result_StatusDesc = @"Không thể đọc dữ liệu trả về từ URL.";
            break;
            
        case MAStatusCanNotDecryptData:
            self.result_StatusDesc = @"Không thể giải mã dữ liệu nhận được.";
            break;
            
        case MAStatusCanNotParseJsonData:
            self.result_StatusDesc = @"Không thể parse json trên dữ liệu đã giải mã.";
            break;
            
        case MAStatusCanNotReadJsonData:
            self.result_StatusDesc = @"Không thể đọc dữ liệu từ dữ liệu json parse được.";
            
        case MAStatusInvalidTransactionKey:
            self.result_StatusDesc = @"Handle status: Invalid Transaction Key.";
            
            break;
        case MAStatusInprogressOtherRequest:
            self.result_StatusDesc = @"Handle status: Is inprogress other request.";
            
            break;
            
        default:
            break;
    }
    
    
    // show alert status
    if (status !=0) {
        [Common_client showAlertMessage:self.result_StatusDesc];
    }
    
    /*
     * save log mPOS
     */
    
    step ++;
    
    [self setUpdateMPOSPaymentLogWithStep:StringFormat(@"%li",(long)step) status:Status andTransaction:transaction];
    
    // write log mPOS to server
    [self writeLogmPOSToServer];
    
}

- (void)getMAError:(MAError *)error
{
    switch (error.code) {
        case MAErrorCodePaymentAppOperationError: // error code 100
            self.result_ErrDesc = @"Xuất hiện lỗi xử lý ở ứng dụng TPBank mPOS.";
            break;
            
        case MAErrorCodePaymentSystemError:       // error code 101
            self.result_ErrDesc = @"Xuất hiện lỗi xử lý ở hệ thống thanh toán của ngân hàng.";
            break;
            
        case MAErrorCodePaymentAppCanNotReadRecievedData: // error code 102
            self.result_ErrDesc = @"Ứng dụng thanh toán không thể đọc được thông tin (yêu cầu) thanh toán.";
            
            break;
            
        case MAErrorCodeAuthenticationError:       // error code 103
            self.result_ErrDesc = @"Chứng thực ứng dụng MobiSale thất bại (Yêu cầu thanh toán bị từ chối).";
            break;
            
        case MAErrorCodeInvalidRequestData:        // error code 104
            self.result_ErrDesc = @"Thông tin thanh toán không hợp lệ (Reference ID rỗng, Amount nhỏ hơn 0 hoặc lớn hơn số có 15 chữ số...).";
            break;
            
        case MAErrorCodeBankSystemError:           // error code 105
            self.result_ErrDesc = @"Xuất hiện lỗi ở hệ thống ngân hàng (Bank system).";
            
            break;
            
        case MAErrorCodePaymentTimeout:            // error code 106
            self.result_ErrDesc = @"Quá thời gian xử lý yêu cầu thanh toán (Yêu cầu thanh toán bị hủy bỏ).";
            
            break;
            
        case MAErrorCodePaymentAppDenyRequest:     // error code 107
            self.result_ErrDesc = @"Yêu cầu thanh toán bị từ chối bởi TPBank mPOS. Do MobiSale đang xử lý cho một yêu cầu thanh toán khác.";
            
            break;
            
        case MAErrorCodeCardProcessingError:       // error code 108
            self.result_ErrDesc = @"Xuất hiện lỗi trong lúc xử lý cập nhật dữ liệu thẻ (Chip) sau khi thực hiện thanh toán.";
            
            break;
        case MAErrorCodeCompanyNotSupportPaymentMethod: // error code 109
            self.result_ErrDesc = @"Công ty không được hỗ trợ phương thức thanh toán.";
            break;
            
        case MAErrorCodeCompanyNotSupportRequestOperator: // error code 110
            self.result_ErrDesc = @"Công ty không được hỗ trợ yêu cầu hoạt động.";
            break;
        case MAErrorCodeTransactionNotExistOnPaymentSystem:
            self.result_ErrDesc = @"Mã hoá đơn không tồn tại trong hệ thống thanh toán.";
            break;
            
        case MAErrorCodeTransactionCanNotBeVoided:
            self.result_ErrDesc = @"Mã hoá đơn không thể huỷ.";
            break;
            
        default:
            break;
    }
    
    result_ErrCode = error.code;
    
    // show alert error
    [Common_client showAlertMessage:self.result_ErrDesc];
    
}

/*
 * Check connect to Payment App
 */
- (void)connectTomPosApp
{
    BOOL checkmPOSApp = [[MAMPosAPI sharedAPI] canOpenPaymentApplication];
    if (checkmPOSApp) {
        NSString *title = StringFormat(@"Hệ thống sẽ gửi thông tin và thanh toán qua TPBank mPOS.\n Bạn có muốn tiếp tục?");
        [self showActionSheetWithTitle:title andTag:mPOSPayment];
        return;
    }
    NSString *message = @"Thiết bị của bạn chưa cài đặt Ứng dụng TPBank mPOS.\n Vui lòng cài đặt ứng dụng để thực hiện quá trình thanh toán!";
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:message delegate:self cancelButtonTitle:@"Đóng" otherButtonTitles:@"OK", nil];
    alertView.tag = 0;
    [alertView show];
}

#pragma mark - Notification delegate.
- (void)didReceiveSuccessNotification:(NSNotification *)notif
{
    // Get data
    MAStatus status = (MAStatus)[[notif.userInfo objectForKey:kMAMPosAPIStatusKey] integerValue];
    MARequestOperator operator = (MARequestOperator)[[notif.userInfo objectForKey:kMAMPosAPIRequestOperatorKey] integerValue];
    MATransaction *transaction = [notif.userInfo objectForKey:kMAMPosAPITransctionKey];
    
    // Handle
    [self onCallBackXSuccessWithHandleStatus:status
                             requestOperator:operator
                                 transaction:transaction];
}

- (void)didReceiveErrorNotification:(NSNotification *)notif
{
    // Get data
    MAStatus status = (MAStatus)[[notif.userInfo objectForKey:kMAMPosAPIStatusKey] integerValue];
    MARequestOperator operator = (MARequestOperator)[[notif.userInfo objectForKey:kMAMPosAPIRequestOperatorKey] integerValue];
    MATransaction *transaction = [notif.userInfo objectForKey:kMAMPosAPITransctionKey];
    MAError *error = [notif.userInfo objectForKey:kMAMPosAPIErrorKey];
    
    // Handle
    [self onCallBackXErrorWithHandleStatus:status
                           requestOperator:operator
                                     error:error
                               transaction:transaction];
}

- (void)didReceiveCancelNotification:(NSNotification *)notif
{
    // Get data
    MAStatus status = (MAStatus)[[notif.userInfo objectForKey:kMAMPosAPIStatusKey] integerValue];
    MARequestOperator operator = (MARequestOperator)[[notif.userInfo objectForKey:kMAMPosAPIRequestOperatorKey] integerValue];
    MATransaction *transaction = [notif.userInfo objectForKey:kMAMPosAPITransctionKey];
    
    // Handle
    [self onCallBackXCancelWithHandleStatus:status
                            requestOperator:operator
                                transaction:transaction];
}


- (IBAction)btnSignature_clicked:(id)sender {
    
    SignatureViewController *signatureViewController = [[SignatureViewController alloc] initWithNibName:@"SignatureViewController" bundle:nil];
    
    signatureViewController.Regcode = self.record.RegCode;
    signatureViewController.delegate = self;

    [self presentViewController:signatureViewController animated:true completion:nil];
    
}

-(void)CancelPopup {
    
     [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
}

-(void)dismissSignatureView:(UIImage *)imageSign {
    
  
    
    [self.btnSignature setTitle:@"" forState:UIControlStateNormal];
    
    self.imgSignature.image = imageSign;
    
}
    
- (void)CancelSignatureView {
    
    [self dismissViewControllerAnimated:true completion:nil];
}

@end
