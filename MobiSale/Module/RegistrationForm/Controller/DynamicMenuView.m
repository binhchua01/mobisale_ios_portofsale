//
//  DynamicMenuView.m
//  MobiSale
//
//  Created by Nguyen Sang on 7/11/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import "DynamicMenuView.h"
#import "DynamicMenuCell.h"

@implementation DynamicMenuView {
    
    NSMutableArray *arrDynamic;
    
    __weak IBOutlet NSLayoutConstraint *constraintHeightTableView;
}

@synthesize delegate;

-(void)configView:(NSMutableArray *)arrDynamicEnum {
    
    arrDynamic = [[NSMutableArray alloc] init];
    
    arrDynamic = arrDynamicEnum;
    
    constraintHeightTableView.constant = 31 * arrDynamic.count;
    
    [self setupTableView];
    
}

-(void)setupTableView {
    
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;
    
    UINib *nib = [UINib nibWithNibName:@"DynamicMenuCell" bundle:nil];
    [self.myTableView registerNib:nib forCellReuseIdentifier:@"DynamicMenuCell"];
    
}

//MARK: -UITABLEVIEWDATASOURCE & UITABLEVIEWDELEGATE

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 31;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrDynamic.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger dynamicMenuView = [arrDynamic[indexPath.row] integerValue];
    
    DynamicMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DynamicMenuCell" forIndexPath:indexPath];
    cell.delegate = self;
    
    switch (dynamicMenuView) {
        case SNDynamicMenuViewBookPort: {
            
            [cell configCell:@"Book port" enumDynamic:SNDynamicMenuViewBookPort];
            
            return cell;
        }
        case SNDynamicMenuViewSurvey: {
            
            [cell configCell:@"Khảo sát" enumDynamic:SNDynamicMenuViewSurvey];
            
            return cell;
        }
        case SNDynamicMenuViewAppointment: {
            
            [cell configCell:@"Hẹn triển khai" enumDynamic:SNDynamicMenuViewAppointment];
            
            return cell;
        }
        case SNDynamicMenuViewUpdate: {
            
            [cell configCell:@"Cập nhật" enumDynamic:SNDynamicMenuViewUpdate];
            
            return cell;
        }
        case SNDynamicMenuViewEarningMoney: {
            
            [cell configCell:@"Thu tiền" enumDynamic:SNDynamicMenuViewEarningMoney];
            
            return cell;
        }
        case SNDynamicMenuViewUpdateAmount: {
            
            [cell configCell:@"Cập nhật tổng tiền" enumDynamic:SNDynamicMenuViewUpdateAmount];
            
            return cell;
        }
        case SNDynamicMenuViewConstructVote: {
            
            [cell configCell:@"Phiếu thi công" enumDynamic:SNDynamicMenuViewConstructVote];
            
            return cell;
        }
        default: {
            
            [cell configCell:@"Huỷ" enumDynamic:SNDynamicMenuViewCancel];
            
            return cell;
        }
    }
    
}

//MARK: -DYNAMICMENUCELLDELEGATE

-(void)pressedButtonDynamicInMenu:(SNDynamicMenuView)dynamic {
    
    //kiểm tra có delegate không ?
    if (!self.delegate) {
        return;
    }
    
    switch (dynamic) {
        case SNDynamicMenuViewBookPort: {
            [self.delegate pressedButtonDynamicInMenu:SNDynamicMenuViewBookPort];
            break;
        }
        case SNDynamicMenuViewSurvey: {
            [self.delegate pressedButtonDynamicInMenu:SNDynamicMenuViewSurvey];
            break;
        }
        case SNDynamicMenuViewAppointment: {
            [self.delegate pressedButtonDynamicInMenu:SNDynamicMenuViewAppointment];
            break;
        }
        case SNDynamicMenuViewUpdate: {
            [self.delegate pressedButtonDynamicInMenu:SNDynamicMenuViewUpdate];
            break;
        }
        case SNDynamicMenuViewEarningMoney: {
            [self.delegate pressedButtonDynamicInMenu:SNDynamicMenuViewEarningMoney];
            break;
        }
        case SNDynamicMenuViewUpdateAmount: {
            [self.delegate pressedButtonDynamicInMenu:SNDynamicMenuViewUpdateAmount];
            break;
        }
        case SNDynamicMenuViewConstructVote: {
            [self.delegate pressedButtonDynamicInMenu:SNDynamicMenuViewConstructVote];
            break;
        }
        default: {
            [self.delegate pressedButtonDynamicInMenu:SNDynamicMenuViewCancel];
            break;
        }
    }
    
}

@end
