//
//  SignatureViewController.h
//  MobiSale
//
//  Created by TranTuanVu on 12/14/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "../../RegistrationForm/Model/RegistrationFormDetailRecord.h"
#import "BaseViewController.h"

@protocol SignatureViewDelegate <NSObject>

-(void) dismissSignatureView: (UIImage *)imageSign;
-(void) CancelSignatureView;

@end

@interface SignatureViewController : BaseViewController

@property (nonatomic, weak) id <SignatureViewDelegate> delegate;

@property (strong ,nonatomic) NSString *Regcode;

@end



