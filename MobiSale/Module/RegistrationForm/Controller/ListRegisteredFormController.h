//
//  ListRegisteredFormController.h
//  MobiSales
//
//  Created by Nguyen Tan Tho on 1/23/15.
//  Copyright (c) 2015 Aryan Ghassemi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "KeyValueModel.h"

@interface ListRegisteredFormController : BaseViewController<UITableViewDataSource,UITableViewDelegate, UIScrollViewDelegate >

@property (nonatomic , retain) IBOutlet UITableView *tableView;

@property (nonatomic , retain) NSMutableArray *arrPage;
@property (nonatomic, strong) KeyValueModel *viewType;

@end
