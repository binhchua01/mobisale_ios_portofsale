//
//  Office365ViewController.h
//  MobiSale
//
//  Created by ISC on 10/3/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.

#import <UIKit/UIKit.h>
#import "ShareData.h"
#import "Office365Model.h"
#import "UIButton+FPTCustom.h"
#import "UIView+FPTCustom.h"
#import "UIViewController+FPTCustom.h"
#import "Office365ViewController.h"
typedef enum:NSInteger
{
    // Gói Mail Pro
    MailPro = 4,
    // Gói Mail Plus
    MailPlus = 3,
    // Gói Tối ưu hóa viễn thông
    TelecomOptimization = 1,
    // Gói hợp thức hóa bản quyền
    LegalizeCopyright = 2
    
} PackageID;

static BOOL FlagChangedOfficeMoneyTotal = NO;
static BOOL checkAddListPucket = NO;
static NSString *kAmountOfficeChangedNotification = @"FPT.ISC.TECH.MobiSale.amountOfficeChange";

/*--------------------------------------------------------------------------*/
/*------------------------ Plus Minus View ----------------------------*/

#define MAXIMUM 100
#define MINIMUM 0

@interface PlusMinusView : UIView

@property (assign, nonatomic) NSInteger maximum;
@property (assign, nonatomic) NSInteger minimum;
@property (assign, nonatomic) NSInteger count;
@property (assign, nonatomic) NSInteger checkMonthAdd;
@property (nonatomic) BOOL checkUpdate;

// Giảm (-)
@property (weak, nonatomic) IBOutlet UIButton *minusButton;
@property (weak, nonatomic) IBOutlet UIButton *miniMailPro;
@property (weak, nonatomic) IBOutlet UIButton *miniMailPlus;
@property (weak, nonatomic) IBOutlet UIButton *miniToiuuhoavienthong;
@property (weak, nonatomic) IBOutlet UIButton *miniHopthuchoabanquyen;

// Tăng (+)
@property (weak, nonatomic) IBOutlet UIButton *plusButton;
@property (weak, nonatomic) IBOutlet UIButton *plusMailPro;
@property (weak, nonatomic) IBOutlet UIButton *plusMailPlus;
@property (weak, nonatomic) IBOutlet UIButton *plusToiuuhoavienthong;
@property (weak, nonatomic) IBOutlet UIButton *plusHopthuchoabanquyen;

// Giá trị
@property (strong, nonatomic) IBOutlet UITextField *countTextField;

- (IBAction)minusButtonPressed:(id)sender;
- (IBAction)plusButtonPressed:(id)sender;

- (void)reset;


@end

/*------------------------------------------------ --------------------------*/
/*---------------------- Package View -----------------------------*/

@interface PackageView : UIView<UIActionSheetDelegate>
@property (strong, nonatomic) Package365Model *packageModel;
// Số tháng
@property (strong, nonatomic) IBOutlet UIButton *monthNumberButton;


@property (weak, nonatomic) IBOutlet UIButton *monthMailPro;
@property (weak, nonatomic) IBOutlet UIButton *monthMailPlus;
@property (weak, nonatomic) IBOutlet UIButton *monthToiuuhoavienthong;
@property (weak, nonatomic) IBOutlet UIButton *monthHopthuchoabanquyen;


@property (assign, nonatomic) NSInteger monthly;
@property (assign, nonatomic) NSInteger checkMonthAdd;
// Số lượng
@property (strong, nonatomic) IBOutlet PlusMinusView *quantityPlusMinusView;
// Check Box
@property (strong, nonatomic) IBOutlet UIButton *checkBoxButton;

@property (strong, nonatomic) IBOutlet UIButton *mailPro;
@property (strong, nonatomic) IBOutlet UIButton *mailPlus;
@property (strong, nonatomic) IBOutlet UIButton *tuhoaVienThong;
@property (strong, nonatomic) IBOutlet UIButton *hthoabanquyen;

//vutt11

- (IBAction)monthNumberButtonPressed:(id)sender;
- (IBAction)checkBoxButtonPressed:(id)sender;

- (void)reset;
- (void)setupData;

@end

/*--------------------------------------------------------------------------*/
/*------------------------ Office 365 View -----------------------------------*/
@protocol Office365ViewControllerDelegate <NSObject>

- (void)cancelOffice365View;
//vutt
- (void)callAddPackage;
@end

@interface Office365ViewController : UIViewController
@property (strong, nonatomic) NSArray *arrayListPacketOld;
@property (strong, nonatomic) NSString *registerID;
@property (assign, nonatomic) NSInteger amountTotal;
@property (strong, nonatomic) id<Office365ViewControllerDelegate> delegate;

// Email admin
@property (strong, nonatomic) IBOutlet UITextField *emailAdminTextField;
// Tên miền
@property (strong, nonatomic) IBOutlet UITextField *domainTextField;
// Họ tên IT
@property (strong, nonatomic) IBOutlet UITextField *nameITTextField;
// Email IT
@property (strong, nonatomic) IBOutlet UITextField *emailITTextField;
// Điện thoại IT
@property (weak, nonatomic) IBOutlet UITextField *phoneITTextField;
// Gói Mail Pro
@property (strong, nonatomic) IBOutlet PackageView *mailProPackageView;
// Gói Mail Plus
@property (strong, nonatomic) IBOutlet PackageView *mailPlusPackageView;
// Gói Tối ưu hóa viễn thông
@property (strong, nonatomic) IBOutlet PackageView *telecomOptimizationPackageView;
// Gói hợp thức hóa bản quyền
@property (strong, nonatomic) IBOutlet PackageView *legalizeCopyrightPackageView;

@property (weak, nonatomic) IBOutlet UILabel *moneyTotal;

- (IBAction)refreshMoneyTotalButtonPressed:(id)sender;
- (IBAction)backButonPressed:(id)sender;
- (IBAction)doneButtonPressed:(id)sender;

@end


