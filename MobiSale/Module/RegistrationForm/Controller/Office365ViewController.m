//             //
//  Office365ViewController.m
//  MobiSale
//
//  Created by ISC on 10/3/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "Office365ViewController.h"
#import "Office365Model.h"

#import "DetailRegisteredForm.h"
/*--------------------------------------------------------------------------*/
/*---------------------------- Plus Minus View -----------------------------*/

@interface PlusMinusView ()

@end

@implementation PlusMinusView
@synthesize maximum, minimum, count,checkMonthAdd,checkUpdate;

- (void)drawRect:(CGRect)rect
{
    [self.minusButton setStyle];
    [self.plusButton  setStyle];
    self.countTextField.userInteractionEnabled = NO;
    if (maximum <= 0) {
        maximum = MAXIMUM;
    }
    if (minimum <= 0) {
        minimum = MINIMUM;
    }
}
//vutt11 fixed
#pragma Mark - Public methods
//All giam so luong box
- (IBAction)minusButtonPressed:(id)sender
{
   // ShareData * shared = [ShareData instance];
    [self changedValueCount:sender];
    
}
// All tang so luong Box
- (IBAction)plusButtonPressed:(id)sender
{
    [self changedValueCount:sender];
    
}

- (void)reset
{
    FlagChangedOfficeMoneyTotal = YES;
    count = 0;
    [self changeValueCountTextField: StringFormat(@"%li", (long)count)];
}


#pragma Mark - Private methods
- (void)changedValueCount:(UIButton *)sender {
    FlagChangedOfficeMoneyTotal = YES;
    count = [self.countTextField.text integerValue];
    
    if (sender == self.minusButton) {
        // decrease 1
        count --;
        if (count < minimum) {
            count = minimum;
        }
    }
    if (sender == self.plusButton) {
        // increase 1
        count ++;
        if (count > maximum) {
            count = maximum;
        }
    }
    //vutt11
    self.checkMonthAdd = count;
  
    ShareData * shared = [ShareData instance];
    
    shared.checkMonthAdd = count;
    
    [self changeValueCountTextField:StringFormat(@"%li", (long)count)];
}

- (void)changeValueCountTextField:(NSString *)text {
    [self.countTextField setText:text];
    [self.countTextField layoutIfNeeded];
}

@end

/*---------------------------------------------------------------------------*/
/*------------------------------ Package View -------------------------------*/

@implementation PackageView

- (void)drawRect:(CGRect)rect
{
    [self styleBorderMap];
}

- (void)setPackageModel:(Package365Model *)packageModel
{

    _packageModel = packageModel;
    if (packageModel) {
        if (packageModel.monthly > 0 ) {
            [self setUserInteractionEnabled:YES];
            
        }
        else{
            
            [self setUserInteractionEnabled:NO];
        }
        self.monthly = packageModel.monthly;
        self.quantityPlusMinusView.count = packageModel.seat + packageModel.seatAdd ;
        [self.quantityPlusMinusView changeValueCountTextField: StringFormat(@"%li", (long)self.quantityPlusMinusView.count)];
        [self changeValueMonthlyButton:StringFormat(@"%li", (long)packageModel.monthly)];
    }
}



#pragma Mark - Public methods
// chon so thang
- (IBAction)monthNumberButtonPressed:(id)sender
{
    
    
    ShareData *shared = [ShareData instance];
    //[self showActionSheetWithTitle:@"Chọn số tháng" andTag:3];
    
        [self showActionSheetWithTitle:@"Chọn số tháng" andTag:3];
    
        if (checkAddListPucket == YES) {
            if (sender == self.monthMailPro) {
                shared.pro = 1;
              
            }
            if (sender == self.monthMailPlus) {
                shared.plus = 2;
                
            }
            if (sender == self.monthToiuuhoavienthong) {
                shared.toiuuhoa = 3;
                
            }
            if (sender == self.monthHopthuchoabanquyen) {
                shared.hopthoa = 4;
                
            }
        }
}
//vutt11 fix Error 365office
// check loai dich vu
- (IBAction)checkBoxButtonPressed:(id)sender
{
    self.checkBoxButton.selected = !self.checkBoxButton.selected;
    [self changedViewData:self.checkBoxButton.selected];
    
}

// Reset data.
- (void)reset
{
    [self changedViewData:NO];
}

// Changed data2
- (void)changedViewData:(BOOL)status
{
    FlagChangedOfficeMoneyTotal = YES;
    self.quantityPlusMinusView.userInteractionEnabled = status;
    self.monthNumberButton.userInteractionEnabled = status;
    if (status) {
        self.quantityPlusMinusView.minimum = self.quantityPlusMinusView.count = 1;
        self.monthly = 3; // value default if checked box
    } else {
        self.quantityPlusMinusView.minimum = self.quantityPlusMinusView.count = 0;
        self.monthly = 0; // value default if not check box
        // if not check box set monthAdd = 0
        
        self.packageModel.monthlyAdd = 0;
    }
    
    [self.quantityPlusMinusView changeValueCountTextField:StringFormat(@"%li", (long)self.quantityPlusMinusView.count)];
    [self changeValueMonthlyButton:StringFormat(@"%li", (long)self.monthly)];
    
}
//set data into PicketModel
//vutt11
- (void)setupData
{
   ShareData * shared = [ShareData instance];
    
   self.packageModel.monthly =  self.monthly;
    NSLog(@"%ld", (long)shared.checkMonthAdd);
    
    if (shared.checkMonthAdd > 1) {
        
        self.packageModel.monthlyAdd = self.monthly;
    }
   
    switch (self.packageModel.packageID) {
            
        //vutt11
        case MailPro:
        case MailPlus:
            if (self.quantityPlusMinusView.count > 0) {
                self.packageModel.seat       = 1;
                self.packageModel.seatAdd    = self.quantityPlusMinusView.count - 1;
               
            } else {
                self.packageModel.seat       = self.quantityPlusMinusView.count;
                self.packageModel.seatAdd    = self.packageModel.seatAdd;
            }
            break;
            
        case TelecomOptimization:
        case LegalizeCopyright:
            self.packageModel.seat       = self.quantityPlusMinusView.count;
            self.packageModel.seatAdd    = self.packageModel.seatAdd;
            
            break;
            
        default:
            break;
    }
}

#pragma Mark - Private methods

- (void)setUserInteractionEnabled:(BOOL)userInteractionEnabled
{
    [self.checkBoxButton setSelected:userInteractionEnabled];
    self.quantityPlusMinusView.userInteractionEnabled = userInteractionEnabled;
    self.monthNumberButton.userInteractionEnabled = userInteractionEnabled;
    
}
- (void)changeValueMonthlyButton:(NSString *)text {
    [self.monthNumberButton setTitle:text forState:UIControlStateNormal];
    [self.monthNumberButton layoutIfNeeded];
}

#pragma mark - Show ActionSheet View method
- (void)showActionSheetWithTitle:(NSString *)title andTag:(NSInteger)tag
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:title delegate:self cancelButtonTitle:@"Đóng" destructiveButtonTitle:nil otherButtonTitles:@"3", @"6", @"12", nil];
    actionSheet.tag = tag;
    
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
    
}

#pragma mark - UIActionSheetDelegate methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 3 && buttonIndex != 3) {
        NSString *title = [actionSheet buttonTitleAtIndex:buttonIndex];
        self.monthly = [title integerValue];
        [self.monthNumberButton setTitle:title forState:UIControlStateNormal];
    
    }
    
}

@end

/*------------------------------------------------------------------------------*/
/*---------------------------- Office 365 View ---------------------------------*/
@interface Office365ViewController ()
@property (strong, nonatomic) dispatch_queue_t concurrentOffice365Queue2;
@property (strong, nonatomic) NSMutableArray*arrayListPackage;
@property (strong, nonatomic) NSArray*arrayListPackage1;
@property (strong, nonatomic) NSArray*arrayListPackage2;
@end

@implementation Office365ViewController
{
    BOOL isCheckedOfficeAmount;
    RegistrationFormDetailRecord *rc;
   // Office365Model *office365Model;
 
}
- (NSMutableArray *)arrayListPackage
{
    if (!_arrayListPackage) {
        _arrayListPackage = [[NSMutableArray alloc]init];
    }
    
    return _arrayListPackage;
    
}
- (NSArray *)arrayListPackage1
{
    if (!_arrayListPackage1) {
        _arrayListPackage1 = [[NSMutableArray alloc]init];
    }
    
    return _arrayListPackage1;
    
}
- (NSArray *)arrayListPackage2
{
    if (!_arrayListPackage2) {
        _arrayListPackage2 = [[NSMutableArray alloc]init];
    }
    
    return _arrayListPackage2;
    
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dataChangedNotification:)
                                                 name:kOffice365ManagerAddedContentNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(setParameterNotification:)
                                                 name:kOffice365ManagerSetParameterNotification
                                               object:nil];
    //vutt11
    [self setViewData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dataChangedNotification:(NSNotification *)notification
{
    Office365Model *office365Model = [Office365Model sharedManager];
    NSArray *listPackages = [office365Model listPackages];
    [self getPackageData:listPackages];
    
}

- (void)setParameterNotification:(NSNotification *)notification
{
    [self showMBProcess];
    [self refreshMoneyTotal];
}

// Hiển thị dữ liệu

- (void)setViewData
{
    //vutt11
    ///  Nếu tổng tiền office > 0, cập nhật Office cho Phiếu đăng ký.
    ShareData * shared = [ShareData instance];
    checkAddListPucket = NO;
    shared.check = NO;
    Office365Model *office365Model = [Office365Model sharedManager];
    NSArray *listPackages = [office365Model listPackages];
   
    if (self.amountTotal > 0) {
        shared.amountTotal = self.amountTotal;
        
        //shared.arrayPacKageParam2 = office365Model.listPackages;
        
        // Email admin.
        self.emailAdminTextField.text  = office365Model.emailAdmin;
        // Tên miền.
        self.domainTextField.text      = office365Model.domainName;
        // Họ tên IT.
        self.nameITTextField.text      = office365Model.techName;
        // Email IT.
        self.emailITTextField.text     = office365Model.techEmail;
        // Điện thoại IT.
        self.phoneITTextField.text     = office365Model.techPhoneNumber;
        // Tổng tiền.
        self.moneyTotal.text           = StringFormat(@"%li", (long)self.amountTotal);
        // Các gói dịch vụ.
        
        if (listPackages.count > 0) {
            
            [self getPackageData:listPackages];
             [self getPackagesOffice365WithType:@"1" withObjID:@"0" withID:@"0" andRefID:@"0"];
            checkAddListPucket = YES;
            shared.checkParam = YES;
            
        }
    }
    
    //  Nếu tổng tiền office < 0, tạo mới Office cho Phiếu đăng ký.
    else {
        [self showMBProcess];
        [self getPackagesOffice365WithType:@"1" withObjID:@"0" withID:@"0" andRefID:@"0"];
        ShareData * shared = [ShareData instance];
        shared.checkParam = NO;
    }
    
    [self.view layoutIfNeeded];
    
}

- (void)getPackageData:(NSArray *)listPackages
{
  

    [self.arrayListPackage addObject:listPackages];
    
    for (Package365Model *package in listPackages) {
        switch (package.packageID) {
            case MailPro:
                _mailProPackageView.packageModel = package;
                
                break;
                
            case MailPlus:
                _mailPlusPackageView.packageModel = package;
                
                break;
                
            case TelecomOptimization:
                _telecomOptimizationPackageView.packageModel = package;
                
                break;
                
            case LegalizeCopyright:
                _legalizeCopyrightPackageView.packageModel = package;
                
                break;
                
            default:
                break;
        }
    }
   // }
//}
}
#pragma Mark - Get Info Packages Office 365
/*
 A. @Type =1:
 • @ObjId=0: (@ID=0,RefId=0)
 Trả về tất cả các gói.
 • ObjId=0: (@ID=0,RefId<>0)
 Trả về các gói có trong phiếu đăng ký  WHERE ADSLRegisterId=@RefId.
 • ObjId=0: (@ID=0,RefId=0)
 Lấy tất cả các gói không có trong hợp đồng và phiếu đăng ký.
 B. @Type=2:
 • @ID<>0: (@ObjId, RefId=0) Trả về gói theo Id
 • @ID=0: (@ObjId<>0 ,@RefId=0) Trả về gói có trong hợp đồng và phiếu đăng ký."
 */

- (void)getPackagesOffice365WithType:(NSString *)type
                           withObjID:(NSString *)objID
                              withID:(NSString *)ID
                            andRefID:(NSString *)refID
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    parameters = [self setParameterWithType:type withObjID:objID withID:ID andRefID:refID];
    
    Office365Model *office365Model = [Office365Model sharedManager];
    NSArray *listPackages = [office365Model listPackages];
    //vutt11

    ShareData * shared = [ShareData instance];
    [shared.registrationFormProxy getPackage365:parameters
                                 completionHandler:^(id result, NSString *errorCode, NSString *message) {
                                     if (listPackages.count == 0) {
                                         
                                         [[Office365Model sharedManager] resetData];
                                     }
                                      //[[Office365Model sharedManager] resetData];
                                     NSMutableArray *arrData = result;
                                     self.arrayListPackage = result;
                                     
                                     if (arrData.count <= 0) {
                                         [self showAlertBox:@"Thông báo!" message:message tag:ErrorTag];
                                         
                                     }
                            
                                     //vutt11
                                     // fix add PackageModel
                                     // trường hợp tạo mới
                                     if (listPackages.count == 0)
                                     {
                                         for (NSDictionary *dict in arrData) {
                                             Package365Model *model = [[Package365Model alloc] initWithDictionary:dict];
                                             [[Office365Model sharedManager] addPackage365:model];
                                         }
                                     }
                                     
                                     int i = 0;
                                     NSInteger  t;
                                     //trường hợp đã tao khi cấp nhật nếu không đủ 4 gói
                                     if (listPackages.count > 0 && listPackages.count < 4) {
                                         
                                         for (Package365Model *package in listPackages) {
                                            
                                             switch (package.packageID) {
                                                 //Toi uu hoa
                                                 case TelecomOptimization:
                                                     for (i= 0; i< arrData.count  ;i++ ) {
                                                       t = [[NSString stringWithFormat:@"%@",[arrData[i] valueForKey:@"PackageID"]] integerValue] ;
                                                         
                                                         if (TelecomOptimization == t) {
                                                             
                                                             [arrData removeObjectAtIndex:i];
                                                             
                                                         }
                                                     }
                                                     
                                                     break;
                                                 // Hop thuc hoa
                                                 case LegalizeCopyright:
                                                     for (i= 0; i< arrData.count  ;i++ ) {
                                                         t = [[NSString stringWithFormat:@"%@",[arrData[i] valueForKey:@"PackageID"]] integerValue] ;
                                                         
                                                         if (LegalizeCopyright == t) {
                                                             
                                                             [arrData removeObjectAtIndex:i];
                                                             
                                                         }
                                                     }
                                                     break;
                                                 //MailPlus
                                                 case MailPlus:
                                                     for (i= 0; i< arrData.count  ;i++ ) {
                                                         t = [[NSString stringWithFormat:@"%@",[arrData[i] valueForKey:@"PackageID"]] integerValue] ;
                                                         
                                                         if (MailPlus == t) {
                                                             
                                                             [arrData removeObjectAtIndex:i];
                                                             
                                                         }
                                                     }
                                                     break;
                                                 //MailPro
                                                 case MailPro:
                                                     for (i= 0; i< arrData.count  ;i++ ) {
                                                         
                                                         t = [[NSString stringWithFormat:@"%@",[arrData[i] valueForKey:@"PackageID"]] integerValue] ;
                                                         
                                                         if (MailPro == t) {
                                                             
                                                             [arrData removeObjectAtIndex:i];
                                                             
                                                         }
                                                     }                                                     break;
                                                     
                                                 default:
                                                     break;
                                             }
                                             
                                            
                                             
                                         }
                                         
                                         for (NSDictionary *dict in arrData) {
                                             Package365Model *model = [[Package365Model alloc] initWithDictionary:dict];
                                             [[Office365Model sharedManager] addPackage365:model];
                                         }
                                         
                                         
                                     }
                                     
                                     [self hideMBProcess];
                                     return;
                                     
                                 } errorHandler:^(NSError *error) {
                                     [self showAlertBox:@"Lỗi!" message:[error description] tag:ErrorTag];
                                     [self hideMBProcess];
                                 }];
    
}

#pragma Mark - Refresh money total
// Button refresh action tong tien
- (IBAction)refreshMoneyTotalButtonPressed:(id)sender {
    
    //vutt11
    
    [self setupPackagesData];
    [[Office365Model sharedManager] setParamaterListPackage];
    
}
// Action back
- (IBAction)backButonPressed:(id)sender {
    if (self.delegate) {
        [self.delegate cancelOffice365View];
    }
    
}
// Action Done
- (IBAction)doneButtonPressed:(id)sender {
    isCheckedOfficeAmount = [self checkOfficeInfo];
    if (isCheckedOfficeAmount) {
        [self setupPackagesData];
        [[Office365Model sharedManager] setParamaterListPackage];
        if (self.delegate) {
            [self.delegate cancelOffice365View];
        }
    }
}
// Kiểm tra thông tin đăng ký Office
//vutt11
- (BOOL)checkOfficeInfo
{
    if (self.emailAdminTextField.text.length > 0) {
        if (![self CheckInputEmail:self.emailAdminTextField.text]) {
            [self checkingTextField:self.emailAdminTextField message:@"Bạn đã nhập mail không đúng vui lòng nhập lại!!!"];
             return NO;
        }
       
    }
    if ([self.emailAdminTextField.text isEqualToString:@""]) {
        [self checkingTextField:self.emailAdminTextField message:@"Vui lòng điền Email admin!"];
        return NO;
        
    }
    
    if (self.domainTextField.text.length > 0) {
        if (![self validateUrl:self.domainTextField.text]) {
            [self checkingTextField:self.domainTextField message:@"IP/Domain không hợp lệ!(ví dụ(172.30.27.24))"];
            return NO;
        }
    
    }
    if ([self.domainTextField.text isEqualToString:@""]) {
        [self checkingTextField:self.domainTextField message:@"Vui lòng điền Tên miền!"];
        
        return NO;
    }

    if (self.emailITTextField.text.length >0) {
        if (![self CheckInputEmail:self.emailITTextField.text]) {
            [self checkingTextField:self.emailITTextField message:@"Bạn đã nhập mail không đúng vui lòng nhập lại!!!"];
            return NO;
        }
        
    }
    if ([self.emailITTextField.text isEqualToString:@""]) {
        [self checkingTextField:self.emailITTextField message:@"Vui lòng điền Họ tên mail IT!"];
        
        return NO;
    }
    if ([self.nameITTextField.text isEqualToString:@""]) {
        [self checkingTextField:self.nameITTextField message:@"Vui lòng điền Họ tên!!! "];
        return NO;
    }
    
    if ([self.phoneITTextField.text isEqualToString:@""]) {
        [self checkingTextField:self.phoneITTextField message:@"Vui lòng điền Điện thoại IT!"];
        
        return NO;
    }
    
    int len = (int)[self.phoneITTextField.text length];
    if (len<10) {
        [self checkingTextField:self.phoneITTextField message:@"Số điện thoại không hợp lệ"];
        return NO;
    }
    
    BOOL selectedMailProPackage  = self.mailProPackageView.checkBoxButton.selected;
    BOOL selectedMailPlusPackage = self.mailPlusPackageView.checkBoxButton.selected;
    BOOL selectedTelecomOptimizationPackage  = self.telecomOptimizationPackageView.checkBoxButton.selected;
    BOOL selectedLegalizeCopyrightPackage  = self.legalizeCopyrightPackageView.checkBoxButton.selected;
    
    if (selectedMailProPackage || selectedMailPlusPackage || selectedTelecomOptimizationPackage || selectedLegalizeCopyrightPackage) {
        
        return YES;
        
    } else {
        
        [self showAlertBox:@"Thông báo!" message:@"Vui lòng chọn Gói dịch vụ!" tag:4];
        
        return NO;
    }
    
    return YES;
}

- (BOOL)CheckInputEmail:(NSString*)Email
{
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    return  ([emailTest evaluateWithObject:Email] == NO)?false:true;
}

//check Domain
- (BOOL)validateUrl:(NSString *)candidate {
    NSString *urlRegEx =
    @"(http[s]?\\:\\/\\/)?((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+";
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:candidate];
    
}

- (void)checkingTextField:(UITextField *)textField message:(NSString *)message
{
    [self showAlertBox:@"Thông báo!" message:message tag:4];
    [textField becomeFirstResponder];
    
    return;
}

// Setup data các gói package khi thay đổi
- (void)setupPackagesData
{
    
    Office365Model *office365Model = [Office365Model sharedManager];
    // Email admin.
    office365Model.emailAdmin       = self.emailAdminTextField.text;
    // Tên miền.
    office365Model.domainName       = self.domainTextField.text ;
    // Họ tên IT.
    office365Model.techName         = self.nameITTextField.text;
    // Email IT.
    office365Model.techEmail        = self.emailITTextField.text;
    // Phone IT.
    office365Model.techPhoneNumber  = self.phoneITTextField.text;
    // Gói Mail Pro
    [self.mailProPackageView setupData];
    // Gói Mail Plus
    [self.mailPlusPackageView setupData];
    // Gói Tối ưu hóa viễn thông
    [self.telecomOptimizationPackageView setupData];
    // Gói Hợp thức hóa bản quyền
    [self.legalizeCopyrightPackageView setupData];
    
}
// Tính tổng tiền Office 365.
//vutt11
- (void)refreshMoneyTotal
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    ShareData *shareData = [ShareData instance];
    parameters = [self setParameterWithUserName:shareData.currentUser.userName];
    
    [shareData.registrationFormProxy getTotalOffice365:parameters
                                     completionHandler:^(id result, NSString *errorCode, NSString *message) {
                                         NSArray *arrData = result;
                                         if (arrData.count <= 0) {
                                             // Tính tổng tiền thất bại
                                             /// Show thông báo lỗi
                                             [self showAlertBox:@"Thông báo!" message:message tag:ErrorTag];
                                             
                                         } else {
                                             // Tính tổng tiền thành công
                                             NSDictionary *dict = arrData[0];
                                             self.amountTotal = [dict[@"Amount"] integerValue];
                                             [self titleMoneyTotalLabelChanged];
                                             if (isCheckedOfficeAmount) {
                                                 // Notification amount office
                                                 [[NSNotificationCenter defaultCenter] postNotificationName:kAmountOfficeChangedNotification object:nil];
                                             }
                                         }
                                         
                                         [self hideMBProcess];
                                         
                                         return;
                                         
                                     } errorHandler:^(NSError *error) {
                                         [self showAlertBox:@"Lỗi!" message:[error description] tag:ErrorTag];
                                         [self hideMBProcess];
                                     }];
    
}
// Thay đổi tổng tiền hiển thị.
- (void)titleMoneyTotalLabelChanged
{
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    self.moneyTotal.text = [nf stringFromNumber:[NSNumber numberWithFloat: self.amountTotal ]];
}

#pragma Mark - Setup parameter for API
// Set Parameters for API GetPackage365
- (NSMutableDictionary *)setParameterWithType:(NSString *)type
                                    withObjID:(NSString *)objID
                                       withID:(NSString *)ID
                                     andRefID:(NSString *)refID
{
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setObject:type  forKey:@"Type"];
    [parameters setObject:objID forKey:@"ObjID"];
    [parameters setObject:ID    forKey:@"ID"];
    [parameters setObject:refID forKey:@"RefID"];
    
    return parameters;
}

// Set Parameters for API GetTotalOffice365
- (NSMutableDictionary *)setParameterWithUserName:(NSString *)userName
{
    NSArray *listPackage = [[Office365Model sharedManager] listPackagesParameter];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    
    [parameters setObject:userName forKey:@"UserName"];
    [parameters setObject:listPackage forKey:@"ListPackage"];
    
    return parameters;
    
}

//vutt11

- (void)slipArray {
    ShareData *shared = [ShareData instance];
    NSInteger count = shared.arrayPacKage.count;
    self.arrayListPackage1 = [shared.arrayPacKage subarrayWithRange:NSMakeRange(0, count - 4)];
    self.arrayListPackage2 = [shared.arrayPacKage subarrayWithRange:NSMakeRange(count - 4, 4)];
    
    shared.arrayPacKage1 = self.arrayListPackage1;
    shared.arrayPacKage2 = self.arrayListPackage2;
    
    
}

@end
