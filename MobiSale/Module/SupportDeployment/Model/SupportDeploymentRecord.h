//
//  SupportDeploymentRecord.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 2/3/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SupportDeploymentRecord : NSObject

@property (nonatomic,retain) NSString *Allot;
@property (nonatomic,retain) NSString *Contract;
@property (nonatomic,retain) NSString *CurrentPage;
@property (nonatomic,retain) NSString *DeploymentDate;
@property (nonatomic,retain) NSString *ErrorService;
@property (nonatomic,retain) NSString *FinishDate;
@property (nonatomic,retain) NSString *ObjDate;
@property (nonatomic,retain) NSString *ObjectPhone;
@property (nonatomic,retain) NSString *RegCode;
@property (nonatomic,retain) NSString *RowNumber;
@property (nonatomic,retain) NSString *TotalPage;
@property (nonatomic,retain) NSString *TotalRow;
@property (nonatomic,retain) NSString *WaitTime;
@property (nonatomic,retain) NSString *Istatus;
@property (nonatomic,retain) NSString *DeployAppointmentDate;
//vutt11
@property (nonatomic,retain) NSString *FullName;

@end
