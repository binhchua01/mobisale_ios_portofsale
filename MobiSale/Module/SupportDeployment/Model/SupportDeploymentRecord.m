//
//  SupportDeploymentRecord.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 2/3/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "SupportDeploymentRecord.h"

@implementation SupportDeploymentRecord

@synthesize Allot;
@synthesize Contract;
@synthesize CurrentPage;
@synthesize DeploymentDate;
@synthesize ErrorService;
@synthesize FinishDate;
@synthesize ObjDate;
@synthesize ObjectPhone;
@synthesize RegCode;
@synthesize RowNumber;
@synthesize TotalPage;
@synthesize TotalRow;
@synthesize WaitTime;
@synthesize Istatus;
@synthesize FullName;
@end
