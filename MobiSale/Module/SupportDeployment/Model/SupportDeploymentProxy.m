//
//  SupportDeploymentProxy.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 2/3/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "SupportDeploymentProxy.h"

#import "ShareData.h"
#import "KeyValueModel.h"
#import "FPTUtility.h"

#define mGetListDeploymentProgress               @"GetListDeploymentProgress"
#define mPushDeployment                          @"PushDeployment"
#define mGetListDeploymentProgress                         @"GetListDeploymentProgress"

@implementation SupportDeploymentProxy

#pragma mark - Get List Prechecklist
- (void)GetListDeploymentProgress:(NSString *)agent AgentName:(NSString *)agentname PageNum:(NSString*)pagenum Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI,mGetListDeploymentProgress )];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:[ShareData instance].currentUser.userName forKey:@"UserName"];
    [dict setObject:agent forKey:@"Agent"];
    [dict setObject:agentname forKey:@"AgentName"];
    [dict setObject:pagenum forKey:@"PageNumber"];
    postString = [dict JSONRepresentation];
    
    NSString *strUTF8 = @"";
    NSMutableString *s = [NSMutableString stringWithString:postString];
    [s replaceOccurrencesOfString:@"\\" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    strUTF8 = [NSString stringWithString:s];
    
    [(NSMutableURLRequest *)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest *)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:strUTF8];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    

    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetListDeploymentProgress:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
}
- (void)endGetListDeploymentProgress:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSArray *arr = [jsonDict objectForKey:@"GetListDeploymentProgressMethodPostResult"]?:nil;
    
    
    if (arr.count <= 0) {
        
        return;
    }
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    if(![ErrorService isEqualToString:@"<null>"]){
        handler(nil, ErrorService, ErrorService);
        return;
    }

    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        SupportDeploymentRecord *p = [self ParseRecordList:d];
        [mArray addObject:p];
    }
    handler(mArray, ErrorService, @"");
    
}

-(SupportDeploymentRecord *) ParseRecordList:(NSDictionary *)dict{
    SupportDeploymentRecord *rc = [[SupportDeploymentRecord alloc]init];
    
    rc.Allot = StringFormat(@"%@",[dict objectForKey:@"Allot"]);
    rc.Contract = StringFormat(@"%@",[dict objectForKey:@"Contract"]);
    rc.CurrentPage = StringFormat(@"%@",[dict objectForKey:@"CurrentPage"]);
    rc.DeploymentDate = StringFormat(@"%@",[dict objectForKey:@"DeploymentDate"]);
    rc.FinishDate = StringFormat(@"%@",[dict objectForKey:@"FinishDate"]);
    rc.RowNumber = StringFormat(@"%@",[dict objectForKey:@"RowNumber"]);
    rc.TotalPage = StringFormat(@"%@",[dict objectForKey:@"TotalPage"]);
    rc.TotalRow = StringFormat(@"%@",[dict objectForKey:@"TotalRow"]);
    rc.ObjDate = StringFormat(@"%@",[dict objectForKey:@"ObjDate"]);
    rc.ObjectPhone = StringFormat(@"%@",[dict objectForKey:@"ObjectPhone"]);
    rc.RegCode = StringFormat(@"%@",[dict objectForKey:@"RegCode"]);
    rc.WaitTime = StringFormat(@"%@",[dict objectForKey:@"WaitTime"]);
    if(![rc.WaitTime isEqualToString:@"0"]){
        rc.Istatus = @"1";
    }else {
        rc.Istatus = @"0";
    }
    
    rc.DeployAppointmentDate = StringFormat(@"%@",[dict objectForKey:@"DeployAppointmentDate"]);
    
    //vutt11
    rc.FullName = StringFormat(@"%@",[dict objectForKey:@"FullName"]);
    
    
    return rc;
}

#pragma mark - send mail
- (void)PushDeployment:(NSString *)username Contract:(NSString *)contract Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mPushDeployment)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:username forKey:@"UserName"];
    [dict setObject:contract forKey:@"Contract"];
    
    postString = [dict JSONRepresentation];

    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];

     [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endPushDeployment:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
  
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
    
}
- (void)endPushDeployment:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }

    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *root = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    NSString *ErrorCode = StringFormat(@"%@",[root objectForKey:@"ErrorCode"]) ;
    
    if(root == nil){
        handler(nil, @"0", @"");
        return;
    }
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, @"0", @"");
        return;
    }

    NSArray *arr = [root objectForKey:@"ListObject"];
    NSString *Message = [[arr objectAtIndex:0] objectForKey:@"Result"];
    ErrorCode = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ResultID"]);
    handler(nil, ErrorCode, Message);
}

@end
