//
//  SupportDeploymentProxy.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 2/3/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BaseProxy.h"
#import "SupportDeploymentRecord.h"

@interface SupportDeploymentProxy : BaseProxy


- (void)GetListDeploymentProgress:(NSString *)agent AgentName:(NSString *)agentname PageNum:(NSString*)pagenum Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)PushDeployment:(NSString *)username Contract:(NSString *)contract Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

@end
