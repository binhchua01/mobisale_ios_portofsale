//
//  SupportDeploymentController.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 1/24/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "SupportDeploymentController.h"
#import "SupportDeploymentCell.h"
#import "UIPopoverListView.h"
#import "SupportDeploymentRecord.h"
#import "KeyValueModel.h"
#import "Common.h"
#import "ShareData.h"
 #import "Common_client.h"

@interface SupportDeploymentController () <UIAlertViewDelegate>

@end

@implementation SupportDeploymentController{
    KeyValueModel *selectedType,*selectedPage;
    NSString *Contract;
    NSString *totalPage;
}
@dynamic tableView;
enum tableSourceStype {
    TypeSearch = 1,
    Page = 2,
};

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//    [self.tableView setSeparatorColor:[UIColor clearColor]];
    
    self.arrTypeFind = [Common getTypeSearch:@"2"];
    selectedType = [self.arrTypeFind objectAtIndex:0];
    [self.btnTypeFind setTitle:selectedType.Values forState:UIControlStateNormal];
    
    [self LoadData:@"0" AgentName:@"0" PageNum:@"1"];
    
    self.title = @"TIẾN ĐỘ TRIỂN KHAI";
    self.screenName=self.title;
    
    self.txtTextSearch.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillDisappear:(BOOL)animated{
    [self hideMBProcess];
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

#pragma TableView


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        if(self.arrList.count > 0 )
            return self.arrList.count;
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 300;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *TableIdentifier = @"SupportDeploymentCell";
    SupportDeploymentCell *cell = (SupportDeploymentCell *)[tableView dequeueReusableCellWithIdentifier:TableIdentifier];
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SupportDeploymentCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    if(self.arrList.count > 0){
        SupportDeploymentRecord *rc = [self.arrList objectAtIndex:indexPath.row];
        cell.lblContact.text = rc.Contract;
        cell.lblSTT.text = rc.RowNumber;
        cell.lblId.text = rc.RegCode;
        cell.lblCreateDate.text = rc.ObjDate;
        cell.lblPhone.text = rc.ObjectPhone;
        cell.lblDeploymentDate.text = rc.DeploymentDate;
        cell.lblFinishDate.text = rc.FinishDate;
        cell.lblAssign.text = rc.Allot;
        cell.lblInventory.text = rc.WaitTime;
        cell.lblDeploymentAppointmentDate.text = rc.DeployAppointmentDate;
        cell.lblFullName.text = rc.FullName;
        if([rc.Istatus isEqualToString:@"1"]){
            cell.img.image = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"ic_message"] height:30];
        }
    }
    
    return cell;
    
}
#pragma mark Table DidSelected
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SupportDeploymentRecord *rc = [self.arrList objectAtIndex:indexPath.row];
    if([rc.Istatus isEqualToString:@"1"]){
        Contract = rc.Contract;
        UIAlertView *myAlert = [[UIAlertView alloc]
                                initWithTitle:@"Thông Báo"
                                message:@"Bạn có muốn gửi mail không?"
                                delegate:self
                                cancelButtonTitle:@"Huỷ"
                                otherButtonTitles:@"Gửi",nil];
        [myAlert show];
    }
}

#pragma mark - DropDown view

- (void)setupDropDownView:(NSInteger) tag
{
    
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.tag = tag;
    poplistview.listView.scrollEnabled = TRUE;
    switch (tag) {
        case TypeSearch:
            [poplistview setTitle:NSLocalizedString(@"Chọn loại tìm kiếm", @"")];
            break;
        case Page:
            [poplistview setTitle:NSLocalizedString(@"Chọn trang", @"")];
            break;
        default:
            break;
    }
    
    [poplistview show];
    
}
- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35.0f;
}

#pragma mark - UIPopoverListViewDataSource

- (void) setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell tag:(NSInteger)tag{
    KeyValueModel *model;
    switch (tag) {
        case TypeSearch:
            model = [self.arrTypeFind objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Page:
            model = [self.arrPage objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        default:
            break;
    }
    
    
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row cell:cell tag:popoverListView.tag];
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    switch (popoverListView.tag) {
        case TypeSearch:
            count = self.arrTypeFind.count;
            break;
        case Page:
            count = self.arrPage.count;
            break;
        default:
            break;
    }
    return count;
}

#pragma mark - UIPopoverListViewDelegate

- (void) didDropDownSelected:(NSInteger)row tag:(NSInteger)tag{
    
    switch (tag) {
        case TypeSearch:
            selectedType = [self.arrTypeFind objectAtIndex:row];
            [self.btnTypeFind setTitle:selectedType.Values forState:UIControlStateNormal];
            break;
        case Page:
            selectedPage = [self.arrPage objectAtIndex:row];
            [self.btnPage setTitle:[NSString stringWithFormat:@"%@/%@",selectedPage.Key,totalPage] forState:UIControlStateNormal];
            [self LoadData:selectedType.Key AgentName:self.txtTextSearch.text PageNum:selectedPage.Key];
        default:
            break;
    }
    
    
}


#pragma mark - UIPopoverListViewDelegate

- (void)popoverListView:(UIPopoverListView *)popoverListView
     didSelectIndexPath:(NSIndexPath *)indexPath
{
    [self didDropDownSelected:indexPath.row tag:popoverListView.tag];
    
}

#pragma mark - Search Delegate
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self.view endEditing:YES];
    NSString *text = self.txtTextSearch.text;
    text = [Common normalizeVietnameseString:text];
    [self LoadData:selectedType.Key AgentName:text PageNum:@"1"];
}

#pragma mark - event button click

-(IBAction)btnFind_clicked:(id)sender
{
    [self.view endEditing:YES];
    NSString *text = self.txtTextSearch.text;
    text = [Common normalizeVietnameseString:text];
    [self LoadData:selectedType.Key AgentName:text PageNum:@"1"];
}
-(IBAction)btnTypeFind_clicked:(id)sender
{
    [self.view endEditing:YES];
    [self setupDropDownView:TypeSearch];
}
-(IBAction)btnPage_clicked:(id)sender
{
    [self.view endEditing:YES];
    [self setupDropDownView:Page];
}

#pragma mark - LoadData
-(void)LoadData:(NSString *)agent AgentName:(NSString *)agentname PageNum:(NSString *)pagenum
{
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    [self showMBProcess];
    ShareData *shared = [ShareData instance];
    [shared.supportDeploymentProxy GetListDeploymentProgress:agent AgentName:agentname PageNum:pagenum Completehander:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            return ;
        }
        if(result == nil){
            [self hideMBProcess];
            [self showAlertBoxWithDelayDismiss:@"Thông báo" message:@"Không có dữ liệu trả về"];
//            [self.tableView setHidden:YES];
            [self.navigationController popViewControllerAnimated:YES];
            
        }else {
            [self.tableView setHidden:NO];
            self.arrList = result;
            SupportDeploymentRecord *rc = [result objectAtIndex:0];
            if([rc.CurrentPage isEqualToString:@"1"]||[rc.CurrentPage isEqualToString:@"0"]){
                
                [self LoadPage: [rc.TotalPage intValue]];
            }
            [self.tableView reloadData];
            [self hideMBProcess];
        }
        
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}

#pragma mark - Load Page
-(void)LoadPage:(int) page
{
    totalPage =[NSString stringWithFormat:@"%d",page];
    if (page <2) {
     //   [self.btnPage setEnabled:NO];
        [self.btnPage setTitle:@"1" forState:UIControlStateNormal];
        return;
    }
    [self.btnPage setEnabled:YES];
    self.arrPage = [NSMutableArray array];
    int i = 1;
    for (i = 1; i <= page; i++) {
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",i] description:[NSString stringWithFormat:@"%d",i]];
        [self.arrPage addObject: s];
    }
    
    selectedPage = [self.arrPage objectAtIndex:0];
    NSString*title =[NSString stringWithFormat:@"%@/%d",selectedPage.Values,page];
    [self.btnPage setTitle:title forState:UIControlStateNormal];
}


-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    if(buttonIndex == 1){
        [self showMBProcess];
        ShareData *shared = [ShareData instance];
        [shared.supportDeploymentProxy PushDeployment:shared.currentUser.userName Contract:Contract Completehander:^(id result, NSString *errorCode, NSString *message) {
            [self hideMBProcess];
            if ([message isEqualToString:@"het phien lam viec"]) {
                [self ShowAlertErrorSession];
                [self LogOut];
                return;
            }
            if(![errorCode isEqualToString:@"0"]){
                [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
                return ;
            }
            
            [self showAlertBox:@"Thông Báo" message:@"Gửi mail thành công"];
          
        } errorHandler:^(NSError *error) {
            [self hideMBProcess];
            [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        }];

    }
}


@end
