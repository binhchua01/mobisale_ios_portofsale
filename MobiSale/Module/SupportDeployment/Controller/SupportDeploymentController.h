//
//  SupportDeploymentController.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 1/24/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BaseViewController.h"

@interface SupportDeploymentController : BaseViewController<UITableViewDataSource, UITableViewDelegate , UISearchBarDelegate >

@property (nonatomic , retain) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IBOutlet UIButton *btnFind;
@property (strong, nonatomic) IBOutlet UIButton *btnTypeFind;
@property (strong, nonatomic) IBOutlet UIButton *btnPage;
@property (strong, nonatomic) IBOutlet UISearchBar *txtTextSearch;
@property (strong, nonatomic) UIWindow *window;


-(IBAction)btnTypeFind_clicked:(id)sender;
-(IBAction)btnFind_clicked:(id)sender;
-(IBAction)btnPage_clicked:(id)sender;


@property (strong, nonatomic) NSArray *arrTypeFind;
@property (strong, nonatomic) NSMutableArray *arrList;
@property (strong, nonatomic) NSMutableArray *arrPage;

@end
