//
//  SupportMobiSaleProxy.h
//  MobiSale
//
//  Created by ISC-DanTT on 11/16/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BaseProxy.h"

@interface SupportMobiSaleProxy : BaseProxy
// Đăng ký RegID nhận từ GCM lên Server MobiSale
- (void) registerPushNotificationGCMWithRegistrationID:(NSString *)regID completeHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

// Lấy RegID của Admin
- (void) getRegisteredIDGCMWithAgent:(NSString*)agent agentName:(NSString *)agentName completeHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

// Gửi Notification
- (void) sendNotificationGCM:(NSDictionary *)message toListTarget:(NSArray *)target completeHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
@end
