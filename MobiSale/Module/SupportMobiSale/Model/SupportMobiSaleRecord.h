//
//  SupportMobiSaleRecord.h
//  MobiSale
//
//  Created by ISC-DanTT on 11/16/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SupportMobiSaleRecord : NSObject
@property (nonatomic,retain) NSString *userRegistrationID;
@property (nonatomic,retain) NSArray *arrAdmin;

@end
