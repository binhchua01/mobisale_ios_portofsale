//
//  SupportMobiSaleProxy.m
//  MobiSale
//
//  Created by ISC-DanTT on 11/16/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "SupportMobiSaleProxy.h"
#import "ShareData.h"
#import "SupportMobiSaleRecord.h"
#import "AppDelegate.h"
#import "OpenUDID.h"
#import "FPTUtility.h"

#define GCM_RegisterPushNotification    @"GCM_RegisterPushNotification"
#define GCM_GetRegisteredID             @"GCM_GetRegisteredID"
#define GCM_SendNotification            @"GCM_SendNotification"

@implementation SupportMobiSaleProxy
// Đăng ký RegID nhận từ GCM lên Server MobiSale
- (void) registerPushNotificationGCMWithRegistrationID:(NSString *)regID completeHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler{
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, GCM_RegisterPushNotification)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    NSString *userName = [ShareData instance].currentUser.userName;
    NSString *deviceIMEI = [ShareData instance].currentUser.userDeviceIMEI;
    @try {
        [dict setObject:userName forKey:@"UserName"];
        [dict setObject:deviceIMEI forKey:@"DeviceIMEI"];
        [dict setObject:regID forKey:@"RegID"];
        
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
         [CrashlyticsKit recordCustomExceptionName:@"SupportMobiSaleProxy - funtion (saveReceiveRemoteNotification)-Error handle Parse registerPushNotificationGCMWithRegistrationID" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
    }
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endRegisterPushNotificationGCM:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endRegisterPushNotificationGCM:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    
    @try {
        /*Parse json response*/
        NSDictionary *jsonDict = [self.parser objectWithData:result];
        NSDictionary *responseResult = [jsonDict objectForKey:@"ResponseResult"];
        
        NSArray *listObject = [responseResult objectForKey:@"ListObject"];
        NSString *ErrorService = StringFormat(@"%@",[[listObject objectAtIndex:0] objectForKey:@"ErrorService"]);
        NSString *message = StringFormat(@"%@",[[listObject objectAtIndex:0] objectForKey:@"Result"]);
        
        if(![ErrorService isEqualToString:@"<null>"]){
            handler(nil, ErrorService, message);
            return;
        }
        
        handler(listObject, ErrorService, message);
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
        
        [CrashlyticsKit recordCustomExceptionName:@"SupportMobiSaleProxy - funtion (saveReceiveRemoteNotification)-Error handle Parse registerPushNotificationGCMWithRegistrationID" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
    }
    
}

// Lấy RegID của Admin
- (void) getRegisteredIDGCMWithAgent:(NSString*)agent agentName:(NSString *)agentName completeHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler{
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI,GCM_GetRegisteredID)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    NSString *userName = [ShareData instance].currentUser.userName;
    @try {
        [dict setObject:userName forKey:@"UserName"];
        [dict setObject:agent forKey:@"Agent"];
        [dict setObject:agentName forKey:@"AgentName"];
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
        [CrashlyticsKit recordCustomExceptionName:@"SupportMobiSaleProxy - funtion (saveReceiveRemoteNotification)-Error handle Parse Lấy RegID của Admin" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
    }

    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetRegisteredID:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}

- (void)endGetRegisteredID:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
  
    
    @try {
        /*Parse json response*/
        NSDictionary *jsonDict = [self.parser objectWithData:result];
        NSDictionary *responseResult = [jsonDict objectForKey:@"ResponseResult"];
        
        NSArray *listObject = [responseResult objectForKey:@"ListObject"];
        NSString *errorCode = StringFormat(@"%@",[responseResult objectForKey:@"ErrorCode"]);
        
        if(![errorCode isEqualToString:@"0"]){
            handler(nil, errorCode, @"");
            return;
        }
        if (listObject.count <= 0) {
            handler(nil,errorCode, @"Không có danh sách admin");
            return;
        }
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        SupportMobiSaleRecord *rc = [[SupportMobiSaleRecord alloc] init];
        rc.arrAdmin = listObject;
        rc.userRegistrationID = appDelegate.registrationToken;
        handler(rc, errorCode, @"Get list admin registration ID successed");
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
         [CrashlyticsKit recordCustomExceptionName:@"SupportMobiSaleProxy - funtion (saveReceiveRemoteNotification)-Error handle Get list admin registration ID" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
    }
}

// Gửi Notification
- (void)sendNotificationGCM:(NSDictionary *)message toListTarget:(NSArray *)target completeHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler{
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI,GCM_SendNotification)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:message forKey:@"Message"];
    [dict setObject:target forKey:@"SendToList"];
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
 
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endSendNotificationGCM:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endSendNotificationGCM:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    

    // add by DanTT 2015.10.12
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *responseResult = [jsonDict objectForKey:@"ResponseResult"];
    
    NSArray *listObject = [responseResult objectForKey:@"ListObject"];
    NSString *ErrorService = StringFormat(@"%@",[[listObject objectAtIndex:0] objectForKey:@"ErrorService"]);
    NSString *message = StringFormat(@"%@",[[listObject objectAtIndex:0] objectForKey:@"Result"]);
    
    if(![ErrorService isEqualToString:@"<null>"]){
        handler(nil, ErrorService, message);
        return;
    }
    
    handler(listObject, ErrorService, message);
}

@end
