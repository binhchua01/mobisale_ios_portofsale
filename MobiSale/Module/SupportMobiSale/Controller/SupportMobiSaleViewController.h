//
//  SupportMobiSaleViewController.h
//  MobiSale
//
//  Created by ISC-DanTT on 11/9/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@protocol SupportMobiSaleViewControllerDelegate <NSObject>

-(void)CancelPopup;

@end

@interface SupportMobiSaleViewController : BaseViewController

@property (nonatomic, retain) id <SupportMobiSaleViewControllerDelegate> delegate;

@end
