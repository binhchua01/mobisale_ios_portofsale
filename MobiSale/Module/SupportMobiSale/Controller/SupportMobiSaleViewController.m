//
//  SupportMobiSaleViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 11/9/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "SupportMobiSaleViewController.h"
#import "NIDropDown.h"
#import "ShareData.h"

#define kOFFSET_FOR_KEYBOARD 80.0

@interface SupportMobiSaleViewController ()<NIDropDownDelegate>{
    NIDropDown * dropDown;
    NSArray *arrData;
    BOOL isSelected;
}
@property (strong, nonatomic) IBOutlet UIButton *btnError;
@property (strong, nonatomic) IBOutlet UITextView *txtContent;
@property (strong, nonatomic) IBOutlet UITextField *txtLinkImage;

@end

@implementation SupportMobiSaleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.txtContent.layer.borderColor= [UIColor colorWithRed:0 green:0.173 blue:0.294 alpha:1].CGColor;
    self.txtContent.layer.borderWidth = 1.f;
    self.txtContent.layer.cornerRadius = 6.0f;
    
    arrData = [[NSArray alloc] initWithObjects:@"Lỗi",@"Nâng cấp chức năng",@"Hỗ trợ",@"Khác...", nil];
    UITapGestureRecognizer *tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    //tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender{
    [self.view endEditing:YES];
    if (dropDown !=nil) {
        [dropDown hideDropDown:self.btnError];
        [self rel];
    }
}

- (IBAction)btnCancel_Clicked:(id)sender {
    if(self.delegate){
        [self.delegate CancelPopup];
    }
}

- (IBAction)btnType_Clicked:(id)sender{
    [self.view endEditing:YES];
    if (dropDown == nil) {
        CGFloat height = 161;
        CGFloat width = 0;
        dropDown = [[NIDropDown alloc] showDropDownAtButton:sender height:&height width:&width data:arrData images:nil animation:@"down"];
        dropDown.delegate = self;
        return;
    }
    [dropDown hideDropDown:sender];
    [self rel];
}
- (IBAction)btnSend_Clicked:(id)sender {
    [self.view endEditing:YES];
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    NSString *message = self.txtContent.text;
    if (message.length <= 0) {
        [self showAlertBox:@"Thông báo" message:@"Vui lòng nhập nội dung!"];
        return;
    }
    NSString *linkImage = self.txtLinkImage.text;
    NSString *type = self.btnError.titleLabel.text;
    [self sendNotificationWithMessage:message image:linkImage type:type];
}

- (void) sendNotificationWithMessage: (NSString *)message image:(NSString *)linkImage type:(NSString *)type{
    NSMutableDictionary *dictMessage = [[NSMutableDictionary alloc] init];
    [dictMessage setObject:message forKey:@"message"];
    [dictMessage setObject:linkImage forKey:@"image"];
    [dictMessage setObject:type forKey:@"category"];
    //Get time
    NSDate *senddate=[NSDate date];
    NSDateFormatter *dateformatter=[[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"dd'-'MM'-'yyyy' 'HH':'mm':'ss"];
    NSString *locationString=[dateformatter stringFromDate:senddate];
    [dictMessage setObject:locationString forKey:@"datetime"];
    
    [dictMessage setObject:@"false" forKey:@"isBookmark"];
    [dictMessage setObject:@"false" forKey:@"isDelete"];
    [dictMessage setObject:@"false" forKey:@"isRead"];
    
    ShareData *shared = [ShareData instance];
    [dictMessage setObject:shared.supportMobiSaleRecord.userRegistrationID forKey:@"sendFromRegID"];
    [dictMessage setObject:shared.currentUser.userName forKey:@"sendfrom"];
    [dictMessage setObject:@"HCM.GiauTQ" forKey:@"to"];
   
    NSArray *arrSend = shared.supportMobiSaleRecord.arrAdmin;
    
    [self showMBProcess];
    [shared.supportMobiSaleProxy sendNotificationGCM:dictMessage toListTarget:arrSend completeHander:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        if ([errorCode isEqualToString:@"<null>"]) {
            [self ShowAlertSendSuccessWithMessage:message];
            if (self.delegate) {
                [self.delegate CancelPopup];
            }
            return;
        }
        [self showAlertBox:@"Thông báo" message:message];
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Lỗi" message:[error localizedDescription]];
        [self hideMBProcess];

    }];
 }

#pragma mark - NIDropDown Delegate method
- (void)dropDownListView:(NIDropDown *)dropDownListView didSelectIndexPath:(NSIndexPath *)indexPath atButton:(UIButton *)button{
    
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
}

-(void)rel{
    dropDown = nil;
}

#pragma mark - setShowAndHideKeyboard
-(void)keyboardWillShow {
    // Animate the current view out of the way
    if (isSelected == NO) {
        [self setViewMovedUp:YES];
        isSelected = YES;
    }
    if (dropDown !=nil) {
        [dropDown hideDropDown:self.btnError];
        [self rel];
    }
}

-(void)keyboardWillHide {
    isSelected = NO;
    [self setViewMovedUp:NO];
}


//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp){
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        //rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else{
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        //rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

#pragma mark - alert box
-(void)ShowAlertSendSuccessWithMessage: (NSString *)message{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    [alert show];
    [self performSelector:@selector(dismiss:) withObject:alert afterDelay:3.0];
}

-(void)dismiss:(UIAlertView*)alert
{
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
