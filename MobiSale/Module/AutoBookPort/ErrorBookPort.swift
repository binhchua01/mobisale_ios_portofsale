//
//  ErrorBookPort.swift
//  MobiSale
//
//  Created by Nguyen Sang on 11/16/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

import Foundation

enum ErrorBookPort:Int {
    case success            = 0         // Success
    case portExists         = 1         // Port Exist
    case inputNotFound      = 10        // error input data
    case INFPortTimeOut     = 20        // error time out when request API INFPort
    case INFPortNotFound    = 25        // error request API INFPort not exist URL or error INFPort
    case INFMapTimeOut      = 30        // error time out when request APi INFMap
    case INFMapNotFound     = 35        // error request API INFMap not exist URL or error INFMap
    case privateService     = -100      // error private service
}

enum ErrorAutoBookPort {
    case parseJSON
    case chooseLocation
    case outRangeRadius(radius:Int)
    case notGPS
    case notAutoBookPort(strErrorApi:String, strStatusCode:String)
    case notFindAddressCustomer
    case portExists
    case bookportManual(strErrorApi:String, strStatusCode:String)
    case bookportRetry(strErrorApi:String, strStatusCode:String)
    case endRetryToManual
    //Status Ngoai 200
    case outRangeStatusSuccess(statusCode:Int)
    case unknown
}

extension ErrorAutoBookPort {
    
    var stringDecrip:String {
        
        switch self {
        case .portExists:
            return "PĐK đã book port trước đó, yêu cầu thu hồi mới đc book port mới"
        case .parseJSON:
            return "Lỗi Parse JSON"
        case .chooseLocation:
            return "Vui lòng chọn vị trí cần BookPort"
        case let .outRangeRadius(radius):
            return "Chỉ được book port trong phạm vi bán kính \(radius)m"
        case .notGPS:
            return "Thông báo bật GPS"
        case let .notAutoBookPort(strErrorApi, strStatusCode):
//            return "MessageFromServer:\(strErrorApi)\nStatusCode:\(strStatusCode)\nKhông thể BookPort Tự động"
            return "Không thể BookPort Tự động"
        case .notFindAddressCustomer:
            return "Địa chỉ khách hàng không có hoặc không lấy được vị trí nhà khách hàng"
        case let .bookportManual(strErrorApi, strStatusCode):
//            return "\(strErrorApi)\n\(strStatusCode)\nBook port không thành công. Vui lòng book port bằng tay"
            return "Book port không thành công. Vui lòng book port bằng tay"
        case let .bookportRetry(strErrorApi, strStatusCode):
//            return "\(strErrorApi)\n\(strStatusCode)\nBook port không thành công. Vui lòng thử lại"
            return "Book port không thành công. Vui lòng thử lại"
        case .endRetryToManual:
            return "Đã hết số lần thử lại vui lòng bookport bằng tay"
        case let .outRangeStatusSuccess(statusCode):
            return "Lỗi. StatusCode: \(statusCode)"
        case .unknown:
            return "Unknown"
        default:
            return "UnKnown Default"
        }
        
    }
    
}
