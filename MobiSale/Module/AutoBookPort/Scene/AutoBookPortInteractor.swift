//
//  AutoBookPortInteractor.swift
//  MobiSale
//
//  Created by Nguyen Sang on 11/9/17.
//  Copyright (c) 2017 FPT.RAD.FTool. All rights reserved.
//
//  This file was generated by the Clean Swift HELM Xcode Templates
//  https://github.com/HelmMobile/clean-swift-templates

protocol AutoBookPortInteractorInput {
    
    func loadShareData()
    
    func saveLocationDevice(location:CLLocation?)
    
    func saveLocationRequest(location: CLLocation?)
    
    func saveLocationWhenDrag(location:CLLocation)
    
    func saveTypeOfPort(keyValModel:KeyValueModel)
    
    func handleDistanceLocation()
    
    func requestAutoPortBook(viewModel:AutoBookPortScene.autoBookPort.Request,completion:@escaping (_ ODCCable:String, _ latLng:String, _ mesg:String) -> Void, completionError:@escaping (_ error:String) -> Void, completionErrorManual:@escaping (_ error:String) -> Void)
    
    func requestRecoveryPort()
    
}

protocol AutoBookPortInteractorOutput {
    
    func presentAlertWith(title:String, message:String)
    
    func presentAutoBookPort(viewModel:AutoBookPortScene.getInfoBookPort.ViewModel)
    
    //true là địa chỉ khách hàng, ngược lại là địa chỉ hiện tại
    func presentLocationAuto(isRequest:Bool,viewModelLocation:AutoBookPortScene.getInfoBookPort.ViewModelLocation?)
    
    func presentKeyValueChoose(keyV:KeyValueModel)
    
    func presentIsShowLoading(isShow:Bool)
    
    func presentResultWhenBookPort(eResult:TypeResultBP, response:AutoBookPortScene.autoBookPort.Response)
    
    func presentAlertWith(error:ErrorAutoBookPort)
    
}

protocol AutoBookPortDataSource {
    
    
    
}

protocol AutoBookPortDataDestination {
    
    var registrationFromDetailRecord:RegistrationFormDetailRecord! { set get }
    
    var txtLocation:String? { set get }
    
}

class AutoBookPortInteractor: AutoBookPortInteractorInput, AutoBookPortDataSource, AutoBookPortDataDestination {
    
    var output: AutoBookPortInteractorOutput?
    var worker:AutoBookPortWorker?
    
    
    var modelViewBookPort:AutoBookPortScene.getInfoBookPort.ViewModel?
    
    var registrationFromDetailRecord:RegistrationFormDetailRecord!
    var txtLocation: String?
    
    var locationRequest:AutoBookPortScene.getInfoBookPort.ViewModelLocation?
    var locationEndDrag:AutoBookPortScene.getInfoBookPort.ViewModelLocation?
    var locationCurrentDevice: CLLocation?
    
    var typePortOfSale:KeyValueModel?
    
    var numberRetry:Int = 5
    
    var numberRetryGoogleMap:Int = 3

}

// MARK: Business logic
extension AutoBookPortInteractor: AutoBookPortViewControllerOutput, AutoBookPortRouterDataSource, AutoBookPortRouterDataDestination {
    
    func loadShareData() {
        
        self.getLocationFromAddress()
        
        let autoBP = ShareData.instance().currentUser.autoBookPort
        let autoBPRetryConnect = ShareData.instance().currentUser.autoBookPort_RetryConnect
        let autoBPTimeOut = ShareData.instance().currentUser.autoBookPort_Timeout
        let autoBPIR = ShareData.instance().currentUser.autoBookPort_iR
        let arrType = ShareData.instance().currentUser.arrTypePortOfSale
        
        let isHiddenRecovery = checkHiddenRecoveryPort(strLocation: txtLocation ?? "")
        
        var arrKeyValue:[KeyValueModel] = []
        
        for typePort in arrType! {
            
            let dicTypePort = typePort as! NSDictionary
            
            let nameModel = dicTypePort["Id"] as? Int
            let valueModel = dicTypePort["Name"] as? String
            
            let keyValueModel = KeyValueModel(name: "\(nameModel ?? 0)", description: valueModel ?? "")
            
            arrKeyValue.append(keyValueModel!)
            
        }
        
        let keyValueChoose = self.checkRecoveryPort(arrType: arrKeyValue)
//        if !isHiddenRecovery {
//            self.output.
//        }
        
        let modelView = AutoBookPortScene.getInfoBookPort.ViewModel(autoBookPort: autoBP, autoBookPortRetryConnect: autoBPRetryConnect, autoBookPortTimeOut: autoBPTimeOut, autoBookPortIR: autoBPIR, arrTypePortOfSale: arrKeyValue, isHiddenRecoveryPort: isHiddenRecovery)
        
        modelViewBookPort = modelView
        output?.presentAutoBookPort(viewModel: modelView)
        output?.presentKeyValueChoose(keyV: keyValueChoose ?? KeyValueModel(name: "", description: ""))
        
        
    }
    
    func handleDistanceLocation() {
        
        guard let location1:CLLocation = locationRequest?.location else { //Alert Error
            self.output?.presentAlertWith(error: .chooseLocation)
            return
        }
        
        if let location2:CLLocation = locationEndDrag?.location {//have Drag Marker
            
            let target = location1.distance(from: location2)
            
            if target <= Double((modelViewBookPort?.autoBookPortIR) ?? 0) {//Ok
                
                self.handleRequestAutoBookPort(location: locationEndDrag!)
                
            } else {//Alert Error
                
                self.output?.presentAlertWith(error: .outRangeRadius(radius: (modelViewBookPort?.autoBookPortIR) ?? 0 ))
                
            }
            
        } else {//have not Drag Marker
            
            self.handleRequestAutoBookPort(location: locationRequest!)
            
        }
        
    }
    
    func handleRequestAutoBookPort(location:AutoBookPortScene.getInfoBookPort.ViewModelLocation) {
        
        //check have location device
        if locationCurrentDevice == nil {//have not Location Device, show alert
            self.output?.presentAlertWith(error: .notGPS)
            return
        }
        
        //Check NumeberRetry
        self.numberRetry = self.numberRetry - 1
        if self.numberRetry < 0 {
            let response = AutoBookPortScene.autoBookPort.Response(ODCCable: "", latLng: "", error: ErrorAutoBookPort.endRetryToManual.stringDecrip)
            self.output?.presentResultWhenBookPort(eResult: .manual, response: response)
            return
        }
        
        //Create Param
        let userName = ShareData.instance().currentUser.userName
        let regCode = self.registrationFromDetailRecord.regCode
        let type = Int((self.typePortOfSale?.key) ?? "0")
        let myLocation = "\(self.locationRequest?.location.coordinate.latitude ?? 0.0),\(self.locationRequest?.location.coordinate.longitude ?? 0.0)"
        let deviceLocation = "\(self.locationCurrentDevice?.coordinate.latitude ?? 0.0),\(self.locationCurrentDevice?.coordinate.longitude ?? 0.0)"
        let modelView = AutoBookPortScene.autoBookPort.Request(userName: userName ?? "", regCode: regCode ?? "0", type: type ?? 0, myLocation: myLocation, deviceLocation: deviceLocation, timeOut: ShareData.instance().currentUser.autoBookPort_Timeout)
        
        self.requestAutoPortBook(viewModel: modelView, completion: { (ODCCable, latLng, strMesg) in
            
            let response = AutoBookPortScene.autoBookPort.Response(ODCCable: ODCCable, latLng: latLng, error: strMesg)
            self.output?.presentResultWhenBookPort(eResult: .success, response: response)
            
        }, completionError: { (strError) in // Error Have Retry
            
            if self.numberRetry <= 0 {
                let response = AutoBookPortScene.autoBookPort.Response(ODCCable: "", latLng: "", error: ErrorAutoBookPort.endRetryToManual.stringDecrip)
                self.output?.presentResultWhenBookPort(eResult: .manual, response: response)
            } else {
                let response = AutoBookPortScene.autoBookPort.Response(ODCCable: "", latLng: "", error: strError)
                self.output?.presentResultWhenBookPort(eResult: .retry, response: response)
            }
            
        }) { (strError) in //Error To Manual
            
            let response = AutoBookPortScene.autoBookPort.Response(ODCCable: "", latLng: "", error: strError)
            self.output?.presentResultWhenBookPort(eResult: .manual, response: response)
            
        }
        
    }
    
}

//MARK: Request API
extension AutoBookPortInteractor {
    
    func getLocationFromAddress() {
        
        numberRetryGoogleMap = numberRetryGoogleMap - 1
        if numberRetryGoogleMap < 0 {
            self.output?.presentLocationAuto(isRequest: true, viewModelLocation: nil)
            return
        }
        
        let strAddress = registrationFromDetailRecord.addressOrigin
        
        worker = AutoBookPortWorker()
        self.output?.presentIsShowLoading(isShow: true)
        worker?.getLocationFromAddressUser(strAddress: strAddress ?? "", completion: { (lat, lng) in
            
            DispatchQueue.main.async {
                
                self.output?.presentIsShowLoading(isShow: false)
                let loca = CLLocation(latitude: lat, longitude: lng)
                let viewModel = AutoBookPortScene.getInfoBookPort.ViewModelLocation(title: strAddress ?? "", location: loca)
                
                //save location
                self.locationRequest = viewModel
                
                //output
                self.output?.presentLocationAuto(isRequest: true, viewModelLocation: viewModel)
                
            }
            
            
        }, completionError: {
            
            DispatchQueue.main.async {
                
                self.output?.presentIsShowLoading(isShow: false)
                self.output?.presentLocationAuto(isRequest: true, viewModelLocation: nil)
                
            }
            
        }, completionRetry: {
            
            DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(1), execute: {
                self.output?.presentIsShowLoading(isShow: false)
                self.getLocationFromAddress()
            })
            
        })
        
    }
    
    func requestAutoPortBook(viewModel:AutoBookPortScene.autoBookPort.Request, completion:@escaping (_ ODCCable:String, _ latLng:String, _ mesg:String) -> Void, completionError:@escaping (_ error:String) -> Void, completionErrorManual:@escaping (_ error:String) -> Void) {
        
        
        worker = AutoBookPortWorker()
        self.output?.presentIsShowLoading(isShow: true)
        worker?.postAutoBookPort2(param: viewModel, completion: { (dataJSON, res) in
            //Success
            self.output?.presentIsShowLoading(isShow: false)
            
            if let statusCode = res as? HTTPURLResponse {
                
                if statusCode.statusCode == 200 { //StatusCode 200
                    
                    if let dataJSON:NSDictionary = dataJSON as? NSDictionary {
                        
                        guard let arrDicJSON:NSDictionary = dataJSON["AutoBookPortResult"] as? NSDictionary else {
                            completionError(ErrorAutoBookPort.parseJSON.stringDecrip)
                            return
                        }
                        
                        guard let isManual:Bool = arrDicJSON["isManual"] as? Bool else {
                            completionError(ErrorAutoBookPort.parseJSON.stringDecrip)
                            return
                        }
                        
                        guard let errorCode:Int = arrDicJSON["ErrorCode"] as? Int else {
                            completionError(ErrorAutoBookPort.parseJSON.stringDecrip)
                            return
                        }
                        
                        if isManual {
                            
                            let error = arrDicJSON["ErrorMessage"] as? String
                            
                            let strError = ErrorAutoBookPort.notAutoBookPort(strErrorApi: error ?? "", strStatusCode: "\(errorCode)").stringDecrip
                            
                            completionErrorManual(strError)
                            return
                        }
                        
                        switch errorCode {
                            
                        case ErrorBookPort.success.rawValue://Success
                            guard let dicResult:NSDictionary = arrDicJSON["Result"] as? NSDictionary else { return }
                            let ODCCable = dicResult["ODCCable"] as? String
                            let latLng = dicResult["latLng"] as? String
                            completion(ODCCable ?? "", latLng ?? "", "BookPort Thành công")
                            break
                        case ErrorBookPort.portExists.rawValue://Port Exists
                            guard let dicResult:NSDictionary = arrDicJSON["Result"] as? NSDictionary else { return }
                            let ODCCable = dicResult["ODCCable"] as? String
                            let latLng = dicResult["latLng"] as? String
                            completion(ODCCable ?? "", latLng ?? "", ErrorAutoBookPort.portExists.stringDecrip)
                            break
                        case ErrorBookPort.inputNotFound.rawValue://Retry
                            let error = arrDicJSON["ErrorMessage"] as? String
                            
                            let strError = ErrorAutoBookPort.bookportRetry(strErrorApi: error ?? "", strStatusCode: "\(ErrorBookPort.inputNotFound.rawValue)").stringDecrip
                            
                            completionError(strError)
                            break
                        case ErrorBookPort.INFPortTimeOut.rawValue://Retry
                            let error = arrDicJSON["ErrorMessage"] as? String
                            let strError = ErrorAutoBookPort.bookportRetry(strErrorApi: error ?? "", strStatusCode: "\(ErrorBookPort.INFPortTimeOut.rawValue)").stringDecrip
                            completionError(strError )
                            break
                        case ErrorBookPort.INFPortNotFound.rawValue://Manual
                            let error = arrDicJSON["ErrorMessage"] as? String
                            let strError = ErrorAutoBookPort.bookportManual(strErrorApi: error ?? "", strStatusCode: "\(ErrorBookPort.INFPortNotFound.rawValue)").stringDecrip
                            completionErrorManual(strError )
                            break
                        case ErrorBookPort.INFMapTimeOut.rawValue://Retry
                            let error = arrDicJSON["ErrorMessage"] as? String
                            let strError = ErrorAutoBookPort.bookportRetry(strErrorApi: error ?? "", strStatusCode: "\(ErrorBookPort.INFMapTimeOut.rawValue)").stringDecrip
                            completionError(strError )
                            break
                        case ErrorBookPort.INFMapNotFound.rawValue://Manual
                            let error = arrDicJSON["ErrorMessage"] as? String
                            let strError = ErrorAutoBookPort.bookportManual(strErrorApi: error ?? "", strStatusCode: "\(ErrorBookPort.INFMapNotFound.hashValue)").stringDecrip
                            completionErrorManual(strError )
                            break
                        case ErrorBookPort.privateService.rawValue://Manual
                            let error = arrDicJSON["ErrorMessage"] as? String
                            let strError = ErrorAutoBookPort.bookportManual(strErrorApi: error ?? "", strStatusCode: "\(ErrorBookPort.privateService.rawValue)").stringDecrip
                            completionErrorManual(strError)
                            break
                        default://Manual
                            let error = arrDicJSON["ErrorMessage"] as? String
                            let strError = ErrorAutoBookPort.bookportManual(strErrorApi: error ?? "", strStatusCode: "\(errorCode)").stringDecrip
                            completionErrorManual(strError )
                            break
                        }
                        
                    } else {
                        let strError = ErrorAutoBookPort.parseJSON.stringDecrip
                        completionError(strError)
                    }
                    
                } else {//StatusCode Error
                    let strError = ErrorAutoBookPort.outRangeStatusSuccess(statusCode: statusCode.statusCode).stringDecrip
                    completionError(strError)
                }
                
            } else {
                let strError = ErrorAutoBookPort.unknown.stringDecrip
                completionError(strError)
            }
            
        }, completionError: { (error) in
            //Error
            let strError = error?.localizedDescription
            completionError(strError ?? "")
            self.output?.presentIsShowLoading(isShow: false)
            
        })
        
    }
    
    func requestRecoveryPort() {
        
        var type = Int((self.typePortOfSale?.key) ?? "0")
        
        let regCode = self.registrationFromDetailRecord.regCode
        
        let typePort = self.registrationFromDetailRecord.bookPortType
        print("TypeBookPort:\(typePort ?? "0")")
        
        let share:ShareData = ShareData.instance()
        self.output?.presentIsShowLoading(isShow: true)
        share.mapproxy.recoverRegistration("\((type ?? 0)+1)", regCode: regCode, createBy: share.currentUser.userName, ip: "", completehander: { (result, error, message) in
            
            self.output?.presentIsShowLoading(isShow: false)
            self.output?.presentAlertWith(title: "", message: message ?? "")
            
        }) { (error) in
            
            self.output?.presentIsShowLoading(isShow: false)
            self.output?.presentAlertWith(title: "", message: (error?.localizedDescription) ?? "")
            
        }
        
    }
    
}

// MARK: Save Data
extension AutoBookPortInteractor {
    
    func saveLocationDevice(location: CLLocation?) {
        
        self.locationCurrentDevice = location
        
    }
    
    func saveTypeOfPort(keyValModel:KeyValueModel) {
        
        self.typePortOfSale = keyValModel
        self.output?.presentKeyValueChoose(keyV: keyValModel)
        
    }
    
    func saveLocationRequest(location: CLLocation?) {
        
        //clear Location when Drag GoogleMaps
        self.locationEndDrag = nil
        
        if location != nil {
            let viewModel = AutoBookPortScene.getInfoBookPort.ViewModelLocation(title: "", location: location!)
            
            //save Data
            self.locationRequest = viewModel
            
            //output present Location
            self.output?.presentLocationAuto(isRequest: false, viewModelLocation: viewModel)

        } else {
            
            //save Data
            self.locationRequest = nil
            
        }
        
        
    }
    
    func saveLocationWhenDrag(location:CLLocation) {
        
        let viewModel = AutoBookPortScene.getInfoBookPort.ViewModelLocation(title: "", location: location)
        
        self.locationEndDrag = viewModel
        
    }
    
}

//MARK: -Method sử lý các func chia nhỏ
extension AutoBookPortInteractor {
    
    func checkHiddenRecoveryPort(strLocation:String) -> Bool {
        
        if strLocation.isEmpty {
            return true
        } else if strLocation.characters.count > 2 {
            return false
        }
        
        return true
        
    }
    
    func checkRecoveryPort(arrType:[KeyValueModel]) -> KeyValueModel? {
        
        let tdName = self.registrationFromDetailRecord.tdName
        let bookportType = self.registrationFromDetailRecord.bookPortType
        
        //check ArrType ...
        if arrType.count <= 0 {
            let key = KeyValueModel(name: "", description: "")
            self.typePortOfSale = key
            return key
        }
        
        if (tdName?.isEmpty) ?? true {
            
            //Chưa BookPort
            let keyValueChoose = self.findFTTHNewFromArray(arrKeyModel: arrType)
            self.typePortOfSale = keyValueChoose
            return keyValueChoose!
            
        } else {
            //Da co BookPort
            if let td = tdName?.characters.split(separator: "/") {
                
                if td.count >= 2 {//FTTHNew
                    let keyValueChoose = self.findFTTHNewFromArray(arrKeyModel: arrType)
                    self.typePortOfSale = keyValueChoose
                    return keyValueChoose!
                } else {//Con Lai
                    
                    var key:KeyValueModel!
                    
                    if bookportType == "0" {//ADSL
                        key = KeyValueModel(name: "0", description: "ADSL")
                    } else {//FTTH
                        key = KeyValueModel(name: "1", description: "FTTH")
                    }
                    
                    let arrFilter:[KeyValueModel] = arrType.filter({ (keyValue) -> Bool in
                        return keyValue.key == key.key
                    })
                    
                    if arrFilter.count > 0 {
                        self.typePortOfSale = key
                    } else {
                        self.typePortOfSale = KeyValueModel(name: "2", description: "FTTHNew")
                    }
                    
                    
                    return self.typePortOfSale
                    
                }
                
            } else {
                return nil
            }
            
        }
        
        return nil
        
    }
    
    //
    func findFTTHNewFromArray(arrKeyModel:[KeyValueModel]) -> KeyValueModel? {
        
        let arrResult = arrKeyModel.filter { (model) -> Bool in
            return model.key == "2"
        }
        
        if arrResult.count > 0 {
            return arrResult.first
        }
        
        return arrKeyModel.first
        
    }
    
}









