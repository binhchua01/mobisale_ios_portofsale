//
//  SBIRecord.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 4/9/15.
//  Copyright (c) 2015 FPT.RAD.MobiSale. All rights reserved.
//

#import "SBIRecord.h"

@implementation SBIRecord

@synthesize ID;
@synthesize SBI;
@synthesize Status;
@synthesize StatusName;
@synthesize Type;
@synthesize TypeName;

@end
