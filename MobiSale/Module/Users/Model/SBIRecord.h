//
//  SBIRecord.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 4/9/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SBIRecord : NSObject

@property (nonatomic, strong) NSString *ID;
@property (nonatomic, strong) NSString *SBI;
@property (nonatomic, strong) NSString *Status;
@property (nonatomic, strong) NSString *StatusName;
@property (nonatomic, strong) NSString *Type;
@property (nonatomic, strong) NSString *TypeName;

@end
