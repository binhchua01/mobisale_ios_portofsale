//
//  ChangePassViewController.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 4/13/15.
//  Copyright (c) 2015 FPT.RAD.MOBISALE. All rights reserved.
//

#import "BaseViewController.h"

@protocol ChangePassDelegate <NSObject>

-(void)CancelPopupChangePass;

@end

@interface ChangePassViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UITextField *txtOldPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;

@property(retain, nonatomic) id<ChangePassDelegate> delegateChange;

-(IBAction)btnCancel_Clicked:(id)sender;
-(IBAction)btnUpdate_Clicked:(id)sender;

@end
