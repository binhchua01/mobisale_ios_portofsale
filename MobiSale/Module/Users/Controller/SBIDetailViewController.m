//
//  SBIDetailViewController.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 4/9/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "SBIDetailViewController.h"
#import "SBIDetailCell.h"
#import "ShareData.h"
#import "SBIRecord.h"
#import "Common_client.h"

@interface SBIDetailViewController ()

@end

@implementation SBIDetailViewController
@dynamic tableView;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.tableView registerNib:[UINib nibWithNibName:@"TableViewCell" bundle:nil] forCellReuseIdentifier:@"TableViewCellReuse"];
    
    [self LoadData];
    self.tableView.separatorColor = [UIColor clearColor];
    switch ([self.Status intValue]) {
        case 0:
            self.lblTitle.text = @"SBI HẾT HẠN SỬ DỤNG";
            break;
        case 1:
            self.lblTitle.text = @"SBI CÒN HẠN SỬ DỤNG";
            break;
        case 2:
            self.lblTitle.text = @"SBI ĐÃ SỬ DỤNG";
            break;
        case 3:
            self.lblTitle.text = @"SBI ĐÃ HUỶ";
            break;
        default:
            break;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.arrList.count > 0 )
        return self.arrList.count;
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 36;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
   // static NSString *simpleTableIdentifier = @"SBIDetailCellInden";
    TableViewCell *cell = (TableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"TableViewCellReuse"];
    if(!cell){
      //  NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SBIDetailCell" owner:self options:nil];
    //cell = [nib objectAtIndex:0];
    }
    // cell.backgroundColor = [UIColor   redColor];
    
    if(self.arrList.count > 0){
        SBIRecord *record = [self.arrList objectAtIndex:indexPath.row];
        cell.lblID.text = record.ID;
        cell.lblTypeName.text = record.TypeName;
        [cell.btnRecovery setHidden:YES];
//        if([self.Status isEqualToString:@"1"]){
//            [cell.btnRecovery addTarget:self action:@selector(btnRecovery_touch:) forControlEvents:UIControlEventTouchUpInside];
//            cell.btnRecovery.tag = indexPath.row;
//        }else {
//            [cell.btnRecovery setHidden:YES];
//        }
    }
    return cell;
}

#pragma mark Table DidSelected
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    return;
}

#pragma mark - event button

-(IBAction)btnCancel:(id)sender{
    if(self.delegate){
        [self.delegate CancelPopup];
    }
}

#pragma mark - load data
-(void)LoadData
{
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    [self showMBProcess];
    ShareData *shared = [ShareData instance];
    [shared.appProxy GetSBIDetail:shared.currentUser.userName RegCode:@"" Status:self.Status completionHandler:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            return ;
        }
        self.arrList = result;
        if(self.arrList.count > 0){
            [self.tableView reloadData];
        }else {
            [self showAlertBox:@"Thông Báo" message:Mesage_DataEmpty];
        }
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];

        [self hideMBProcess];
    }];
}

-(IBAction)btnRecovery_touch:(id)sender{
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    UIButton *btn  = (UIButton *)sender;
    int row = (int) btn.tag;
     SBIRecord *record = [self.arrList objectAtIndex:row];
    [self showMBProcess];
    ShareData *shared = [ShareData instance];
    [shared.appProxy RequestToCancelSBI:shared.currentUser.userName SBI:record.SBI completionHandler:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
             [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            return ;
        }
        
        [self showAlertBox:@"Thông Báo" message:@"Huỷ SBI thành công"];

    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
