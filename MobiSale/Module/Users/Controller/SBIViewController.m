//
//  SBIViewController.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 4/9/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "SBIViewController.h"

@interface SBIViewController ()

@end

@implementation SBIViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.btnSBIAble setTitle:[self.dic objectForKey:@"SBIAble"] forState:UIControlStateNormal];
    [self.btnSBIUnAble setTitle:[self.dic objectForKey:@"SBIUnAble"] forState:UIControlStateNormal];
    [self.btnSBIDelete setTitle:[self.dic objectForKey:@"SBIDelete"] forState:UIControlStateNormal];
    [self.btnSBIUsed setTitle:[self.dic objectForKey:@"SBIUsed"] forState:UIControlStateNormal];
    int total = (int) [[self.dic objectForKey:@"SBIAble"] integerValue] + (int)[[self.dic objectForKey:@"SBIUnAble"] integerValue] + (int)[[self.dic objectForKey:@"SBIDelete"] integerValue] + (int)[[self.dic objectForKey:@"SBIUsed"] integerValue];
    self.lblTotal.text = [NSString stringWithFormat:@"%d", total];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - event button
-(IBAction)btnSBIAble_Clicked:(id)sender{
    SBIDetailViewController *vc = [[SBIDetailViewController alloc]initWithNibName:@"SBIDetailViewController" bundle:nil];
    vc.delegate = (id)self;
    vc.Status = @"1";
    [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomTop];
}

-(IBAction)btnSBIUnAble_Clicked:(id)sender{
    SBIDetailViewController *vc = [[SBIDetailViewController alloc]initWithNibName:@"SBIDetailViewController" bundle:nil];
    vc.delegate = (id)self;
    vc.Status = @"0";
    [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomTop];
}

-(IBAction)btnSBIDelete_Clicked:(id)sender{
    SBIDetailViewController *vc = [[SBIDetailViewController alloc]initWithNibName:@"SBIDetailViewController" bundle:nil];
    vc.delegate = (id)self;
    vc.Status = @"3";
    [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomTop];
}

-(IBAction)btnSBIUsed_Clicked:(id)sender{
    SBIDetailViewController *vc = [[SBIDetailViewController alloc]initWithNibName:@"SBIDetailViewController" bundle:nil];
    vc.delegate = (id)self;
    vc.Status = @"2";
    [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomTop];
}

#pragma mark - event button
-(IBAction)btnCancel:(id)sender{
    if(self.delegate){
        [self.delegate CancelPopup];
    }
}

#pragma mark - delegate
-(void)CancelPopup{
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
