//
//  SBIViewController.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 4/9/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBIDetailViewController.h"
#import "../../../Helper/MJPopup/UIViewController+MJPopupViewController.h"

@protocol SBIDelegate <NSObject>

-(void)CancelPopup;

@end

@interface SBIViewController : UIViewController<SBIDetailDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblTotal;
@property (weak, nonatomic) IBOutlet UIButton *btnSBIAble;
@property (weak, nonatomic) IBOutlet UIButton *btnSBIUnAble;
@property (weak, nonatomic) IBOutlet UIButton *btnSBIDelete;
@property (weak, nonatomic) IBOutlet UIButton *btnSBIUsed;

@property (retain, nonatomic) id<SBIDelegate> delegate;

@property (strong, nonatomic) NSDictionary *dic;

-(IBAction)btnCancel:(id)sender;
-(IBAction)btnSBIAble_Clicked:(id)sender;
-(IBAction)btnSBIUnAble_Clicked:(id)sender;
-(IBAction)btnSBIDelete_Clicked:(id)sender;
-(IBAction)btnSBIUsed_Clicked:(id)sender;


@end
