//
//  SBIDetailViewController.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 4/9/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "TableViewCell.h"


@protocol SBIDetailDelegate <NSObject>

-(void)CancelPopup;

@end

@interface SBIDetailViewController : BaseViewController<UITabBarDelegate,UITableViewDataSource>

@property (nonatomic , strong) IBOutlet UITableView *tableView;
@property (nonatomic , strong) IBOutlet UILabel *lblTitle;


@property (nonatomic , strong) NSMutableArray *arrList;
@property (nonatomic , strong) NSString *Status;

@property (retain, nonatomic) id<SBIDetailDelegate> delegate;

-(IBAction)btnCancel:(id)sender;
@end
