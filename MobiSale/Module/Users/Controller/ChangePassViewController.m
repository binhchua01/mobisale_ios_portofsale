//
//  ChangePassViewController.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 4/13/15.
//  Copyright (c) 2015 FPT.RAD.MOBISALE. All rights reserved.
//

#import "ChangePassViewController.h"
#import "ShareData.h"
#import "Common_client.h"

@interface ChangePassViewController ()

@end

@implementation ChangePassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"ĐỔI MẬT KHẨU";
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}
-(void)viewDidAppear:(BOOL)animated{
    self.view.frame = CGRectMake(0, 30, self.view.frame.size.width, [SiUtils getScreenFrameSize].height);
}

-(IBAction)btnCancel_Clicked:(id)sender{
    if(self.delegateChange){
        [self.delegateChange CancelPopupChangePass];
    }
}
-(IBAction)btnUpdate_Clicked:(id)sender{
    [self showMBProcess];
    if(![self.txtNewPassword.text isEqualToString:self.txtConfirmPassword.text]){
        [self showAlertBox:@"Thông Báo" message:@"Mật khẩu xác nhận không đúng"];
        [self hideMBProcess];
        return;
    }
    ShareData *shared = [ShareData instance];
    [shared.userProxy ResetPassword:shared.currentUser.userName password:self.txtOldPassword.text NewPassword:self.txtNewPassword.text completeHandler:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi mã %@ -%@",errorCode,message]];
            return ;
        }
     
        [self showAlertBox:@"Thông Báo" message:@"Đổi mật khẩu thành công"];
       
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
