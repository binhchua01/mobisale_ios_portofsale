//
//  PromotionProgramRecord.m
//  MobiSale
//
//  Created by ISC-DanTT on 3/24/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "PromotionProgramRecord.h"

@implementation PromotionProgramRecord

@synthesize mediaUrl, thumbnail, title, type;

@end
