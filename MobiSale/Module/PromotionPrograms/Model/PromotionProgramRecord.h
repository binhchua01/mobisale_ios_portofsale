//
//  PromotionProgramRecord.h
//  MobiSale
//
//  Created by ISC-DanTT on 3/24/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PromotionProgramRecord : NSObject

@property (copy, nonatomic) NSString *mediaUrl;

@property (copy, nonatomic) NSString *thumbnail;

@property (copy, nonatomic) NSString *title;

@property (copy, nonatomic) NSString *type;

@property (copy, nonatomic) NSString *ID;

@end
