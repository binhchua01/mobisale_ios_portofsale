//
//  PromotionProgramsProxy.m
//  MobiSale
//
//  Created by ISC-DanTT on 3/24/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "PromotionProgramsProxy.h"
#import "PromotionProgramRecord.h"
#import "ShareData.h"
#import "FPTUtility.h"

#define mGetBrochure          @"GetBrochure"
#define mGetBrochureDetail    @"GetBrochureDetail"
#define mGetImage             @"GetImage"

@implementation PromotionProgramsProxy

- (void)getBrochure:(NSDictionary *)dict completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetBrochure)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetBrochure:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endGetBrochure:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
//    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(dict == nil){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]?:nil);
    NSString *Error = StringFormat(@"%@",[dict objectForKey:@"Error"]?:nil);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, Error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr) {
        PromotionProgramRecord *rc = [self parePromotionProgramRecord:d];
        [mArray addObject:rc];
    }
    
    handler(mArray, ErrorCode, @"");
}

- (PromotionProgramRecord *)parePromotionProgramRecord:(NSDictionary *)dict {
    PromotionProgramRecord *rc = [PromotionProgramRecord new];
    rc.mediaUrl  = [dict objectForKey:@"MediaUrl"];
    rc.thumbnail = [dict objectForKey:@"Thumbnail"];
    rc.title     = [dict objectForKey:@"Title"];
    rc.type      = [dict objectForKey:@"Type"];
    rc.ID        = [dict objectForKey:@"ID"];
    
    return rc;
}

- (void)getBrochureDetail:(NSDictionary *)dict completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetBrochureDetail)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetBrochureDetail:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endGetBrochureDetail:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    //
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(dict == nil){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]?:nil);
    NSString *Error = StringFormat(@"%@",[dict objectForKey:@"Error"]?:nil);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, Error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr) {
        PromotionProgramRecord *rc = [self parePromotionProgramRecord:d];
        [mArray addObject:rc];
    }
    
    handler(mArray, ErrorCode, @"");
}


- (void)getImage:(NSDictionary *)dict completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPIIMAGE, mGetImage)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    //urlimage
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetImage:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endGetImage:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(dict == nil){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]?:nil);
    NSString *Error = StringFormat(@"%@",[dict objectForKey:@"Error"]?:nil);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, Error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    
    NSDictionary *data = [arr objectAtIndex:0];
    
    handler(data, ErrorCode, @"");
}

@end
