//
//  ImageVideoCell.m
//  MobiSale
//
//  Created by ISC-DanTT on 3/24/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ImageVideoCell.h"
#import "ShareData.h"
#import "PromotionProgramRecord.h"

@implementation ImageVideoCell

@synthesize viewType;

- (void)awakeFromNib {
    [super awakeFromNib];

}

- (void)drawRect:(CGRect)rect {
    self.mView.layer.cornerRadius = 6.0;
    self.mView.layer.masksToBounds = YES;
    
}

- (void)setRecord:(PromotionProgramRecord *)record {
    self.title.text = record.title;

    
    if (viewType == imageView) {
        self.activityIndicator.hidden = NO;
        [self.activityIndicator startAnimating];
        self.playImage.hidden = YES;
        [self getImageWithUrl:record.mediaUrl title:record.title];
        return;
    }
    
    if (viewType == videoView) {
        self.activityIndicator.hidden = YES;
        self.playImage.hidden = NO;
        [self loadThumbnailOfVideoWithUrl:record.thumbnail urlVideo:record.mediaUrl];
        return;
    }
}

#pragma mark - load data
- (void)getImageWithUrl:(NSString *)url title:(NSString *)title {
    
    ShareData *shared = [ShareData instance];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    [dict setObject:url forKey:@"Path"];
    
    [shared.promotionProgramProxy getImage:dict completeionHander:^(id result, NSString *errorCode, NSString *message) {
        
        if(![errorCode isEqualToString:@"0"] || result == nil ) {
            
            return ;
        }
        
        NSString *strData = [result objectForKey:@"Image"];
        
        [self decodeBase64ToImage:strData title:title];
        
    } errorHandler:^(NSError *error) {
        
    }];
    
}

#pragma mark - get image from binary string
- (void)decodeBase64ToImage:(NSString *)strEncodeData title:(NSString *)title{
    
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
        if ( data == nil )
            return;
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *image = [UIImage imageWithData:data];
            self.imageView.image = image;
            if (self.delegate) {
                [self.delegate image:image title:title];
            }
            [self.activityIndicator stopAnimating];
            self.activityIndicator.hidden = YES;
        });
    });
}

#pragma mark - load thumbnail for video
- (void)loadThumbnailOfVideoWithUrl:(NSString *)urlThumbnail urlVideo:(NSString *)urlVideo {
    
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        
        NSData * data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:urlThumbnail]];
        if ( data == nil )
            return;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIImage *image = [UIImage imageWithData:data];
            self.imageView.image = image;
            
        });
    });
}

@end
