//
//  InfoDetailViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 4/1/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "InfoDetailViewController.h"
#import "SiUtils.h"
#import "ShareData.h"

@interface InfoDetailViewController ()

@end

@implementation InfoDetailViewController {
    NSArray *arrData;
}

@synthesize record;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.lblTitle.text = record.title;
    [self loadData];
    
    // Do any additional setup after loading the view from its nib.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.view.frame = CGRectMake(0,62,[SiUtils getScreenFrameSize].width,[SiUtils getScreenFrameSize].height - 62);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - load data
- (void)loadData {
    ShareData *shared = [ShareData instance];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    [dict setObject:self.record.type forKey:@"Type"];
    [dict setObject:self.record.ID forKey:@"ID"];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    
    [self showMBProcess];
    
    [shared.promotionProgramProxy getBrochureDetail:dict completeionHander:^(id result, NSString *errorCode, NSString *message) {
        
        [self hideMBProcess];
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"0"] || result == nil) {
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",@"Không có kết quả trả về"]];
            return ;
        }
        arrData = result;
        
        self.record = arrData[0];
        [self.webViewContent loadHTMLString:self.record.mediaUrl baseURL:nil];

        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
        [self hideMBProcess];
        
    }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnCancel_Clicked:(id)sender {
    if(self.delegate){
        [self.delegate CancelPopupInfoDetail];
    }
}
@end
