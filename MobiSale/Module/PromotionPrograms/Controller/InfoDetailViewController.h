//
//  InfoDetailViewController.h
//  MobiSale
//
//  Created by ISC-DanTT on 4/1/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PromotionProgramRecord.h"
#import "BaseViewController.h"

@protocol InfoDetailDelegate <NSObject>

-(void)CancelPopupInfoDetail;

@end

@interface InfoDetailViewController : BaseViewController

@property (strong, nonatomic) PromotionProgramRecord *record;

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@property (strong, nonatomic) IBOutlet UIWebView *webViewContent;

@property (retain, nonatomic) id<InfoDetailDelegate> delegate;

- (IBAction)btnCancel_Clicked:(id)sender;

@end
