//
//  PromotionImageVideoViewController.h
//  MobiSale
//
//  Created by ISC-DanTT on 3/24/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

typedef enum {
    imageView = 1,
    videoView
}viewType;

@interface PromotionImageVideoViewController : BaseViewController

@property (strong, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) IBOutlet UILabel *lblNoResult;

@property (assign) viewType viewType;

@end
