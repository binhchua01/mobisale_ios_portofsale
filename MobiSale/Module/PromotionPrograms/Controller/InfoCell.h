//
//  InfoCell.h
//  MobiSale
//
//  Created by ISC-DanTT on 3/25/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *title;

@property (strong, nonatomic) IBOutlet UIWebView *contentWebView;

@end
