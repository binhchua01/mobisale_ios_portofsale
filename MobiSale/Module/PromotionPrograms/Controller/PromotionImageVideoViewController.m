//
//  PromotionImageVideoViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 3/24/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "PromotionImageVideoViewController.h"
#import "PromotionProgramRecord.h"
#import "ImageVideoCell.h"
#import "ShareData.h"
#import "SiUtils.h"
#import <MWPhotoBrowser/MWPhoto.h>
#import <MWPhotoBrowser/MWPhotoBrowser.h>
#import <XCDYouTubeKit/XCDYouTubeKit.h>
#import "AppDelegate.h"

@interface PromotionImageVideoViewController () <UICollectionViewDelegate, UICollectionViewDataSource, MWPhotoBrowserDelegate, ImageVideoDelegate>

@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSMutableArray *thumbs;

@end

@implementation PromotionImageVideoViewController {
    NSArray *arrData;
    NSMutableArray *arrPhotos;
    
    PromotionProgramRecord *record;
    
    MWPhoto *photo, *thumb;
    
    AppDelegate *appDelegate;
}

@synthesize viewType, photos, thumbs;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"ImageVideoCell" bundle:nil] forCellWithReuseIdentifier:@"ImageVideoCell"];
    
    self.collectionView.backgroundColor = [UIColor clearColor];
    
    self.collectionView.hidden = YES;
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Pattern"]];
    
    arrPhotos = [NSMutableArray array];
    record = [[PromotionProgramRecord alloc] init];
    photos = [[NSMutableArray alloc] init];
    thumbs = [[NSMutableArray alloc] init];
    
    if (viewType == imageView) {
        [self loadDataWithType:@"1"];
        return;
    }
    if (viewType == videoView) {
        [self loadDataWithType:@"2"];
        return;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    UIApplication* application = [UIApplication sharedApplication];
    if (application.statusBarOrientation != UIInterfaceOrientationPortrait) {
        UIViewController *c = [[UIViewController alloc]init];
        [self.navigationController presentViewController:c animated:NO completion:^{
            [self.navigationController dismissViewControllerAnimated:YES completion:^{
            }];
        }];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionView Datasource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return arrData.count;
}

- (ImageVideoCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ImageVideoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ImageVideoCell" forIndexPath:indexPath];

    if (arrPhotos.count > 0 && indexPath.item < arrPhotos.count) {
        cell.activityIndicator.hidden = YES;
        cell.imageView.image = arrPhotos[indexPath.item];
        return cell;
    }
    
    cell.imageView.image = [UIImage imageNamed:@"ic_no_image_fpt.png"];
    
    record = [arrData objectAtIndex:indexPath.item];
    
    [cell.contentView clearsContextBeforeDrawing];
    
    cell.delegate = self;
    cell.item = indexPath.item;
    cell.viewType = viewType;
    cell.record = record;

//    if (viewType == imageView) {
//        cell.playImage.hidden = YES;
//        cell.imageView.image = photos[indexPath.item];
//        //[self getImageWithUrl:record.thumbnail cell:cell];
//        //[self getImageWithUrl:record.mediaUrl cell:cell];
//    }
//    if (viewType == videoView) {
//        cell.playImage.hidden = NO;
//        [self loadThumbnailOfVideoWithUrl:record.thumbnail urlVideo:record.mediaUrl cell:cell];
//    }
    
    return cell;
}


#pragma mark - UICollectionViewDelegate
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
//    ImageVideoCell *cell = (ImageVideoCell*)[collectionView cellForItemAtIndexPath:indexPath];
//    NSArray *views = [cell.contentView subviews];
//    UILabel *label = [views objectAtIndex:1];
//    NSLog(@"Select %@",label.text);
    
    if (viewType == imageView) {
        [self brownserImage:indexPath.item];
        return;
    }
    
    if (viewType == videoView) {
        [appDelegate setShouldRotate:YES]; // or NO to disable rotation
        
        record = [arrData objectAtIndex:indexPath.item];
        [self playVideo:record.mediaUrl];
        return;
    }
    
}

#pragma  mark - implement ImageViewDelegate method
- (void)image:(UIImage *)image title:(NSString *)title {
    
    if (photos.count < arrData.count) {
        photo = [MWPhoto photoWithImage:image];
        photo.caption = title;
        [photos addObject:photo];
        
    }
    /*
     * save image loaded in array temp for cell don't load image againt 
     */
    for (UIImage *temp in arrPhotos) {
        if (temp == image) {
            return;
        }
    }
    [arrPhotos addObject:image];

}

- (void)brownserImage:(NSInteger)indexItem {
    // Create browser (must be done each time photo browser is
    // displayed. Photo browser objects cannot be re-used)
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = YES; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = NO; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = NO; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    browser.autoPlayOnAppear = NO; // Auto-play first video
    
    // Customise selection images to change colours if required
    //browser.customImageSelectedIconName = @"ImageSelected.png";
    //browser.customImageSelectedSmallIconName = @"ImageSelectedSmall.png";
    
    // Optionally set the current visible photo before displaying
    [browser setCurrentPhotoIndex:indexItem];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    // Manipulate
    [browser showNextPhotoAnimated:YES];
    [browser showPreviousPhotoAnimated:YES];
}

- (void)playVideo:(NSString *)identifire {
    XCDYouTubeVideoPlayerViewController *videoPlayerViewController = [[XCDYouTubeVideoPlayerViewController alloc] initWithVideoIdentifier:identifire];
    //videoPlayerViewController.moviePlayer.backgroundPlaybackEnabled = [[NSUserDefaults standardUserDefaults] boolForKey:@"PlayVideoInBackground"];
    videoPlayerViewController.preferredVideoQualities = @[ @(XCDYouTubeVideoQualityMedium360), @(XCDYouTubeVideoQualityHD720)];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlayerPlaybackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:videoPlayerViewController.moviePlayer];
    
    /*
     * Videos play sound even if the phone is on silent or vibrate
     */
    NSError *_error = nil;
    [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error: &_error];
    
    [self presentMoviePlayerViewControllerAnimated:videoPlayerViewController];
}


#pragma mark - Notifications

- (void) moviePlayerPlaybackDidFinish:(NSNotification *)notification {
    
    [appDelegate setShouldRotate: NO];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:notification.object];
    MPMovieFinishReason finishReason = [notification.userInfo[MPMoviePlayerPlaybackDidFinishReasonUserInfoKey] integerValue];
    if (finishReason == MPMovieFinishReasonPlaybackError) {
        NSString *title = NSLocalizedString(@"Video Playback Error", @"Full screen video error alert - title");
        NSError *error = notification.userInfo[XCDMoviePlayerPlaybackDidFinishErrorUserInfoKey];
        NSString *message = [NSString stringWithFormat:@"%@\n%@ (%@)", error.localizedDescription, error.domain, @(error.code)];
        NSString *cancelButtonTitle = NSLocalizedString(@"OK", @"Full screen video error alert - cancel button");
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:cancelButtonTitle otherButtonTitles:nil];
        [alertView show];
    }
}

#pragma mark - load data
- (void)loadDataWithType:(NSString *)type {
    ShareData *shared = [ShareData instance];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    [dict setObject:type forKey:@"Type"];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    
    [self showMBProcess];
    
    [shared.promotionProgramProxy getBrochure:dict completeionHander:^(id result, NSString *errorCode, NSString *message) {
        
        [self hideMBProcess];
        
        arrData = result;
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"0"] || result == nil || arrData.count <= 0) {
            self.lblNoResult.hidden = NO;
            return ;
        }
        
        self.lblNoResult.hidden     = YES;
        self.collectionView.hidden  = NO;
        
        [self.collectionView reloadData];
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
        [self hideMBProcess];
        
    }];
    
}

#pragma mark - load list image
- (void)loadListImage {
    for (record in arrData) {
        [self getImageWithUrl:record.mediaUrl cell:nil];
    }
}

#pragma mark - get image from url
- (void)getImageWithUrl:(NSString *)url cell:(ImageVideoCell *)cell {
    ShareData *shared = [ShareData instance];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    [dict setObject:url forKey:@"Path"];
    
    [shared.promotionProgramProxy getImage:dict completeionHander:^(id result, NSString *errorCode, NSString *message) {
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        
        if(![errorCode isEqualToString:@"0"] || result == nil || arrData.count <= 0) {
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",@"Không có kết quả trả về"]];
            return ;
        }
        
        NSString *strData = [result objectForKey:@"Image"];
        NSData *data = [[NSData alloc]initWithBase64EncodedString:strData options:NSDataBase64DecodingIgnoreUnknownCharacters];
        [photos addObject:[UIImage imageWithData:data]];
        
        [self.collectionView reloadData];

        //[self decodeBase64ToImage:strData cell:cell];
        //cell.imageView.image = image;
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
        [self hideMBProcess];
        
    }];
    
}

#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < photos.count)
        return [photos objectAtIndex:index];
    return nil;

}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser thumbPhotoAtIndex:(NSUInteger)index {
    if (index < thumbs.count)
        return [thumbs objectAtIndex:index];
    return nil;
}

//- (MWCaptionView *)photoBrowser:(MWPhotoBrowser *)photoBrowser captionViewForPhotoAtIndex:(NSUInteger)index {
//    MWPhoto *photo = [self.photos objectAtIndex:index];
//    MWCaptionView *captionView = [[MWCaptionView alloc] initWithPhoto:photo];
//    return [captionView autorelease];
//}

//- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser actionButtonPressedForPhotoAtIndex:(NSUInteger)index {
//    NSLog(@"ACTION!");
//}

- (void)photoBrowser:(MWPhotoBrowser *)photoBrowser didDisplayPhotoAtIndex:(NSUInteger)index {
    NSLog(@"Did start viewing photo at index %lu", (unsigned long)index);
}


//- (NSString *)photoBrowser:(MWPhotoBrowser *)photoBrowser titleForPhotoAtIndex:(NSUInteger)index {
//    return [NSString stringWithFormat:@"Photo %lu", (unsigned long)index+1];
//}


- (void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser {
    // If we subscribe to this method we must dismiss the view controller ourselves
    NSLog(@"Did finish modal presentation");
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - load thumbnail for video
- (void)loadThumbnailOfVideoWithUrl:(NSString *)urlThumbnail urlVideo:(NSString *)urlVideo cell:(ImageVideoCell *)cell {
    //    NSData * imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:url]];
    //    cell.imageView.image = [UIImage imageWithData:imageData];
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        NSData * data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:urlThumbnail]];
        if ( data == nil )
            return;
        dispatch_async(dispatch_get_main_queue(), ^{
            // WARNING: is the cell still using the same data by this point??
            
            UIImage *image = [UIImage imageWithData:data];
            cell.imageView.image = image;

        });
    });
}

#pragma mark - get image from binary string
- (void)decodeBase64ToImage:(NSString *)strEncodeData cell:(ImageVideoCell *)cell {
    //NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    //return [UIImage imageWithData:data];
    
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
        if ( data == nil )
            return;
        dispatch_async(dispatch_get_main_queue(), ^{
            // WARNING: is the cell still using the same data by this point??
           // cell.imageView.image = [UIImage imageWithData:data];
            photo = [MWPhoto photoWithImage:[UIImage imageWithData:data]];
            photo.caption = cell.title.text;
            
            if (photos.count <= arrData.count) {
                [photos addObject:photo];

            }
        });
    });
}

@end
