//
//  PromotionProgramViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 3/24/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "PromotionProgramViewController.h"
#import "PromotionInfoViewController.h"
#import "PromotionImageVideoViewController.h"
#import "SiUtils.h"

@interface PromotionProgramViewController () <ViewPagerDataSource, ViewPagerDelegate>

@end

@implementation PromotionProgramViewController {
    NSUInteger numberTabs;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"CHƯƠNG TRÌNH KHUYẾN MÃI";
    
    self.dataSource = self;
    self.delegate   = self;
    
    numberTabs = 3;
    
    [self selectTabAtIndex:0];
    // Do any additional setup after loading the view.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ViewPagerDataSource

- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager {
    return numberTabs;
}

- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index {
    UILabel *label = [UILabel new];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont systemFontOfSize:14.0];
    NSString *labelTitle = @"";
    switch (index) {
        case 0:
            labelTitle = @"HÌNH ẢNH";
            break;
        case 1:
            labelTitle = @"VIDEOS";
            break;
        case 2:
            labelTitle = @"THÔNG TIN";
            break;
        default:
            break;
    }
    
    label.text = StringFormat(@"%@", labelTitle);
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    [label sizeToFit];
    
    return label;
}

- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index {
    switch (index) {
        case 0: {
            PromotionImageVideoViewController *vc = [[PromotionImageVideoViewController alloc] init];
            vc.viewType = imageView;
            return vc;
        }
            
        case 1: {
            PromotionImageVideoViewController *vc = [[PromotionImageVideoViewController alloc] init];
            vc.viewType = videoView;
            return vc;
        }
            
        case 2:
            return [[PromotionInfoViewController alloc] init];
        default:
            return viewPager;
    }
}

#pragma mark - ViewPagerDelegate 

- (CGFloat)viewPager:(ViewPagerController *)viewPager valueForOption:(ViewPagerOption)option withDefault:(CGFloat)value {
    switch (option) {
        case ViewPagerOptionStartFromSecondTab:
            return 1.0;
        case ViewPagerOptionCenterCurrentTab:
            return 0.0;
        case ViewPagerOptionTabLocation:
            return 1.0;
        case ViewPagerOptionTabHeight:
            return 39.0;
        case ViewPagerOptionTabOffset:
            return 36.0;
        case ViewPagerOptionTabWidth:
            //return UIInterfaceOrientationIsLandscape(self.interfaceOrientation) ? 128.0 : 96.0;
            return [SiUtils getScreenFrameSize].width / numberTabs;
        case ViewPagerOptionFixFormerTabsPositions:
            return 0.0;
        case ViewPagerOptionFixLatterTabsPositions:
            return 0.0;
        default:
            return value;
    }
}

- (UIColor *)viewPager:(ViewPagerController *)viewPager colorForComponent:(ViewPagerComponent)component withDefault:(UIColor *)color {
    switch (component) {
        case ViewPagerIndicator:
            return [UIColor orangeColor];
        case ViewPagerTabsView:
            return [[UIColor colorWithRed:0.133 green:0.698 blue:0.569 alpha:1] colorWithAlphaComponent:1.0];
        case ViewPagerContent:
            return [UIColor whiteColor];
        default:
            return color;
    }
}

@end
