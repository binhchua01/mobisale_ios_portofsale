//
//  ImageVideoCell.h
//  MobiSale
//
//  Created by ISC-DanTT on 3/24/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PromotionImageVideoViewController.h"

@class PromotionProgramRecord;

@protocol ImageVideoDelegate <NSObject>

@required

- (void)image:(UIImage *)image title:(NSString *)title;

@end

@interface ImageVideoCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imageView;

@property (strong, nonatomic) IBOutlet UILabel *title;

@property (strong, nonatomic) IBOutlet UIImageView *playImage;

@property (strong, nonatomic) IBOutlet UIView *mView;

@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (strong, nonatomic) NSString *mediaUrl;

@property (assign, nonatomic) NSInteger item;

@property (nonatomic, strong) PromotionProgramRecord *record;

@property (assign) viewType viewType;

@property (strong, nonatomic) id <ImageVideoDelegate> delegate;

@end
