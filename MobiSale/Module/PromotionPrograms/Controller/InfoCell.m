//
//  InfoCell.m
//  MobiSale
//
//  Created by ISC-DanTT on 3/25/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "InfoCell.h"

@implementation InfoCell

- (void)awakeFromNib {
    [super awakeFromNib];

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
