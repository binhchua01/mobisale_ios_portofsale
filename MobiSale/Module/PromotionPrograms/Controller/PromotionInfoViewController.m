//
//  PromotionInfoViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 3/24/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "PromotionInfoViewController.h"
#import "PromotionProgramRecord.h"
#import "InfoCell.h"
#import "ShareData.h"
#import "InfoDetailViewController.h"

@interface PromotionInfoViewController () <InfoDetailDelegate>

@end

@implementation PromotionInfoViewController {
    NSMutableArray *arrData;
    PromotionProgramRecord *record;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    arrData = [[NSMutableArray alloc] init];
    
    self.listTableView.hidden = YES;
    
    [self loadDataWithType:@"3"];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - load data
- (void)loadDataWithType:(NSString *)type {
    ShareData *shared = [ShareData instance];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    [dict setObject:type forKey:@"Type"];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    
    [self showMBProcess];
    
    [shared.promotionProgramProxy getBrochure:dict completeionHander:^(id result, NSString *errorCode, NSString *message) {
        
        [self hideMBProcess];
        
        arrData = result;
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"0"] || result == nil || arrData.count <= 0) {
            self.lblNoResult.hidden = NO;
            //[Common_client showAlertMessage:[NSString stringWithFormat:@"%@",@"Không có kết quả trả về"]];
            return ;
        }
        self.lblNoResult.hidden = YES;
        self.listTableView.hidden = NO;
        [self.listTableView reloadData];
                
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
        [self hideMBProcess];
        
    }];
    
}


#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(arrData.count > 0 )
        return arrData.count;
    
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 49;
}


//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    return 3;
//}
//
//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//    UIView *footerView = [[UIView alloc] init];
//    footerView.backgroundColor = [UIColor  colorWithRed:0.133 green:0.698 blue:0.569 alpha:1];
//    return footerView;
//}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        cell.accessoryType = UITableViewCellAccessoryDetailButton;
    }
//    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"InfoCell" owner:self options:nil];
//    cell = [nib objectAtIndex:0];
    
    if(arrData.count > 0) {
        record = [arrData objectAtIndex:indexPath.row];
        cell.textLabel.text = record.title;
        cell.textLabel.numberOfLines = 0;
        [cell.textLabel sizeToFit];
//        [cell.contentWebView loadHTMLString:record.mediaUrl baseURL:nil];

    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self showPopupInfoDetailAtIndex:indexPath.row];

}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    [self showPopupInfoDetailAtIndex:indexPath.row];

}

- (void)showPopupInfoDetailAtIndex:(NSInteger)index {
    record = [arrData objectAtIndex:index];
    InfoDetailViewController *vc = [[InfoDetailViewController alloc] init];
    vc.record = [[PromotionProgramRecord alloc] init];
    vc.record = record;
    vc.delegate = self;
    [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideTopBottom];
}

- (void)CancelPopupInfoDetail {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
}

@end
