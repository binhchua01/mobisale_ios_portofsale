//
//  AddNewSchedulePotentialModel.h
//  MobiSale
//
//  Created by Tran Vu on 10/16/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddNewSchedulePotentialModel : NSObject
@property (strong, nonatomic) NSString *fullName;
@property ( nonatomic) NSUInteger potentialObjID;
@property ( nonatomic) NSInteger potentialObjScheduleID;
@property (strong, nonatomic) NSString *scheduleDate;
@property (strong, nonatomic) NSString *scheduleDescription;
@property (strong, nonatomic) NSString *supporter;

- (id)initWithDictionary:(NSDictionary *)dict;
+ (id)sharedInstance;


@end
