//
//  PotentialCustomerDetailForm.h
//  MobiSale
//
//  Created by ISC-DanTT on 11/20/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

typedef enum: NSInteger {
    FromNotification = 1,
    FromCode,
    Normal
    
} PotentialType;

@interface PotentialCustomerDetailForm :BaseViewController

@property (strong, nonatomic) IBOutlet UILabel *lblRegCode;
@property (strong, nonatomic) IBOutlet UIScrollView *scorllView;
@property (strong, nonatomic) IBOutlet UIView *menuView;

@property (strong, nonatomic) IBOutlet UILabel *lblFullName;
@property (strong, nonatomic) IBOutlet UILabel *lblPassport;
@property (strong, nonatomic) IBOutlet UILabel *lblTaxID;
@property (strong, nonatomic) IBOutlet UILabel *lblPhone1;
@property (strong, nonatomic) IBOutlet UILabel *lblEmail;
@property (strong, nonatomic) IBOutlet UILabel *lblFacebook;
@property (strong, nonatomic) IBOutlet UITextView *textAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblCreateDate;
@property (strong, nonatomic) IBOutlet UITextView *txtNote;

@property (strong, nonatomic) IBOutlet UILabel *lblServiceType;
@property (strong, nonatomic) IBOutlet UILabel *lblISPType;
@property (strong, nonatomic) IBOutlet UILabel *lblISPIPTV;
@property (strong, nonatomic) IBOutlet UILabel *lblISPStartDate;
@property (strong, nonatomic) IBOutlet UILabel *lblISPEndDate;

@property (strong, nonatomic) IBOutlet UILabel *lblLocation;
@property (strong, nonatomic) IBOutlet UIButton *btnRegType;

@property (strong, nonatomic) NSString *ID;
@property (assign, nonatomic) PotentialType potentialType;
@property (strong, nonatomic) NSString *Supporter;

@end
