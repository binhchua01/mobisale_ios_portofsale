//
//  AddNewSchedulePotentialModel.m
//  MobiSale
//
//  Created by Tran Vu on 10/16/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import "AddNewSchedulePotentialModel.h"

@implementation AddNewSchedulePotentialModel
@synthesize supporter,scheduleDate,scheduleDescription,potentialObjScheduleID,potentialObjID;

- (id)initWithDictionary:(NSDictionary *)dict {
    if (self == [super init]) {
        self.scheduleDescription = dict[@"ScheduleDescription"]?:@"";
        self.fullName = dict[@"FullName"]?:@"";
        self.potentialObjID = [dict[@"PotentialObjID" ]integerValue];
        self.potentialObjScheduleID = [dict [@"PotentialObjScheduleID"]integerValue];
        self.scheduleDate = dict[@"ScheduleDate"];
        self.supporter = dict[@"Supporter"];
        
    }
    
    
    return self;
}

+ (id)sharedInstance {
    static AddNewSchedulePotentialModel *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[AddNewSchedulePotentialModel alloc] init];
        // Do any other initialisation stuff here
    });
    
    return sharedInstance;
}


@end
