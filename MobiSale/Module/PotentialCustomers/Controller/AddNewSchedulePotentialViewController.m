//
//  AddNewSchedulePotentialViewController.m
//  MobiSale
//
//  Created by Tran Vu on 10/16/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import "AddNewSchedulePotentialViewController.h"
#import "Common_client.h"
#import "ShareData.h"
#import "AddNewSchedulePotentialModel.h"
#import "LocalNotificationModel.h"
#import "AppDelegate.h"

@interface AddNewSchedulePotentialViewController ()<PickerViewControllerDelegate,UIAlertViewDelegate,UITextViewDelegate>

@property(strong,nonatomic) UILocalNotification *localNotification;

@end

@implementation AddNewSchedulePotentialViewController{
    NSDate *dateNotification;
    UIAlertView *alertView;
    AddNewSchedulePotentialModel *record;
    LocalNotificationModel *localNotificationModel;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //intit localNtif
    self.localNotification = [[UILocalNotification alloc] init];
    
    [self setUpView];
    if ([self.potentialObjSchedule isEqualToString:@""]) {
      self.title = @"TẠO MỚI LỊCH HẸN";
        
     //self.btnUpdate.titleLabel.text = @"TẠO MỚI";
        [self.btnUpdate setTitle:@"TẠO MỚI"forState:UIControlStateNormal];
        
    } else {
        
        self.title = @"CẬP NHẬT LỊCH HẸN";
        self.txvScheduleDescription.text = self.scheduleDescription;
        [self.btnScheduleDate setTitle:self.scheduleDate forState:UIControlStateNormal];
        //self.btnUpdate.titleLabel.text = @"CẬP NHẬT";
        [self.btnUpdate setTitle:@"CẬP NHẬT"forState:UIControlStateNormal];
}
    
}

- (void)setUpView {
    self.viewInfoAndTime.layer.borderColor  = [UIColor colorWithRed:0.133 green:0.698 blue:0.569 alpha:1].CGColor;
    self.viewInfoAndTime.layer.borderWidth  = 1.0f;
    self.viewInfoAndTime.layer.cornerRadius = 4.0f;
    
    self.btnUpdate.layer.cornerRadius = 4.0f;
    
    self.txvScheduleDescription.layer.borderColor= [UIColor colorWithRed:0 green:0.173 blue:0.294 alpha:1].CGColor;
    self.txvScheduleDescription.layer.borderWidth = 1.f;
    self.txvScheduleDescription.layer.cornerRadius = 6.0f;
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
   
}

-(instancetype) initWithNibName:(NSString *)nibNameOrNil withID:(NSString*)potentialObjID withScheduleDescription:(NSString *)scheduleDescription withScheduleDate:(NSString *)scheduleDate withPotentialObjSchedule:(NSString *)potentialObjSchedule andSupporter:(NSString *)supporter {
    
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        self.potentialObjID = potentialObjID;
        self.potentialObjSchedule = potentialObjSchedule;
        self.scheduleDate = scheduleDate;
        self.scheduleDescription = scheduleDescription;
        self.supporter = supporter;
    }
    return self;
}

- (IBAction)btnUpdate_clicked:(id)sender {
    
    [self updatePotentialObjSchedule:self.potentialObjSchedule];
    
}


- (IBAction)btnScheduleDate_clicked:(id)sender {
    PickerViewController *pc = [[PickerViewController alloc] initFromNib];
    pc.pickerType = DateAndTimePickerType;
    pc.delegate = self;
    [self presentViewControllerOverCurrentContext:pc animated:YES completion:nil];
    
}


-(void)didSelectDate:(NSDate *)date formattedString:(NSString *)dateString {
    
    [self.btnScheduleDate setTitle:dateString forState:UIControlStateNormal];
    
    dateNotification = date;
    
}

- (void)updatePotentialObjSchedule:(NSString *)potentialObjScheduleID {
    
    NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
    NSString *scheduleDescription = [self.txvScheduleDescription.text stringByTrimmingCharactersInSet:charSet];
    
    //checking Data
    if ([scheduleDescription isEqualToString:@""] ) {
        
        [self showAlertBox:@"Thông Báo" message:@"Bạn cần nhập nội dung để tạo thông báo lịch hẹn cho KHTN"];
        return;
    }
    if ([self.btnScheduleDate.titleLabel.text isEqualToString:@"[Chọn ngày để đặt lịch hẹn]"]) {
        [self showAlertBox:@"Thông Báo" message:@"Bạn cần chọn ngày để tạo thông báo lịch hẹn cho KHTN"];
        return;
    }
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    
    NSString *strSupporter = @"";
    if (self.supporter == @"") {
        self.supporter = shared.currentUser.userName;
    }
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:self.supporter ?:@"" forKey:@"Supporter"];
    [dict setObject:self.potentialObjID ?:@"" forKey:@"PotentialObjID"];
    [dict setObject:potentialObjScheduleID forKey:@"PotentialObjScheduleID"];
    [dict setObject:scheduleDescription forKey:@"ScheduleDescription"];
    
    [dict setObject:self.btnScheduleDate.titleLabel.text forKey:@"ScheduleDate"];
     [self showMBProcess];
    
    
    [shared.potentialCustomerProxy UpdatePotentialObjSchedule:dict completehander:^(id result,NSString *errorCode,NSString *message){
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        NSArray *arr = result;
        
        if ( ![errorCode isEqualToString:@"0"]|| result == nil ) {
             [Common_client showAlertMessage:@"Không có kết quả trả về"];
             [self hideMBProcess];
        } else {
           // [Common_client showAlertMessage:message];
            [self showAlertView:message];
            alertView.tag = 40;
            record = [arr objectAtIndex:0];
            if ([potentialObjScheduleID isEqualToString:@""]) {
                
                [self scheduleNotificationLocal];
            } else {
                
                [self editScheduleLocalNotification:self.scheduleDescription];
            }
            
            [self hideMBProcess];
        }
        
    } errorHandler:^(NSError *error){
       
        
        
        [self hideMBProcess];
        
    } ];
    
}
- (void)showAlertView:(NSString *)message {
    
    alertView = [[UIAlertView alloc] initWithTitle:@"Thông Báo" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView1 clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            if (alertView.tag == 40) {
                [self.navigationController popViewControllerAnimated:YES];
                break;
            }
            break;
            
        default:
            break;
    }
    
}

//converStringToDate
- (NSDate *)convertStringToDate:(NSString *)dateStringInput{
    @try {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy HH:mm:ss"];
        NSTimeZone *timeZone = [NSTimeZone defaultTimeZone];
        [dateFormatter setTimeZone:timeZone];
        NSDate *dateFromString = [[NSDate alloc] init];
        dateFromString = [dateFormatter dateFromString:dateStringInput];
        return dateFromString;
        
        
    }
    @catch (NSException *exception) {
        [self showAlertBox:@"Lỗi" message:@"Bạn đã nhập sai định dạng thời gian cần tìm"];
        [CrashlyticsKit recordCustomExceptionName:@"AddNewSchedulePotentialViewController - funtion (convertStringToDate)-Error handle Bạn đã nhập sai định dạng thời gian cần tìm" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
    }
    
}

- (void)scheduleNotificationLocal{
    
   // UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    dateNotification = [self convertStringToDate:self.btnScheduleDate.titleLabel.text];
    _localNotification.fireDate = dateNotification;
    
    _localNotification.alertBody = self.txvScheduleDescription.text;
    _localNotification.alertAction = @"Show me the item";
    _localNotification.timeZone = [NSTimeZone defaultTimeZone];
    _localNotification.soundName = UILocalNotificationDefaultSoundName;
    _localNotification.applicationIconBadgeNumber = [[UIApplication sharedApplication] applicationIconBadgeNumber] +1;
   
    
    [[UIApplication sharedApplication] scheduleLocalNotification:_localNotification];
    
    ShareData *shared = [ShareData instance];
    shared.arrayLocalNotification = [[UIApplication sharedApplication] scheduledLocalNotifications];
    
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadData" object:self];
    
}

- (void)editScheduleLocalNotification:(NSString *)scheduleDescriptionOld{
    
    
    //[self scheduleNotificationLocal];
    
    ShareData *shared = [ShareData instance];
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = shared.arrayLocalNotification;
    
    for (int i = 0; i < [eventArray count]; i ++) {
        UILocalNotification *oneEvent = [eventArray objectAtIndex:i];
        NSString *body = [NSString stringWithFormat:@"%@",[eventArray[i] valueForKey:@"alertBody"]];
        
        if ([body isEqualToString:scheduleDescriptionOld]) {
            [app cancelLocalNotification:oneEvent];
            break;
        }
    
    }
    
     [self scheduleNotificationLocal];
}
// DELETED WHITE SPACE OF TEXVIEW


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
    }
    
    return YES;
}

@end
