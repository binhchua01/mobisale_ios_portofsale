//
//  MapsListPotentialCustomersViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 11/23/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "MapsListPotentialCustomersViewController.h"
#import "PotentialCustomerDetailForm.h"
#import "ShareData.h"
#import "KeyValueModel.h"
#import "Common.h"
#import "NIDropDownCustom.h"

@interface MapsListPotentialCustomersViewController () <NIDropDownCustomDelegate> {
    NSMutableArray *arrMarkerType;
    NSMutableArray *arrAnnotate;
    KeyValueModel  *markerImage;
    NIDropDownCustom *dropDown;
}

@end

@implementation MapsListPotentialCustomersViewController{
    CLLocation  *latLng;
    NSString    *lng, *lngMarker;
    NSString    *lngCoordinate;
    UIButton    *buttonMapType;
    UIButton    *buttonAnnotate;
    GMSMarker   *pin;
    CLLocationCoordinate2D position;

}
@synthesize mapView_;
@synthesize locationManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"BẢN ĐỒ";
    self.screenName = @"DANH SÁCH KHÁCH HÀNG TIỀM NĂNG (MAP)";
    self.mapView_.delegate = self;
    arrAnnotate   = [NSMutableArray array];
    arrMarkerType = [NSMutableArray array];
    arrMarkerType = [Common getMarkerType];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self loadMaps];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    if(IS_OS_8_OR_LATER) {
        [locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.arrCustomers = [ShareData instance].arrPotentialCutomers;
}

#pragma Load Map and location

- (void)loadMaps {
    [self showCurrentLocation];
    
    double latitude=latLng.coordinate.latitude;
    double longitude=latLng.coordinate.longitude;
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude longitude:longitude zoom:15];
    if(!self.mapView_){
        mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height - 113) camera:camera];
    }
    mapView_.delegate = self;
    mapView_.myLocationEnabled = YES;
    mapView_.settings.compassButton = YES;
    mapView_.settings.myLocationButton = YES;
    //self.view = mapView_;
    [self.view addSubview:mapView_];
    [self addButtonMapType];
    [self addButtonAnnotate];
    // add markers customers location
    [self loadPotentialCustomerLocation];
    
}
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    //latLng = [locations lastObject];
    if([CLLocationManager locationServicesEnabled]){
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied) {
            [self showAlertBox:@"Thông báo" message:@"Vui lòng cấp phát quyền truy cập GPS cho ứng dụng bằng cách vào Cài đăt ->Quyền riềng tư ->Dịch vụ định vị-> MobiSale"];
            return;
        }
        CLLocation* location = [locations lastObject];
        NSDate* eventDate = location.timestamp;
        NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
        if (fabs(howRecent) < 15.0) {
            latLng = location;
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latLng.coordinate.latitude
                                                                    longitude:latLng.coordinate.longitude
                                                                         zoom:11];
            [mapView_ animateToCameraPosition:camera];
            [self.locationManager stopUpdatingLocation];
            // add marker my location
            //[self AddMarkerLocation:[NSString stringWithFormat:@"%f",latLng.coordinate.latitude] Longitude:[NSString stringWithFormat:@"%f",latLng.coordinate.longitude]];
            // add markers customers location
            //[self loadPotentialCustomerLocation];
            lng = [NSString stringWithFormat:@"(%@,%@)", [NSString stringWithFormat:@"%f",latLng.coordinate.latitude],[NSString stringWithFormat:@"%f",latLng.coordinate.longitude]];
        }
    }
}

- (void)showCurrentLocation {
    mapView_.myLocationEnabled = YES;
    if(IS_OS_8_OR_LATER) {
        [self.locationManager requestWhenInUseAuthorization];
    }
    [self.locationManager startUpdatingLocation];
    
    if([CLLocationManager locationServicesEnabled]){
        
        NSLog(@"Location Services Enabled");
        
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            [self showAlertBox:@"Thông báo" message:@"Vui lòng cấp phát quyền truy cập GPS cho ứng dụng bằng cách vào Cài đăt ->Quyền riềng tư ->Dịch vụ định vị-> MobiSale"];
            return;
        }
    } else {
        if (IS_OS_8_OR_LATER) {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:@"Vui lòng bật GPS để sử dụng bản đồ" delegate:self cancelButtonTitle:@"Đóng" otherButtonTitles:@"Cài đặt",nil];
            alertView.tag = 199;
            [alertView show];
            return;
        }
        [self showAlertBox:@"Thông báo" message:@"Vui lòng bật GPS để sử dụng bản đồ, bằng cách vào Cài đăt ->Quyền riềng tư ->Dịch vụ định vị"];
        return;
    }
    
}

#pragma mark - add marker
-(void)AddMarker:(NSString *)latitude Longitude:(NSString *)longitude Snipet:(NSString *)snipet Title:(NSString *)title customerID:(NSString *)ID withImageName:(NSString*)imageName {
    
    double lat = [latitude doubleValue];
    double ln = [longitude doubleValue];
    
    CLLocationCoordinate2D position2d = CLLocationCoordinate2DMake(lat, ln);
    GMSMarker *marker = [GMSMarker markerWithPosition:position2d];
    marker.snippet = snipet;
    marker.title = title;
    marker.userData = ID;
//    marker.icon = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"ic_td"] height:32];
    marker.icon = [SiUtils imageWithImageHeight:[UIImage imageNamed:imageName] height:32];

    [marker setDraggable:NO];
    marker.map = self.mapView_;
}

-(void)AddMarkerLocation:(NSString *)latitude Longitude:(NSString *)longitude {
    pin.map = nil;
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[latitude doubleValue]
                                                            longitude:[longitude doubleValue]
                                                                 zoom:11.0];
    [mapView_ animateToCameraPosition:camera];
    CLLocationCoordinate2D positions = CLLocationCoordinate2DMake([latitude doubleValue], [longitude doubleValue]);
    pin = [GMSMarker markerWithPosition:positions];
    pin.title = @"Vị trí của tôi";
    pin.icon = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"cus_location.png"] height:32] ;
    pin.map = self.mapView_;
}

#pragma mark - Map delegate

-(BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker{
    [mapView animateToLocation:marker.position];
    [self.mapView_ setSelectedMarker:marker];
    NSArray *nip = [marker.snippet componentsSeparatedByString:@":"];
    if(nip.count > 0){
        lngMarker = [nip objectAtIndex:0];
    }
    lngCoordinate = [NSString stringWithFormat:@"(%f,%f)", marker.position.latitude, marker.position.longitude];
    return YES;
}

-(void)mapView:(GMSMapView *)mapView didDragMarker:(GMSMarker *)marker{
    
    lng = [NSString stringWithFormat:@"(%f,%f)", marker.position.latitude, marker.position.longitude];
}

-(void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker{
    NSString *nibName = @"PotentialCustomerDetailForm";
    PotentialCustomerDetailForm *vc = [[PotentialCustomerDetailForm alloc]initWithNibName:nibName bundle:nil];
    if (marker.userData != nil) {
        vc.ID = marker.userData;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

-(void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    [self handleSingleTap:nil];
}

#pragma show alert
- (void)showAlertBox:(NSString *)title
             message:(NSString *)message {
    
    UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
    [alertView show];
    
}

- (void)loadPotentialCustomerLocation {
    for (int i = 0; i < self.arrCustomers.count; i++) {
        markerImage = [[KeyValueModel alloc] init];
        self.recordCustomer       = [self.arrCustomers objectAtIndex:i];
        NSString *IPTVISPDesc     = [self.recordCustomer.IPTVISPDesc     uppercaseString];
        NSString *InternetISPDesc = [self.recordCustomer.InternetISPDesc uppercaseString];
        if (IPTVISPDesc.length > 0 && InternetISPDesc.length > 0) {
            markerImage.Key = InternetISPDesc;
        } else if (IPTVISPDesc.length >  0  && InternetISPDesc.length <= 0) {
            markerImage.Key = IPTVISPDesc;
        } else if (IPTVISPDesc.length <= 0  && InternetISPDesc.length >  0) {
            markerImage.Key = InternetISPDesc;
        } else {
            markerImage.Key    = @"";
            markerImage.Values = @"ic_td";
        }
        for (KeyValueModel *km in arrMarkerType) {
            if ([markerImage.Key isEqualToString:km.Key]) {
                markerImage = km;
                if (![arrAnnotate containsObject:km]) {
                    [arrAnnotate addObject:km];
                }
                break;
            }
        }
        lng = self.recordCustomer.Latlng;
        if (![lng isEqualToString:@""]) {
            NSArray *arr = [lng componentsSeparatedByString:@","];
            NSString *lat = [[arr objectAtIndex:0] stringByReplacingOccurrencesOfString:@"(" withString:@""]?:@"";
            NSString *ln = [[arr objectAtIndex:1] stringByReplacingOccurrencesOfString:@")" withString:@""]?:@"";
            [self AddMarker:lat Longitude:ln Snipet:self.recordCustomer.Address Title:self.recordCustomer.FullName customerID:self.recordCustomer.ID withImageName:markerImage.Values];
        }
    }
}

- (IBAction)btnMapType_Clicked:(id)sender {
    (self.btnMapType.selected == NO) ? (self.btnMapType.selected = YES):(self.btnMapType.selected= NO);
    
    if(self.btnMapType.selected){
        self.mapView_.mapType = kGMSTypeSatellite;
        [self.btnMapType setTitle:@"Bản đồ" forState:UIControlStateNormal];
    } else {
        self.mapView_.mapType = kGMSTypeNormal;
        [self.btnMapType setTitle:@"Vệ tinh" forState:UIControlStateNormal];
    }
}

// add button map type
- (void)addButtonMapType{
    buttonMapType = [[UIButton alloc] initWithFrame:CGRectMake(5,self.view.frame.size.height - 96,80,40)];
    [buttonMapType addTarget:self action:@selector(buttonMapType_Click:) forControlEvents:UIControlEventTouchUpInside];
    buttonMapType.backgroundColor = [UIColor whiteColor];
    [buttonMapType setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [buttonMapType setTitle:@"Vệ tinh" forState:UIControlStateNormal];
    buttonMapType.layer.shadowOffset = CGSizeMake(-2, -2);
    buttonMapType.layer.masksToBounds = NO;
    buttonMapType.layer.cornerRadius = 2;
    buttonMapType.layer.shadowRadius = 5;
    buttonMapType.layer.shadowOpacity = 0.5;
    [self.view addSubview:buttonMapType];
}

// add button annotate
- (void)addButtonAnnotate {
    CGRect frame = [UIScreen mainScreen].bounds;
    buttonAnnotate = [[UIButton alloc] initWithFrame:CGRectMake(frame.size.width-143,72,129,40)];
    [buttonAnnotate addTarget:self action:@selector(buttonAnnotate_Clicked:) forControlEvents:UIControlEventTouchUpInside];
    buttonAnnotate.backgroundColor = [UIColor whiteColor];
    [buttonAnnotate setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [buttonAnnotate setTitle:@"Chú thích" forState:UIControlStateNormal];
    buttonAnnotate.layer.shadowOffset = CGSizeMake(-2, -2);
    buttonAnnotate.layer.masksToBounds = NO;
    buttonAnnotate.layer.cornerRadius = 2;
    buttonAnnotate.layer.shadowRadius = 5;
    buttonAnnotate.layer.shadowOpacity = 0.5;
    [self.view addSubview:buttonAnnotate];
}

- (void)buttonMapType_Click:(id)sender{
    (buttonMapType.selected == NO) ? (buttonMapType.selected = YES):(buttonMapType.selected= NO);
    
    if(buttonMapType.selected){
        self.mapView_.mapType = kGMSTypeSatellite;
        [buttonMapType setTitle:@"Bản đồ" forState:UIControlStateNormal];
    } else {
        self.mapView_.mapType = kGMSTypeNormal;
        [buttonMapType setTitle:@"Vệ tinh" forState:UIControlStateNormal];
    }
}

- (void)buttonAnnotate_Clicked:(id)sender {
    [self pareArrayForKeyWith:arrAnnotate button:sender];
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
    if (dropDown !=nil) {
        [dropDown hideDropDown:buttonAnnotate];
        [self rel];
    }
}

- (void)pareArrayForKeyWith:(NSArray*)arrInput button:(UIButton*)button{
    int i = 0;
    KeyValueModel *temp;
    NSMutableArray *arrData     = [NSMutableArray array];
    NSMutableArray *arrImage    = [NSMutableArray array];

    for (i = 0; i < arrInput.count; i++) {
        temp = [arrInput objectAtIndex:i];
        [arrData    addObject:temp.Key];
        [arrImage   addObject:[SiUtils imageWithImageHeight:[UIImage imageNamed:temp.Values] height:25]];
    }
    [self showDropDownAtButton:button withArrayData:arrData withArrayImage:arrImage];
}

#pragma mark - alert view delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 199 && buttonIndex == 1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
    }
}


#pragma mark - NIDropDown
- (void)showDropDownAtButton:(UIButton *)sender withArrayData:(NSArray*)arrayData withArrayImage:(NSArray *)arrImage {
    [self.view endEditing:YES];
    if (dropDown == nil) {
        CGFloat height = 161;
        CGFloat width  = 10;
        dropDown = [[NIDropDownCustom alloc] showDropDownAtButton:sender height:&height width:&width data:arrayData images:arrImage animation:@"down"];
        dropDown.delegate = self;
        return;
    }
    [dropDown hideDropDown:sender];
    [self rel];
}

- (void)niDropDownDelegateMethod:(NIDropDownCustom *)sender {
    [self rel];
}

-(void)rel {
    dropDown = nil;
}

- (void)dropDownListView:(NIDropDownCustom *)dropDownListView didSelectIndexPath:(NSIndexPath *)indexPath atButton:(UIButton *)button{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
