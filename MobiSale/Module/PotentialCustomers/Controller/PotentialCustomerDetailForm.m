//
//  PotentialCustomerDetailForm.m
//  MobiSale
//
//  Created by ISC-DanTT on 11/20/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "PotentialCustomerDetailForm.h"
#import "PotentialCustomerDetailRecord.h"
#import "NoteInfoPotentialCustomerViewController.h"
#import "DetailInfoPotentialCustomerViewController.h"
#import "RegisteredFormController.h"
#import "DetailRegisteredForm.h"
#import "CategorySurveyListViewController.h"
#import "ShareData.h"
#import "Common.h"
#import "KeyValueModel.h"
#import "PotentialAdvisoryResultViewController.h"
#import "InfoContractViewController.h"
#import "ListRegisteredFormViewController.h"
#import "ListSchedulePotentialViewController.h"

@interface PotentialCustomerDetailForm ()<UIActionSheetDelegate>{
    int statusMenu;
    PotentialCustomerDetailRecord *record;
    NSMutableArray *arrServiceType;
    NSMutableArray *arrISPType;
    KeyValueModel *selectedService, *selectedISP;
}

@end

@implementation PotentialCustomerDetailForm
@synthesize ID,Supporter;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"THÔNG TIN KH";
    self.screenName = @"CHI TIẾT KHÁCH HÀNG TIỀM NĂNG";
    arrISPType = [Common getISP];
    arrServiceType = [Common getServiceType];
    [self setType];
    // add refresh control when scroll top of scrollview
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.scorllView addSubview:refreshControl];
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.menuContainerViewController setRightMenuWidth:0];
    // load data
    [self loadData];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    self.menuContainerViewController.rightMenuWidth = 0;
}

- (void)setType{
    //    self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
    
    UITapGestureRecognizer *tapScreem = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnScreem:)];
    [self.view addGestureRecognizer:tapScreem];
    
    statusMenu = 0;
    [self.menuView setFrame:CGRectMake(0, [SiUtils getScreenFrameSize].height, self.menuView.frame.size.width, self.menuView.frame.size.height)];
    
    self.txtNote.editable = NO;
    self.txtNote.layer.borderColor= [UIColor colorWithRed:215.0/255.0 green:215.0/255.0 blue:215.0/255.0 alpha:1].CGColor;
    self.txtNote.layer.borderWidth = 0.7f;
    self.txtNote.layer.cornerRadius = 6.0f;
    
    self.textAddress.editable = NO;
    self.textAddress.layer.borderColor= [UIColor colorWithRed:215.0/255.0 green:215.0/255.0 blue:215.0/255.0 alpha:1].CGColor;
    self.textAddress.layer.borderWidth = 0.7f;
    self.textAddress.layer.cornerRadius = 6.0f;
}

#pragma mark - load data
-(void)loadData{
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    [self showMBProcess];
    ShareData *shared = [ShareData instance];
    [shared.potentialCustomerProxy GetPotentialCustomerDetailWithID:self.ID Completehander:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self hideMBProcess];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        if(result == nil){
            [self showAlertBox:@"Thông Báo" message:Mesage_DataEmpty];
            [self hideMBProcess];
            return;
        }
        
        [self hideMBProcess];
        
        if([errorCode isEqualToString:@"0"]){
            record = [result objectAtIndex:0];
            [self setupViewData];
        }
        
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}

- (void)setupViewData {
    self.lblRegCode.text = record.RegCode;
    self.lblFullName.text     = record.FullName;
    self.lblPassport.text     = record.Passport;
    self.lblTaxID.text        = record.TaxID;
    self.lblPhone1.text       = record.Phone_1;
    self.lblEmail.text        = record.Email;
    self.lblFacebook.text     = record.Facebook;
    self.textAddress.text     = record.Address;
    self.lblCreateDate.text   = record.CreateDate;
    self.txtNote.text         = record.Note;
    self.lblLocation.text     = record.Latlng;
    self.lblServiceType.text  = record.ServiceTypeName;
    self.lblISPStartDate.text = record.ISPStartDate;
    self.lblISPEndDate.text   = record.ISPEndDate;
    self.lblISPIPTV.text      = record.ISPIPTV;
    self.lblISPType.text      = [self loadISP:record.ISPType];
    // Nếu khách hàng tiềm năng là do Sale tạo hoặc đã nhận khách hàng (acceptStatus = 1):
    /// Show menu chức năng.
    if (record.source == 0 || record.acceptStatus == 1) {
        self.navigationItem.rightBarButtonItem = [self rightMenuBarButtonItem];
        // Nếu KHTN đã có hợp đồng trước đó:
        /// Show thông tin hợp đồng để bán thêm dịch vụ.
        if (record.contract.length > 0) {
            [self.btnRegType setTitle:@"THÔNG TIN HỢP ĐỒNG" forState:UIControlStateNormal];
            return;
        }
        // Nếu KHTN chưa có hợp đồng nào:
        /// Tạo phiếu đăng ký hoặc xem phiếu đăng ký đã tạo
        if (record.RegCode.length > 0) {
            [self.btnRegType setTitle:@"XEM PHIẾU ĐĂNG KÝ" forState:UIControlStateNormal];
        } else {
            [self.btnRegType setTitle:@"TẠO PHIẾU ĐĂNG KÝ" forState:UIControlStateNormal];
        }
    // Nếu khách hàng tiềm năng là do tổng đài gửi và chưa nhận khách hàng (acceptStatus = 0):
    } else {
        /// Nếu load thông tin khách hàng từ danh sách thì Show chức năng "Nhận".
        if (_potentialType == Normal) {
            UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Nhận" style:UIBarButtonItemStyleBordered target:self action:@selector(rightSideMenuButtonPressed:)];
            [rightBarButtonItem setTintColor:[UIColor colorMain]];
            self.navigationItem.rightBarButtonItem = rightBarButtonItem;
            
            return;
        }
        /// Nếu load thông tin khách hàng từ notify/code, thì accept khách hàng.
        [self acceptPotentitalCustomer];
    }
}

- (void)pushToContractInfoView {
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"InfoContract" bundle:nil];
    InfoContractViewController *vc = [[InfoContractViewController alloc] init];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"InfoContract"];
    vc.contract = record.contract;
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Gọi API AcceptPotential
- (void)acceptPotentitalCustomer {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    ShareData *shared = [ShareData instance];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    [dict setObject:self.ID ?:@"0" forKey:@"ID"];
    [dict setObject:record.caseID forKey:@"CaseID"];
    
    [self showMBProcess];
    
    [shared.potentialCustomerProxy acceptPotential:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self hideMBProcess];
            [self LogOut];
            return;
        }
        
        [self hideMBProcess];
        [self showAlertBoxWithDelayDismiss:@"Thông báo" message:message];
        // Nhận khách hàng thất bại
        if (result == nil || [errorCode integerValue] < 0) {
            return;
            
            // Nhận Khách hàng thành công
        } else {
            /// Reload data
            [self loadData];
        }
        
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}

#pragma mark - menu
- (void)rightSideMenuButtonPressed:(id)sender {
    if (record.source == 0 ||record.acceptStatus == 1 ) {
        if(statusMenu  == 0){
            [self.menuView setShadow];
            [self.view addSubview:self.menuView];
            [UIView animateWithDuration:0.4
                                  delay:0.1
                                options: UIViewAnimationOptionTransitionNone
                             animations:^{
                                 
                                 self.menuView.frame = CGRectMake(0, [SiUtils getScreenFrameSize].height - self.menuView.frame.size.height, self.menuView.frame.size.width, self.menuView.frame.size.height);
                             }
                             completion:^(BOOL finished){
                                 
                             }];
            
            statusMenu = 1;
        }else if(statusMenu == 1){
            [self tapOnScreem:sender];
        }
        
    } else {
        
        [self showActionSheetWithTitle:@"Bạn chắc chắn muốn nhận Khách hàng này?" andTag:1];
    }
}

#pragma refresh table view
- (void)refresh:(UIRefreshControl *)refreshControl {
    // load data
    [self loadData];
    [refreshControl endRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - event button
// LÊN PHIẾU ĐĂNG KÝ
- (IBAction)btnLenPDK_Clicked:(id)sender {
    if (record.contract.length > 0) {
        // Xem Thông tin hợp đồng
        [self pushToContractInfoView];
        return;
    }
    if (record.RegCode.length > 0) {
        // XEM PĐK
        DetailRegisteredForm *vc = [[DetailRegisteredForm alloc]initWithNibName:@"DetailRegisteredForm" bundle:nil];
        vc.Id = record.RegID;
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
    // TẠO PĐK
//    RegisteredFormController *vc = [[RegisteredFormController alloc]initWithNibName:@"RegisteredFormController" bundle:nil];
    ListRegisteredFormViewController *vc = [[ListRegisteredFormViewController alloc] initWithNibName:@"ListRegisteredFormViewController" bundle:nil];
    vc.rc = [self convertPotentialCustomerRecordToRegistrationRecord:record];
    vc.Id = record.ID;
    
    [self.navigationController pushViewController:vc animated:YES];
    
    
}

// CẬP NHẬT
- (IBAction)btnUpdate_clicked:(id)sender {
    ShareData *shared = [ShareData instance];
    shared.potentialRecord = record;
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"PotentialCustomerDetail" bundle:nil];
    UIViewController * initialVC = [storyboard instantiateInitialViewController];
    initialVC.title = @"CẬP NHẬT THÔNG TIN KH";
    //UITabBarController *tabBarController = (UITabBarController *)initialVC;
    //tabBarController.selectedIndex = 1;
    [self.navigationController pushViewController:initialVC animated:YES];
}

// KHẢO SÁT THÔNG TIN
- (IBAction)btnSurvey_clicked:(id)sender {
    //    SurveyListViewController *vc = [[SurveyListViewController alloc] initWithNibName:@"SurveyListViewController" bundle:nil];
    CategorySurveyListViewController *vc = [[CategorySurveyListViewController alloc] init];
    vc.PotentialID = self.ID;
    [self.navigationController pushViewController:vc animated:YES];
}

// KẾT QUẢ TƯ VẤN
- (IBAction)btnAdvisoryResult_clicked:(id)sender {
    PotentialAdvisoryResultViewController *vc = [[PotentialAdvisoryResultViewController alloc] init];
    vc.potentialID = self.ID;
    vc.potentialRecord = record;
    [self.navigationController pushViewController:vc animated:YES];
}

//DS Lịch Hẹn KHTN

- (IBAction)btnListSchedulePotantial_clicked:(id)sender {

    
    ListSchedulePotentialViewController *ls = [[ListSchedulePotentialViewController alloc] initWithNibName:@"ListSchedulePotentialViewController" withID:self.ID andSupporter:self.Supporter];
    
    
    [self.navigationController pushViewController:ls animated:true];
}

- (IBAction)btnCancel_clicked:(id)sender {
    [self tapOnScreem:sender];
    
}

- (void)tapOnScreem:(UITapGestureRecognizer *)sender{
    [UIView animateWithDuration:0.4
                          delay:0.1
                        options: UIViewAnimationOptionTransitionNone
                     animations:^{
                         
                         self.menuView.frame = CGRectMake(0, [SiUtils getScreenFrameSize].height, self.menuView.frame.size.width, self.menuView.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         
                     }];
    statusMenu = 0;
}

#pragma mark - UIActionSheetDelegate methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag == 1) {
        if (buttonIndex == 0) {
            [self acceptPotentitalCustomer];
            return;
        }
    }
}

- (void)showActionSheetWithTitle:(NSString *)title andTag:(NSInteger)tag {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:title delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
    actionSheet.tag = tag;
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
    
}

-(NSString *)loadService:(NSString *)Id {
    for (selectedService in arrServiceType) {
        if([Id isEqualToString:selectedService.Key]){
            return selectedService.Values;
        }
    }
    return @"";
}

-(NSString *)loadISP:(NSString *)Id {
    for (selectedISP in arrISPType) {
        if([Id isEqualToString:selectedISP.Key]){
            return selectedISP.Values;
        }
    }
    return @"";
}

- (RegistrationFormDetailRecord *)convertPotentialCustomerRecordToRegistrationRecord: (PotentialCustomerDetailRecord *)potentialRecord{
    RegistrationFormDetailRecord *rc = [[RegistrationFormDetailRecord alloc]init];
    rc.Address = potentialRecord.Address;
    rc.BillTo_City = (potentialRecord.BillTo_City == nil) ?@"":potentialRecord.BillTo_City;
    rc.BillTo_District = (potentialRecord.BillTo_District == nil) ?@"0":potentialRecord.BillTo_District;
    rc.BillTo_Number = (potentialRecord.BillTo_Number == nil) ?@"":potentialRecord.BillTo_Number;
    rc.BillTo_Street = (potentialRecord.BillTo_Street == nil) ?@"0":potentialRecord.BillTo_Street;
    rc.BillTo_Ward = (potentialRecord.BillTo_Ward == nil) ?@"0":potentialRecord.BillTo_Ward;
    
    rc.BookPortType = @"";
    rc.BranchCode = @"";
    rc.CableStatus = @"";
    rc.Contact = @"";
    rc.Contact_1 = potentialRecord.Contact_1;
    rc.Contact_2 = potentialRecord.Contact_2;
    rc.Contract = @"";
    rc.CreateBy = potentialRecord.CreateBy;
    rc.CreateDate = potentialRecord.CreateDate;
    rc.CurrentHouse = @"";
    rc.CusType = @"";
    rc.CusTypeDetail = potentialRecord.CustomerType;
    rc.CustomerType = @"0";
    rc.Deposit = @"";
    rc.DepositBlackPoint = @"";
    rc.Description = @"";
    rc.DescriptionIBB = @"";
    rc.DivisionID = @"";
    rc.Email = potentialRecord.Email;
    rc.EoC = @"";
    rc.Floor = potentialRecord.Floor;
    rc.FullName = potentialRecord.FullName;
    rc.ID = nil;
    rc.INSCable = @"";
    rc.IPTVBoxCount = @"0";
    rc.IPTVChargeTimes = @"0";
    rc.IPTVDeviceTotal = @"0";
    rc.IPTVHBOChargeTimes = @"0";
    rc.IPTVHBOPrepaidMonth = @"0";
    rc.IPTVKPlusChargeTimes = @"0";
    rc.IPTVKPlusPrepaidMonth = @"0";
    rc.IPTVPLCCount = @"0";
    rc.IPTVPackage = @"0";
    rc.IPTVPrepaid = @"0";
    rc.IPTVPrepaidTotal = @"0";
    rc.IPTVPromotionID = @"0";
    rc.IPTVRequestDrillWall = @"";
    rc.IPTVRequestSetUp = @"0";
    rc.IPTVReturnSTBCount = @"0";
    rc.IPTVStatus = @"";
    rc.IPTVTotal =@"0";
    rc.IPTVUseCombo = @"0";
    rc.IPTVVTCChargeTimes = @"0";
    rc.IPTVVTCPrepaidMonth = @"0";
    rc.IPTVVTVChargeTimes = @"0";
    rc.IPTVVTVPrepaidMonth = @"0";
    rc.ISPType = potentialRecord.ISPType;
    rc.Image = @"";
    rc.InDType = @"0";
    rc.InDoor = @"";
    rc.InternetTotal = @"";
    rc.Latlng = potentialRecord.Latlng;
    rc.LegalEntity = @"";
    rc.LocalType = @"";
    rc.LocalTypeName = @"";
    rc.LocationID = potentialRecord.LocationID;
    rc.Lot = potentialRecord.Lot;
    rc.MapCode = @"";
    rc.Modem = @"";
    rc.NameVilla = potentialRecord.NameVilla;
    rc.Note = potentialRecord.Note;
    rc.ODCCableType = @"";
    rc.ObjID = @"0";
    rc.ObjectType = potentialRecord.ISPType;
    rc.OutDType = @"";
    rc.OutDoor = @"";
    rc.PartnerID = @"";
    rc.Passport = potentialRecord.Passport;
    rc.PaymentAbility = @"";
    rc.PhoneNumber = @"";
    rc.Phone_1 = potentialRecord.Phone_1;
    rc.Phone_2 = potentialRecord.Phone_2;
    rc.Position = potentialRecord.Position;
    rc.PromotionID = @"";
    rc.PotentialID = potentialRecord.ID;
    rc.PromotionName = @"";
    rc.RegCode = potentialRecord.RegCode;
    rc.Room = potentialRecord.Room;
    rc.StatusDeposit = @"";
    
    //neu support rong -> support = login
    rc.Supporter = potentialRecord.Supporter;
    
//    if (potentialRecord.Supporter == @"") {
//        rc.Supporter = rc.UserName;
//    } else {
//        rc.Supporter = potentialRecord.Supporter;
//    }
    
    rc.TaxId = potentialRecord.TaxID;
    rc.Total = @"0";
    rc.TypeHouse = (potentialRecord.TypeHouse == nil) ?@"0":potentialRecord.TypeHouse;
    rc.Type_1 = @"0";
    rc.Type_2 = @"0";
    rc.UserName = @"";
    rc.TDName = @"";
    
    return rc;
}

@end
