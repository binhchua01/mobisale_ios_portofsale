//
//  ListSchedulePotentialModel.h
//  MobiSale
//
//  Created by Tran Vu on 10/13/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ListSchedulePotentialModel : NSObject
@property (strong,nonatomic) NSString *description;
@property (strong,nonatomic) NSString *fullName;
@property (nonatomic) NSUInteger potentialObjID;
@property (nonatomic) NSInteger potentialObjScheduleID;
@property (strong,nonatomic) NSString *scheduleDate;
@property (strong,nonatomic) NSString *supporter;
@property (nonatomic)NSInteger rowNumber;
@property (nonatomic)NSInteger currentPage;
@property (nonatomic)NSInteger totalRow;
@property (nonatomic)NSInteger totalPage;

- (id)initWithDictionary:(NSDictionary *)dict;
+ (id)sharedInstance;

@end
