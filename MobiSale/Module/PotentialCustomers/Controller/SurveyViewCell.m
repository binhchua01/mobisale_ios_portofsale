//
//  SurveyViewCell.m
//  MobiSale
//
//  Created by ISC-DanTT on 1/20/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "SurveyViewCell.h"
#import "TNCheckBoxGroup.h"
#import "TNRadioButtonGroup.h"
#import "SurveyModel.h"
#import "AnswerModel.h"

#define titleHeight 50;
@interface SurveyViewCell() <UITextViewDelegate> {
    TNRadioButtonGroup *radioButtonGroup;
    TNCheckBoxGroup    *checkBoxGroup;
    UITextView         *textViewEdit;
    UIButton           *btnPickerDate;
    CGFloat            cellHeight;
}

@end

@implementation SurveyViewCell
@synthesize cellIndex;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
    }
    return self;
}

- (void)setCellModel:(SurveyModel *)cellModel{
    _cellModel = cellModel;
    [self.contentView addSubview:[self createLabelTitle]];
    switch (_cellModel.type) {
        case SurveyTypeSpinner:
            [self createSpinner];
            break;
            
        case SurveyTypeRadio:
            [self.contentView addSubview:[self createRadioButtonGroup]];
            if (self.delegate) {
                [self.delegate updateCellHeight:cellHeight];
            }
            
            break;
            
        case SurveyTypeCheckBox:
            [self.contentView addSubview:[self createCheckBoxGroup]];
            if (self.delegate) {
                [self.delegate updateCellHeight:cellHeight];
            }
            
            break;
            
        case SurveyTypeEditText:
            [self.contentView addSubview:[self createEditText]];
            if (self.delegate) {
                [self.delegate updateCellHeight:cellHeight];
            }
            
            break;
            
        case SurveyTypePickerDate:
            [self.contentView addSubview:[self createButtonPickerDate]];
            if (self.delegate) {
                [self.delegate updateCellHeight:cellHeight];
            }
            
            break;
            
        default:
            break;
    }
}

- (UILabel *)createLabelTitle{
    CGRect frame = [UIScreen mainScreen].bounds;
    UILabel *lblTitle = [[UILabel alloc] init];
    lblTitle.frame = CGRectMake(15, 0, frame.size.width, 50);
    lblTitle.text       = StringFormat(@"%li. %@",(long)cellIndex + 1, _cellModel.surveyDescription);
    lblTitle.numberOfLines = 2;
    [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:16]];
    return lblTitle;
}

- (void)createSpinner {
    
}

- (TNRadioButtonGroup *)createRadioButtonGroup {
    radioButtonGroup = [[TNRadioButtonGroup alloc] initWithRadioButtonData:[self setRadioButtonGroup:_cellModel.values] layout:TNRadioButtonGroupLayoutVertical];
    radioButtonGroup.identifier = _cellModel.surveyID;
    radioButtonGroup.textPassiveColor = [UIColor grayColor];
    radioButtonGroup.controlPassiveColor = [UIColor cyanColor];
    radioButtonGroup.labelFont = [UIFont systemFontOfSize:14];
    [radioButtonGroup create];
    radioButtonGroup.position = CGPointMake(25, 50);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(radioButtonGroupUpdated:) name:SELECTED_RADIO_BUTTON_CHANGED object:radioButtonGroup];
    cellHeight = radioButtonGroup.frame.size.height + 50;
    return radioButtonGroup;
}

- (TNCheckBoxGroup *)createCheckBoxGroup {
    checkBoxGroup = [[TNCheckBoxGroup alloc] initWithCheckBoxData:[self setCheckBoxGroup:_cellModel.values] style:TNCheckBoxLayoutVertical];
    checkBoxGroup.identifier = _cellModel.surveyID;
    checkBoxGroup.labelColor = [UIColor grayColor];
    checkBoxGroup.labelFont = [UIFont systemFontOfSize:14];
    [checkBoxGroup create];
    checkBoxGroup.position = CGPointMake(25, 50);
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkBoxGroupUpdated:) name:GROUP_CHANGED object:checkBoxGroup];
    cellHeight = checkBoxGroup.frame.size.height + 50;
    return checkBoxGroup;
}

- (UITextView *)createEditText {
    CGRect frame = [UIScreen mainScreen].bounds;
    NSArray *arrAnswer = _cellModel.arrAnswer;
    AnswerModel *answer = [arrAnswer objectAtIndex:0];
    textViewEdit = [[UITextView alloc] init];
    textViewEdit.frame = CGRectMake(15, 50, frame.size.width - 25, 40);
    textViewEdit.delegate = self;
    textViewEdit.text = answer.SurveyValue;
    [textViewEdit setFont:[UIFont systemFontOfSize:14]];
    textViewEdit.editable = YES;
    textViewEdit.layer.borderColor= [UIColor colorWithRed:215.0/255.0 green:215.0/255.0 blue:215.0/255.0 alpha:1].CGColor;
    textViewEdit.layer.borderWidth = 0.7f;
    textViewEdit.layer.cornerRadius = 6.0f;
    cellHeight = textViewEdit.frame.size.height + 60;
    return textViewEdit;
}

- (UIButton *)createButtonPickerDate {
    CGRect frame = [UIScreen mainScreen].bounds;
    NSArray *arrAnswer = _cellModel.arrAnswer;
    AnswerModel *answer = [arrAnswer objectAtIndex:0];
    btnPickerDate = [[UIButton alloc] init];
    btnPickerDate.frame = CGRectMake(15, 50, frame.size.width - 150, 40);
    [btnPickerDate setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [btnPickerDate setTitle:answer.SurveyValue forState:UIControlStateNormal];
    btnPickerDate.titleLabel.font = [UIFont systemFontOfSize:14];
    btnPickerDate.layer.borderColor= [UIColor colorWithRed:215.0/255.0 green:215.0/255.0 blue:215.0/255.0 alpha:1].CGColor;
    btnPickerDate.layer.borderWidth = 0.7f;
    btnPickerDate.layer.cornerRadius = 6.0f;
    btnPickerDate.layer.shadowOffset = CGSizeMake(-2, -2);
    btnPickerDate.layer.shadowRadius = 5;
    btnPickerDate.layer.shadowOpacity = 0.5;
    [btnPickerDate addTarget:self action:@selector(button_clicked:) forControlEvents:UIControlEventTouchDown];
    cellHeight = btnPickerDate.frame.size.height + 60;
    return btnPickerDate;

}


- (NSMutableArray *) setRadioButtonGroup:(NSArray *)arr {
    NSMutableArray *arrRadio = [[NSMutableArray alloc] init];
    for (int i = 0; i < arr.count; i++) {
        NSDictionary * dict = [arr objectAtIndex:i] ?:nil;
        NSString *Selected = StringFormat(@"%@", [dict objectForKey:@"Selected"]);
        NSString *SurveyValue = [dict objectForKey:@"SurveyValue"];
        NSString *SurveyValueID = StringFormat(@"%@", [dict objectForKey:@"SurveyValueID"]);
        TNCircularRadioButtonData *radioButton = [TNCircularRadioButtonData new];
        radioButton.labelText = SurveyValue;
        radioButton.identifier = SurveyValueID;
        
        if ([Selected isEqualToString:@"0"]) {
            radioButton.selected = NO;
        }
        if ([Selected isEqualToString:@"1"]) {
            radioButton.selected = YES;
        }
        [arrRadio addObject:radioButton];
    }
    return arrRadio;
}

- (NSMutableArray *)setCheckBoxGroup:(NSArray *)arr {
    NSMutableArray *arrCheckBox = [[NSMutableArray alloc] init];
    for (int i = 0; i < arr.count; i++) {
        NSDictionary * dict = [arr objectAtIndex:i] ?:nil;
        NSString *Selected = StringFormat(@"%@", [dict objectForKey:@"Selected"]);
        NSString *SurveyValue = [dict objectForKey:@"SurveyValue"];
        NSString *SurveyValueID = StringFormat(@"%@", [dict objectForKey:@"SurveyValueID"]);
        TNImageCheckBoxData *checkBox = [[TNImageCheckBoxData alloc] init];
        checkBox.identifier = SurveyValueID;
        checkBox.labelText = SurveyValue;
        checkBox.checkedImage = [UIImage imageNamed:@"checked-25"];
        checkBox.uncheckedImage = [UIImage imageNamed:@"unchecked-25"];
        
        if ([Selected isEqualToString:@"0"]) {
            checkBox.checked = NO;
        }
        if ([Selected isEqualToString:@"1"]) {
            checkBox.checked = YES;
        }
        [arrCheckBox addObject:checkBox];
    }
    return arrCheckBox;
}

- (void)radioButtonGroupUpdated:(NSNotification *)notification {
    if (self.delegate) {
        NSLog(@"---radio button selected: %@ -ID: %@ -selected:%d",radioButtonGroup.selectedRadioButton.data.labelText, radioButtonGroup.selectedRadioButton.data.identifier, radioButtonGroup.selectedRadioButton.data.selected);
        NSLog(@"--- surveyDescription: %@ -surveyID: %@",_cellModel.surveyDescription,_cellModel.surveyID);
        NSString *surveyID = _cellModel.surveyID;
        NSMutableDictionary *dict  = [[NSMutableDictionary alloc] init];
        [dict setObject:radioButtonGroup.selectedRadioButton.data.labelText forKey:@"SurveyValue"];
        [dict setObject:radioButtonGroup.selectedRadioButton.data.identifier forKey:@"SurveyValueID"];
        [dict setObject:StringFormat(@"%d",radioButtonGroup.selectedRadioButton.data.selected) forKey:@"Selected"];
        NSMutableArray *arr = [NSMutableArray array];
        [arr addObject:dict];
        [self.delegate updateSurveyID:surveyID values:arr];
        
        //update cellModel
        [self updateCellModelWithSurveyValueID:radioButtonGroup.selectedRadioButton.data.identifier SurveyValue:radioButtonGroup.selectedRadioButton.data.labelText];
        [self.delegate updateCellModel:_cellModel atIndex:cellIndex];
    }
}

- (void)checkBoxGroupUpdated:(NSNotification *)notification {
    if (self.delegate) {
        NSLog(@"--- surveyDescription: %@ -surveyID: %@",_cellModel.surveyDescription,_cellModel.surveyID);
        NSString *surveyID = _cellModel.surveyID;
        NSMutableArray *arr = [NSMutableArray array];
        for (TNCheckBoxData *data in  checkBoxGroup.checkedCheckBoxes) {
            NSLog(@"--- check box selected: %@ -ID: %@ -selected:%d",data.labelText, data.identifier, data.checked);
            NSMutableDictionary *dict  = [[NSMutableDictionary alloc] init];
            [dict setObject:data.labelText forKey:@"SurveyValue"];
            [dict setObject:data.identifier forKey:@"SurveyValueID"];
            [dict setObject:StringFormat(@"%d",data.checked) forKey:@"Selected"];
            [arr addObject:dict];
            
            //update cellModel
            [self updateCellModelWithSurveyValueID:data.identifier SurveyValue:data.labelText];
            [self.delegate updateCellModel:_cellModel atIndex:cellIndex];
        }
        [self.delegate updateSurveyID:surveyID values:arr];
        
        //update cellModel
        [self updateCellModelWithSurveyValueID:radioButtonGroup.selectedRadioButton.data.identifier SurveyValue:radioButtonGroup.selectedRadioButton.data.labelText];
        [self.delegate updateCellModel:_cellModel atIndex:cellIndex];
    }
}

- (void) textViewDidEndEditing:(UITextView *)textView {
    if (self.delegate) {
        NSLog(@"--- surveyDescription: %@ -surveyID: %@",_cellModel.surveyDescription,_cellModel.surveyID);
        NSString *surveyID = _cellModel.surveyID;
        NSMutableArray *arr = [NSMutableArray array];
        NSMutableDictionary *dict  = [[NSMutableDictionary alloc] init];
        [dict setObject:textView.text forKey:@"SurveyValue"];
        [dict setObject:@"1" forKey:@"Selected"];
        [arr addObject:dict];
        [self.delegate updateSurveyID:surveyID values:arr];
        
        //update cellModel
        [self updateCellModelWithSurveyValueID:@"0" SurveyValue:textView.text];
        [self.delegate updateCellModel:_cellModel atIndex:cellIndex];
    }
}

- (void) button_clicked:(id)sender {
    if (self.delegate) {
        NSLog(@"--- surveyDescription: %@ -surveyID: %@",_cellModel.surveyDescription,_cellModel.surveyID);
        NSString *surveyID = _cellModel.surveyID;
        [self.delegate updatePickerDate:btnPickerDate surveyID:surveyID];
    }
}

// update _cellModel when click radio button or check box, edit text, select date
- (void)updateCellModelWithSurveyValueID:(NSString *)SurveyValueID SurveyValue:(NSString*)SurveyValue{
    NSMutableArray *arrAnswers = [NSMutableArray array];
    NSMutableArray *arrValues = [NSMutableArray array];
    
    for (AnswerModel *answer in  _cellModel.arrAnswer) {
        switch (_cellModel.type) {
            case SurveyTypeRadio:
                if ([answer.SurveyValueID isEqualToString:SurveyValueID]) {
                    answer.Selected = @"1";
                } else {
                    answer.Selected = @"0";
                }
                break;
            case SurveyTypeCheckBox:
                if ([answer.SurveyValueID isEqualToString:SurveyValueID]) {
                    answer.Selected = @"1";
                }
                break;
            case SurveyTypeEditText:
            case SurveyTypePickerDate:
            case SurveyTypeSpinner:
                answer.SurveyValue = SurveyValue;
                if (SurveyValue.length > 0) {
                    answer.Selected    = @"1";
                }
                else {
                    answer.Selected    = @"0";
                }
                break;
            default:
                break;
        }
        
        NSMutableDictionary *dict  = [[NSMutableDictionary alloc] init];
        [dict setObject:answer.SurveyValue forKey:@"SurveyValue"];
        [dict setObject:answer.SurveyValueID forKey:@"SurveyValueID"];
        [dict setObject:answer.Selected forKey:@"Selected"];
        
        [arrAnswers addObject:answer];
        [arrValues addObject:dict];
    }
    _cellModel.arrAnswer = [NSArray arrayWithArray:arrAnswers];
    _cellModel.values    = [NSArray arrayWithArray:arrValues];
}

@end
