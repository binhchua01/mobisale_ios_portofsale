//
//  DetailPotentialCustomerViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 11/18/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "DetailInfoPotentialCustomerViewController.h"
#import "ShareData.h"
#import "Common.h"
#import "NIDropDown.h"
#import "CategorySurveyListViewController.h"
#import "AppDelegate.h"

@interface DetailInfoPotentialCustomerViewController ()<NIDropDownDelegate, UIAlertViewDelegate, UITextViewDelegate, UITextFieldDelegate>{
    
    ShareData    *shared;
    NIDropDown   *dropDown;
    UIButton     *buttonSelected;
    UIAlertView  *alertCheckSave;
    BOOL         isButtonPressed;
    UIDatePicker *datePicker;
    NSString     *chooseISPStartDate;
    NSString     *chooseISPEndDate;
    
    IBOutlet NSLayoutConstraint *constraintsTop;
    IBOutlet NSLayoutConstraint *constraintsLeft;
    IBOutlet NSLayoutConstraint *constraintsRight;
}

@end

@implementation DetailInfoPotentialCustomerViewController
@synthesize record, city, ID;

- (void)viewDidLoad {
    [super viewDidLoad];
    shared = [ShareData instance];
    if (!IS_OS_8_OR_LATER) {
        constraintsTop.constant = 62;
        constraintsLeft.constant = 16;
        constraintsRight.constant = 0;
    }
    self.title = @"NHIỀU THÔNG TIN";
    self.screenName = @"KHÁCH HÀNG TIỀM NĂNG (LIST ITEM)";
    [self setType];
    
    
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // load data
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setType{
    [self.view setMultipleTouchEnabled:YES];
    UITapGestureRecognizer *tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:tapper];
    
    self.txtFullName.delegate = self;
    self.txtPassport.delegate = self;
    self.txtTaxID.delegate    = self;
    self.txtPhone1.delegate   = self;
    self.txtContact1.delegate = self;
    self.txtPhone2.delegate   = self;
    self.txtContact2.delegate = self;
    self.txtFax.delegate      = self;
    self.txtEmail.delegate    = self;
    self.txtFacebook.delegate = self;
    self.txtTiwtter.delegate  = self;
    self.txtLot.delegate      = self;
    self.txtFloor.delegate    = self;
    self.txtRoom.delegate     = self;
    self.txtNote.delegate     = self;
    self.txtNumberHouse.delegate = self;
    //vutt
    [self.txtFullName setKeyboardType:UIKeyboardTypeNamePhonePad];
    [self.txtPassport setKeyboardType:UIKeyboardTypeNamePhonePad];
    [self.txtTaxID setKeyboardType:UIKeyboardTypeNumberPad];
    [self.txtContact1 setKeyboardType:UIKeyboardTypeDefault];
    [self.txtContact2 setKeyboardType:UIKeyboardTypeDefault];
    [self.txtFax setKeyboardType:UIKeyboardTypeNumberPad];
    [self.txtFacebook setKeyboardType:UIKeyboardTypeNamePhonePad];
    [self.txtTiwtter setKeyboardType:UIKeyboardTypeNamePhonePad];
    [self.txtNote setKeyboardType:UIKeyboardTypeDefault];
    

    CGRect datePickerFrame = CGRectMake(0,0, self.view.frame.size.width, 200);
    datePicker = [[UIDatePicker alloc] initWithFrame:datePickerFrame];
    
    datePicker.datePickerMode = UIDatePickerModeDate;
    chooseISPStartDate = @"";
    chooseISPEndDate = @"";
    
    self.txtNote.layer.borderColor= [UIColor colorMain].CGColor;
    self.txtNote.layer.borderWidth = 0.7f;
    self.txtNote.layer.cornerRadius = 6.0f;
    
    self.txtAddress.layer.borderColor= [UIColor colorMain].CGColor;
    self.txtAddress.layer.borderWidth = 0.7f;
    self.txtAddress.layer.cornerRadius = 6.0f;
    self.txtAddress.editable = NO;
    
    // vutt11
    
}
// Vutt11 fix not reload Lo (do chua viet )
- (void)loadData{
    
    record= shared.potentialRecord;
    self.txtFullName.text = record.FullName;
    self.txtPassport.text = record.Passport;
    self.txtTaxID.text    = record.TaxID;
    self.txtPhone1.text   = record.Phone_1;
    self.txtPhone2.text   = record.Phone_2;
    self.txtContact1.text = record.Contact_1;
    self.txtContact2.text = record.Contact_2;
    self.txtEmail.text    = record.Email;
    self.txtFacebook.text = record.Facebook;
    self.txtTiwtter.text  = record.Twitter;
    self.txtFloor.text    = record.Floor;
    self.txtLot.text = record.Lot;
    self.txtFax.text = record.Fax;
    self.txtRoom.text     = record.Room;
    self.txtNote.text     = record.Note;
    self.txtAddress.text  = record.Address;
    self.txtNumberHouse.text = record.BillTo_Number;
    
    [self.btnISPStartDate setTitle:record.ISPStartDate forState:UIControlStateNormal];
    [self.btnISPEndDate setTitle:record.ISPEndDate forState:UIControlStateNormal];
    
    [self loadTypePhone:@"0" sender:self.btnPhone1Type];
    [self loadTypePhone:@"0" sender:self.btnPhone2Type];
    
    if (record.ID == nil || [record.ID isEqualToString:@"0"]) {
        [self loadDistrict:@"0"  WardId:@"0" Street:@"0"];
        [self loadTypeHouse:@"0"];
        [self loadPosition:@"0"];
        
        return;
    }
    
    [self loadDistrict:record.BillTo_District WardId:record.BillTo_Ward Street:record.BillTo_Street];
    [self loadTypeHouse:record.TypeHouse];
    [self loadPosition:record.Position];

}

// thông tin khách hàng
- (IBAction)btnTypePhone1_Clicked:(id)sender {
    [self pareArrayForKeyWith:self.arrPhone1 button:sender];
}

- (IBAction)btnTypePhone2_Clicked:(id)sender {
    [self pareArrayForKeyWith:self.arrPhone1 button:sender];
}

- (IBAction)btnDistrict_Clicked:(id)sender {
    isButtonPressed = YES;
    [self pareArrayForKeyWith:self.arrDistrict button:sender];
}

- (IBAction)btnCustomerType_Clicked:(id)sender {
    [self pareArrayForKeyWith:self.arrTypeCustomer button:sender];
    
}

- (IBAction)btnWard_Clicked:(id)sender {
    isButtonPressed = YES;
    [self pareArrayForKeyWith:self.arrWard button:sender];
}

- (IBAction)btnTypeHouse_Clicked:(id)sender {
    [self pareArrayForKeyWith:self.arrTypeHouse button:sender];
    
}

- (IBAction)btnStreet_Clicked:(id)sender {
    isButtonPressed = YES;
    [self pareArrayForKeyWith:self.arrStreet button:sender];
    
}

- (IBAction)btnPositionHouse_Clicked:(id)sender {
    [self pareArrayForKeyWith:self.arrPosition button:sender];
}

- (IBAction)btnNameVilla_Clicked:(id)sender {
    [self pareArrayForKeyWith:self.arrNameVilla button:sender];
}

// đối tượng khách hàng
- (IBAction)btnServiceType_Clicked:(id)sender {
    [self pareArrayForKeyWith:self.arrServiceType button:sender];
    
}

- (IBAction)btnISPType_Clicked:(id)sender {
    [self pareArrayForKeyWith:self.arrISPType button:sender];
    
}

- (IBAction)btnISPStartDate_Clicked:(id)sender {
    @try {
        [self limitDate:[NSDate date] minYear:-99 maxYear:0];
        [self showDatePicker:datePicker withButton:sender];
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
         [CrashlyticsKit recordCustomExceptionName:@"DetailInfoPotentialCustomerViewController - funtion (saveReceiveRemoteNotification - Error handle ISPStartDate)" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
    }
}

- (IBAction)btnISPEndDate_Clicked:(id)sender {
    @try {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        if (self.btnISPStartDate.titleLabel.text.length > 0) {
            NSDate *date = [dateFormatter dateFromString:self.btnISPStartDate.titleLabel.text];
            [self limitDate:date minYear:0 maxYear:99];
        } else {
            [self limitDate:[NSDate date] minYear:-99 maxYear:99];
        }
        [self showDatePicker:datePicker withButton:sender];
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
         [CrashlyticsKit recordCustomExceptionName:@"DetailInfoPotentialCustomerViewController - funtion (saveReceiveRemoteNotification - Error handle ISPEndDate)" reason: [[ShareData instance].currentUser.userName lowercaseString]frameArray:@[]];
    }
    
}

- (IBAction)btnSave_Clicked:(id)sender {
    alertCheckSave = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Bạn có muốn lưu thông tin khách hàng" delegate:self cancelButtonTitle:@"Có" otherButtonTitles:@"Không", nil];
    [alertCheckSave show];
}

- (void)loadTypePhone:(NSString *)Id sender: (UIButton *)sender {
    self.arrPhone1 = [Common getPhoneType];
    int i = 0;
    if([Id isEqualToString:@"0"]){
        selectedPhone1 = [self.arrPhone1 objectAtIndex:0];
        [sender setTitle:selectedPhone1.Values forState:UIControlStateNormal];
    }else {
        for (i = 0; i< self.arrPhone1.count; i++) {
            selectedPhone1 = [self.arrPhone1 objectAtIndex:i];
            if([Id isEqualToString:selectedPhone1.Key]){
                [sender setTitle:selectedPhone1.Values forState:UIControlStateNormal];
                return;
            }
        }
    }
}

- (void)loadTypeHouse:(NSString *)Id {
    self.arrTypeHouse = [Common getTypeHouse];
    if (![Id isEqualToString:@"2"]) {
        [self.btnNameVilla setEnabled:NO];
    }
    int i = 0;
    if([Id isEqualToString:@"0"]){
        selectedTypeHouse = [self.arrTypeHouse objectAtIndex:0];
        //vutt fix
        shared.potentialRecord.TypeHouse = selectedTypeHouse.Key;
        
        [self.btnTypeHouse setTitle:selectedTypeHouse.Values forState:UIControlStateNormal];
        [self showHideSelecttypeHouse];
    }else {
        for (i = 0; i < self.arrTypeHouse.count; i++) {
            selectedTypeHouse = [self.arrTypeHouse objectAtIndex:i];
            if([Id isEqualToString:selectedTypeHouse.Key]){
                [self.btnTypeHouse setTitle:selectedTypeHouse.Values forState:UIControlStateNormal];
                [self showHideSelecttypeHouse];
                return;
            }
        }
    }
}

- (void)loadDistrict:(NSString *)Id WardId:(NSString *)wardid Street:(NSString *)street {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    [self showMBProcess];
    NSString *locationid = shared.currentUser.LocationID;
    [shared.appProxy GetDistrictList:locationid completionHandler:^(id result, NSString *errorCode, NSString *message) {
        self.arrDistrict = result;
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        
        if (self.arrDistrict.count <= 0) {
            [self hideMBProcess];
            [self.btnDistrict setTitle:@"" forState:UIControlStateNormal];
            return;
        }
        
        if(self.arrDistrict.count > 0){
            int i = 0;
            if([Id isEqualToString:@"0"]){
                [self.btnDistrict setTitle:@"[Vui lòng chọn quận (huyện)]" forState:UIControlStateNormal];
            }else {
                for (i = 0; i< self.arrDistrict.count; i++) {
                    selectedDistrict = [self.arrDistrict objectAtIndex:i];
                    shared.potentialRecord.BillTo_District = selectedDistrict.Key;

                    if([Id isEqualToString:selectedDistrict.Key]){
                        [self.btnDistrict setTitle:selectedDistrict.Values forState:UIControlStateNormal];
                        shared.potentialRecord.BillTo_District = selectedDistrict.Key;

                        break;
                    }
                }
                
                
            }
            if ([selectedDistrict.Key length] >0) {
                [self LoadWard:wardid Street:street];
                return;
            }
            [self hideMBProcess];
        }
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@- %@",@"GetDistrictList",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}

- (void)LoadWard:(NSString *)Id Street:(NSString *)street {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        self.btnDistrict.selected = NO;
        return;
    }
    if (self.btnDistrict.selected == YES) {
        [self showMBProcess];
    }
    
    [shared.appProxy GetWardList:shared.currentUser.LocationID DistrictName:selectedDistrict.Key completionHandler:^(id result, NSString *errorCode, NSString *message) {
        self.arrWard = result;
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            self.btnDistrict.selected = NO;
            [self.btnWard setTitle:@"" forState:UIControlStateNormal];
            self.arrStreet = nil;
            [self.btnStreet setTitle:@"" forState:UIControlStateNormal];
            self.arrNameVilla = nil;
            [self.btnNameVilla setTitle:@"" forState:UIControlStateNormal];
            
            selectedWard = nil;
            selectedStreet = nil;
            selectedNameVilla = nil;
            return ;
        }
        
        if (self.arrWard.count <= 0) {
            [self hideMBProcess];
            [self.btnWard setTitle:@"" forState:UIControlStateNormal];
            self.arrStreet = nil;
            [self.btnStreet setTitle:@"" forState:UIControlStateNormal];
            self.arrNameVilla = nil;
            [self.btnNameVilla setTitle:@"" forState:UIControlStateNormal];
            
            selectedWard = nil;
            selectedStreet = nil;
            selectedNameVilla = nil;
            if (self.btnStreet.selected == YES || self.btnDistrict.selected == YES || self.btnWard.selected == YES) {
                self.btnDistrict.selected = NO;
                [self getAddress];
            }
            return;
        }
        
        if(self.arrWard.count > 0){
            int i = 0;
            if([Id isEqualToString:@"0"] || Id == nil){
                selectedWard = [self.arrWard objectAtIndex:0];
                shared.potentialRecord.BillTo_Ward     = selectedWard.Key;
                
                [self.btnWard setTitle:selectedWard.Values forState:UIControlStateNormal];
            }else {
                for (i = 0; i< self.arrWard.count; i++) {
                    selectedWard = [self.arrWard objectAtIndex:i];
                    shared.potentialRecord.BillTo_Ward     = selectedWard.Key;
                    if([Id isEqualToString:selectedWard.Key]){
                        [self.btnWard setTitle:selectedWard.Values forState:UIControlStateNormal];
                        break;
                    }
                }
            }
            [self loadStreet:street];
        }
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@- %@",@"GetWardList",[error localizedDescription]]];
        [self hideMBProcess];
        self.btnDistrict.selected = NO;
        self.arrWard = nil;
        [self.btnWard setTitle:@"" forState:UIControlStateNormal];
        self.arrStreet = nil;
        [self.btnStreet setTitle:@"" forState:UIControlStateNormal];
        self.arrNameVilla = nil;
        [self.btnNameVilla setTitle:@"" forState:UIControlStateNormal];
        selectedWard = nil;
        selectedStreet = nil;
        selectedNameVilla = nil;
        
    }];
}

- (void)loadStreet:(NSString *)Id {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        [self resetButtonSelected];
        return;
    }
    if (self.btnWard.selected == YES) {
        [self showMBProcess];
    }
    NSString *District=selectedDistrict.Key;
    NSString *selWard=selectedWard.Key;
    
    [shared.appProxy GetStreetOrCondominiumList:shared.currentUser.LocationID DistrictName:District Ward:selWard Type:@"0" completionHandler:^(id result, NSString *errorCode, NSString *message) {
        //[self hideMBProcess];
        self.arrStreet = result;
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(errorCode.length <1){
            errorCode=@"<null>";
        }
        
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            [self resetButtonSelected];
            [self.btnStreet setTitle:@"" forState:UIControlStateNormal];
            self.arrNameVilla = nil;
            [self.btnNameVilla setTitle:@"" forState:UIControlStateNormal];
            
            selectedStreet = nil;
            selectedNameVilla = nil;
            return ;
        }
        
        if (self.arrStreet.count <= 0) {
            [self hideMBProcess];
            [self.btnStreet setTitle:@"" forState:UIControlStateNormal];
            self.arrNameVilla = nil;
            [self.btnNameVilla setTitle:@"" forState:UIControlStateNormal];
            
            selectedStreet = nil;
            selectedNameVilla = nil;
            if (self.btnStreet.selected == YES || self.btnDistrict.selected == YES || self.btnWard.selected == YES) {
                [self resetButtonSelected];
                [self getAddress];
            }
            return;
        }
        
        if(self.arrStreet.count > 0){
            int i = 0;
            if([Id isEqualToString:@"0"]){
                selectedStreet = [self.arrStreet objectAtIndex:0];
                shared.potentialRecord.BillTo_Street = selectedStreet.Key;
                [self.btnStreet setTitle:selectedStreet.Values forState:UIControlStateNormal];
            }else {
                for (i = 0; i< self.arrStreet.count; i++) {
                    selectedStreet = [self.arrStreet objectAtIndex:i];
                    shared.potentialRecord.BillTo_Street = selectedStreet.Key;
                    if([Id isEqualToString:selectedStreet.Key]){
                        [self.btnStreet setTitle:selectedStreet.Values forState:UIControlStateNormal];
                        break;
                    }
                }
            }
            
            [self loadNameVilla:@"0"];
            
            if([selectedTypeHouse.Key isEqualToString:@"2"]) {
                // [self LoadNameVilla:@"0"];
            }
        }
    } errorHandler:^(NSError *error) {
        [self resetButtonSelected];
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetStreetOrCondominiumList-LoadStreet",[error localizedDescription]]];
        [self hideMBProcess];
        [self.btnStreet setTitle:@"" forState:UIControlStateNormal];
        self.arrNameVilla = nil;
        [self.btnNameVilla setTitle:@"" forState:UIControlStateNormal];
    }];
}

- (void)loadNameVilla:(NSString *)Id {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        [self resetButtonSelected];
        return;
    }
    if (self.btnStreet.selected == YES) {
        [self showMBProcess];
    }
    if (Id.length <1) {
        [self showAlertBox:@"Thông Báo" message:@"Không có địa chỉ"];
        [self hideMBProcess];
        [self resetButtonSelected];
        return;
    }
    [shared.appProxy GetStreetOrCondominiumList:shared.currentUser.LocationID DistrictName:selectedDistrict.Key Ward:selectedWard.Key Type:@"1" completionHandler:^(id result, NSString *errorCode, NSString *message) {
        self.arrNameVilla = result;
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            [self.btnNameVilla setTitle:@"" forState:UIControlStateNormal];
            
            selectedNameVilla = nil;
            
            [self resetButtonSelected];
            
            return ;
        }
        
        if (self.arrNameVilla.count <= 0) {
            [self.btnNameVilla setTitle:@"" forState:UIControlStateNormal];
            selectedNameVilla = nil;
        }
        
        if(self.arrNameVilla.count > 0){
            int i = 0;
            if([Id isEqualToString:@"0"]){
                selectedNameVilla = [self.arrNameVilla objectAtIndex:0];
                [self.btnNameVilla setTitle:selectedNameVilla.Values forState:UIControlStateNormal];
            }else {
                for (i = 0; i< self.arrNameVilla.count; i++) {
                    selectedNameVilla = [self.arrNameVilla objectAtIndex:i];
                    if([Id isEqualToString:selectedNameVilla.Key]){
                        [self.btnNameVilla setTitle:selectedNameVilla.Values forState:UIControlStateNormal];
                        return;
                    }
                }
            }
            
        }
        
        if (self.btnStreet.selected == YES || self.btnDistrict.selected == YES || self.btnWard.selected == YES) {
            [self getAddress];
            [self hideMBProcess];
            [self resetButtonSelected];
            return;
        }
        
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetStreetOrCondominiumList",[error localizedDescription]]];
        [self.btnNameVilla setTitle:@"" forState:UIControlStateNormal];
        selectedNameVilla = nil;
        [self resetButtonSelected];
    }];
}

- (void)loadPosition:(NSString *)Id {
    int i = 0;
    self.arrPosition = [Common getHouseCoordinate];
    if([Id isEqualToString:@"0"]){
        selectedPosition = [self.arrPosition objectAtIndex:0];
        [self.btnPositionHouse setTitle:selectedPosition.Values forState:UIControlStateNormal];
    }else {
        for (i = 0; i< self.arrPosition.count; i++) {
            selectedPosition = [self.arrPosition objectAtIndex:i];
            if([Id isEqualToString:selectedPosition.Key]){
                [self.btnPositionHouse setTitle:selectedPosition.Values forState:UIControlStateNormal];
                return;
            }
        }
    }
}

- (void)loadCustomerType:(NSString *)Id {
    self.arrTypeCustomer = [Common getCustomerType];
    int i = 0;
    if([Id isEqualToString:@"0"]){
        selectedTypeCustomer = [self.arrTypeCustomer objectAtIndex:0];
        [self.btnCustomerType setTitle:selectedTypeCustomer.Values forState:UIControlStateNormal];
    }else {
        for (i = 0; i< self.arrTypeCustomer.count; i++) {
            selectedTypeCustomer = [self.arrTypeCustomer objectAtIndex:i];
            if([Id isEqualToString:selectedTypeCustomer.Key]){
                [self.btnCustomerType setTitle:selectedTypeCustomer.Values forState:UIControlStateNormal];
                return;
            }
        }
    }
    
}

- (void)loadService:(NSString *)Id {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    self.arrServiceType = [Common getServiceType];
    int i = 0;
    if([Id isEqualToString:@"0"]){
        selectedService = [self.arrServiceType objectAtIndex:0];
        [self.btnServiceType setTitle:selectedService.Values forState:UIControlStateNormal];
    }else {
        for (i = 0; i< self.arrServiceType.count; i++) {
            selectedService = [self.arrServiceType objectAtIndex:i];
            if([Id isEqualToString:selectedService.Key]){
                [self.btnServiceType setTitle:selectedService.Values forState:UIControlStateNormal];
                return;
            }
        }
    }
    
}

- (void)loadISP:(NSString *)Id {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    self.arrISPType = [Common getISP];
    int i = 0;
    if([Id isEqualToString:@"0"]){
        selectedISP = [self.arrISPType objectAtIndex:0];
        [self.btnISPType setTitle:selectedISP.Values forState:UIControlStateNormal];
        [self.btnISPStartDate setEnabled:NO];
        [self.btnISPEndDate setEnabled:NO];
    }else {
        for (i = 0; i< self.arrISPType.count; i++) {
            selectedISP = [self.arrISPType objectAtIndex:i];
            if([Id isEqualToString:selectedISP.Key]){
                [self.btnISPType setTitle:selectedISP.Values forState:UIControlStateNormal];
                return;
            }
        }
        [self.btnISPStartDate setEnabled:YES];
        [self.btnISPEndDate setEnabled:YES];
    }
    
}

- (void)resetButtonSelected {
    self.btnStreet.selected = NO;
    self.btnWard.selected = NO;
    self.btnDistrict.selected = NO;
}

#pragma mark - NIDropDown
- (void)showDropDownAtButton:(UIButton *)sender withArrayData:(NSArray*)arrayData{
    [self.view endEditing:YES];
    buttonSelected = sender;
    if (dropDown == nil) {
        CGFloat height = 161;
        CGFloat width = 0;
        dropDown = [[NIDropDown alloc] showDropDownAtButton:sender height:&height width:&width data:arrayData images:nil animation:@"down"];
        dropDown.delegate = self;
        return;
    }
    [dropDown hideDropDown:sender];
    [self rel];
}

- (void)niDropDownDelegateMethod:(NIDropDown *)sender {
    [self rel];
}

-(void)rel {
    dropDown = nil;
}

- (void)dropDownListView:(NIDropDown *)dropDownListView didSelectIndexPath:(NSIndexPath *)indexPath atButton:(UIButton *)button{
    [self didDropDownSelected:indexPath.row atButton:button];
    
}

- (void)didDropDownSelected:(NSInteger)row atButton:(UIButton *)button{
    if (button == self.btnPhone1Type) {
        selectedPhone1 = [self.arrPhone1 objectAtIndex:row];
        return;
    }
    if (button == self.btnPhone2Type) {
        selectedPhone2 = [self.arrPhone1 objectAtIndex:row];
        return;
    }
    if (button == self.btnTypeHouse) {
        selectedTypeHouse = [self.arrTypeHouse objectAtIndex:row];
        shared.potentialRecord.TypeHouse = selectedTypeHouse.Key;
        if(![selectedTypeHouse.Key isEqualToString:@"2"]){
            self.txtFloor.text = @"";
            self.txtRoom.text = @"";
            self.txtLot.text = @"";
            self.txtNumberHouse.text = @"";
            shared.potentialRecord.BillTo_Number = @"";
        }else if([selectedTypeHouse.Key isEqualToString:@"2"]){
            self.txtFloor.text = record.Floor;
            self.txtRoom.text = record.Room;
            self.txtLot.text = record.Lot;
            shared.potentialRecord.BillTo_Number = @"";
            
        }
        [self showHideSelecttypeHouse];
        return;
    }
    if (button == self.btnCustomerType) {
        selectedTypeCustomer = [self.arrTypeCustomer objectAtIndex:row];
        shared.potentialRecord.CustomerType = selectedTypeCustomer.Key;
        return;
    }
    if (button == self.btnPositionHouse) {
        selectedPosition = [self.arrPosition objectAtIndex:row];
        shared.potentialRecord.Position = selectedPosition.Key;
        return;
    }
    if (button == self.btnDistrict) {
        selectedDistrict = [self.arrDistrict objectAtIndex:row];
        shared.potentialRecord.BillTo_District = selectedDistrict.Key;
        [self.btnDistrict setTitle:selectedDistrict.Values forState:UIControlStateNormal];
        self.btnDistrict.selected = YES;
        if ([selectedDistrict.Key length] >0 ) {
            [self LoadWard:@"0" Street:@"0"];
            return;
        }
        self.btnDistrict.selected = NO;
        [self.btnWard setTitle:@"" forState:UIControlStateNormal];
        [self.btnStreet setTitle:@"" forState:UIControlStateNormal];
        [self.btnNameVilla setTitle:@"" forState:UIControlStateNormal];
        selectedNameVilla = nil;
        selectedWard = nil;
        selectedStreet = nil;
        selectedNameVilla = nil;
        [self getAddress];
        self.arrWard = nil;
        self.arrStreet = nil;
        self.arrNameVilla = nil;
        return;
    }
    if (button == self.btnWard) {
        selectedWard = [self.arrWard objectAtIndex:row];
        shared.potentialRecord.BillTo_Ward     = selectedWard.Key;
        [self.btnWard setTitle:selectedWard.Values forState:UIControlStateNormal];
        self.btnWard.selected = YES;
        [self loadStreet:@"0"];
        
        return;
    }
    if (button == self.btnStreet) {
        selectedStreet = [self.arrStreet objectAtIndex:row];
        shared.potentialRecord.BillTo_Street = selectedStreet.Key;

        [self.btnStreet setTitle:selectedStreet.Values forState:UIControlStateNormal];
        self.btnStreet.selected = YES;
        [self loadNameVilla:@"0"];
        return;
    }
    if (button == self.btnNameVilla) {
        selectedNameVilla = [self.arrNameVilla objectAtIndex:row];
        shared.potentialRecord.NameVilla = selectedNameVilla.Key;
        [self.btnNameVilla setTitle:selectedNameVilla.Values forState:UIControlStateNormal];

    }
    if (button == self.btnServiceType) {
        selectedService = [self.arrServiceType objectAtIndex:row];
        shared.potentialRecord.ServiceType = selectedService.Key;
        return;
    }
    if (button == self.btnISPType) {
        selectedISP = [self.arrISPType objectAtIndex:row];
        shared.potentialRecord.ISPType = selectedISP.Key;
        if ([selectedISP.Key isEqualToString:@"0"]) {
            [self.btnISPStartDate setEnabled:NO];
            [self.btnISPEndDate setEnabled:NO];
            return;
        }
        [self.btnISPStartDate setEnabled:YES];
        [self.btnISPEndDate setEnabled:YES];
        return;
    }
}

#pragma  mark - hide address
-(void)showHideSelecttypeHouse {
    if([selectedTypeHouse.Key isEqualToString:@"1"]){
        [self.txtNumberHouse setEnabled:YES];
        [self.txtLot setEnabled:NO];
        [self.txtFloor setEnabled:NO];
        [self.txtRoom setEnabled:NO];
        [self.btnNameVilla setEnabled:NO];
        [self.btnPositionHouse setEnabled:NO];
        
        [self.txtNumberHouse setHidden:NO];
        [self.btnPositionHouse setHidden:YES];
        [self.txtLot setHidden:YES];
        [self.txtFloor setHidden:YES];
        [self.txtRoom setHidden:YES];
        [self.btnNameVilla setHidden:YES];
        [self.lblLot setHidden:YES];
        [self.lblFloor setHidden:YES];
        [self.lblRoom setHidden:YES];
        [self.lblNameVilla setHidden:YES];
        [self.lblNumberHouse setHidden:NO];
        [self.lblPositionHouse setHidden:YES];
        self.constranintPositionHouse.constant = -39;
        self.constranintNote.constant = -59;
        [UIView animateWithDuration:0.25
                         animations:^{
                             [self.view layoutIfNeeded];
                         }];
    }else if([selectedTypeHouse.Key isEqualToString:@"2"]){
        [self.txtNumberHouse setEnabled:NO];
        [self.txtLot setEnabled:YES];
        [self.txtFloor setEnabled:YES];
        [self.txtRoom setEnabled:YES];
        [self.btnNameVilla setEnabled:YES];
        [self.btnPositionHouse setEnabled:NO];
        
        [self.txtNumberHouse setHidden:YES];
        [self.btnPositionHouse setHidden:YES];
        [self.txtLot setHidden:NO];
        [self.txtFloor setHidden:NO];
        [self.txtRoom setHidden:NO];
        [self.btnNameVilla setHidden:NO];
        [self.lblLot setHidden:NO];
        [self.lblFloor setHidden:NO];
        [self.lblRoom setHidden:NO];
        [self.lblNameVilla setHidden:NO];
        [self.lblNumberHouse setHidden:YES];
        [self.lblPositionHouse setHidden:YES];
        self.constranintPositionHouse.constant = -88;
        self.constranintNote.constant = 29;
        [UIView animateWithDuration:0.25
                         animations:^{
                             [self.view layoutIfNeeded];
                         }];

    }else if([selectedTypeHouse.Key isEqualToString:@"3"]){
        [self.txtNumberHouse setEnabled:YES];
        [self.txtLot setEnabled:NO];
        [self.txtFloor setEnabled:NO];
        [self.txtRoom setEnabled:NO];
        [self.btnNameVilla setEnabled:NO];
        [self.btnPositionHouse setEnabled:YES];
        
        [self.txtNumberHouse setHidden:NO];
        [self.btnPositionHouse setHidden:NO];
        [self.txtLot setHidden:YES];
        [self.txtFloor setHidden:YES];
        [self.txtRoom setHidden:YES];
        [self.btnNameVilla setHidden:YES];
        [self.lblLot setHidden:YES];
        [self.lblFloor setHidden:YES];
        [self.lblRoom setHidden:YES];
        [self.lblNameVilla setHidden:YES];
        [self.lblNumberHouse setHidden:NO];
        [self.lblPositionHouse setHidden:NO];

        self.constranintPositionHouse.constant = 18;
        self.constranintNote.constant = -59;
        [UIView animateWithDuration:0.25
                         animations:^{
                             [self.view layoutIfNeeded];
                         }];

    }
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
    if (dropDown !=nil) {
        [dropDown hideDropDown:buttonSelected];
        [self rel];
    }
}

- (void)pareArrayForKeyWith:(NSArray*)arrInput button:(UIButton*)button{
    int i = 0;
    KeyValueModel *temp;
    NSMutableArray *arrOutput = [[NSMutableArray alloc] init];
    for (i = 0; i < arrInput.count; i++) {
        temp = [arrInput objectAtIndex:i];
        [arrOutput addObject:temp.Values];
    }
    [self showDropDownAtButton:button withArrayData:arrOutput];
}

- (void)getAddress{
    NSString *LocationName = [ShareData instance].currentUser.LocationName;
    if([selectedTypeHouse.Key isEqualToString:@"2"]) {
        self.txtAddress.text = [NSString stringWithFormat:@"L.%@ ,T.%@, P.%@, %@, %@, %@, %@, %@", self.txtLot.text, self.txtFloor.text, self.txtRoom.text, [self checkNull: selectedNameVilla.Values], [self checkNull:selectedStreet.Values], [self checkNull:selectedWard.Values], [self checkNull:selectedDistrict.Values], LocationName];
        return;
        
    }
    self.txtAddress.text = [NSString stringWithFormat:@"%@,%@,%@,%@,%@", self.txtNumberHouse.text, [self checkNull:selectedStreet.Values], [self checkNull:selectedWard.Values], [self checkNull:selectedDistrict.Values], LocationName];
    
}

-(NSString*)checkNull:(NSString *)stringInput {
    if ([stringInput isEqualToString:@"(null)"] || stringInput == nil) {
        stringInput = @"";
        return stringInput;
    }
    return stringInput;
}

#pragma mark - UITextField delegate

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField == self.txtFullName) {
        shared.potentialRecord.FullName = self.txtFullName.text ;
        return;
    }
    if (textField == self.txtPassport) {
        shared.potentialRecord.Passport = self.txtPassport.text ;
        return;
    }
    if (textField == self.txtTaxID) {
        shared.potentialRecord.TaxID = self.txtTaxID.text ;
        return;
    }
    if (textField == self.txtPhone1) {
        shared.potentialRecord.Phone_1 = self.txtPhone1.text ;
        return;
    }
    if (textField == self.txtPhone2) {
        shared.potentialRecord.Phone_2 = self.txtPhone2.text ;
        return;
    }
    if (textField == self.txtContact1) {
        shared.potentialRecord.Contact_1 = self.txtContact1.text ;
        return;
    }
    if (textField == self.txtContact2) {
        shared.potentialRecord.Contact_2 = self.txtContact2.text ;
        return;
    }
    if (textField == self.txtFax) {
        shared.potentialRecord.Fax = self.txtFax.text;
    }
    if (textField == self.txtEmail) {
        shared.potentialRecord.Email = self.txtEmail.text ;
        return;
    }
    if (textField == self.txtFacebook) {
        shared.potentialRecord.Facebook = self.txtFacebook.text ;
        return;
    }
    if (textField == self.txtTiwtter) {
        shared.potentialRecord.Twitter = self.txtTiwtter.text ;
        return;
    }
    if (textField == self.txtNumberHouse || self.txtLot || self.txtFloor || self.txtRoom) {
        isButtonPressed = YES;
        shared.potentialRecord.BillTo_Number = self.txtNumberHouse.text;
        shared.potentialRecord.Floor = self.txtFloor.text;
        shared.potentialRecord.Room  = self.txtRoom.text;
        shared.potentialRecord.Lot   = self.txtLot.text;
        [self getAddress];
        shared.potentialRecord.Address = self.txtAddress.text;
        return;
    }
    
}

#pragma mark - UITextView delegate
- (void)textViewDidEndEditing:(UITextView *)textView {
    
    if (textView == self.txtNote) {
        shared.potentialRecord.Note = self.txtNote.text;
       // shared.potentialRecord.Note = [shared.potentialRecord.Note stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        return;
    }
}
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
    }
    
    return YES;
}

#pragma mark - select date
- (void)limitDate:(NSDate*)date minYear:(NSInteger)min maxYear:(NSInteger)max {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:min];
    NSDate *minDate = [calendar dateByAddingComponents:comps toDate:date options:0];
    [comps setYear:max];
    NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:date options:0];
    [datePicker setMinimumDate:minDate];
    [datePicker setMaximumDate:maxDate];
}

-(void)setSelectedDateInField:(UIButton*)sender {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    [sender setTitle:[dateFormatter stringFromDate:datePicker.date] forState:UIControlStateNormal];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
}

-(void)showDatePicker:(UIDatePicker *)modeDatePicker withButton:(UIButton*)sender{
    buttonSelected = sender;
    UIView *viewDatePicker = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width+30, 200)];
    [viewDatePicker setBackgroundColor:[UIColor clearColor]];
    
    modeDatePicker.hidden = NO;
    modeDatePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"Vietnamese"];
    
    [viewDatePicker addSubview:modeDatePicker];
    
    if(IS_OS_8_OR_LATER){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"\n\n\n\n\n\n\n\n\n\n" preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *doneAction = [UIAlertAction actionWithTitle:@"Xong" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            [self setSelectedDateInField:sender];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Đóng" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            
        }];
        [alertController addAction:doneAction];
        [alertController addAction:cancelAction];
        [alertController.view addSubview:viewDatePicker];
        
        //[self.view.window.rootViewController presentViewController:alertController animated:YES completion:nil];
        [self.navigationController presentViewController:alertController animated:YES completion:nil];

        //[self presentViewController:alertController animated:YES completion:nil]; //error iOS 9
        return;
    }
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"\n\n\n\n\n\n\n\n\n\n" delegate:self cancelButtonTitle:@"Đóng" destructiveButtonTitle:@"Xong" otherButtonTitles:nil, nil];
    [actionSheet addSubview:viewDatePicker];
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
    return;
}

#pragma mark - convert string ISP date
- (NSDate *)convertStringToDate:(NSString *)dateStringInput{
    @try {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"GMT"];
        [dateFormatter setTimeZone:timeZone];
        NSDate *dateFromString = [[NSDate alloc] init];
        dateFromString = [dateFormatter dateFromString:dateStringInput];
        return dateFromString;
    }
    @catch (NSException *exception) {
        
         [CrashlyticsKit recordCustomExceptionName:@"DetailInfoPotentialCustomerViewController - funtion (saveReceiveRemoteNotification - Error handle convertringTodate)" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
        
    }
    
}

- (NSString *)convertDateToString:(NSDate*)dateInput{
    @try {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"GMT+7"];
        [dateFormatter setTimeZone:timeZone];
        NSString *stringDate = [dateFormatter stringFromDate:dateInput];
        return stringDate;
    }
    @catch (NSException *exception) {
         [CrashlyticsKit recordCustomExceptionName:@"DetailInfoPotentialCustomerViewController - funtion (saveReceiveRemoteNotification)-Error handle converdatetostring" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
    }
    
}

#pragma mark - UIActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self setSelectedDateInField:buttonSelected];
    }
}






@end
