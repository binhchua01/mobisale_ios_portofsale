//
//  CategorySurveyListViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 1/15/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "CategorySurveyListViewController.h"
#import "SurveyViewController.h"
#import "ShareData.h"
#import "Common_client.h"
#import "ListPotentialCustomersViewController.h"
#import "MapsListPotentialCustomersViewController.h"

@interface CategorySurveyListViewController ()

@property (strong, nonatomic) NSMutableArray      *arrCategory;
@property (strong, nonatomic) NSMutableArray      *arrData;
@property (strong, nonatomic) NSMutableDictionary *dictDataCategory;


@end

@implementation CategorySurveyListViewController {
    NSString *categoryID;
    BOOL     backedMainView;

}

@synthesize PotentialID;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"KHẢO SÁT THÔNG TIN KHTN";
    self.arrCategory      = [NSMutableArray array];
    self.arrData          = [NSMutableArray array];
    self.dictDataCategory = [NSMutableDictionary dictionary];
    // add refresh control when scroll top of tableview
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.listTableView addSubview:refreshControl];
    self.navigationItem.leftBarButtonItem = [self setBackBarButtonItem];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadData];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIBarButtonItem *)setBackBarButtonItem{
    UIImage *image = [SiUtils imageWithImageHeight:[[UIImage imageNamed:@"ic_pre"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] height:30];
    return [[UIBarButtonItem alloc]
            initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered
            target:self
            action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    if (self.saved == YES) {
        UINavigationController *nav = [((UINavigationController *)self.menuContainerViewController.centerViewController) initWithRootViewController:[self setViewPotentialCustomers] ];
        self.menuContainerViewController.centerViewController = nav;
        [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        self.saved = NO;
        backedMainView = YES;
        return;
    }
    if (backedMainView == YES) {
        backedMainView = NO;
        [self.navigationController popToRootViewControllerAnimated:YES];
        return;
    }

    [self.navigationController popViewControllerAnimated:YES];
}

- (UIViewController *)setViewPotentialCustomers{
    UITabBarController *tabBar = [[UITabBarController alloc]init];
    ListPotentialCustomersViewController *vc1 = [[ListPotentialCustomersViewController alloc] initWithNibName:@"ListPotentialCustomersViewController" bundle:nil];
    vc1.listPotentialType = Sale;
    ListPotentialCustomersViewController *vc2 = [[ListPotentialCustomersViewController alloc] initWithNibName:@"ListPotentialCustomersViewController" bundle:nil];
    vc2.listPotentialType = TongDai;
    MapsListPotentialCustomersViewController *vc3 = [[MapsListPotentialCustomersViewController alloc] initWithNibName:@"MapsListPotentialCustomersViewController" bundle:nil];
    [vc1.tabBarItem setTitle:@"DANH SÁCH"];
    [vc1.tabBarItem setImage:[UIImage imageNamed:@"list_32"]];
    [vc2.tabBarItem setTitle:@"18006000"];
    [vc2.tabBarItem setImage:[UIImage imageNamed:@"list_32"]];
    [vc3.tabBarItem setTitle:@"BẢN ĐỒ"];
    [vc3.tabBarItem setImage:[UIImage imageNamed:@"map_32"]];
    [tabBar setViewControllers:[NSArray arrayWithObjects:vc1,vc2,vc3, nil]];
    // add right bar button
    UIImage *image = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"ic_create_potentail_obj"] height:30];
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc]
                                       initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered
                                       target:self
                                       action:@selector(addNewPotentialCustomer:)];
    tabBar.navigationItem.rightBarButtonItem = rightBarButton;
    tabBar.navigationItem.leftBarButtonItem  = [self backBarButtonItem];
    [tabBar setTitle:@"DS KH TIỀM NĂNG"];
    
    return tabBar;
}

- (void)addNewPotentialCustomer:(id) sender{
    
    ShareData *shared = [ShareData instance];
    shared.potentialRecord = [[PotentialCustomerDetailRecord alloc] init];
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"PotentialCustomerDetail" bundle:nil];
    UIViewController * initialVC = [storyboard instantiateInitialViewController];
    initialVC.title = @"THÊM KH TIỀM NĂNG";
    [self.navigationController pushViewController:initialVC animated:YES];
    
}

- (void)refresh:(UIRefreshControl *)refreshControl {
    [self.arrData removeAllObjects];
    [self loadData];
    [refreshControl endRefreshing];
}

#pragma mark - Load data

- (void)loadData {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    [self showMBProcess];
    ShareData *shared = [ShareData instance];
    [shared.potentialCustomerProxy getPotentialObjSurveyList:shared.currentUser.userName potentialObjID:self.PotentialID  completehander:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self hideMBProcess];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        if(result == nil){
            [self showAlertBox:@"Thông Báo" message:Mesage_DataEmpty];
            [self hideMBProcess];
            return;
        }
        if([errorCode isEqualToString:@"0"]){
            self.arrData = result;
            [self getArrCategoryFromData:self.arrData];
            [self.listTableView reloadData];
        }
        [self hideMBProcess];
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}

- (void)getArrCategoryFromData:(NSArray*)data {
    for (int i = 0; i < data.count; i++) {
        NSDictionary * dict = [data objectAtIndex:i];
        NSString *CategoryName =[dict objectForKey:@"CategoryName"];
        if (![self.arrCategory containsObject: CategoryName]) {
            [self.arrCategory addObject:CategoryName];
        }
    }
}

- (NSMutableArray*)getDataForCategory:(NSString*)categoryName {
    NSMutableArray *arr = [[NSMutableArray alloc] init];
    for (int j = 0; j < self.arrData.count; j++) {
        NSDictionary * dict = [self.arrData objectAtIndex:j];
        NSString *newCategoryName =[dict objectForKey:@"CategoryName"];
            if ([newCategoryName isEqualToString:categoryName]) {
                [arr addObject:dict];
            }
        }
    return arr;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.arrCategory.count;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 59;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 5;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
        [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:18]];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.textLabel.numberOfLines = 0;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    }
    // Configure the cell...
    int count = 0;
    for (int i= 0; i < self.arrData.count; i++) {
        NSDictionary * dict = [self.arrData objectAtIndex:i];
        NSString *CategoryName =[dict objectForKey:@"CategoryName"];
        if ([CategoryName isEqualToString:[self.arrCategory objectAtIndex:indexPath.row]]) {
            count ++;
        }
    }
    cell.textLabel.text = StringFormat(@"%@",[self.arrCategory objectAtIndex:indexPath.row]);
    cell.detailTextLabel.text = StringFormat(@"%i",count);
    [cell.textLabel sizeToFit];
    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SurveyViewController *vc = [[SurveyViewController alloc] init];
    vc.title = [self.arrCategory objectAtIndex:indexPath.row];
    vc.arrData = [self getDataForCategory:vc.title];
    vc.PotentialID = self.PotentialID;
    [self.navigationController pushViewController:vc animated:YES];
}


@end
