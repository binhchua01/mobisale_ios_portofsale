//
//  SurveyViewCell.h
//  MobiSale
//
//  Created by ISC-DanTT on 1/20/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SurveyModel;

@protocol SurveyViewCellDelegate <NSObject>
// return cell height 
- (void)updateCellHeight:(CGFloat)cellHeight;
// update survey you want to save on server
- (void)updateSurveyID:(NSString *)surveyID values:(NSArray *)values;
// update cell model for view cell load not error
- (void)updateCellModel:(SurveyModel*)cellModel atIndex:(NSInteger)cellIndex;
// select date
- (void)updatePickerDate:(UIButton *)sender surveyID:(NSString *)surveyID;

@end

@interface SurveyViewCell : UITableViewCell

@property (nonatomic, strong) SurveyModel *cellModel;
@property (nonatomic, assign) NSInteger   cellIndex;
@property (nonatomic, strong) id<SurveyViewCellDelegate> delegate;

@end
