//
//  CategorySurveyListViewController.h
//  MobiSale
//
//  Created by ISC-DanTT on 1/15/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface CategorySurveyListViewController : BaseViewController

@property (strong, nonatomic) NSString *PotentialID;
@property (strong, nonatomic) IBOutlet UITableView *listTableView;
@property BOOL saved;

@end
