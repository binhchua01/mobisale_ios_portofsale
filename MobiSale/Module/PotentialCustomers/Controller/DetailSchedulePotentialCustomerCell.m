//
//  DetailSchedulePotentialCustomerCell.m
//  MobiSale
//
//  Created by Tran Vu on 10/11/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import "DetailSchedulePotentialCustomerCell.h"

@implementation DetailSchedulePotentialCustomerCell{

}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setUpView];
}

- (void)setUpView {
    
    self.viewContent.layer.cornerRadius = 3;
    self.viewContent.layer.borderWidth = 0.5;
    self.viewContent.layer.borderColor = [UIColor brownColor].CGColor;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (IBAction)btnEdit_clicked:(id)sender {
    
    
    [self.delegate pressendButtonSelected:EditSchedulePotential ScheduleDescription:_cellModel.description ScheduleDate:_cellModel.scheduleDate PotentialObjScheduleID:StringFormat(@"%ld",(long)_cellModel.potentialObjScheduleID)];
    
   
}

- (IBAction)btnDeleted_clicked:(id)sender {
    
   
    [self.delegate pressendButtonSelected:DeletedSchedulePotential ScheduleDescription:_cellModel.description ScheduleDate:_cellModel.scheduleDate PotentialObjScheduleID:StringFormat(@"%ld",(long)_cellModel.potentialObjScheduleID)];
    
}

@end
