//
//  MapsListPotentialCustomersViewController.h
//  MobiSale
//
//  Created by ISC-DanTT on 11/23/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "PotentialCustomerFormRecord.h"

@interface MapsListPotentialCustomersViewController :BaseViewController<GMSMapViewDelegate, CLLocationManagerDelegate, UIAlertViewDelegate>
@property GMSMapView *mapView_;
@property (strong, nonatomic) IBOutlet UIView *buttonView;
@property (strong, nonatomic) IBOutlet UIButton *btnMapType;

@property (nonatomic,strong) CLLocationManager *locationManager;

@property (nonatomic,strong) NSMutableArray *arrList;
@property (nonatomic,strong) NSString *flat;
@property (nonatomic,strong) NSString *txtLocationCustomer;

@property (nonatomic,retain) NSMutableArray *arrCustomers;
@property (nonatomic,strong) PotentialCustomerFormRecord *recordCustomer;
@end
