//
//  DetailSchedulePotentialCustomerCell.h
//  MobiSale
//
//  Created by Tran Vu on 10/11/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListSchedulePotentialModel.h"

typedef NS_ENUM(NSInteger,EditAndDeletedSchedulePotential) {
    EditSchedulePotential,
    DeletedSchedulePotential
};

@protocol DetailSchedulePotentialCustomerCellDelegate <NSObject>

- (void)pressendButtonSelected:(EditAndDeletedSchedulePotential)editAndDeleted ScheduleDescription:(NSString *)scheduleDescription ScheduleDate:(NSString *)scheduleDate PotentialObjScheduleID:(NSString *)potentialObjScheduleID;

@end

@interface DetailSchedulePotentialCustomerCell : UITableViewCell

@property(weak, nonatomic)id<DetailSchedulePotentialCustomerCellDelegate>delegate;

@property (weak, nonatomic) IBOutlet UILabel *lblScheduleDate;

@property (weak, nonatomic) IBOutlet UITextView *txvScheduleDescriptions;
@property (weak, nonatomic) IBOutlet UIView *viewContent;
@property (strong, nonatomic) ListSchedulePotentialModel *cellModel;

@end
