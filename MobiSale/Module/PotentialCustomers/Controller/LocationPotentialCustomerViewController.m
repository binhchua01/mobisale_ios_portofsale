//
//  LocationPotentialCustomerViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 11/18/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "LocationPotentialCustomerViewController.h"
#import "SiUtils.h"
#import "ShareData.h"
#import <AddressBook/AddressBook.h>
#import "Common.h"
#import "CategorySurveyListViewController.h"

@interface LocationPotentialCustomerViewController ()

@end

@implementation LocationPotentialCustomerViewController{
    CLLocation  *latLng;
    NSString    *lng, *lngMarker;
    CLLocationCoordinate2D position;
    NSString    *lngCoordinate;
    NSString    *address;
    BOOL        isDragMarker;
    UIButton    *buttonMapType;
    UIAlertView *alertCheckSave;
}

@synthesize mapView_;
@synthesize locationManager;
@synthesize record;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"TẠO ĐỘ";
    self.screenName = @"KHÁCH HÀNG TIỀM NĂNG (MAP)";
    self.record = [ShareData instance].potentialRecord;
    lng = self.record.Latlng;
    self.mapView_.delegate = self;
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    if(IS_OS_8_OR_LATER) {
        [locationManager requestWhenInUseAuthorization];
    }
    [self showAlertBox:@"Gợi ý" message:@"Nhấn giữ rồi di chuyển điểm đánh dấu màu xanh để cập nhật vị trí khách hàng"];
    [self loadMaps];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

#pragma Load Map and location

- (void)loadMaps {
    [self showCurrentLocation];
    double latitude=latLng.coordinate.latitude;
    double longitude=latLng.coordinate.longitude;
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latitude longitude:longitude zoom:15];
    if(!self.mapView_){
        mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height - 113) camera:camera];
    }
    mapView_.delegate = self;
    mapView_.myLocationEnabled = YES;
    mapView_.settings.compassButton = YES;
    mapView_.settings.myLocationButton = YES;
    [self.view addSubview:mapView_];
    [self addButtonMapType];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    if([CLLocationManager locationServicesEnabled]){
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied) {
            [self showAlertBox:@"Thông báo" message:@"Vui lòng cấp phát quyền truy cập GPS cho ứng dụng bằng cách vào Cài đăt ->Quyền riềng tư ->Dịch vụ định vị-> MobiSale"];
            return;
            
        }
        CLLocation* location = [locations lastObject];
        NSDate* eventDate = location.timestamp;
        NSTimeInterval howRecent = [eventDate timeIntervalSinceNow];
        if (fabs(howRecent) < 15.0) {
            latLng = location;
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:latLng.coordinate.latitude longitude:latLng.coordinate.longitude zoom:15];
            [mapView_ animateToCameraPosition:camera];
            [locationManager stopUpdatingLocation];
            //get address
            [self getAddressFromLatLon:latLng];
            //add marker at myLocation
            if (self.record.ID != nil && ![self.record.ID isEqualToString:@"0"]) {
                [self AddMarkerLocation:[NSString stringWithFormat:@"%f",latLng.coordinate.latitude] Longitude:[NSString stringWithFormat:@"%f",latLng.coordinate.longitude]];
            }
            lng = [NSString stringWithFormat:@"(%@,%@)", [NSString stringWithFormat:@"%f",latLng.coordinate.latitude],[NSString stringWithFormat:@"%f",latLng.coordinate.longitude]];
            //add marker customer
            [self loadPotentialCustomerLocation];
            
        }
    }
}

- (void)showCurrentLocation {
    mapView_.myLocationEnabled = YES;
    if(IS_OS_8_OR_LATER) {
        [locationManager requestWhenInUseAuthorization];
    }
    [locationManager startUpdatingLocation];
    
    if([CLLocationManager locationServicesEnabled]){
        NSLog(@"Location Services Enabled");
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            [self showAlertBox:@"Thông báo" message:@"Vui lòng cấp phát quyền truy cập GPS cho ứng dụng bằng cách vào Cài đăt ->Quyền riềng tư ->Dịch vụ định vị-> MobiSale"];
            
            return;
        }
    } else {
        
        //if (IS_OS_8_OR_LATER) {
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:@"Vui lòng bật GPS để sử dụng bản đồ" delegate:self cancelButtonTitle:@"Đóng" otherButtonTitles:@"Cài đặt",nil];
            alertView.tag = 99;
            [alertView show];
            return;
       // }
        [self showAlertBox:@"Thông báo" message:@"Vui lòng bật GPS để sử dụng bản đồ, bằng cách vào Cài đăt ->Quyền riềng tư ->Dịch vụ định vị"];
        return;
    }
    
}

#pragma mark - add marker
-(void)AddMarker:(NSString *)latitude Longitude:(NSString *)longitude Snipet:(NSString *)snipet Title:(NSString *)title {
    double lat = [latitude doubleValue];
    double ln = [longitude doubleValue];
    CLLocationCoordinate2D position2d = CLLocationCoordinate2DMake(lat, ln);
    GMSMarker *marker = [GMSMarker markerWithPosition:position2d];
    [marker setSnippet: [NSString stringWithFormat:@"%@", snipet]];
    [marker setTitle: title];
    marker.icon = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"ic_td"] height:32];
    [marker setDraggable:YES];
    marker.map = self.mapView_;
}

-(void)AddMarkerLocation:(NSString *)latitude Longitude:(NSString *)longitude {
    [self.mapView_ clear];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[latitude doubleValue]
                                                            longitude:[longitude doubleValue]
                                                                 zoom:13.0];
    [mapView_ animateToCameraPosition:camera];
    CLLocationCoordinate2D positions = CLLocationCoordinate2DMake([latitude doubleValue], [longitude doubleValue]);
    
    GMSMarker *marker = [GMSMarker markerWithPosition:positions];
    marker.title = @"Vị trí của tôi";
    marker.snippet = address;
    marker.icon = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"cus_location.png"] height:32] ;
    [marker setDraggable:NO];
    [self.mapView_ setSelectedMarker:marker];
    marker.map = self.mapView_;
}

#pragma mark - Map delegate

-(BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker{
    [mapView animateToLocation:marker.position];
    [self.mapView_ setSelectedMarker:marker];
    NSArray *nip = [marker.snippet componentsSeparatedByString:@":"];
    if(nip.count > 0){
        lngMarker = [nip objectAtIndex:0];
    }
    lngCoordinate = [NSString stringWithFormat:@"(%f,%f)", marker.position.latitude, marker.position.longitude];
    return YES;
}

-(void)mapView:(GMSMapView *)mapView didDragMarker:(GMSMarker *)marker{
    
}

- (void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker{
    marker.title = @"Vị trí mới";
    lng = [NSString stringWithFormat:@"(%f,%f)", marker.position.latitude, marker.position.longitude];
    isDragMarker = YES;
    [self getAdrressFromLatLong:marker.position.latitude lon:marker.position.longitude marker:marker];
}

-(void)mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker{
    
}

-(void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    
}

- (void)loadPotentialCustomerLocation {
    NSArray *arr;
    if (self.record.ID.length <= 0 || [self.record.ID isEqualToString:@"0"] || self.record.Latlng.length <= 0) {
        arr = [lng componentsSeparatedByString:@","];
    } else{
        arr = [self.record.Latlng componentsSeparatedByString:@","];
    }
    NSString *lat = [[arr objectAtIndex:0] stringByReplacingOccurrencesOfString:@"(" withString:@""];
    NSString *ln = [[arr objectAtIndex:1] stringByReplacingOccurrencesOfString:@")" withString:@""];
    if (self.record.ID == nil || [self.record.ID isEqualToString:@"0"]) {
        [self.mapView_ clear];
        [self AddMarker:lat Longitude:ln Snipet:@"" Title:@"Vị trí của tôi"];
        return;
    }
    [self AddMarker:lat Longitude:ln Snipet:self.record.Address Title:self.record.FullName ];
    
}

- (void)getAddressFromLatLon:(CLLocation *)bestLocation {
    NSLog(@"%f %f", bestLocation.coordinate.latitude, bestLocation.coordinate.longitude);
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder reverseGeocodeLocation:bestLocation
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       [self parseAddress:placemarks error:error];
                   }];
}

- (void)getAddressFromLatLong:(CLLocation *)bestLocation atMarker:(GMSMarker *)marker {
    NSLog(@"%f %f", bestLocation.coordinate.latitude, bestLocation.coordinate.longitude);
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [self showMBProcess];
    [geocoder reverseGeocodeLocation:bestLocation
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       [self parseAddress:placemarks error:error];
                       [marker setSnippet: address];
                       NSLog(@"hideMBProcess 2");
                       [self hideMBProcess];
                   }];
}

- (void)parseAddress:(NSArray *)placemarks error:(NSError *)error {
    if (error){
        NSLog(@"Geocode failed with error: %@", [error localizedDescription]);
    }
    for (CLPlacemark *placemark in placemarks){
        NSDictionary *addressDictionary = [placemark addressDictionary];
        NSString *houseNumber = [addressDictionary objectForKey:@"SubThoroughfare"]?:@"";
        NSString *street      = [addressDictionary objectForKey:@"Thoroughfare"]?:@"";
        NSString *ward        = [addressDictionary objectForKey:@"SubLocality"]?:@"";
        NSString *district    = [addressDictionary objectForKey:@"SubAdministrativeArea"]?:@"";
        NSString *state       = [addressDictionary objectForKey:@"State"]?:@"";
        address = StringFormat(@"%@ %@, %@, %@, %@",houseNumber,street,ward,district,state);
    }
    if (isDragMarker == YES || self.record.ID == nil || [self.record.ID isEqualToString:@"0"]) {
        ShareData *shared = [ShareData instance];
        shared.potentialRecord.Address = address;
        shared.potentialRecord.Latlng  = lng;
        
    }
}

- (void)addButtonMapType{
    buttonMapType = [[UIButton alloc] initWithFrame:CGRectMake(5,self.view.frame.size.height - 96,80,40)];
    [buttonMapType addTarget:self action:@selector(buttonMapType_Click:) forControlEvents:UIControlEventTouchUpInside];
    buttonMapType.backgroundColor = [UIColor whiteColor];
    [buttonMapType setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [buttonMapType setTitle:@"Vệ tinh" forState:UIControlStateNormal];
    buttonMapType.layer.shadowOffset = CGSizeMake(-2, -2);
    buttonMapType.layer.masksToBounds = NO;
    buttonMapType.layer.cornerRadius = 2;
    buttonMapType.layer.shadowRadius = 5;
    buttonMapType.layer.shadowOpacity = 0.5;
    [self.view addSubview:buttonMapType];
}

- (void)buttonMapType_Click:(id)sender{
    (buttonMapType.selected == NO) ? (buttonMapType.selected = YES):(buttonMapType.selected= NO);
    
    if(buttonMapType.selected){
        self.mapView_.mapType = kGMSTypeSatellite;
        [buttonMapType setTitle:@"Bản đồ" forState:UIControlStateNormal];
    } else {
        self.mapView_.mapType = kGMSTypeNormal;
        [buttonMapType setTitle:@"Vệ tinh" forState:UIControlStateNormal];
    }
}

- (void)getAdrressFromLatLong:(CGFloat)lat lon:(CGFloat)lon marker:(GMSMarker *)marker {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    ShareData *shared = [ShareData instance];
    [shared.potentialCustomerProxy getAdrressFromLatLong:lat lon:lon completehander:^(id result, NSString *errorCode, NSString *message) {
        if(result == nil){
            [marker setSnippet: message];
            [self hideMBProcess];
            return;
        }
        if(![errorCode isEqualToString:@"OK"]){
            [marker setSnippet: message];
            [self hideMBProcess];
            return ;
        }
        address = result;
        [marker setSnippet: address];

        if (isDragMarker == YES || self.record.ID == nil || [self.record.ID isEqualToString:@"0"]) {
            ShareData *shared = [ShareData instance];
            shared.potentialRecord.Address = address;
            shared.potentialRecord.Latlng = lng;
        }
        [self hideMBProcess];
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
        return;
    }];
}

#pragma mark - alert view delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (alertView.tag == 99 && buttonIndex == 1) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        return;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
