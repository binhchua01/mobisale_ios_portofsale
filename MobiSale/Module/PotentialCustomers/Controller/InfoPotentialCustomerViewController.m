//
//  InfoPotentialCustomerViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 1/28/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "InfoPotentialCustomerViewController.h"
#import "PotentialCustomerDetailRecord.h"
#import "CategorySurveyListViewController.h"
#import "SiUtils.h"
#import "ShareData.h"
#import "MBProgressHUD.h"
#import "Common.h"
#import "AppDelegate.h"

@interface InfoPotentialCustomerViewController ()<UIActionSheetDelegate,UIAlertViewDelegate>

@end

@implementation InfoPotentialCustomerViewController {
    PotentialCustomerDetailRecord *potentialRecord;
    MBProgressHUD *HUD;
    UIAlertView *alertView ;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [self rightSaveBarButtonItem];
    self.navigationItem.leftBarButtonItem  = [self backBarButtonItem];
    if ([self.title isEqualToString:@"CẬP NHẬT THÔNG TIN KH"]) {
        self.selectedIndex = 1;
    }
    // Do any additional setup after loading the view.
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - update potential customer info

- (UIBarButtonItem *)rightSaveBarButtonItem {
    UIImage *image = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"save_32_2"] height:30];
    return [[UIBarButtonItem alloc]
            initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered
            target:self
            action:@selector(rightSaveBarButton_Clicked:)];
    
}

- (UIBarButtonItem *)backBarButtonItem{
    UIImage *image = [SiUtils imageWithImageHeight:[[UIImage imageNamed:@"ic_pre"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] height:30];
    return [[UIBarButtonItem alloc]
            initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered
            target:self
            action:@selector(backButtonPressed:)];
}

#pragma mark - UIBarButtonItem save
-(void)rightSaveBarButton_Clicked:(id)sender {
    NSLog(@"Lưu thông tin khảo sát");
    UIActionSheet *noticeSave = [[UIActionSheet alloc] initWithTitle:@"Bạn chắc chắn muốn lưu thông tin khách hàng?" delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
    [noticeSave showInView:[UIApplication sharedApplication].keyWindow];
}

#pragma mark - UIBarButtonItem Callbacks
- (void)backButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSMutableDictionary *)setDataUpdate {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    if (potentialRecord.Address == nil) {
        potentialRecord.Address = @"";
    }
    potentialRecord.Address = [Common normalizeVietnameseString:potentialRecord.Address];
    if (potentialRecord.NameVilla == nil) {
        potentialRecord.NameVilla = @"";
    }
    if (potentialRecord.BillTo_District  == nil) {
        potentialRecord.BillTo_District  = @"";
    }
    if (potentialRecord.BillTo_Ward == nil) {
        potentialRecord.BillTo_Ward = @"";
    }
    if (potentialRecord.BillTo_Street == nil) {
        potentialRecord.BillTo_Street = @"";
    }
    if (potentialRecord.TypeHouse == nil) {
        potentialRecord.TypeHouse = @"";
    }
    if (potentialRecord.CustomerType == nil) {
        potentialRecord.CustomerType = @"";
    }
    if (potentialRecord.Position == nil) {
        potentialRecord.Position = @"";
    }
    if (potentialRecord.Latlng == nil) {
        potentialRecord.Latlng = @"";
    }
    if (potentialRecord.ServiceType == nil) {
        potentialRecord.ServiceType = @"";
    }
    if (potentialRecord.ISPType == nil) {
        potentialRecord.ISPType = @"";
    }
    
    //potentialRecord.Note = [potentialRecord.Note stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    //check white space for Note
    //NSCharacterSet *charSet = [NSCharacterSet whitespaceCharacterSet];
    //NSString *trimmedNoteDes = [potentialRecord.Note stringByTrimmingCharactersInSet:charSet];
    //potentialRecord.Note = trimmedNoteDes;
    
    potentialRecord.Phone_1 = [potentialRecord.Phone_1 stringByReplacingOccurrencesOfString:@" " withString:@""];
    potentialRecord.Phone_2 = [potentialRecord.Phone_2 stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    
    
    @try {
        //vutt11 fix tráiton on server 0
        [dict setObject:[ShareData  instance].currentUser.userName forKey:@"UserName"];
        [dict setObject:potentialRecord.ID               ?:@"0" forKey:@"ID"];
        [dict setObject:potentialRecord.Address          ?:@""  forKey:@"Address"];
        [dict setObject:potentialRecord.Note             ?:@""  forKey:@"Note"];
        [dict setObject:potentialRecord.FullName         ?:@""  forKey:@"FullName"];
        [dict setObject:potentialRecord.Phone_1          ?:@""  forKey:@"Phone1"];
        [dict setObject:potentialRecord.Latlng           ?:@""  forKey:@"Latlng"];
        [dict setObject:potentialRecord.BillTo_City      ?:@"" forKey:@"BillTo_City"];
        //0
        [dict setObject:potentialRecord.BillTo_District  ?:@"" forKey:@"BillTo_District"];
        [dict setObject:potentialRecord.BillTo_Number    ?:@"" forKey:@"BillTo_Number"];
        
        [dict setObject:potentialRecord.BillTo_Street    ?:@"" forKey:@"BillTo_Street"];
        [dict setObject:potentialRecord.BillTo_Ward      ?:@"" forKey:@"BillTo_Ward"];
        [dict setObject:potentialRecord.Contact_1        ?:@""  forKey:@"Contact1"];
        [dict setObject:potentialRecord.Contact_2        ?:@""  forKey:@"Contact2"];
        [dict setObject:potentialRecord.CustomerType     ?:@"0" forKey:@"CustomerType"];
        [dict setObject:potentialRecord.Email            ?:@""  forKey:@"Email"];
        [dict setObject:potentialRecord.Facebook         ?:@""  forKey:@"Facebook"];
        [dict setObject:potentialRecord.Fax              ?:@""  forKey:@"Fax"];
        [dict setObject:potentialRecord.Floor            ?:@""  forKey:@"Floor"];
        [dict setObject:potentialRecord.Lot              ?:@""  forKey:@"Lot"];
        //0 vu fit
        [dict setObject:potentialRecord.NameVilla        ?:@"" forKey:@"NameVilla"];
        [dict setObject:potentialRecord.Passport         ?:@""  forKey:@"Passport"];
        [dict setObject:potentialRecord.Phone_2          ?:@""  forKey:@"Phone2"];
        //0
        [dict setObject:potentialRecord.Position         ?:@"" forKey:@"Position"];
        [dict setObject:potentialRecord.Room             ?:@""  forKey:@"Room"];
        [dict setObject:potentialRecord.TaxID            ?:@""  forKey:@"TaxID"];
        [dict setObject:potentialRecord.Twitter          ?:@""  forKey:@"Twitter"];
        //
        [dict setObject:potentialRecord.TypeHouse        ?:@"" forKey:@"TypeHouse"];
        [dict setObject:potentialRecord.ServiceType      ?:@"0" forKey:@"ServiceType"];
        [dict setObject:potentialRecord.ISPType          ?:@"0" forKey:@"ISPType"];
        [dict setObject:potentialRecord.ISPStartDate     ?:@""  forKey:@"ISPStartDate"];
        [dict setObject:potentialRecord.ISPEndDate       ?:@""  forKey:@"ISPEndDate"];
        
        return  dict;
        
    }
    @catch (NSException *exception) {
        [CrashlyticsKit recordCustomExceptionName:@"InfoPotentialCustomerViewController - funtion (saveReceiveRemoteNotification)-Error handle Info Potential Customer" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
        
    }
}

- (void)checkUpdate {
    ShareData *shared = [ShareData instance];
    potentialRecord = [[PotentialCustomerDetailRecord alloc] init];
    potentialRecord = shared.potentialRecord;

    // CHECK TEN
    if ([potentialRecord.FullName isEqualToString:@""]||potentialRecord.FullName == nil) {
        [self showAlertBox:@"Thông báo" message:@"Bạn chưa nhập tên khách hàng"];
        return;
    }
    //CHECK SDT
    if ([potentialRecord.Phone_1 isEqualToString:@""] || potentialRecord.Phone_1 == nil ) {
        [self showAlertBox:@"Thông báo" message:@"Bạn chưa nhập số điện thoại"];
        return;
    }
    
    //CHECK DIA CHI
    if ([potentialRecord.BillTo_District isEqualToString:@""] || potentialRecord.BillTo_District == nil) {
        [self showAlertBox:@"Thông báo" message:@"Bạn vui lòng chọn Quận (Huyện)"];
        return;
    }
    
    if ([potentialRecord.BillTo_Ward isEqualToString:@""] || potentialRecord.BillTo_Ward == nil) {
        [self showAlertBox:@"Thông báo" message:@"Bạn vui lòng chọn Phường (Xã)"];
        return;
    }
    
    if ([potentialRecord.BillTo_Street isEqualToString:@""] || potentialRecord.BillTo_Street == nil) {
        [self showAlertBox:@"Thông báo" message:@"Bạn vui lòng chọn Đường"];
        return;
    }
    if ([potentialRecord.TypeHouse isEqualToString:@"1"]||[potentialRecord.TypeHouse isEqualToString:@"3"]) {
        
        if (potentialRecord.BillTo_Number.length <=0) {
            
            [self showAlertBox:@"Thông báo" message:@"Bạn chưa nhập số nhà hoặc chưa kết thúc quá trình nhập bấm Enter hoặc Done của keyboard để kết thúc quá trình nhập "];
            return;
        }
    
        
    }
    
    // vutt fix truong hop chon nha khong dia chi ma khong chonj vi tri nha
    if ([potentialRecord.TypeHouse isEqualToString:@"3"]) {
        if (potentialRecord.Position == nil ||[potentialRecord.Position isEqualToString:@""] || [potentialRecord.Position isEqualToString:@"0"]) {
            [self showAlertBox:@"Thông báo" message:@"Bạn vui lòng chọn vị trí nhà!!!"];
            return;
        }
    }
    
    //truong hop khong chon CCu - Lo - tang
    
    if ([potentialRecord.TypeHouse isEqualToString:@"2"]) {
        
        if (potentialRecord.Lot == nil || potentialRecord.Floor == nil || potentialRecord.Room == nil || [potentialRecord.Lot isEqualToString:@""]||[potentialRecord.Floor isEqualToString:@""]||[potentialRecord.Room isEqualToString:@""]) {
            
            [self showAlertBox:@"Thông báo" message:@"Bạn vui lòng điền đầy đủ thông tin C.Cư, C.Xá, Chợ,Thương xá"];
            return;
        }
        
    }
    
    if (potentialRecord.Passport.length > 0) {
        int len = (int)[potentialRecord.Passport length];
        if(len < 9){
            [self showAlertBox:@"Thông Báo" message:@"CMND/Passport không hợp lệ"];
            return;
        }
    }
    
//    if(potentialRecord.Phone_1.length > 0){
//        int len = (int)[potentialRecord.Phone_1 length];
//        NSString* num = [potentialRecord.Phone_1 substringToIndex:1];
//        if(len < 10 || len > 11 || ![num isEqualToString:@"0"]){
//            [self showAlertBox:@"Thông Báo" message:@"Số điện thoại liên hệ 1 không hợp lệ"];
//            return;
//        }
//    }
//
//    if(potentialRecord.Phone_2.length > 0){
//        int len =  (int)[potentialRecord.Phone_2 length];
//        NSString* num = [potentialRecord.Phone_2 substringToIndex:1];
//        if(len < 10 || len > 11 || ![num isEqualToString:@"0"]){
//            [self showAlertBox:@"Thông Báo" message:@"Số điện thoại liên hệ 2 không hợp lệ"];
//            return;
//        }
//    }
    
    
    if(potentialRecord.Email.length > 0){
        if(![self validateEmail:potentialRecord.Email]){
            [self showAlertBox:@"Thông Báo" message:@"Email không hợp lệ"];
            return;
        }
    }
    
    
    [self savePotentialCustomerInfo];
}

- (void)savePotentialCustomerInfo {
    [self showMBProcess];
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    ShareData* shared = [ShareData instance];
    NSMutableDictionary *dictInfo = [NSMutableDictionary dictionary];
    dictInfo = [self setDataUpdate];
    [shared.potentialCustomerProxy updatePotentialCustomerInfo:dictInfo Completehander:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi: %@",message]];
            [self hideMBProcess];
            return ;
        }
       // [self showAlertBoxWithDelayDismiss:@"Thông báo" message:message];
        [self showAlertBox:@"Thông báo" message:message];
        alertView.tag = 55;
        [self hideMBProcess];
        if (potentialRecord.ID.length <= 0) {
            if (self.selectedIndex != 0) {
                NSString *ResultID = StringFormat(@"%@",[[result objectAtIndex:0] objectForKey:@"ResultID"]);
                CategorySurveyListViewController *vc = [[CategorySurveyListViewController alloc] init];
                vc.PotentialID = ResultID;
                vc.saved = YES;
                [self.navigationController pushViewController:vc animated:YES];
                return;
            }
        }
        //[self.navigationController popViewControllerAnimated:YES];
        
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
        return;
    }];
}

#pragma mark - validate email
- (BOOL)validateEmail:(NSString *)candidate {
    NSString *string= candidate;
    NSError *error = NULL;
    NSRegularExpression *regex = nil;
    regex = [NSRegularExpression regularExpressionWithPattern:@"\\w+@[a-zA-Z_]+?\\.[a-zA-Z]{2,6}"
                                                      options:NSRegularExpressionCaseInsensitive
                                                        error:&error];
    NSUInteger numberOfMatches = 0;
    numberOfMatches = [regex numberOfMatchesInString:string
                                             options:0
                                               range:NSMakeRange(0, [string length])];
    if(numberOfMatches != 0){
        return TRUE;
    }
    return FALSE;
}

#pragma mark - UIActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self checkUpdate];
    }
}

#pragma mark process bar
- (void) showMBProcess {
    if (self.navigationController.view == nil) {
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    } else {
        HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    }
    //[self.view addSubview:HUD];
    HUD.delegate = (id<MBProgressHUDDelegate>)self;
    HUD.labelText = @"Loading";
    [HUD show:YES];
}

- (void) hideMBProcess {
    [HUD hide:YES];
}

#pragma mark - show alert view method
- (void)showAlertBox:(NSString *)title message:(NSString *)message {
    
    alertView = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
    [alertView show];
}

-(void)ShowAlertErrorSession{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:@"Đã hết phiên làm việc. Vui long đăng nhập lại để tiếp tục" delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    [alert show];
    [self performSelector:@selector(dismiss:) withObject:alert afterDelay:3.0];
}

-(void)dismiss:(UIAlertView*)alert{
    [alert dismissWithClickedButtonIndex:0 animated:YES];
}

-(void)showAlertBoxWithDelayDismiss:(NSString*)tile message:(NSString*)message {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:tile message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:nil];
    [alert show];
    [self performSelector:@selector(dismiss:) withObject:alert afterDelay:2.0];
}

#pragma mark - logOut
-(void)LogOut {
    self.menuContainerViewController.menuState = MFSideMenuStateClosed;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:[NSBundle mainBundle]];
    if(IS_IPHONE4){
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone4" bundle:[NSBundle mainBundle]];
    }
    MFSideMenuContainerViewController *container = self.menuContainerViewController;
    
    NSString *firstViewControllerName = @"LoginViewController";
    
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:firstViewControllerName];
    [container setCenterViewController:navigationController];
    //container.enableMenu = NO;
    
}

//Vutt11

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0 :
            if (alertView.tag == 55) {
                [self.navigationController popViewControllerAnimated:YES];
                break;
            }
            break;
            
        default:
            break;
    }
}


@end
