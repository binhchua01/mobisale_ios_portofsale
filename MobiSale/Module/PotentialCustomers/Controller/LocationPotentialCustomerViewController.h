//
//  LocationPotentialCustomerViewController.h
//  MobiSale
//
//  Created by ISC-DanTT on 11/18/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "PotentialCustomerDetailRecord.h"
#import "BaseViewController.h"

@interface LocationPotentialCustomerViewController : BaseViewController <GMSMapViewDelegate, CLLocationManagerDelegate, UIAlertViewDelegate>
@property GMSMapView *mapView_;
@property (nonatomic,strong) CLLocationManager *locationManager;

@property (nonatomic,strong) NSMutableArray *arrList;
@property (nonatomic,strong) NSString *flat;
@property (nonatomic,strong) NSString *txtLocationCustomer;
@property (nonatomic,strong) PotentialCustomerDetailRecord *record;

@end
