//
//  ListSchedulePotentialViewController.m
//  MobiSale
//
//  Created by Tran Vu on 10/11/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import "ListSchedulePotentialViewController.h"
#import "Common_client.h"
#import "ShareData.h"
#import "ListSchedulePotentialModel.h"
#import "DetailSchedulePotentialCustomerCell.h"
#import "DemoTableFooterView.h"
#import "AddNewSchedulePotentialViewController.h"
#import "AppDelegate.h"

@interface ListSchedulePotentialViewController ()<PickerViewControllerDelegate,UITableViewDelegate,UITableViewDataSource,DetailSchedulePotentialCustomerCellDelegate,UIAlertViewDelegate>
@property (nonatomic,strong) NSArray* arrayData;

@end

@implementation ListSchedulePotentialViewController{
    UIButton *selectedButtonDate;
    NSMutableArray *arrayDataSearch;
    ListSchedulePotentialModel *record;
    CGFloat             startOffset;
    BOOL                isSearchViewShow; // show/hide search
    NSInteger totalPage;
    NSInteger currentPage;
    DemoTableFooterView *footerView;
    UIAlertView *alertView;
    UIButton *btnSenser;
}
@synthesize ScheduleDate,ScheduleDesription,PotentialObjScheduleID;
@synthesize Supporter;

- (void)viewDidLoad {
    [super viewDidLoad];
    arrayDataSearch = [NSMutableArray array];
   self.title = @"DS Lịch Hẹn KHTN";
    PickerViewController *pc = [[PickerViewController alloc] initFromNib];
    pc.pickerType = DateAndTimePickerType;
    pc.delegate = self;
    [self setUpView];
   
    [self setUpBeginDatePicker];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [arrayDataSearch removeAllObjects];
    
    NSString *strFromDate = [NSString stringWithFormat:@"%@ %@",self.btnFromDate.titleLabel.text,@"00:00:00"];
    NSString *strToDate = [NSString stringWithFormat:@"%@ %@",self.btnToDate.titleLabel.text,@"23:59:59"];
    
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: @"dd/MM/yyyy HH:mm:ss"];
    NSDate *dateTo = [dateFormatter dateFromString:strToDate];
    
    NSCalendar *calerda = [NSCalendar currentCalendar];
    NSDate *dateMonth = [calerda dateByAddingUnit:NSCalendarUnitDay value:30 toDate:dateTo options:0];
    
    strToDate = [dateFormatter stringFromDate: dateMonth];
    
    
    [self getData:@"" ToDate:@"" PageNumber:1];
   
}

- (void)setUpBeginDatePicker {
    
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat: @"dd/MM/yyyy"];
    NSString *dateString = [dateFormatter stringFromDate: [NSDate date]];
    
    
    [self.btnFromDate setTitle:dateString forState:UIControlStateNormal];
    [self.btnToDate setTitle:dateString forState:UIControlStateNormal];
    
    
}

- (void)setUpView {
    
    startOffset = - 20;
    isSearchViewShow = YES;
    self.searchView.layer.borderColor  = [UIColor colorWithRed:0.133 green:0.698 blue:0.569 alpha:1].CGColor;
    self.searchView.layer.borderWidth  = 1.0f;
    self.searchView.layer.cornerRadius = 4.0f;
    
    self.navigationItem.rightBarButtonItem = [self rightBarButtonItem];
    // set the custom view for "load more". See DemoTableFooterView.xib.
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DemoTableFooterView" owner:self options:nil];
    footerView = (DemoTableFooterView *)[nib objectAtIndex:0];
    //Refresh
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.listTableView addSubview:refreshControl];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



- (instancetype)initWithNibName:(NSString *)nibNameOrNil withID:(NSString *)potentialObjID andSupporter:(NSString *)supporter {
    
    self = [super initWithNibName:nibNameOrNil bundle:nil];
    if (self) {
        self.potentialObjID = potentialObjID;
        self.Supporter = supporter;
    }
    return self;
}

- (UIBarButtonItem *)rightBarButtonItem {
    UIImage *image = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"ic_create_potentail_obj"] height:30];
    return [[UIBarButtonItem alloc] initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(addNewPotential:)];
}

- (void)addNewPotential:(id)sender {
    
    AddNewSchedulePotentialViewController *ad = [[AddNewSchedulePotentialViewController alloc] initWithNibName:@"AddNewSchedulePotentialViewController" withID:self.potentialObjID withScheduleDescription:@"" withScheduleDate:@"" withPotentialObjSchedule:@"" andSupporter:self.Supporter];
    [self.navigationController pushViewController:ad animated:YES];
    
    
}
- (IBAction)btnFromDate_clicked:(id)sender {

    btnSenser = sender;
    [self showDatePicker];
}

- (IBAction)btnToDate_clicked:(id)sender {
    

    btnSenser = sender;
    [self showDatePicker];
    
}

- (void)setSelectedDateInField { 
    
    [super setSelectedDateInField];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    if (btnSenser == self.btnFromDate) {
        [self.btnFromDate setTitle:[dateFormatter stringFromDate:self.selectedDate] forState:UIControlStateNormal];
    }
    if (btnSenser == self.btnToDate) {
        [self.btnToDate setTitle:[dateFormatter stringFromDate:self.selectedDate] forState:UIControlStateNormal];
    }

    

}

-(void)didSelectDate:(NSDate *)date formattedString:(NSString *)dateString {
    
    if (selectedButtonDate == self.btnFromDate) {
        NSLog(@"FromDate");
        [self.btnFromDate setTitle:dateString forState:UIControlStateNormal];
    }
    if (selectedButtonDate == self.btnToDate) {
        NSLog(@"ToDate");
        [self.btnToDate setTitle:dateString forState:UIControlStateNormal];
    }
    
    
}

- (IBAction)btnSearch_clicked:(id)sender {
    [arrayDataSearch removeAllObjects];
    
    NSString *strFromDate = [NSString stringWithFormat:@"%@ %@",self.btnFromDate.titleLabel.text,@"00:00:00"];
    NSString *strToDate = [NSString stringWithFormat:@"%@ %@",self.btnToDate.titleLabel.text,@"23:59:59"];
    
    [self getData:strFromDate ToDate:strToDate PageNumber:1];

}
- (IBAction)btnTouch_clicked:(id)sender {
    [self showConditionsSearchView:!isSearchViewShow];
}


- (void)getData:(NSString *)fromDate ToDate:(NSString*)toDate PageNumber:(NSInteger)pageNumber{
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }

    ShareData *shared = [ShareData instance];
    
    NSString *strSupporter = @"";
    if (self.Supporter == @"") {
        self.Supporter = shared.currentUser.userName;
    }
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:fromDate forKey:@"FromDate"];
    [dict setObject:toDate forKey:@"ToDate"];
    [dict setObject:self.potentialObjID forKey:@"PotentialObjID"];
    [dict setObject:self.Supporter forKey:@"Supporter"];
    
    [dict setObject:StringFormat(@"%ld",(long)pageNumber) forKey:@"PageNumber"];
     [self showMBProcess];
    [shared.potentialCustomerProxy GetAllPotentialObjSchedule:dict completehander:^(id result, NSString *errorCode,NSString*message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        NSArray *arr = result;
        [arrayDataSearch addObjectsFromArray:arr];
        if (![errorCode isEqualToString:@"0"]|| result == nil ||arrayDataSearch.count <= 0) {
            [Common_client showAlertMessage:@"Không có kết quả trả về"];
            self.lblFullName.text = @"  KHÔNG CÓ KẾT QUẢ NÀO";
            self.ldlSL.text = @"0";
            [self.listTableView reloadData];
             [self hideMBProcess];
        }else {
            
            record = [arr objectAtIndex:0];
            self.ldlSL.text = StringFormat(@"%lu",(unsigned long)record.totalRow);
            NSString *fullName = record.fullName;
            self.lblFullName.text = [NSString stringWithFormat:@"  KẾT QUẢ KHTN: %@",fullName];
            totalPage = record.totalPage ;
            currentPage = record.currentPage;
            [self.listTableView reloadData];
            [self showConditionsSearchView:NO];
            [self hideMBProcess];
            [self loadMoreCompleted];
            
        }
        
        
    } errorHandler:^(NSError *error){
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
        [self hideMBProcess];
    } ];
    
    
}

- (void)deletedPotentialObjSchedule:(NSString *)potentialObjScheduleID {
    
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:potentialObjScheduleID forKey:@"PotentialObjScheduleID"];
     [self showMBProcess];
    [shared.potentialCustomerProxy DeletePotentialObjSchedule:dict completehander:^(id result,NSString *errorCode,NSString*message){
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if ( ![errorCode isEqualToString:@"0"] ) {
            [Common_client showAlertMessage:@"Không có kết quả trả về"];
            [self hideMBProcess];
        } else {
            //[self showAlertView:message];
            [self showAlertBox:@"Thông Báo" message:message];
            [self.listTableView reloadData];
            alertView.tag = 43;
            [self deletedLocalNotification:self.ScheduleDate ScheduleDescription:self.ScheduleDesription];
            [self hideMBProcess];
        }
        
    } errorHandler:^(NSError *error){
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
          [self hideMBProcess];
        
    }];
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrayDataSearch.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *detailSchedulePotentialCustomerCell = @"DetailSchedulePotentialCustomerCell";
    DetailSchedulePotentialCustomerCell *cell = (DetailSchedulePotentialCustomerCell *)[tableView dequeueReusableCellWithIdentifier:detailSchedulePotentialCustomerCell];
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DetailSchedulePotentialCustomerCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    cell.delegate = self;
    
    if (arrayDataSearch.count > 0) {
        record = [arrayDataSearch objectAtIndex:indexPath.row];
        cell.lblScheduleDate.text = record.scheduleDate;
        cell.txvScheduleDescriptions.text = record.description;
        cell.cellModel = record;
    }
    
    
    return cell;
}

#pragma mark - show or hide conditions search view
- (void)showConditionsSearchView:(BOOL)status {
    
    isSearchViewShow = status;
    self.searchView.hidden = !status;
    //show conditions searchView
    if (status) {
        self.searchViewHeightConstraint.constant = 150;
        self.imgUpDown.image = [UIImage imageNamed:@"up-icon-512"];
    } else {
        
        //hide conditions search View
        self.searchViewHeightConstraint.constant = 0;
        self.imgUpDown.image = [UIImage imageNamed:@"down-icon-512"];
    }
    [UIView animateWithDuration:0.3
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
    
}

- (void) willBeginLoadingMore {
    [footerView.activityIndicator startAnimating];
}

- (void)loadMoreCompleted {
    [footerView.activityIndicator stopAnimating];
}
- (void)loadMore {
    
    self.listTableView.tableFooterView = footerView;
    currentPage++;
    if (currentPage <= totalPage) {
        [self willBeginLoadingMore];
        [self getData:self.btnFromDate.titleLabel.text ToDate:self.btnToDate.titleLabel.text PageNumber:currentPage];
        
    }
    footerView.infoLabel.hidden = NO;
}
#pragma mark - Scroll view delegate
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [self.view endEditing:YES];
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    if (maximumOffset - currentOffset < 0) {
        [self loadMore];
    }

    
}


- (void)pressendButtonSelected:(EditAndDeletedSchedulePotential)editAndDeleted ScheduleDescription:(NSString *)scheduleDescription ScheduleDate:(NSString *)scheduleDate PotentialObjScheduleID:(NSString *)potentialObjScheduleID {
    
    
    switch (editAndDeleted) {
        case EditSchedulePotential:{
           
            AddNewSchedulePotentialViewController *ad = [[AddNewSchedulePotentialViewController alloc]initWithNibName:@"AddNewSchedulePotentialViewController" withID:self.potentialObjID withScheduleDescription:scheduleDescription withScheduleDate:scheduleDate withPotentialObjSchedule:potentialObjScheduleID andSupporter:self.Supporter];
            
            [self.navigationController pushViewController:ad animated:YES];
            break;
            
        }
            
        case DeletedSchedulePotential:{
           
            self.ScheduleDate = scheduleDate;
            self.ScheduleDesription = scheduleDescription;
            self.PotentialObjScheduleID = potentialObjScheduleID;
            [self showAlertView:@"Bạn có muốn Xoá Lịch hẹn KHTN này không"];
            alertView.tag = 44;
            break;
            
        }
            
        default:
            break;
    }
}


//CUSTOMUIALERTVIEW
- (void)showAlertView:(NSString *)message {
    
    alertView = [[UIAlertView alloc] initWithTitle:@"Thông Báo" message:message delegate:self cancelButtonTitle:@"Có" otherButtonTitles:@"Không", nil];
    [alertView show];
}
- (void)alertView:(UIAlertView *)alertView1 clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            //[self getData:@"" ToDate:@"" PageNumber:1];
            
            if (alertView.tag == 44) {
                [self deletedPotentialObjSchedule:self.PotentialObjScheduleID];
                break;
            } else {
                [arrayDataSearch removeAllObjects];
                [self getData:@"" ToDate:@"" PageNumber:1];
                break;
            }
            
        case 1:
            
           
            break;
            
        default:
            break;
    }
    
}
//DELETEDLOCALNOTIFICATION

- (void)deletedLocalNotification:(NSString*)scheduleDate ScheduleDescription:(NSString*)scheduleDescription {
    
    ShareData *shared = [ShareData instance];
    UIApplication *app = [UIApplication sharedApplication];
    NSArray *eventArray = shared.arrayLocalNotification;
    
    for (int i = 0; i < [eventArray count]; i ++) {
        UILocalNotification *oneEvent = [eventArray objectAtIndex:i];
        NSString *body = [NSString stringWithFormat:@"%@",[eventArray[i] valueForKey:@"alertBody"]];
        
        if ([body isEqualToString:self.ScheduleDesription]) {
            [app cancelLocalNotification:oneEvent];
            break;
        }
    }
}

#pragma refresh table view
- (void)refresh:(UIRefreshControl *)refreshControl {
    
    [arrayDataSearch removeAllObjects];
    
    NSString *strFromDate = [NSString stringWithFormat:@"%@ %@",self.btnFromDate.titleLabel.text,@"00:00:00"];
    NSString *strToDate = [NSString stringWithFormat:@"%@ %@",self.btnToDate.titleLabel.text,@"23:59:59"];
    [self getData:@"" ToDate:@"" PageNumber:1];
    [refreshControl endRefreshing];
}

@end
