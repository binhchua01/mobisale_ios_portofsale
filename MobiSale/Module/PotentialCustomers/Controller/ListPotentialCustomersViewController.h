//
//  ListPotentialCustomersViewController.h
//  MobiSale
//
//  Created by ISC-DanTT on 11/17/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

typedef enum {
    none = 1,
    detail,
    Sale,
    TongDai
    
} ListPotentialType;

@interface ListPotentialCustomersViewController :BaseViewController<UIScrollViewDelegate, UISearchBarDelegate>



@property (strong, nonatomic) IBOutlet UIButton *btnOptions;

@property (strong, nonatomic) IBOutlet UISearchBar *txtSearchContent;

@property (strong, nonatomic) NSString *ToDate;

@property (strong, nonatomic) NSString *FromDate;

@property (strong, nonatomic) NSString *Status;

@property (strong, nonatomic) NSString *Agent;

@property (strong, nonatomic) NSString *AgentName;

@property (strong, nonatomic) NSString *UserName;

@property (assign) ListPotentialType listPotentialType;
@property (weak, nonatomic) IBOutlet UIButton *viewAll;

@end
