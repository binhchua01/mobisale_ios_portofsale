//
//  SurveyViewController.h
//  MobiSale
//
//  Created by ISC-DanTT on 1/20/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface SurveyViewController : BaseViewController

@property (nonatomic, strong) NSArray *arrData;
@property (nonatomic, strong) NSString *PotentialID;

@end
