//
//  LocalNotificationModel.m
//  MobiSale
//
//  Created by Tran Vu on 10/19/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import "LocalNotificationModel.h"

@implementation LocalNotificationModel
//@synthesize arrayDataLocalNotification;


+ (id)sharedInstance {
    static LocalNotificationModel *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[LocalNotificationModel alloc] init];
        // Do any other initialisation stuff here
        sharedInstance.arrayDataLocalNotification = [NSArray array];
        
    });
    
    return sharedInstance;
}
@end
