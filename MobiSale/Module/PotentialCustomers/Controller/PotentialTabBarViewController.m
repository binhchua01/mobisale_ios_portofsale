//
//  PotentialTabBarViewController.m
//  MobiSale
//
//  Created by ISC on 9/20/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "PotentialTabBarViewController.h"
#import "PotentialCustomerDetailForm.h"

@interface PotentialTabBarViewController ()

@end

@implementation PotentialTabBarViewController

- (id)init {
    if (self = [super init]) {
        [self setView];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Set view Potential Customers
- (void)setView{
    self.listPotential = [[ListPotentialCustomersViewController alloc] init];
    self.listPotential.listPotentialType = Sale;
    self.listPotential18006000 = [[ListPotentialCustomersViewController alloc] init];
    self.listPotential18006000.listPotentialType = TongDai;
    self.listPotentialMap = [[MapsListPotentialCustomersViewController alloc] init];
    [self.listPotential.tabBarItem setTitle:@"DANH SÁCH"];
    [self.listPotential.tabBarItem setImage:[UIImage imageNamed:@"list_32"]];
    [self.listPotential18006000.tabBarItem setTitle:@"18006000"];
    [self.listPotential18006000.tabBarItem setImage:[UIImage imageNamed:@"list_32"]];
    [self.listPotentialMap.tabBarItem setTitle:@"BẢN ĐỒ"];
    [self.listPotentialMap.tabBarItem setImage:[UIImage imageNamed:@"map_32"]];
    
    [self setViewControllers:[NSArray arrayWithObjects:self.listPotential, self.listPotential18006000, self.listPotentialMap, nil]];
    // add right bar button
    self.navigationItem.rightBarButtonItem = [self rightBarButtonItem];
    // set title view
    [self setTitle:@"DS KH TIỀM NĂNG"];
    
}

#pragma mark - Setup right bar item
// Add right bar item in view
- (UIBarButtonItem *)rightBarButtonItem {
    UIImage *image = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"ic_create_potentail_obj"] height:30];
    return [[UIBarButtonItem alloc] initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]
                                            style:UIBarButtonItemStyleBordered
                                           target:self
                                           action:@selector(addNewPotential:)];
}

// Bar item actions
- (void)addNewPotential:(id)sender {
    switch (self.selectedIndex) {
        case 0:
        case 2:
            [self showInfoPotentialView];
            break;
        case 1:
            [self showAlertTextField];
            break;
        default:
            break;
    }
}

- (void)showInfoPotentialView {
    ShareData *shared = [ShareData instance];
    shared.potentialRecord = [[PotentialCustomerDetailRecord alloc] init];
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"PotentialCustomerDetail" bundle:nil];
    UIViewController * initialVC = [storyboard instantiateInitialViewController];
    initialVC.title = @"THÊM KH TIỀM NĂNG";
    UINavigationController *nav = [((UINavigationController *)self.menuContainerViewController.centerViewController) initWithRootViewController:initialVC ];

    //Sửa lỗi: "Unbalanced calls to begin/end appearance transitions for <.....>"
    [self.menuContainerViewController.navigationController pushViewController:nav animated:YES];
}

#pragma mark - Setup UIAlertView
// Show alert view with text field
- (void)showAlertTextField {
    UIAlertView* dialog = [[UIAlertView alloc] initWithTitle:@"THÊM KH TIỀM NĂNG"
                                                     message:@"Nhập mã code để lấy thông tin KH"
                                                    delegate:self
                                           cancelButtonTitle:@"Đóng"
                                           otherButtonTitles:@"OK", nil];
    
    dialog.alertViewStyle = UIAlertViewStylePlainTextInput;
    dialog.tag = 3;
    [dialog show];
}

// UIAlertViewDelegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == 3) {
        UITextField *textField = [alertView textFieldAtIndex:0];
        [textField endEditing:YES];
        if (buttonIndex == 1) {
            [self checkEnterCode:textField.text];
        }
    }
}

#pragma mark - Check enter code
- (void)checkEnterCode:(NSString *)code {
    if (code.length > 0) {
        [self getPotentialByCode:code];
    }
}

#pragma mark -Gọi API Get Potential By Code

- (void)getPotentialByCode:(NSString *)code {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    ShareData *shared = [ShareData instance];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    [dict setObject:code ?:@"0" forKey:@"Code"];
    
    [self showMBProcess];
    
    [shared.potentialCustomerProxy getPotentialByCode:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self showAlertBoxWithDelayDismiss:@"Thông báo" message:@"" tag:1];
            [self hideMBProcess];
            [self logOut];
            return;
        }
        
        [self hideMBProcess];

        // Nếu code không có tồn tại
        if (result == nil || [errorCode integerValue] < 0) {
            [self showAlertBox:@"Thông báo" message:message tag:2];
            return;
        // Nếu code tồn tại thì load thông tin khách hàng.
        } else {
            NSArray *dataArray = result;
            NSDictionary *dict = dataArray[0];
            NSInteger ID = [dict[@"ID"] integerValue];
            [self pushToPotentialDetailViewWithID:ID];
        }
        
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}

- (void)pushToPotentialDetailViewWithID:(NSInteger)ID {
    PotentialCustomerDetailForm *vc = [[PotentialCustomerDetailForm alloc] init];
    vc.ID = StringFormat(@"%li", (long)ID);
    vc.potentialType = FromCode;
    
    [self.navigationController pushViewController:vc animated:YES];
    
}

- (void)logOut {
    BaseViewController *baseVC = [[BaseViewController alloc] init];
    [baseVC LogOut];
}
@end
