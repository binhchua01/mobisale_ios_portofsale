//
//  ListPotentialCustomersViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 11/17/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ListPotentialCustomersViewController.h"
#import "ListPotentialCustomersViewCell.h"
#import "PotentialCustomerFormRecord.h"
#import "PotentialCustomerDetailForm.h"
#import "Common_client.h"
#import "ShareData.h"
#import "NIDropDown.h"
#import "DemoTableFooterView.h"
#import "MapsListPotentialCustomersViewController.h"
#import "AppDelegate.h"
#import "ListSchedulePotentialViewController.h"

@interface ListPotentialCustomersViewController ()<UITableViewDelegate, UITableViewDataSource, NIDropDownDelegate,ListPotentialCustomersViewCellDelegate>{
    NSMutableArray *arrList;
    NSMutableArray *arrData;
    NIDropDown *dropDown;
    NSArray *arrOptions;
    DemoTableFooterView *footerView;
    NSInteger totalPage;
    NSInteger currentPage;
    
    IBOutlet NSLayoutConstraint *topSpaceConstraint;
    IBOutlet NSLayoutConstraint *bottomSpaceConstranint;
    CGFloat startOffset;
    
    NSInteger sourceValue;
}
@property (strong, nonatomic) IBOutlet UITableView *listTableView;

@end

@implementation ListPotentialCustomersViewController

@synthesize UserName, ToDate, FromDate, Status, Agent, AgentName, listPotentialType;

- (void)viewDidLoad {
    [super viewDidLoad];
    //vutt11
    self.viewAll.layer.cornerRadius = 5;
    self.listTableView.rowHeight = UITableViewAutomaticDimension;
    self.listTableView.estimatedRowHeight = 10;
    
    self.screenName = @"DANH SÁCH KHÁCH HÀNG TIỀM NĂNG";
    if (self.listPotentialType == detail) {
        self.title = @"DANH SÁCH KHTN (CHI TIẾT)";
        
    }
    
    if (self.listPotentialType == TongDai) {
        sourceValue = 1;
        
    } else {
        sourceValue = 0;
    }
    
    arrData = [[NSMutableArray alloc]init];
    arrOptions = @[@"Tất cả",@"Tên KH",@"Số ĐT",@"Địa chỉ",@"TG kết thúc dịch vụ ISP",@"Tích hẹn KHTN"];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    // set the custom view for "load more". See DemoTableFooterView.xib.
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DemoTableFooterView" owner:self options:nil];
    footerView = (DemoTableFooterView *)[nib objectAtIndex:0];
    startOffset = -20;
    // add refresh control when scroll top of tableview
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.listTableView addSubview:refreshControl];
    
    
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.txtSearchContent setHidden:NO];
    [self.tableView reloadData];
    
    if (self.listPotentialType == detail) {
        bottomSpaceConstranint.constant = 5;
        [self hideSearching];
    }
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //load data
    [arrData removeAllObjects];
    footerView.infoLabel.hidden = YES;
    if (self.listPotentialType == detail) {
        [self LoadDataWithAgent:Agent agentName:AgentName source:sourceValue pageNumber:@"1"];
        return;
    }
    [self LoadDataWithAgent:@"0" agentName:@"" source:sourceValue pageNumber:@"1"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView methods Delegate and DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (arrData.count >0) {
        return arrData.count;
    }
    return 0;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    static NSString *simpleTableIdentifier = @"ListPotentialCustomersCell";
//    ListPotentialCustomersViewCell *cell = (ListPotentialCustomersViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
//    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ListPotentialCustomersViewCell" owner:self options:nil];
//    cell = [nib objectAtIndex:0];
//    CGFloat height = 0;
//    if (arrData.count >0) {
//        PotentialCustomerFormRecord *rc = [arrData objectAtIndex:indexPath.section];
//        if (rc.FullName.length >25) {
//            height = 13*(rc.FullName.length/25 +1) + height;
//        }
//        if (rc.Email.length >25) {
//            height = 13*(rc.Email.length/25 +1) + height;
//        }
//        if (rc.Address.length >25) {
//            height = 19*(rc.Address.length/25 +1) + height;
//        }
//        if (rc.Note.length >25) {
//            height = 15*(rc.Note.length/25 +1) + height;
//        }
//    }
//    if (self.listPotentialType == Sale) {
//        return 185 + height;
//    }
//    return 230 +height;
//}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
    return headerView;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *simpleTableIdentifier = @"ListPotentialCustomersCell";
    ListPotentialCustomersViewCell *cell = (ListPotentialCustomersViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ListPotentialCustomersViewCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    
    cell.delegate = self;
    
    if (arrData.count >0) {
        PotentialCustomerFormRecord *rc = [arrData objectAtIndex:indexPath.section];
        cell.cellIndex = indexPath.section + 1;
        cell.source    = self.listPotentialType;
        cell.cellModel = rc;
        
    }
    return cell;
}

#pragma mark - TableView didSelected Cell

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.listPotentialType == detail ) {
        return;
    }
    [self.view endEditing:YES];
    NSString *nibName = @"PotentialCustomerDetailForm";
    PotentialCustomerFormRecord *rc = [arrData objectAtIndex:indexPath.section];
    PotentialCustomerDetailForm *vc = [[PotentialCustomerDetailForm alloc]initWithNibName:nibName bundle:nil];
    if (rc.ID !=nil) {
        vc.ID = rc.ID;
        vc.potentialType = Normal;
        vc.Supporter = rc.Supporter;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - Scroll view delegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [self.view endEditing:YES];
    if (dropDown !=nil) {
        [dropDown hideDropDown:self.btnOptions];
        [self rel];
    }

    CGFloat currentOffset = scrollView.contentOffset.y;
    if (currentOffset > startOffset) {
        [self hideSearching];
    }else{
        [self showSearching];
    }
    startOffset = currentOffset;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    if (maximumOffset - currentOffset < 0) {
        [self loadMore];
    }
}

- (IBAction)btnOptions_Clicked:(id)sender {
    [self showDropDownAtButton:sender withArrayData:arrOptions];
}

- (IBAction)btnSearch_Clicked:(id)sender {
    [self searching];
}

#pragma mark - LoadData
-(void)LoadDataWithAgent:(NSString*)agent agentName:(NSString*)agentName source:(NSInteger)source pageNumber:(NSString*)pageNumber {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    ShareData *shared = [ShareData instance];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:agent      ?:@"0"  forKey:@"Agent"];
    [dict setObject:agentName  ?:@""   forKey:@"AgentName"];
    [dict setObject:pageNumber ?:@"1"  forKey:@"PageNumber"];
    [dict setObject:StringFormat(@"%li", (long)source) forKey:@"Source"];
    
    if (self.listPotentialType == detail) {
        [dict setObject:UserName ?:@"" forKey:@"UserName"];
        [dict setObject:FromDate ?:@"" forKey:@"FromDate"];
        [dict setObject:ToDate   ?:@"" forKey:@"ToDate"];
        [dict setObject:Status   ?:@"" forKey:@"Status"];

    } else {
        [dict setObject:shared.currentUser.userName ?:@"" forKey:@"UserName"];

    }
    
    [self showMBProcess];

    [shared.potentialCustomerProxy GetListPotentialCustomersWithInput:dict completehander:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(result == nil){
            [self ShowAlertBoxEmpty];
            [self hideMBProcess];
            [self.listTableView setHidden:YES];
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            [self.listTableView setHidden:YES];

            return ;
        }
        [self.listTableView setHidden:NO];
        arrList = result;
        [arrData addObjectsFromArray:arrList];
        
        /*
         * save data for Map list potential customers
         */
        if (self.listPotentialType != detail) {
            shared.arrPotentialCutomers = arrData;
        }
        
        PotentialCustomerFormRecord *rc = [arrList objectAtIndex:0];;
        totalPage = [rc.TotalPage integerValue];
        currentPage = [rc.CurrentPage integerValue];
        [self.listTableView reloadData];
        [self hideMBProcess];
        [self loadMoreCompleted];
        
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
        return;
    }];
}

#pragma refresh table view
- (void)refresh:(UIRefreshControl *)refreshControl {
    [arrData removeAllObjects];
    if (self.listPotentialType == detail) {
        [self LoadDataWithAgent:Agent agentName:AgentName source:sourceValue pageNumber:@"1"];
        [refreshControl endRefreshing];
        return;
    }
    [self LoadDataWithAgent:@"0" agentName:@"" source:sourceValue pageNumber:@"1"];
    [refreshControl endRefreshing];
}

#pragma mark - Load More
//
// The method -loadMore was called and will begin fetching data for the next page (more).
// Do custom handling of -footerView if you need to.
//
- (void) willBeginLoadingMore {
    [footerView.activityIndicator startAnimating];
}

// Do UI handling after the "load more" process was completed. In this example, -footerView will
// show a "No more items to load" text.
//
- (void)loadMoreCompleted {
    [footerView.activityIndicator stopAnimating];
}

- (void)loadMore {
    // Do your async loading here
    self.listTableView.tableFooterView = footerView;
    currentPage ++;
    if (currentPage <= totalPage) {
        [self willBeginLoadingMore];
        if (self.listPotentialType == detail) {
            [self LoadDataWithAgent:Agent agentName:AgentName source:sourceValue pageNumber:StringFormat(@"%li",(long)currentPage)];
            return;
        }
        [self LoadDataWithAgent:@"0" agentName:@"" source:sourceValue pageNumber:StringFormat(@"%li",(long)currentPage)];
        return;
    }
    footerView.infoLabel.hidden = NO;
}

#pragma mark - setShowAndHideKeyboard
-(void)keyboardWillShow {
    // Animate the current view out of the way
    if (dropDown !=nil) {
        [dropDown hideDropDown:self.btnOptions];
        [self rel];
    }
}

#pragma mark - show dropdown list
- (void)handleSingleTap:(UITapGestureRecognizer *) sender{
    [self.view endEditing:YES];
    if (dropDown !=nil) {
        [dropDown hideDropDown:self.btnOptions];
        [self rel];
    }
}

- (void)showDropDownAtButton:(UIButton *)sender withArrayData:(NSArray*)arrayData{
    [self.view endEditing:YES];
    if (dropDown == nil) {
        CGFloat height = 200;
        CGFloat width = 109;
        dropDown = [[NIDropDown alloc] showDropDownAtButton:sender height:&height width:&width data:arrayData images:nil animation:@"down"];
        dropDown.delegate = self;
        return;
    }
    [dropDown hideDropDown:sender];
    [self rel];
}

#pragma mark - NIDropDown Delegate method
- (void)dropDownListView:(NIDropDown *)dropDownListView didSelectIndexPath:(NSIndexPath *)indexPath atButton:(UIButton *)button{
    [self didDropDownSelected:indexPath.row atButton:button];
    
    
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
}

-(void)rel{
    dropDown = nil;
}

- (void) didDropDownSelected:(NSInteger)row atButton:(UIButton *)button{
    if (button == self.btnOptions) {
        self.txtSearchContent.text = @"";
        if (row==0) {
            self.txtSearchContent.placeholder = @"Nhập thông tin cần tìm";
            self.txtSearchContent.keyboardType = UIKeyboardTypeDefault;
            [self.txtSearchContent becomeFirstResponder];
            return;
        }
        if (row == 1) {
            self.txtSearchContent.placeholder = @"Nhập tên KH cần tìm";
            self.txtSearchContent.keyboardType = UIKeyboardTypeDefault;
            [self.txtSearchContent becomeFirstResponder];
            return;
        }
        if (row == 2) {
            self.txtSearchContent.placeholder = @"Nhập số ĐT cần tìm";
            self.txtSearchContent.keyboardType = UIKeyboardTypeNumberPad;
            [self.txtSearchContent becomeFirstResponder];
            return;
        }
        if (row == 3) {
            self.txtSearchContent.placeholder = @"Nhập địa chỉ cần tìm";
            self.txtSearchContent.keyboardType = UIKeyboardTypeDefault;
            [self.txtSearchContent becomeFirstResponder];
            return;
        }
        
        if (row == 5) {
            [self.txtSearchContent setUserInteractionEnabled:NO];
            return;
            //canBecomeFirstResponder
        }
        self.txtSearchContent.placeholder = @"VD: 28/12/2015";
        self.txtSearchContent.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        [self.txtSearchContent becomeFirstResponder];
        return;
    }
}

- (void)showSearching {
    if (self.listPotentialType == detail) {
        return;
    }
    topSpaceConstraint.constant = 69;
    [UIView animateWithDuration:0.25
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

- (void)hideSearching {
    topSpaceConstraint.constant = 29;
    [UIView animateWithDuration:0.25
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

#pragma mark - UISearchBar delegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self searching];
}

- (void)searching {
    [self.view endEditing:YES];
    [arrData removeAllObjects];
    [self.txtSearchContent setHidden:NO];
    if ([self.btnOptions.titleLabel.text isEqualToString:@"Tất cả"]) {
        [self LoadDataWithAgent:@"0" agentName:self.txtSearchContent.text source:sourceValue pageNumber:@"1"];
        return;
    }
    if (self.txtSearchContent.text.length <=0) {
        if (![self.btnOptions.titleLabel.text isEqualToString:@"Tích hẹn KHTN"]){
            [self showAlertBox:@"Thông báo" message:@"Vui lòng nhập thông tin cần tìm!"];
            return;
        }
        
    }
    if ([self.btnOptions.titleLabel.text isEqualToString:@"Tên KH"]) {
       // NSString *nameCustomer = [self.txtSearchContent.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        [self LoadDataWithAgent:@"2" agentName:self.txtSearchContent.text source:sourceValue pageNumber:@"1"];
        return;
    }
    if ([self.btnOptions.titleLabel.text isEqualToString:@"Số ĐT"]) {
        NSString *sdt = [self.txtSearchContent.text stringByReplacingOccurrencesOfString:@" "
                          withString:@""];
        [self LoadDataWithAgent:@"3" agentName:sdt source:sourceValue pageNumber:@"1"];
        return;
    }
    if ([self.btnOptions.titleLabel.text isEqualToString:@"Địa chỉ"]) {
        //NSString *addpress = [self.txtSearchContent.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        [self LoadDataWithAgent:@"4" agentName:self.txtSearchContent.text source:sourceValue pageNumber:@"1"];
        return;
    }
    if ([self.btnOptions.titleLabel.text isEqualToString:@"Tích hẹn KHTN"]) {
        NSString *scheduleKHTN = [self.txtSearchContent.text stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        [self LoadDataWithAgent:@"7" agentName:scheduleKHTN source:sourceValue pageNumber:@"1"];
        return;
    }
    NSString *textSearch= [self convertDateToString:[self convertStringToDate:self.txtSearchContent.text]];
    NSLog(@"self.txtSearchContent.text = %@",textSearch);
    if (textSearch !=nil) {
        [self LoadDataWithAgent:@"5" agentName:textSearch source:sourceValue pageNumber:@"1"];
        return;
    }
    [self showAlertBox:@"Lỗi" message:@"Bạn đã nhập sai định dạng thời gian cần tìm"];

}

- (NSDate *)convertStringToDate:(NSString *)dateStringInput{
    @try {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"GMT"];
        [dateFormatter setTimeZone:timeZone];
        NSDate *dateFromString = [[NSDate alloc] init];
        dateFromString = [dateFormatter dateFromString:dateStringInput];
        return dateFromString;
    }
    @catch (NSException *exception) {
        [self showAlertBox:@"Lỗi" message:@"Bạn đã nhập sai định dạng thời gian cần tìm"];
        [CrashlyticsKit recordCustomExceptionName:@"ListPotentialCustomersViewController - funtion (saveReceiveRemoteNotification)-Error handle Bạn đã nhập sai định dạng thời gian cần tìm" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
    }
}

- (NSString *)convertDateToString:(NSDate*)dateInput{
    @try {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"GMT+7"];
        [dateFormatter setTimeZone:timeZone];
        NSString *stringDate = [dateFormatter stringFromDate:dateInput];
        return stringDate;
    }
    @catch (NSException *exception) {
        [self showAlertBox:@"Lỗi" message:@"Bạn đã nhập sai định dạng thời gian cần tìm"];
        [CrashlyticsKit recordCustomExceptionName:@"ListPotentialCustomersViewController- funtion (saveReceiveRemoteNotification)-Error handle Bạn đã nhập sai định dạng thời gian cần tìm" reason:[[ShareData instance].currentUser.userName lowercaseString]frameArray:@[]];
    }
}

-(void)didSelectedAlarm:(NSString *)ID andSupporter:(NSString *)supporter {
    
    ListSchedulePotentialViewController *ls = [[ListSchedulePotentialViewController alloc] initWithNibName:@"ListSchedulePotentialViewController" withID:ID andSupporter:supporter];
    [self.navigationController pushViewController:ls animated:true];
    
    
}

@end
