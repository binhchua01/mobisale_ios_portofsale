//
//  ListSchedulePotentialModel.m
//  MobiSale
//
//  Created by Tran Vu on 10/13/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import "ListSchedulePotentialModel.h"

@implementation ListSchedulePotentialModel
@synthesize description,fullName,potentialObjID,potentialObjScheduleID,scheduleDate,supporter;
@synthesize rowNumber,currentPage,totalRow,totalPage;
- (id)initWithDictionary:(NSDictionary *)dict {
    if (self == [super init]) {
        self.description = dict[@"ScheduleDescription"]?:@"";
        self.fullName = dict[@"FullName"]?:@"";
        self.potentialObjID = [dict[@"PotentialObjID" ]integerValue];
        self.potentialObjScheduleID = [dict [@"PotentialObjScheduleID"]integerValue];
        self.scheduleDate = dict[@"ScheduleDate"];
        self.supporter = dict[@"Supporter"];
        self.rowNumber = [dict[@"RowNumber"] integerValue];
        self.currentPage = [dict[@"CurrentPage"] integerValue];
        self.totalRow = [dict[@"TotalRow"] integerValue];
        self.totalPage = [dict[@"TotalPage"] integerValue];
    }
    
    
    return self;
}
+ (id)sharedInstance {
    static ListSchedulePotentialModel *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ListSchedulePotentialModel alloc] init];
        // Do any other initialisation stuff here
    });
    
    return sharedInstance;
}

@end
