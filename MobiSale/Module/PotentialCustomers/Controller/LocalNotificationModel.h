//
//  LocalNotificationModel.h
//  MobiSale
//
//  Created by Tran Vu on 10/19/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalNotificationModel : NSObject

@property (retain, nonatomic) NSArray *arrayDataLocalNotification;

+ (id)sharedInstance;

@end
