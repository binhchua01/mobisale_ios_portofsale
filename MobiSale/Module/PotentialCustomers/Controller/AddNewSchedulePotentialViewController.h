//
//  AddNewSchedulePotentialViewController.h
//  MobiSale
//
//  Created by Tran Vu on 10/16/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PickerViewController.h"
#import "BaseViewController.h"



@interface AddNewSchedulePotentialViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UITextView *txvScheduleDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnScheduleDate;
@property (weak, nonatomic) IBOutlet UIView *viewInfoAndTime;
@property (weak, nonatomic) IBOutlet UIButton *btnUpdate;
-(instancetype) initWithNibName:(NSString *)nibNameOrNil withID:(NSString*)potentialObjID withScheduleDescription:(NSString *)scheduleDescription withScheduleDate:(NSString *)scheduleDate withPotentialObjSchedule:(NSString *)potentialObjSchedule andSupporter:(NSString *)supporter;
@property (strong, nonatomic ) NSString*potentialObjID;
@property (strong, nonatomic) NSString *potentialObjSchedule;
@property (strong, nonatomic) NSString *scheduleDescription;
@property (strong, nonatomic) NSString *scheduleDate;
@property (strong, nonatomic) NSString *supporter;

@end
