//
//  NoteInfoPotentialCustomerViewController.h
//  MobiSale
//
//  Created by ISC-DanTT on 11/18/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PotentialCustomerDetailRecord.h"
#import "BaseViewController.h"

@interface NoteInfoPotentialCustomerViewController :BaseViewController
@property (strong, nonatomic) IBOutlet UITextField *txtFullName;
@property (strong, nonatomic) IBOutlet UITextField *txtPhoneNumber;
@property (strong, nonatomic) IBOutlet UITextView *txtAddress;
@property (strong, nonatomic) IBOutlet UITextView *txtNote;

@property (retain, nonatomic) PotentialCustomerDetailRecord *record;

@end
