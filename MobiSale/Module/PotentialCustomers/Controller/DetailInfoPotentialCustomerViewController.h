//
//  DetailPotentialCustomerViewController.h
//  MobiSale
//
//  Created by ISC-DanTT on 11/18/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PotentialCustomerDetailRecord.h"
#import "KeyValueModel.h"
#import "BaseViewController.h"
@interface DetailInfoPotentialCustomerViewController : BaseViewController<UITextFieldDelegate, UIActionSheetDelegate>{
    KeyValueModel
    *selectedPhone1 ,
    *selectedPhone2 ,
    *selectedObjectCustomer ,
    *selectedTypeCustomer ,
    *selectedPlaceCurrent ,
    *selectedDistrict ,
    *selectedWard ,
    *selectedTypeHouse ,
    *selectedStreet ,
    *selectedNameVilla,
    *selectedPosition,
    *selectedService,
    *selectedISP;
    
    NSString *ID;
    UIAlertView * updateCheck, *updateSuccess;
}

@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *contentView;

// thông tin khách hàng
@property (strong, nonatomic) IBOutlet UITextField *txtFullName;
@property (strong, nonatomic) IBOutlet UITextField *txtPassport;
@property (strong, nonatomic) IBOutlet UITextField *txtTaxID;
@property (strong, nonatomic) IBOutlet UIButton *btnPhone1Type;
@property (strong, nonatomic) IBOutlet UITextField *txtPhone1;
@property (strong, nonatomic) IBOutlet UITextField *txtContact1;
@property (strong, nonatomic) IBOutlet UIButton *btnPhone2Type;
@property (strong, nonatomic) IBOutlet UITextField *txtPhone2;
@property (strong, nonatomic) IBOutlet UITextField *txtContact2;
@property (strong, nonatomic) IBOutlet UITextField *txtFax;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtFacebook;
@property (strong, nonatomic) IBOutlet UITextField *txtTiwtter;

// đối tượng khách hàng
@property (strong, nonatomic) IBOutlet UIButton *btnCustomerType;
@property (strong, nonatomic) IBOutlet UIButton *btnServiceType;
@property (strong, nonatomic) IBOutlet UIButton *btnISPType;
@property (strong, nonatomic) IBOutlet UIButton *btnISPStartDate;
@property (strong, nonatomic) IBOutlet UIButton *btnISPEndDate;

// địa chỉ
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) IBOutlet UIButton *btnDistrict;
@property (strong, nonatomic) IBOutlet UIButton *btnWard;
@property (strong, nonatomic) IBOutlet UIButton *btnTypeHouse;
@property (strong, nonatomic) IBOutlet UIButton *btnStreet;
@property (strong, nonatomic) IBOutlet UITextView *txtAddress;
@property (strong, nonatomic) IBOutlet UIButton *btnPositionHouse;
@property (strong, nonatomic) IBOutlet UITextField *txtNumberHouse;
@property (strong, nonatomic) IBOutlet UITextField *txtLot;
@property (strong, nonatomic) IBOutlet UITextField *txtFloor;
@property (strong, nonatomic) IBOutlet UITextField *txtRoom;
@property (strong, nonatomic) IBOutlet UIButton *btnNameVilla;
@property (strong, nonatomic) IBOutlet UITextView *txtNote;
@property (strong, nonatomic) NSString *ID;

@property (strong, nonatomic) IBOutlet UILabel *lblPositionHouse;
@property (strong, nonatomic) IBOutlet UILabel *lblNumberHouse;
@property (strong, nonatomic) IBOutlet UILabel *lblLot;
@property (strong, nonatomic) IBOutlet UILabel *lblFloor;
@property (strong, nonatomic) IBOutlet UILabel *lblRoom;
@property (strong, nonatomic) IBOutlet UILabel *lblNameVilla;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constranintPositionHouse;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constranintNote;

@property (retain, nonatomic) PotentialCustomerDetailRecord *record;
@property (retain, nonatomic) NSMutableArray *arrPhone1;
@property (retain, nonatomic) NSMutableArray *arrPhone2;
@property (retain, nonatomic) NSMutableArray *arrObjectCustomer;
@property (retain, nonatomic) NSMutableArray *arrTypeCustomer;
@property (retain, nonatomic) NSMutableArray *arrPlaceCurrent;
@property (retain, nonatomic) NSMutableArray *arrSolvency;
@property (retain, nonatomic) NSMutableArray *arrClientPartners;
@property (retain, nonatomic) NSMutableArray *arrLegal;
@property (retain, nonatomic) NSMutableArray *arrDistrict;
@property (retain, nonatomic) NSMutableArray *arrWard;
@property (retain, nonatomic) NSMutableArray *arrTypeHouse;
@property (retain, nonatomic) NSMutableArray *arrStreet;
@property (retain, nonatomic) NSMutableArray *arrNameVilla;
@property (retain, nonatomic) NSMutableArray *arrPosition;
@property (retain, nonatomic) NSMutableArray *arrCusType;
@property (retain, nonatomic) NSMutableArray *arrISPType;
@property (retain, nonatomic) NSMutableArray *arrServiceType;

@end
