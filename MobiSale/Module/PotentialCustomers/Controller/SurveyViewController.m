//
//  SurveyViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 1/20/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "SurveyViewController.h"
#import "SurveyModel.h"
#import "SurveyViewCell.h"
#import "AnswerModel.h"
#import "ShareData.h"
#import "Common_client.h"
#import "AppDelegate.h"

@interface SurveyViewController () <UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, UIActionSheetDelegate, UIAlertViewDelegate, SurveyViewCellDelegate> {
    
    NSMutableArray *cellDatas;
    NSMutableArray *arrSurveyAnswer;
    UITableView    *surveyTable;
    UIActionSheet  *noticeSave;
    NSString       *_surveyID;
    UIAlertView    *alertSave;
    CGFloat        _cellHeight;
    
    UIDatePicker   *datePicker;
    NSString       *chooseDate;
    UIButton       *selectedButton;
    NSInteger      count;

}

@end

@implementation SurveyViewController
@synthesize arrData;

- (void)viewDidLoad {
    [super viewDidLoad];
    arrSurveyAnswer = [NSMutableArray array];
    
    CGRect datePickerFrame = CGRectMake(0,0, self.view.frame.size.width, 200);
    datePicker  = [[UIDatePicker alloc] initWithFrame:datePickerFrame];
    datePicker.datePickerMode = UIDatePickerModeDate;
    chooseDate = @"";
    
    [self loadData];
    [self addSurveyTable];
    self.navigationItem.rightBarButtonItem = [self rightBarButtonItem];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadData {
    cellDatas = [NSMutableArray array];
    for (NSDictionary *dict in arrData) {
        SurveyModel *cellModel = [SurveyModel surveyModelWithDict:dict];
        [cellDatas addObject:cellModel];
    }
    [self setNumberQuestionDoneAnswered];
}

- (void)addSurveyTable {
    self.view.backgroundColor = [UIColor colorWithRed:235.0/255 green:235.0/255 blue:235.0/255 alpha:1.0];
    UITableView *table = [[UITableView alloc] init];
    table.frame = CGRectMake(0, 5, self.view.frame.size.width, self.view.frame.size.height);
    table.backgroundColor = [UIColor colorWithRed:235.0/255 green:235.0/255 blue:235.0/255 alpha:1.0];
    table.delegate = self;
    table.dataSource = self;
    table.allowsSelection = NO;
    [table addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(endEdit)]];
    surveyTable = table;
    [self.view addSubview:table];
}

- (UIBarButtonItem *)rightBarButtonItem {
    UIImage *image = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"save_32"] height:30];
    return [[UIBarButtonItem alloc]
            initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered
            target:self
            action:@selector(buttonSave_Click:)];
    
}

- (void)buttonSave_Click:(id)sender {
    NSLog(@"Lưu thông tin khảo sát");
    if (count == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Bạn chưa trả lời câu hỏi nào" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    if (arrSurveyAnswer.count <= 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Bạn không có thay đổi gì nên không cần phải cập nhật" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    noticeSave = [[UIActionSheet alloc] initWithTitle:@"Bạn chắc chắn muốn lưu đáp án khảo sát?" delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
    [noticeSave showInView:[UIApplication sharedApplication].keyWindow];
    
}

- (void)endEdit {
    [self.view endEditing:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return cellDatas.count;
}

- (SurveyViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"cell";
    SurveyViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[SurveyViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    [cell.contentView clearsContextBeforeDrawing];
    cell.delegate  = self;
    cell.cellIndex = indexPath.row;
    cell.cellModel = cellDatas[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (IS_OS_8_OR_LATER) {
        return _cellHeight;
    }
    
    static NSString *cellIdentifier = @"cell";
    SurveyViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[SurveyViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    [cell.contentView clearsContextBeforeDrawing];
    cell.delegate  = self;
    cell.cellIndex = indexPath.row;
    cell.cellModel = cellDatas[indexPath.row];
    return _cellHeight;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

#pragma ark - SurveyViewCellDelegate method
//update cell height of tableView
- (void)updateCellHeight:(CGFloat)cellHeight{
    _cellHeight = cellHeight;
}
// update survey answer
-(void)updateSurveyID:(NSString *)surveyID values:(NSArray *)values {
    NSLog(@"updating surveyID: %@",surveyID);
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:surveyID forKey:@"SurveyID"];
    [dict setObject:values forKey:@"Values"];
    if (arrSurveyAnswer.count >0) {
        for (NSDictionary *oldDict in arrSurveyAnswer) {
            NSString *oldSurveyID = [oldDict objectForKey:@"SurveyID"];
            if ([oldSurveyID isEqualToString:surveyID]) {
                [arrSurveyAnswer removeObject:oldDict];
                break;
            }
        }
    }
    [arrSurveyAnswer addObject:dict];
}

- (void) updateCellModel:(SurveyModel *)cellModel atIndex:(NSInteger)cellIndex {
    [cellDatas replaceObjectAtIndex:cellIndex withObject:cellModel];
    [self setNumberQuestionDoneAnswered];
}

// update date when click button type date picker in survey list
- (void)updatePickerDate:(UIButton *)sender surveyID:(NSString *)surveyID {
    @try {
        _surveyID = surveyID;
        [self limitDate:[NSDate date] minYear:-99 maxYear:0];
        [self showDatePicker:datePicker withButton:sender];
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception.description);
         [CrashlyticsKit recordCustomExceptionName:@"SurveyViewController - funtion (saveReceiveRemoteNotification)-Error handle Gupdate date when click button type date picker in survey list" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
    }
}

// update _cellModel when select date
- (void)updateCellModelWithSurveyValue:(NSString*)SurveyValue {
    NSMutableArray *arrAnswers = [NSMutableArray array];
    NSMutableArray *arrValues  = [NSMutableArray array];
    for (int i = 0; i < cellDatas.count; i++) {
        SurveyModel *sm = [cellDatas objectAtIndex:i];
        if ([sm.surveyID isEqualToString:_surveyID]) {
            for (AnswerModel *answer in  sm.arrAnswer) {
                answer.SurveyValue = SurveyValue;
                answer.Selected    = @"1";
                
                NSMutableDictionary *dict  = [[NSMutableDictionary alloc] init];
                [dict setObject:answer.SurveyValue   forKey:@"SurveyValue"];
                [dict setObject:answer.SurveyValueID forKey:@"SurveyValueID"];
                [dict setObject:answer.Selected      forKey:@"Selected"];
                
                [arrAnswers addObject:answer];
                [arrValues  addObject:dict];
            }
            sm.arrAnswer = [NSArray arrayWithArray:arrAnswers];
            sm.values    = [NSArray arrayWithArray:arrValues];
            [self updateCellModel:sm atIndex:i];
        }
    }
}

#pragma mark - API update survey
- (void)updateSurvey:(NSMutableArray *)arrSurveys {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    [self showMBProcess];
    ShareData *shared = [ShareData instance];
    [shared.potentialCustomerProxy updatePotentialObjSurveyList:shared.currentUser.userName potentialObjID:self.PotentialID surveys:arrSurveys completehander:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self hideMBProcess];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        if(result == nil){
            [self showAlertBox:@"Thông Báo" message:Mesage_DataEmpty];
            [self hideMBProcess];
            return;
        }
        [self showAlertSave:@"Thông Báo" message:message];
        [self hideMBProcess];
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}

- (void) showAlertSave:(NSString *)title message:(NSString *)message {
    alertSave = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alertSave show];
}


#pragma mark - UIActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet == noticeSave) {
        if (buttonIndex == 0) {
            [self updateSurvey:arrSurveyAnswer];
            return;
        }
    }
    
    if (buttonIndex == 0) {
        [self setSelectedDateInField:selectedButton];
    }
}

#pragma mark - UIAlertView Delegate
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView == alertSave) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - select date
- (void)limitDate:(NSDate*)date minYear:(NSInteger)min maxYear:(NSInteger)max {
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:min];
    NSDate *minDate = [calendar dateByAddingComponents:comps toDate:date options:0];
    [comps setYear:max];
    NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:date options:0];
    [datePicker setMinimumDate:minDate];
    [datePicker setMaximumDate:maxDate];
}

-(void)setSelectedDateInField:(UIButton*)sender {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    //[dateFormatter setDateFormat:@"dd/MM/yyyy"];
    //[sender setTitle:[dateFormatter stringFromDate:datePicker.date] forState:UIControlStateNormal];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    chooseDate = [dateFormatter stringFromDate:datePicker.date];
    [sender setTitle:chooseDate forState:UIControlStateNormal];

    // update survey data of picker date
    NSMutableArray *values = [NSMutableArray array];
    NSMutableDictionary *dict  = [[NSMutableDictionary alloc] init];
    [dict setObject:chooseDate forKey:@"SurveyValue"];
    [dict setObject:@"1" forKey:@"Selected"];
    [values addObject:dict];
    [self updateSurveyID:_surveyID values:values];
    // update cell model at cellIndex when select date
    [self updateCellModelWithSurveyValue:chooseDate];
}

-(void)showDatePicker:(UIDatePicker *)modeDatePicker withButton:(UIButton*)sender{
    UIView *viewDatePicker = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width+30, 200)];
    [viewDatePicker setBackgroundColor:[UIColor clearColor]];
    
    modeDatePicker.hidden = NO;
    modeDatePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"Vietnamese"];
    
    [viewDatePicker addSubview:modeDatePicker];
    
    if(IS_OS_8_OR_LATER){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"\n\n\n\n\n\n\n\n\n\n" preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *doneAction = [UIAlertAction actionWithTitle:@"Xong" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            [self setSelectedDateInField:sender];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Đóng" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            
        }];
        [alertController addAction:doneAction];
        [alertController addAction:cancelAction];
        [alertController.view addSubview:viewDatePicker];
        
        //[self.view.window.rootViewController presentViewController:alertController animated:YES completion:nil];
        [self.navigationController presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
    selectedButton = sender;
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"\n\n\n\n\n\n\n\n\n\n" delegate:self cancelButtonTitle:@"Đóng" destructiveButtonTitle:@"Xong" otherButtonTitles:nil, nil];
    [actionSheet addSubview:viewDatePicker];
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
    return;
}

// Custom title navigation bar
- (UIView*) settitleOfNavigationBar:(NSString*)title subTitle:(NSString*)subTitle {
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:16];
    titleLabel.text = title;
    [titleLabel sizeToFit];
    
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 19, 0, 0)];
    subTitleLabel.backgroundColor = [UIColor clearColor];
    subTitleLabel.textColor = [UIColor blueColor];
    subTitleLabel.font = [UIFont systemFontOfSize:14];
    subTitleLabel.text = subTitle;
    [subTitleLabel sizeToFit];
    
    UIView *twoLineTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAX(subTitleLabel.frame.size.width, titleLabel.frame.size.width), 30)];
    [twoLineTitleView addSubview:titleLabel];
    [twoLineTitleView addSubview:subTitleLabel];
    
    float widthDiff = subTitleLabel.frame.size.width - titleLabel.frame.size.width;
    
    if (widthDiff > 0) {
        CGRect frame = titleLabel.frame;
        frame.origin.x = widthDiff / 2;
        titleLabel.frame = CGRectIntegral(frame);
    }else{
        CGRect frame = subTitleLabel.frame;
        frame.origin.x = fabsf(widthDiff) / 2;
        subTitleLabel.frame = CGRectIntegral(frame);
    }
    
    return twoLineTitleView;
}

// set number question done answered
- (void)setNumberQuestionDoneAnswered {
    count = 0;
    for (SurveyModel *sm in cellDatas) {
        for (AnswerModel*am in sm.arrAnswer) {
            if ([am.Selected isEqualToString:@"1"]) {
                count ++;
                break;
            }
        }
    }
    NSString *subTitle = StringFormat(@"%li/%li",(long)count,(long)cellDatas.count);
    self.navigationItem.titleView = [self settitleOfNavigationBar:self.title subTitle:subTitle];
}

@end
