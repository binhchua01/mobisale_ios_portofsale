//
//  ListSchedulePotentialViewController.h
//  MobiSale
//
//  Created by Tran Vu on 10/11/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "PickerViewController.h"
#import "TDDatePicker.h"

@interface ListSchedulePotentialViewController : TDDatePicker
-(instancetype) initWithNibName:(NSString *)nibNameOrNil withID:(NSString*)potentialObjID andSupporter:(NSString *)supporter;
@property (strong,nonatomic) NSString *potentialObjID;
@property (weak, nonatomic) IBOutlet UIButton *btnFromDate;

@property (weak, nonatomic) IBOutlet UIButton *btnToDate;
@property (weak, nonatomic) IBOutlet UITableView *listTableView;

@property (weak, nonatomic) IBOutlet UILabel *ldlSL;
@property (weak, nonatomic) IBOutlet UILabel *lblFullName;
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *imgUpDown;
@property (strong, nonatomic) NSString *ScheduleDesription;
@property (strong, nonatomic) NSString *ScheduleDate;
@property (strong, nonatomic) NSString *PotentialObjScheduleID;
@property (strong, nonatomic) NSString *Supporter;
@end
