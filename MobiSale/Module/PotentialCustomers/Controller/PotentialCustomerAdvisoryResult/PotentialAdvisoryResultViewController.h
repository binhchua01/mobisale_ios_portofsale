//
//  PotentialAdvisoryResultViewController.h
//  MobiSale
//
//  Created by ISC on 6/23/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "PotentialCustomerDetailRecord.h"

@interface PotentialAdvisoryResultViewController :BaseViewController

@property (weak, nonatomic) IBOutlet UIImageView *imageUpDown;

@property (nonatomic, weak) IBOutlet UIButton *btnAdvisoryResult;

@property (nonatomic, weak) IBOutlet UITextField *txtContent;

@property (weak, nonatomic) IBOutlet UITableView *advisoryResultTableView;

@property (weak, nonatomic) IBOutlet UIButton *btnUpdate;

@property (weak, nonatomic) IBOutlet UILabel *lblAdvisoryResultNumber;

@property (weak, nonatomic) IBOutlet UIView *updateAdvisoryResultView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintUpdateAdvisoryResultView;

@property (copy, nonatomic) NSString *potentialID;

@property (strong, nonatomic) PotentialCustomerDetailRecord *potentialRecord;

@end
