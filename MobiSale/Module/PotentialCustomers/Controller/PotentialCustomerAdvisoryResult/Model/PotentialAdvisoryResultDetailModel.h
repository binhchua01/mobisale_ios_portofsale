//
//  PotentialAdvisoryResultDetailModel.h
//  MobiSale
//
//  Created by ISC on 6/23/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PotentialAdvisoryResultDetailModel : NSObject

@property (nonatomic, assign) NSInteger rowNumber;
@property (nonatomic, copy)   NSString  *advisoryTimes;
@property (nonatomic, copy)   NSString  *advisoryResult;
@property (nonatomic, copy)   NSString  *createDate;

+ (id)potentialAdvisoryResultDetailModelWithDict:(NSDictionary *)dict;

@end
