//
//  PotentialAdvisoryResultValueModel.h
//  MobiSale
//
//  Created by ISC on 6/23/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PotentialAdvisoryResultValueModel : NSObject

@property (nonatomic, assign) NSInteger ID;
@property (nonatomic, assign) NSInteger position;
@property (nonatomic, copy)   NSString  *value;
@property (nonatomic, copy)   NSString  *desc;

+ (id)potentialAdvisoryResultValueModelWithDict:(NSDictionary *)dict;

@end
