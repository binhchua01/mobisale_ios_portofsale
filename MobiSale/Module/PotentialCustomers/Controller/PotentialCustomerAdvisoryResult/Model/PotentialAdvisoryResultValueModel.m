//
//  PotentialAdvisoryResultValueModel.m
//  MobiSale
//
//  Created by ISC on 6/23/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "PotentialAdvisoryResultValueModel.h"

@implementation PotentialAdvisoryResultValueModel

+(id)potentialAdvisoryResultValueModelWithDict:(NSDictionary *)dict {
    PotentialAdvisoryResultValueModel *model = [[PotentialAdvisoryResultValueModel alloc] init];
    
    model.ID       = [dict[@"ID"] integerValue];
    model.position = [dict[@"Position"]   integerValue];
    model.value    = StringFormat(@"%@",dict[@"Value"]);
    model.desc     = StringFormat(@"%@",dict[@"Desc"]);

    return model;
}

@end
