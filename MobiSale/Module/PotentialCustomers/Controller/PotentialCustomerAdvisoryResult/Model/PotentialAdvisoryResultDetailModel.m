//
//  PotentialAdvisoryResultDetailModel.m
//  MobiSale
//
//  Created by ISC on 6/23/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "PotentialAdvisoryResultDetailModel.h"

@implementation PotentialAdvisoryResultDetailModel

+(id)potentialAdvisoryResultDetailModelWithDict:(NSDictionary *)dict {
    PotentialAdvisoryResultDetailModel *model = [[PotentialAdvisoryResultDetailModel alloc] init];
    
    model.rowNumber      = [dict[@"RowNumber"] integerValue];
    model.advisoryTimes  = StringFormat(@"%@",dict[@"AdvisoryTimes"]);
    model.advisoryResult = StringFormat(@"%@",dict[@"AdvisoryResult"]);
    model.createDate     = StringFormat(@"%@",dict[@"CreateDate"]);
    
    return model;
}

@end
