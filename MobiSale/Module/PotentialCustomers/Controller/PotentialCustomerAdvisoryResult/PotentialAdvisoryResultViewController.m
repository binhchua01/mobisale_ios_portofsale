//
//  PotentialAdvisoryResultViewController.m
//  MobiSale
//
//  Created by ISC on 6/23/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "PotentialAdvisoryResultViewController.h"
#import "NIDropDown.h"
#import "ShareData.h"
#import "PotentialAdvisoryResultDetailModel.h"
#import "PotentialAdvisoryResultValueModel.h"
#import "RegisteredFormController.h"
#import "ListRegisteredFormViewController.h"

@interface PotentialAdvisoryResultViewController ()<NIDropDownDelegate, UIAlertViewDelegate>

@end

@implementation PotentialAdvisoryResultViewController {
    NIDropDown          *dropDownView; // drop down view show when clicked button
    NSMutableArray      *arrAdvisoryResultUpdate;    // data received from server
    NSInteger           totalPage;
    NSInteger           currentPage;
    CGFloat             startOffset;
    BOOL                isSearchViewShow; // show/hide search conditions
    
    PotentialAdvisoryResultDetailModel   *advisoryResultDetailModel;
    PotentialAdvisoryResultValueModel    *advisoryResultValueModel;
    
    NSArray         *arrAdvisoryResultValue;
    NSArray         *arrAdvisoryResultDetail;

    UIButton        *btnSelected;
    UIAlertView     *alertView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // set navigation title
    self.title =  @"KẾT QUẢ TƯ VẤN";
    // show conditions search view
    isSearchViewShow = YES;
    // set offset scroll view of table
    startOffset = -20;
    // Set empty label advisory result number
    self.lblAdvisoryResultNumber.text = @"";
    // Set button update
    [self.btnUpdate styleButtonUpdate];
    [self.btnUpdate setShadow];
    // Get list last advisory result
    [self getPotentialAdvisoryResultDetail];
    
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            // Get reason advisory stop
            [self getPotentialAdvisoryResultValue];
        });
    });
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
// Handle touch on screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self handleSingleTap:nil];
}

#pragma mark - show or hide conditions search view
- (void)showUpdateAdvisoryResultView:(BOOL)status {
    isSearchViewShow = status;
    self.updateAdvisoryResultView.hidden = !status;
    
    /*
     * show conditions search View
     */
    if (status) {
        self.heightConstraintUpdateAdvisoryResultView.constant = 168;
        self.imageUpDown.image = [UIImage imageNamed:@"up-icon-512"];
        
    } else {
        /*
         * hide conditions search View
         */
        self.heightConstraintUpdateAdvisoryResultView.constant = 0;
        self.imageUpDown.image = [UIImage imageNamed:@"down-icon-512"];
    }
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

#pragma mark - NIDropDown method
- (void)showDropDownAtButton:(UIButton *)sender withArrayData:(NSArray*)arrData{
    [self.view endEditing:YES];
    
    if (dropDownView == nil) {
        NSMutableArray *arrOutput = [[NSMutableArray alloc] init];
        for (advisoryResultValueModel in arrData) {
            [arrOutput addObject:advisoryResultValueModel.desc];
        }
        
        CGFloat height = 40*arrData.count;
        if (arrData.count > 3) {
            height = 110;
        }
        CGFloat width = 0;
        dropDownView = [[NIDropDown alloc] showDropDownAtButton:sender height:&height width:&width data:arrOutput images:nil animation:@"down"];
        dropDownView.delegate = self;
        
        return;
    }
    [dropDownView hideDropDown:sender];
    [self rel];
}

- (void)niDropDownDelegateMethod:(NIDropDown *)sender {
    [self rel];
}

- (void)rel {
    dropDownView = nil;
}

- (void)dropDownListView:(NIDropDown *)dropDownListView didSelectIndexPath:(NSIndexPath *)indexPath atButton:(UIButton *)button{
    [self didDropDownSelected:indexPath.row atButton:button];
}

- (void)didDropDownSelected:(NSInteger)row atButton:(UIButton *)button {
    if (button == self.btnAdvisoryResult) {
        advisoryResultValueModel = arrAdvisoryResultValue[row];
        
        [self enableContentTextField];

        return;
    }
    
}

// Hide content textField if advisory result ID = -1 ("Khác")
- (void)enableContentTextField {
    BOOL status = NO;
    if (advisoryResultValueModel.ID == -1) {
        status = YES;
    }
    
    self.txtContent.userInteractionEnabled = status;
    self.txtContent.enabled = status;
    if (!status) {
        self.txtContent.text = @"";
    }
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
    if (dropDownView !=nil) {
        [dropDownView hideDropDown:self.btnAdvisoryResult];
        [self rel];
    }
}

#pragma mark - UITableView datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return arrAdvisoryResultDetail.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 79;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reuseIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if( cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
    }
    
    cell.textLabel.textColor = [UIColor colorWithRed:0.133 green:0.698 blue:0.569 alpha:1];
    cell.detailTextLabel.font = [UIFont systemFontOfSize:14.0f];
    [cell.detailTextLabel setNumberOfLines:2];
    
    if (arrAdvisoryResultDetail.count > 0) {
        advisoryResultDetailModel = arrAdvisoryResultDetail[indexPath.section];
        
        cell.textLabel.text =advisoryResultDetailModel.advisoryTimes;
        cell.detailTextLabel.text = advisoryResultDetailModel.advisoryResult;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(cell.contentView.frame.origin.x, cell.contentView.frame.size.height + 12, cell.contentView.frame.size.width-5, 21)];
        UIFontDescriptor *fontDesc = [label.font.fontDescriptor fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitItalic];
        label.font = [UIFont fontWithDescriptor:fontDesc size:13.0];
        label.textColor = [UIColor blackColor];
        label.textAlignment = NSTextAlignmentRight;
        label.tag  = 3;
        label.text = advisoryResultDetailModel.createDate;
        
        [cell.contentView addSubview:label];


    }
    
    return cell;
}

#pragma mark - Scroll in table view delegate
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    CGFloat currentOffset = scrollView.contentOffset.y;
    
    if (currentOffset > startOffset) {
        [self showUpdateAdvisoryResultView:NO];
    }

}

#pragma mark - Get List reason advisory stop
- (void)getPotentialAdvisoryResultValue {
    if (arrAdvisoryResultValue.count > 0) {
        return;
    }
    
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
//    [self showMBProcess];

    [shared.potentialCustomerProxy getPotentialAdvisoryResultValue:dict completeionHander:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        arrAdvisoryResultValue = result;

        if(![errorCode isEqualToString:@"0"] || result == nil || arrAdvisoryResultValue.count <= 0) {
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",@"Không có kết quả trả về"]];
            [self hideMBProcess];
            
            return ;
        }
        
        advisoryResultValueModel = arrAdvisoryResultValue[0];
        
        [self.btnAdvisoryResult setTitle:advisoryResultValueModel.desc forState:UIControlStateNormal];
        
        [self enableContentTextField];
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
        [self hideMBProcess];

    }];
    
}

#pragma mark - Get List last advisory result
- (void)getPotentialAdvisoryResultDetail {
    
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    [dict setObject:self.potentialID forKey:@"PotentialID"];

    [self showMBProcess];
    
    [shared.potentialCustomerProxy getPotentialAdvisoryResultDetail:dict completeionHander:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        arrAdvisoryResultDetail = result;
        
        if(![errorCode isEqualToString:@"0"] || result == nil || arrAdvisoryResultDetail.count <= 0) {
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",@"Không có kết quả trả về"]];
            [self hideMBProcess];
            
            return ;
        }
        
        self.lblAdvisoryResultNumber.text = StringFormat(@"%lu", (unsigned long)arrAdvisoryResultDetail.count);
        
        // Hide update advisory result view
        [self showUpdateAdvisoryResultView:NO];
        // Reload data for tableView
        self.advisoryResultTableView.hidden = NO;
        [self.advisoryResultTableView reloadData];
        
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
        [self hideMBProcess];
        
    }];
}

#pragma mark - Update advisory result
- (void)updatePotentialAdvisoryResult {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    [dict setObject:self.potentialID forKey:@"PotentialID"];
    [dict setObject:StringFormat(@"%li",(long)advisoryResultValueModel.ID) forKey:@"SurveyValueID"];
    [dict setObject:self.txtContent.text forKey:@"Value"];
    [dict setObject:self.potentialRecord.caseID forKey:@"CaseID"];
    [dict setObject:StringFormat(@"%li", (long)self.potentialRecord.source) forKey:@"Source"];
    
    [self showMBProcess];
    
    [shared.potentialCustomerProxy updatePotentialAdvisoryResult:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        
        if ([self.btnAdvisoryResult.titleLabel.text isEqualToString:@"OK. Tạo hợp đồng"]) {
            
            [self showAlert:message];
        }
        else{
            
            [self showAlertBoxWithDelayDismiss:@"Thông Báo" message:message];
        }
        
        [self hideMBProcess];

        if ([errorCode intValue] == 1) {
            [self getPotentialAdvisoryResultDetail];
        }
        

    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
        [self hideMBProcess];
    }];
     
}

- (IBAction)btnUpdateAdvisoryResult_Clicked:(id)sender {
    [self handleSingleTap:nil];
    [self showUpdateAdvisoryResultView:!isSearchViewShow];

}

- (IBAction)btnAdvisoryResult_Clicked:(id)sender {
    if (dropDownView != nil && btnSelected != sender) {
        [dropDownView hideDropDown:btnSelected];
        dropDownView = nil;
    }
    btnSelected = sender;
    [self showDropDownAtButton:sender withArrayData:arrAdvisoryResultValue];
}

- (IBAction)btnUpdate_Clicked:(id)sender {
    [self.view endEditing:YES];
    if (dropDownView != nil && btnSelected != sender) {
        [dropDownView hideDropDown:self.btnAdvisoryResult];
        dropDownView = nil;
    }
    
    [self updatePotentialAdvisoryResult];

}


- (void)showAlert:(NSString *)message
{
    
    alertView = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    alertView.tag = 4;
    [alertView show];
}

- (void)alertView:(UIAlertView *)alertView1 clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            if (alertView1.tag == 4) {
                
//                RegisteredFormController *vc = [[RegisteredFormController alloc]initWithNibName:@"RegisteredFormController" bundle:nil];
                ListRegisteredFormViewController *vc = [[ListRegisteredFormViewController alloc] initWithNibName:@"ListRegisteredFormViewController" bundle:nil];
                vc.rc = [self convertPotentialCustomerRecordToRegistrationRecord:_potentialRecord];
                [self.navigationController pushViewController:vc animated:YES];
                
            }
            break;
            
        default:
            break;
    }
}

- (RegistrationFormDetailRecord *)convertPotentialCustomerRecordToRegistrationRecord: (PotentialCustomerDetailRecord *)potentialRecord{
    RegistrationFormDetailRecord *rc = [[RegistrationFormDetailRecord alloc]init];
    rc.Address = potentialRecord.Address;
    rc.BillTo_City = (potentialRecord.BillTo_City == nil) ?@"":potentialRecord.BillTo_City;
    rc.BillTo_District = (potentialRecord.BillTo_District == nil) ?@"0":potentialRecord.BillTo_District;
    rc.BillTo_Number = (potentialRecord.BillTo_Number == nil) ?@"":potentialRecord.BillTo_Number;
    rc.BillTo_Street = (potentialRecord.BillTo_Street == nil) ?@"0":potentialRecord.BillTo_Street;
    rc.BillTo_Ward = (potentialRecord.BillTo_Ward == nil) ?@"0":potentialRecord.BillTo_Ward;
    
    rc.BookPortType = @"";
    rc.BranchCode = @"";
    rc.CableStatus = @"";
    rc.Contact = @"";
    rc.Contact_1 = potentialRecord.Contact_1;
    rc.Contact_2 = potentialRecord.Contact_2;
    rc.Contract = @"";
    rc.CreateBy = potentialRecord.CreateBy;
    rc.CreateDate = potentialRecord.CreateDate;
    rc.CurrentHouse = @"";
    rc.CusType = @"";
    rc.CusTypeDetail = potentialRecord.CustomerType;
    rc.CustomerType = @"0";
    rc.Deposit = @"";
    rc.DepositBlackPoint = @"";
    rc.Description = @"";
    rc.DescriptionIBB = @"";
    rc.DivisionID = @"";
    rc.Email = potentialRecord.Email;
    rc.EoC = @"";
    rc.Floor = potentialRecord.Floor;
    rc.FullName = potentialRecord.FullName;
    rc.ID = nil;
    rc.INSCable = @"";
    rc.IPTVBoxCount = @"0";
    rc.IPTVChargeTimes = @"0";
    rc.IPTVDeviceTotal = @"0";
    rc.IPTVHBOChargeTimes = @"0";
    rc.IPTVHBOPrepaidMonth = @"0";
    rc.IPTVKPlusChargeTimes = @"0";
    rc.IPTVKPlusPrepaidMonth = @"0";
    rc.IPTVPLCCount = @"0";
    rc.IPTVPackage = @"0";
    rc.IPTVPrepaid = @"0";
    rc.IPTVPrepaidTotal = @"0";
    rc.IPTVPromotionID = @"0";
    rc.IPTVRequestDrillWall = @"";
    rc.IPTVRequestSetUp = @"0";
    rc.IPTVReturnSTBCount = @"0";
    rc.IPTVStatus = @"";
    rc.IPTVTotal =@"0";
    rc.IPTVUseCombo = @"0";
    rc.IPTVVTCChargeTimes = @"0";
    rc.IPTVVTCPrepaidMonth = @"0";
    rc.IPTVVTVChargeTimes = @"0";
    rc.IPTVVTVPrepaidMonth = @"0";
    rc.ISPType = potentialRecord.ISPType;
    rc.Image = @"";
    rc.InDType = @"0";
    rc.InDoor = @"";
    rc.InternetTotal = @"";
    rc.Latlng = potentialRecord.Latlng;
    rc.LegalEntity = @"";
    rc.LocalType = @"";
    rc.LocalTypeName = @"";
    rc.LocationID = potentialRecord.LocationID;
    rc.Lot = @"";
    rc.MapCode = @"";
    rc.Modem = @"";
    rc.NameVilla = potentialRecord.NameVilla;
    rc.Note = potentialRecord.Note;
    rc.ODCCableType = @"";
    rc.ObjID = @"0";
    rc.ObjectType = potentialRecord.ISPType;
    rc.OutDType = @"";
    rc.OutDoor = @"";
    rc.PartnerID = @"";
    rc.Passport = potentialRecord.Passport;
    rc.PaymentAbility = @"";
    rc.PhoneNumber = @"";
    rc.Phone_1 = potentialRecord.Phone_1;
    rc.Phone_2 = potentialRecord.Phone_2;
    rc.Position = potentialRecord.Position;
    rc.PromotionID = @"";
    rc.PotentialID = potentialRecord.ID;
    rc.PromotionName = @"";
    rc.RegCode = potentialRecord.RegCode;
    rc.Room = potentialRecord.Room;
    rc.StatusDeposit = @"";
    rc.Supporter = potentialRecord.Supporter;
    rc.TaxId = potentialRecord.TaxID;
    rc.Total = @"0";
    rc.TypeHouse = (potentialRecord.TypeHouse == nil) ?@"0":potentialRecord.TypeHouse;
    rc.Type_1 = @"0";
    rc.Type_2 = @"0";
    rc.UserName = @"";
    rc.TDName = @"";
    
    return rc;
}

@end
