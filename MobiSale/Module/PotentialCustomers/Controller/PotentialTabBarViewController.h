//
//  PotentialTabBarViewController.h
//  MobiSale
//
//  Created by ISC on 9/20/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShareData.h"
#import "UIViewController+FPTCustom.h"
#import "BaseViewController.h"
#import "ListPotentialCustomersViewController.h"
#import "MapsListPotentialCustomersViewController.h"

@interface PotentialTabBarViewController : UITabBarController<UIAlertViewDelegate>

@property (strong, nonatomic) ListPotentialCustomersViewController *listPotential, *listPotential18006000;
@property (strong, nonatomic) MapsListPotentialCustomersViewController *listPotentialMap;

@end
