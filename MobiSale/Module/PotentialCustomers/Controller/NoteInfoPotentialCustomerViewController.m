//
//  NoteInfoPotentialCustomerViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 11/18/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "NoteInfoPotentialCustomerViewController.h"
#import "ShareData.h"
#import "Common.h"

@interface NoteInfoPotentialCustomerViewController ()<UIAlertViewDelegate, UITextFieldDelegate, UITextViewDelegate> {
    UIAlertView *alertCheckSave;
    IBOutlet NSLayoutConstraint *constraintsTop;
    IBOutlet NSLayoutConstraint *constraintsLeft;
    IBOutlet NSLayoutConstraint *constraintsRight;
    
}

@end

@implementation NoteInfoPotentialCustomerViewController
@synthesize record;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"TÓM TẮT";
    self.screenName = @"KHÁCH HÀNG TIỀM NĂNG (NOTE)";
    self.txtFullName.delegate    = self;
    self.txtPhoneNumber.delegate = self;
    self.txtAddress.delegate     = self;
    self.txtNote.delegate        = self;
    if (!IS_OS_8_OR_LATER) {
        constraintsTop.constant = 62;
        constraintsLeft.constant = 16;
        constraintsRight.constant = 0;
    }
    [self setType];
    // Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];

}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnUpdate_Clicked:(id)sender {
    alertCheckSave = [[UIAlertView alloc] initWithTitle:@"Thông báo" message:@"Bạn có muốn lưu thông tin khách hàng" delegate:self cancelButtonTitle:@"Có" otherButtonTitles:@"Không", nil];
    [alertCheckSave show];
}

- (void)setType{
    [self.view setMultipleTouchEnabled:YES];
    UITapGestureRecognizer *tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:tapper];
    
    self.txtNote.layer.borderColor  = [UIColor colorMain].CGColor;
    self.txtNote.layer.borderWidth  = 0.7f;
    self.txtNote.layer.cornerRadius = 6.0f;
    
    self.txtAddress.layer.borderColor  = [UIColor colorMain].CGColor;
    self.txtAddress.layer.borderWidth  = 0.7f;
    self.txtAddress.layer.cornerRadius = 6.0f;
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *) sender{
    [self.view endEditing:YES];
}

- (void)loadData {
    ShareData *shared = [ShareData instance];
    record = shared.potentialRecord;
    self.txtFullName.text    = record.FullName;
    self.txtAddress.text     = record.Address;
    self.txtPhoneNumber.text = record.Phone_1;
    self.txtNote.text        = record.Note;
    
    
}

#pragma mark - UITextField delegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    ShareData *shared = [ShareData instance];
    if (textField == self.txtFullName) {
        shared.potentialRecord.FullName = self.txtFullName.text;
        return;
    }
    if (textField == self.txtPhoneNumber) {
        shared.potentialRecord.Phone_1  = self.txtPhoneNumber.text;
        return;
    }
   
}

#pragma mark - UITextView delegate whilespace \n

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
    }
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    ShareData *shared = [ShareData instance];

    if (textView == self.txtAddress) {
        shared.potentialRecord.Address = self.txtAddress.text ;
        return;
    }
    if (textView == self.txtNote) {
        shared.potentialRecord.Note    = self.txtNote.text;
        return;
    }
}

@end
