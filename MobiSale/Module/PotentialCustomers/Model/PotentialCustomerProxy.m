//
//  PotentialCustomerProxy.m
//  MobiSale
//
//  Created by ISC-DanTT on 11/17/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "PotentialCustomerProxy.h"
#import "ShareData.h"
#import "PotentialCustomerFormRecord.h"
#import "PotentialCustomerDetailRecord.h"
#import "PotentialAdvisoryResultValueModel.h"
#import "PotentialAdvisoryResultDetailModel.h"
#import "FPTUtility.h"
#import "ListSchedulePotentialModel.h"
#import "AddNewSchedulePotentialModel.h"


#define GetPotentialCustomersList   @"GetPotentialObjList"
#define GetPotentialCustomerDetail  @"GetPotentialObjDetail"
#define UpdatePotentialCustomerInfo @"UpdatePotentialObj"
#define GetPotentialObjSurveyList   @"GetPotentialObjSurveyList"
#define mAcceptPotential            @"AcceptPotential"
#define mGetPotentialByCode         @"GetPotentialByCode"
#define UpdatePotentialObjDetailList    @"UpdatePotentialObjDetailList"

// Potential customer advisory result
#define mGetPotentialAdvisoryResultValue @"GetPotentialAdvisoryResultValue"
#define mGetPotentialAdvisoryResultDetail @"GetPotentialAdvisoryResultDetail"
#define mUpdatePotentialAdvisoryResult    @"UpdatePotentialAdvisoryResult"
#define mGetAllPotentialObjSchedule       @"GetAllPotentialObjSchedule"
#define mUpdatePotentialObjSchedule       @"UpdatePotentialObjSchedule"
#define mDeletePotentialObjSchedule       @"DeletePotentialObjSchedule"

@implementation PotentialCustomerProxy

#pragma mark - Get List RegistrationForm
- (void)GetListPotentialCustomersWithInput:(NSDictionary *)dict completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    BaseOperation * callOp = [[BaseOperation alloc] init];
    
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@", urlAPI, GetPotentialCustomersList)]; //POST
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetListPotentialCustomers:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
    
}

- (void)endGetListPotentialCustomers:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *responseResult = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    NSArray *listObject = [responseResult objectForKey:@"ListObject"]?:nil;
    
    if(listObject.count <= 0){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[responseResult objectForKey:@"ErrorCode"]);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, @"Có lỗi, vui lòng thử lại!");
        return;
    }
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in listObject){
        PotentialCustomerFormRecord *p = [self ParseRecordList:d];

        [mArray addObject:p];
    }
    
    handler(mArray, ErrorCode, @"");
}

- (void)GetAllPotentialObjSchedule:(NSDictionary *)dict completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
     BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@", urlAPI, mGetAllPotentialObjSchedule)]; //POST
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetAllPotentialObjSchedule:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];

}

- (void)endGetAllPotentialObjSchedule:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }

    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *responseResult = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    NSArray *listObject = [responseResult objectForKey:@"ListObject"]?:nil;
    
    if(listObject.count <= 0){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[responseResult objectForKey:@"ErrorCode"]);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, @"Có lỗi, vui lòng thử lại!");
        return;
    }
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in listObject){
        ListSchedulePotentialModel *p = [[ListSchedulePotentialModel alloc]initWithDictionary:d];
        
        [mArray addObject:p];
    }
    
    handler(mArray, ErrorCode, @"");

    
}
//UpdatePotentialObjSchedule
- (void)UpdatePotentialObjSchedule:(NSDictionary *)dict completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@", urlAPI, mUpdatePotentialObjSchedule)]; //POST
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSString * strUTF8 = @"";
    NSMutableString *s = [NSMutableString stringWithString:postString];
    [s replaceOccurrencesOfString:@"\\" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    strUTF8 = [NSString stringWithString:s];
    
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:strUTF8];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endUpdatePotentialObjSchedule:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];

}
- (void)endUpdatePotentialObjSchedule:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *responseResult = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    NSArray *listObject = [responseResult objectForKey:@"ListObject"]?:nil;
    NSString *ErrorCode = StringFormat(@"%@",[responseResult objectForKey:@"ErrorCode"]);
    //NSInteger ErrorCode = [[responseResult objectForKey:@"Error" ]integerValue];
    NSString *message = StringFormat(@"%@",[responseResult objectForKey:@"Error"]);
    
    if(listObject == (id)[NSNull null]) {
        handler(nil, ErrorCode, message);
        return;
    }
    
    if(listObject.count <= 0){
        handler(nil, ErrorCode, message);
        return;
    }
    
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, @"", @"Có lỗi, vui lòng thử lại!");
        return;
    }
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in listObject){
        AddNewSchedulePotentialModel *p = [[AddNewSchedulePotentialModel alloc]initWithDictionary:d];
        
        [mArray addObject:p];
    }
    
    handler(mArray, ErrorCode, message);

}

//DeletePotentialObjSchedule

- (void)DeletePotentialObjSchedule:(NSDictionary *)dict completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@", urlAPI, mDeletePotentialObjSchedule)]; //POST
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endDeletePotentialObjSchedule:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
    
}
- (void)endDeletePotentialObjSchedule:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *responseResult = [jsonDict objectForKey:@"ResponseResult"]?:nil;
   // NSArray *listObject = [responseResult objectForKey:@"ListObject"]?:nil;
    
//    if(listObject.count <= 0){
//        handler(nil, @"", @"");
//        return;
//    }
    
    NSString *ErrorCode = StringFormat(@"%@",[responseResult objectForKey:@"ErrorCode"]);
    //NSInteger ErrorCode = [[responseResult objectForKey:@"Error" ]integerValue];
    NSString *message = StringFormat(@"%@",[responseResult objectForKey:@"Error"]);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, @"", @"Có lỗi, vui lòng thử lại!");
        return;
    }

    handler(nil,ErrorCode,message);
    
}

- (void)GetPotentialCustomerDetailWithID:(NSString*)ID Completehander:(DidGetResultBlock)handler
                            errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    
    NSString *username = [ShareData instance].currentUser.userName;
    
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, GetPotentialCustomerDetail)]; //POST
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:username ?:@"" forKey:@"UserName"];
    [dict setObject:ID?:@"0" forKey:@"ID"];
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetPotentialCustomerDetail:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
    
}

- (void)endGetPotentialCustomerDetail:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *responseResult = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    NSArray *listObject = [responseResult objectForKey:@"ListObject"]?:nil;
    
    NSString *ErrorCode = StringFormat(@"%@",[responseResult objectForKey:@"ErrorCode"]);
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, @"Có lỗi, vui lòng thử lại!");
        return;
    }
    
    if(listObject.count <= 0){
        handler(nil, @"", @"");
        return;
    }
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in listObject){
        PotentialCustomerDetailRecord *p = [self ParseRecordDetail:d];

        [mArray addObject:p];
    }
    
    handler(mArray, ErrorCode, @"");
}
//vutt12
- (void)updatePotentialCustomerInfo:(NSMutableDictionary*)info Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, UpdatePotentialCustomerInfo)]; //POST
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    NSString *postString = @"";
    postString = [info JSONRepresentation];
    //vutt11
    NSString *strUTF8 = @"";
    NSMutableString *s = [NSMutableString stringWithString:postString];
    [s replaceOccurrencesOfString:@"\\" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    
   // [s replaceOccurrencesOfString:@"\'" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    
    strUTF8 = [NSString stringWithString:s];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:strUTF8];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endUpdatePotentialCustomerInfo:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
    
}

- (void)endUpdatePotentialCustomerInfo:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *responseResult = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    NSArray *listObject = [responseResult objectForKey:@"ListObject"]?:nil;
    
    NSString *errorCode = StringFormat(@"%@",[responseResult objectForKey:@"ErrorCode"]);
    
    if(![errorCode isEqualToString:@"0"]){
        NSString *error = StringFormat(@"%@",[responseResult objectForKey:@"Error"]);
        handler(nil, errorCode, error);
        return;
    }
    NSString *failure = StringFormat(@"%@",[[listObject objectAtIndex:0] objectForKey:@"Failure"]);
    if (![failure isEqualToString:@"0"]) {
        NSString *errorService = StringFormat(@"%@",[[listObject objectAtIndex:0] objectForKey:@"ErrorService"]);
        handler(nil, failure, errorService);
        return;
        
    }
    NSString *message = StringFormat(@"%@",[[listObject objectAtIndex:0] objectForKey:@"Result"]);
    handler(listObject, errorCode, message);
}

-(void)getAdrressFromLatLong:(CGFloat)lat lon:(CGFloat)lon completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler{
    NSString *urlString = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&amp;sensor=false",lat,lon];
    BaseOperation * callOp = [[BaseOperation alloc] init];
    
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@",urlString)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetAdrressFromLatLong:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    [callOp start];
}

- (void)endGetAdrressFromLatLong:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
  /*Parse json response*/
    
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSArray *listObject = [jsonDict objectForKey:@"results"]?:nil;
    NSString *status = StringFormat(@"%@",[jsonDict objectForKey:@"status"]);
    if(![status isEqualToString:@"OK"]){
        handler(nil, status, @"Có lỗi, không lấy được địa chỉ!");
        return;
    }
    
    if(listObject.count <= 0){
        handler(nil, @"", @"Không lấy được địa chỉ");
        return;
    }
    NSString *address = nil;
    address = [[listObject objectAtIndex:0] valueForKey:@"formatted_address"];
    handler(address, status, @"");
}

#pragma mark - getPotentialObjSurveyList

- (void) getPotentialObjSurveyList:(NSString *)userName potentialObjID:(NSString *)potentialObjID completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler{
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, GetPotentialObjSurveyList)]; //POST
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:userName ?:@"" forKey:@"UserName"];
    [dict setObject:potentialObjID ?:@"" forKey:@"PotentialObjID"];
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetPotentialObjSurveyList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
}

- (void)endGetPotentialObjSurveyList:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *responseResult = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    NSArray *listObject = [responseResult objectForKey:@"ListObject"]?:nil;
    
    NSString *ErrorCode = StringFormat(@"%@",[responseResult objectForKey:@"ErrorCode"]);
    NSString *Error = StringFormat(@"%@",[responseResult objectForKey:@"Error"]);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, Error);
        return;
    }
    
    if(listObject.count <= 0){
        handler(nil, @"", @"");
        return;
    }
    
    handler(listObject, ErrorCode, Error);
}

#pragma mark - updatePotentialObjSurveyList

- (void) updatePotentialObjSurveyList:(NSString *)userName potentialObjID:(NSString *)potentialObjID surveys:(NSArray *)arrSurveys completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, UpdatePotentialObjDetailList)]; //POST
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:userName ?:@"" forKey:@"UserName"];
    [dict setObject:potentialObjID ?:@"" forKey:@"PotentialObjID"];
    [dict setObject:arrSurveys ?:@[] forKey:@"Surveys"];
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endUpdatePotentialObjSurveyList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
    
}

- (void)endUpdatePotentialObjSurveyList:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *responseResult = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    NSArray *listObject = [responseResult objectForKey:@"ListObject"]?:nil;
    
    NSString *ErrorCode = StringFormat(@"%@",[responseResult objectForKey:@"ErrorCode"]);
    NSString *Error = StringFormat(@"%@",[responseResult objectForKey:@"Error"]);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, Error);
        return;
    }
    
    if(listObject.count <= 0){
        handler(nil, @"", @"");
        return;
    }
    NSString *ErrorService = StringFormat(@"%@",[[listObject objectAtIndex:0] objectForKey:@"ErrorService"]);
    if ([ErrorService isEqualToString:@"<null>"]) {
        ErrorService = @"";
    }
    NSString *Message = StringFormat(@"%@",[[listObject objectAtIndex:0] objectForKey:@"Message"]);
    
    handler(listObject, ErrorCode, StringFormat(@"%@ \n %@",Message, ErrorService));
}

-(PotentialCustomerFormRecord *) ParseRecordList:(NSDictionary *)dict{
    PotentialCustomerFormRecord *rc = [[PotentialCustomerFormRecord alloc]init];
    NSString *str = @"(null)";
    rc.Address = StringFormat(@"%@",[dict objectForKey:@"Address"]);
    rc.Address = ([rc.Address isEqualToString: str]) ? @"": rc.Address ;
    rc.CreateBy = StringFormat(@"%@",[dict objectForKey:@"CreateBy"]);
    rc.CreateBy = ([rc.CreateBy isEqualToString: str]) ? @"": rc.CreateBy;
    rc.CreateDate = StringFormat(@"%@",[dict objectForKey:@"CreateDate"]);
    rc.CreateDate = ([rc.CreateDate isEqualToString: str]) ? @"": rc.CreateDate;
    rc.Email = StringFormat(@"%@",[dict objectForKey:@"Email"]);
    rc.Email = ([rc.Email isEqualToString: str]) ? @"": rc.Email;
    rc.Facebook = StringFormat(@"%@",[dict objectForKey:@"Facebook"]);
    rc.Facebook = ([rc.Facebook isEqualToString: str]) ? @"": rc.Facebook;
    rc.FullName = StringFormat(@"%@",[dict objectForKey:@"FullName"]);
    rc.FullName = ([rc.FullName isEqualToString: str]) ? @"": rc.FullName;
    rc.ID = StringFormat(@"%@",[dict objectForKey:@"ID"]);
    rc.ID = ([rc.ID isEqualToString: str]) ? @"": rc.ID;
    rc.Latlng = StringFormat(@"%@",[dict objectForKey:@"Latlng"]);
    rc.Latlng = ([rc.Latlng isEqualToString: str]) ? @"": rc.Latlng;
    rc.LocationID = StringFormat(@"%@",[dict objectForKey:@"LocationID"]);
    rc.LocationID = ([rc.LocationID isEqualToString: str]) ? @"": rc.LocationID;
    rc.Note = StringFormat(@"%@",[dict objectForKey:@"Note"]);
    rc.Note = ([rc.Note isEqualToString: str]) ? @"": rc.Note;
    rc.Phone1 = StringFormat(@"%@",[dict objectForKey:@"Phone1"]);
    rc.Phone1 = ([rc.Phone1 isEqualToString: str]) ? @"": rc.Phone1;
    rc.Twitter = StringFormat(@"%@",[dict objectForKey:@"Twitter"]);
    rc.Twitter = ([rc.Twitter isEqualToString: str]) ? @"": rc.Twitter;
    rc.RowNumber = StringFormat(@"%@",[dict objectForKey:@"RowNumber"]);
    rc.RowNumber = ([rc.RowNumber isEqualToString: str]) ? @"": rc.RowNumber;
    rc.TotalPage = StringFormat(@"%@",[dict objectForKey:@"TotalPage"]);
    rc.TotalPage = ([rc.TotalPage isEqualToString: str]) ? @"": rc.TotalPage;
    rc.TotalRow = StringFormat(@"%@",[dict objectForKey:@"TotalRow"]);
    rc.TotalRow = ([rc.TotalRow isEqualToString: str]) ? @"": rc.TotalRow;
    rc.CurrentPage = StringFormat(@"%@",[dict objectForKey:@"CurrentPage"]);
    rc.CurrentPage = ([rc.CurrentPage isEqualToString: str]) ? @"": rc.CurrentPage;
    
    rc.IPTVISPDesc = StringFormat(@"%@",[dict objectForKey:@"IPTVISPDesc"]);
    rc.IPTVISPDesc = ([rc.IPTVISPDesc isEqualToString: str]) ? @"": rc.IPTVISPDesc;
    rc.InternetISPDesc = StringFormat(@"%@",[dict objectForKey:@"InternetISPDesc"]);
    rc.InternetISPDesc = ([rc.InternetISPDesc isEqualToString: str]) ? @"": rc.InternetISPDesc;
    
    rc.acceptStatus = [dict[@"AcceptStatus"] integerValue];
    rc.remainder    = dict[@"Remainder"];
    rc.TotalSchedule = StringFormat(@"%@",[dict objectForKey:@"TotalSchedule"]);
    rc.TotalSchedule = ([rc.TotalSchedule isEqualToString: str]) ? @"": rc.TotalSchedule;
    
    rc.Supporter = StringFormat(@"%@",[dict objectForKey:@"Supporter"]);
    rc.Supporter = ([rc.Supporter isEqualToString:str])?@"":rc.Supporter;
    
    return rc;                            
}

-(PotentialCustomerDetailRecord *) ParseRecordDetail:(NSDictionary *)dict{
    PotentialCustomerDetailRecord *rc = [[PotentialCustomerDetailRecord alloc]init];
    NSString *str = @"(null)";
    
    rc.Address = StringFormat(@"%@",[dict objectForKey:@"Address"]);
    rc.Address = ([rc.Address isEqualToString: str]) ? @"": rc.Address ;
    
    rc.BillTo_City = StringFormat(@"%@",[dict objectForKey:@"BillTo_City"]);
    rc.BillTo_City = ([rc.BillTo_City isEqualToString: str]) ? @"": rc.BillTo_City ;
    
    rc.BillTo_District = StringFormat(@"%@",[dict objectForKey:@"BillTo_District"]);
    rc.BillTo_District = ([rc.BillTo_District isEqualToString: str]) ? @"": rc.BillTo_District ;
    
    rc.BillTo_Number = StringFormat(@"%@",[dict objectForKey:@"BillTo_Number"]);
    rc.BillTo_Number = ([rc.BillTo_Number isEqualToString: str]) ? @"": rc.BillTo_Number ;
    
    rc.BillTo_Street = StringFormat(@"%@",[dict objectForKey:@"BillTo_Street"]);
    rc.BillTo_Street = ([rc.BillTo_Street isEqualToString: str]) ? @"": rc.BillTo_Street ;
    
    rc.BillTo_Ward = StringFormat(@"%@",[dict objectForKey:@"BillTo_Ward"]);
    rc.BillTo_Ward = ([rc.Address isEqualToString: str]) ? @"": rc.BillTo_Ward ;
    
    rc.Contact_1 = StringFormat(@"%@",[dict objectForKey:@"Contact1"]);
    rc.Contact_1 = ([rc.Contact_1 isEqualToString: str]) ? @"": rc.Contact_1 ;
    
    rc.Contact_2 = StringFormat(@"%@",[dict objectForKey:@"Contact2"]);
    rc.Contact_2 = ([rc.Contact_2 isEqualToString: str]) ? @"": rc.Contact_2 ;
    
    rc.CreateBy = StringFormat(@"%@",[dict objectForKey:@"CreateBy"]);
    rc.CreateBy = ([rc.Address isEqualToString: str]) ? @"": rc.CreateBy ;
    
    rc.CreateDate = StringFormat(@"%@",[dict objectForKey:@"CreateDate"]);
    rc.CreateDate = ([rc.CreateDate isEqualToString: str]) ? @"": rc.CreateDate ;
    
    rc.CustomerType = StringFormat(@"%@",[dict objectForKey:@"CustomerType"]);
    rc.CustomerType = ([rc.CustomerType isEqualToString: str]) ? @"": rc.CustomerType ;
    
    
    rc.Fax = StringFormat(@"%@",[dict objectForKey:@"Fax"]);
    rc.Fax = ([rc.Fax isEqualToString:str])?@"":rc.Fax;
    
    rc.Email = StringFormat(@"%@",[dict objectForKey:@"Email"]);
    rc.Email = ([rc.Email isEqualToString: str]) ? @"": rc.Email ;
    
    rc.Facebook = StringFormat(@"%@",[dict objectForKey:@"Facebook"]);
    rc.Facebook = ([rc.Facebook isEqualToString: str]) ? @"": rc.Facebook;
    
    rc.Floor = StringFormat(@"%@",[dict objectForKey:@"Floor"]);
    rc.Floor = ([rc.Floor isEqualToString: str]) ? @"": rc.Floor;
    
    rc.FullName = StringFormat(@"%@",[dict objectForKey:@"FullName"]);
    rc.FullName = ([rc.FullName isEqualToString: str]) ? @"": rc.FullName ;
    
    rc.ID = StringFormat(@"%@",[dict objectForKey:@"ID"]);
    rc.ID = ([rc.ID isEqualToString: str]) ? @"": rc.ID ;
    
    rc.Latlng = StringFormat(@"%@",[dict objectForKey:@"Latlng"]);
    rc.Latlng = ([rc.Latlng isEqualToString: str]) ? @"": rc.Latlng ;
    
    rc.LocationID = StringFormat(@"%@",[dict objectForKey:@"LocationID"]);
    rc.LocationID = ([rc.LocationID isEqualToString: str]) ? @"": rc.LocationID ;
    
    rc.NameVilla = StringFormat(@"%@",[dict objectForKey:@"NameVilla"]);
    rc.NameVilla = ([rc.NameVilla isEqualToString: str]) ? @"": rc.NameVilla ;
    
    rc.Note = StringFormat(@"%@",[dict objectForKey:@"Note"]);
    rc.Note = ([rc.Note isEqualToString: str]) ? @"": rc.Note ;
    
    rc.Passport = StringFormat(@"%@",[dict objectForKey:@"Passport"]);
    rc.Passport = ([rc.Passport isEqualToString: str]) ? @"": rc.Passport ;
    
    rc.Phone_1 = StringFormat(@"%@",[dict objectForKey:@"Phone1"]);
    rc.Phone_1 = ([rc.Phone_1 isEqualToString: str]) ? @"": rc.Phone_1 ;
    
    rc.Phone_2 = StringFormat(@"%@",[dict objectForKey:@"Phone2"]);
    rc.Phone_2 = ([rc.Phone_2 isEqualToString: str]) ? @"": rc.Phone_2 ;
    
    rc.Position = StringFormat(@"%@",[dict objectForKey:@"Position"]);
    rc.Position = ([rc.Position isEqualToString: str]) ? @"": rc.Position ;
    
    rc.RegCode = StringFormat(@"%@",[dict objectForKey:@"RegCode"]);
    rc.RegCode = ([rc.RegCode isEqualToString: str]) ? @"": rc.RegCode ;
    
    rc.RegID = StringFormat(@"%@",[dict objectForKey:@"RegID"]);
    rc.RegID = ([rc.RegID isEqualToString: str]) ? @"": rc.RegID ;
    
    rc.Room = StringFormat(@"%@",[dict objectForKey:@"Room"]);
    rc.Room = ([rc.Room isEqualToString: str]) ? @"": rc.Room ;
    
    rc.Supporter = StringFormat(@"%@",[dict objectForKey:@"Supporter"]);
    rc.Supporter = ([rc.Supporter isEqualToString: str]) ? @"": rc.Supporter ;
    
    rc.TaxID = StringFormat(@"%@",[dict objectForKey:@"TaxID"]);
    rc.TaxID = ([rc.TaxID isEqualToString: str]) ? @"": rc.TaxID ;
    
    rc.TypeHouse = StringFormat(@"%@",[dict objectForKey:@"TypeHouse"]);
    rc.TypeHouse = ([rc.TypeHouse isEqualToString: str]) ? @"": rc.TypeHouse;
    
    rc.Twitter = StringFormat(@"%@",[dict objectForKey:@"Twitter"]);
    rc.Twitter = ([rc.Twitter isEqualToString: str]) ? @"": rc.Twitter;
    
    rc.Lot = StringFormat(@"%@",[dict objectForKey:@"Lot"]);
    rc.Lot = ([rc.Lot isEqualToString: str]) ? @"": rc.Lot;
    
    rc.ServiceType = StringFormat(@"%@",[dict objectForKey:@"ServiceType"]);
    rc.ServiceType = ([rc.ServiceType isEqualToString: str]) ? @"": rc.ServiceType;
    
    rc.ServiceTypeName = StringFormat(@"%@",[dict objectForKey:@"ServiceTypeName"]);
    rc.ServiceTypeName = ([rc.ServiceTypeName isEqualToString: str]) ? @"": rc.ServiceTypeName;
    
    rc.ISPType = StringFormat(@"%@",[dict objectForKey:@"ISPType"]);
    rc.ISPType = ([rc.ISPType isEqualToString: str]) ? @"": rc.ISPType;
    
    rc.ISPIPTV = StringFormat(@"%@",[dict objectForKey:@"ISPIPTV"]);
    rc.ISPIPTV = ([rc.ISPIPTV isEqualToString: str]) ? @"": rc.ISPIPTV;
    
    rc.ISPStartDate = StringFormat(@"%@",[dict objectForKey:@"ISPStartDate"]);
    rc.ISPStartDate = ([rc.Twitter isEqualToString: str]) ? @"": rc.ISPStartDate;
    
    rc.ISPEndDate = StringFormat(@"%@",[dict objectForKey:@"ISPEndDate"]);
    rc.ISPEndDate = ([rc.ISPEndDate isEqualToString: str]) ? @"": rc.ISPEndDate;
    
    rc.BillTo_City = (rc.BillTo_City == nil) ?@"":rc.BillTo_City;
    rc.BillTo_District = (rc.BillTo_District == nil) ?@"0":rc.BillTo_District;
    rc.BillTo_Number = (rc.BillTo_Number == nil) ?@"":rc.BillTo_Number;
    rc.BillTo_Street = (rc.BillTo_Street == nil) ?@"0":rc.BillTo_Street;
    rc.BillTo_Ward = (rc.BillTo_Ward == nil) ?@"0":rc.BillTo_Ward;
    rc.CustomerType = (rc.CustomerType == nil) ?@"0":rc.CustomerType;
    rc.ServiceType = (rc.ServiceType == nil) ?@"0":rc.ServiceType;
    rc.ISPType = (rc.ISPType == nil) ?@"0":rc.ISPType;
    
    rc.source = [dict[@"Source"] integerValue];
    rc.remainder = dict[@"Remainder"] ?:@"";
    rc.acceptStatus = [dict[@"AcceptStatus"] integerValue];
    rc.caseID = dict[@"CaseID"] ?:@"";
    rc.contract = dict[@"Contract"] ?:@"";
    
    return rc;
}

/*
 * Lấy danh sách lý do kết thúc tư vấn
 */

- (void)getPotentialAdvisoryResultValue:(NSDictionary *)dict completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetPotentialAdvisoryResultValue)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetPotentialAdvisoryResultValue:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endGetPotentialAdvisoryResultValue:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(dict == nil){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]?:nil);
    NSString *Error = StringFormat(@"%@",[dict objectForKey:@"Error"]?:nil);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, Error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        PotentialAdvisoryResultValueModel *p = [PotentialAdvisoryResultValueModel potentialAdvisoryResultValueModelWithDict:d];
        [mArray addObject:p];
    }
    
    handler(mArray, ErrorCode, @"");
    
}

/*
 * Lấy danh sách kết quả kết thúc tư vấn trả về
 */

- (void)getPotentialAdvisoryResultDetail:(NSDictionary *)dict completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetPotentialAdvisoryResultDetail)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetPotentialAdvisoryResultDetail:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endGetPotentialAdvisoryResultDetail:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(dict == nil){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]?:nil);
    NSString *Error = StringFormat(@"%@",[dict objectForKey:@"Error"]?:nil);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, Error);
        return;
    }
    
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        PotentialAdvisoryResultDetailModel *p = [PotentialAdvisoryResultDetailModel potentialAdvisoryResultDetailModelWithDict:d];
        [mArray addObject:p];
    }
    
    handler(mArray, ErrorCode, @"");
    
}

/*
 * Cập nhật kết quả tư vấn
 */

- (void)updatePotentialAdvisoryResult:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mUpdatePotentialAdvisoryResult)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endUpdatePotentialAdvisoryResult:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}

- (void)endUpdatePotentialAdvisoryResult:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    NSString *error = StringFormat(@"%@",[dict objectForKey:@"Error"]);
    NSString *errorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]);
    NSArray *arr = [dict objectForKey:@"ListObject"];
    
    if([error  integerValue] != 0 || arr.count <= 0){
        handler(nil, error, errorCode);
        return;
    }
    
    NSString *resultID  = [arr[0] objectForKey:@"ResultID"];
    NSString *resultStr = [arr[0] objectForKey:@"Result"];
    
    handler(arr, resultID, resultStr);
    
    return;
    
}

// Nhận Khách hàng tiềm năng
- (void)acceptPotential:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mAcceptPotential)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endAcceptPotential:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}

- (void)endAcceptPotential:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    NSString *error = StringFormat(@"%@",[dict objectForKey:@"Error"]);
    NSString *errorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]);
    NSArray *arr = [dict objectForKey:@"ListObject"];

    if([error  integerValue] != 0 || arr.count <= 0){
        handler(nil, error, errorCode);
        return;
    }
    
    NSString *resultID  = [arr[0] objectForKey:@"ResultID"];
    NSString *resultStr = [arr[0] objectForKey:@"Result"];
    
    handler(arr, resultID, resultStr);
    
    return;
    
}

// Nhận Khách hàng tiềm năng từ mã CEM
- (void)getPotentialByCode:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetPotentialByCode)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetPotentialByCode:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}

- (void)endGetPotentialByCode:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    NSString *error = StringFormat(@"%@",[dict objectForKey:@"Error"]);
    
    if(![error isEqualToString:@"<null>"]){
        handler(nil, error, error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    
    if(arr.count <= 0){
        return;
    }
    
    NSString *resultID  = [arr[0] objectForKey:@"ResultID"];
    NSString *resultStr = [arr[0] objectForKey:@"Result"];
    
    handler(arr, resultID, resultStr);
    
    return;
    
}

@end
