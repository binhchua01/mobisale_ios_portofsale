//
//  PotentialCustomerProxy.h
//  MobiSale
//
//  Created by ISC-DanTT on 11/17/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BaseProxy.h"

@interface PotentialCustomerProxy : BaseProxy
- (void) GetListPotentialCustomersWithInput:(NSDictionary *)dict completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void) updatePotentialCustomerInfo:(NSMutableDictionary*)info Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void) GetPotentialCustomerDetailWithID:(NSString*)ID Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void) getAdrressFromLatLong:(CGFloat)lat lon:(CGFloat)lon completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void) getPotentialObjSurveyList:(NSString*)userName potentialObjID:(NSString*)potentialObjID completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void) updatePotentialObjSurveyList:(NSString *)userName potentialObjID:(NSString *)potentialObjID surveys:(NSArray *)arrSurveys completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
/*
 * Lấy danh sách lý do kết thúc tư vấn
 */

- (void)getPotentialAdvisoryResultValue:(NSDictionary *)dict completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
/*
 * Lấy danh sách kết quả kết thúc tư vấn trả về
 */

- (void)getPotentialAdvisoryResultDetail:(NSDictionary *)dict completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
/*
 * Cập nhật kết quả tư vấn
 */

- (void)updatePotentialAdvisoryResult:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

// Nhận Khách hàng tiềm năng
- (void)acceptPotential:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

// Nhận Khách hàng tiềm năng từ mã CEM
- (void)getPotentialByCode:(NSDictionary *)dict completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Lấy danh sách lich hen KHTN
- (void)GetAllPotentialObjSchedule:(NSDictionary *)dict completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// TẠO MỚI , CẬP NHẬT LỊCH HẸN KHTN
- (void)UpdatePotentialObjSchedule:(NSDictionary *)dict completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

//DeletePotentialObjSchedule
- (void)DeletePotentialObjSchedule:(NSDictionary *)dict completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

@end
