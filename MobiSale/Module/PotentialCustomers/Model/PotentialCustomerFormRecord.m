//
//  PotentialCustomerFormRecord.m
//  MobiSale
//
//  Created by ISC-DanTT on 11/18/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "PotentialCustomerFormRecord.h"

@implementation PotentialCustomerFormRecord
@synthesize Address, CreateBy, CreateDate, Email, Facebook, FullName, ID, Latlng, LocationID, Note, Phone1, Twitter, TotalPage, TotalRow, CurrentPage, RowNumber, IPTVISPDesc, InternetISPDesc;

- (instancetype)initWithData:(NSDictionary *)dict {
    if (self = [super init]) {
        self.Address =  dict[@"Address"] ?:@"0";
        self.CreateBy =  dict[@"CreateBy"] ?:@"";
        self.CreateDate =  dict[@"CreateDate"] ?:@"";
        self.Email =  dict[@"Email"] ?:@"";
        self.Facebook =  dict[@"Facebook"] ?:@"";
        self.FullName =  dict[@"FullName"] ?:@"";
        self.ID =  dict[@"ID"] ?:@"";
        self.Latlng =  dict[@"Latlng"] ?:@"";
        self.LocationID =  dict[@"LocationID"] ?:@"";
        self.Note =  dict[@"Note"] ?:@"";
        self.Phone1 =  dict[@"Phone1"] ?:@"";
        self.Twitter =  dict[@"Twitter"] ?:@"";
        self.RowNumber =  dict[@"RowNumber"] ?:@"";
        self.TotalPage =  dict[@"TotalPage"] ?:@"";
        self.TotalRow =  dict[@"TotalRow"] ?:@"";
        self.CurrentPage =  dict[@"CurrentPage"] ?:@"";
        self.IPTVISPDesc =  dict[@"IPTVISPDesc"] ?:@"";
        self.InternetISPDesc =  dict[@"InternetISPDesc"] ?:@"";
        self.acceptStatus = [dict[@"AcceptStatus"] integerValue];
        self.remainder    = dict[@"Remainder"] ?:@"";
        self.TotalSchedule = dict[@"TotalSchedule"]?:@"";
        self.Supporter = dict[@"Supporter"]?:@"";
    }
    return self;
}
@end
