//
//  AnswerModel.m
//  MobiSale
//
//  Created by ISC-DanTT on 1/20/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "AnswerModel.h"

@implementation AnswerModel

+(id)answerModelWithDict:(NSDictionary *)dict {
    
    AnswerModel *answer = [[self alloc] init];
    answer.Selected = StringFormat(@"%@",dict[@"Selected"]);
    answer.SurveyValue = dict[@"SurveyValue"];
    answer.SurveyValueID = StringFormat(@"%@", dict[@"SurveyValueID"]);
    
    return answer;
}

@end
