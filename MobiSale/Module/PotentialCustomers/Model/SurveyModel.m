//
//  SurveyModel.m
//  MobiSale
//
//  Created by ISC-DanTT on 1/20/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "SurveyModel.h"
#import "AnswerModel.h"

@implementation SurveyModel

+(id)surveyModelWithDict:(NSDictionary *)dict {
    
    SurveyModel *surveyContent = [[self alloc] init];
    surveyContent.surveyID = StringFormat(@"%@",dict[@"SurveyID"]);
    surveyContent.surveyDescription = dict[@"SurveyDesc"];
    surveyContent.values = dict[@"Values"];
    surveyContent.type = [dict[@"Type"] intValue];
    NSMutableArray *arrAnswer = [NSMutableArray array];
    for (NSDictionary *dict in  surveyContent.values) {
        AnswerModel *answer = [[AnswerModel alloc] init];
        answer = [AnswerModel answerModelWithDict:dict];
        [arrAnswer addObject:answer];
    }
    surveyContent.arrAnswer = [NSArray arrayWithArray:arrAnswer];

    return surveyContent;
}

@end
