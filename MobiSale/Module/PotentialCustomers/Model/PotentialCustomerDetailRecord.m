//
//  PotentialCustomerDetailRecord.m
//  MobiSale
//
//  Created by ISC-DanTT on 11/19/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "PotentialCustomerDetailRecord.h"

@implementation PotentialCustomerDetailRecord
@synthesize Address, BillTo_City, BillTo_District, BillTo_Number, BillTo_Street, BillTo_Ward, BranchCode, Contact_1, Contact_2, CreateBy, CreateDate, CustomerType, Email, Facebook, Fax, Floor, FullName, ID, Latlng, LocationID, NameVilla, Note, Passport, Phone_1, Phone_2, Position, RegCode, RegID, Room, Supporter, TaxID, Twitter, TypeHouse, Lot, ServiceType, ServiceTypeName, ISPType, ISPIPTV, ISPStartDate, ISPEndDate, source, remainder, acceptStatus, caseID, contract,selectedNumberHouse;


static PotentialCustomerDetailRecord *sharedInstance = nil;

+(PotentialCustomerDetailRecord *)sharedInstance {
    @synchronized (self) {
        if (sharedInstance == nil) {
            sharedInstance = [[PotentialCustomerDetailRecord alloc] init];
        }
    }
    return sharedInstance;
}

- (instancetype)initWithData:(NSDictionary *)dict {
    if (self = [super init]) {
        self.Address =  dict[@"Address"] ?:@"";
        self.BillTo_City =  dict[@"BillTo_City"] ?:@"";
        self.BillTo_District =  dict[@"BillTo_District"] ?:@"";
        self.BillTo_Number =  dict[@"BillTo_Number"] ?:@"";
        self.BillTo_Street =  dict[@"BillTo_Street"] ?:@"";
        self.BillTo_Ward =  dict[@"BillTo_Ward"] ?:@"";
        self.Contact_1 =  dict[@"Contact1"] ?:@"";
        self.Contact_2 =  dict[@"Contact2"] ?:@"";
        self.CreateBy =  dict[@"CreateBy"] ?:@"";
        self.CreateDate =  dict[@"CreateDate"] ?:@"";
        self.CustomerType =  dict[@"CustomerType"] ?:@"";
        self.Email =  dict[@"Email"] ?:@"";
        self.Facebook =  dict[@"Facebook"] ?:@"";
        self.Floor =  dict[@"Floor"] ?:@"";
        self.FullName =  dict[@"FullName"] ?:@"";
        self.ID =  dict[@"ID"] ?:@"";
        self.Latlng =  dict[@"Latlng"] ?:@"";
        self.LocationID =  dict[@"LocationID"] ?:@"";
        self.NameVilla =  dict[@"NameVilla"] ?:@"";
        self.Note =  dict[@"Note"] ?:@"";
        self.Passport =  dict[@"Passport"] ?:@"";
        self.Phone_1 =  dict[@"Phone1"] ?:@"";
        self.Phone_2 =  dict[@"Phone2"] ?:@"";
        self.Position =  dict[@"Position"] ?:@"";
        self.RegCode =  dict[@"RegCode"] ?:@"";
        self.RegID =  dict[@"RegID"] ?:@"";
        self.Room =  dict[@"Room"] ?:@"";
        self.Supporter =  dict[@"Supporter"] ?:@"";
        self.TaxID =  dict[@"TaxID"] ?:@"";
        self.TypeHouse =  dict[@"TypeHouse"] ?:@"";
        self.Twitter =  dict[@"Twitter"] ?:@"";
        self.Lot =  dict[@"Lot"] ?:@"";
        self.ServiceType =  dict[@"ServiceType"] ?:@"";
        self.ServiceTypeName =  dict[@"ServiceTypeName"] ?:@"";
        self.ISPType =  dict[@"ISPType"] ?:@"";
        self.ISPIPTV =  dict[@"ISPIPTV"] ?:@"";
        self.ISPStartDate =  dict[@"ISPStartDate"] ?:@"";
        self.ISPEndDate =  dict[@"ISPEndDate"] ?:@"";
        self.source = [dict[@"Source"] integerValue];
        self.remainder = dict[@"Remainder"] ?:@"";
        self.acceptStatus = [dict[@"AcceptStatus"] integerValue];
        self.caseID = dict[@"CaseID"] ?:@"";
        self.contract = dict[@"Contract"] ?:@"";
        self.Fax = dict[@"Fax"]?:@"";
        
    
    }
    return self;
}


@end
