//
//  PotentialCustomerDetailRecord.h
//  MobiSale
//
//  Created by ISC-DanTT on 11/19/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KeyValueModel.h"

@interface PotentialCustomerDetailRecord : NSObject
@property (nonatomic,retain) NSString *Address;
@property (nonatomic,retain) NSString *BillTo_City;
@property (nonatomic,retain) NSString *BillTo_District;
@property (nonatomic,retain) NSString *BillTo_Number;
@property (nonatomic,retain) NSString *BillTo_Street;
@property (nonatomic,retain) NSString *BillTo_Ward;
@property (nonatomic,retain) NSString *BranchCode;
@property (nonatomic,retain) NSString *Contact_1;
@property (nonatomic,retain) NSString *Contact_2;
@property (nonatomic,retain) NSString *CreateBy;
@property (nonatomic,retain) NSString *CreateDate;
@property (nonatomic,retain) NSString *CustomerType;
@property (nonatomic,retain) NSString *Email;
@property (nonatomic,retain) NSString *Facebook;
@property (nonatomic,retain) NSString *Fax;
@property (nonatomic,retain) NSString *Floor;
@property (nonatomic,retain) NSString *FullName;
@property (nonatomic,retain) NSString *ID;
@property (nonatomic,retain) NSString *Latlng;
@property (nonatomic,retain) NSString *LocationID;
@property (nonatomic,retain) NSString *NameVilla;
@property (nonatomic,retain) NSString *Note;
@property (nonatomic,retain) NSString *Passport;
@property (nonatomic,retain) NSString *Phone_1;
@property (nonatomic,retain) NSString *Phone_2;
@property (nonatomic,retain) NSString *Position;
@property (nonatomic,retain) NSString *RegCode;
@property (nonatomic,retain) NSString *RegID;
@property (nonatomic,retain) NSString *Room;
@property (nonatomic,retain) NSString *Supporter;
@property (nonatomic,retain) NSString *TaxID;
@property (nonatomic,retain) NSString *Twitter;
@property (nonatomic,retain) NSString *TypeHouse;
@property (nonatomic,retain) NSString *Lot;
@property (nonatomic,retain) NSString *ServiceType;
@property (nonatomic,retain) NSString *ServiceTypeName; //new
@property (nonatomic,retain) NSString *ISPType;
@property (nonatomic,retain) NSString *ISPIPTV;// new
@property (nonatomic,retain) NSString *ISPStartDate;
@property (nonatomic,retain) NSString *ISPEndDate;
@property (nonatomic,retain) KeyValueModel *selectedNumberHouse;
//Nguồn dữ liệu:
// 0: Sale
// 1: Tổng đài"
@property (assign, nonatomic) NSInteger source;
@property (strong, nonatomic) NSString *remainder; //Thời gian còn lại
//Tình trạng accept:
//1: Đã accept;
//0: Chưa accept"
@property (assign, nonatomic) NSInteger acceptStatus;
@property (strong, nonatomic) NSString *caseID;
@property (strong, nonatomic) NSString *contract;

- (instancetype)initWithData:(NSDictionary *)dict;
+ (PotentialCustomerDetailRecord *)sharedInstance;

@end
