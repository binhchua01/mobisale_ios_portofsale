//
//  PotentialCustomerFormRecord.h
//  MobiSale
//
//  Created by ISC-DanTT on 11/18/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PotentialCustomerFormRecord : NSObject
@property (nonatomic,retain) NSString *Address;
@property (nonatomic,retain) NSString *CreateBy;
@property (nonatomic,retain) NSString *CreateDate;
@property (nonatomic,retain) NSString *Email;
@property (nonatomic,retain) NSString *Facebook;
@property (nonatomic,retain) NSString *FullName;
@property (nonatomic,retain) NSString *ID;
@property (nonatomic,retain) NSString *Latlng;
@property (nonatomic,retain) NSString *LocationID;
@property (nonatomic,retain) NSString *Note;
@property (nonatomic,retain) NSString *Phone1;
@property (nonatomic,retain) NSString *Twitter;
@property (nonatomic,retain) NSString *RowNumber;
@property (nonatomic,retain) NSString *TotalPage;
@property (nonatomic,retain) NSString *TotalRow;
@property (nonatomic,retain) NSString *CurrentPage;
@property (nonatomic,retain) NSString *IPTVISPDesc;
@property (nonatomic,retain) NSString *InternetISPDesc;
@property (nonatomic, assign) NSInteger acceptStatus;
@property (nonatomic, strong) NSString *remainder;
@property (nonatomic, strong) NSString *TotalSchedule;
@property (nonatomic, strong) NSString *Supporter;

- (instancetype)initWithData:(NSDictionary *)dict;

@end
