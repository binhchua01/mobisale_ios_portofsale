//
//  SurveyModel.h
//  MobiSale
//
//  Created by ISC-DanTT on 1/20/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    SurveyTypeSpinner = 1,
    SurveyTypeRadio,
    SurveyTypeCheckBox,
    SurveyTypeEditText,
    SurveyTypePickerDate,
    
} SurveyType;

@interface SurveyModel : NSObject


@property (nonatomic, copy) NSString *surveyID;
@property (nonatomic, copy) NSString *surveyDescription;
@property (nonatomic, copy) NSArray  *values;
@property (nonatomic, copy) NSArray *arrAnswer;
@property (nonatomic, assign) SurveyType type;

+ (id) surveyModelWithDict:(NSDictionary *)dict;

@end
