//
//  AnswerModel.h
//  MobiSale
//
//  Created by ISC-DanTT on 1/20/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AnswerModel : NSObject

@property (nonatomic, copy) NSString *Selected;
@property (nonatomic, copy) NSString *SurveyValue;
@property (nonatomic, copy) NSString *SurveyValueID;

+ (id) answerModelWithDict:(NSDictionary *)dict;

@end
