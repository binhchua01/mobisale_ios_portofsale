//
//  NIDropDownCustom.h
//  MobiSale
//
//  Created by ISC-DanTT on 1/25/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NIDropDownCustom;

@protocol NIDropDownCustomDelegate
- (void)niDropDownDelegateMethod: (NIDropDownCustom *) sender;
- (void)dropDownListView:(NIDropDownCustom *)dropDownListView
      didSelectIndexPath:(NSIndexPath *)indexPath atButton:(UIButton *)button;
@end

@interface NIDropDownCustom : UIView <UITableViewDelegate, UITableViewDataSource> {
    NSString *animationDirection;
    UIImageView *imgView;
}
@property(nonatomic, strong) UITableView *table;
@property(nonatomic, strong) UIButton *btnSender;
@property(nonatomic, retain) NSArray *list;
@property(nonatomic, retain) NSArray *imageList;
@property (nonatomic, retain) id <NIDropDownCustomDelegate> delegate;
@property (nonatomic, retain) NSString *animationDirection;
-(void)hideDropDown:(UIButton *)b;
- (id)showDropDownAtButton:(UIButton *)b height:(CGFloat *)height width:(CGFloat *)width data:(NSArray *)arr images:(NSArray *)imgArr animation:(NSString *)direction;
@end
