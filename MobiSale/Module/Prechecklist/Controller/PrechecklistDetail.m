//
//  PrechecklistDetail.m
//  MobiSale
//
//  Created by HIEUPC on 1/25/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "PrechecklistDetail.h"
#import "UIPopoverListView.h"
#import "KeyValueModel.h"
#import "ShareData.h"
#import "Common.h"
#import "Common_client.h"
@interface PrechecklistDetail ()

@end

@implementation PrechecklistDetail{
    CGPoint location;
    CGFloat height;
    KeyValueModel *selectedStatus, *selectedDepartment;
    UIAlertView *alert;
}
enum tableSourceStype {
    Status = 1,
    Department = 2,
};
@synthesize  arrStatus;
@synthesize arrDepartment;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"TẠO PRE-CHECKLIST";
    self.screenName=self.title;
    [self LoadStatus];
    //Khai bao srollview
    [self.scrollview setBackgroundColor:[UIColor clearColor]];
    [self.scrollview setCanCancelContentTouches:NO];
    self.scrollview.indicatorStyle = UIScrollViewIndicatorStyleWhite;
    [self.scrollview setScrollEnabled:YES];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnView:)];
    
    [self.view addGestureRecognizer:tap];
    [self registerForKeyboardNotifications];
    
    //khai bao delegate cho textbox
    self.txtPhone.delegate = self;
    self.txtNameContact.delegate = self;
    
    self.lblFullName.text = self.FullName;
    self.lblAddress.text = self.Address;
    self.lblContact.text = self.Contact;
    
    //Load Department
    self.arrDepartment =[Common getDepartments];
    selectedDepartment = [self.arrDepartment objectAtIndex:0];
    [self.btnDepartment setTitle:selectedDepartment.Values forState:UIControlStateNormal];
    
    self.tvNote.delegate = self;
    
    [self setType];
    
    
}

-(void)viewDidLayoutSubviews{
    if(IS_IPHONE4){
         [self.scrollview setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 150)];
    }else if(IS_IPHONE5){
         [self.scrollview setContentSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 50)];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.scrollview endEditing:YES];
    
    
}
- (void)tapOnView:(UITapGestureRecognizer *)sender
{
    [self.scrollview endEditing:YES];
    
}
-(void)handleSingleTap{
    //handle tap in here
    location = self.tvNote.frame.origin;
    height = self.btnUpdate.frame.size.height;
}

#pragma mark - How to make UITextView move up when keyboard is present
// Called when the UIKeyboardDidShowNotification is received
- (void)keyboardWasShown:(NSNotification *)notification {
    NSDictionary* info = [notification userInfo];
    CGPoint point = location;
    if(point.y == 0){
        point =self.btnUpdate.frame.origin;
        height = self.btnUpdate.frame.size.height;
    }
    /** STEP 1 **/
    // Get the size of the keyboard from the userInfo dictionary.
    CGSize keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    CGRect visibleRect = self.view.frame;
    
    visibleRect.size.height -= keyboardSize.height;
    
    CGPoint scrollPoint = CGPointMake(0.0, point.y - visibleRect.size.height + height);
    
    [self.scrollview setContentOffset:scrollPoint animated:YES];
    
    location.y = 0;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField{
    location = textField.frame.origin;
    height = textField.frame.size.height;
    
}

- (void)keyboardWillBeHidden:(NSNotification *)notification {
    
    // Move the scroll view back into its original position.
    CGPoint scrollPoint = CGPointMake(0, -64);
    [self.scrollview setContentOffset:scrollPoint animated:YES];
    return;
    
}
- (void)registerForKeyboardNotifications{
    
    // Add two notifications for the keyboard. One when the keyboard is shown and
    // one when it's about to hide.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}
- (void)deregisterFromKeyboardNotifications {
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidHideNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
}

#pragma mark - DropDown view

- (void)setupDropDownView: (NSInteger) tag
{
    
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.tag = tag;
    poplistview.listView.scrollEnabled = TRUE;
    [poplistview setTitle:NSLocalizedString(@"Chọn Tình Trạng", @"")];
    switch (tag) {
        case Status:
             [poplistview setTitle:NSLocalizedString(@"Chọn Tình Trạng", @"")];
            break;
        case Department:
            [poplistview setTitle:NSLocalizedString(@"Chọn Phòng Ban", @"")];
            break;
        default:
            break;
    }
    [poplistview show];
    
}
- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30.0f;
}

#pragma mark - UIPopoverListViewDataSource

- (void) setUpDropDownCell:(NSInteger)row tag:(NSInteger)tag cell:(UITableViewCell*)cell{
    KeyValueModel *model;
    switch (tag) {
        case Status:
            model = [self.arrStatus objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Department:
            model = [self.arrDepartment objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        default:
            break;
    }
    cell.textLabel.font = [UIFont fontWithName:@"Arial" size:14];
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row tag:popoverListView.tag cell:cell];
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0 ;
    switch (popoverListView.tag) {
        case Status:
            count = self.arrStatus.count;
            break;
        case Department:
            count = self.arrDepartment.count;
            break;
        default:
            break;
    }
    return count;
}

#pragma mark - UIPopoverListViewDelegate

- (void) didDropDownSelected:(NSInteger)row tag:(NSInteger)tag{
    
    switch (tag) {
        case Status:
            selectedStatus = [self.arrStatus objectAtIndex:row];
            [self.btnStatus setTitle:selectedStatus.Values forState:UIControlStateNormal];

            break;
        case Department:
            selectedDepartment = [self.arrDepartment objectAtIndex:row];
            [self.btnDepartment setTitle:selectedDepartment.Values forState:UIControlStateNormal];

            break;
        default:
            break;
    }
    
}


#pragma mark - UIPopoverListViewDelegate

- (void)popoverListView:(UIPopoverListView *)popoverListView
     didSelectIndexPath:(NSIndexPath *)indexPath
{
    [self didDropDownSelected:indexPath.row tag:popoverListView.tag];
    
}

#pragma mark - event click
-(IBAction)btnStatus_clicked:(id)sender
{
    [self setupDropDownView:Status];
    [self.view endEditing:YES];
}

-(IBAction)btnDepartment_clicked:(id)sender
{
    [self setupDropDownView:Department];
    [self.view endEditing:YES];
}

-(IBAction)btnUpdate_clicked:(id)sender
{
    if([self.txtNameContact.text isEqualToString:@""]){
        [self showAlertBox:@"Thông Báo" message:@"Chưa điền tên người liên hệ"];
        return;
    }
    if([self.txtPhone.text isEqualToString:@""]){
        [self showAlertBox:@"Thông Báo" message:@"Chưa điền số điện thoại"];
        return;
    }else {
        int len = (int)[self.txtPhone.text length];
        if(len < 10){
            [self showAlertBox:@"Thông Báo" message:@"Số điện thoại không hợp lệ"];
            return;
        }
    }
    if([selectedStatus.Key isEqualToString:@"0"] || selectedStatus.Key == nil){
        [self showAlertBox:@"Thông Báo" message:@"Chưa chọn trạng thái"];
        return;
    }
    if([selectedDepartment.Key isEqualToString:@"0"] || selectedDepartment.Key == nil){
        [self showAlertBox:@"Thông Báo" message:@"Chưa chọn phòng ban"];
        return;
    }
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    [self showMBProcess];
    ShareData *shared =[ShareData instance];
    
    //** Deleted Spacewhite  **//
    //  noteAddress = [noteAddress stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    NSCharacterSet *charset = [NSCharacterSet whitespaceCharacterSet];
    NSString *stringNote,*stringNameContact;
    stringNote = self.tvNote.text;
    stringNote = [stringNote stringByTrimmingCharactersInSet:charset];
    //stringNote = [stringNote stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    //stringNote = [stringNote stringByReplacingOccurrencesOfString:@"\'" withString:@""];
    //vutt11

   stringNameContact = [self.txtNameContact.text stringByTrimmingCharactersInSet:charset];
    //stringNameContact = [stringNameContact stringByReplacingOccurrencesOfString:@"\'" withString:@""];

    
    [shared.prechecklistProxy InsertPrechecklist:self.ObjId LocationName:stringNameContact LocationPhone:self.txtPhone.text FirstStatus:selectedStatus.Key Description:stringNote DivisionID:@"0" completeHandler:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if([errorCode intValue] <= 0){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            return ;
        }
        
        //[self showAlertBox:@"Thông Báo" message:@"Cập nhật thành công"];
        //Vutt11 fix UX close View end
        [self showAlertView:@"Cập nhật thành công"];
        
        
    } errorHandler:^(NSError *error) {
       [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
    
    
}

#pragma mark - LoadData
-(void)LoadStatus
{
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    [self showMBProcess];
    ShareData *shared = [ShareData instance];
    [shared.prechecklistProxy GetFirstStatus:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            return ;
        }
        self.arrStatus = result;
        selectedStatus = [self.arrStatus objectAtIndex:0];
        [self.btnStatus setTitle:selectedStatus.Values forState:UIControlStateNormal];
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
    }];
}

#pragma mark - set type button
-(void)setType{
    self.tvNote.layer.borderColor= [UIColor colorWithRed:0.133 green:0.698 blue:0.569 alpha:1].CGColor;
    self.tvNote.layer.borderWidth = 1.f;
    self.tvNote.layer.cornerRadius = 6.0f;
    
    
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void) showAlertView:(NSString*) message {
    
    alert = [[UIAlertView alloc] initWithTitle:@"Thông Báo" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    
    
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
            [self.navigationController popViewControllerAnimated:YES];
            break;
            
        default:
            break;
    }
}


#pragma mark - UITextView delegate whilespace \n

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if ([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
    }
    
    return YES;
}


@end
