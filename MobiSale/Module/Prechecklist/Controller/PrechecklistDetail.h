//
//  PrechecklistDetail.h
//  MobiSale
//
//  Created by HIEUPC on 1/25/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BaseViewController.h"

@interface PrechecklistDetail : BaseViewController<UITextFieldDelegate,UIAlertViewDelegate,UITextViewDelegate>

@property (nonatomic , retain) IBOutlet UIScrollView *scrollview;

@property (nonatomic , retain) IBOutlet UITextField *txtNameContact;
@property (nonatomic , retain) IBOutlet UITextField *txtPhone;
@property (nonatomic , retain) IBOutlet UITextView *tvNote;


@property (nonatomic , retain) IBOutlet UILabel *lblContact;
@property (nonatomic , retain) IBOutlet UILabel *lblFullName;
@property (nonatomic , retain) IBOutlet UILabel *lblAddress;

@property (nonatomic , retain) IBOutlet UIButton *btnStatus;
@property (nonatomic , retain) IBOutlet UIButton *btnUpdate;
@property (nonatomic , retain) IBOutlet UIButton *btnDepartment;

-(IBAction)btnStatus_clicked:(id)sender;
-(IBAction)btnUpdate_clicked:(id)sender;
-(IBAction)btnDepartment_clicked:(id)sender;

@property (nonatomic , retain) NSString *Contact;
@property (nonatomic , retain) NSString *FullName;
@property (nonatomic , retain) NSString *Address;
@property (nonatomic , retain) NSString *ObjId;
@property (nonatomic , retain) NSMutableArray *arrStatus;
@property (nonatomic , retain) NSMutableArray *arrDepartment;

@end
