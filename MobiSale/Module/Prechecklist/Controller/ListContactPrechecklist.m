//
//  ListContactPrechecklist.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 1/26/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ListContactPrechecklist.h"
#import "ListContactPrechecklistCell.h"
#import "PrechecklistDetail.h"
#import "ContactPrechecklistRecord.h"
#import "ShareData.h"
#import "KeyValueModel.h"
#import "UIPopoverListView.h"
#import "Common.h"
#import "Common_client.h"
#import "DemoTableFooterView.h"

@interface ListContactPrechecklist (){
    NSMutableArray *arrData;
    NSInteger totalPage;
    NSInteger currentPage;
    DemoTableFooterView *footerView;
}

@property NSMutableArray *arrList;

@end

@implementation ListContactPrechecklist{
    KeyValueModel *selectedType, *selectedPage;
    bool flagPage;
    //NSString *totalPage;
}

@dynamic tableView;
@synthesize arrType;
@synthesize arrPage;

enum tableSourceStype {
    TypeSearch = 1,
    Page = 2,
};

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"TẠO PRE-CHECKLIST";
    
    [self.tableView setHidden:YES];
    arrData = [[NSMutableArray alloc]init];
    // set the custom view for "load more". See DemoTableFooterView.xib.
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DemoTableFooterView" owner:self options:nil];
    footerView = (DemoTableFooterView *)[nib objectAtIndex:0];
    //Load type search
    self.arrType = [Common getTypeSearch:@"1"];
    selectedType = [self.arrType objectAtIndex:0];
    
    [self.btnType setTitle:selectedType.Values forState:UIControlStateNormal];
    self.tableView.separatorColor = [UIColor clearColor];
    
    flagPage = NO;
    
    self.txtTextSearch.delegate = self;
    [self.txtTextSearch becomeFirstResponder];

    self.screenName=self.title;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

#pragma mark - TableView


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(arrData.count > 0 )
        return arrData.count;
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 164;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"ListContactPrechecklistCell";
    ListContactPrechecklistCell *cell = (ListContactPrechecklistCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ListContactPrechecklistCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    if(arrData.count > 0){
        ContactPrechecklistRecord *rc = [arrData objectAtIndex:indexPath.row];
        cell.lblSTT.text = rc.RowNumber;
        cell.lblFullName.text = rc.FullName;
        cell.lblAdress.text = rc.Address;
        cell.lblContact.text = rc.Contract;
    }
    return cell;
    
}
#pragma mark - Table DidSelected
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *nibName = @"PrechecklistDetail";
    if(IS_IPHONE4){
        nibName = @"PrechecklistDetail_iphone4";
    }
    ContactPrechecklistRecord *list = [arrData objectAtIndex:indexPath.row];
    PrechecklistDetail *vc = [[PrechecklistDetail alloc]initWithNibName:nibName bundle:nil];
    vc.FullName = list.FullName;
    vc.Contact = list.Contract;
    vc.Address = list.Address;
    vc.ObjId = list.ObjID;
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark - Scroll view delegate
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    if (maximumOffset - currentOffset < 0) {
        [self loadMore];
    }
}

#pragma mark - DropDown view

- (void)setupDropDownView:(NSInteger) tag {
    
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.tag = tag;
    poplistview.listView.scrollEnabled = TRUE;
    switch (tag) {
        case TypeSearch:
            [poplistview setTitle:NSLocalizedString(@"Chọn loại tìm kiếm", @"")];
            break;
        case Page:
            [poplistview setTitle:NSLocalizedString(@"Chọn trang", @"")];
            break;
        default:
            break;
    }
    
    [poplistview show];
    
}
- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 35.0f;
}

#pragma mark - UIPopoverListViewDataSource

- (void) setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell tag:(NSInteger)tag{
    KeyValueModel *model;
    switch (tag) {
        case TypeSearch:
            model = [self.arrType objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Page:
            model = [self.arrPage objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        default:
            break;
    }
    
    
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row cell:cell tag:popoverListView.tag];
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section {
    NSInteger count = 0;
    switch (popoverListView.tag) {
        case TypeSearch:
            count = self.arrType.count;
            break;
        case Page:
            count = self.arrPage.count;
            break;
        default:
            break;
    }
    return count;
}

#pragma mark - UIPopoverListViewDelegate

- (void)didDropDownSelected:(NSInteger)row tag:(NSInteger)tag{
    
    switch (tag) {
        case TypeSearch:
            selectedType = [self.arrType objectAtIndex:row];
            [self.btnType setTitle:selectedType.Values forState:UIControlStateNormal];
            [self.txtTextSearch becomeFirstResponder];
            self.txtTextSearch.text = @"";
            break;
        case Page:
            selectedPage = [self.arrPage objectAtIndex:row];
            [self LoadData:selectedPage.Key];
            [self.btnPage setTitle:[NSString stringWithFormat:@"%@/%ld",selectedPage.Values,(long)totalPage] forState:UIControlStateNormal];
            break;
        default:
            break;
    }
    
    
}


#pragma mark - UIPopoverListViewDelegate

- (void)popoverListView:(UIPopoverListView *)popoverListView
     didSelectIndexPath:(NSIndexPath *)indexPath {
    [self didDropDownSelected:indexPath.row tag:popoverListView.tag];
    
}

#pragma mark - Search Delegate
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [arrData removeAllObjects];
    [self.view endEditing:YES];
    [self LoadData:@"1"];
}
#pragma mark - event button clicked
-(IBAction)btnFind_clicked:(id)sender{
    [arrData removeAllObjects];
    [self.view endEditing:YES];
    [self LoadData:@"1"];
}
-(IBAction)btnType_clicked:(id)sender{
    [self.view endEditing:YES];
    [self setupDropDownView:TypeSearch];
}

-(IBAction)btnPage_clicked:(id)sender{
    [self.view endEditing:YES];
    [self setupDropDownView:Page];
}

#pragma mark - LoadData
-(void)LoadData:(NSString *)pagenum {
    if([self.txtTextSearch.text isEqualToString:@""]){
        [self showAlertBox:@"Thông Báo" message:@"Chưa điền thông tin tìm kiếm"];
        return;
    }else {
        if([self returnSpecialType:self.txtTextSearch.text]){
            [self showAlertBox:@"Thông Báo" message:@"Không điền các ký tự đặt biệt"];
            return;
        }
        
    }
    
    [self showMBProcess];
    [self.tableView setHidden:NO];
    ShareData *shared = [ShareData instance];
    [shared.prechecklistProxy SearchObject:selectedType.Key SearchText:self.txtTextSearch.text PageNum:pagenum Completehander:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            return ;
        }
        if(result == nil){
            [self ShowAlertBoxEmpty];
            [self.tableView setHidden:YES];
        }else {
            self.arrList = result;
//            [self.tableView reloadData];
//            ContactPrechecklistRecord *rc = [self.arrList objectAtIndex:0];
//            if(flagPage == NO){
//                [self LoadPage: [rc.TotalPage intValue]];
//                flagPage = YES;
//            }
            [arrData addObjectsFromArray:self.arrList];
            ContactPrechecklistRecord *rc = [self.arrList objectAtIndex:0];
            totalPage = [rc.TotalPage integerValue];
            currentPage = [rc.CurrentPage integerValue];
            [self.tableView reloadData];
            [self hideMBProcess];
            [self loadMoreCompleted];
        }
        
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self.tableView setHidden:YES];
        [self hideMBProcess];
    }];
}


#pragma mark - Load More
//
// The method -loadMore was called and will begin fetching data for the next page (more).
// Do custom handling of -footerView if you need to.
//
- (void) willBeginLoadingMore {
    [footerView.activityIndicator startAnimating];
}

// Do UI handling after the "load more" process was completed. In this example, -footerView will
// show a "No more items to load" text.
//
- (void)loadMoreCompleted {
    [footerView.activityIndicator stopAnimating];
}

- (void)loadMore {
    // Do your async loading here
    self.tableView.tableFooterView = footerView;
    currentPage ++;
    if (currentPage <= totalPage) {
        [self willBeginLoadingMore];
        [self LoadData:StringFormat(@"%li",(long)currentPage)];
        return;
    }
    footerView.infoLabel.hidden = NO;
}

#pragma mark - Load Page
-(void)LoadPage:(int)page {
    totalPage =page;
    if (page <2) {
       // [self.btnPage setEnabled:NO];
        [self.btnPage setTitle:@"1" forState:UIControlStateNormal];
        return;
    }
    [self.btnPage setEnabled:YES];
    self.arrPage = [NSMutableArray array];
    int i = 1;
    for (i = 1; i <= page; i++) {
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",i] description:[NSString stringWithFormat:@"%d",i]];
        [self.arrPage addObject: s];
    }
    
    selectedPage = [self.arrPage objectAtIndex:0];
    NSString*title =[NSString stringWithFormat:@"%@/%ld",selectedPage.Values,(long)totalPage];
    [self.btnPage setTitle:title forState:UIControlStateNormal];
}


@end
