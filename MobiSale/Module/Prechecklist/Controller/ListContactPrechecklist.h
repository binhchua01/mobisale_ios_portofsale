//
//  ListContactPrechecklist.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 1/26/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BaseViewController.h"

@interface ListContactPrechecklist : BaseViewController<UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate, UIScrollViewDelegate>

@property (nonatomic , strong) IBOutlet UITableView *tableView;

@property (nonatomic , retain) IBOutlet UISearchBar *txtTextSearch;

@property (nonatomic , retain) IBOutlet UIButton *btnFind;
@property (nonatomic , retain) IBOutlet UIButton *btnType;
@property (nonatomic , retain) IBOutlet UIButton *btnPage;

@property (nonatomic , retain) NSMutableArray *arrType;
@property (nonatomic , retain) NSMutableArray *arrPage;

-(IBAction)btnFind_clicked:(id)sender;
-(IBAction)btnType_clicked:(id)sender;
-(IBAction)btnPage_clicked:(id)sender;
@end
