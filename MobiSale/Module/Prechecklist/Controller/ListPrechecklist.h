//
//  ListPrechecklist.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 1/25/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BaseViewController.h"

@interface ListPrechecklist : BaseViewController<UITableViewDelegate,UITableViewDataSource, UIScrollViewDelegate>

@property (nonatomic , retain) IBOutlet UITableView *tableView;

@property (nonatomic , retain) IBOutlet UIButton *btnPage;

@property (nonatomic , retain) NSMutableArray *arrPage;

-(IBAction)btnPage_clicked:(id)sender;

@end
