//
//  ListPrechecklist.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 1/25/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ListPrechecklist.h"
#import "PrechecklistCell.h"
#import "PrechecklistDetail.h"
#import "ShareData.h"
#import "ListPrechecklistRecord.h"
#import "KeyValueModel.h"
#import "UIPopoverListView.h"
#import "Common_client.h"
#import "DemoTableFooterView.h"

@interface ListPrechecklist (){
    NSMutableArray *arrData;
    NSInteger totalPage;
    NSInteger currentPage;
    DemoTableFooterView *footerView;
}

@property NSMutableArray *arrList;

@end

@implementation ListPrechecklist{
    KeyValueModel *selectedPage;
    //NSString *totalPage;
}
@dynamic tableView;

@synthesize arrPage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"DANH SÁCH PRE-CHECKLIST";
    self.screenName=self.title;
    arrData = [[NSMutableArray alloc]init];
    // set the custom view for "load more". See DemoTableFooterView.xib.
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DemoTableFooterView" owner:self options:nil];
    footerView = (DemoTableFooterView *)[nib objectAtIndex:0];
}

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    //load data
    [arrData removeAllObjects];
    footerView.infoLabel.hidden = YES;
    [self LoadData:@"1"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section  {
    if(arrData.count > 0 )
        return arrData.count;
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 310;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"PrechecklistCell";
    PrechecklistCell *cell = (PrechecklistCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PrechecklistCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    if(arrData.count > 0){
        ListPrechecklistRecord *rc = [arrData objectAtIndex:indexPath.row];
        //        if(indexPath.row == 0){
        //            [self LoadPage: [rc.TotalPage intValue]];
        //        }
        cell.lblSTT.text = rc.RowNumber;
        cell.lblContact.text = rc.Contract;
        cell.lblCreateDate.text = rc.CreateDate;
        cell.lblFullName.text = rc.FullName;
        cell.lblNote.text = rc.Description;
        cell.lblDateProcess.text = rc.UpdateDate;
        cell.lblPerSon.text = rc.UpdateBy;
        cell.tvContent.text = rc.SupDescription;
        //        cell.lblView.layer.borderColor= [UIColor colorWithRed:0.00 green:0.18 blue:0.31 alpha:0.6].CGColor;
        //        cell.lblView.layer.borderWidth = 1.f;
        //        cell.lblView.layer.cornerRadius = 6.0f;
        cell.tvContent.layer.borderColor= [UIColor colorWithRed:0.00 green:0.18 blue:0.31 alpha:0.6].CGColor;
        cell.tvContent.layer.borderWidth = 1.f;
        cell.tvContent.layer.cornerRadius = 6.0f;
    }
    
    return cell;
    
}
#pragma mark Table DidSelected
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    return;
}

#pragma mark - Scroll view delegate
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    if (maximumOffset - currentOffset < 0) {
        [self loadMore];
    }
}

#pragma mark - DropDown view

- (void)setupDropDownView {
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.listView.scrollEnabled = TRUE;
    [poplistview setTitle:NSLocalizedString(@"Chọn trang", @"")];
    [poplistview show];
    
}

- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.0f;
}

#pragma mark - UIPopoverListViewDataSource

- (void) setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell{
    KeyValueModel *model;
    model = [self.arrPage objectAtIndex:row];
    cell.textLabel.text = model.Values;
    
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row cell:cell];
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section {
    NSInteger count = self.arrPage.count;
    return count;
}

#pragma mark - UIPopoverListViewDelegate

- (void) didDropDownSelected:(NSInteger)row {
    selectedPage = [self.arrPage objectAtIndex:row];
    [self LoadData:selectedPage.Values];
    [self.btnPage setTitle:[NSString stringWithFormat:@"%@/%ld",selectedPage.Key,(long)totalPage] forState:UIControlStateNormal];
    
}


#pragma mark - UIPopoverListViewDelegate

- (void)popoverListView:(UIPopoverListView *)popoverListView
     didSelectIndexPath:(NSIndexPath *)indexPath {
    [self didDropDownSelected:indexPath.row];
    
}


#pragma mark - event button clicked
-(void)LoadData:(NSString *)pagenum {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    [self showMBProcess];
    ShareData *shared = [ShareData instance];
    [shared.prechecklistProxy GetListPrechecklist:pagenum Completehander:^(id result, NSString *errorCode, NSString *message) {
        
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            return ;
        }
        if(result == nil){
            [self ShowAlertBoxEmpty];
            [self.tableView setHidden:YES];
            return;
        }
        self.arrList  = result;
        [arrData addObjectsFromArray:self.arrList];
        ListPrechecklistRecord *rc = [self.arrList objectAtIndex:0];
        totalPage = [rc.TotalPage integerValue];
        currentPage = [rc.CurrentPage integerValue];
        [self.tableView reloadData];
        [self loadMoreCompleted];
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}

-(IBAction)btnPage_clicked:(id)sender {
    [self setupDropDownView];
}


#pragma mark - Load More
//
// The method -loadMore was called and will begin fetching data for the next page (more).
// Do custom handling of -footerView if you need to.
//
- (void) willBeginLoadingMore {
    [footerView.activityIndicator startAnimating];
}

// Do UI handling after the "load more" process was completed. In this example, -footerView will
// show a "No more items to load" text.
//
- (void)loadMoreCompleted {
    [footerView.activityIndicator stopAnimating];
}

- (void)loadMore {
    // Do your async loading here
    self.tableView.tableFooterView = footerView;
    currentPage ++;
    if (currentPage <= totalPage) {
        [self willBeginLoadingMore];
        [self LoadData:StringFormat(@"%li",(long)currentPage)];
        return;
    }
    footerView.infoLabel.hidden = NO;
}

#pragma mark - Load Page
-(void)LoadPage:(int) page {
    totalPage =page;
    if (page <2) {
        //  [self.btnPage setEnabled:NO];
        [self.btnPage setTitle:@"1" forState:UIControlStateNormal];
        return;
    }
    [self.btnPage setEnabled:YES];
    self.arrPage = [NSMutableArray array];
    int i = 1;
    for (i = 1; i <= page; i++) {
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",i] description:[NSString stringWithFormat:@"%d",i]];
        [self.arrPage addObject: s];
    }
    
    selectedPage = [self.arrPage objectAtIndex:0];
    NSString*title =[NSString stringWithFormat:@"%@/%d",selectedPage.Values,page];
    [self.btnPage setTitle:title forState:UIControlStateNormal];
}
@end
