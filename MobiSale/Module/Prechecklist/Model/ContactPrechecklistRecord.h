//
//  ContactPrechecklistRecord.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 1/28/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContactPrechecklistRecord : NSObject

@property (nonatomic,retain) NSString *Contract;
@property (nonatomic,retain) NSString *Address;
@property (nonatomic,retain) NSString *CurrentPage;
@property (nonatomic,retain) NSString *FullName;
@property (nonatomic,retain) NSString *ObjID;
@property (nonatomic,retain) NSString *RowNumber;
@property (nonatomic,retain) NSString *TotalPage;
@property (nonatomic,retain) NSString *TotalRow;

@end
