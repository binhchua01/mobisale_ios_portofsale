//
//  ContactPrechecklistRecord.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 1/28/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ContactPrechecklistRecord.h"

@implementation ContactPrechecklistRecord

@synthesize Contract;
@synthesize Address;
@synthesize CurrentPage;
@synthesize FullName;
@synthesize ObjID;
@synthesize RowNumber;
@synthesize TotalPage;

@end
