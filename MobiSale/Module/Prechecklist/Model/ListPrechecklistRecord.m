//
//  ListPrechecklistRecord.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 1/28/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ListPrechecklistRecord.h"

@implementation ListPrechecklistRecord

@synthesize Contract;
@synthesize CreateDate;
@synthesize CurrentPage;
@synthesize Description;
@synthesize ErrorService;
@synthesize FullName;
@synthesize RowNumber;
@synthesize SupDescription;
@synthesize TotalPage;
@synthesize TotalRow;
@synthesize UpdateBy;
@synthesize UpdateDate;

@end
