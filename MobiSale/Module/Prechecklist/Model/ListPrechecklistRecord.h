//
//  ListPrechecklistRecord.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 1/28/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ListPrechecklistRecord : NSObject

@property (nonatomic,retain) NSString *Contract;
@property (nonatomic,retain) NSString *CreateDate;
@property (nonatomic,retain) NSString *CurrentPage;
@property (nonatomic,retain) NSString *Description;
@property (nonatomic,retain) NSString *ErrorService;
@property (nonatomic,retain) NSString *FullName;
@property (nonatomic,retain) NSString *RowNumber;
@property (nonatomic,retain) NSString *SupDescription;
@property (nonatomic,retain) NSString *TotalPage;
@property (nonatomic,retain) NSString *TotalRow;
@property (nonatomic,retain) NSString *UpdateBy;
@property (nonatomic,retain) NSString *UpdateDate;

@end
