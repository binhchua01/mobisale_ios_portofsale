//
//  PrechecklistProxy.h
//  MobiSale
//
//  Created by HIEUPC on 1/28/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseProxy.h"

@interface PrechecklistProxy : BaseProxy

- (void)GetListPrechecklist:(NSString*)PageNum Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler ;

- (void)SearchObject:(NSString*)type SearchText:(NSString *)searchText PageNum:(NSString  *)pagenum Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)GetFirstStatus:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)InsertPrechecklist:(NSString *)ObjId LocationName:(NSString *)locationName LocationPhone:(NSString *)locationPhone FirstStatus:(NSString *)firstStatus Description:(NSString *)description DivisionID:(NSString *)divisionID
           completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
@end
