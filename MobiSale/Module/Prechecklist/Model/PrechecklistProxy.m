//
//  PrechecklistProxy.m
//  MobiSale
//
//  Created by HIEUPC on 1/28/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "PrechecklistProxy.h"
#import "ShareData.h"
#import "ListPrechecklistRecord.h"
#import "ContactPrechecklistRecord.h"
#import "KeyValueModel.h"
#import "FPTUtility.h"

#define mGetPrecheckList                @"GetPrecheckList"
#define mSearchObject                   @"SearchObject"
#define mGetFirstStatus                 @"GetFirstStatus"
#define mInsertPrecheckList             @"InsertPrecheckList"

@implementation PrechecklistProxy

#pragma mark - Get List Prechecklist
- (void)GetListPrechecklist:(NSString*)PageNum Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetPrecheckList)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    NSString *username = [ShareData instance].currentUser.userName;
    NSString *pagenum = PageNum;
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:username forKey:@"UserName"];
    [dict setObject:pagenum forKey:@"PageNumber"];
                                 
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetListPrechecklist:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
    
}
- (void)endGetListPrechecklist:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }

    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSArray *arr = [jsonDict objectForKey:@"GetPrecheckListMethodPostResult"]?:nil;
    
    if(arr.count <= 0){
        handler(nil, @"<null>", @"");
        return;
    }
    
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    
    if(![ErrorService isEqualToString:@"<null>"]){
        //NSString *message = StringFormat(@"%@",[root objectForKey:@"Message"]); ;
        handler(nil, ErrorService, ErrorService);
        return;
    }
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        ListPrechecklistRecord *p = [self ParseRecordList:d];
        [mArray addObject:p];
    }
    handler(mArray, ErrorService, @"");
    
    
}

-(ListPrechecklistRecord *) ParseRecordList:(NSDictionary *)dict{
    ListPrechecklistRecord *rc = [[ListPrechecklistRecord alloc]init];
    
    
    rc.Contract = StringFormat(@"%@",[dict objectForKey:@"Contract"]);
    rc.CreateDate = StringFormat(@"%@",[dict objectForKey:@"CreateDate"]);
    rc.CurrentPage = StringFormat(@"%@",[dict objectForKey:@"CurrentPage"]);
    rc.Description = StringFormat(@"%@",[dict objectForKey:@"Description"]);
    rc.FullName = StringFormat(@"%@",[dict objectForKey:@"FullName"]);
    rc.RowNumber = StringFormat(@"%@",[dict objectForKey:@"RowNumber"]);
    rc.TotalPage = StringFormat(@"%@",[dict objectForKey:@"TotalPage"]);
    rc.TotalRow = StringFormat(@"%@",[dict objectForKey:@"TotalRow"]);
    rc.SupDescription = StringFormat(@"%@",[dict objectForKey:@"SupDescription"]);
    rc.UpdateBy = StringFormat(@"%@",[dict objectForKey:@"UpdateBy"]);
    rc.UpdateDate = StringFormat(@"%@",[dict objectForKey:@"UpdateDate"]);
    
    return rc;
}

#pragma mark - Search List contact Prechecklist
- (void)SearchObject:(NSString*)type SearchText:(NSString *)searchText PageNum:(NSString  *)pagenum Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mSearchObject)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *username = [ShareData instance].currentUser.userName;
    if ([type isEqualToString:@"3"]) {
        searchText = [searchText stringByReplacingOccurrencesOfString:@" " withString:@""];
    }
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:type forKey:@"Agent"];
    [dict setObject:searchText forKey:@"AgentName"];
    [dict setObject:pagenum forKey:@"PageNumber"];
    [dict setObject:username forKey:@"UserName"];
    
    postString = [dict JSONRepresentation];
    
    NSString * strUTF8 = @"";
    NSMutableString *s = [NSMutableString stringWithString:postString];
    [s replaceOccurrencesOfString:@"\\" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    strUTF8 = [NSString stringWithString:s];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:strUTF8];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];

    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endSearchObject:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
    
}
- (void)endSearchObject:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }

    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSArray *arr = [jsonDict objectForKey:@"SearchObjectMethodPostResult"]?:nil;
    
    if(arr.count <= 0){
        handler(nil, @"<null>", Mesage_DataEmpty);
        return;
    }
    
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    
    if(![ErrorService isEqualToString:@"<null>"]){
        //NSString *message = StringFormat(@"%@",[root objectForKey:@"Message"]); ;
        handler(nil, ErrorService, ErrorService);
        return;
    }
 
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        ContactPrechecklistRecord *p = [self ParseContactRecordList:d];
        [mArray addObject:p];
    }
    handler(mArray, ErrorService, @"");
    
    
}

-(ContactPrechecklistRecord *) ParseContactRecordList:(NSDictionary *)dict{
    ContactPrechecklistRecord *rc = [[ContactPrechecklistRecord alloc]init];
    
    
    rc.Contract = StringFormat(@"%@",[dict objectForKey:@"Contract"]);
    rc.CurrentPage = StringFormat(@"%@",[dict objectForKey:@"CurrentPage"]);
    rc.FullName = StringFormat(@"%@",[dict objectForKey:@"FullName"]);
    rc.RowNumber = StringFormat(@"%@",[dict objectForKey:@"RowNumber"]);
    rc.TotalPage = StringFormat(@"%@",[dict objectForKey:@"TotalPage"]);
    rc.TotalRow = StringFormat(@"%@",[dict objectForKey:@"TotalRow"]);
    rc.ObjID = StringFormat(@"%@",[dict objectForKey:@"ObjID"]);
    rc.Address = StringFormat(@"%@",[dict objectForKey:@"Address"]);
    
    return rc;
}

#pragma mark - Search List status
- (void)GetFirstStatus:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler
{
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetFirstStatus)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = @"";
    
    NSString * strUTF8 = @"";
    NSMutableString *s = [NSMutableString stringWithString:postString];
    [s replaceOccurrencesOfString:@"\\" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    strUTF8 = [NSString stringWithString:s];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:strUTF8];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];

    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetFirstStatus:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
    
}
- (void)endGetFirstStatus:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }

    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    
    NSArray *arr = [jsonDict objectForKey:@"GetFirstStatusMethodPostResult"]?:nil;
    
    if(arr.count <= 0){
        return;
    }
    
    NSString *ErrorService = StringFormat(@"%@",[[arr objectAtIndex:0] objectForKey:@"ErrorService"]);
    
    
    if(![ErrorService isEqualToString:@"<null>"]){
        handler(nil, ErrorService, ErrorService);
        return;
    }
 
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        NSString *FirstStatus = StringFormat(@"%@",[d objectForKey:@"FirstStatus"]);
        NSString *ID = StringFormat(@"%@",[d objectForKey:@"ID"]);
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:ID description:FirstStatus];
        
        [mArray addObject:s];
    }
    handler(mArray, ErrorService, @"");
    
    
}

#pragma mark - Create Prechecklist
- (void)InsertPrechecklist:(NSString *)ObjId LocationName:(NSString *)locationName LocationPhone:(NSString *)locationPhone FirstStatus:(NSString *)firstStatus Description:(NSString *)description DivisionID:(NSString *)divisionID
           completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mInsertPrecheckList)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    //create json
    //NSString *deviceKey = [OpenUDID value];358239055770376
    NSString *username = [ShareData instance].currentUser.userName;
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    NSNumber *objid = [f numberFromString:ObjId];
    NSNumber *firststatus = [f numberFromString:firstStatus];
    NSNumber *division = [f numberFromString:divisionID];
    
    [dict setObject:username forKey:@"UserName"];
    [dict setObject:objid forKey:@"ObjID"];
    [dict setObject:firststatus forKey:@"FirstStatus"];
    [dict setObject:division forKey:@"DivisionID"];
    [dict setObject:locationName forKey:@"Location_Name"];
    [dict setObject:locationPhone forKey:@"Location_Phone"];
    [dict setObject:description forKey:@"Description"];
    
    NSString *postString = [dict JSONRepresentation];
    
    //vutt11 fix error "\\" into dict
    NSString * strUTF8 = @"";
    NSMutableString *s = [NSMutableString stringWithString:postString];
    [s replaceOccurrencesOfString:@"\\" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, [s length])];
    strUTF8 = [NSString stringWithString:s];
    
    
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];

    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:strUTF8];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endInsertPrechecklist:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endInsertPrechecklist:(NSData *)result response:(NSURLResponse *)response
            completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }

    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSArray *root = [jsonDict objectForKey:@"InsertPreCheckListResult"]?:nil;
    
    if(root.count <= 0) {
        handler(nil, @"0", @"");
        return;
    }
    
    NSString *ErrorService = StringFormat(@"%@",[[root objectAtIndex:0] objectForKey:@"ResultID"]);
    
    if([ErrorService intValue] <= 0){
         NSString *message = StringFormat(@"%@",[[root objectAtIndex:0] objectForKey:@"Result"]);
        handler(nil, ErrorService, message);
        return;
    }

    /*Login success*/
    
    handler(nil, ErrorService, @"");
}

@end
