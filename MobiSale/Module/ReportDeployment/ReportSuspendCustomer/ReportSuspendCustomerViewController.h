//
//  ReportSuspendCustomerViewController.h
//  MobiSale
//
//  Created by ISC on 9/19/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "NIDropDown.h"

@interface ReportSuspendCustomerViewController : BaseViewController <NIDropDownDelegate, UIPickerViewDelegate, UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *lblResultNumber; // Số kết quả tìm kiếm được

@property (weak, nonatomic) IBOutlet UIView *searchView;       // view điều kiện tìm kiếm

@property (weak, nonatomic) IBOutlet UITableView *resultTableView; // table hiển thị kết quả tìm kiếm

@property (weak, nonatomic) IBOutlet UIImageView *imgUpDown;  // image icon up/down on condition search button

@property (weak, nonatomic) IBOutlet UIButton *btnFromDate;  // button chọn ngày

@property (weak, nonatomic) IBOutlet UIButton *btnAgent;    // button chọn tìm theo

@property (weak, nonatomic) IBOutlet UITextField *txtAgentName; // text nhập nội dung cần tìm 

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchViewHeight;

@property (nonatomic, copy) NSString     *month;
@property (nonatomic, copy) NSString     *year;

@end
