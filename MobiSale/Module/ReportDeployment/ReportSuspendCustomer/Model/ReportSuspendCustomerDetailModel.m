//
//  ReportSuspendCustomerDetailModel.m
//  MobiSale
//
//  Created by ISC on 9/19/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ReportSuspendCustomerDetailModel.h"

@implementation ReportSuspendCustomerDetailModel

- (id)initWithDictionary:(NSDictionary *)dict {
    if (self = [super init]) {
        self.amount = [dict[@"Amount"] integerValue];
        self.contract = dict[@"Contract"] ;
        self.currentPage = [dict[@"CurrentPage"] integerValue];
        self.date = dict[@"Date"] ;
        self.fullname = dict[@"Fullname"] ;
        self.localTypeName = dict[@"LocalTypeName"] ;
        self.location_Phone = dict[@"Location_Phone"] ;
        self.rowNumber = [dict[@"RowNumber"] integerValue];
        self.sale = dict[@"Sale"] ;
        self.saleName = dict[@"SaleName"] ;
        self.totalPage = [dict[@"TotalPage"] integerValue];
        self.totalRow = [dict[@"TotalRow"] integerValue];
    }
    
    return self;
}

+ (id)sharedInstance {
    static ReportSuspendCustomerDetailModel *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ReportSuspendCustomerDetailModel alloc] init];
        // Do any other initialisation stuff here
    });
    
    return sharedInstance;
}

@end
