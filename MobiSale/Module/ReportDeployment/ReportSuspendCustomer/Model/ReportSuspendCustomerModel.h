//
//  ReportSuspendCustomerModel.h
//  MobiSale
//
//  Created by ISC on 9/19/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportSuspendCustomerModel : NSObject

@property (strong, nonatomic) NSString* saleName; //Tên đăng nhập
@property (assign, nonatomic) NSInteger currentPage; //Trang hiện tại
@property (assign, nonatomic) NSInteger rowNumber; //Số thứ tự dòng hiện tại
@property (assign, nonatomic) NSInteger totalRow; //tổng số dòng
@property (assign, nonatomic) NSInteger totalPage; //Tổng số trang
@property (assign, nonatomic) NSInteger total; //Tổng số HĐ theo saleman
@property (assign, nonatomic) NSInteger sumTotal; //Tổng tất cả HĐ

- (id)initWithDictionary:(NSDictionary *)dict;
+ (id)sharedInstance;

@end
