//
//  ReportSuspendCustomerDetailModel.h
//  MobiSale
//
//  Created by ISC on 9/19/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportSuspendCustomerDetailModel : NSObject

@property (assign, nonatomic) NSInteger amount; //Số tiền
@property (strong, nonatomic) NSString* contract; //Số hợp đồng
@property (assign, nonatomic) NSInteger currentPage; //Trang hiện tại
@property (strong, nonatomic) NSString* date; //Ngày tạo hợp đồng
@property (strong, nonatomic) NSString* fullname; //Họ tên KH
@property (strong, nonatomic) NSString* localTypeName; //Gói dịch vụ
@property (strong, nonatomic) NSString* location_Phone; //Số điện thoại
@property (assign, nonatomic) NSInteger rowNumber; //STT
@property (strong, nonatomic) NSString* sale; //Account sale
@property (strong, nonatomic) NSString* saleName; //Họ tên Sale
@property (assign, nonatomic) NSInteger totalPage; //Tổng trang
@property (assign, nonatomic) NSInteger totalRow; //Tổng dòng

- (id)initWithDictionary:(NSDictionary *)dict;
+ (id)sharedInstance;

@end
