//
//  ReportSuspendCustomerModel.m
//  MobiSale
//
//  Created by ISC on 9/19/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ReportSuspendCustomerModel.h"

@implementation ReportSuspendCustomerModel

- (id)initWithDictionary:(NSDictionary *)dict {
    if (self = [super init]) {
        self.saleName = dict[@"SaleName"] ;
        self.currentPage = [dict[@"CurrentPage"] integerValue];
        self.rowNumber = [dict[@"RowNumber"] integerValue];
        self.totalRow = [dict[@"TotalRow"] integerValue];
        self.totalPage = [dict[@"TotalPage"] integerValue];
        self.total = [dict[@"Total"] integerValue];
        self.sumTotal = [dict[@"SumTotal"] integerValue];
    }
    
    return self;
}

+ (id)sharedInstance {
    static ReportSuspendCustomerModel *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[ReportSuspendCustomerModel alloc] init];
        // Do any other initialisation stuff here
    });
    
    return sharedInstance;
}

@end
