//
//  ReportSuspendCustomerDetailViewController.m
//  MobiSale
//
//  Created by ISC on 9/19/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ReportSuspendCustomerDetailViewController.h"
#import "ReportSuspendCustomerDetailModel.h"
#import "ShareData.h"
#import "DemoTableFooterView.h"
#import "ReportSuspendCustomerDetailTableViewCell.h"

@interface ReportSuspendCustomerDetailViewController ()

@end

@implementation ReportSuspendCustomerDetailViewController
{
    ReportSuspendCustomerDetailModel *record;
    DemoTableFooterView *tableFooterView; // show view load more when scroll table to bottom
    
    NSMutableArray *arrData;
    NSInteger           totalPage;
    NSInteger           currentPage;
    CGFloat             startOffset;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    // set screen name on google anaticz
    self.screenName = @"B/C KH RỜI MẠNG CHI TIẾT";
    // set offset scroll view of table
    startOffset = -20;
    // Init data
    arrData = [NSMutableArray array];
    /*
     *set the custom view for "load more". See DemoTableFooterView.xib.
     */
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DemoTableFooterView" owner:self options:nil];
    tableFooterView = (DemoTableFooterView *)[nib objectAtIndex:0];
    // set navigation title
    self.navigationItem.titleView = [self settitleOfNavigationBar:self.saleName subTitle:StringFormat(@"(%@ hợp đồng)", self.numberContract)];
    
    // add refresh control when scroll top of tableview
    [self.resultTableView addSubview:[self setRefreshControlWithTitle:@"Kéo thả để tải lại dữ liệu..."]];
    // get data
    
    [self getReportSuspendCutomerDetailWithPageNumber:@"1"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


// Init data for view
+(id)initWithNibName:(NSString *)nibName andData:(NSDictionary *)dict {
    ReportSuspendCustomerDetailViewController *vc = [[ReportSuspendCustomerDetailViewController alloc] initWithNibName:nibName bundle:nil];
    vc.saleName = StringFormat(@"%@",dict[@"SaleName"]);
    vc.month    = StringFormat(@"%@",dict[@"Month"]);
    vc.year     = StringFormat(@"%@",dict[@"Year"]);
    vc.numberContract  = StringFormat(@"%@",dict[@"NumberContract"]);
    
    return vc;
}

- (UIRefreshControl *)setRefreshControlWithTitle:(NSString *)title {
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    
    refreshControl.tintColor = [UIColor colorMain];
    
    NSMutableAttributedString *titleAttString =  [[NSMutableAttributedString alloc] initWithString:title];
    
    [titleAttString addAttribute:NSFontAttributeName value:[UIFont fontWithName:@"Helvetica" size:14.0f] range:NSMakeRange(0, [title length])];
    
    [titleAttString addAttribute:NSForegroundColorAttributeName value:[UIColor colorMain] range:NSMakeRange(0, [title length])];
    
    refreshControl.attributedTitle = titleAttString;
    
    return refreshControl;
}

- (UIView*)settitleOfNavigationBar:(NSString*)title subTitle:(NSString*)subTitle {
    UIColor *textColor = [UIColor colorMain];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = textColor;
    titleLabel.font = [UIFont boldSystemFontOfSize:18];
    titleLabel.text = title;
    [titleLabel sizeToFit];
    
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 19, 0, 0)];
    subTitleLabel.backgroundColor = [UIColor clearColor];
    subTitleLabel.textColor = textColor;
    subTitleLabel.font = [UIFont systemFontOfSize:13];
    subTitleLabel.text = subTitle;
    [subTitleLabel sizeToFit];
    
    UIView *twoLineTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAX(subTitleLabel.frame.size.width, titleLabel.frame.size.width), 30)];
    [twoLineTitleView addSubview:titleLabel];
    [twoLineTitleView addSubview:subTitleLabel];
    
    float widthDiff = subTitleLabel.frame.size.width - titleLabel.frame.size.width;
    
    if (widthDiff > 0) {
        CGRect frame = titleLabel.frame;
        frame.origin.x = widthDiff / 2;
        titleLabel.frame = CGRectIntegral(frame);
    }else{
        CGRect frame = subTitleLabel.frame;
        frame.origin.x = fabsf(widthDiff) / 2;
        subTitleLabel.frame = CGRectIntegral(frame);
    }
    
    return twoLineTitleView;
}

#pragma refresh table view
- (void)refresh:(UIRefreshControl *)refreshControl {
    [arrData removeAllObjects];
    // get data
    [self getReportSuspendCutomerDetailWithPageNumber:@"1"];
    [refreshControl endRefreshing];
}

#pragma mark - UITableView datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return arrData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 183;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reuseIdentifier = @"ReportSuspendCustomerDetailCellID";
    ReportSuspendCustomerDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if(!cell) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ReportSuspendCustomerDetailTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    if (arrData.count > 0) {
        record = arrData[indexPath.section];
        cell.cellModel = record;
    }
    
    return cell;
}

#pragma mark - Scroll in table view delegate
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    CGFloat currentOffset = scrollView.contentOffset.y;
    
    startOffset = currentOffset;
    
    CGFloat maximumOffset = fabs(scrollView.contentSize.height - scrollView.frame.size.height);
    if (maximumOffset - currentOffset < 0) {
        [self loadMore];
    }
}

#pragma mark - Load More
//
// The method -loadMore was called and will begin fetching data for the next page (more).
// Do custom handling of -footerView if you need to.
//
- (void) willBeginLoadingMore {
    [tableFooterView.activityIndicator startAnimating];
}

// Do UI handling after the "load more" process was completed. In this example, -footerView will
// show a "No more items to load" text.
//
- (void)loadMoreCompleted {
    [tableFooterView.activityIndicator stopAnimating];
}

- (void)loadMore {
    // Do your async loading here
    self.resultTableView.tableFooterView = tableFooterView;
    currentPage ++;
    if (currentPage <= totalPage) {
        [self willBeginLoadingMore];
        [self getReportSuspendCutomerDetailWithPageNumber:StringFormat(@"%li",(long)currentPage)];
        return;
    }
    tableFooterView.infoLabel.hidden = NO;
}
#pragma mark - get report Pay TV detail with package of sale
- (void)getReportSuspendCutomerDetailWithPageNumber:(NSString *)pageNumber {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:self.month forKey:@"Month"];
    [dict setObject:self.year forKey:@"Year"];
    [dict setObject:self.saleName forKey:@"UserName"];
    [dict setObject:pageNumber forKey:@"PageNumber"];
    
    [self showMBProcess];
    
    [shared.reportlistdeploymentproxy getReportSuspendCustomerDetail:dict completeionHander:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        
        NSArray *arr = result;
        [arrData addObjectsFromArray:arr];
        
        if(![errorCode isEqualToString:@"0"] || result == nil || arrData.count <= 0) {
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",@"Không có kết quả trả về"]];
            
        } else {
            record      = [arr objectAtIndex:0];
            
            currentPage = record.currentPage;
            totalPage   = record.totalPage;
        }
        
        self.resultTableView.hidden = NO;
        [self.resultTableView reloadData];
        
        [self loadMoreCompleted];
        
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
        [self hideMBProcess];
    }];
    
}

@end
