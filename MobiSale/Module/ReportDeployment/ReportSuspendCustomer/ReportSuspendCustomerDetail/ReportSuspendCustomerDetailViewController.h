//
//  ReportSuspendCustomerDetailViewController.h
//  MobiSale
//
//  Created by ISC on 9/19/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface ReportSuspendCustomerDetailViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UITableView *resultTableView;

@property (nonatomic, copy) NSString *saleName;
@property (nonatomic, copy) NSString *month;
@property (nonatomic, copy) NSString *year;
@property (nonatomic, copy) NSString *numberContract;

+(id)initWithNibName:(NSString *)nibName andData:(NSDictionary *)dict;

@end
