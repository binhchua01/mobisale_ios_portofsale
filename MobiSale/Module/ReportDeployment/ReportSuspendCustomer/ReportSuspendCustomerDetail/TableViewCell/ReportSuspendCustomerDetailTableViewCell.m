//
//  ReportSuspendCustomerDetailTableViewCell.m
//  MobiSale
//
//  Created by ISC on 9/19/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ReportSuspendCustomerDetailTableViewCell.h"

@implementation ReportSuspendCustomerDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setCellModel:(ReportSuspendCustomerDetailModel *)cellModel {
    _cellModel = cellModel;
    
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    
    self.amountLabel.text = StringFormat(@"%@ VNĐ",[nf stringFromNumber:[NSNumber numberWithFloat:cellModel.amount]]); //Số tiền
    self.contractLabel.text = cellModel.contract; //Số hợp đồng
    self.dateLabel.text = cellModel.date; //Ngày tạo hợp đồng
    self.fullnameLabel.text = cellModel.fullname; //Họ tên KH
    self.localTypeNameLabel.text = cellModel.localTypeName; //Gói dịch vụ
    self.location_PhoneLabel.text = cellModel.location_Phone; //Số điện thoại
    self.saleLabel.text = StringFormat(@"%@ (%@)", cellModel.saleName, cellModel.sale); //Họ tên Sale
}
@end
