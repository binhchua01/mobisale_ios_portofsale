//
//  ReportSuspendCustomerDetailTableViewCell.h
//  MobiSale
//
//  Created by ISC on 9/19/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReportSuspendCustomerDetailModel.h"

@interface ReportSuspendCustomerDetailTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *amountLabel; //Số tiền
@property (weak, nonatomic) IBOutlet UILabel *contractLabel; //Số hợp đồng
@property (weak, nonatomic) IBOutlet UILabel *dateLabel; //Ngày tạo hợp đồng
@property (weak, nonatomic) IBOutlet UILabel *fullnameLabel; //Họ tên KH
@property (weak, nonatomic) IBOutlet UILabel *localTypeNameLabel; //Gói dịch vụ
@property (weak, nonatomic) IBOutlet UILabel *location_PhoneLabel; //Số điện thoại
@property (weak, nonatomic) IBOutlet UILabel *saleLabel; //Họ tên Sale

@property (strong, nonatomic) ReportSuspendCustomerDetailModel *cellModel;

@end
