//
//  ReportSuspendCustomerViewController.m
//  MobiSale
//
//  Created by ISC on 9/19/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ReportSuspendCustomerViewController.h"
#import "NIDropDown.h"
#import "ShareData.h"
#import "DemoTableFooterView.h"
#import "ReportSuspendCustomerModel.h"
#import "ItemsModel.h"
#import "ReportSuspendCustomerDetailViewController.h"

@implementation ReportSuspendCustomerViewController
{
    NIDropDown          *dropDownView; // drop down view show when clicked button
    DemoTableFooterView *tableFooterView; // show view load more when scroll table to bottom
    NSMutableArray      *arrSearchResult;    // data received from server
    NSInteger           totalPage;
    NSInteger           currentPage;
    CGFloat             startOffset;
    BOOL                isSearchViewShow; // show/hide search conditions
    
    ReportSuspendCustomerModel   *record;
    
    NSArray         *arrAgent;
    NSDictionary    *selectedAgent;

    UIButton        *btnSelected;
    
    UIPickerView   *datePickerView;
    NSMutableArray *sourceYear;
    NSMutableArray *sourceMonth;
    NSString       *currentYear;
    NSString       *currentMonth;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    // set navigation title
    self.title = @"B/C KH RỜI MẠNG";
    // set screen name on google anaticz
    self.screenName = self.title;
    
    [self setType];
    
    [self loadAgent];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self handleSingleTap:nil];
}

#pragma mark - set type view
- (void)setType {
    // show conditions search view
    isSearchViewShow = YES;
    // set empty result number
    self.lblResultNumber.text = @"";
    // set offset scroll view of table
    startOffset = -20;
    
    // set format date for datePicker
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"1/M/yyyy"];
    [self.btnFromDate setTitle:[dateFormatter stringFromDate:[NSDate date]] forState:UIControlStateNormal];
    // get only year defult
    [dateFormatter setDateFormat:@"yyyy"];
    currentYear  = self.year  = [dateFormatter stringFromDate:[NSDate date]];
    // get only month defult
    [dateFormatter setDateFormat:@"M"];
    currentMonth = self.month = [dateFormatter stringFromDate:[NSDate date]];
    
    
    // Init data
    arrSearchResult = [NSMutableArray array];
    /*
     * set border for conditions search view
     */
    self.searchView.layer.borderColor  = [UIColor colorWithRed:0.133 green:0.698 blue:0.569 alpha:1].CGColor;
    self.searchView.layer.borderWidth  = 1.0f;
    self.searchView.layer.cornerRadius = 4.0f;
    self.resultTableView.hidden = YES;
    /*
     *set the custom view for "load more". See DemoTableFooterView.xib.
     */
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DemoTableFooterView" owner:self options:nil];
    tableFooterView = (DemoTableFooterView *)[nib objectAtIndex:0];

    // set data source for picker view
    NSString *lastYear = StringFormat(@"%li",[currentYear integerValue] - 1);
    sourceYear = [[NSMutableArray alloc] initWithObjects:lastYear, self.year, nil];
    sourceMonth = [[NSMutableArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12", nil];
}

#pragma mark - load list agent
- (void)loadAgent {
    arrAgent = [NSArray arrayWithObjects:[NSDictionary dictionaryWithObject:@"Tất cả" forKey:@"0"],[NSDictionary dictionaryWithObject:@"Tên nhân viên" forKey:@"1"], nil];
    selectedAgent = [arrAgent objectAtIndex:0];
    [self.btnAgent setTitle:[[selectedAgent allValues] objectAtIndex:0] forState:UIControlStateNormal];
}

#pragma mark - show or hide conditions search view
- (void)showConditionsSearchView:(BOOL)status {
    isSearchViewShow = status;
    self.searchView.hidden = !status;
    
    /*
     * show conditions search View
     */
    if (status) {
        self.searchViewHeight.constant = 148;
        self.imgUpDown.image = [UIImage imageNamed:@"up-icon-512"];
        
    } else {
        /*
         * hide conditions search View
         */
        self.searchViewHeight.constant = 0;
        self.imgUpDown.image = [UIImage imageNamed:@"down-icon-512"];
    }
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

#pragma mark - NIDropDown method
- (void)showDropDownAtButton:(UIButton *)sender withArrayData:(NSArray*)arrData{
    [self.view endEditing:YES];
    
    if (dropDownView == nil) {
        int i = 0;
        NSMutableArray *arrOutput = [[NSMutableArray alloc] init];
        for (i = 0; i < arrData.count; i++) {
            NSDictionary *dict = [arrData objectAtIndex:i];
            [arrOutput addObjectsFromArray:[dict allValues]];
        }
        
        CGFloat height = 40*arrData.count;
        if (arrData.count > 3) {
            height = 120;
        }
        CGFloat width = 0;
        dropDownView = [[NIDropDown alloc] showDropDownAtButton:sender height:&height width:&width data:arrOutput images:nil animation:@"down"];
        dropDownView.delegate = self;
        
        return;
    }
    [dropDownView hideDropDown:sender];
    [self rel];
}

- (void)niDropDownDelegateMethod:(NIDropDown *)sender {
    [self rel];
}

- (void)rel {
    dropDownView = nil;
}

- (void)dropDownListView:(NIDropDown *)dropDownListView didSelectIndexPath:(NSIndexPath *)indexPath atButton:(UIButton *)button{
    [self didDropDownSelected:indexPath.row atButton:button];
}

- (void)didDropDownSelected:(NSInteger)row atButton:(UIButton *)button {
    if (button == self.btnAgent) {
        selectedAgent = [arrAgent objectAtIndex:row];
        return;
    }
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
    if (dropDownView !=nil) {
        [dropDownView hideDropDown:self.btnAgent];
        [self rel];
    }
}

#pragma mark - UITableView datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return arrSearchResult.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 49;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reuseIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    if (arrSearchResult.count > 0) {
        record = arrSearchResult[indexPath.section];
        
        
        cell.textLabel.text =record.saleName;
        cell.detailTextLabel.text = StringFormat(@"Tổng số hợp đồng: %li", (long)record.total);
        
        cell.textLabel.textColor = [UIColor colorWithRed:0.133 green:0.698 blue:0.569 alpha:1];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:14.0f];

    }
    
    return cell;
}

#pragma mark - UITableView delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    record = arrSearchResult[indexPath.section];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:record.saleName forKey:@"SaleName"];
    [dict setObject:self.month forKey:@"Month"];
    [dict setObject:self.year forKey:@"Year"];
    [dict setObject:StringFormat(@"%li", (long)record.total) forKey:@"NumberContract"];
    ReportSuspendCustomerDetailViewController *vc = [ReportSuspendCustomerDetailViewController initWithNibName:@"ReportSuspendCustomerDetailViewController" andData:dict];
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Scroll in table view delegate
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    CGFloat currentOffset = scrollView.contentOffset.y;
    
    if (currentOffset > startOffset) {
        [self showConditionsSearchView:NO];
    }
    startOffset = currentOffset;
    
    CGFloat maximumOffset = fabs(scrollView.contentSize.height - scrollView.frame.size.height);
    if (maximumOffset - currentOffset < 0) {
        [self loadMore];
    }
}

#pragma mark - UIPickerViewDataSource methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(component == 0)
        return [sourceMonth count];
    
    return [sourceYear count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    if(component == 0)
        return StringFormat(@"Tháng %@",[sourceMonth objectAtIndex:row]);

    return [sourceYear objectAtIndex:row];
    
}


#pragma mark - UIPickerViewDelegate methods
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    switch (component) {
        case 0:
            NSLog(@"--- month: %@", [sourceMonth objectAtIndex:row]);
            self.month = [sourceMonth objectAtIndex:row];
            // set max of month <= current month
            if ([self.year isEqualToString:currentYear]) {
                NSInteger differentMonth = [self.month integerValue] - [currentMonth integerValue];
                if (differentMonth > 0) {
                    self.month = currentMonth;
                    [pickerView selectRow:labs(row - differentMonth) inComponent:0 animated:YES];
                }
            }
            break;
            
        case 1:
            NSLog(@"--- year: %@", [sourceYear objectAtIndex:row]);
            self.year = [sourceYear objectAtIndex:row];
            // set max of month <= current month
            if ([self.year isEqualToString:currentYear]) {
                self.month = currentMonth;
                [pickerView selectRow:5 inComponent:0 animated:YES];
            }
            break;
            
        default:
            break;
    }
    
    [self.btnFromDate setTitle:StringFormat(@"1/%@/%@", self.month, self.year) forState:UIControlStateNormal];

}

- (void)showPickerView {
    
    CGRect pickerViewFrame = CGRectMake(0,0, self.view.frame.size.width, 200);
    datePickerView = [[UIPickerView alloc] initWithFrame:pickerViewFrame];
    
    datePickerView.delegate = self;
    datePickerView.dataSource = self;
    
    UIView *viewDatePicker = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width+30, 200)];
    [viewDatePicker setBackgroundColor:[UIColor clearColor]];
    
    
    [viewDatePicker addSubview:datePickerView];
    
    for (int i = 0; i < sourceMonth.count; i++) {
        if ([self.month isEqualToString:sourceMonth[i]]) {
            [datePickerView selectRow:i inComponent:0 animated:YES];
            break;
        }
    }

    for (int i = 0; i < sourceYear.count; i++) {
        if ([self.year isEqualToString:sourceYear[i]]) {
            [datePickerView selectRow:i inComponent:1 animated:YES];
            break;
        }
    }

    
    if(IS_OS_8_OR_LATER) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"\n\n\n\n\n\n\n\n\n\n" preferredStyle:UIAlertControllerStyleActionSheet];
 
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Đóng" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            
        }];
        
        [alertController addAction:cancelAction];
        [alertController.view addSubview:viewDatePicker];
        
        [self.navigationController presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"\n\n\n\n\n\n\n\n\n\n" delegate:self cancelButtonTitle:@"Đóng" destructiveButtonTitle:nil otherButtonTitles:nil, nil];
    [actionSheet addSubview:viewDatePicker];
    actionSheet.tag = 3;
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
    return;
}

#pragma mark - Load More
//
// The method -loadMore was called and will begin fetching data for the next page (more).
// Do custom handling of -footerView if you need to.
//
- (void) willBeginLoadingMore {
    [tableFooterView.activityIndicator startAnimating];
}

// Do UI handling after the "load more" process was completed. In this example, -footerView will
// show a "No more items to load" text.
//
- (void)loadMoreCompleted {
    [tableFooterView.activityIndicator stopAnimating];
}

- (void)loadMore {
    // Do your async loading here
    self.resultTableView.tableFooterView = tableFooterView;
    currentPage ++;
    if (currentPage <= totalPage) {
        [self willBeginLoadingMore];
        [self getReportSuspendCustomerWithPageNumber:StringFormat(@"%li",(long)currentPage)];
        return;
    }
    tableFooterView.infoLabel.hidden = NO;
}

#pragma mark - get search results
- (void)getReportSuspendCustomerWithPageNumber:(NSString *)pageNumber {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:self.month forKey:@"Month"];
    [dict setObject:self.year  forKey:@"Year"];
    [dict setObject:[[selectedAgent allKeys] objectAtIndex:0] forKey:@"Agent"];
    [dict setObject:self.txtAgentName.text forKey:@"AgentName"];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];  //@"HCM.HuongTD" for test 
    [dict setObject:pageNumber forKey:@"PageNumber"];
    
    [self showMBProcess];
    
    [shared.reportlistdeploymentproxy getReportSuspendCustomer:dict completeionHander:^(id result, NSString *errorCode, NSString *message) {
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        
        NSArray *arr = result;
        [arrSearchResult addObjectsFromArray:arr];
        
        if(![errorCode isEqualToString:@"0"] || result == nil || arrSearchResult.count <= 0) {
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",@"Không có kết quả trả về"]];
            
        } else {
            record      = [arr objectAtIndex:0];
            
            currentPage = record.currentPage;
            totalPage   = record.totalPage;
        }

        self.lblResultNumber.text = StringFormat(@"Tổng số: %li", (long)[self getResultNumber:arrSearchResult]);
        
        self.resultTableView.hidden = NO;
        [self.resultTableView reloadData];
        
        [self showConditionsSearchView:NO];
        
        [self loadMoreCompleted];
        
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
        [self hideMBProcess];
    }];
    
}

- (NSInteger)getResultNumber:(NSArray *)arrData {
    NSInteger sum = 0;
    for (ReportSuspendCustomerModel *rc in arrData) {
        sum += rc.total;
    }
    return sum;
}

#pragma mark - actions button click
// action clicked button From date
- (IBAction)btnFromDate_Clicked:(id)sender
{
    if (dropDownView != nil && btnSelected != sender) {
        [dropDownView hideDropDown:btnSelected];
        dropDownView = nil;
    }
    btnSelected = sender;

    [self showPickerView];
}

// action clicked button search with
- (IBAction)btnAgent_Clicked:(id)sender
{
    if (dropDownView != nil && btnSelected != sender) {
        [dropDownView hideDropDown:btnSelected];
        dropDownView = nil;
    }
    btnSelected = sender;
    [self showDropDownAtButton:sender withArrayData:arrAgent];
}

// action clicked button search (icon search)
- (IBAction)btnSearch_Clicked:(id)sender
{
    [self.view endEditing:YES];

    if (dropDownView != nil && btnSelected != sender) {
        [dropDownView hideDropDown:self.btnAgent];
        dropDownView = nil;
    }

    [arrSearchResult removeAllObjects];
    [self getReportSuspendCustomerWithPageNumber:@"1"];
}

// action clicked button search condition for show/hide conditions
- (IBAction)btnSearchCondition_Clicked:(id)sender
{
    [self showConditionsSearchView:!isSearchViewShow];
}


@end
