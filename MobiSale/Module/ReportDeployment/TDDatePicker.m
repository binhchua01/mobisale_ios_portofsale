//
//  TDDatePicker.m
//  MobiSale
//
//  Created by ISC-DanTT on 3/17/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "TDDatePicker.h"
#import "RegistrationFormModel.h"
#import "AppDelegate.h"
#import "ShareData.h"

@implementation TDDatePicker

@synthesize dateFormat, buttonDate, datePicker, chooseDate, selectedDate;

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (void)limitDate:(NSDate*)date minYear:(NSInteger)min maxYear:(NSInteger)max {
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:min];
    NSDate *minDate = [calendar dateByAddingComponents:comps toDate:date options:0];
    [comps setYear:max];
    NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:date options:0];
    [datePicker setMinimumDate:minDate];
    [datePicker setMaximumDate:maxDate];
    
}

- (void)setSelectedDateInField {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:dateFormat];
    chooseDate = [dateFormatter stringFromDate:datePicker.date];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    [buttonDate setTitle:[dateFormatter stringFromDate:datePicker.date] forState:UIControlStateNormal];
    selectedDate = datePicker.date;
    
    
}


- (void)showDatePicker {
    
    CGRect datePickerFrame = CGRectMake(0,0, self.view.frame.size.width, 200);
    self.datePicker = [[UIDatePicker alloc] initWithFrame:datePickerFrame];
    self.datePicker.datePickerMode = UIDatePickerModeDate;
    
    UIView *viewDatePicker = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width+30, 200)];
    [viewDatePicker setBackgroundColor:[UIColor clearColor]];
    
    datePicker.hidden = NO;
    datePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"Vietnamese"];
    
    [viewDatePicker addSubview:datePicker];
    
    if(IS_OS_8_OR_LATER) {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"\n\n\n\n\n\n\n\n\n\n" preferredStyle:UIAlertControllerStyleActionSheet];
        UIAlertAction *doneAction = [UIAlertAction actionWithTitle:@"Xong" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            [self setSelectedDateInField];
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Đóng" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            
        }];
        [alertController addAction:doneAction];
        [alertController addAction:cancelAction];
        [alertController.view addSubview:viewDatePicker];
        
        //[self.view.window.rootViewController presentViewController:alertController animated:YES completion:nil];
        [self.navigationController presentViewController:alertController animated:YES completion:nil];
        
        return;
    }
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"\n\n\n\n\n\n\n\n\n\n" delegate:self cancelButtonTitle:@"Đóng" destructiveButtonTitle:@"Xong" otherButtonTitles:nil, nil];
    [actionSheet addSubview:viewDatePicker];
    actionSheet.tag = 3;
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
    return;
}

#pragma convert date
- (NSDate *)convertStringToDate:(NSString *)dateStringInput {
    @try {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"GMT"];
        [dateFormatter setTimeZone:timeZone];
        NSDate *dateFromString = [[NSDate alloc] init];
        dateFromString = [dateFormatter dateFromString:dateStringInput];
        return dateFromString;
    }
    @catch (NSException *exception) {
        [self showAlertBox:@"Lỗi" message:@"Bạn đã nhập sai định dạng thời gian cần tìm"];
        [CrashlyticsKit recordCustomExceptionName:@"TDDatePicker - funtion -Error convertStringToDate" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
    }
    
}

- (NSString *)convertDateToString:(NSDate*)dateInput {
    @try {
        [[RegistrationFormModel sharedInstance]setDateBirthday:dateInput];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"GMT+7"];
        [dateFormatter setTimeZone:timeZone];
        NSString *stringDate = [dateFormatter stringFromDate:dateInput];
        return stringDate;
    }
    @catch (NSException *exception) {
        [self showAlertBox:@"Lỗi" message:@"Bạn đã nhập sai định dạng thời gian cần tìm"];
        [CrashlyticsKit recordCustomExceptionName:@"TDDatePicker - funtion -Error Bạn đã nhập sai định dạng thời gian cần tìm" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
    }
}

#pragma mark - UIActionSheet delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [self setSelectedDateInField];
    }
}

@end
