//
//  ReportPrecheckListViewController.h
//  MobiSale
//
//  Created by HIEUPC on 4/13/15.
//  Copyright (c) 2015 FPT.RAD.MOBISALE. All rights reserved.
//

#import "BaseViewController.h"
#import "ShareData.h"
#import "Common.h"
#import "UIPopoverListView.h"
#import "ReportPrcheckListRecord.h"
#import "ReportPrecheckListCell.h"

@interface ReportPrecheckListViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIButton *btnPage;
@property (strong, nonatomic) IBOutlet UIButton *btnMonth;
@property (strong, nonatomic) IBOutlet UIButton *btnYear;
@property (strong, nonatomic) IBOutlet UIButton *btnFind;
@property (strong, nonatomic) IBOutlet UIButton *btnDay;
@property (strong, nonatomic) IBOutlet UILabel *lblTotal;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *viewPage;

-(IBAction)btnPage_clicked:(id)sender;
-(IBAction)btnMonth_clicked:(id)sender;
-(IBAction)btnYear_clicked:(id)sender;
-(IBAction)btnDay_clicked:(id)sender;
-(IBAction)btnFind_clicked:(id)sender;

@property (strong, nonatomic) NSMutableArray *arrPage;
@property (strong, nonatomic) NSMutableArray *arrList;
@property (strong, nonatomic) NSMutableArray *arrMonth;
@property (strong, nonatomic) NSMutableArray *arrYear;
@property (strong, nonatomic) NSMutableArray *arrDay;


@end
