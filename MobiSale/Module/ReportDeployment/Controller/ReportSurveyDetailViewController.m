//
//  ReportSurveyDetailViewController.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 4/12/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ReportSurveyDetailViewController.h"
#import "ReportSurveyDetailRecord.h"
#import "ReportSurveyDetailCell.h"
#import "UIPopoverListView.h"
#import "ShareData.h"
#import "KeyValueModel.h"
#import "Common_client.h"
@interface ReportSurveyDetailViewController ()

@end

@implementation ReportSurveyDetailViewController{
    KeyValueModel  *selectedPage;
    bool flagPageNum;
    NSString *totalPage;
}
NSString *totalPage;
@dynamic tableView;
enum tableSourceStype {
    Page = 1,
};

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.separatorColor = [UIColor clearColor];
    self.title = @"BÁO CÁO KHẢO SÁT CT";
    [self LoadData:@"0"];
    flagPageNum = FALSE;
    self.lblName.text = self.SaleName;
    self.screenName=self.title;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews{
    if(IS_IPHONE4){
        self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.tableView.frame.size.height - 88);
        self.viewPage.frame = CGRectMake(self.viewPage.frame.origin.x, self.viewPage.frame.origin.y - 88, self.viewPage.frame.size.width, self.viewPage.frame.size.height);
    }
    
}

#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(self.arrList.count > 0){
        return self.arrList.count;
    }
    return 0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 180;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"ReportSurveyDetailCell";
    ReportSurveyDetailCell *cell = (ReportSurveyDetailCell *)[self.tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ReportSurveyDetailCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    
    if(self.arrList.count > 0){
        ReportSurveyDetailRecord *rc = [self.arrList objectAtIndex:indexPath.row];
        cell.lblAfterTotalCab.text = rc.AfterTotalCab;
        cell.lblBalance.text = rc.Balance;
        cell.lblContract.text = rc.Contract;
        cell.lblFirstTotalCab.text = rc.FirstTotalCab;
        cell.lblFullName.text = rc.FullName;
        cell.lblRegCode.text = rc.RegCode;
        cell.lblRowNumber.text = rc.RowNumber;
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - DropDown view

- (void)setupDropDownView:(NSInteger) tag
{
    
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.tag = tag;
    poplistview.listView.scrollEnabled = TRUE;
    switch (tag) {
        case Page:
            [poplistview setTitle:NSLocalizedString(@"Chọn trang", @"")];
            break;
        default:
            break;
    }
    
    [poplistview show];
    
}
- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35.0f;
}

#pragma mark - UIPopoverListViewDataSource

- (void) setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell tag:(NSInteger)tag{
    KeyValueModel *model;
    switch (tag) {
        case Page:
            model = [self.arrPage objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        default:
            break;
    }
    
    
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row cell:cell tag:popoverListView.tag];
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    switch (popoverListView.tag) {
        case Page:
            count = self.arrPage.count;
            break;
        default:
            break;
    }
    return count;
}

#pragma mark - UIPopoverListViewDelegate

- (void) didDropDownSelected:(NSInteger)row tag:(NSInteger)tag{
    
    switch (tag) {
        case Page:
            selectedPage = [self.arrPage objectAtIndex:row];
            [self.btnPage setTitle:[NSString stringWithFormat:@"%@/%@",selectedPage.Key,totalPage] forState:UIControlStateNormal];
            [self LoadData:selectedPage.Key];
            break;
        default:
            break;
    }
    
    
}


#pragma mark - UIPopoverListViewDelegate

- (void)popoverListView:(UIPopoverListView *)popoverListView
     didSelectIndexPath:(NSIndexPath *)indexPath
{
    [self didDropDownSelected:indexPath.row tag:popoverListView.tag];
    
}

#pragma mark - event button click

-(IBAction)btnPage_clicked:(id)sender
{
    [self setupDropDownView:Page];
}
#pragma mark - Load Data
-(void)LoadData:(NSString *)pagenum {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    [self.view endEditing:YES];
    [self showMBProcess];
    ShareData *shared = [ShareData instance];
    [shared.reportlistdeploymentproxy Reportsurvey:self.SaleName Month:self.Month Year:self.Year Day:self.Day Agent:self.Agent AgentName:self.AgentName PageNumber:pagenum Completehander:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            return ;
        }
       
            [self.tableView setHidden:NO];
            self.arrList = result;
            if(self.arrList.count > 0){
                ReportSurveyDetailRecord *rc = [self.arrList objectAtIndex:0];
                if(flagPageNum == FALSE){
                    [self LoadPage:[rc.TotalPage intValue]];
                    flagPageNum = TRUE;
                }
                self.lblTotal.text = rc.TotalRow;
                [self.tableView reloadData];
            }else {
                [self.tableView setHidden:YES];
                [self ShowAlertBoxEmpty];
                self.lblTotal.text = @"0";
            }
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
    
    
}


#pragma mark - Load dropdownlist
-(void)LoadPage:(int) page {
    totalPage =[NSString stringWithFormat:@"%d",page];
    self.arrPage = [NSMutableArray array];
    int i = 1;
    for (i = 1; i <= page; i++) {
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",i] description:[NSString stringWithFormat:@"%d",i]];
        [self.arrPage addObject: s];
    }
    selectedPage = [self.arrPage objectAtIndex:0];
    NSString*title =[NSString stringWithFormat:@"%@/%@",selectedPage.Key,totalPage];
    [self.btnPage setTitle:title forState:UIControlStateNormal];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
