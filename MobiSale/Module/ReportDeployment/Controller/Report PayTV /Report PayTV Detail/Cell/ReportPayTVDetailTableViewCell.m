//
//  ReportPayTVDetailTableViewCell.m
//  MobiSale
//
//  Created by ISC on 6/14/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ReportPayTVDetailTableViewCell.h"

@implementation ReportPayTVDetailTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // Initialization code
    self.lblContract.text      = @"";
    self.lblPackage.text       = @"";
    self.lblBoxSaleDate.text   = @"";
    self.lblBoxActiveDate.text = @"";
    self.lblType.text          = @"";
    self.lblPhoneNumber.text   = @"";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
