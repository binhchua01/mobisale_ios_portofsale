//
//  ReportPayTVDetailTableViewCell.h
//  MobiSale
//
//  Created by ISC on 6/14/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportPayTVDetailTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblContract;
@property (weak, nonatomic) IBOutlet UILabel *lblPackage;
@property (weak, nonatomic) IBOutlet UILabel *lblBoxSaleDate;
@property (weak, nonatomic) IBOutlet UILabel *lblBoxActiveDate;
@property (weak, nonatomic) IBOutlet UILabel *lblType;
@property (weak, nonatomic) IBOutlet UILabel *lblPhoneNumber;

@end
