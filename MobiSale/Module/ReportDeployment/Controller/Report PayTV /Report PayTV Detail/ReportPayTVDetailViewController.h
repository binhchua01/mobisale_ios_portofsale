//
//  ReportPayTVDetailViewController.h
//  MobiSale
//
//  Created by ISC on 6/14/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"


@interface ReportPayTVDetailViewController :BaseViewController

@property (weak, nonatomic) IBOutlet UITableView *resultTableView;

@property (nonatomic, copy) NSString *saleName;
@property (nonatomic, copy) NSString *subTitle;
@property (nonatomic, copy) NSString *localType;
@property (nonatomic, copy) NSString *month;
@property (nonatomic, copy) NSString *year;
@property (nonatomic, copy) NSString *package;

+(id)initWithNibName:(NSString *)nibName andData:(NSDictionary *)dict;


@end
