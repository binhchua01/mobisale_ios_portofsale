//
//  ReportPayTVViewController.h
//  MobiSale
//
//  Created by ISC on 6/11/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "SelectedExpectedDateViewController.h"
#import "TDDatePicker.h"

@interface ReportPayTVViewController :TDDatePicker<UITextFieldDelegate,SelectedExpectedDateViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lblResultNumber; // Số kết quả tìm kiếm được

@property (weak, nonatomic) IBOutlet UIView *searchView;       // view điều kiện tìm kiếm

@property (weak, nonatomic) IBOutlet UITableView *resultTableView; // table hiển thị kết quả tìm kiếm

@property (weak, nonatomic) IBOutlet UIImageView *imgUpDown;  // image icon up/down on condition search button



@property (weak, nonatomic) IBOutlet UIButton *btnToDate;
@property (weak, nonatomic) IBOutlet UIButton *btnFromDate;


@property (weak, nonatomic) IBOutlet UIButton *btnAgent;    // button chọn tìm theo

@property (weak, nonatomic) IBOutlet UITextField *txtAgentName; // text nhập nội dung cần tìm 

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchViewHeight;
//vutt11
@property (weak, nonatomic) IBOutlet UIView *viewDate;
@property (weak, nonatomic) IBOutlet UIDatePicker *pickerDate;
@property (weak, nonatomic) IBOutlet UITextField *textFieldDatePickerTo;
@property (weak, nonatomic) IBOutlet UITextField *textFieldDatePickerFrom;
@property (strong, nonatomic) NSDateFormatter *formatter;
@property (strong, nonatomic) NSDateFormatter *formatterSendServer;
@property (nonatomic) BOOL sttTextFieldDatePicker;
@property (nonatomic) NSInteger i;
@property (strong, nonatomic) NSString *expectedDateUpdate;
@property (strong, nonatomic) NSString *requiredDateUpdate;
@property (strong, nonatomic) NSString *issueDateUpdate; 
@property BOOL isUpdateExpectedDate;
@property (nonatomic, copy) NSString     *month;
@property (nonatomic, copy) NSString     *year;
@property (nonatomic, copy) NSString     *day;
@property (strong, nonatomic) NSDate *createDate;
@property (strong, nonatomic) NSString *chooseExpectedDate;



@end
