//
//  ReportPayTVViewController.m
//  MobiSale
//
//  Created by ISC on 6/11/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ReportPayTVViewController.h"
#import "NIDropDown.h"
#import "ShareData.h"
#import "DemoTableFooterView.h"
#import "ReportPayTVRecord.h"
#import "ItemsModel.h"
#import "ReportPayTVDetailViewController.h"
#import "DemoModelData.h"


@interface ReportPayTVViewController ()<NIDropDownDelegate, UIPickerViewDelegate, UIPickerViewDataSource>

@end

@implementation ReportPayTVViewController
{
    NIDropDown          *dropDownView; // drop down view show when clicked button
    DemoTableFooterView *tableFooterView; // show view load more when scroll table to bottom
    NSMutableArray      *arrSearchResult;    // data received from server
    NSInteger           totalPage;
    NSInteger           currentPage;
    CGFloat             startOffset;
    BOOL                isSearchViewShow; // show/hide search conditions
    
    ReportPayTVRecord   *record;
    
    NSArray         *arrAgent;
    NSDictionary    *selectedAgent;

    UIButton        *btnSelected;
    
    UIPickerView   *datePickerView;
    NSMutableArray *sourceYear;
    NSMutableArray *sourceMonth;
    NSMutableArray *sourceDay;
    NSString       *currentYear;
    NSString       *currentMonth;
    NSString       *currentDay;
    BOOL isPressButtonIssueDate;
    BOOL isPressButtonExpectedDate;
    
   // UIDatePicker *datePicker;
    BOOL isCheckToDate;
    BOOL isCheckFromDate;
    NSInteger fromDate,toDate;
    
    //vutt11
    NSInteger monthToDate,monthFromDate;
    NSInteger dayToDate,dayFromDate;
    NSString *firstDayofToDate;
    
}
- (NSDateFormatter *)formatter
{
    if (!_formatter) {
        _formatter = [[NSDateFormatter alloc]init];
        [_formatter setDateFormat:@"dd-MM-yyyy"];
    }
    return _formatter;
}

- (NSDateFormatter *)formatterSendServer
{
    if (_formatterSendServer) {
        _formatterSendServer = [[NSDateFormatter alloc]init];
        [_formatterSendServer setDateFormat:@"yyyy-MM-dd"];
    }
    return _formatterSendServer;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    // set navigation title
    self.title = @"B/C HĐ PAY TV";
    // set screen name on google anaticz
    self.screenName = self.title;
   // [self.viewDate setHidden:YES];
    isCheckToDate = NO;
    isCheckFromDate = NO;
    [self setType];
    [self loadAgent];
   // [self typePickerDate];
    //[self setSelectedDateInField];
    
    
   // [datePicker addTarget:self action:@selector(setSelectedDateInField) forControlEvents:UIControlEventValueChanged];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //[self initializeTextFieldInputView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)typePickerDate {
//    
//    CGRect datePickerFrame = CGRectMake(0,0, self.view.frame.size.width, 200);
//    datePicker = [[UIDatePicker alloc] initWithFrame:datePickerFrame];
//    datePicker.datePickerMode = UIDatePickerModeDate;
//}


- (void)setSelectedDateInField {
    
   // NSLog(@"date: %@",datePicker.date.description);
    //vutt11
    [super setSelectedDateInField];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents * components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:self.selectedDate];
    //set Date formatter show on view
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    
    if (isCheckToDate == YES) {
      
        
     // [self.btnToDate setTitle:[self convertDateToString:components] forState:UIControlStateNormal];
        isCheckToDate = NO;
        toDate = [components day];
        dayToDate = [components day];
        monthToDate = [components month];
        
    }
    if (isCheckFromDate == YES) {
            //[self.btnFromDate setTitle:[self convertDateToString:components] forState:UIControlStateNormal];
        
        fromDate = [components day];
        isCheckFromDate = NO;
        dayFromDate = [components day];
        monthFromDate = [components month];
        
    }
    
   // [dateFormatter setDateFormat:@"yyyy-MM-dd"];
   // self.chooseExpectedDate = [dateFormatter stringFromDate:datePicker.date];
    
    // testing
//    dayToDate   = [toDate substringToIndex:2];
//    monthToDate = [toDate substringWithRange:NSMakeRange(3, 2)];
    
//    dayFromDate   = [fromDate substringToIndex:2];
//    monthFromDate = [fromDate substringWithRange:NSMakeRange(3, 2)];
    

    if (monthFromDate!=monthToDate) {
        
//        if (monthFromDate > monthToDate) {
//            [self showAlertBox:@"Thông Báo" message:@"Bạn vui lòng chọn tháng của (Từ ngày) giống tháng (Đến ngày) "];
//            return;
//        }
//        
        
        firstDayofToDate = [dateFormatter stringFromDate:self.selectedDate];
        //firstDayofToDate = [self convertDateToString:self.selectedDate];
        
        firstDayofToDate = [firstDayofToDate stringByReplacingCharactersInRange:NSMakeRange(0, 2) withString:@"01"];
        
        
        [self.btnFromDate setTitle:StringFormat(@"%@",firstDayofToDate) forState:UIControlStateNormal];
        //toDate = firstDayofToDate;
        
    } else {
        
        if (fromDate > toDate) {
            
            [self showAlertBox:@"Thông Báo" message:@"Bạn vui lòng chọn (Từ ngày) nhỏ hơn (Đến ngày)"];
            isCheckToDate = YES;
            return;
        }
    }
    
    //toDate = [self.btnToDate.titleLabel.text integerValue ];
   // fromDate = [self.btnFromDate.titleLabel.text integerValue];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self handleSingleTap:nil];
}

#pragma mark - set type view
- (void)setType {
    // show conditions search view
    isSearchViewShow = YES;
    // set empty result number
    self.lblResultNumber.text = @"";
    // set offset scroll view of table
    startOffset = -20;
    
    // set format date for datePicker
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    [self.btnToDate setTitle:[dateFormatter stringFromDate:[NSDate date]] forState:UIControlStateNormal];
    
    // get firstday
    firstDayofToDate = StringFormat(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    
    firstDayofToDate = [firstDayofToDate stringByReplacingCharactersInRange:NSMakeRange(0, 2) withString:@"01"];
    [self.btnFromDate setTitle:firstDayofToDate forState:UIControlStateNormal];
    //monthToDate = [[firstDayofToDate substringWithRange:NSMakeRange(3, 2)] integerValue];
    // get only year defult
    [dateFormatter setDateFormat:@"yyyy"];
    currentYear  = self.year  = [dateFormatter stringFromDate:[NSDate date]];
    // get only month defult
    [dateFormatter setDateFormat:@"MM"];
    currentMonth = self.month = [dateFormatter stringFromDate:[NSDate date]];
    // get only day defult
    [dateFormatter setDateFormat:@"dd"];
    currentDay = self.day = [dateFormatter stringFromDate:[NSDate date]];
    
    monthToDate = [currentMonth integerValue];
    monthFromDate = [currentMonth integerValue];
    
    
    // Init data
    arrSearchResult = [NSMutableArray array];
    /*
     * set border for conditions search view
     */
    self.searchView.layer.borderColor  = [UIColor colorWithRed:0.133 green:0.698 blue:0.569 alpha:1].CGColor;
    self.searchView.layer.borderWidth  = 1.0f;
    self.searchView.layer.cornerRadius = 4.0f;
    self.resultTableView.hidden = YES;
    /*
     *set the custom view for "load more". See DemoTableFooterView.xib.
     */
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DemoTableFooterView" owner:self options:nil];
    tableFooterView = (DemoTableFooterView *)[nib objectAtIndex:0];

    // set data source for picker view
//    NSString *lastYear = StringFormat(@"%li",[currentYear integerValue] - 1);
//    sourceYear = [[NSMutableArray alloc] initWithObjects:lastYear, self.year, nil];
//    sourceMonth = [[NSMutableArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12", nil];
    
        toDate = [[  NSString stringWithFormat: @"%@",self.btnToDate.titleLabel.text] integerValue];
        fromDate = [[  NSString stringWithFormat: @"%@",self.btnFromDate.titleLabel.text] integerValue];
    
}

#pragma mark - load list agent
- (void)loadAgent {
    arrAgent = [NSArray arrayWithObjects:[NSDictionary dictionaryWithObject:@"Tất cả" forKey:@"0"],[NSDictionary dictionaryWithObject:@"Tên nhân viên" forKey:@"1"], nil];
    selectedAgent = [arrAgent objectAtIndex:0];
    [self.btnAgent setTitle:[[selectedAgent allValues] objectAtIndex:0] forState:UIControlStateNormal];
}

#pragma mark - show or hide conditions search view
- (void)showConditionsSearchView:(BOOL)status {
    isSearchViewShow = status;
    self.searchView.hidden = !status;
    
    /*
     * show conditions search View
     */
    if (status) {
        self.searchViewHeight.constant = 200;
        self.imgUpDown.image = [UIImage imageNamed:@"up-icon-512"];
        
    } else {
        
        /*
         * hide conditions search View
         */
        self.searchViewHeight.constant = 0;
        self.imgUpDown.image = [UIImage imageNamed:@"down-icon-512"];
    }
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

#pragma mark - NIDropDown method
- (void)showDropDownAtButton:(UIButton *)sender withArrayData:(NSArray*)arrData{
    [self.view endEditing:YES];
    
    if (dropDownView == nil) {
        int i = 0;
        NSMutableArray *arrOutput = [[NSMutableArray alloc] init];
        for (i = 0; i < arrData.count; i++) {
            NSDictionary *dict = [arrData objectAtIndex:i];
            [arrOutput addObjectsFromArray:[dict allValues]];
        }
        
        CGFloat height = 40*arrData.count;
        if (arrData.count > 3) {
            height = 120;
        }
        CGFloat width = 0;
        dropDownView = [[NIDropDown alloc] showDropDownAtButton:sender height:&height width:&width data:arrOutput images:nil animation:@"down"];
        dropDownView.delegate = self;
        
        return;
    }
    [dropDownView hideDropDown:sender];
    [self rel];
}

- (void)niDropDownDelegateMethod:(NIDropDown *)sender {
    [self rel];
}

- (void)rel {
    dropDownView = nil;
}

- (void)dropDownListView:(NIDropDown *)dropDownListView didSelectIndexPath:(NSIndexPath *)indexPath atButton:(UIButton *)button{
    [self didDropDownSelected:indexPath.row atButton:button];
}

- (void)didDropDownSelected:(NSInteger)row atButton:(UIButton *)button {
    if (button == self.btnAgent) {
        selectedAgent = [arrAgent objectAtIndex:row];
        return;
    }
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
    if (dropDownView !=nil) {
        [dropDownView hideDropDown:self.btnAgent];
        [self rel];
    }
}

#pragma mark - UITableView datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return arrSearchResult.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    record = arrSearchResult[section];
    return record.items.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    record = arrSearchResult[section];
    return record.saleName;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reuseIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:reuseIdentifier];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    if (arrSearchResult.count > 0) {
        record = arrSearchResult[indexPath.section];
        
        ItemsModel *item = record.items[indexPath.row];
        
        cell.textLabel.text =item.title;
        cell.detailTextLabel.text = StringFormat(@"%li", (long)item.total);
        
        cell.textLabel.textColor = [UIColor colorWithRed:0.133 green:0.698 blue:0.569 alpha:1];
        cell.detailTextLabel.font = [UIFont systemFontOfSize:14.0f];

    }
    
    return cell;
}

#pragma mark - UITableView delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    record = arrSearchResult[indexPath.section];
    
    ItemsModel *item = record.items[indexPath.row];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:record.saleName forKey:@"SaleName"];
    [dict setObject:item.title forKey:@"SubTitle"];
    [dict setObject:StringFormat(@"%li", (long)item.package) forKey:@"Package"];
    [dict setObject:StringFormat(@"%li", (long)item.localType) forKey:@"LocalType"];
    [dict setObject:StringFormat(@"%ld",(long)monthToDate) forKey:@"Month"];
    [dict setObject:self.year forKey:@"Year"];
    [dict setObject:StringFormat(@"%ld",(long)toDate) forKey:@"ToDate"];
    [dict setObject:StringFormat(@"%ld",(long)fromDate) forKey:@"FromDate"];
    
    ReportPayTVDetailViewController *vc = [ReportPayTVDetailViewController initWithNibName:@"ReportPayTVDetailViewController" andData:dict];
    
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Scroll in table view delegate
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    CGFloat currentOffset = scrollView.contentOffset.y;
    
    if (currentOffset > startOffset) {
        [self showConditionsSearchView:NO];
    }
    startOffset = currentOffset;
    
    CGFloat maximumOffset = fabs(scrollView.contentSize.height - scrollView.frame.size.height);
    if (maximumOffset - currentOffset < 0) {
        [self loadMore];
    }
}

//#pragma mark - UIPickerViewDataSource methods
//- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
//{
//    return 3;
//}
//
//- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
//{
//    if(component == 0)
//        return [sourceDay count];
//        return [sourceMonth count];
//        return [sourceYear count];
//}
//
//- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
//{
//
//    if(component == 0)
//    
//        return[sourceDay objectAtIndex:row];
//        return [sourceMonth objectAtIndex:row];
//        return [sourceYear objectAtIndex:row];
//}
//
//
//#pragma mark - UIPickerViewDelegate methods
//- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
//{
//    
//    switch (component) {
//            
//        case 0:
//            
//            NSLog(@"--- day: %@", [sourceDay objectAtIndex:row]);
//            self.day = [sourceDay objectAtIndex:row];
//            // set max of month <= current month
//            if ([self.day isEqualToString:currentDay]) {
//                self.day = currentDay;
//                [pickerView selectRow:1 inComponent:0 animated:YES];
//            }
//            break;
//        case 1:
//            
//            NSLog(@"--- month: %@", [sourceMonth objectAtIndex:row]);
//            self.month = [sourceMonth objectAtIndex:row];
//            // set max of month <= current month
//            if ([self.year isEqualToString:currentYear]) {
//                NSInteger differentMonth = [self.month integerValue] - [currentMonth integerValue];
//                if (differentMonth > 0) {
//                    self.month = currentMonth;
//                    [pickerView selectRow:labs(row - differentMonth) inComponent:0 animated:YES];
//                }
//            }
//            break;
//
//            
//        case 2:
//            
//
//            NSLog(@"--- year: %@", [sourceYear objectAtIndex:row]);
//            self.year = [sourceYear objectAtIndex:row];
//            // set max of month <= current month
//            if ([self.year isEqualToString:currentYear]) {
//                self.month = currentMonth;
//                [pickerView selectRow:3 inComponent:0 animated:YES];
//            }
//            break;
//            
//            
//        default:
//            break;
//    }
//    
//    [self.btnFromDate setTitle:StringFormat(@"%@/%@/%@",self.day, self.month, self.year) forState:UIControlStateNormal];
//
//}

//- (void)showPickerView {
//    
//    CGRect pickerViewFrame = CGRectMake(0,0, self.view.frame.size.width, 200);
//    datePickerView = [[UIPickerView alloc] initWithFrame:pickerViewFrame];
//    
//    datePickerView.delegate = self;
//    datePickerView.dataSource = self;
//    
//    UIView *viewDatePicker = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width+30, 200)];
//    [viewDatePicker setBackgroundColor:[UIColor clearColor]];
//    
//    
//    [viewDatePicker addSubview:datePickerView];
//    
//    for (int i = 0; i < sourceMonth.count; i++) {
//        if ([self.month isEqualToString:sourceMonth[i]]) {
//            [datePickerView selectRow:i inComponent:0 animated:YES];
//            break;
//        }
//    }
//
//    for (int i = 0; i < sourceYear.count; i++) {
//        if ([self.year isEqualToString:sourceYear[i]]) {
//            [datePickerView selectRow:i inComponent:1 animated:YES];
//            break;
//        }
//    }
//
//    for (int i = 0; i<sourceDay.count; i++) {
//        
//        if ([self.day isEqualToString:sourceDay[i]]) {
//            [datePickerView selectRow:i inComponent:2 animated:YES];
//            break;
//        }
//        
//    }
//    
//    if(IS_OS_8_OR_LATER) {
//        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"\n\n\n\n\n\n\n\n\n\n" preferredStyle:UIAlertControllerStyleActionSheet];
// 
//        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Đóng" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
//            
//        }];
//        
//        [alertController addAction:cancelAction];
//        [alertController.view addSubview:viewDatePicker];
//        
//        [self.navigationController presentViewController:alertController animated:YES completion:nil];
//        
//        return;
//    }
//    
//    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"\n\n\n\n\n\n\n\n\n\n" delegate:self cancelButtonTitle:@"Đóng" destructiveButtonTitle:nil otherButtonTitles:nil, nil];
//    [actionSheet addSubview:viewDatePicker];
//    actionSheet.tag = 3;
//    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
//    return;

//    
//    CGRect datePickerFrame = CGRectMake(0,0, self.view.frame.size.width, 200);
//    datePicker = [[UIDatePicker alloc] initWithFrame:datePickerFrame];
//    datePicker.datePickerMode = UIDatePickerModeDate;
//    
//    UIView *viewDatePicker = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width+30, 200)];
//    [viewDatePicker setBackgroundColor:[UIColor clearColor]];
//    
//    datePicker.hidden = NO;
//    datePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"Vietnamese"];
//    
//    [viewDatePicker addSubview:datePicker];
//    
//    if(IS_OS_8_OR_LATER) {
//        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"\n\n\n\n\n\n\n\n\n\n" preferredStyle:UIAlertControllerStyleActionSheet];
//        UIAlertAction *doneAction = [UIAlertAction actionWithTitle:@"Xong" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
//            [self setSelectedDateInField];
//        }];
//        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Đóng" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
//            
//        }];
//        [alertController addAction:doneAction];
//        [alertController addAction:cancelAction];
//        [alertController.view addSubview:viewDatePicker];
//        
//        //[self.view.window.rootViewController presentViewController:alertController animated:YES completion:nil];
//        [self.navigationController presentViewController:alertController animated:YES completion:nil];
//        
//        return;
//    }
//    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"\n\n\n\n\n\n\n\n\n\n" delegate:self cancelButtonTitle:@"Đóng" destructiveButtonTitle:@"Xong" otherButtonTitles:nil, nil];
//    [actionSheet addSubview:viewDatePicker];
//    actionSheet.tag = 3;
//    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
//    return;
    
    
    
//}

#pragma mark - Load More
//
// The method -loadMore was called and will begin fetching data for the next page (more).
// Do custom handling of -footerView if you need to.
//
- (void) willBeginLoadingMore {
    [tableFooterView.activityIndicator startAnimating];
}

// Do UI handling after the "load more" process was completed. In this example, -footerView will
// show a "No more items to load" text.
//
- (void)loadMoreCompleted {
    [tableFooterView.activityIndicator stopAnimating];
}

- (void)loadMore {
    // Do your async loading here
    self.resultTableView.tableFooterView = tableFooterView;
    currentPage ++;
    if (currentPage <= totalPage) {
        [self willBeginLoadingMore];
        [self getReportPayTVWithPageNumber:StringFormat(@"%li",(long)currentPage)];
        return;
    }
    tableFooterView.infoLabel.hidden = NO;
}

#pragma mark - get search results
- (void)getReportPayTVWithPageNumber:(NSString *)pageNumber {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:StringFormat(@"%ld",(long)monthToDate) forKey:@"Month"];
    [dict setObject:self.year  forKey:@"Year"];
    [dict setObject:[NSString stringWithFormat:@"%ld",(long)toDate] forKey:@"ToDate"];
    [dict setObject:[NSString stringWithFormat:@"%ld",(long)fromDate] forKey:@"FromDate"];
    [dict setObject:[[selectedAgent allKeys] objectAtIndex:0] forKey:@"Agent"];
    [dict setObject:self.txtAgentName.text forKey:@"AgentName"];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];  //@"HCM.HuongTD" for test 
    [dict setObject:pageNumber forKey:@"PageNumber"];
    
    [self showMBProcess];
    
    [shared.reportlistdeploymentproxy getReportPayTV:dict completeionHander:^(id result, NSString *errorCode, NSString *message) {
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        
        NSArray *arr = result;
        [arrSearchResult addObjectsFromArray:arr];
        
        if(![errorCode isEqualToString:@"0"] || result == nil || arrSearchResult.count <= 0) {
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",@"Không có kết quả trả về"]];
            

        } else {
            record      = [arr objectAtIndex:0];
            
            currentPage = record.currentPage;
            totalPage   = record.totalPage;
        }

        self.lblResultNumber.text = StringFormat(@"Tổng số: %li",(long)[self getResultNumber:arrSearchResult]);
        
        self.resultTableView.hidden = NO;
        [self.resultTableView reloadData];
        
        [self showConditionsSearchView:NO];
        
        [self loadMoreCompleted];
        
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
        [self hideMBProcess];
    }];
    
}

- (NSInteger)getResultNumber:(NSArray *)arrData {
    NSInteger sum = 0;
    for (ReportPayTVRecord *rc in arrData) {
        sum += rc.items.count;
    }
    return sum;
}

#pragma mark - actions button click
// action clicked button From date
- (IBAction)btnToDate_clicked:(id)sender {
    
//    isPressButtonExpectedDate = YES;
//    SelectedExpectedDateViewController *s = [[SelectedExpectedDateViewController alloc] initWithNibName:@"SelectedExpectedDateViewController" bundle:nil];
//    s.delegate = self;
//    
//    [self presentPopupViewController:s animationType:MJPopupViewAnimationSlideBottomTop];
    isCheckToDate = YES;
    
    //[self alertDidShow];
    //[self showDatePicker:datePicker];
    self.buttonDate = sender;
     [self showDatePicker];
    
}
//vutt11

- (void)alertDidShow{
    //if(IS_IPHONE4){
    CGRect frm = self.view.frame;
    frm.origin.x = frm.origin.x ;
    frm.origin.y = frm.origin.y - 60 ;
    frm.size.width = frm.size.width;
    frm.size.height = frm.size.height;
    
    [UIView animateWithDuration:0.4
                     animations:^{
                         self.view.frame = frm;
                     }];
    
}

-(void) showDatePicker: (UIDatePicker *) modeDatePicker{
    
    UIView *viewDatePicker = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width+30, 200)];
    [viewDatePicker setBackgroundColor:[UIColor clearColor]];
    
    modeDatePicker.hidden = NO;
    modeDatePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"Vietnamese"];
    
    [viewDatePicker addSubview:modeDatePicker];
    
    if(IS_OS_8_OR_LATER){ //if [[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0
        
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"\n\n\n\n\n\n\n\n\n\n" preferredStyle:UIAlertControllerStyleActionSheet];
        
        [alertController.view addSubview:viewDatePicker];
        
        UIAlertAction *doneAction = [UIAlertAction actionWithTitle:@"Xong" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
            [self alertDidHide];
            NSLog(@"OK action");
        }];
        [alertController addAction:doneAction];
        [self.view.window.rootViewController presentViewController:alertController animated:YES completion:nil];
        //[self presentViewController:alertController animated:YES completion:nil]; // error on iOS 9
    }
    else{
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"\n\n\n\n\n\n\n\n\n\n" delegate:self cancelButtonTitle:nil destructiveButtonTitle:@"Xong" otherButtonTitles:nil, nil];
        [actionSheet addSubview:viewDatePicker];
        //actionSheet.bounds = CGRectMake(0, 0, self.view.frame.size.width, 200);
        //[actionSheet showInView:self.view];
        [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
    }
    
//    
//    CGRect datePickerFrame = CGRectMake(0,0, self.view.frame.size.width, 200);
//    datePicker = [[UIDatePicker alloc] initWithFrame:datePickerFrame];
//    datePicker.datePickerMode = UIDatePickerModeDate;
//    
//    UIView *viewDatePicker = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width+30, 200)];
//    [viewDatePicker setBackgroundColor:[UIColor clearColor]];
//    
//    datePicker.hidden = NO;
//    datePicker.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"Vietnamese"];
//    
//    [viewDatePicker addSubview:datePicker];
//    
//    if(IS_OS_8_OR_LATER) {
//        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:@"\n\n\n\n\n\n\n\n\n\n" preferredStyle:UIAlertControllerStyleActionSheet];
//        UIAlertAction *doneAction = [UIAlertAction actionWithTitle:@"Xong" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
//            [self setSelectedDateInField];
//        }];
//        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Đóng" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action){
//            
//        }];
//        [alertController addAction:doneAction];
//        [alertController addAction:cancelAction];
//        [alertController.view addSubview:viewDatePicker];
//        
//        //[self.view.window.rootViewController presentViewController:alertController animated:YES completion:nil];
//        [self.navigationController presentViewController:alertController animated:YES completion:nil];
//        
//        return;
//    }
//    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"\n\n\n\n\n\n\n\n\n\n" delegate:self cancelButtonTitle:@"Đóng" destructiveButtonTitle:@"Xong" otherButtonTitles:nil, nil];
//    [actionSheet addSubview:viewDatePicker];
//    actionSheet.tag = 3;
//    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
//    return;

    
    
}

- (void)alertDidHide{
    // if(IS_IPHONE4){
    CGRect frm = self.view.frame;
    frm.origin.x = frm.origin.x ;
    frm.origin.y = frm.origin.y + 60;
    frm.size.width = frm.size.width;
    frm.size.height = frm.size.height;
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.view.frame = frm;
                     }];
    //  }
}



//bi sai ten dat cho button va co checked todate
//TỪ NGÀY
- (IBAction)btnFromDate_clicked:(id)sender {
    
    isCheckFromDate = YES;

    //[self alertDidShow];
    //[self showDatePicker:datePicker];
      self.buttonDate = sender;
     [self showDatePicker];
}



//vutt11
// action clicked button search with
- (IBAction)btnAgent_Clicked:(id)sender
{
    if (dropDownView != nil && btnSelected != sender) {
        [dropDownView hideDropDown:btnSelected];
        dropDownView = nil;
    }
    btnSelected = sender;
    [self showDropDownAtButton:sender withArrayData:arrAgent];
}

// action clicked button search (icon search)
- (IBAction)btnSearch_Clicked:(id)sender
{
    [self.view endEditing:YES];

    if (dropDownView != nil && btnSelected != sender) {
        [dropDownView hideDropDown:self.btnAgent];
        dropDownView = nil;
    }

    [arrSearchResult removeAllObjects];
    [self getReportPayTVWithPageNumber:@"1"];
}

// action clicked button search condition for show/hide conditions
- (IBAction)btnSearchCondition_Clicked:(id)sender
{
    [self showConditionsSearchView:!isSearchViewShow];
}

//vutt11
- (void) CancelPopup:(NSString *)stringExpectedDate expectedDateUpdate:(NSString *)expectedDateUpdate isUpdateExpectedDate:(BOOL)isUpdateExpectedDate{
    
    self.isUpdateExpectedDate = isUpdateExpectedDate;
    if (isUpdateExpectedDate == YES) {
        if (isPressButtonExpectedDate == YES) {
            isPressButtonExpectedDate = NO;
            [self.btnToDate setTitle:stringExpectedDate forState:UIControlStateNormal];
          //  self.expectedDateUpdate = expectedDateUpdate;
        }
        if (isPressButtonExpectedDate == YES) {
            isPressButtonIssueDate = NO;
            [self.btnFromDate setTitle:stringExpectedDate forState:UIControlStateNormal];
            //self.issueDateUpdate = expectedDateUpdate;
            
        }
    }
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
}

@end
