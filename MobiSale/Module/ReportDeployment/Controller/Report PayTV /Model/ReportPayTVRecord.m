//
//  ReportPayTVRecord.m
//  MobiSale
//
//  Created by ISC on 6/13/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ReportPayTVRecord.h"
#import "ItemsModel.h"

@implementation ReportPayTVRecord

+(id)reportPayTVRecordWithDict:(NSDictionary *)dict
{
    ReportPayTVRecord *record = [[ReportPayTVRecord alloc] init];
    
    record.currentPage  = [dict[@"CurrentPage"] integerValue];
    record.rowNumber    = [dict[@"RowNumber"]   integerValue];
    record.totalPage    = [dict[@"TotalPage"]   integerValue];
    record.totalRow     = [dict[@"TotalRow"]    integerValue];
    record.saleName     = StringFormat(@"%@",dict[@"SaleName"]);
    NSArray *arr        = dict[@"Items"];
    
    NSMutableArray *mArr = [NSMutableArray array];
    
    for (NSDictionary *dict in  arr) {
        ItemsModel *item = [ItemsModel itemsModelWithDict:dict];
        [mArr addObject:item];
    }
    
    record.items = [NSArray arrayWithArray:mArr];
    
    return record;
    
}

@end
