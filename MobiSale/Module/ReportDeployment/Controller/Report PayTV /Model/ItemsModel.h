//
//  ItemsModel.h
//  MobiSale
//
//  Created by ISC on 6/13/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ItemsModel : NSObject

@property (nonatomic, assign) NSInteger localType;
@property (nonatomic, assign) NSInteger package;
@property (nonatomic, assign) NSInteger total;
@property (nonatomic, copy) NSString  *title;

+ (id)itemsModelWithDict:(NSDictionary *)dict;

@end
