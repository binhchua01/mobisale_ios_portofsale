//
//  ReportPayTVDetailRecord.m
//  MobiSale
//
//  Created by ISC on 6/14/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ReportPayTVDetailRecord.h"

@implementation ReportPayTVDetailRecord

+ (id)reportPayTVDetailRecordWithDict:(NSDictionary *)dict {
    
    ReportPayTVDetailRecord *record = [[ReportPayTVDetailRecord alloc] init];
    record.currentPage  = [dict[@"CurrentPage"] integerValue];
    record.rowNumber    = [dict[@"RowNumber"] integerValue];
    record.totalPage    = [dict[@"TotalPage"] integerValue];
    record.totalRow     = [dict[@"TotalRow"] integerValue];
    record.box1         = [dict[@"Box1"] integerValue];
    record.contract     = StringFormat(@"%@",dict[@"Contract"]);
    record.firstTV     = StringFormat(@"%@",dict[@"FirstTV"]);
    record.type     = StringFormat(@"%@",dict[@"Loai"]);
    record.boxActiveDate     = StringFormat(@"%@",dict[@"NgayActiveBox"]);
    record.boxSaleDate     = StringFormat(@"%@",dict[@"NgayBanBox"]);
    record.package     = StringFormat(@"%@",dict[@"Package"]);
    record.saleBox     = StringFormat(@"%@",dict[@"SaleBox"]);
    //NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    NSString *phone = StringFormat(@"%@",dict[@"PhoneNumber"]);
    
    if (phone ?:nil) {
        record.phoneNumber = @"0";
        
    } else{
        
    record.phoneNumber = dict[@"PhoneNumber"];
        
    }
    return record;
}

@end
