//
//  ReportPayTVDetailRecord.h
//  MobiSale
//
//  Created by ISC on 6/14/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportPayTVDetailRecord : NSObject

@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) NSInteger rowNumber;
@property (nonatomic, assign) NSInteger totalPage;
@property (nonatomic, assign) NSInteger totalRow;
@property (nonatomic, assign) NSInteger box1;
@property (nonatomic, copy) NSString *saleBox;
@property (nonatomic, copy) NSString *package;
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *contract;
@property (nonatomic, copy) NSString *firstTV;
@property (nonatomic, copy) NSString *boxActiveDate;
@property (nonatomic, copy) NSString *boxSaleDate;
@property (nonatomic, assign) NSString *phoneNumber;

+ (id)reportPayTVDetailRecordWithDict:(NSDictionary *)dict;

@end
