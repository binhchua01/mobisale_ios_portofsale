//
//  ReportPayTVRecord.h
//  MobiSale
//
//  Created by ISC on 6/13/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportPayTVRecord : NSObject

@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) NSInteger rowNumber;
@property (nonatomic, assign) NSInteger totalPage;
@property (nonatomic, assign) NSInteger totalRow;
@property (nonatomic, copy) NSString *saleName;
@property (nonatomic, copy) NSArray  *items;

+ (id)reportPayTVRecordWithDict:(NSDictionary *)dict;

@end
