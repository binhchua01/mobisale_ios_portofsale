//
//  ItemsModel.m
//  MobiSale
//
//  Created by ISC on 6/13/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ItemsModel.h"

@implementation ItemsModel

+ (id)itemsModelWithDict:(NSDictionary *)dict {
    
    ItemsModel *item = [[self alloc] init];
    
    item.localType  = [dict[@"LocalType"] integerValue];
    item.package    = [dict[@"Package"]   integerValue];
    item.total      = [dict[@"Total"]     integerValue];
    item.title      = StringFormat(@"%@", dict[@"Title"]);
    
    return item;
}

@end
