//
//  ReportSurveyDetailViewController.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 4/12/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BaseViewController.h"

@interface ReportSurveyDetailViewController : BaseViewController<UITableViewDataSource, UITableViewDelegate>

@property(weak, nonatomic)IBOutlet UILabel *lblTotal;
@property(weak, nonatomic)IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *viewPage;
@property (strong, nonatomic) IBOutlet UIButton *btnPage;

-(IBAction)btnPage_clicked:(id)sender;

@property (strong, nonatomic) NSMutableArray *arrList;
@property (strong, nonatomic) NSMutableArray *arrPage;
@property (strong, nonatomic) NSString *Day;
@property (strong, nonatomic) NSString *Month;
@property (strong, nonatomic) NSString *Year;
@property (strong, nonatomic) NSString *Agent;
@property (strong, nonatomic) NSString *AgentName;
@property (strong, nonatomic) NSString *SaleName;



@end
