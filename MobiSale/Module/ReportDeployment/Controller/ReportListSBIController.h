//
//  ReportListSBIController.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 2/4/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BaseViewController.h"


@protocol ReportListSBIControllerDelegate <NSObject>

-(void)CancelPopup;

@end

@interface ReportListSBIController : BaseViewController <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIButton *btnPage;
@property (strong, nonatomic) NSMutableArray *arrPage;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *viewpage;


-(IBAction)btnPage_clicked:(id)sender;
-(IBAction)btnCancel_clicked:(id)sender;

@property (strong, nonatomic) NSMutableArray *arrList;
@property (strong, nonatomic) NSString *Flat;
@property (strong, nonatomic) NSString *SaleName;
@property (strong, nonatomic) NSString *Month;
@property (strong, nonatomic) NSString *Year;
@property (strong, nonatomic) NSString *ObjectName;

@property (strong, nonatomic) NSString *typeTable;

@property (retain, nonatomic) id<ReportListSBIControllerDelegate>delegate;

@end
