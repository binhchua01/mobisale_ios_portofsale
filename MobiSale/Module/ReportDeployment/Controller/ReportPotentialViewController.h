//
//  ReportPotentialViewController.h
//  MobiSale
//
//  Created by ISC-DanTT on 3/16/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TDDatePicker.h"

typedef enum {
    reportPotential = 1,
    reportContractAuto
    
} ReportType;

@interface ReportPotentialViewController : TDDatePicker

@property (assign)ReportType reportType;

@property (strong, nonatomic) IBOutlet UILabel *lblResultNumber;

- (IBAction)btnConditionSearch_Clicked:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *searchView;

@property (strong, nonatomic) IBOutlet UITableView *resultTableView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *searchViewTopSpace;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *searchViewHeight;

@property (strong, nonatomic) IBOutlet UIImageView *imageUpDown;

@property (strong, nonatomic) IBOutlet UIButton *btnToDate;

@property (strong, nonatomic) IBOutlet UIButton *btnFromDate;

@property (strong, nonatomic) IBOutlet UIButton *btnDepartment;

@property (strong, nonatomic) IBOutlet UIButton *btnStatus;

@property (strong, nonatomic) IBOutlet UIButton *btnAgent;

@property (strong, nonatomic) IBOutlet UITextField *txtAgentName;

- (IBAction)btnToDate_Clicked:(id)sender;

- (IBAction)btnFromDate_Clicked:(id)sender;

- (IBAction)btnDepartment_Clicked:(id)sender;

- (IBAction)btnStatus_Clicked:(id)sender;

- (IBAction)btnAgent_Clicked:(id)sender;

- (IBAction)btnSearch_Clicked:(id)sender;

- (IBAction)btnReloadDepartmetn_Clicked:(id)sender;
@end
