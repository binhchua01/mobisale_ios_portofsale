//
//  ReportListSBIController.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 2/4/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ReportListSBIController.h"
#import "ListSBIDetailsViewCell.h"    // List bao cao chung
#import "ListSBIViewCell.h" // Bao cao SBI chi tiet
#import "ContactReportDeploymentRecord.h"
#import "UIPopoverListView.h"
#import "ShareData.h"
#import "KeyValueModel.h"
#import "ReportSBIDetailsViewController.h"
#import "Common_client.h"


@interface ReportListSBIController ()

@end

@implementation ReportListSBIController{
    KeyValueModel *selectedPage;
    int constRepost;
    NSString *totalPage;

}


@dynamic tableView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"DANH SÁCH SBI";
    self.screenName=self.title;
    self.tableView.separatorColor = [UIColor clearColor];
   [self LoadData:@"1"];

}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self LoadPage];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews{
    self.tableView.frame =CGRectMake(0, 62, self.view.frame.size.width, [SiUtils getScreenFrameSize].height  - 90);
}

#pragma mark - TableView


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSMutableArray *key =[[ self.arrList objectAtIndex:section] objectForKey:@"Rows"];
    if([self.arrList count] > 0){
        return [key count];
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [self.arrList count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:
                                 CGRectMake(0, 100, tableView.frame.size.width, 70)];
    [sectionHeaderView setBackgroundColor:[UIColor colorWithRed:0.1 green:0.8 blue:0.8 alpha:1]];
    UIView *topView = [[UIView alloc] initWithFrame:
                       CGRectMake(0, 0, tableView.frame.size.width, 1)];
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:
                            CGRectMake(40, 5, tableView.frame.size.width, 25.0)];
    
    topView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1];

    headerLabel.backgroundColor = [UIColor clearColor];
    [headerLabel setFont:[UIFont fontWithName:@"" size:15.0]];
    headerLabel.textColor = [UIColor colorWithRed:188 green:149 blue:88 alpha:1.0];
    UIImage *myImage = [UIImage imageNamed:@"ic_report_prechecklist_2"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:myImage];
    imageView.frame = CGRectMake(5,1,30,30);
    [sectionHeaderView addSubview:headerLabel];
    [sectionHeaderView addSubview:imageView];
    [sectionHeaderView addSubview:topView];
    if ([self.arrList count] >= section) {
           headerLabel.text = [[self.arrList objectAtIndex:section] objectForKey:@"SaleName"];
        return sectionHeaderView;
    }
    
    else
        return nil;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 34;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *simpleTableIdentifier = @"ListSBIDetailsViewCell";

    ListSBIDetailsViewCell *cell = (ListSBIDetailsViewCell *)[self.tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
    cell = [nib objectAtIndex:0];
    
    NSMutableArray * rowsInSection =[[ self.arrList objectAtIndex:indexPath.section] objectForKey:@"Rows"] ;
    NSString *title =[[rowsInSection objectAtIndex:indexPath.row] objectForKey:@"Title"]?:nil;
    NSString *totals =[[rowsInSection objectAtIndex:indexPath.row] objectForKey:@"Total"]?:nil;
    NSString *all = [NSString stringWithFormat:@"%@ : %@",title,totals];
    cell.lblName.text =all;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
    
}
#pragma mark Table DidSelected
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSMutableArray *check =[[ self.arrList objectAtIndex:indexPath.section] objectForKey:@"Rows"] ;
    if (indexPath.row <= [check count]) {
        ReportSBIDetailsViewController *vc = [[ReportSBIDetailsViewController alloc]initWithNibName:@"ReportSBIDetailsViewController" bundle:nil];
        vc.contentDetailsType =[[check objectAtIndex:indexPath.row] objectForKey:@"Type"]?:nil;
        vc.contentDetailsUser =[[self.arrList objectAtIndex:indexPath.section] objectForKey:@"SaleName"]?:nil;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - DropDown view

- (void)setupDropDownView
{
    
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.listView.scrollEnabled = TRUE;
    [poplistview setTitle:NSLocalizedString(@"Chọn trang", @"")];
    [poplistview show];
    
}
- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35.0f;
}

#pragma mark - UIPopoverListViewDataSource

- (void) setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell{
    KeyValueModel *model;
    model = [self.arrPage objectAtIndex:row];
    cell.textLabel.text = model.Values;
    
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row cell:cell];
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = self.arrPage.count;
    return count;
}

#pragma mark - UIPopoverListViewDelegate

- (void) didDropDownSelected:(NSInteger)row{
    
    selectedPage = [self.arrPage objectAtIndex:row];
    [self.btnPage setTitle:selectedPage.Values forState:UIControlStateNormal];
    [self LoadData:selectedPage.Key];
    NSString*title =[NSString stringWithFormat:@"%@/%@",selectedPage.Key,totalPage];
    [self.btnPage setTitle:title forState:UIControlStateNormal];
    
}


#pragma mark - UIPopoverListViewDelegate

- (void)popoverListView:(UIPopoverListView *)popoverListView
     didSelectIndexPath:(NSIndexPath *)indexPath
{
    [self didDropDownSelected:indexPath.row];
    
}

#pragma Load Data
-(void)LoadData:(NSString *)pagenumber
{
    [self showMBProcess];
    ShareData *shared =[ShareData instance];
    if ([shared.repostSBIList count] >0 && [pagenumber isEqualToString:@"1"]) {
        [self.arrList removeAllObjects];
        self.arrList =(NSMutableArray*)shared.repostSBIList;
        totalPage= [[self.arrList objectAtIndex:0] objectForKey:@"TotalPage"];
        [self.tableView reloadData];
        [self hideMBProcess];
        
    }
    else {
        [shared.appProxy reportSBITotal:shared.currentUser.userName Day:shared.daySBI Month:shared.monthSBI Year:shared.yearSBI Status:shared.statusSBI Agent:shared.typeSBI AgentName:shared.contentSBI PageNumber:pagenumber completionHandler:^(id result, NSString *errorCode, NSString *message) {
            [self hideMBProcess];
            if ([message isEqualToString:@"het phien lam viec"]) {
                [self ShowAlertErrorSession];
                [self LogOut];
                return;
            }
            if(![errorCode isEqualToString:@"0"]){
                [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
                return ;
            }
         
            NSMutableArray *ListObject =[result objectForKey:@"ListObject"];
            if([ListObject count] <1){
                [self showAlertBox:@"Thông báo" message:@"Không có báo cáo SBI"];
                
            }else{
                [self pareDataForRepost:ListObject];
            }
            
        } errorHandler:^(NSError *error) {
            [self showAlertBox:@"Lỗi" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
            [self hideMBProcess];
        }];

    }
    
}
-(void)pareDataForRepost:(NSMutableArray *)repostSBIList{
    [self.arrList removeAllObjects];
    self.arrList =repostSBIList;
    totalPage= [[self.arrList objectAtIndex:0] objectForKey:@"TotalPage"];
    [self.tableView reloadData];
    [self hideMBProcess];
}
#pragma mark - Load page
-(void)LoadPage
{
    int pageNumber=[totalPage intValue];
    if (pageNumber <2) {
        [self.btnPage setEnabled:NO];
        [self.btnPage setTitle:@"1/1" forState:UIControlStateNormal];
        return;
    }
    [self.btnPage setEnabled:YES];
    self.arrPage = [NSMutableArray array];
    int i = 1;
    for (i = 1; i <= pageNumber; i++) {
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",i] description:[NSString stringWithFormat:@"%d",i]];
        [self.arrPage addObject: s];
    }
    selectedPage = [self.arrPage objectAtIndex:0];
    NSString*title =[NSString stringWithFormat:@"%@/%@",selectedPage.Values,totalPage];
    [self.btnPage setTitle:title forState:UIControlStateNormal];
}

#pragma mark - event button click
-(IBAction)btnPage_clicked:(id)sender
{
    [self setupDropDownView];
}

-(IBAction)btnCancel_clicked:(id)sender
{
    if(self.delegate){
        [self.delegate CancelPopup];
    }
    [ShareData instance].repostSBIList=nil;
}
@end
