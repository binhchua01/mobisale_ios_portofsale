//
//  ReportPotentialViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 3/16/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ReportPotentialViewController.h"
#import "NIDropDown.h"
#import "ShareData.h"
#import "ReportPotentialObjTotalRecord.h"
#import "ReportContractAutoRecord.h"
#import "ListPotentialCustomersViewController.h"
#import "ListReportContractAutoDetailViewController.h"
#import "DemoTableFooterView.h"

@interface ReportPotentialViewController () <NIDropDownDelegate, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>

@end

@implementation ReportPotentialViewController {
    NSMutableArray *arrResult;
    BOOL            isSearchViewShow;
    UIButton        *btnSelected;
    NSArray         *arrStatus;
    NSArray         *arrAgent;
    NSArray         *arrDepartment;
    NIDropDown      *dropDown;
    NSDictionary    *selectedStatus;
    NSDictionary    *selectedAgent;
    NSDictionary    *selectedDepartment;
    NSString        *fromDate;
    NSString        *toDate;
    ReportPotentialObjTotalRecord *record;
    ReportContractAutoRecord *contractAutoRecord;
    
    DemoTableFooterView *footerView;
    NSInteger totalPage;
    NSInteger currentPage;
    CGFloat startOffset;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.reportType == reportContractAuto) {
        self.title = @"B/C HỢP ĐỒNG TỰ ĐỘNG";

        
    } else {
        self.title = @"B/C KHÁCH HÀNG TIỀM NĂNG";
    }
    
    self.screenName = self.title;
    
    arrResult = [NSMutableArray array];
    self.lblResultNumber.text = @"";
    startOffset = -20;
    
    self.searchView.layer.borderColor  = [UIColor colorWithRed:0.133 green:0.698 blue:0.569 alpha:1].CGColor;
    self.searchView.layer.borderWidth  = 1.0f;
    self.searchView.layer.cornerRadius = 4.0f;
    self.resultTableView.hidden = YES;
    /*
     *set the custom view for "load more". See DemoTableFooterView.xib.
     */
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DemoTableFooterView" owner:self options:nil];
    footerView = (DemoTableFooterView *)[nib objectAtIndex:0];
    
    self.btnToDate.enabled = NO;
    
    //[self.view setMultipleTouchEnabled:YES];
    //UITapGestureRecognizer *tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    //[self.view addGestureRecognizer:tapper];
    /*
     *add date picker
     */
//    CGRect datePickerFrame = CGRectMake(0,0, self.view.frame.size.width, 200);
//    self.datePicker = [[UIDatePicker alloc] initWithFrame:datePickerFrame];
//    self.datePicker.datePickerMode = UIDatePickerModeDate;
    self.dateFormat = @"yyyy-MM-dd";
    
    isSearchViewShow = YES;
    
    record = [[ReportPotentialObjTotalRecord alloc] init];
    
    contractAutoRecord = [[ReportContractAutoRecord alloc] init];

    if (self.reportType == reportContractAuto) {
        arrStatus = [NSArray arrayWithObjects:[NSDictionary dictionaryWithObject:@"Tất cả" forKey:@"0"], [NSDictionary dictionaryWithObject:@"HĐ đã được duyệt" forKey:@"1"],[NSDictionary dictionaryWithObject:@"HĐ chưa được duyệt" forKey:@"2"], [NSDictionary dictionaryWithObject:@"HĐ chưa có phiếu thi công" forKey:@"3"],[NSDictionary dictionaryWithObject:@"HĐ đã duyệt chưa có phiếu thi công" forKey:@"4"], nil];
        
    } else {
        arrStatus = [NSArray arrayWithObjects:[NSDictionary dictionaryWithObject:@"Chưa lên PĐK" forKey:@"0"],[NSDictionary dictionaryWithObject:@"Đã lên PĐK" forKey:@"1"],[NSDictionary dictionaryWithObject:@"Tất cả" forKey:@"2"], nil];
    }
    
    arrAgent = [NSArray arrayWithObjects:[NSDictionary dictionaryWithObject:@"Tất cả" forKey:@"0"],[NSDictionary dictionaryWithObject:@"Tên nhân viên" forKey:@"1"], nil];
    
    [self loadData];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self handleSingleTap:nil];
}

- (void)loadData {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    [self.btnFromDate setTitle:[dateFormatter stringFromDate:[NSDate date]] forState:UIControlStateNormal];
    [self.btnToDate   setTitle:[dateFormatter stringFromDate:[NSDate date]] forState:UIControlStateNormal];

    selectedStatus = [arrStatus objectAtIndex:0];
    [self.btnStatus setTitle:[[selectedStatus allValues] objectAtIndex:0] forState:UIControlStateNormal];
    
    selectedAgent = [arrAgent objectAtIndex:0];
    [self.btnAgent setTitle:[[selectedAgent allValues] objectAtIndex:0] forState:UIControlStateNormal];
    
    arrDepartment = [ShareData instance].currentUser.arrDepartment;
    selectedDepartment = [arrDepartment objectAtIndex:0];
    [self.btnDepartment setTitle:[[selectedDepartment allValues] objectAtIndex:0] forState:UIControlStateNormal];

}

- (void)loadDepartmentList {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    [self showMBProcess];
    [shared.reportlistdeploymentproxy getDepartmentList:shared.currentUser.userName completeionHander:^(id result, NSString *errorCode, NSString *message) {
        
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"0"] || result == nil){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",@"Không lấy được danh sách phòng"]];
            return ;
        }
        NSMutableArray *arr = [NSMutableArray array];
        for (NSDictionary *dict in result) {
            NSString *key   = [dict objectForKey:@"ID"];
            NSString *value = [dict objectForKey:@"Name"];
            [arr addObject:[NSDictionary dictionaryWithObject:value forKey:key]];
        }
        arrDepartment = [NSArray arrayWithArray:arr];
        selectedDepartment = [arrDepartment objectAtIndex:0];
        [self.btnDepartment setTitle:[[selectedDepartment allValues] objectAtIndex:0] forState:UIControlStateNormal];
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
        [self hideMBProcess];
    }];
}

- (IBAction)btnConditionSearch_Clicked:(id)sender {
    if (isSearchViewShow == NO) {
        [self showSearchView];
        return;
    }
    if (isSearchViewShow == YES) {
        [self hideSearchView];
        return;
    }
}

- (IBAction)btnFromDate_Clicked:(id)sender {
    
    if (dropDown != nil && btnSelected != sender) {
        [dropDown hideDropDown:btnSelected];
        dropDown = nil;

    }
    self.buttonDate = sender;
    [self showDatePicker];
    [self limitDate:[NSDate date] minYear:-99 maxYear:0];

    self.btnToDate.enabled = YES;
    
}

- (IBAction)btnToDate_Clicked:(id)sender {

    if (dropDown != nil && btnSelected != sender) {
        [dropDown hideDropDown:btnSelected];
        dropDown = nil;
    }
    self.buttonDate = sender;
    [self showDatePicker];
    [self limitDate:self.selectedDate minYear:0 maxYear:9];

}

- (IBAction)btnDepartment_Clicked:(id)sender {
    if (dropDown != nil && btnSelected != sender) {
        [dropDown hideDropDown:btnSelected];
        dropDown = nil;
    }
    btnSelected = sender;
    [self showDropDownAtButton:sender withArrayData:arrDepartment];
}

- (IBAction)btnStatus_Clicked:(id)sender {
    if (dropDown != nil && btnSelected != sender) {
        [dropDown hideDropDown:btnSelected];
        dropDown = nil;
    }
    btnSelected = sender;
    [self showDropDownAtButton:sender withArrayData:arrStatus];
    
}

- (IBAction)btnAgent_Clicked:(id)sender {
    if (dropDown != nil && btnSelected != sender) {
        [dropDown hideDropDown:btnSelected];
        dropDown = nil;
    }
    btnSelected = sender;
    [self showDropDownAtButton:sender withArrayData:arrAgent];
    
}

- (IBAction)btnSearch_Clicked:(id)sender {
    [self.view endEditing:YES];

    if (dropDown != nil && btnSelected != sender) {
        [dropDown hideDropDown:btnSelected];
        dropDown = nil;
    }
    if (arrDepartment.count <= 0) {
        [self showAlertBox:@"Thông báo" message:@"Chưa tải được danh sách phòng. \nVui lòng bấm nút refresh để tải danh sách phòng!"];
        return;
    }
    [arrResult removeAllObjects];
    [self getReportPotentialResultWithPageNumber:@"1"];
}

- (IBAction)btnReloadDepartmetn_Clicked:(id)sender {
    [self loadDepartmentList];
}

#pragma mark - return search result 
- (void)getReportPotentialResultWithPageNumber:(NSString *)pageNumber {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    ShareData *shared = [ShareData instance];

    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    fromDate = [self convertDateToString: [self convertStringToDate:self.btnFromDate.titleLabel.text]];
    toDate   = [self convertDateToString: [self convertStringToDate:self.btnToDate.titleLabel.text]];

    [dict setObject:fromDate forKey:@"FromDate"];
    [dict setObject:toDate forKey:@"ToDate"];
    [dict setObject:[[selectedDepartment allKeys] objectAtIndex:0] forKey:@"Dept"];
    [dict setObject:[[selectedStatus allKeys] objectAtIndex:0] forKey:@"Status"];
    [dict setObject:[[selectedAgent allKeys] objectAtIndex:0] forKey:@"Agent"];
    [dict setObject:self.txtAgentName.text forKey:@"AgentName"];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    [dict setObject:pageNumber forKey:@"PageNumber"];
    
    [self showMBProcess];
    if (self.reportType == reportContractAuto) {
        [shared.reportlistdeploymentproxy reportCreateObjectTotal:dict completeionHander:^(id result, NSString *errorCode, NSString *message) {
            
            [self hideMBProcess];
            
            NSArray *arr = result;
            [arrResult addObjectsFromArray:arr];
            
            if ([message isEqualToString:@"het phien lam viec"]) {
                [self ShowAlertErrorSession];
                [self LogOut];
                return;
            }
            if(![errorCode isEqualToString:@"0"] || result == nil || arrResult.count <= 0) {
                [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",@"Không có kết quả trả về"]];
                
            } else {
                contractAutoRecord      = [arr objectAtIndex:0];
                currentPage = [contractAutoRecord.CurrentPage integerValue];
                totalPage   = [contractAutoRecord.TotalPage integerValue];
            }
     
            self.lblResultNumber.text = contractAutoRecord.TotalRow;

            self.resultTableView.hidden = NO;
            [self.resultTableView reloadData];
            [self hideSearchView];
            [self loadMoreCompleted];
            
        } errorHandler:^(NSError *error) {
            [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
            [self hideMBProcess];
            
        }];

        return;
    }
    
    [shared.reportlistdeploymentproxy reportPotentialObjTotal:dict completeionHander:^(id result, NSString *errorCode, NSString *message) {
        
        [self hideMBProcess];
        
        NSArray *arr = result;
        [arrResult addObjectsFromArray:arr];
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"0"] || result == nil || arrResult.count <= 0) {
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",@"Không có kết quả trả về"]];
            return ;
        }

        record      = [arr objectAtIndex:0];
        currentPage = [record.CurrentPage integerValue];
        totalPage   = [record.TotalPage integerValue];
            
        self.lblResultNumber.text = record.TotalRow;
        
        self.resultTableView.hidden = NO;
        [self.resultTableView reloadData];
        [self hideSearchView];
        [self loadMoreCompleted];
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
        [self hideMBProcess];

    }];
    
}

#pragma mark - NIDropDown method
- (void)showDropDownAtButton:(UIButton *)sender withArrayData:(NSArray*)arrData{
    [self.view endEditing:YES];
    
    if (dropDown == nil) {
        int i = 0;
        NSMutableArray *arrOutput = [[NSMutableArray alloc] init];
        for (i = 0; i < arrData.count; i++) {
            NSDictionary *dict = [arrData objectAtIndex:i];
            [arrOutput addObjectsFromArray:[dict allValues]];
        }
        
        CGFloat height = 40*arrData.count;
        if (arrData.count > 3) {
            height = 120;
        }
        CGFloat width = 0;
        dropDown = [[NIDropDown alloc] showDropDownAtButton:sender height:&height width:&width data:arrOutput images:nil animation:@"down"];
        dropDown.delegate = self;
        
        return;
    }
    [dropDown hideDropDown:sender];
    [self rel];
}

- (void)niDropDownDelegateMethod:(NIDropDown *)sender {
    [self rel];
}

- (void)rel {
    dropDown = nil;
}

- (void)dropDownListView:(NIDropDown *)dropDownListView didSelectIndexPath:(NSIndexPath *)indexPath atButton:(UIButton *)button{
    [self didDropDownSelected:indexPath.row atButton:button];
}

- (void)didDropDownSelected:(NSInteger)row atButton:(UIButton *)button {
    if (button == self.btnStatus) {
        selectedStatus = [arrStatus objectAtIndex:row];
        return;
    }
    if (button == self.btnAgent) {
        selectedAgent = [arrAgent objectAtIndex:row];
        return;
    }
    if (button == self.btnDepartment) {
        selectedDepartment = [arrDepartment objectAtIndex:row];
        return;
    }
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
    if (dropDown !=nil) {
        [dropDown hideDropDown:btnSelected];
        [self rel];
    }
}

#pragma mark - show and hide search view

- (void)showSearchView {
    self.searchViewHeight.constant = 255;
    self.searchView.hidden = NO;
    self.imageUpDown.image = [UIImage imageNamed:@"up-icon-512"];
    isSearchViewShow = YES;
    [UIView animateWithDuration:0.25
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

- (void)hideSearchView {
    self.searchViewHeight.constant = 0;
    self.searchView.hidden = YES;
    self.imageUpDown.image = [UIImage imageNamed:@"down-icon-512"];
    isSearchViewShow = NO;
    [UIView animateWithDuration:0.25
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

#pragma mark - UITableView datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrResult.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 69;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *reuseIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    if (self.reportType == reportContractAuto) {
        
        contractAutoRecord = [arrResult objectAtIndex:indexPath.row];
        
        cell.textLabel.text = contractAutoRecord.Name;
        cell.detailTextLabel.text = StringFormat(@"%@: %@",contractAutoRecord.StatusName, contractAutoRecord.Total);

        
    } else {
        record = [arrResult objectAtIndex:indexPath.row];
        
        cell.textLabel.text = record.CreateBy;
        cell.detailTextLabel.text = StringFormat(@"Tổng số KHTN: %@",record.Total);

    }
    
    cell.textLabel.textColor = [UIColor colorWithRed:0.133 green:0.698 blue:0.569 alpha:1];
    
    cell.detailTextLabel.font = [UIFont systemFontOfSize:14.0f];
    
    cell.imageView.image = [UIImage imageNamed:@"search_32"];
    cell.imageView.hidden = YES;
    
    UILabel *numberLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, 39, 40)];
    numberLabel.backgroundColor = [UIColor colorWithRed:0.133 green:0.698 blue:0.569 alpha:0.8];
    numberLabel.textColor = [UIColor whiteColor];
    numberLabel.textAlignment = NSTextAlignmentCenter;
    numberLabel.layer.masksToBounds = YES;
    numberLabel.layer.cornerRadius = numberLabel.frame.size.width/2;
    numberLabel.clipsToBounds = YES;
    numberLabel.text = StringFormat(@"%li", (long)indexPath.row + 1);

    [cell.contentView addSubview:numberLabel];
    
    return cell;
}

#pragma mark - UITableView delegate 
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.reportType == reportContractAuto) {
        contractAutoRecord = [arrResult objectAtIndex:indexPath.row];
        
        ListReportContractAutoDetailViewController *vc = [[ListReportContractAutoDetailViewController alloc] init];
        vc.fromDate = fromDate;
        vc.toDate   = toDate;
        vc.status   = contractAutoRecord.Status;
        vc.username = contractAutoRecord.Name;
        
        [self.navigationController pushViewController:vc animated:YES];
        
        return;
        
    }
    
    record = [arrResult objectAtIndex:indexPath.row];
    
    ListPotentialCustomersViewController *vc = [[ListPotentialCustomersViewController alloc] init];
    vc.listPotentialType = detail;
    vc.UserName = record.CreateBy;
    vc.FromDate = fromDate;//@"2015-12-01";
    vc.ToDate   = toDate;  //@"2016-03-17";
    vc.Status   = [[selectedStatus allKeys] objectAtIndex:0];
    vc.Agent    = [[selectedAgent  allKeys] objectAtIndex:0];
    
    if (self.txtAgentName.text.length <= 0) {
        self.txtAgentName.text = @"";
    }
    
    vc.AgentName = self.txtAgentName.text;
    
    [self.navigationController pushViewController:vc animated:YES];

    return;
}

#pragma mark - Scroll view delegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    CGFloat currentOffset = scrollView.contentOffset.y;

    if (currentOffset > startOffset) {
        [self hideSearchView];
    }
    startOffset = currentOffset;
    
    CGFloat maximumOffset = fabs(scrollView.contentSize.height - scrollView.frame.size.height);
    if (maximumOffset - currentOffset < 0) {
        [self loadMore];
    }
}

#pragma mark - Load More
//
// The method -loadMore was called and will begin fetching data for the next page (more).
// Do custom handling of -footerView if you need to.
//
- (void) willBeginLoadingMore {
    [footerView.activityIndicator startAnimating];
}

// Do UI handling after the "load more" process was completed. In this example, -footerView will
// show a "No more items to load" text.
//
- (void)loadMoreCompleted {
    [footerView.activityIndicator stopAnimating];
}

- (void)loadMore {
    // Do your async loading here
    self.resultTableView.tableFooterView = footerView;
    currentPage ++;
    if (currentPage <= totalPage) {
        [self willBeginLoadingMore];
        [self getReportPotentialResultWithPageNumber:StringFormat(@"%li",(long)currentPage)];
        return;
    }
    footerView.infoLabel.hidden = NO;
}


@end
