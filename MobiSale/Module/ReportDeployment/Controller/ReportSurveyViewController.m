//
//  ReportSurveyViewController.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 4/11/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ReportSurveyViewController.h"
#import "KeyValueModel.h"
#import "UIPopoverListView.h"
#import "ReportSurveyRecord.h"
#import "ReportSurveyCell.h"
#import "ReportSurveyDetailViewController.h"
#import "ShareData.h"
#import "Common.h"
#import "Common_client.h"

@interface ReportSurveyViewController ()

@end

@implementation ReportSurveyViewController{
    KeyValueModel  *selectedPage, *selectedMonth, *selectedYear, *selectedDay, *selectedType;
    bool flagPageNum;
    NSString *totalPage;
}
@dynamic tableView;
enum tableSourceStype {
    Page = 1,
    Month = 2,
    Year = 3,
    Day= 4,
    Type= 5,
};

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self LoadMonth];
    [self LoadYear];
    [self LoadDay];
    [self LoadTypeSearch];
    self.tableView.separatorColor = [UIColor clearColor];
    self.title = @"BÁO CÁO KHẢO SÁT";
    flagPageNum = FALSE;
    self.screenName=self.title;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews{
    if(IS_IPHONE4){
        self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.tableView.frame.size.height - 88);
        self.viewPage.frame = CGRectMake(self.viewPage.frame.origin.x, self.viewPage.frame.origin.y - 88, self.viewPage.frame.size.width, self.viewPage.frame.size.height);
    }
}

#pragma mark - UITableView Delegate & Datasrouce -
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.arrList.count > 0){
        return self.arrList.count;
    }
    return 0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 180;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"ReportSurveyCell";
    ReportSurveyCell *cell = (ReportSurveyCell *)[self.tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ReportSurveyCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    
    if(self.arrList.count > 0){
        ReportSurveyRecord *rc = [self.arrList objectAtIndex:indexPath.row];
        cell.lblSaleName.text = rc.SaleName;
        cell.lblCabPlus.text = rc.CabPlus;
        cell.lblCabSub.text = rc.CabSub;
        cell.lblObjectCabPlus.text = rc.ObjectCabPlus;
        cell.lblObjectCabSub.text = rc.ObjectCabSub;
        cell.lblTotalObject.text = rc.TotalObject;
        cell.lblRowNumber.text = rc.RowNumber;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ReportSurveyRecord *rc = [self.arrList objectAtIndex:indexPath.row];
    ReportSurveyDetailViewController *vc = [[ReportSurveyDetailViewController alloc]initWithNibName:@"ReportSurveyDetailViewController" bundle:nil];
    vc.Day = selectedDay.Key;
    vc.Month = selectedMonth.Key;
    vc.Year = selectedYear.Key;
    vc.Agent = @"";//selectedType.Key;
    vc.AgentName = @"";
    vc.SaleName = rc.SaleName;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - DropDown view
- (void)setupDropDownView:(NSInteger) tag {
    
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.tag = tag;
    poplistview.listView.scrollEnabled = TRUE;
    switch (tag) {
        case Month:
            [poplistview setTitle:NSLocalizedString(@"Chọn tháng", @"")];
            break;
        case Year:
            [poplistview setTitle:NSLocalizedString(@"Chọn năm", @"")];
            break;
        case Page:
            [poplistview setTitle:NSLocalizedString(@"Chọn trang", @"")];
            break;
        case Day:
            [poplistview setTitle:NSLocalizedString(@"Chọn ngày", @"")];
            break;
        case Type:
            [poplistview setTitle:NSLocalizedString(@"Chọn loại tìm kiếm", @"")];
            break;
        default:
            break;
    }
    
    [poplistview show];
    
}
- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35.0f;
}

#pragma mark - UIPopoverListViewDataSource
- (void) setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell tag:(NSInteger)tag{
    KeyValueModel *model;
    switch (tag) {
        case Month:
            model = [self.arrMonth objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Year:
            model = [self.arrYear objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Page:
            model = [self.arrPage objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Day:
            model = [self.arrDay objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Type:
            model = [self.arrType objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        default:
            break;
    }
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row cell:cell tag:popoverListView.tag];
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section {
    NSInteger count = 0;
    switch (popoverListView.tag) {
        case Month:
            count = self.arrMonth.count;
            break;
        case Year:
            count = self.arrYear.count;
            break;
        case Page:
            count = self.arrPage.count;
            break;
        case Day:
            count = self.arrDay.count;
            break;
        case Type:
            count = self.arrType.count;
            break;
        default:
            break;
    }
    return count;
}

#pragma mark - UIPopoverListViewDelegate
- (void) didDropDownSelected:(NSInteger)row tag:(NSInteger)tag {
    switch (tag) {
        case Month:
            selectedMonth = [self.arrMonth objectAtIndex:row];
            [self.btnMonth setTitle:selectedMonth.Values forState:UIControlStateNormal];
            break;
        case Year:
            selectedYear = [self.arrYear objectAtIndex:row];
            [self.btnYear setTitle:selectedYear.Values forState:UIControlStateNormal];
            break;
        case Page:
            selectedPage = [self.arrPage objectAtIndex:row];
            [self.btnPage setTitle:[NSString stringWithFormat:@"%@/%@",selectedPage.Key,totalPage] forState:UIControlStateNormal];
            [self LoadData:selectedPage.Key];
            break;
        case Day:
            selectedDay = [self.arrDay objectAtIndex:row];
            [self.btnDay setTitle:selectedDay.Values forState:UIControlStateNormal];
            break;
        case Type:
            selectedType = [self.arrType objectAtIndex:row];
            [self.btnType setTitle:selectedType.Values forState:UIControlStateNormal];
            self.txtTextSearch.text = @"";
            if (![selectedType.Key isEqualToString:@"0"]) {
                self.txtTextSearch.enabled = YES;
                [self.txtTextSearch becomeFirstResponder];
            }
            if ([selectedType.Key isEqualToString:@"0"]) {
                self.txtTextSearch.enabled = NO;
            }
            break;
        default:
            break;
    }
    
    
}


#pragma mark - UIPopoverListViewDelegate

- (void)popoverListView:(UIPopoverListView *)popoverListView
     didSelectIndexPath:(NSIndexPath *)indexPath
{
    [self didDropDownSelected:indexPath.row tag:popoverListView.tag];
    
}

#pragma mark - event button click

-(IBAction)btnPage_clicked:(id)sender {
    [self setupDropDownView:Page];
}
-(IBAction)btnMonth_clicked:(id)sender {
    [self setupDropDownView:Month];
}
-(IBAction)btnYear_clicked:(id)sender {
    [self setupDropDownView:Year];
}
-(IBAction)btnDay_clicked:(id)sender {
    [self setupDropDownView:Day];
}
-(IBAction)btnType_clicked:(id)sender {
    [self setupDropDownView:Type];
}
-(IBAction)btnFind_clicked:(id)sender {
    [self LoadData:@"1"];
}
#pragma mark - Load Data
-(void)LoadData:(NSString *)pagenum {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    [self.view endEditing:YES];
    [self showMBProcess];
    ShareData *shared = [ShareData instance];
    [shared.reportlistdeploymentproxy ReportTotalSurvey:shared.currentUser.userName Month:selectedMonth.Key Year:selectedYear.Key Day:selectedDay.Key Agent:selectedType.Key AgentName:self.txtTextSearch.text PageNumber:pagenum Completehander:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            return ;
        }
          [self.tableView setHidden:NO];
            self.arrList = result;
            if(self.arrList.count > 0){
                ReportSurveyRecord *rc = [self.arrList objectAtIndex:0];
                if([pagenum isEqualToString:@"1"]){
                    self.lblTotal.text = [NSString stringWithFormat:@"%@", rc.TotalRow];
                    [self LoadPage:[rc.TotalPage intValue]];
                    flagPageNum = TRUE;
                }
                [self.tableView reloadData];
            }else {
                [self.tableView setHidden:YES];
                [self ShowAlertBoxEmpty];
                self.lblTotal.text = @"0";
            }
        
    } errorHandler:^(NSError *error) {
         [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
    
    
}


#pragma mark - Load dropdownlist
-(void)LoadPage:(int) page {
    totalPage =[NSString stringWithFormat:@"%d",page];
//    if (page <2) {
//     //   [self.btnPage setEnabled:NO];
//        [self.btnPage setTitle:@"1" forState:UIControlStateNormal];
//        return;
//    }
    [self.btnPage setEnabled:YES];
    self.arrPage = [NSMutableArray array];
    int i = 1;
    for (i = 1; i <= page; i++) {
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",i] description:[NSString stringWithFormat:@"%d",i]];
        [self.arrPage addObject: s];
    }
    
    selectedPage = [self.arrPage objectAtIndex:0];
    NSString*title =[NSString stringWithFormat:@"%@/%d",selectedPage.Values,page];
    [self.btnPage setTitle:title forState:UIControlStateNormal];
}

-(void)LoadMonth {
    self.arrMonth = [NSMutableArray array];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM"];
    NSString *MonthString = [formatter stringFromDate:[NSDate date]];
    for (int i = 1; i <= 12; i++) {
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",i] description:[NSString stringWithFormat:@"%d",i]];
        [self.arrMonth addObject: s];
    }
    selectedMonth = [self.arrMonth objectAtIndex:[MonthString intValue] - 1];
    [self.btnMonth setTitle:selectedMonth.Values forState:UIControlStateNormal];
}

-(void)LoadYear {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    NSString *yearString = [formatter stringFromDate:[NSDate date]];
    self.arrYear = [NSMutableArray array];
    int year = [yearString intValue];
    for (int i = 0; i <= 1; i++) {
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",year - i] description:[NSString stringWithFormat:@"%d",year - i]];
        [self.arrYear addObject: s];
    }
    selectedYear= [self.arrYear objectAtIndex:0];
    [self.btnYear setTitle:selectedYear.Values forState:UIControlStateNormal];
}

-(void)LoadDay {
    self.arrDay = [NSMutableArray array];
    for (int i = 0; i <= 31; i++) {
        KeyValueModel *s;
        if(i == 0){
            s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",i] description:[NSString stringWithFormat:@"%@",@"Tất cả"]];
        }else {
            s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",i] description:[NSString stringWithFormat:@"%d",i]];
        }
        
        [self.arrDay addObject: s];
    }
    
    selectedDay= [self.arrDay objectAtIndex:0];
    [self.btnDay setTitle:selectedDay.Values forState:UIControlStateNormal];
}

-(void)LoadTypeSearch {
    ShareData *shared = [ShareData instance];
    self.arrType = [Common getTypeSearch:@"3"];
    if([shared.currentUser.IsManager isEqualToString:@"1"]){
        selectedType = [self.arrType objectAtIndex:0];
        [self.btnType setTitle:selectedType.Values forState:UIControlStateNormal];
        self.txtTextSearch.enabled = NO;
    }else {
        selectedType = [self.arrType objectAtIndex:1];
        [self.btnType setTitle:selectedType.Values forState:UIControlStateNormal];
        self.btnType.enabled = NO;
        self.txtTextSearch.text = shared.currentUser.userName;
        self.txtTextSearch.enabled = NO;
    }
    
}


@end
