//
//  ListReportContractAutoDetailViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 3/21/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ListReportContractAutoDetailViewController.h"
#import "ListReportContractAutoDetailViewCell.h"
#import "ReportContractAutoDetailRecord.h"
#import "DemoTableFooterView.h"
#import "ShareData.h"

@interface ListReportContractAutoDetailViewController ()

@end

@implementation ListReportContractAutoDetailViewController {
    NSMutableArray *arrData;
    ReportContractAutoDetailRecord *record;
    
    DemoTableFooterView *footerView;
    NSInteger currentPage;
    NSInteger totalPage;
}

@synthesize username, fromDate, toDate, status;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"DANH SÁCH HĐ TỰ ĐỘNG (CHI TIẾT)";
    
    arrData = [NSMutableArray array];
    record  = [[ReportContractAutoDetailRecord alloc] init];
    
    self.listTableView.hidden = YES;
    
    /*
     *set the custom view for "load more". See DemoTableFooterView.xib.
     */
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DemoTableFooterView" owner:self options:nil];
    footerView = (DemoTableFooterView *)[nib objectAtIndex:0];
    
    /*
     * loading data at page number = 1
     */
    [self loadDataWithPage:@"1"];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadDataWithPage:(NSString *)pageNumber {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    [dict setObject:fromDate forKey:@"FromDate"];
    [dict setObject:toDate forKey:@"ToDate"];
    [dict setObject:status forKey:@"Status"];
    [dict setObject:username forKey:@"UserName"];
    [dict setObject:pageNumber forKey:@"PageNumber"];
    ShareData *shared = [ShareData instance];
    [self showMBProcess];
        [shared.reportlistdeploymentproxy reportCreateObjectDetail:dict completeionHander:^(id result, NSString *errorCode, NSString *message) {
            
            [self hideMBProcess];
            
            NSArray *arr = result;
            [arrData addObjectsFromArray:arr];
            
            if ([message isEqualToString:@"het phien lam viec"]) {
                [self ShowAlertErrorSession];
                [self LogOut];
                return;
            }
            if(![errorCode isEqualToString:@"0"] || result == nil || arrData.count <= 0) {
                [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",@"Không có kết quả trả về"]];
                
            } else {
                record      = [arr objectAtIndex:0];
                currentPage = [record.currentPage integerValue];
                totalPage   = [record.totalPage integerValue];
            }
            
            self.listTableView.hidden = NO;
            [self.listTableView reloadData];
            [self loadMoreCompleted];
            
        } errorHandler:^(NSError *error) {
            [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
            [self hideMBProcess];
            
        }];
        
}

#pragma mark - TableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if(arrData.count > 0 )
        return arrData.count;
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    record = [arrData objectAtIndex:indexPath.section];
    
    NSString *address = record.address;
    NSString *name    = record.fullName;

    CGSize constraint = CGSizeMake(178, 20000.0f);
    
    // constratins the size of the table row according to the text
    CGRect addressRect = [address boundingRectWithSize:constraint options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Light" size:14]} context:nil];
    
    CGRect nameRect = [name boundingRectWithSize:constraint options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica-Light" size:14]} context:nil];
    
    CGFloat addressHeight = MAX(addressRect.size.height, 21);
    CGFloat nameHeight    = MAX(nameRect.size.height, 21);
    
    return 175 +addressHeight +nameHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor  colorWithRed:0.133 green:0.698 blue:0.569 alpha:1];
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *simpleTableIdentifier = @"ReportContractAutoDetailCell";
    ListReportContractAutoDetailViewCell *cell = (ListReportContractAutoDetailViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ListReportContractAutoDetailViewCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    
    if(arrData.count > 0){
        record = [arrData objectAtIndex:indexPath.section];
        
        cell.lblOrderNumber.text = StringFormat(@"%li",(long)indexPath.section + 1);
        cell.lblContract.text    = record.contract;
        cell.lblRegCode.text     = record.regCode;
        cell.lblFullName.text    = record.fullName;
        cell.lblPhoneNumber.text = record.phoneNumber;
        cell.lblStartDate.text   = record.startDate;
        cell.lblAddress.text     = record.address;
        cell.lblStatus.text      = record.statusName;
    }
    return cell;
}

#pragma mark - Scroll view delegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    CGFloat currentOffset = scrollView.contentOffset.y;
    
    CGFloat maximumOffset = fabs(scrollView.contentSize.height - scrollView.frame.size.height);
    if (maximumOffset - currentOffset < 0) {
        [self loadMore];
    }
}

#pragma mark - Load More
//
// The method -loadMore was called and will begin fetching data for the next page (more).
// Do custom handling of -footerView if you need to.
//
- (void) willBeginLoadingMore {
    [footerView.activityIndicator startAnimating];
}

// Do UI handling after the "load more" process was completed. In this example, -footerView will
// show a "No more items to load" text.
//
- (void)loadMoreCompleted {
    [footerView.activityIndicator stopAnimating];
}

- (void)loadMore {
    // Do your async loading here
    self.listTableView.tableFooterView = footerView;
    currentPage ++;
    if (currentPage <= totalPage) {
        [self willBeginLoadingMore];
        [self loadDataWithPage:StringFormat(@"%li",(long)currentPage)];
        return;
    }
    footerView.infoLabel.hidden = NO;
}


@end
