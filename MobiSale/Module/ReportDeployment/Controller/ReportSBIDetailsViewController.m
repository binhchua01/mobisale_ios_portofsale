//
//  ReportSBIDetailsViewController.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 2/4/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ReportSBIDetailsViewController.h"
#import "ListSBIViewCell.h" // Bao cao SBI chi tiet
#import "ContactReportDeploymentRecord.h"
#import "UIPopoverListView.h"
#import "ShareData.h"
#import "KeyValueModel.h"
#import "ReportSBITotal.h"
#import "Common_client.h"

@interface ReportSBIDetailsViewController ()

@end

@implementation ReportSBIDetailsViewController{
    KeyValueModel *selectedPage;
    NSMutableDictionary *ListSBIO;
    int constRepost;
    NSString *nameSaler;
    NSString *keySecleted;
    
}
@dynamic tableView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
     ListSBIO = [NSMutableDictionary dictionary];
    // Do any additional setup after loading the view.
    self.title = @"CHI TIẾT SBI";
    self.screenName=self.title;
    [self LoadData:@"1"];
    self.tableView.separatorColor = [UIColor clearColor];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews{
    float h = 30;
    if(IS_IPHONE4){
        h = 118;
    }
   // self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, [SiUtils getScreenFrameSize].height - 30);
    self.tableView.frame =CGRectMake(0, 62, self.view.frame.size.width, [SiUtils getScreenFrameSize].height  - 90);
}

#pragma mark - TableView


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.arrList.count > 0){
        return self.arrList.count;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 210;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *simpleTableIdentifier = @"ListSBIViewCell";

    ListSBIViewCell *cell = (ListSBIViewCell *)[self.tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
    
    cell = [nib objectAtIndex:0];
    int index=(int)indexPath.row;
    NSDictionary *temp=[self.arrList objectAtIndex:index];
    if(self.arrList.count > 0){
        cell.bl_number.text=[NSString stringWithFormat:@"%d",index+1];
        
     //   if ([[self.arrList objectAtIndex:indexPath.row] objectForKey:@"SBI"] != nil) {
             cell.lb_sbiNumber.text=[temp objectForKey:@"SBI"];
      //  }
     //   if ([[self.arrList objectAtIndex:indexPath.row] objectForKey:@"ReIssuedDate"] != nil) {
             cell.lb_date.text=[temp objectForKey:@"ReIssuedDate"];
      //  }
     //   if ([[self.arrList objectAtIndex:indexPath.row] objectForKey:@"RegCode"] != nil) {
            cell.lb_sgkNumber.text=[temp objectForKey:@"RegCode"];
      //  }
      //  if ([[self.arrList objectAtIndex:indexPath.row] objectForKey:@"StatusName"] != nil) {
            cell.lb_status.text=[temp objectForKey:@"StatusName"];
      //  }
      //  if ([[self.arrList objectAtIndex:indexPath.row] objectForKey:@"Contract"] != nil) {
           cell.lb_HDNumber.text=[temp objectForKey:@"Contract"];
      //  }
      //  if ([[self.arrList objectAtIndex:indexPath.row] objectForKey:@"TypeName"] != nil) {
            cell.lb_SBIType.text=[temp objectForKey:@"TypeName"];
     //   }
        
    }
    
  //  cell.backgroundColor = [UIColor clearColor];
    return cell;
    
}
#pragma mark Table DidSelected
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    return;
}

#pragma mark - DropDown view

- (void)setupDropDownView
{
    
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.listView.scrollEnabled = TRUE;
    [poplistview setTitle:NSLocalizedString(@"Chọn trang", @"")];
    [poplistview show];
    
}
- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35.0f;
}

#pragma mark - UIPopoverListViewDataSource

- (void) setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell{
    KeyValueModel *model;
    model = [self.arrPage objectAtIndex:row];
    cell.textLabel.text = model.Values;
    
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row cell:cell];
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = self.arrPage.count;
    return count;
}

#pragma mark - UIPopoverListViewDelegate

- (void) didDropDownSelected:(NSInteger)row{
    
    selectedPage = [self.arrPage objectAtIndex:row];
  //  [self.btnPage setTitle:selectedPage.Values forState:UIControlStateNormal];
    [self LoadData:selectedPage.Key];
    
}


#pragma mark - UIPopoverListViewDelegate

- (void)popoverListView:(UIPopoverListView *)popoverListView
     didSelectIndexPath:(NSIndexPath *)indexPath
{
    [self didDropDownSelected:indexPath.row];
    
}

#pragma Load Data
-(void)LoadData:(NSString *)pagenumber
{
    [self showMBProcess];
    ShareData *shared =[ShareData instance];
        [shared.appProxy ReportSBIDetail:self.contentDetailsUser Day:shared.daySBI Month:shared.monthSBI Year:shared.yearSBI ReportType:self.contentDetailsType Agent:shared.typeSBI AgentName:shared.contentSBI PageNumber:pagenumber completionHandler:^(id result, NSString *errorCode, NSString *message) {
            [self hideMBProcess];
            if ([message isEqualToString:@"het phien lam viec"]) {
                [self ShowAlertErrorSession];
                [self LogOut];
                return;
            }
            if(![errorCode isEqualToString:@"0"]){
                [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
                return ;
            }

            NSArray *ListObject =[result objectForKey:@"ListObject"];
            if([ListObject count] <1){
                [self showAlertBox:@"Thông báo" message:@"Không có báo cáo SBI"];
                [self.navigationController popViewControllerAnimated:YES];
                    
            }else{
                self.arrList=[result objectForKey:@"ListObject"];
                [self LoadPage:@"1"];
                [self.tableView reloadData];
            }
        } errorHandler:^(NSError *error) {
            [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
            [self hideMBProcess];
            [self.navigationController popViewControllerAnimated:YES];
        }];

 
    
}

#pragma mark - Load page
-(void)LoadPage:(NSString*) page
{
    int pageNumber=[page intValue];
    if (pageNumber <2) {
        [self.btnPage setEnabled:NO];
        [self.btnPage setTitle:@"1/1" forState:UIControlStateNormal];
        return;
    }
    [self.btnPage setEnabled:YES];
    self.arrPage = [NSMutableArray array];
    int i = 1;
    for (i = 1; i <= pageNumber; i++) {
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",i] description:[NSString stringWithFormat:@"%d",i]];
        [self.arrPage addObject: s];
    }
    selectedPage = [self.arrPage objectAtIndex:0];
    NSString*title =[NSString stringWithFormat:@"%@/%@",selectedPage.Values,page];
    [self.btnPage setTitle:title forState:UIControlStateNormal];
}

#pragma mark - event button click
-(IBAction)btnPage_clicked:(id)sender
{
    [self setupDropDownView];
}

-(IBAction)btnCancel_clicked:(id)sender
{
    if(self.delegate){
        [self.delegate CancelPopup];
    }
    [ShareData instance].repostSBIList=nil;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
