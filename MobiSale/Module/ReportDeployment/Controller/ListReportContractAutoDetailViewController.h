//
//  ListReportContractAutoDetailViewController.h
//  MobiSale
//
//  Created by ISC-DanTT on 3/21/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ListReportContractAutoDetailViewController : BaseViewController <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *listTableView;

@property (strong, nonatomic) NSString *username;

@property (strong, nonatomic) NSString *fromDate;

@property (strong, nonatomic) NSString *toDate;

@property (strong, nonatomic) NSString *status;

@end
