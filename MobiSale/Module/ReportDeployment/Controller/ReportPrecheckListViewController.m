//
//  ReportPrecheckListViewController.m
//  MobiSale
//
//  Created by HIEUPC on 4/13/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ReportPrecheckListViewController.h"
#import "ReportPrecheckListCell.h"
#import "Common_client.h"

@interface ReportPrecheckListViewController ()

@end

@implementation ReportPrecheckListViewController{
    KeyValueModel  *selectedPage, *selectedMonth, *selectedYear, *selectedDay;
    bool flagPageNum;
    NSString *totalPage;
}

@dynamic tableView;

enum tableSourceStype {
    Page = 1,
    Month = 2,
    Year = 3,
    Day= 4,
};
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self LoadMonth];
    [self LoadYear];
    [self LoadDay];
    self.tableView.separatorColor = [UIColor clearColor];
    self.title = @"BÁO CÁO PRE-CHECKLIST";
    flagPageNum = FALSE;
    self.screenName=self.title;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews {
    if(IS_IPHONE4){
        self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, self.tableView.frame.size.height - 88);
        self.viewPage.frame = CGRectMake(self.viewPage.frame.origin.x, self.viewPage.frame.origin.y - 88, self.viewPage.frame.size.width, self.viewPage.frame.size.height);
    }
    
}

#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.arrList.count > 0){
        return self.arrList.count;
    }
    return 0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"ReportPrecheckListCell";
    ReportPrecheckListCell *cell = (ReportPrecheckListCell *)[self.tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ReportPrecheckListCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    
    if(self.arrList.count > 0){
        ReportPrcheckListRecord *rc = [self.arrList objectAtIndex:indexPath.row];
        cell.lblName.text = rc.SaleName;
        cell.lblTotal.text = rc.Total;
        cell.lblClose.text = rc.TotalComplete;
        cell.lblInprocess.text = rc.TotalInComplete;
        cell.lblRowNumber.text = rc.RowNumber;
    }
    return cell;
}

#pragma mark - DropDown view
- (void)setupDropDownView:(NSInteger)tag {
    
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.tag = tag;
    poplistview.listView.scrollEnabled = TRUE;
    switch (tag) {
        case Month:
            [poplistview setTitle:NSLocalizedString(@"Chọn tháng", @"")];
            break;
        case Year:
            [poplistview setTitle:NSLocalizedString(@"Chọn năm", @"")];
            break;
        case Page:
            [poplistview setTitle:NSLocalizedString(@"Chọn trang", @"")];
            break;
        case Day:
            [poplistview setTitle:NSLocalizedString(@"Chọn ngày", @"")];
            break;
        default:
            break;
    }
    
    [poplistview show];
    
}
- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 35.0f;
}

#pragma mark - UIPopoverListViewDataSource

- (void) setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell tag:(NSInteger)tag{
    KeyValueModel *model;
    switch (tag) {
        case Month:
            model = [self.arrMonth objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Year:
            model = [self.arrYear objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Page:
            model = [self.arrPage objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Day:
            model = [self.arrDay objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        default:
            break;
    }
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row cell:cell tag:popoverListView.tag];
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section {
    NSInteger count = 0;
    switch (popoverListView.tag) {
        case Month:
            count = self.arrMonth.count;
            break;
        case Year:
            count = self.arrYear.count;
            break;
        case Page:
            count = self.arrPage.count;
            break;
        case Day:
            count = self.arrDay.count;
            break;
        default:
            break;
    }
    return count;
}

#pragma mark - UIPopoverListViewDelegate
- (void) didDropDownSelected:(NSInteger)row tag:(NSInteger)tag {
    switch (tag) {
        case Month:
            selectedMonth = [self.arrMonth objectAtIndex:row];
            [self.btnMonth setTitle:selectedMonth.Values forState:UIControlStateNormal];
            break;
        case Year:
            selectedYear = [self.arrYear objectAtIndex:row];
            [self.btnYear setTitle:selectedYear.Values forState:UIControlStateNormal];
            break;
        case Page:
            selectedPage = [self.arrPage objectAtIndex:row];
            [self.btnPage setTitle:[NSString stringWithFormat:@"%@/%@",selectedPage.Key,totalPage] forState:UIControlStateNormal];
            [self LoadData:selectedPage.Key];
            break;
        case Day:
            selectedDay = [self.arrDay objectAtIndex:row];
            [self.btnDay setTitle:selectedDay.Values forState:UIControlStateNormal];
            break;
        default:
            break;
    }
}


#pragma mark - UIPopoverListViewDelegate

- (void)popoverListView:(UIPopoverListView *)popoverListView didSelectIndexPath:(NSIndexPath *)indexPath {
    [self didDropDownSelected:indexPath.row tag:popoverListView.tag];
    
}

#pragma mark - event button click

-(IBAction)btnPage_clicked:(id)sender {
    [self setupDropDownView:Page];
}

-(IBAction)btnMonth_clicked:(id)sender {
    [self setupDropDownView:Month];
}

-(IBAction)btnYear_clicked:(id)sender {
    [self setupDropDownView:Year];
}

-(IBAction)btnDay_clicked:(id)sender {
    [self setupDropDownView:Day];
}

-(IBAction)btnFind_clicked:(id)sender {
    flagPageNum = FALSE;
    [self LoadData:@"1"];
}

#pragma mark - Load Data
-(void)LoadData:(NSString *)pagenum {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    [self.view endEditing:YES];
    [self showMBProcess];
    ShareData *shared = [ShareData instance];
    [shared.reportlistdeploymentproxy ReportPrecheckList:shared.currentUser.userName Month:selectedMonth.Key Year:selectedYear.Key Day:selectedDay.Key Agent:@"0" AgentName:@"0" PageNumber:pagenum Completehander:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            return ;
        }
        [self.tableView setHidden:NO];
        self.arrList = result;
        if(self.arrList.count > 0){
            ReportPrcheckListRecord *rc = [self.arrList objectAtIndex:0];
            if([pagenum isEqualToString:@"1"]){
                [self LoadPage:[rc.TotalPage intValue]];
                flagPageNum = TRUE;
            }
            totalPage = rc.TotalPage;
            self.lblTotal.text = rc.TotalRow;
            [self.tableView reloadData];
        }else {
            [self.tableView setHidden:YES];
            [self ShowAlertBoxEmpty];
            self.lblTotal.text = @"0";
        }
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}


#pragma mark - Load dropdownlist
-(void)LoadPage:(int)page {
//    if (page <2) {
//        [self.btnPage setTitle:@"1" forState:UIControlStateNormal];
//        return;
//    }
    [self.btnPage setEnabled:YES];
    self.arrPage = [NSMutableArray array];
    int i = 1;
    for (i = 1; i <= page; i++) {
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",i] description:[NSString stringWithFormat:@"%d",i]];
        [self.arrPage addObject: s];
    }
    
    selectedPage = [self.arrPage objectAtIndex:0];
    NSString*title =[NSString stringWithFormat:@"%@/%d",selectedPage.Values,page];
    [self.btnPage setTitle:title forState:UIControlStateNormal];
}

-(void)LoadMonth {
    self.arrMonth = [NSMutableArray array];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM"];
    NSString *MonthString = [formatter stringFromDate:[NSDate date]];
    for (int i = 1; i <= 12; i++) {
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",i] description:[NSString stringWithFormat:@"%d",i]];
        [self.arrMonth addObject: s];
    }
    selectedMonth = [self.arrMonth objectAtIndex:[MonthString intValue] - 1];
    [self.btnMonth setTitle:selectedMonth.Values forState:UIControlStateNormal];
}

-(void)LoadYear {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    NSString *yearString = [formatter stringFromDate:[NSDate date]];
    self.arrYear = [NSMutableArray array];
    int year = [yearString intValue];
    for (int i = 0; i <= 1; i++) {
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",year - i] description:[NSString stringWithFormat:@"%d",year - i]];
        [self.arrYear addObject: s];
    }
    selectedYear= [self.arrYear objectAtIndex:0];
    [self.btnYear setTitle:selectedYear.Values forState:UIControlStateNormal];
}

-(void)LoadDay {
    self.arrDay = [NSMutableArray array];
    for (int i = 0; i <= 31; i++) {
        KeyValueModel *s;
        if(i == 0){
            s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",i] description:[NSString stringWithFormat:@"%@",@"Tất cả"]];
        }else {
            s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",i] description:[NSString stringWithFormat:@"%d",i]];
        }
        
        [self.arrDay addObject: s];
    }
    
    selectedDay= [self.arrDay objectAtIndex:0];
    [self.btnDay setTitle:selectedDay.Values forState:UIControlStateNormal];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
