//
//  ReportListDeploymentRecord.m
//  MobiSale
//
//  Created by HIEUPC on 2/3/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ReportListDeploymentRecord.h"

@implementation ReportListDeploymentRecord

@synthesize ADSL;
@synthesize ADSL_Me;
@synthesize ADSL_Save;
@synthesize ADSL_You;
@synthesize FTI;
@synthesize FTI_Domain;
@synthesize FTI_Hosting;
@synthesize FTI_IPCamera;
@synthesize FTTH;
@synthesize FTTH_Bussiness;
@synthesize FTTH_Diamond;
@synthesize FTTH_Gold;
@synthesize FTTH_Play;
@synthesize FTTH_Plus;
@synthesize FTTH_Public;
@synthesize FTTH_Silver;
@synthesize GPON;
@synthesize GPON_FiberFamily;
@synthesize GPON_FiberHome;
@synthesize GPON_FiberMe;
@synthesize GPON_MegaSave;
@synthesize GPON_MegaYou;
@synthesize OneTV;
@synthesize OneTV_HDB;
@synthesize OneTV_STB;
@synthesize SaleName;
@synthesize CurrentPage;
@synthesize RownNumber;
@synthesize TotalPage;
@synthesize TotalRow;
@synthesize arrADSL;
@synthesize arrFTI;
@synthesize arrFTTH;
@synthesize arrGPON;
@synthesize arrOneTV;

@end
