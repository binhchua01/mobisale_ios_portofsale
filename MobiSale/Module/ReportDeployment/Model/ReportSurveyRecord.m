//
//  ReprotSurveyRecord.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 4/11/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ReportSurveyRecord.h"

@implementation ReportSurveyRecord

@synthesize CabPlus;
@synthesize CabSub;
@synthesize ObjectCabPlus;
@synthesize ObjectCabSub;
@synthesize SaleName;
@synthesize TotalObject;
@synthesize TotalPage;
@synthesize RowNumber;
@synthesize TotalRow;

@end
