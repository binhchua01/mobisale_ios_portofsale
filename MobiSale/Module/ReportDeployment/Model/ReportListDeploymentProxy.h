//
//  ReportListDeploymentProxy.h
//  MobiSale
//
//  Created by HIEUPC on 2/3/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BaseProxy.h"
#import "ReportSalaryRecord.h"

@interface ReportListDeploymentProxy : BaseProxy

- (void)ReportSubscriberGrowthForManager:(NSDictionary *)dict Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)GetSubscriberGrowthObjectList:(NSDictionary *)dict Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)ReportSalary:(NSString *)username Month: (NSString *)month Year:(NSString *)year Day:(NSString *)day Agent:(NSString*)agent AgentName:(NSString*)agentname PageNumber:(NSString *)pagenumber Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)ReportTotalSurvey:(NSString *)username Month: (NSString *)month Year:(NSString *)year Day:(NSString *)day Agent:(NSString*)agent AgentName:(NSString*)agentname PageNumber:(NSString *)pagenumber Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)Reportsurvey:(NSString *)username Month: (NSString *)month Year:(NSString *)year Day:(NSString *)day Agent:(NSString*)agent AgentName:(NSString*)agentname PageNumber:(NSString *)pagenumber Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)ReportPrecheckList:(NSString *)username Month: (NSString *)month Year:(NSString *)year Day:(NSString *)day Agent:(NSString*)agent AgentName:(NSString*)agentname PageNumber:(NSString *)pagenumber Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)ReportRegistration:(NSString *)username Month: (NSString *)month Year:(NSString *)year Day:(NSString *)day Agent:(NSString*)agent AgentName:(NSString*)agentname PageNumber:(NSString *)pagenumber Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getDepartmentList:(NSString *)username completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

/*
 * REPORT POTENTIAL CUSTOMER
 */
- (void)reportPotentialObjTotal:(NSDictionary *)dict completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

/*
 * REPORT CONTRACT AUTO
 */
- (void)reportCreateObjectTotal:(NSDictionary *)dict completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
- (void)reportCreateObjectDetail:(NSDictionary *)dict completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

/*
 * Get info contract from list Report Deployment Subscriber Detail
 */

- (void)getInfoContract:(NSDictionary *)dict completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

/*
 * REPORT PAY TV
 */
- (void)getReportPayTV:(NSDictionary *)dict completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
/*
 * Get report Pay TV Detail
 */
- (void)getReportPayTVDetail:(NSDictionary *)dict completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Report suspend customer
- (void)getReportSuspendCustomer:(NSDictionary *)dict completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Report suspend customer detail
- (void)getReportSuspendCustomerDetail:(NSDictionary *)dict completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
@end
