//
//  ReportSurveyDetailRecord.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 4/11/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ReportSurveyDetailRecord.h"

@implementation ReportSurveyDetailRecord

@synthesize AfterTotalCab;
@synthesize Balance;
@synthesize Contract;
@synthesize FirstTotalCab;
@synthesize FullName;
@synthesize RegCode;
@synthesize RowNumber;
@synthesize TotalRow;
@synthesize TotalPage;

@end
