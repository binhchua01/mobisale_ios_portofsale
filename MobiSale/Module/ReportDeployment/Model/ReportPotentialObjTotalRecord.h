//
//  ReportPotentialObjTotalRecord.h
//  MobiSale
//
//  Created by ISC-DanTT on 3/17/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportPotentialObjTotalRecord : NSObject

@property (strong, nonatomic)NSString *CreateBy;
@property (strong, nonatomic)NSString *CurrentPage;
@property (strong, nonatomic)NSString *RowNumber;
@property (strong, nonatomic)NSString *Total;
@property (strong, nonatomic)NSString *TotalPage;
@property (strong, nonatomic)NSString *TotalRow;

@end
