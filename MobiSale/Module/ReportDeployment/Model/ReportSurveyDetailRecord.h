//
//  ReportSurveyDetailRecord.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 4/11/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportSurveyDetailRecord : NSObject

@property (strong, nonatomic) NSString *AfterTotalCab;
@property (strong, nonatomic) NSString *Balance;
@property (strong, nonatomic) NSString *Contract;
@property (strong, nonatomic) NSString *FirstTotalCab;
@property (strong, nonatomic) NSString *FullName;
@property (strong, nonatomic) NSString *RegCode;
@property (strong, nonatomic) NSString *RowNumber;
@property (strong, nonatomic) NSString *TotalRow;
@property (strong, nonatomic) NSString *TotalPage;

@end
