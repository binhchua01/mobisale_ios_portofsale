//
//  InfoContractRecord.h
//  MobiSale
//
//  Created by ISC-DanTT on 4/22/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InfoContractRecord : NSObject

@property (strong, nonatomic) NSString *contract;       // Số hợp đồng
@property (strong, nonatomic) NSString *fullName;       // Họ tên
@property (strong, nonatomic) NSString *address;        // Điạ chỉ khách hàng
@property (strong, nonatomic) NSString *phone_1;        // Số điện thoại 1
@property (strong, nonatomic) NSString *phone_2;        // Số điện thoại 2
@property (strong, nonatomic) NSString *contact_1;      // Người liên hệ 1
@property (strong, nonatomic) NSString *contact_2;      // Người liên hệ 2
@property (strong, nonatomic) NSString *email;          // email khách hàng
@property (assign, nonatomic) NSInteger localType;      // loại dịch vụ
@property (assign, nonatomic) NSInteger locationID;     // ID câu lệnh khuyến mãi
@property (strong, nonatomic) NSString *regCode;        // mã phiếu đăng ký
@property (strong, nonatomic) NSString *inputTypeName;  // nguồn Phiếu đăng ký
@property (strong, nonatomic) NSString *sourceCreate;   // nguốn lên hợp đồng
@property (strong, nonatomic) NSString *date;           // ngày lên hợp đồng
@property (strong, nonatomic) NSString *name;           // tên khách hàng
@property (strong, nonatomic) NSString *saleName;       // tên sale
@property (strong, nonatomic) NSString *localTypeName;  // Loại dịch vụ
@property (strong, nonatomic) NSString *note;           // ghi chú

// Số lượng Box đã đăng ký
@property (assign, nonatomic) NSInteger boxCount;
// Loại dịch vụ đã đăng ký( 0: internet, 1:IPTV, 2: Internet+IPTV, ngoài ra "Không xác định")
@property (assign, nonatomic) NSInteger serviceType;
// Mô tả loại dịch vụ
@property (strong, nonatomic) NSString *serviceTypeName;
// ID PĐK bán thêm (0: chưa tạo PĐK, > 0: ID PĐK đã tạo trước đó)
@property (assign, nonatomic) NSInteger regIDNew;
// Mã PĐK bán thêm(rỗng: chưa tạo PĐK bán thêm, khác rỗng: cho phép update)
@property (strong, nonatomic) NSString *regCodeNew;
// Tình trạng Combo(0: chưa combo, >0: đã combo)
@property (assign, nonatomic) NSInteger comboStatus;
// Mô tả tình trạng combo
@property (strong, nonatomic) NSString *comboStatusName;
// ID câu lệnh khuyến mãi internet (dùng để lấy câu lệnh khuyến mãi combo/ select lại)
@property (assign, nonatomic) NSInteger promotionID;
// Tên câu lệnh khuyến mãi
@property (strong, nonatomic) NSString *promotionName;

- (id)initWithData:(NSDictionary *)dict;

@end
