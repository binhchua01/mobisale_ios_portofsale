//
//  ReportSBITotal.h
//  
//
//  Created by Thont on 3/12/15.
//  Copyright (c) 2015 FPT. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportSBITotal : NSObject

@property (nonatomic,retain) NSString *ReIssuedDate;
@property (nonatomic,retain) NSString *Contract;
@property (nonatomic,retain) NSString *RegCode;
@property (nonatomic,retain) NSString *SBI;
@property (nonatomic,retain) NSString *NotClose;
@property (nonatomic,retain) NSString *StatusName;
@property (nonatomic,retain) NSString *TypeName;

@end
