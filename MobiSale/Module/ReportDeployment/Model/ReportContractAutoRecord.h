//
//  ReportContractAutoRecord.h
//  MobiSale
//
//  Created by ISC-DanTT on 3/21/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportContractAutoRecord : NSObject

@property (strong, nonatomic)NSString *Name;
@property (strong, nonatomic)NSString *CurrentPage;
@property (strong, nonatomic)NSString *RowNumber;
@property (strong, nonatomic)NSString *Status;
@property (strong, nonatomic)NSString *StatusName;
@property (strong, nonatomic)NSString *Total;
@property (strong, nonatomic)NSString *TotalPage;
@property (strong, nonatomic)NSString *TotalRow;

@end
