//
//  ReportPotentialObjTotalRecord.m
//  MobiSale
//
//  Created by ISC-DanTT on 3/17/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ReportPotentialObjTotalRecord.h"

@implementation ReportPotentialObjTotalRecord

@synthesize CreateBy, CurrentPage, RowNumber, Total, TotalPage, TotalRow;

@end
