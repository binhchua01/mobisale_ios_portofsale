//
//  ReportContractAutoDetailRecord.m
//  MobiSale
//
//  Created by ISC-DanTT on 3/21/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ReportContractAutoDetailRecord.h"

@implementation ReportContractAutoDetailRecord

@synthesize address, contract, currentPage, fullName, phoneNumber, regCode, regID, rowNumber, saleName, startDate, statusName, totalPage, totalRow;

@end
