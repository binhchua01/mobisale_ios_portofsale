//
//  ReportListDeploymentRecord.h
//  MobiSale
//
//  Created by HIEUPC on 2/3/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportListDeploymentRecord : NSObject

@property (nonatomic,retain) NSString *ADSL;
@property (nonatomic,retain) NSString *ADSL_Me;
@property (nonatomic,retain) NSString *ADSL_Save;
@property (nonatomic,retain) NSString *ADSL_You;
@property (nonatomic,retain) NSString *FTI;
@property (nonatomic,retain) NSString *FTI_CA;
@property (nonatomic,retain) NSString *FTI_Domain;
@property (nonatomic,retain) NSString *FTI_Hosting;
@property (nonatomic,retain) NSString *FTI_IPCamera;
@property (nonatomic,retain) NSString *FTTH;
@property (nonatomic,retain) NSString *FTTH_Bussiness;
@property (nonatomic,retain) NSString *FTTH_Diamond;
@property (nonatomic,retain) NSString *FTTH_Gold;
@property (nonatomic,retain) NSString *FTTH_Play;
@property (nonatomic,retain) NSString *FTTH_Plus;
@property (nonatomic,retain) NSString *FTTH_Public;
@property (nonatomic,retain) NSString *FTTH_Silver;
@property (nonatomic,retain) NSString *GPON;
@property (nonatomic,retain) NSString *GPON_FiberFamily;
@property (nonatomic,retain) NSString *GPON_FiberHome;
@property (nonatomic,retain) NSString *GPON_FiberMe;
@property (nonatomic,retain) NSString *GPON_MegaSave;
@property (nonatomic,retain) NSString *GPON_MegaYou;
@property (nonatomic,retain) NSString *OneTV;
@property (nonatomic,retain) NSString *OneTV_HDB;
@property (nonatomic,retain) NSString *OneTV_STB;
@property (nonatomic,retain) NSString *SaleName;
@property (nonatomic,retain) NSString *CurrentPage;
@property (nonatomic,retain) NSString *RownNumber;
@property (nonatomic,retain) NSString *TotalPage;
@property (nonatomic,retain) NSString *TotalRow;
@property (nonatomic,retain) NSMutableArray *arrADSL;
@property (nonatomic,retain) NSMutableArray *arrFTI;
@property (nonatomic,retain) NSMutableArray *arrFTTH;
@property (nonatomic,retain) NSMutableArray *arrGPON;
@property (nonatomic,retain) NSMutableArray *arrOneTV;


@end
