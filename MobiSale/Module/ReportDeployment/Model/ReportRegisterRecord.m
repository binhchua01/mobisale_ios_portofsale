//
//  ReportRegisterRecord.m
//  MobiSale
//
//  Created by HIEUPC on 4/13/15.
//  Copyright (c) 2015 FPT.RAD.MOBISALE. All rights reserved.
//

#import "ReportRegisterRecord.h"

@implementation ReportRegisterRecord

@synthesize SaleName;
@synthesize Total;
@synthesize TotalComplete;
@synthesize TotalInComplete;
@synthesize TotalPage;
@synthesize TotalRow;

@end
