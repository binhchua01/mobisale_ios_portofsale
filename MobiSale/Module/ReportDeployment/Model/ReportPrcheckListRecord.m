//
//  ReportPrcheckListRecord.m
//  MobiSale
//
//  Created by HIEUPC on 4/13/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ReportPrcheckListRecord.h"

@implementation ReportPrcheckListRecord

@synthesize RowNumber;
@synthesize TotalRow;
@synthesize Total;
@synthesize TotalComplete;
@synthesize TotalInComplete;
@synthesize TotalPage;

@end
