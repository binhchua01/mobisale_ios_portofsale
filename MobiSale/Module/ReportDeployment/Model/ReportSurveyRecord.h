//
//  ReprotSurveyRecord.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 4/11/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportSurveyRecord : NSObject

@property (strong, nonatomic) NSString *CabPlus;
@property (strong, nonatomic) NSString *CabSub;
@property (strong, nonatomic) NSString *ObjectCabPlus;
@property (strong, nonatomic) NSString *ObjectCabSub;
@property (strong, nonatomic) NSString *SaleName;
@property (strong, nonatomic) NSString *TotalObject;
@property (strong, nonatomic) NSString *TotalPage;
@property (strong, nonatomic) NSString *RowNumber;
@property (strong, nonatomic) NSString *TotalRow;
//

@end
