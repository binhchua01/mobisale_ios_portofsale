//
//  ReportRegisterRecord.h
//  MobiSale
//
//  Created by HIEUPC on 4/13/15.
//  Copyright (c) 2015 FPT.RAD.MOBISALE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportRegisterRecord : NSObject

@property (strong, nonatomic)NSString *RowNumber;
@property (strong, nonatomic)NSString *SaleName;
@property (strong, nonatomic)NSString *Total;
@property (strong, nonatomic)NSString *TotalComplete;
@property (strong, nonatomic)NSString *TotalInComplete;
@property (strong, nonatomic)NSString *TotalPage;
@property (strong, nonatomic)NSString *TotalRow;

@end
