//
//  ReportSalaryRecord.m
//  MobiSale
//
//  Created by HIEUPC on 4/10/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ReportSalaryRecord.h"

@implementation ReportSalaryRecord

@synthesize Name;
@synthesize FullName;
@synthesize Branch;
@synthesize Dept;
@synthesize DateSalary;
@synthesize BasicSalary;
@synthesize MonthCommission;
@synthesize PrepaidNew;
@synthesize IncomeSalary;
@synthesize TPlusTotal;
@synthesize TMinTotal;
@synthesize BoxTPlusTotal;
@synthesize BoxTMinTotal;
@synthesize NetSalary;
//
@synthesize TotalPage;
@synthesize TotalRow;
@synthesize Currentpage;
@synthesize RowNumber;
@synthesize Html;

@end
