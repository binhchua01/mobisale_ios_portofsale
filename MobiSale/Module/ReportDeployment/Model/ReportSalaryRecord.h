//
//  ReportSalaryRecord.h
//  MobiSale
//
//  Created by HIEUPC on 4/10/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportSalaryRecord : NSObject

@property (nonatomic, strong) NSString *Name;
@property (nonatomic, strong) NSString *FullName;
@property (nonatomic, strong) NSString *Branch;
@property (nonatomic, strong) NSString *Dept;
@property (nonatomic, strong) NSString *DateSalary;
@property (nonatomic, strong) NSString *BasicSalary;
@property (nonatomic, strong) NSString *MonthCommission;
@property (nonatomic, strong) NSString *PrepaidNew;
@property (nonatomic, strong) NSString *IncomeSalary;
@property (nonatomic, strong) NSString *TPlusTotal;
@property (nonatomic, strong) NSString *TMinTotal;
@property (nonatomic, strong) NSString *BoxTPlusTotal;
@property (nonatomic, strong) NSString *BoxTMinTotal;
@property (nonatomic, strong) NSString *NetSalary;

//vutt11
@property (nonatomic, strong) NSString *TotalPage;
@property (nonatomic, strong) NSString *TotalRow;
@property (nonatomic, strong) NSString *Currentpage;
@property (nonatomic, strong) NSString *RowNumber;
@property (nonatomic, strong) NSString *Html;

@end
