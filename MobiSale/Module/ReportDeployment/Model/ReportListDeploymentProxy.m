//
//  ReportListDeploymentProxy.m
//  MobiSale
//
//  Created by HIEUPC on 2/3/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ReportListDeploymentProxy.h"
#import "ReportListDeploymentRecord.h"
#import "ContactReportDeploymentRecord.h"
#import "ReportSurveyRecord.h"
#import "ReportSurveyDetailRecord.h"
#import "ReportPrcheckListRecord.h"
#import "ReportRegisterRecord.h"
#import "ReportPotentialObjTotalRecord.h"
#import "ReportContractAutoRecord.h"
#import "ReportContractAutoDetailRecord.h"
#import "KeyValueModel.h"
#import "../../../Client/ShareData.h"
#import "InfoContractRecord.h"
#import "ReportPayTVRecord.h"
#import "ReportPayTVDetailRecord.h"

// Báo cáo Khách hàng rời mạng
#import "ReportSuspendCustomerModel.h"
#import "ReportSuspendCustomerDetailModel.h"
#import "FPTUtility.h"

@implementation ReportListDeploymentProxy

#define mReportSubscriberGrowthForManager       @"ReportSubscriberGrowthForManager"
#define mGetSubscriberGrowthObjectList          @"GetSubscriberGrowthObjectList"
#define mGetReportSalary                        @"ReportSalary"
#define mGetReportTotalSurvey                   @"ReportTotalSurvey"
#define mGetReportsurvey                        @"Reportsurvey"
#define mGetReportPrecheckList                  @"ReportPrecheckList"
#define mGetReportRegistration                  @"ReportRegistration"
#define mGetDeptList                            @"GetDeptList"
#define mReportPotentialObjTotal                @"ReportPotentialObjTotal"
#define mReportCreateObjectTotal                @"ReportCreateObjectTotal"
#define mReportCreateObjectDetail               @"ReportCreateObjectDetail"
#define mGetObjectDetail                        @"GetObjectDetail"
#define mGetReportPayTV                         @"GetReportPayTV"
#define mGetReportPayTVDetail                   @"GetReportPayTVDetail"
#define mReportSuspendCustomer                  @"ReportSuspendCustomer"
#define mReportSuspendCustomerDetail            @"ReportSuspendCustomerDetail"

#pragma mark - Get List report deployment
- (void)ReportSubscriberGrowthForManager:(NSDictionary *)dict Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mReportSubscriberGrowthForManager)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];

    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endReportSubscriberGrowthForManager:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
   
}

- (void)endReportSubscriberGrowthForManager:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }

    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(dict == nil){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]?:nil);
    
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, ErrorCode);
        return;
    }
 
     NSArray *ListObject =[dict objectForKey:@"ListObject"];
     handler(ListObject, ErrorCode, @"");
    
}

#pragma mark - Get List detail report deployment
- (void)GetSubscriberGrowthObjectList:(NSDictionary *)dict Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetSubscriberGrowthObjectList)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];

    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetSubscriberGrowthObjectList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}
- (void)endGetSubscriberGrowthObjectList:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }

    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *root = [jsonDict objectForKey:@"ResponseResult"];
    
    NSString *ErrorCode = StringFormat(@"%@",[root objectForKey:@"ErrorCode"]);
    
    
    if(![ErrorCode isEqualToString:@"0"]){
        NSString *message = StringFormat(@"%@",[root objectForKey:@"Error"]); ;
        handler(nil, ErrorCode, message);
        return;
    }
 
    NSArray *arr = [root objectForKey:@"ListObject"]?:nil;
    if(arr.count <= 0){
        handler(nil, @"", @"");
        return;
    }

    handler(arr, ErrorCode, @"");
}


#pragma mark - Get List report salary
- (void)ReportSalary:(NSString *)username Month: (NSString *)month Year:(NSString *)year Day:(NSString *)day Agent:(NSString*)agent AgentName:(NSString*)agentname PageNumber:(NSString *)pagenumber Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetReportSalary)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:username forKey:@"UserName"];
    [dict setObject:day forKey:@"Day"];
    [dict setObject:month forKey:@"Month"];
    [dict setObject:year forKey:@"Year"];
    [dict setObject:agent forKey:@"Agent"];
    [dict setObject:agentname forKey:@"AgentName"];
    [dict setObject:pagenumber forKey:@"PageNumber"];
    [dict setObject:@"1" forKey:@"Version"];
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];

    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endReportSalary:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
    
    
    
}
- (void)endReportSalary:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }

    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(dict == nil){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]?:nil);
    
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, ErrorCode);
        return;
    }
  
    NSArray *arr = [dict objectForKey:@"Object"];
   // NSMutableArray *mArray = [NSMutableArray array];
//    for(NSDictionary *d in arr){
//        ReportSalaryRecord *p = [self ParseRecordReportSalary:d];
//        [mArray addObject:p];
//    }
    handler(arr, ErrorCode, @"");
}

-(ReportSalaryRecord *) ParseRecordReportSalary:(NSDictionary *)dict{
    ReportSalaryRecord *rc = [[ReportSalaryRecord alloc]init];
    
//    rc.Name = StringFormat(@"%@",[dict objectForKey:@"Name"]);
//    rc.FullName = StringFormat(@"%@",[dict objectForKey:@"FullName"]);
//    rc.Branch = StringFormat(@"%@",[dict objectForKey:@"CN"]);
//    rc.Dept = StringFormat(@"%@",[dict objectForKey:@"Dept"]);
//    rc.DateSalary = StringFormat(@"%@",[dict objectForKey:@"DateSalary"]);
//    rc.BasicSalary = StringFormat(@"%@",[dict objectForKey:@"BasicSalary"]);
//    rc.MonthCommission = StringFormat(@"%@",[dict objectForKey:@"MonthCommission"]);
//    rc.PrepaidNew = StringFormat(@"%@",[dict objectForKey:@"PrepaidNew"]);
//    rc.IncomeSalary = StringFormat(@"%@",[dict objectForKey:@"IncomeSalary"]);
//    rc.TPlusTotal = StringFormat(@"%@",[dict objectForKey:@"TPlusTotal"]);
//    rc.TMinTotal = StringFormat(@"%@",[dict objectForKey:@"TMinTotal"]);
//    rc.BoxTPlusTotal = StringFormat(@"%@",[dict objectForKey:@"BoxTPlusTotal"]);
//    rc.BoxTMinTotal = StringFormat(@"%@",[dict objectForKey:@"BoxTMinTotal"]);
//    rc.NetSalary = StringFormat(@"%@",[dict objectForKey:@"NetSalary"]);
    //VUTT11
    rc.Currentpage = StringFormat(@"%@",[dict objectForKey:@"CurrentPage"]);
    rc.TotalPage = StringFormat(@"%@",[dict objectForKey:@"TotalPage"]);
    rc.RowNumber   = StringFormat(@"%@",[dict objectForKey:@"RowNumber"]);
    rc.Html        = StringFormat(@"%@",[dict objectForKey:@"Html"]);
    rc.TotalRow = StringFormat(@"%@",[dict objectForKey:@"TotalRow"]);
   
    return rc;
}

#pragma mark - Get List report survey
- (void)ReportTotalSurvey:(NSString *)username Month: (NSString *)month Year:(NSString *)year Day:(NSString *)day Agent:(NSString*)agent AgentName:(NSString*)agentname PageNumber:(NSString *)pagenumber Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetReportTotalSurvey)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:username forKey:@"UserName"];
    [dict setObject:day forKey:@"Day"];
    [dict setObject:month forKey:@"Month"];
    [dict setObject:year forKey:@"Year"];
    [dict setObject:agent forKey:@"Agent"];
    [dict setObject:agentname forKey:@"AgentName"];
    [dict setObject:pagenumber forKey:@"PageNumber"];
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];

    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endReportTotalSurvey:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}
- (void)endReportTotalSurvey:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }

    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(dict == nil){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]?:nil);
    
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, ErrorCode);
        return;
    }

    NSArray *arr = [dict objectForKey:@"ListObject"];
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        ReportSurveyRecord *p = [self ParseRecordReportSurvey:d];
        [mArray addObject:p];
    }
    handler(mArray, ErrorCode, @"");
    
    
}

-(ReportSurveyRecord *) ParseRecordReportSurvey:(NSDictionary *)dict{
    ReportSurveyRecord *rc = [[ReportSurveyRecord alloc]init];
    
    rc.CabPlus = StringFormat(@"%@",[dict objectForKey:@"CabPlus"]);
    rc.CabSub = StringFormat(@"%@",[dict objectForKey:@"CabSub"]);
    rc.ObjectCabPlus = StringFormat(@"%@",[dict objectForKey:@"ObjectCabPlus"]);
    rc.ObjectCabSub = StringFormat(@"%@",[dict objectForKey:@"ObjectCabSub"]);
    rc.SaleName = StringFormat(@"%@",[dict objectForKey:@"SaleName"]);
    rc.TotalObject = StringFormat(@"%@",[dict objectForKey:@"TotalObject"]);
    rc.TotalPage = StringFormat(@"%@",[dict objectForKey:@"TotalPage"]);
    rc.RowNumber = StringFormat(@"%@",[dict objectForKey:@"RowNumber"]);
    rc.TotalRow = StringFormat(@"%@",[dict objectForKey:@"TotalRow"]);
    
    return rc;
}

#pragma mark - Get List report survey detail
- (void)Reportsurvey:(NSString *)username Month: (NSString *)month Year:(NSString *)year Day:(NSString *)day Agent:(NSString*)agent AgentName:(NSString*)agentname PageNumber:(NSString *)pagenumber Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetReportsurvey)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:username forKey:@"UserName"];
    [dict setObject:day forKey:@"Day"];
    [dict setObject:month forKey:@"Month"];
    [dict setObject:year forKey:@"Year"];
    [dict setObject:agent forKey:@"Agent"];
    [dict setObject:agentname forKey:@"AgentName"];
    [dict setObject:pagenumber forKey:@"PageNumber"];
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];

    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endReportsurvey:result response:res completionHandler:handler errorHandler:errHandler];
    };
    

    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}
- (void)endReportsurvey:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }

    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(dict == nil){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]?:nil);
    
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, ErrorCode);
        return;
    }

    NSArray *arr = [dict objectForKey:@"ListObject"];
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        ReportSurveyDetailRecord *p = [self ParseRecordReportSurveyDetail:d];
        [mArray addObject:p];
    }
    handler(mArray, ErrorCode, @"");
    
    
}

-(ReportSurveyDetailRecord *) ParseRecordReportSurveyDetail:(NSDictionary *)dict{
    ReportSurveyDetailRecord *rc = [[ReportSurveyDetailRecord alloc]init];
    
    rc.AfterTotalCab = StringFormat(@"%@",[dict objectForKey:@"AfterTotalCab"]);
    rc.Balance = StringFormat(@"%@",[dict objectForKey:@"Balance"]);
    rc.Contract = StringFormat(@"%@",[dict objectForKey:@"Contract"]);
    rc.FirstTotalCab = StringFormat(@"%@",[dict objectForKey:@"FirstTotalCab"]);
    rc.FullName = StringFormat(@"%@",[dict objectForKey:@"FullName"]);
    rc.RegCode = StringFormat(@"%@",[dict objectForKey:@"RegCode"]);
    rc.TotalPage = StringFormat(@"%@",[dict objectForKey:@"TotalPage"]);
    rc.RowNumber = StringFormat(@"%@",[dict objectForKey:@"RowNumber"]);
    rc.TotalRow = StringFormat(@"%@",[dict objectForKey:@"TotalRow"]);
    return rc;
}

#pragma mark - Get List report prechecklist
- (void)ReportPrecheckList:(NSString *)username Month: (NSString *)month Year:(NSString *)year Day:(NSString *)day Agent:(NSString*)agent AgentName:(NSString*)agentname PageNumber:(NSString *)pagenumber Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetReportPrecheckList)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:username forKey:@"UserName"];
    [dict setObject:day forKey:@"Day"];
    [dict setObject:month forKey:@"Month"];
    [dict setObject:year forKey:@"Year"];
    [dict setObject:agent forKey:@"Agent"];
    [dict setObject:agentname forKey:@"AgentName"];
    [dict setObject:pagenumber forKey:@"PageNumber"];
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];

    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endReportPrecheckList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
   
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}
- (void)endReportPrecheckList:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }

    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(dict == nil){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]?:nil);
    
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, ErrorCode);
        return;
    }

    NSArray *arr = [dict objectForKey:@"ListObject"];
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        ReportPrcheckListRecord *p = [self ParseRecordReportPrechecklist:d];
        [mArray addObject:p];
    }
    handler(mArray, ErrorCode, @"");
    
    
}

-(ReportPrcheckListRecord *) ParseRecordReportPrechecklist:(NSDictionary *)dict{
    ReportPrcheckListRecord *rc = [[ReportPrcheckListRecord alloc]init];
    
    rc.SaleName = StringFormat(@"%@",[dict objectForKey:@"SaleName"]);
    rc.Total = StringFormat(@"%@",[dict objectForKey:@"Total"]);
    rc.TotalComplete = StringFormat(@"%@",[dict objectForKey:@"TotalComplete"]);
    rc.TotalInComplete = StringFormat(@"%@",[dict objectForKey:@"TotalInComplete"]);
    rc.TotalPage = StringFormat(@"%@",[dict objectForKey:@"TotalPage"]);
    rc.RowNumber = StringFormat(@"%@",[dict objectForKey:@"RowNumber"]);
    rc.TotalRow = StringFormat(@"%@",[dict objectForKey:@"TotalRow"]);
    return rc;
}

#pragma mark - Get List report register
- (void)ReportRegistration:(NSString *)username Month: (NSString *)month Year:(NSString *)year Day:(NSString *)day Agent:(NSString*)agent AgentName:(NSString*)agentname PageNumber:(NSString *)pagenumber Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetReportRegistration)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:username forKey:@"UserName"];
    [dict setObject:day forKey:@"Day"];
    [dict setObject:month forKey:@"Month"];
    [dict setObject:year forKey:@"Year"];
    [dict setObject:agent forKey:@"Agent"];
    [dict setObject:agentname forKey:@"AgentName"];
    [dict setObject:pagenumber forKey:@"PageNumber"];
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];

    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endReportRegistration:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
  
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}
- (void)endReportRegistration:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetItemsBlock)handler errorHandler:(ErrorBlock)errHandler {
    // NSString *string = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }

    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(dict == nil){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]?:nil);
    
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, ErrorCode);
        return;
    }

    NSArray *arr = [dict objectForKey:@"ListObject"];
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        ReportRegisterRecord *p = [self ParseRecordReportRegister:d];
        [mArray addObject:p];
    }
    handler(mArray, ErrorCode, @"");
    
    
}

-(ReportRegisterRecord *) ParseRecordReportRegister:(NSDictionary *)dict{
    ReportRegisterRecord *rc = [[ReportRegisterRecord alloc]init];
    
    rc.SaleName = StringFormat(@"%@",[dict objectForKey:@"SaleName"]);
    rc.Total = StringFormat(@"%@",[dict objectForKey:@"Total"]);
    rc.TotalComplete = StringFormat(@"%@",[dict objectForKey:@"TotalComplete"]);
    rc.TotalInComplete = StringFormat(@"%@",[dict objectForKey:@"TotalInComplete"]);
    rc.TotalPage = StringFormat(@"%@",[dict objectForKey:@"TotalPage"]);
    rc.RowNumber = StringFormat(@"%@",[dict objectForKey:@"RowNumber"]);
    rc.TotalRow = StringFormat(@"%@",[dict objectForKey:@"TotalRow"]);
    return rc;
}

- (void)getDepartmentList:(NSString *)username completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetDeptList)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:username forKey:@"UserName"];
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetDepartmentList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
  
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endGetDepartmentList:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
   
//    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(dict == nil){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]?:nil);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, ErrorCode);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    handler(arr, ErrorCode, @"");
}

/*
 * REPORT POTENTIAL CUSTOMER
 */
- (void)reportPotentialObjTotal:(NSDictionary *)dict completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mReportPotentialObjTotal)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endReportPotentialObjTotal:result response:res completionHandler:handler errorHandler:errHandler];
    };
  
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    
    
    [callOp start];
}

- (void)endReportPotentialObjTotal:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(dict == nil){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]?:nil);
    NSString *Error = StringFormat(@"%@",[dict objectForKey:@"Error"]?:nil);

    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, Error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        ReportPotentialObjTotalRecord *p = [self ParseRecordReportPotential:d];
        [mArray addObject:p];
    }
    handler(mArray, ErrorCode, @"");
}

-(ReportPotentialObjTotalRecord *) ParseRecordReportPotential:(NSDictionary *)dict{
    ReportPotentialObjTotalRecord *rc = [[ReportPotentialObjTotalRecord alloc]init];
    
    rc.CreateBy     = StringFormat(@"%@",[dict objectForKey:@"CreateBy"]);
    rc.CurrentPage  = StringFormat(@"%@",[dict objectForKey:@"CurrentPage"]);
    rc.Total        = StringFormat(@"%@",[dict objectForKey:@"Total"]);
    rc.TotalPage    = StringFormat(@"%@",[dict objectForKey:@"TotalPage"]);
    rc.RowNumber    = StringFormat(@"%@",[dict objectForKey:@"RowNumber"]);
    rc.TotalRow     = StringFormat(@"%@",[dict objectForKey:@"TotalRow"]);
    return rc;
}

/*
 * REPORT CONTRACT AUTO
 */
- (void)reportCreateObjectTotal:(NSDictionary *)dict completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mReportCreateObjectTotal)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endReportCreateObjectTotal:result response:res completionHandler:handler errorHandler:errHandler];
    };
   
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endReportCreateObjectTotal:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(dict == nil){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]?:nil);
    NSString *Error = StringFormat(@"%@",[dict objectForKey:@"Error"]?:nil);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, Error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        ReportContractAutoRecord *p = [self ParseRecordReportContractAuto:d];
        [mArray addObject:p];
    }
    handler(mArray, ErrorCode, @"");
}

-(ReportContractAutoRecord *) ParseRecordReportContractAuto:(NSDictionary *)dict{
    ReportContractAutoRecord *rc = [[ReportContractAutoRecord alloc]init];
    
    rc.Name         = StringFormat(@"%@",[dict objectForKey:@"Name"]);
    rc.CurrentPage  = StringFormat(@"%@",[dict objectForKey:@"CurrentPage"]);
    rc.Total        = StringFormat(@"%@",[dict objectForKey:@"Total"]);
    rc.Status       = StringFormat(@"%@",[dict objectForKey:@"Status"]);
    rc.StatusName   = StringFormat(@"%@",[dict objectForKey:@"StatusName"]);
    rc.TotalPage    = StringFormat(@"%@",[dict objectForKey:@"TotalPage"]);
    rc.RowNumber    = StringFormat(@"%@",[dict objectForKey:@"RowNumber"]);
    rc.TotalRow     = StringFormat(@"%@",[dict objectForKey:@"TotalRow"]);
    
    return rc;
}

- (void)reportCreateObjectDetail:(NSDictionary *)dict completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mReportCreateObjectDetail)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endReportCreateObjectDetail:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
   
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endReportCreateObjectDetail:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(dict == nil){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]?:nil);
    NSString *Error = StringFormat(@"%@",[dict objectForKey:@"Error"]?:nil);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, Error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        ReportContractAutoDetailRecord *p = [self ParseRecordReportContractAutoDetail:d];
        [mArray addObject:p];
    }
    handler(mArray, ErrorCode, @"");
}

-(ReportContractAutoDetailRecord *) ParseRecordReportContractAutoDetail:(NSDictionary *)dict{
    ReportContractAutoDetailRecord *rc = [[ReportContractAutoDetailRecord alloc]init];
    
    rc.address     = StringFormat(@"%@",[dict objectForKey:@"Address"]);
    rc.contract    = StringFormat(@"%@",[dict objectForKey:@"Contract"]);
    rc.currentPage = StringFormat(@"%@",[dict objectForKey:@"CurrentPage"]);
    rc.fullName    = StringFormat(@"%@",[dict objectForKey:@"FullName"]);
    rc.phoneNumber = StringFormat(@"%@",[dict objectForKey:@"PhoneNumber"]);
    rc.regCode     = StringFormat(@"%@",[dict objectForKey:@"RegCode"]);
    rc.regID       = StringFormat(@"%@",[dict objectForKey:@"RegID"]);
    rc.rowNumber   = StringFormat(@"%@",[dict objectForKey:@"RowNumber"]);
    rc.saleName    = StringFormat(@"%@",[dict objectForKey:@"SaleName"]);
    rc.startDate   = StringFormat(@"%@",[dict objectForKey:@"Start_Date"]);
    rc.statusName  = StringFormat(@"%@",[dict objectForKey:@"StatusName"]);
    rc.totalPage   = StringFormat(@"%@",[dict objectForKey:@"TotalPage"]);
    rc.totalRow    = StringFormat(@"%@",[dict objectForKey:@"TotalRow"]);
    return rc;


}
/*
 * Get info contract from list Report Deployment Subscriber Detail
 */

- (void)getInfoContract:(NSDictionary *)dict completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetObjectDetail)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetInfoContract:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
   
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endGetInfoContract:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(dict == nil){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]?:nil);
    NSString *Error = StringFormat(@"%@",[dict objectForKey:@"Error"]?:nil);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, Error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    NSDictionary *d = arr[0];
    InfoContractRecord *p = [[InfoContractRecord alloc] initWithData:d];
    handler(p, ErrorCode, @"");

}


/*
 * Get report Pay TV
 */

- (void)getReportPayTV:(NSDictionary *)dict completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetReportPayTV)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetReportPayTV:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endGetReportPayTV:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(dict == nil){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]?:nil);
    NSString *Error = StringFormat(@"%@",[dict objectForKey:@"Error"]?:nil);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, Error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        ReportPayTVRecord *p = [ReportPayTVRecord reportPayTVRecordWithDict:d];
        [mArray addObject:p];
    }

    handler(mArray, ErrorCode, @"");
    
}

/*
 * Get report Pay TV Detail
 */

- (void)getReportPayTVDetail:(NSDictionary *)dict completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mGetReportPayTVDetail)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetReportPayTVDetail:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endGetReportPayTVDetail:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(dict == nil){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]?:nil);
    NSString *Error = StringFormat(@"%@",[dict objectForKey:@"Error"]?:nil);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, Error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        ReportPayTVDetailRecord *p = [ReportPayTVDetailRecord reportPayTVDetailRecordWithDict:d];
        [mArray addObject:p];
    }
    
    handler(mArray, ErrorCode, @"");
    
}
/*
 * Get report Suspend Customer
 */

- (void)getReportSuspendCustomer:(NSDictionary *)dict completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mReportSuspendCustomer)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetReportSuspendCutomer:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endGetReportSuspendCutomer:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(dict == nil){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]?:nil);
    NSString *Error = StringFormat(@"%@",[dict objectForKey:@"Error"]?:nil);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, Error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        ReportSuspendCustomerModel *p = [[ReportSuspendCustomerModel alloc] initWithDictionary:d];
        [mArray addObject:p];
    }
    
    handler(mArray, ErrorCode, @"");
    
}

/*
 * Get report Suspend Customer Detail
 */

- (void)getReportSuspendCustomerDetail:(NSDictionary *)dict completeionHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, mReportSuspendCustomerDetail)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    
    
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetReportSuspendCustomerDetail:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endGetReportSuspendCustomerDetail:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *dict = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    
    if(dict == nil){
        handler(nil, @"", @"");
        return;
    }
    
    NSString *ErrorCode = StringFormat(@"%@",[dict objectForKey:@"ErrorCode"]?:nil);
    NSString *Error = StringFormat(@"%@",[dict objectForKey:@"Error"]?:nil);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, Error);
        return;
    }
    
    NSArray *arr = [dict objectForKey:@"ListObject"];
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in arr){
        ReportSuspendCustomerDetailModel *p = [[ReportSuspendCustomerDetailModel alloc] initWithDictionary:d];
        [mArray addObject:p];
    }
    
    handler(mArray, ErrorCode, @"");
    
}


@end
