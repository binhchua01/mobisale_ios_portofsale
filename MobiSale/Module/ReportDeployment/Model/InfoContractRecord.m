//
//  InfoContractRecord.m
//  MobiSale
//
//  Created by ISC-DanTT on 4/22/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "InfoContractRecord.h"
#import "AppDelegate.h"
#import "ShareData.h"
@implementation InfoContractRecord

- (id)initWithData:(NSDictionary *)dict {
    @try {
        self.contract       = dict[@"Contract"];
        self.fullName       = dict[@"FullName"];
        self.address        = dict[@"Address"];
        self.phone_1        = dict[@"Phone_1"];
        self.phone_2        = dict[@"Phone_2"];
        self.contact_1      = dict[@"Contact_1"];
        self.contact_2      = dict[@"Contact_2"];
        self.email          = dict[@"Email"];
        self.localType      = [dict[@"LocalType"] integerValue];
        self.locationID     = [dict[@"LocationID"] integerValue];
        self.regCode        = dict[@"RegCode"];
        self.inputTypeName  = dict[@"InputTypeName"];
        self.sourceCreate   = dict[@"SourceCreate"];
        self.date           = dict[@"Date"];
        self.name           = dict[@"Name"];
        self.saleName       = dict[@"SaleName"];
        self.localTypeName  = dict[@"LocalTypeName"];
        self.note           = dict[@"Note"];
        self.boxCount       = [dict[@"BoxCount"] integerValue];
        self.serviceType    = [dict[@"ServiceType"] integerValue];
        self.serviceTypeName = dict[@"ServiceTypeName"];
        self.regIDNew       = [dict[@"RegIDNew"] integerValue];
        self.regCodeNew     = dict[@"RegCodeNew"];
        self.comboStatus    = [dict[@"ComboStatus"] integerValue];
        self.comboStatusName = dict[@"ComboStatusName"];
        self.promotionID    = [dict[@"PromotionID"] integerValue];
        self.promotionName  = dict[@"PromotionName"];
        
        return self;
        
    } @catch (NSException *exception) {
        NSLog(@"%@", exception.description);
         [CrashlyticsKit recordCustomExceptionName:@"InfoContractRecord - funtion (saveReceiveRemoteNotification)-Error handle InfoContractReCord" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
        return nil;
    }
}

@end
