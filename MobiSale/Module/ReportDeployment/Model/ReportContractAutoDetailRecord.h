//
//  ReportContractAutoDetailRecord.h
//  MobiSale
//
//  Created by ISC-DanTT on 3/21/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReportContractAutoDetailRecord : NSObject

@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *contract;
@property (strong, nonatomic) NSString *currentPage;
@property (strong, nonatomic) NSString *fullName;
@property (strong, nonatomic) NSString *phoneNumber;
@property (strong, nonatomic) NSString *regCode;
@property (strong, nonatomic) NSString *regID;
@property (strong, nonatomic) NSString *rowNumber;
@property (strong, nonatomic) NSString *saleName;
@property (strong, nonatomic) NSString *startDate;
@property (strong, nonatomic) NSString *statusName;
@property (strong, nonatomic) NSString *totalPage;
@property (strong, nonatomic) NSString *totalRow;

@end
