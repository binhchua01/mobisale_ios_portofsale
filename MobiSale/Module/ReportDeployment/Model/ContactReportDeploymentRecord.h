//
//  ContactReportDeploymentRecord.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 2/4/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContactReportDeploymentRecord : NSObject

@property (nonatomic,retain) NSString *Address;
@property (nonatomic,retain) NSString *Contract;
@property (nonatomic,retain) NSString *CurrentPage;
@property (nonatomic,retain) NSString *Date;
@property (nonatomic,retain) NSString *FullName;
@property (nonatomic,retain) NSString *ID;
@property (nonatomic,retain) NSString *LocalType;
@property (nonatomic,retain) NSString *LocalTypeName;
@property (nonatomic,retain) NSString *RowNumber;
@property (nonatomic,retain) NSString *TotalPage;
@property (nonatomic,retain) NSString *TotalRow;

@end
