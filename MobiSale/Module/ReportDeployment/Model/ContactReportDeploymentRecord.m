//
//  ContactReportDeploymentRecord.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 2/4/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ContactReportDeploymentRecord.h"

@implementation ContactReportDeploymentRecord

@synthesize Address;
@synthesize Contract;
@synthesize CurrentPage;
@synthesize Date;
@synthesize FullName;
@synthesize ID;
@synthesize LocalType;
@synthesize LocalTypeName;
@synthesize RowNumber;
@synthesize TotalPage;
@synthesize TotalRow;

@end
