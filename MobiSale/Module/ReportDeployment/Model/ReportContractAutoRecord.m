//
//  ReportContractAutoRecord.m
//  MobiSale
//
//  Created by ISC-DanTT on 3/21/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ReportContractAutoRecord.h"

@implementation ReportContractAutoRecord

@synthesize Name, CurrentPage, RowNumber, Status, StatusName, Total, TotalPage, TotalRow;

@end
