//
//  TDDatePicker.h
//  MobiSale
//
//  Created by ISC-DanTT on 3/17/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface TDDatePicker:BaseViewController

@property (nonatomic, strong) NSString     *dateFormat;

@property (nonatomic, strong) UIButton     *buttonDate;

@property (nonatomic, strong) UIDatePicker *datePicker;

@property (nonatomic, strong) NSString     *chooseDate;

@property (nonatomic, strong) NSDate       *selectedDate;

- (void)limitDate:(NSDate*)date minYear:(NSInteger)min maxYear:(NSInteger)max;

- (void)setSelectedDateInField;

- (void)showDatePicker;

- (NSDate *)convertStringToDate:(NSString *)dateStringInput;

- (NSString *)convertDateToString:(NSDate*)dateInput;

@end
