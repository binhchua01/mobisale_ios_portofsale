//
//  ReportSBIViewController.m
//  MobiSale
//
//  Created by Tan Tho Nguyen on 7/27/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ReportSBIViewController.h"
#import "UIPopoverListView.h"
#import "ShareData.h"
#import "KeyValueModel.h"
#import "Common.h"

@interface ReportSBIViewController ()

@end

@implementation ReportSBIViewController{
    
    KeyValueModel  *selectedMonth, *selectedYear, *selectedDay, *selectedType, *selectedStatus;
    NSString *salename;
}
enum tableSourceStype {
    Month = 1,
    Year = 2,
    Day= 3,
    Type= 4,
    Status=5
};

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadDataInput];
    
    // add by dantt2
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    // add by dantt2
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    // Do any additional setup after loading the view from its nib.
}
-(void)loadDataInput{
    [self LoadDay];
    [self LoadMonth];
    [self LoadYear];
    [self LoadStatus];
    [self LoadType];
    
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row cell:cell tag:popoverListView.tag];
    return cell;
}
#pragma mark - UIPopoverListViewDataSource

- (void) setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell tag:(NSInteger)tag{
    KeyValueModel *model;
    switch (tag) {
        case Month:
            model = [self.arrMonth objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Year:
            model = [self.arrYear objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Status:
            model = [self.arrStatus objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Day:
            model = [self.arrDay objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        case Type:
            model = [self.arrType objectAtIndex:row];
            cell.textLabel.text = model.Values;
            break;
        default:
            break;
    }
    
    
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = 0;
    switch (popoverListView.tag) {
        case Month:
            count = self.arrMonth.count;
            break;
        case Year:
            count = self.arrYear.count;
            
            break;
        case Day:
            count = self.arrDay.count;
            break;
        case Status:
            count = self.arrStatus.count;
            break;
        case Type:
            count = self.arrType.count;
            break;
        default:
            break;
    }
    return count;
}

#pragma mark - UIPopoverListViewDelegate

- (void) didDropDownSelected:(NSInteger)row tag:(NSInteger)tag{
    
    switch (tag) {
        case Month:
            selectedMonth = [self.arrMonth objectAtIndex:row];
            [self.drop_month setTitle:selectedMonth.Values forState:UIControlStateNormal];
            break;
        case Year:
            selectedYear = [self.arrYear objectAtIndex:row];
            
            [self.drop_year setTitle:selectedYear.Values forState:UIControlStateNormal];
            break;
        case Day:
            selectedDay = [self.arrDay objectAtIndex:row];
            [self.drop_all setTitle:selectedDay.Values forState:UIControlStateNormal];
            break;
        case Status:
            selectedStatus = [self.arrStatus objectAtIndex:row];
            [self.drop_status setTitle:selectedStatus.Values forState:UIControlStateNormal];
            break;
        case Type:
            selectedType = [self.arrType objectAtIndex:row];
            [self.drop_type setTitle:selectedType.Values forState:UIControlStateNormal];
            break;
        default:
            break;
    }
    
    
}


#pragma mark - UIPopoverListViewDelegate

- (void)popoverListView:(UIPopoverListView *)popoverListView
     didSelectIndexPath:(NSIndexPath *)indexPath
{
    [self didDropDownSelected:indexPath.row tag:popoverListView.tag];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}


- (void)setupDropDownView:(NSInteger) tag
{
    
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.tag = tag;
    poplistview.listView.scrollEnabled = TRUE;
    switch (tag) {
        case Month:
            [poplistview setTitle:NSLocalizedString(@"Chọn tháng", @"")];
            break;
        case Year:
            [poplistview setTitle:NSLocalizedString(@"Chọn năm", @"")];
            break;
        case Status:
            [poplistview setTitle:NSLocalizedString(@"Chọn tình trạng", @"")];
            break;
        case Day:
            [poplistview setTitle:NSLocalizedString(@"Chọn ngày", @"")];
            break;
        case Type:
            [poplistview setTitle:NSLocalizedString(@"Chọn loại tìm kiếm", @"")];
            break;
        default:
            break;
    }
    
    [poplistview show];
    
}
- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 35.0f;
}
-(void)LoadDay
{
    self.arrDay = [NSMutableArray array];
    for (int i = 0; i <= 31; i++) {
        KeyValueModel *s;
        if(i == 0){
            s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",i] description:[NSString stringWithFormat:@"%@",@"Tất cả"]];
        }else {
            s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",i] description:[NSString stringWithFormat:@"%d",i]];
        }
        
        [self.arrDay addObject: s];
    }
    
    selectedDay= [self.arrDay objectAtIndex:0];
    [self.drop_all setTitle:selectedDay.Values forState:UIControlStateNormal];
}
-(void)LoadMonth
{
    self.arrMonth = [NSMutableArray array];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM"];
    NSString *MonthString = [formatter stringFromDate:[NSDate date]];
    for (int i = 1; i <= 12; i++) {
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",i] description:[NSString stringWithFormat:@"%d",i]];
        [self.arrMonth addObject: s];
    }
    selectedMonth = [self.arrMonth objectAtIndex:[MonthString intValue]-1];
    [self.drop_month setTitle:selectedMonth.Values forState:UIControlStateNormal];
}

-(void)LoadYear
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    NSString *yearString = [formatter stringFromDate:[NSDate date]];
    self.arrYear = [NSMutableArray array];
    int year = [yearString intValue];
    for (int i = 0; i <= 1; i++) {
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",year - i] description:[NSString stringWithFormat:@"%d",year - i]];
        [self.arrYear addObject: s];
    }
    selectedYear = [self.arrYear objectAtIndex:0];
    
    [self.drop_year setTitle:selectedYear.Values forState:UIControlStateNormal];
    
}
-(void)LoadStatus
{
    self.arrStatus = [NSMutableArray array];
    NSString *descriptionStaus =@"";
    for (int i = -2; i <= 3; i++) {
        if (i ==-2) {
            descriptionStaus=@"Tất cả";
        }else if( i==-1){
            descriptionStaus=@"Còn hạn/Hết hạn";
            
        }else if( i==0){
            descriptionStaus=@"Hết hạn";
        }else if( i==1){
            descriptionStaus=@"Còn hạn";
        }else if( i==2){
            descriptionStaus=@"Đã sử dụng";
        }else if( i==3){
            descriptionStaus=@"Đã huỷ";
        }
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",i] description:[NSString stringWithFormat:@"%@",descriptionStaus]];
        [self.arrStatus addObject: s];
    }
    selectedStatus= [self.arrStatus objectAtIndex:0];
    [self.drop_status setTitle:selectedStatus.Values forState:UIControlStateNormal];
}
-(void)LoadType
{
    self.arrType = [NSMutableArray array];
    
    NSString *descriptionType =@"";
    for (int i =0; i <= 3; i++) {
        if (i ==0) {
            descriptionType=@"Tất cả";
        }else if( i==1){
            descriptionType=@"Số SBI";
            
        }
        else if( i==2){
            descriptionType=@"Số phiếu đăng ký";
            
        }
        else if( i==3){
            descriptionType=@"Sele Account";
        }
        
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",i] description:[NSString stringWithFormat:@"%@",descriptionType]];
        [self.arrType addObject: s];
    }
    selectedType= [self.arrType objectAtIndex:0];
    [self.drop_status setTitle:selectedType.Values forState:UIControlStateNormal];
}

- (IBAction)btn_search:(id)sender {
    ShareData *shared =[ShareData instance];
    
    NSMutableDictionary *toReturn = [NSMutableDictionary dictionary];
    [toReturn setValue: shared.currentUser.userName forKey:@"UserName"];
    NSString*noidung=self.drop_contents.text;
    if (noidung.length <1) {
        noidung=@"";
    }
    //[toReturn setValue: @"HCM.DIEUTTN" forKey:@"UserName"];
    [toReturn setValue: selectedDay.Key forKey:@"Day"];
    [toReturn setValue: selectedMonth.Key forKey:@"Month"];
    [toReturn setValue: selectedYear.Key forKey:@"Year"];
    [toReturn setValue: selectedStatus.Key forKey:@"Status"];
    [toReturn setValue: selectedType.Key forKey:@"Agent"];
    [toReturn setValue: noidung forKey:@"AgentName"];
    [toReturn setValue: @"1" forKey:@"PageNumber"];
    
    NSDictionary *parram=  [NSDictionary dictionaryWithDictionary:toReturn];
    
    if(self.delegate){
        [self.delegate callSBIAPISearch:parram];
    }
}

- (IBAction)btnCancel:(id)sender {
    if(self.delegate){
        [self.delegate CancelPopupInfo];
    }
}

- (IBAction)btn_day:(id)sender {
    [self setupDropDownView:Day];
}

- (IBAction)btn_mouth:(id)sender {
    [self setupDropDownView:Month];
}

- (IBAction)btn_year:(id)sender {
    [self setupDropDownView:Year];
}

- (IBAction)btn_status:(id)sender {
    [self setupDropDownView:Status];
}

- (IBAction)btn_type:(id)sender {
    [self setupDropDownView:Type];
}

// add by dantt2
- (void)keyboardDidShow: (NSNotification *) notif{
    CGFloat y = 60;
    if (IS_IPHONE4) {
        y = 85;
    }
    CGRect frm = self.view.frame;
    frm.origin.x = frm.origin.x ;
    frm.origin.y = frm.origin.y - y;
    frm.size.width = frm.size.width;
    frm.size.height = frm.size.height;
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.view.frame = frm;
                     }];
    
}

// add by dantt2
- (void)keyboardDidHide: (NSNotification *) notif{
    CGFloat y = 60;
    if (IS_IPHONE4) {
        y = 85;
    }
    CGRect frm = self.view.frame;
    frm.origin.x = frm.origin.x ;
    frm.origin.y = frm.origin.y + y;
    frm.size.width = frm.size.width;
    frm.size.height = frm.size.height;
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.view.frame = frm;
                     }];
}


@end
