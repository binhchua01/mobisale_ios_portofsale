//
//  ReportListDeploymentDetails.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 2/4/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BaseViewController.h"


@protocol ReportListDeploymentDetailsDelegate <NSObject>

-(void)CancelPopup;

@end

@interface ReportListDeploymentDetails : BaseViewController <UITableViewDataSource, UITableViewDelegate,UIAlertViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *btn_pageNumber;
- (IBAction)btn_pageNumberClicked:(id)sender;

-(IBAction)btnCancel_clicked:(id)sender;

@property (strong, nonatomic) NSMutableArray *arrList;
@property (strong, nonatomic) NSMutableArray *arrPage;
@property (strong, nonatomic) NSString *Flat;
@property (strong, nonatomic) NSString *SaleName;
@property (strong, nonatomic) NSString *Month;
@property (strong, nonatomic) NSString *Year;
@property (strong, nonatomic) NSString *ObjectName;
//contentDetailsType
@property (strong, nonatomic) NSString *contentDetailsType;
@property (strong, nonatomic) NSString *contentDetailsUser;

@property (strong, nonatomic) NSString *typeTable;

@property (strong, nonatomic) NSString *ToDate;

@property (strong, nonatomic) NSString *FromDate;

@property (strong, nonatomic) NSString *Agent;

@property (strong, nonatomic) NSString *AgentName;

@property (retain, nonatomic) id<ReportListDeploymentDetailsDelegate>delegate;

@end
