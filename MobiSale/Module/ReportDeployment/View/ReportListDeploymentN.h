//
//  ReportListSBIController.h
//  MobiSale
//
//  Created by Nguyen Tan Tho on 2/4/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TDDatePicker.h"

@protocol ReportListDeploymentNDelegate <NSObject>

-(void)CancelPopup;

@end

@interface ReportListDeploymentN : TDDatePicker <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate >

@property (strong, nonatomic) IBOutlet UILabel *lblResultNumber;

- (IBAction)btnConditionSearch_Clicked:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *searchView;

@property (strong, nonatomic) IBOutlet UITableView *resultTableView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *searchViewHeight;

@property (strong, nonatomic) IBOutlet UIImageView *imageUpDown;

@property (strong, nonatomic) IBOutlet UIButton *btnToDate;

@property (strong, nonatomic) IBOutlet UIButton *btnFromDate;

@property (strong, nonatomic) IBOutlet UIButton *btnDepartment;

@property (strong, nonatomic) IBOutlet UIButton *btnAgent;

@property (strong, nonatomic) IBOutlet UITextField *txtAgentName;

- (IBAction)btnToDate_Clicked:(id)sender;

- (IBAction)btnFromDate_Clicked:(id)sender;

- (IBAction)btnDepartment_Clicked:(id)sender;

- (IBAction)btnAgent_Clicked:(id)sender;

- (IBAction)btnSearch_Clicked:(id)sender;

- (IBAction)btnReloadDepartment_Clicked:(id)sender;

@property (strong, nonatomic) NSArray *arrList;

@property (retain, nonatomic) id<ReportListDeploymentNDelegate>delegate;

@end
