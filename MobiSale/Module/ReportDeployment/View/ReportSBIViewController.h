//
//  ReportSBIViewController.h
//  MobiSale
//
//  Created by Tan Tho Nguyen on 7/27/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BaseViewController.h"

@protocol ReportSBIViewController <NSObject>
-(void)CancelPopupInfo;
-(void)callSBIAPISearch:(NSDictionary *)Data;
@end

@interface ReportSBIViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIButton *drop_all;
@property (weak, nonatomic) IBOutlet UIButton *drop_month;
@property (weak, nonatomic) IBOutlet UIButton *drop_year;
@property (weak, nonatomic) IBOutlet UIButton *drop_status;
@property (weak, nonatomic) IBOutlet UIButton *drop_type;
@property (weak, nonatomic) IBOutlet UITextField *drop_contents;
- (IBAction)btn_search:(id)sender;
- (IBAction)btnCancel:(id)sender;
- (IBAction)btn_day:(id)sender;
- (IBAction)btn_mouth:(id)sender;
- (IBAction)btn_year:(id)sender;
- (IBAction)btn_status:(id)sender;
- (IBAction)btn_type:(id)sender;

@property (strong, nonatomic) NSMutableDictionary *arrList;
@property (strong, nonatomic) NSMutableArray *arrMonth;
@property (strong, nonatomic) NSMutableArray *arrYear;
@property (strong, nonatomic) NSMutableArray *arrDay;
@property (strong, nonatomic) NSMutableArray *arrStatus;
@property (strong, nonatomic) NSMutableArray *arrType;


@property (retain, nonatomic) id<ReportSBIViewController>delegate;


@end
