//
//  InfoContractViewController.h
//  MobiSale
//
//  Created by ISC-DanTT on 4/21/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "InfoContractRecord.h"
#import "ContractInfoCellModel.h"

@interface InfoContractViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) ContractInfoCellModel *data;
@property (strong, nonatomic) InfoContractRecord *record;
@property (strong, nonatomic) NSString *contract;

@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblContract;
@property (strong, nonatomic) IBOutlet UILabel *lblRegCode;
@property (strong, nonatomic) IBOutlet UILabel *lblPhone1;
@property (strong, nonatomic) IBOutlet UILabel *lblPhone2;
@property (strong, nonatomic) IBOutlet UILabel *lblEmail;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblNote;
@property (strong, nonatomic) IBOutlet UILabel *lblServiceType;
@property (strong, nonatomic) IBOutlet UILabel *lblCreateDate;
@property (strong, nonatomic) IBOutlet UILabel *lblRegCodeNew;
@property (strong, nonatomic) IBOutlet UILabel *lblComboStatus;
@property (strong, nonatomic) IBOutlet UILabel *lblServiceTypeRegisted;
@property (strong, nonatomic) IBOutlet UILabel *lblBoxCount;

@property (strong, nonatomic) IBOutlet UIButton *btnCreateOrUpdateRegCodeNew;

@end
