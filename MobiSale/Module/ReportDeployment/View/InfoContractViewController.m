//
//  InfoContractViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 4/21/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "InfoContractViewController.h"
#import "RegistrationFormSaleMorePageViewController.h"
#import "DetailRegisteredForm.h"
#import "ShareData.h"
#import "UIButton+FPTCustom.h"

#define TitleCreate @"Tạo Phiếu đăng ký bán thêm"
#define TitleUpdate @"Xem Phiếu đăng ký bán thêm"

@interface InfoContractViewController ()

@end

@implementation InfoContractViewController

@synthesize data;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"THÔNG TIN HỢP ĐỒNG";
    self.screenName = self.title;
    
    [self.lblName    sizeToFit];
    [self.lblEmail   sizeToFit];
    [self.lblAddress sizeToFit];
    [self.lblNote    sizeToFit];
    [self.lblComboStatus sizeToFit];
    [self.lblServiceTypeRegisted sizeToFit];
    [self.btnCreateOrUpdateRegCodeNew styleButtonUpdate];
    [self.btnCreateOrUpdateRegCodeNew setShadow];
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshing:) forControlEvents:UIControlEventValueChanged];
    [self.scrollView addSubview:refreshControl];
    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)refreshing:(UIRefreshControl *)sender {
    [sender endRefreshing];
    [self loadData];
}
#pragma mark - Load data from web service
// Get API
- (void)loadData {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    [dict setObject:self.contract forKey:@"Contract"];

    
    [self showMBProcess];
    [shared.reportlistdeploymentproxy getInfoContract:dict completeionHander:^(id result, NSString *errorCode, NSString *message) {
        
        [self hideMBProcess];
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"0"] || result == nil){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",@"Không lấy được thông tin"]];
            return ;
        }
        
        self.record = result;
        [self setViewData];
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
        [self hideMBProcess];
    }];
    
    
}

// Set data for label and button
- (void)setViewData {
    
    self.lblName.text     = self.record.fullName;
    self.lblContract.text = self.record.contract;
    self.lblRegCode.text  = self.record.regCode;
    self.lblEmail.text    = self.record.email;
    // fixed đổi số điện thoại
    self.lblPhone1.text   = self.record.phone_1;
    self.lblPhone2.text   = self.record.phone_2;
    self.lblAddress.text  = self.record.address;
    self.lblNote.text     = self.record.note;
    self.lblServiceType.text = self.record.localTypeName;
    self.lblCreateDate.text  = self.record.date;
    
    self.lblServiceTypeRegisted.text = self.record.serviceTypeName;
    self.lblRegCodeNew.text     = self.record.regCodeNew;
    self.lblComboStatus.text    = self.record.comboStatusName;
    self.lblBoxCount.text       = StringFormat(@"%li", (long)self.record.boxCount);
    
    [self.btnCreateOrUpdateRegCodeNew setTitle:[self titleForButton] forState:UIControlStateNormal];

   }

// Set title for button
- (NSString *)titleForButton {
    
    if (self.record.regIDNew == 0 && self.record.regCodeNew.length <= 0) {
        
        return TitleCreate;
    }

    return TitleUpdate;
}

#pragma mark - Action pressed button 
- (IBAction)btnCreateOrUpdateRegCodeNewPressed:(id)sender {
    if ([self.btnCreateOrUpdateRegCodeNew.titleLabel.text isEqual:TitleCreate]) {
        // implementing create reg...
        RegistrationFormSaleMorePageViewController *vc = [[RegistrationFormSaleMorePageViewController alloc] init];

        vc.viewType = CreateRegisterForm;
        vc.updateRegistrationContractModel = [[UpdateRegistrationContractModel alloc] initWithInfoContractRecord:self.record];
        [self.navigationController pushViewController:vc animated:YES];
        return;
    }
    
    if ([self.btnCreateOrUpdateRegCodeNew.titleLabel.text isEqual:TitleUpdate]) {
        // implementing update reg...
        DetailRegisteredForm *vc = [[DetailRegisteredForm alloc] init];

        vc.Id = StringFormat(@"%li",(long)self.record.regIDNew);
        [self.navigationController pushViewController:vc animated:YES];
        return;

    }
    

}

@end
