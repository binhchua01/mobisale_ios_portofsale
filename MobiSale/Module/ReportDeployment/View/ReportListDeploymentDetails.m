//
//  ReportListDeploymentDetails.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 2/4/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ReportListDeploymentDetails.h"
#import "DetailReportDeploymentCell.h" // Bao cao SBI chi tiet
#import "ContactReportDeploymentRecord.h"
#import "UIPopoverListView.h"
#import "ShareData.h"
#import "KeyValueModel.h"
#import "Common_client.h"
//#import "InfoContractViewController.h"

@interface ReportListDeploymentDetails ()

@end

@implementation ReportListDeploymentDetails{
    KeyValueModel *selectedPage;
    NSMutableDictionary *ListSBIO;
    NSString *pageNumber;
    NSString *keySecleted;
    NSString *totalPage;
    UIAlertView *alert;
}
@dynamic tableView;

@synthesize Agent, AgentName, FromDate, ToDate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
     ListSBIO = [NSMutableDictionary dictionary];
    // Do any additional setup after loading the view.
    self.title = @"CHI TIẾT THUÊ BAO";
    self.screenName=@"BÁO CÁO PTTB (CHI TIẾT)";
    [self LoadData:@"1"];
    self.tableView.separatorColor = [UIColor clearColor];
   
  
}
-(void)viewDidAppear:(BOOL)animated{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidLayoutSubviews{
    float h = 30;
    if(IS_IPHONE4){
        h = 118;
    }
   // self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, [SiUtils getScreenFrameSize].height - 30);
    self.tableView.frame =CGRectMake(0, 62, self.view.frame.size.width, [SiUtils getScreenFrameSize].height  - 90);
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(self.arrList.count > 0) {
        return self.arrList.count;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 220;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *simpleTableIdentifier = @"DetailReportDeploymentCell";
    DetailReportDeploymentCell *cell = (DetailReportDeploymentCell *)[self.tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
    
    cell = [nib objectAtIndex:0];
    //int index=(int)indexPath.row;
    NSDictionary *temp=[self.arrList objectAtIndex:indexPath.row];
    if(self.arrList.count > 0){
        //cell.lblSTT.text =[NSString stringWithFormat:@"%d",index+1];
        cell.lblSTT.text = StringFormat(@"%@",[temp objectForKey:@"RowNumber"]);
        cell.lblFullName.text=[temp objectForKey:@"FullName"];
        cell.lblAddress.text=[temp objectForKey:@"Address"];
        cell.lblContact.text=[temp objectForKey:@"Contract"];
        cell.lblLocaltype.text=[temp objectForKey:@"LocalTypeName"];
        cell.lblCreateDate.text=[temp objectForKey:@"Date"];
        cell.lblPhoneNumber.text = [temp objectForKey:@"PhoneNumber"];
        //PhoneNumber
        
        
    }
    return cell;
    
}
#pragma mark Table DidSelected
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
/*
    NSDictionary *temp=[self.arrList objectAtIndex:indexPath.row];
    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"InfoContract" bundle:nil];
    
    InfoContractViewController *vc = [[InfoContractViewController alloc] init];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"InfoContract"];
    vc.data = temp;
    
    [self.navigationController pushViewController:vc animated:YES];
 */
    return;
}

#pragma mark - DropDown view

- (void)setupDropDownView {
    CGFloat xWidth = self.view.bounds.size.width - 20.0f;
    CGFloat yHeight = 272.0f;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    UIPopoverListView *poplistview = [[UIPopoverListView alloc] initWithFrame:CGRectMake(10, yOffset, xWidth, yHeight)];
    poplistview.delegate = (id)self;
    poplistview.datasource = (id)self;
    poplistview.listView.scrollEnabled = TRUE;
    [poplistview setTitle:NSLocalizedString(@"Chọn trang", @"")];
    [poplistview show];
    
}
- (CGFloat)popoverListView:(UIPopoverListView *)popoverListView
   heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 35.0f;
}

#pragma mark - UIPopoverListViewDataSource

- (void) setUpDropDownCell:(NSInteger)row cell:(UITableViewCell*)cell {
    KeyValueModel *model;
    model = [self.arrPage objectAtIndex:row];
    cell.textLabel.text = model.Values;
    
}

#pragma mark - UIPopoverListViewDataSource

- (UITableViewCell *)popoverListView:(UIPopoverListView *)popoverListView
                    cellForIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:identifier] ;
    [self setUpDropDownCell:indexPath.row cell:cell];
    return cell;
}

- (NSInteger)popoverListView:(UIPopoverListView *)popoverListView
       numberOfRowsInSection:(NSInteger)section {
    NSInteger count = self.arrPage.count;
    return count;
}

#pragma mark - UIPopoverListViewDelegate

- (void) didDropDownSelected:(NSInteger)row {
    selectedPage = [self.arrPage objectAtIndex:row];
    [self LoadData:selectedPage.Key];
    NSString*title =[NSString stringWithFormat:@"%@/%@",selectedPage.Values,totalPage];
    [self.btn_pageNumber setTitle:title forState:UIControlStateNormal];
    
}

#pragma mark - UIPopoverListViewDelegate

- (void)popoverListView:(UIPopoverListView *)popoverListView
     didSelectIndexPath:(NSIndexPath *)indexPath {
    [self didDropDownSelected:indexPath.row];
    
}

#pragma Load Data
-(void)LoadData:(NSString *)pagenumber {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    //dữ liệu test
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:self.contentDetailsUser forKey:@"UserName"];
    [dict setObject:FromDate   forKey:@"FromDate"];
    [dict setObject:ToDate     forKey:@"ToDate"];
    [dict setObject:Agent      forKey:@"Agent"];
    [dict setObject:AgentName  forKey:@"AgentName"];
    [dict setObject:pagenumber forKey:@"PageNumber"];
    [dict setObject:self.contentDetailsType forKey:@"LocalType"];

    [self showMBProcess];

    [shared.reportlistdeploymentproxy GetSubscriberGrowthObjectList:dict Completehander:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            //kiểm tra resuld trả về là 0
            //trường hợp không có message
            if ([message isEqualToString:@""]) {
                [self showAlert];
            }else
            {
                [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            }
            
            return ;
        }
        if([result count] <1){
            [self showAlertBox:@"Thông báo" message:@"Không có báo cáo SBI"];
            return;
            
        }else{
            self.arrList=result;
            totalPage =[[self.arrList objectAtIndex:0] objectForKey:@"TotalPage"];
            [self.tableView reloadData];
            if([pagenumber isEqualToString:@"1"]){
                 [self LoadPage];
            }
        }

    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
    } ];
  
}

#pragma mark - Load page
-(void)LoadPage {
    int pageNumbers=[totalPage intValue];
    if (pageNumbers <2) {
        [self.btn_pageNumber setEnabled:NO];
        [self.btn_pageNumber setTitle:@"1/1" forState:UIControlStateNormal];
        return;
    }
    [self.btn_pageNumber setEnabled:YES];
    self.arrPage = [NSMutableArray array];
    int i = 1;
    for (i = 1; i <= pageNumbers; i++) {
        KeyValueModel *s = [[KeyValueModel alloc] initWithName:[NSString stringWithFormat:@"%d",i] description:[NSString stringWithFormat:@"%d",i]];
        [self.arrPage addObject: s];
    }
    selectedPage = [self.arrPage objectAtIndex:0];
    NSString*title =[NSString stringWithFormat:@"%@/%@",selectedPage.Values,totalPage];
    [self.btn_pageNumber setTitle:title forState:UIControlStateNormal];
}

#pragma mark - event button click


- (IBAction)btn_pageNumberClicked:(id)sender {
    [self setupDropDownView];
}

-(IBAction)btnCancel_clicked:(id)sender {
    if(self.delegate){
        [self.delegate CancelPopup];
    }
    [ShareData instance].repostSBIList=nil;
}

//show alert
- (void)showAlert {
    
    alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Không có dữ liệu trả về !!!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case 0:
            [self.navigationController popViewControllerAnimated:YES];
            break;
            
        default:
            break;
    }
}

@end
