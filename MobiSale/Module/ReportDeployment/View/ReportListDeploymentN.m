//
//  ReportListDeploymentN.m
//  MobiSale
//
//  Created by Nguyen Tan Tho on 2/4/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ReportListDeploymentN.h"
#import "ContactReportDeploymentRecord.h"
#import "UIPopoverListView.h"
#import "ShareData.h"
#import "KeyValueModel.h"
#import "ReportSBIDetailsViewController.h"
#import "ListSBIDetailsViewCell.h"
#import "ReportListDeploymentDetails.h"
#import "Common_client.h"

#import "NIDropDown.h"
#import "DemoTableFooterView.h"

@interface ReportListDeploymentN () <NIDropDownDelegate>

@end

@implementation ReportListDeploymentN {
    
    NSMutableArray *arrResult;
    
    BOOL            isSearchViewShow;
    UIButton        *btnSelected;
    NSArray         *arrStatus;
    NSArray         *arrAgent;
    NSArray         *arrDepartment;
    NIDropDown      *dropDown;
    NSDictionary    *selectedAgent;
    NSDictionary    *selectedDepartment;
    NSString        *fromDate;
    NSString        *toDate;
    
    DemoTableFooterView *footerView;
    NSInteger totalPage;
    NSInteger currentPage;
    CGFloat   startOffset;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title      = @"B/C PHÁT TRIỂN THUÊ BAO";
    self.screenName = @"BÁO CÁO PHÁT TRIỂN THUÊ BAO";
    self.resultTableView.separatorColor = [UIColor clearColor];
    
    arrResult = [NSMutableArray array];
    
    self.searchView.layer.borderColor  = [UIColor colorWithRed:0.133 green:0.698 blue:0.569 alpha:1].CGColor;
    self.searchView.layer.borderWidth  = 1.0f;
    self.searchView.layer.cornerRadius = 4.0f;
    self.resultTableView.hidden = YES;
    
    self.lblResultNumber.text = @"";
    startOffset = - 20;
    /*
     *set the custom view for "load more". See DemoTableFooterView.xib.
     */
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DemoTableFooterView" owner:self options:nil];
    footerView = (DemoTableFooterView *)[nib objectAtIndex:0];
    
    self.btnToDate.enabled = NO;
    
//    [self.view setMultipleTouchEnabled:YES];
//    UITapGestureRecognizer *tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
//    [self.view addGestureRecognizer:tapper];
    /*
     *add date picker
     */
//    CGRect datePickerFrame = CGRectMake(0,0, self.view.frame.size.width, 200);
//    self.datePicker = [[UIDatePicker alloc] initWithFrame:datePickerFrame];
//    self.datePicker.datePickerMode = UIDatePickerModeDate;
    self.dateFormat = @"yyyy-MM-dd";
    
    isSearchViewShow = YES;
        
    arrAgent = [NSArray arrayWithObjects:[NSDictionary dictionaryWithObject:@"Tất cả" forKey:@"0"],[NSDictionary dictionaryWithObject:@"Tên nhân viên" forKey:@"1"], nil];
    
    [self loadData];
}

- (void)loadData {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd/MM/yyyy"];
    [self.btnFromDate setTitle:[dateFormatter stringFromDate:[NSDate date]] forState:UIControlStateNormal];
    [self.btnToDate   setTitle:[dateFormatter stringFromDate:[NSDate date]] forState:UIControlStateNormal];
    
    selectedAgent = [arrAgent objectAtIndex:0];
    [self.btnAgent setTitle:[[selectedAgent allValues] objectAtIndex:0] forState:UIControlStateNormal];
    
    arrDepartment = [ShareData instance].currentUser.arrDepartment;
    selectedDepartment = [arrDepartment objectAtIndex:0];
    [self.btnDepartment setTitle:[[selectedDepartment allValues] objectAtIndex:0] forState:UIControlStateNormal];
    
}

- (void)loadDepartmentList {
    ShareData *shared = [ShareData instance];
    [self showMBProcess];
    [shared.reportlistdeploymentproxy getDepartmentList:shared.currentUser.userName completeionHander:^(id result, NSString *errorCode, NSString *message) {
        
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        if(![errorCode isEqualToString:@"0"] || result == nil){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",@"Không lấy được danh sách phòng"]];
            return ;
        }
        NSMutableArray *arr = [NSMutableArray array];
        for (NSDictionary *dict in result) {
            NSString *key   = [dict objectForKey:@"ID"];
            NSString *value = [dict objectForKey:@"Name"];
            [arr addObject:[NSDictionary dictionaryWithObject:value forKey:key]];
        }
        arrDepartment = [NSArray arrayWithArray:arr];
        selectedDepartment = [arrDepartment objectAtIndex:0];
        [self.btnDepartment setTitle:[[selectedDepartment allValues] objectAtIndex:0] forState:UIControlStateNormal];
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@", [error localizedDescription]]];
        [self hideMBProcess];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self handleSingleTap:nil];
}

- (IBAction)btnConditionSearch_Clicked:(id)sender {
    if (isSearchViewShow == NO) {
        [self showSearchView];
        return;
    }
    
    if (isSearchViewShow == YES) {
        [self hideSearchView];
        return;
    }
}

- (IBAction)btnFromDate_Clicked:(id)sender {
    
    if (dropDown != nil && btnSelected != sender) {
        [dropDown hideDropDown:btnSelected];
        dropDown = nil;
        
    }
    self.buttonDate = sender;
    [self showDatePicker];
    [self limitDate:[NSDate date] minYear:-99 maxYear:0];

    self.btnToDate.enabled = YES;
    
}

- (IBAction)btnToDate_Clicked:(id)sender {
    
    if (dropDown != nil && btnSelected != sender) {
        [dropDown hideDropDown:btnSelected];
        dropDown = nil;
    }
    self.buttonDate = sender;
    [self showDatePicker];
    [self limitDate:self.selectedDate minYear:0 maxYear:9];
    
}

- (IBAction)btnDepartment_Clicked:(id)sender {
    if (dropDown != nil && btnSelected != sender) {
        [dropDown hideDropDown:btnSelected];
        dropDown = nil;
    }
    btnSelected = sender;
    [self showDropDownAtButton:sender withArrayData:arrDepartment];
}

- (IBAction)btnAgent_Clicked:(id)sender {
    if (dropDown != nil && btnSelected != sender) {
        [dropDown hideDropDown:btnSelected];
        dropDown = nil;
    }
    btnSelected = sender;
    [self showDropDownAtButton:sender withArrayData:arrAgent];
    
}

- (IBAction)btnSearch_Clicked:(id)sender {
    [self.view endEditing:YES];

    if (dropDown != nil && btnSelected != sender) {
        [dropDown hideDropDown:btnSelected];
        dropDown = nil;
    }
    
    if (arrDepartment.count <= 0) {
        [self showAlertBox:@"Thông báo" message:@"Chưa tải được danh sách phòng. \nVui lòng bấm nút refresh để tải danh sách phòng!"];
        return;
    }
    
    [arrResult removeAllObjects];
    
    [self getReportlistdeployment:@"1"];
}

- (IBAction)btnReloadDepartment_Clicked:(id)sender {
    [self loadDepartmentList];
}

#pragma mark - NIDropDown method
- (void)showDropDownAtButton:(UIButton *)sender withArrayData:(NSArray*)arrData{
    [self.view endEditing:YES];
    
    if (dropDown == nil) {
        int i = 0;
        NSMutableArray *arrOutput = [[NSMutableArray alloc] init];
        for (i = 0; i < arrData.count; i++) {
            NSDictionary *dict = [arrData objectAtIndex:i];
            [arrOutput addObjectsFromArray:[dict allValues]];
        }
        
        CGFloat height = 40*arrData.count;
        if (arrData.count > 3) {
            height = 120;
        }
        CGFloat width = 0;
        dropDown = [[NIDropDown alloc] showDropDownAtButton:sender height:&height width:&width data:arrOutput images:nil animation:@"down"];
        dropDown.delegate = self;
        
        return;
    }
    [dropDown hideDropDown:sender];
    [self rel];
}

- (void)niDropDownDelegateMethod:(NIDropDown *)sender {
    [self rel];
}

- (void)rel {
    dropDown = nil;
}

- (void)dropDownListView:(NIDropDown *)dropDownListView didSelectIndexPath:(NSIndexPath *)indexPath atButton:(UIButton *)button{
    [self didDropDownSelected:indexPath.row atButton:button];
}

- (void)didDropDownSelected:(NSInteger)row atButton:(UIButton *)button {
    if (button == self.btnDepartment) {
        selectedDepartment = [arrDepartment objectAtIndex:row];
        return;
    }
    if (button == self.btnAgent) {
        selectedAgent = [arrAgent objectAtIndex:row];
        return;
    }
    
}

- (void)handleSingleTap:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
    if (dropDown !=nil) {
        [dropDown hideDropDown:btnSelected];
        [self rel];
    }
}

#pragma mark - show and hide search view

- (void)showSearchView {
    self.searchViewHeight.constant = 225;
    self.searchView.hidden = NO;
    self.imageUpDown.image = [UIImage imageNamed:@"up-icon-512"];
    isSearchViewShow = YES;
    [UIView animateWithDuration:0.25
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

- (void)hideSearchView {
    self.searchViewHeight.constant = 0;
    self.searchView.hidden = YES;
    self.imageUpDown.image = [UIImage imageNamed:@"down-icon-512"];
    isSearchViewShow = NO;
    [UIView animateWithDuration:0.25
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSMutableArray *key =[[arrResult objectAtIndex:section] objectForKey:@"Rows"];
    if([arrResult count] > 0){
        return [key count];
    }
    return 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [arrResult count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame:
                                 CGRectMake(0, 100, tableView.frame.size.width, 70)];
    [sectionHeaderView setBackgroundColor:[UIColor colorWithRed:0.1 green:0.8 blue:0.8 alpha:1]];
    UIView *topView = [[UIView alloc] initWithFrame:
                       CGRectMake(0, 0, tableView.frame.size.width, 1)];
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:
                            CGRectMake(40, 5, tableView.frame.size.width, 25.0)];
    
    topView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:1];

    headerLabel.backgroundColor = [UIColor clearColor];
    [headerLabel setFont:[UIFont fontWithName:@"" size:15.0]];
    headerLabel.textColor = [UIColor colorWithRed:188 green:149 blue:88 alpha:1.0];
    UIImage *myImage = [UIImage imageNamed:@"ic_report_new_object"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:myImage];
    imageView.frame = CGRectMake(5,1,30,30);
    [sectionHeaderView addSubview:headerLabel];
    [sectionHeaderView addSubview:imageView];
    [sectionHeaderView addSubview:topView];
    if ([arrResult count] >= section) {
        headerLabel.text = [[arrResult objectAtIndex:section] objectForKey:@"SaleName"];
        return sectionHeaderView;
    }
    else
        return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 34;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *simpleTableIdentifier = @"ListSBIDetailsViewCell";
    
    ListSBIDetailsViewCell *cell = (ListSBIDetailsViewCell *)[self.tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:simpleTableIdentifier owner:self options:nil];
    cell = [nib objectAtIndex:0];
    
    NSMutableArray * rowsInSection =[[arrResult objectAtIndex:indexPath.section] objectForKey:@"Rows"] ;
    NSString *title =[[rowsInSection objectAtIndex:indexPath.row] objectForKey:@"Title"]?:nil;
    NSString *totals =[[rowsInSection objectAtIndex:indexPath.row] objectForKey:@"Total"]?:nil;
    NSString *all = [NSString stringWithFormat:@"%@ : %@",title,totals];
    cell.lblName.text =all;
    cell.backgroundColor = [UIColor clearColor];
    return cell;
    
}

#pragma mark Table DidSelected
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   
    NSMutableArray *check =[[arrResult objectAtIndex:indexPath.section] objectForKey:@"Rows"] ;
    if (indexPath.row <= [check count]) {
        ReportListDeploymentDetails *vc = [[ReportListDeploymentDetails alloc]initWithNibName:@"ReportListDeploymentDetails" bundle:nil];
        vc.contentDetailsType =[[check objectAtIndex:indexPath.row] objectForKey:@"Type"]?:nil;
        vc.contentDetailsUser =[[arrResult objectAtIndex:indexPath.section] objectForKey:@"SaleName"]?:nil;
        
        vc.FromDate = fromDate;//@"2015-12-01";
        vc.ToDate   = toDate;  //@"2016-03-17";
        vc.Agent    = [[selectedAgent  allKeys] objectAtIndex:0];
        
        if (self.txtAgentName.text.length <= 0) {
            self.txtAgentName.text = @"";
        }
        vc.AgentName = self.txtAgentName.text;
        
        [self.navigationController pushViewController:vc animated:YES];
        
    }
}

#pragma Load Data
-(void)getReportlistdeployment:(NSString *)pagenumber {
    
    ShareData *shared =[ShareData instance];
    
    //create json
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    fromDate = [self convertDateToString: [self convertStringToDate:self.btnFromDate.titleLabel.text]];
    toDate   = [self convertDateToString: [self convertStringToDate:self.btnToDate.titleLabel.text]];
    
    [dict setObject:fromDate forKey:@"FromDate"];
    [dict setObject:toDate   forKey:@"ToDate"];
    [dict setObject:[[selectedDepartment allKeys] objectAtIndex:0] forKey:@"Dept"];
    [dict setObject:[[selectedAgent      allKeys] objectAtIndex:0] forKey:@"Agent"];
    [dict setObject:self.txtAgentName.text forKey:@"AgentName"];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    [dict setObject:pagenumber forKey:@"PageNumber"];
    
    [self showMBProcess];
    [shared.reportlistdeploymentproxy ReportSubscriberGrowthForManager:dict Completehander:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return;
        }
        
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            return ;
        }
        [self.tableView setHidden:NO];
        if ([result count]<1) {
            [self showAlertBox:@"Thông báo" message:@"Không kết quả nào"];
            return ;
        }
        else {
            
            self.arrList = result;
            [arrResult addObjectsFromArray:self.arrList];
            
            currentPage = [[result[0] objectForKey:@"CurrentPage"] integerValue];
            totalPage   = [[result[0] objectForKey:@"TotalPage"]   integerValue];
            
            self.lblResultNumber.text = StringFormat(@"Tổng số: %@",[result[0] objectForKey:@"SumTotal"]);
            
            self.resultTableView.hidden = NO;
            [self.resultTableView reloadData];
            [self hideSearchView];
            [self loadMoreCompleted];

            return ;
        }
        [self hideMBProcess];
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Lỗi" message:[NSString stringWithFormat:@"%@",[error localizedDescription]]];
       
    }];
}

#pragma mark - Scroll view delegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    CGFloat currentOffset = scrollView.contentOffset.y;
    
    if (currentOffset > startOffset) {
        [self hideSearchView];
    }
    startOffset = currentOffset;
    
    CGFloat maximumOffset = fabs(scrollView.contentSize.height - scrollView.frame.size.height);
    if (maximumOffset - currentOffset < 0) {
        [self loadMore];
    }
}

#pragma mark - Load More
//
// The method -loadMore was called and will begin fetching data for the next page (more).
// Do custom handling of -footerView if you need to.
//
- (void) willBeginLoadingMore {
    [footerView.activityIndicator startAnimating];
}

// Do UI handling after the "load more" process was completed. In this example, -footerView will
// show a "No more items to load" text.
//
- (void)loadMoreCompleted {
    [footerView.activityIndicator stopAnimating];
}

- (void)loadMore {
    // Do your async loading here
    self.resultTableView.tableFooterView = footerView;
    currentPage ++;
    if (currentPage <= totalPage) {
        [self willBeginLoadingMore];
        [self getReportlistdeployment:StringFormat(@"%li",(long)currentPage)];
        return;
    }
    footerView.infoLabel.hidden = NO;
}

@end
