//
//  MessageListViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 12/3/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "MessageListViewController.h"
#import "MessageListViewCell.h"
#import "ChatViewController.h"
#import "ShareData.h"
#import "SiUtils.h"

@interface MessageListViewController ()<MGSwipeTableCellDelegate>{

}

@end

@implementation MessageListViewController {
    NSArray *arrList;
    BOOL selectedCell;
    UIToolbar *actionToolbar;
    UIBarButtonItem *actionButton;
    NSMutableArray *arrIndexRowSelected;  // array for save index cell will be delete
}

@synthesize record, customerRecord, data;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"TIN NHẮN";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage jsq_defaultTypingIndicatorImage] style:UIBarButtonItemStyleBordered target:self action:@selector(editPressed:)];
    
    actionToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, [SiUtils getScreenFrameSize].height, [SiUtils getScreenFrameSize].width, 44)];
    
    actionButton = [[UIBarButtonItem alloc] initWithImage:[SiUtils imageWithImageHeight:[UIImage imageNamed:@"trash-icon-512"]height:32] style:UIBarButtonItemStyleBordered target:self action:@selector(deletePressed:)];
    
    [actionToolbar setItems:[NSArray arrayWithObject:actionButton]];
    
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    selectedCell = YES;
    [self loadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)editPressed:(UIBarButtonItem *)sender {
    //[self showActionToolbar:YES];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[SiUtils imageWithImageHeight:[UIImage imageNamed:@"cancel-icon-512"] height:32] style:UIBarButtonItemStyleDone target:self action:@selector(cancel:)];
    [self.listTableView setEditing:YES animated:YES];
    selectedCell = NO;
    [self showActionToolbar:YES];
    
}

- (void)cancel:(id)sender {
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage jsq_defaultTypingIndicatorImage] style:UIBarButtonItemStyleBordered target:self action:@selector(editPressed:)];
    [self.listTableView setEditing:NO animated:YES];
    selectedCell = YES;
    [self showActionToolbar:NO];
    
    arrIndexRowSelected = [NSMutableArray array];
}

- (void)deletePressed:(UIBarButtonItem *)sender {
    NSLog(@"deleting message...");
    NSMutableArray *arr = [NSMutableArray arrayWithArray:arrList];
    for (NSString *strIndex in arrIndexRowSelected) {
        [arr removeObjectAtIndex:[strIndex integerValue]];
        
    }
    NSString *userName = [[ShareData instance].currentUser.userName lowercaseString];
    NSString *keyUserDefautls = StringFormat(@"%@-message",userName);
    [[NSUserDefaults standardUserDefaults] setObject:arr forKey:keyUserDefautls];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self loadData];

}

- (void)showActionToolbar:(BOOL)show {
    CGRect toolbarFrame = actionToolbar.frame;
    CGRect tableViewFrame = self.listTableView.frame;
    CGFloat h = 45;
    
    self.listTableView.translatesAutoresizingMaskIntoConstraints = YES;
    
    if (show) {
        toolbarFrame.origin.y = toolbarFrame.origin.y - h;
        tableViewFrame.size.height -= toolbarFrame.size.height;
    } else {
        toolbarFrame.origin.y = toolbarFrame.origin.y + h;
        tableViewFrame.size.height += toolbarFrame.size.height;
    }
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    actionToolbar.frame = toolbarFrame;
    actionButton.enabled = NO;
    self.listTableView.frame = tableViewFrame;
    [self.view addSubview:actionToolbar];
    [UIView commitAnimations];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (arrList.count >0) {
        return arrList.count;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 99;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *simpleTableIdentifier = @"MessageListCell";
    MessageListViewCell *cell = (MessageListViewCell *)[tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MessageListViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    if (arrList.count >0) {
        NSInteger index = arrList.count - indexPath.row - 1;
        NSDictionary *dict = [arrList objectAtIndex:index];
        NSString *key = [dict.allKeys objectAtIndex:0];
        NSMutableArray *arr = [NSMutableArray array];
        [arr addObjectsFromArray:[dict objectForKey:key]];
        data = [arr lastObject];
        cell.lblName.text = data[@"sendfrom"];
        cell.lblMessage.text = data[@"message"];
        cell.lblDate.text = data[@"datetime"];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.delegate = self;
        //configure right buttons
        cell.rightButtons = @[[MGSwipeButton buttonWithTitle:@"Delete" backgroundColor:[UIColor redColor]]];
        cell.rightSwipeSettings.transition = MGSwipeTransition3D;
    }
    return cell;
}

#pragma mark - Table view delegate
- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 3;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger index = arrList.count - indexPath.row - 1;
    if (selectedCell == NO) {
        actionButton.enabled = YES;
        [arrIndexRowSelected addObject:StringFormat(@"%li",(long)index)];
        return;
    }
    ChatViewController *vc = [[ChatViewController alloc] init];
    NSDictionary *dict = [arrList objectAtIndex:index];
    NSString *key = [dict.allKeys objectAtIndex:0];
    NSMutableArray *arr = [NSMutableArray array];
    [arr addObjectsFromArray:[dict objectForKey:key]];
    data = [arr objectAtIndex:0];
    vc.sendFrom = data[@"sendfrom"];
    vc.sendFromRegID = data[@"sendFromRegID"];
    vc.data = arr;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSInteger index = arrList.count - indexPath.row - 1;
    
    for (NSString *strIndex in arrIndexRowSelected) {
        if ([strIndex integerValue] == index) {
            [arrIndexRowSelected removeObject:strIndex];
            break;
        }
    }
    
    if (arrIndexRowSelected.count <= 0) {
        actionButton.enabled = NO;
        return;
    }

}

- (BOOL)swipeTableCell:(MGSwipeTableCell *)cell tappedButtonAtIndex:(NSInteger)index direction:(MGSwipeDirection)direction fromExpansion:(BOOL)fromExpansion {
    
    if (direction == MGSwipeDirectionRightToLeft && index == 0) {
        //delete button clicked
        NSIndexPath * path = [self.listTableView indexPathForCell:cell];
        NSMutableArray *arr = [NSMutableArray arrayWithArray:arrList];
        [arr removeObjectAtIndex:path.row];
        NSString *userName = [[ShareData instance].currentUser.userName lowercaseString];
        NSString *keyUserDefautls = StringFormat(@"%@-message",userName);
        [[NSUserDefaults standardUserDefaults] setObject:arr forKey:keyUserDefautls];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self loadData];
        return NO; //Don't autohide to improve delete expansion animation
    }
    return YES;
}

- (void)loadData {
    arrIndexRowSelected = [NSMutableArray array];
    actionButton.enabled = NO;

    NSString *userName = [[ShareData instance].currentUser.userName lowercaseString];
    NSString *keyUserDefautls = StringFormat(@"%@-message",userName);
    arrList = [[[NSUserDefaults standardUserDefaults] arrayForKey:keyUserDefautls] mutableCopy];
    if (arrList.count <= 0) {
        self.listTableView.hidden = YES;
        return;
    }
    self.listTableView.hidden = NO;
    [self.listTableView reloadData];

}

@end
