//
//  Created by Jesse Squires
//  http://www.jessesquires.com
//
//
//  Documentation
//  http://cocoadocs.org/docsets/JSQMessagesViewController
//
//
//  GitHub
//  https://github.com/jessesquires/JSQMessagesViewController
//
//
//  License
//  Copyright (c) 2014 Jesse Squires
//  Released under an MIT license: http://opensource.org/licenses/MIT
//

#import "DemoModelData.h"

/**
 *  This is for demo/testing purposes only.
 *  This object sets up some fake model data.
 *  Do not actually do anything like this.
 */

@implementation DemoModelData

- (instancetype)initWithData:(NSArray *)arrData
{
    self = [super init];
    if (self) {
        self.data = arrData;
        [self loadFakeMessages];
        
        /**
         *  Create message bubble images objects.
         *
         *  Be sure to create your bubble images one time and reuse them for good performance.
         *
         */
        JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
        
        self.outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
        self.incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleBlueColor]];
    }
    
    return self;
}

- (void)loadFakeMessages {
    /**
     *  Load some fake messages for demo.
     */
    self.messages = [NSMutableArray array];
    for (NSDictionary *dict  in  self.data) {
        NSString *senderID = [dict objectForKey:@"sendfrom"];
        NSString *senderDisplayName = [dict objectForKey:@"sendfrom"];
        NSString *text = [dict objectForKey:@"message"];
        NSString *strDate = [dict objectForKey:@"datetime"];
        NSDate *date = [self convertStringToDate:strDate];
       [self.messages addObject:[[JSQMessage alloc] initWithSenderId:senderID senderDisplayName:senderDisplayName date:date text:text]];
    }
}

- (void)addPhotoMediaMessage {
    //JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageNamed:@"goldengate"]];
    //JSQMessage *photoMessage = [JSQMessage messageWithSenderId:kJSQDemoAvatarIdSquires
    //                                               displayName:kJSQDemoAvatarDisplayNameSquires
    //                                                     media:photoItem];
    //[self.messages addObject:photoMessage];
}

- (void)addLocationMediaMessageCompletion:(JSQLocationMediaItemCompletionBlock)completion {
    CLLocation *ferryBuildingInSF = [[CLLocation alloc] initWithLatitude:37.795313 longitude:-122.393757];
    
    JSQLocationMediaItem *locationItem = [[JSQLocationMediaItem alloc] init];
    [locationItem setLocation:ferryBuildingInSF withCompletionHandler:completion];
    
    //JSQMessage *locationMessage = [JSQMessage messageWithSenderId:kJSQDemoAvatarIdSquires
    //                                                  displayName:kJSQDemoAvatarDisplayNameSquires
    //                                                        media:locationItem];
    //[self.messages addObject:locationMessage];
}

- (void)addVideoMediaMessage {
    // don't have a real video, just pretending
    //NSURL *videoURL = [NSURL URLWithString:@"file://"];
    
   // JSQVideoMediaItem *videoItem = [[JSQVideoMediaItem alloc] initWithFileURL:videoURL isReadyToPlay:YES];
    //JSQMessage *videoMessage = [JSQMessage messageWithSenderId:kJSQDemoAvatarIdSquires
    //                                               displayName:kJSQDemoAvatarDisplayNameSquires
    //                                                     media:videoItem];
    //[self.messages addObject:videoMessage];
}

#pragma mark - convert string to date
- (NSDate *)convertStringToDate:(NSString *)dateStringInput {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    // this is imporant - we set our input date format to match our input string
    // if format doesn't match you'll get nil from your string, so be careful
    [dateFormatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"GMT+7"];
    [dateFormatter setTimeZone:timeZone];
    NSDate *dateFromString = [[NSDate alloc] init];
    dateFromString = [dateFormatter dateFromString:dateStringInput];
    return dateFromString;
}

@end
