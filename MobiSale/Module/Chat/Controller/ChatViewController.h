//
//  ChatViewController.h
//  MobiSale
//
//  Created by ISC-DanTT on 3/1/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JSQMessagesViewController/JSQMessage.h>
#import <JSQMessagesViewController/JSQMessagesViewController.h>
#import <JSQMessagesViewController/JSQMessagesBubbleImage.h>
#import "NSUserDefaults+Notification.h"
#import "DemoModelData.h"

BOOL isChatViewActived;

@interface ChatViewController :JSQMessagesViewController <UIActionSheetDelegate, JSQMessagesComposerTextViewPasteDelegate>

@property (strong, nonatomic) DemoModelData *demoData;
@property (strong, nonatomic) NSString *sendFrom;
@property (strong, nonatomic) NSString *sendFromRegID;
@property (strong, nonatomic) NSArray *data;

@end
