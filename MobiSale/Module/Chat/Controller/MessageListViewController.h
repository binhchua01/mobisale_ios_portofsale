//
//  MessageListViewController.h
//  MobiSale
//
//  Created by ISC-DanTT on 12/3/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MessageRecord.h"
#import "BaseViewController.h"
#import "CustomerCareRecord.h"

@interface MessageListViewController : BaseViewController <UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) IBOutlet UITableView *listTableView;
@property (retain, nonatomic) MessageRecord *record;
@property (strong, nonatomic) CustomerCareRecord *customerRecord;
@property (strong, nonatomic) NSDictionary *data;

@end
