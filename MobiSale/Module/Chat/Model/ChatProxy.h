//
//  ChaProxy.h
//  MobiSale
//
//  Created by ISC-DanTT on 11/11/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BaseProxy.h"

@interface ChatProxy : BaseProxy

// Gửi tin nhắn chat
- (void) sendMessageTo:(NSString *)targetRegID notification:(NSDictionary*)notification data:(NSDictionary *)data completeHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
@end
