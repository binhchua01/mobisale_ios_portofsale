//
//  ChaProxy.m
//  MobiSale
//
//  Created by ISC-DanTT on 11/11/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ChatProxy.h"
#import "ShareData.h"
#import "FPTUtility.h"
#define urlGCMAPI                       @"https://gcm-http.googleapis.com/gcm/send"
#define GCMAPIKey                       @"key=AIzaSyAe_fdTfmZW1J05crNBAC5doseTBYJHVcc"
@implementation ChatProxy

// Gửi tin nhắn chat
- (void) sendMessageTo:(NSString *)targetRegID notification:(NSDictionary*)notification data:(NSDictionary *)data completeHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler{
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@",urlGCMAPI)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    
    [dict setObject:data forKey:@"data"];
    [dict setObject:targetRegID forKey:@"to"];
    [dict setObject:notification forKey:@"notification"];
    [dict setObject:@"high" forKey:@"priority"];
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [(NSMutableURLRequest*)callOp.request setValue:GCMAPIKey forHTTPHeaderField:@"Authorization"];

    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endSendMessage:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
    
}

- (void)endSendMessage:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    

    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSString *success = [jsonDict objectForKey:@"success"];
    if ([success intValue] != 0) {
        handler(nil, @"1", @"Đã gửi");
        return;
    }
    handler(nil, @"0", @"Gửi thất bại");
    return;

}

@end
