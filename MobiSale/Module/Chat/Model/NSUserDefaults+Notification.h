//
//  NSUserDefaults+Notification.h
//  MobiSale
//
//  Created by ISC-DanTT on 3/4/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

#define LOG_MPOS_KEY    @"LogMPOSKey"

@interface NSUserDefaults (Notification)

+ (void)saveNotifications:(NSDictionary *)data withKey:(NSString *)key;

+ (void)saveMessages:(NSDictionary *)data withKey:(NSString *)key userName:(NSString *)userName;

+ (void)saveLogMPOS:(NSDictionary *)data withKey:(NSString *)key;

@end
