//
//  NSUserDefaults+Notification.m
//  MobiSale
//
//  Created by ISC-DanTT on 3/4/16.
//  Copyright (c) 2016 FPT.RAD.FTool. All rights reserved.
//

#import "NSUserDefaults+Notification.h"

@implementation NSUserDefaults (Notification)

+ (void)saveNotifications:(NSDictionary *)data withKey:(NSString *)key {
    //save receive notification for notice
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *arrUser      = [[[NSUserDefaults standardUserDefaults] arrayForKey:key] mutableCopy];
    if (arrUser == nil) {
        arrUser = [NSMutableArray new];
    }
    [arrUser addObject:data];
    [userDefaults setObject:arrUser forKey:key];
    [userDefaults synchronize];
    
}
//Vutt11 
+ (void)saveMessages:(NSDictionary *)data withKey:(NSString *)key userName:(NSString *)userName {
    // save receive notification for chat
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *messages = [[[NSUserDefaults standardUserDefaults] arrayForKey:key] mutableCopy];
    if (messages == nil) {
        messages = [NSMutableArray new];
    }
    NSMutableDictionary *dictMessage = [NSMutableDictionary dictionary];
    NSMutableArray *arr = [NSMutableArray array];
    if (messages.count > 0) {
        for (NSDictionary *dict in  messages) {
            NSString *str = [dict.allKeys objectAtIndex:0];
            if ([str isEqualToString:userName]) {
                [arr addObjectsFromArray:[dict objectForKey:userName]];
                if (arr.count > 99) {
                    [arr removeLastObject];
                }
                [arr addObject:data];
                [dictMessage setObject:arr forKey:str];
                [messages removeObject:dict];
                [messages addObject:dictMessage];
                [userDefaults setObject:messages forKey:key];
                [userDefaults synchronize];
                return;
            }
        }
        [arr addObject:data];
        [dictMessage setObject:arr forKey:userName];
        [messages addObject:dictMessage];
    }
    if (messages.count <= 0) {
        [arr addObject:data];
        [dictMessage setObject:arr forKey:userName];
        [messages addObject:dictMessage];
    }
    [userDefaults setObject:messages forKey:key];
    [userDefaults synchronize];

}

+ (void)saveLogMPOS:(NSDictionary *)data withKey:(NSString *)key {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSMutableArray *arr = [[[NSUserDefaults standardUserDefaults] arrayForKey:key] mutableCopy];
    if (arr == nil) {
        arr = [NSMutableArray new];
    }
    [arr addObject:data];
    [userDefaults setObject:arr forKey:key];
    [userDefaults synchronize];
    
    NSLog(@"----Logs mPOS Content: %@", [arr description]);
}

@end
