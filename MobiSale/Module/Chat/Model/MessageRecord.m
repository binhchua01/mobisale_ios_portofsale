//
//  MessageRecord.m
//  MobiSale
//
//  Created by ISC-DanTT on 12/3/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "MessageRecord.h"

@implementation MessageRecord
@synthesize category, datetime, image, isBookmark, isDelete, isRead, title, message, sendFromRegID, sendFrom, to;
@end
