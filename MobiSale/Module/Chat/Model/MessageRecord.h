//
//  MessageRecord.h
//  MobiSale
//
//  Created by ISC-DanTT on 12/3/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MessageRecord : NSObject
@property (nonatomic,retain) NSString *category;
@property (nonatomic,retain) NSString *datetime;
@property (nonatomic,retain) NSString *image;
@property (nonatomic) bool isBookmark;
@property (nonatomic) bool isDelete;
@property (nonatomic) bool isRead;
@property (nonatomic,retain) NSString *title;
@property (nonatomic,retain) NSString *message;
@property (nonatomic,retain) NSString *sendFromRegID;
@property (nonatomic,retain) NSString *sendFrom;
@property (nonatomic,retain) NSString *to;
@end
