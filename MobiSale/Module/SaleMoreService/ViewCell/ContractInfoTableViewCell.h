//
//  ContractInfoTableViewCell.h
//  MobiSale
//
//  Created by ISC on 8/8/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContractInfoCellModel.h"

@interface ContractInfoTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *fullNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *contractLabel;
@property (weak, nonatomic) IBOutlet UILabel *serviceTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *servicePackageLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusComboLabel;

@property (strong, nonatomic) ContractInfoCellModel *cellModel;

@end
