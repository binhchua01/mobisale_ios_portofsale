//
//  ContractInfoTableViewCell.m
//  MobiSale
//
//  Created by ISC on 8/8/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ContractInfoTableViewCell.h"
#import "UILabel+FPTCustom.h"

@implementation ContractInfoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [self setDefaultData];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)drawRect:(CGRect)rect {
    [self.fullNameLabel autoFixFrame];
    [self.addressLabel autoFixFrame];
    
}
- (void)setCellModel:(ContractInfoCellModel *)cellModel {
    _cellModel = cellModel;
    self.fullNameLabel.text       = _cellModel.fullName;
    self.addressLabel.text        = _cellModel.address;
    self.phoneLabel.text          = _cellModel.phone;
    self.contractLabel.text       = _cellModel.contract;
    self.serviceTypeLabel.text    = _cellModel.serviceTypeName;
    self.servicePackageLabel.text = _cellModel.localTypeName;
    self.statusComboLabel.text    = _cellModel.comboStatusName;

}

- (void)setDefaultData {
    self.fullNameLabel.text       = @"";
    self.addressLabel.text        = @"";
    self.phoneLabel.text          = @"";
    self.contractLabel.text       = @"";
    self.serviceTypeLabel.text    = @"";
    self.servicePackageLabel.text = @"";
    self.statusComboLabel.text    = @"";
}

@end
