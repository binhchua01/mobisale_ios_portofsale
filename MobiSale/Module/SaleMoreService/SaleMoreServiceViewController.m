//
//  SaleMoreServiceViewController.m
//  MobiSale
//
//  Created by ISC on 8/8/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

/*
 -	Yêu cầu:
 o	Thêm chức năng tìm Hợp đồng vào Menu “Bán hàng”.
 o	Tạo trang tìm kiếm HĐ, bao gồm:
 ♣	Loại tìm kiếm: tìm theo số HĐ/Họ tên/Số điện thoại.
 ♣	Nội dung tìm kiếm: do người dùng nhập vào.
 ♣	Nút tìm kiếm.
 ♣	Danh sách kết quả trả về:
 •	Họ tên KH;
 •	Số HĐ;
 •	Gói dịch vụ:
 •	Địa chỉ;
 •	Loại dịch vụ đã đăng ký (Internet/IPTV/Internet + IPTV);
 •	Tình trạng combo (đã/chưa đăng ký Combo);
 -	Thực hiện: API 78 (Tìm HĐ)
 */

#import "SaleMoreServiceViewController.h"
#import "ContractInfoTableViewCell.h"
#import "ContractInfoCellModel.h"
#import "InfoContractViewController.h"
#import "KeyValueModel.h"
#import "NIDropDown.h"
#import "ShareData.h"
#import "NSString+GetStringHeight.h"

@interface SaleMoreServiceViewController () <NIDropDownDelegate, UITableViewDelegate, UITableViewDataSource>

@end

@implementation SaleMoreServiceViewController {
    NIDropDown          *dropDownView; // drop down view show when clicked button
    NSMutableArray      *searchResultArray;    // data received from server
    BOOL                isSearchViewShow; // show/hide search conditions
    NSArray             *agentArray;
    KeyValueModel       *selectedAgent;
    ContractInfoCellModel   *record;
    NSString * agentName;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    // Set title for View
    self.screenName = self.title = @"BÁN THÊM DỊCH VỤ";
    
    [self setDefaultData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Set default data
- (void)setDefaultData {
    /*
     *  Set data condition search
     */
    
    KeyValueModel *item1 = [[KeyValueModel alloc] initWithName:@"1" description:@"Số hợp đồng"];
    KeyValueModel *item2 = [[KeyValueModel alloc] initWithName:@"2" description:@"Họ tên KH"];
    KeyValueModel *item3 = [[KeyValueModel alloc] initWithName:@"3" description:@"Số điện thoại"];
    agentArray = [NSArray arrayWithObjects:item1, item2, item3, nil];
    selectedAgent = item1;
    [self.agentButton setTitle:item1.Values forState:UIControlStateNormal];
    // show conditions search view
    isSearchViewShow = YES;
    // set empty result number
    self.resultNumberLabel.text = @"";
    // Init data
    searchResultArray = [NSMutableArray array];
}

#pragma mark - Handle event touch on screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self handleSingleTap:nil];
}

// Hide keyboard and drop down view if them show
- (void)handleSingleTap:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
    if (dropDownView !=nil) {
        [dropDownView hideDropDown:self.agentButton];
        dropDownView = nil;
    }
}


#pragma mark - show or hide conditions search view
- (void)showConditionsSearchView:(BOOL)status {
    isSearchViewShow = status;
    
    /*
     * show conditions search View
     */
    if (status) {
        self.searchViewHeight.constant = 131;
        self.upDownImageView.image = [UIImage imageNamed:@"up-icon-512"];
        
    } else {
        /*
         * hide conditions search View
         */
        self.searchViewHeight.constant = 5;
        self.upDownImageView.image = [UIImage imageNamed:@"down-icon-512"];
    }
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

#pragma mark - NIDropDown method
- (void)showDropDownAtButton:(UIButton *)sender withArrayData:(NSArray*)arrData{
    [self.view endEditing:YES];
    
    if (dropDownView == nil) {
        NSMutableArray *arrOutput = [[NSMutableArray alloc] init];
        for (KeyValueModel *item in arrData) {
            [arrOutput addObject:item.Values];
        }
        
        CGFloat height = arrOutput.count *40;
        CGFloat width = 0;
        dropDownView = [[NIDropDown alloc] showDropDownAtButton:sender height:&height width:&width data:arrOutput images:nil animation:@"down"];
        dropDownView.delegate = self;
        
        return;
    }
    [dropDownView hideDropDown:sender];
    [self rel];
}

- (void)niDropDownDelegateMethod:(NIDropDown *)sender {
    [self rel];
}

- (void)rel {
    dropDownView = nil;
}

- (void)dropDownListView:(NIDropDown *)dropDownListView didSelectIndexPath:(NSIndexPath *)indexPath atButton:(UIButton *)button{
    [self didDropDownSelected:indexPath.row atButton:button];
}

- (void)didDropDownSelected:(NSInteger)row atButton:(UIButton *)button {
    if (button == self.agentButton) {
        selectedAgent = agentArray[row];
        return;
    }
    
}
#pragma mark - UITableViewDataSource Methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return searchResultArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (searchResultArray.count > 0) {
        ContractInfoCellModel *item = searchResultArray[indexPath.row];
        CGFloat heightFullName = [item.fullName getStringHeightWithFontSize:14 withSizeMake:CGSizeMake(279, 21)];
        CGFloat heightAddress = [item.address getStringHeightWithFontSize:14 withSizeMake:CGSizeMake(149, 21)];
        
        return 200 +heightAddress + heightFullName;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"ContractInfoCellID";
    ContractInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ContractInfoTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.cellModel = searchResultArray[indexPath.row];
    
    return cell;
}

#pragma mark - UITableViewDelegate Methods
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.resultTableView deselectRowAtIndexPath:indexPath animated:YES];
    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"InfoContract" bundle:nil];
    
    InfoContractViewController *vc = [[InfoContractViewController alloc] init];
    vc = [storyboard instantiateViewControllerWithIdentifier:@"InfoContract"];
    ContractInfoCellModel *cellModel = searchResultArray[indexPath.row];
    vc.contract = cellModel.contract;
    
    [self.navigationController pushViewController:vc animated:YES];
    return;
    
}

#pragma mark - Action pressed button
// action clicked button search condition for show/hide conditions
- (IBAction)searchViewButtonPressed:(id)sender {
    [self handleSingleTap:nil];
    [self showConditionsSearchView:!isSearchViewShow];
    
}

// action pressed button agent
- (IBAction)agentButtonPressed:(id)sender {
    [self.view endEditing:YES];
    
    if (dropDownView != nil) {
        [dropDownView hideDropDown:sender];
        dropDownView = nil;
        return;
    }
    [self showDropDownAtButton:sender withArrayData:agentArray];
}

// action clicked button search (icon search)
- (IBAction)searchButtonPressed:(id)sender {
    [self.view endEditing:YES];
    
    [self handleSingleTap:nil];
    
    [self getContractList];
}


#pragma mark - Get contract list from search result
- (void)getContractList {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    ShareData *shareData = [ShareData instance];
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    
    if ([selectedAgent.Key isEqualToString:@"3"]) {
      agentName = [self.agentTextField.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    }else {
        
        agentName = self.agentTextField.text;
    }
    
    [dict setObject:shareData.currentUser.userName forKey:@"UserName"];
    [dict setObject:selectedAgent.Key ?:@""        forKey:@"Agent"];
    [dict setObject:agentName      forKey:@"AgentName"];
    
    [self showMBProcess];
    
    [shareData.saleMoreServiceProxy getContractList:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        [self hideMBProcess];
        
        searchResultArray  = result;
        self.resultNumberLabel.text = StringFormat(@"%lu", (unsigned long)searchResultArray.count);
        
        if([errorCode isEqualToString:@"0"]){
            if(searchResultArray.count > 0){
                [self showConditionsSearchView:NO];
                [self.resultTableView setHidden:NO];
                [self.resultTableView reloadData];
                return;
            }
        }
        
        [searchResultArray removeAllObjects];
        [self.resultTableView reloadData];
        [self showAlertBox:@"Thông báo" message:message];
        
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[error localizedDescription]];
    }];
    
}

@end
