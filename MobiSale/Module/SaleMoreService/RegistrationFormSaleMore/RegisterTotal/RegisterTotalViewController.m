//
//  RegisterTotalViewController.m
//  MobiSale
//
//  Created by ISC on 8/10/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "RegisterTotalViewController.h"
#import "RegistrationFormModel.h"

@interface RegisterTotalViewController ()<UIAlertViewDelegate, UIActionSheetDelegate>

@end

@implementation RegisterTotalViewController

- (NSMutableArray *)arrayListDevices {
    if (!_arrayListDevices) {
        _arrayListDevices = [[NSMutableArray alloc] init];
    }
    return _arrayListDevices;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(contentChangedNotification:)
                                                 name:@"IPTVTotal"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(contentChangedNotification:)
                                                 name:@"InternetTotal"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(contentChangedNotification:)
                                                 name:@"DeviceTotal"
                                               object:nil];
    [self setViewType];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];

    [self getData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setViewType {
    [self.updateButton setShadow];
    [self.updateButton styleButtonUpdate];
    [self.contentView styleBorderMap];
    
}
- (void)contentChangedNotification:(NSNotification *)notification {
    //[self getData];
}

#pragma mark - UIButton action
- (IBAction)updateButtonPressed:(id)sender {
    [self checkUpdate];
}

#pragma mark - Load data for form when begin load view
/* Load data when load view */
- (void)getData {
    self.iptvMoney = [self.updateRegistrationContractModel.iptvTotal integerValue];
    self.internetMoney = [self.updateRegistrationContractModel.internetTotal integerValue];
    self.devicesMoney =  [[RegistrationFormModel sharedInstance] totalDevice ] ;
    
    self.totalMoney = self.iptvMoney + self.internetMoney + [self.devicesMoney integerValue];
    
    self.updateRegistrationContractModel.total = StringFormat(@"%li", (long)self.totalMoney);
    
    [self setTextForAmountTextField:self.internetMoneyTextField andData:self.internetMoney];
    [self setTextForAmountTextField:self.iptvMoneyTextField andData:self.iptvMoney];
    [self setTextForAmountTextField:self.totalMoneyTextField andData:self.totalMoney];
    
    
    if (![self.devicesMoney isEqualToString:@"0" ]) {
          [self setTextForAmountTextField:self.devicesMoneyTextField andData:[self.devicesMoney integerValue]];
    } else {
        
        [self setTextForAmountTextField:self.devicesMoneyTextField andData:0];
    }
    
}


// Set label money total
- (void)setTextForAmountTextField:(UITextField *)textField andData:(NSInteger )data {
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    
    [textField setText:StringFormat(@"%@ VNĐ ", [nf stringFromNumber:[NSNumber numberWithFloat:data]])];
}

#pragma mark - Update Data
// Checking data before update registration form
- (void)checkUpdate {
    
    //checking Device
    
    self.arrayListDevices = [[RegistrationFormModel sharedInstance] listDevicePackagesArray];
    if (![self.arrayListDevices isEqual:[NSNull null]]) {
        
        for (int i = 0; i < self.arrayListDevices.count; i++) {
            NSString *devicePromotionText;
            devicePromotionText = StringFormat(@"%@",[self.arrayListDevices[i] valueForKey:@"PromotionDeviceText"]);
            if ([devicePromotionText isEqualToString:@"Vui lòng chọn CLKM"]) {
                [self showAlertBox:@"Thông báo" message:@"Vui lòng chọn câu lệnh khuyến mãi của thiết bị"];
                return;
            }
        }
    }
    
   
    //checking Internet

    if (self.updateRegistrationContractModel.serviceType == 1) {
        if (self.updateRegistrationContractModel.checkSwitchInternet == YES) {
            if ([self.updateRegistrationContractModel.localType isEqual:@"0"]) {
                // Chưa chọn gói dịch vụ Internet
               // [self callbackWithMessage:@"Vui lòng chọn Gói dịch vụ Internet!"];
                [self showAlertBox:@"Thông báo" message:@"Vui lòng chọn Gói dịch vụ Internet!" ];
                return;
            }
        // if ([self.updateRegistrationContractModel.promotionID isEqual:@"0"])
            if ([self.updateRegistrationContractModel.contractPromotionID isEqual:@"0"]) {
                // Chưa chọn CLKM internet
                //[self callbackWithMessage:@"Vui lòng chọn Khuyến mãi dịch vụ Internet!"];
                 [self showAlertBox:@"Thông báo" message:@"Vui lòng chọn Khuyến mãi dịch vụ Internet!"];
                return;
            }
            
        }
    }
    //Vutt11
    //checking IPTV
    if ([self.updateRegistrationContractModel.iptvBoxCount integerValue] > 0) 
    {
        
        if ([self.updateRegistrationContractModel.iptvPackage isEqual:@"0"]) {
        // [self callbackWithMessage:@"Vui lòng chọn gói dịch vụ IPTV!"];
        [self showAlertBox:@"Thông báo" message:@"Vui lòng chọn gói dịch vụ IPTV!" ];
        return;
        }
        if (self.updateRegistrationContractModel.serviceType == 0)
        {
            if ([self.updateRegistrationContractModel.iptvPromotionID isEqual:@"0"]) {
                //[self callbackWithMessage:@"Vui lòng chọn khuyến mãi Box đầu tiên!"];
                [self showAlertBox:@"Thông báo" message:@"Vui lòng chọn khuyến mãi Box đầu tiên!"];
                return;
            }
            if ([self.updateRegistrationContractModel.iptvBoxCount integerValue] > 1 && [self.updateRegistrationContractModel.iptvPromotionIDBoxOrder isEqualToString:@"0"]) {
                [self showAlertBox:@"Thông báo" message:@"Vui lòng chọn khuyến mãi Box thứ hai!"];
                return;
                
            }
        }
        else
        {
            if ([self.updateRegistrationContractModel.iptvPromotionIDBoxOrder isEqualToString:@"0"]) {
                [self showAlertBox:@"Thông báo" message:@"Vui lòng chọn khuyến mãi Box thứ hai!"];
                return;
                
            }
        }
//        if (self.updateRegistrationContractModel.serviceType == 0 && [self.updateRegistrationContractModel.iptvPromotionID isEqual:@"0"]) {
//            //[self callbackWithMessage:@"Vui lòng chọn khuyến mãi Box đầu tiên!"];
//             [self showAlertBox:@"Thông báo" message:@"Vui lòng chọn khuyến mãi Box đầu tiên!"];
//            return;
//        }
//        else {
//            
//            if ([self.updateRegistrationContractModel.iptvPromotionIDBoxOrder isEqualToString:@"0"]) {
//                [self showAlertBox:@"Thông báo" message:@"Vui lòng chọn khuyến mãi Box thứ hai!"];
//                return;
//
//            }
//        }
    }
    
    //vutt11 fixed
    //&& self.updateRegistrationContractModel.iptvPromotionIDBoxOrder == 0
//    if ([self.updateRegistrationContractModel.iptvBoxCount integerValue] >= 2 && [self.updateRegistrationContractModel.iptvPromotionIDBoxOrder isEqualToString:@"0"]) {
//        
//        [self showAlertBox:@"Thông báo" message:@"Vui lòng chọn khuyến mãi Box thứ 2"];
//        return;
//    }
    
    if (![self.updateRegistrationContractModel.contractPromotionID isEqual:@"0"] && self.updateRegistrationContractModel.contractPromotionID.length > 0) {
        [self showActionSheetWithTitle:@"Bạn chắc chắn muốn cập nhật Phiếu đăng ký này?" andTag:1];
        return;
    }
    
    if (![self.updateRegistrationContractModel.iptvBoxCount isEqual:@"0"] && self.updateRegistrationContractModel.iptvBoxCount.length > 0 ) {
        [self showActionSheetWithTitle:@"Bạn chắc chắn muốn cập nhật Phiếu đăng ký này?" andTag:1];
        return;
    }
    
    [self showAlertViewWithTitle:@"Thông báo" message:@"Bạn chưa chọn loại dịch vụ (internet hoặc IPTV) nào nên không thể cập nhật Phiếu đăng ký!" tag:UpdateError];
    
    
    //kiem tra so dien thoai

}

// Update registration form after checked data success
- (void)updateRegistrationForm {
    // Kiểm tra kết nối internet
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:[self.updateRegistrationContractModel setupData] forKey:@"Register"];
    ShareData *shared = [ShareData instance];
    [shared.saleMoreServiceProxy updateRegistrationContract:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        NSInteger code = [errorCode integerValue];
        if(code < 0){
            // Update error
            [self showAlertViewWithTitle:@"Lỗi!" message:message tag:UpdateError];
            
        } else {
            // Update success
            [self showAlertViewWithTitle:@"Thông báo" message:message tag:UpdateSuccess];
        }
        
        [self hideMBProcess];
        return ;
        
    } errorHandler:^(NSError *error) {
        [self showAlertViewWithTitle:@"Lỗi" message:[error localizedDescription] tag:UpdateError];
        [self hideMBProcess];
    }];
}

#pragma mark - Implement UIActionSheet
// UIActionSheetDelegate methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (actionSheet.tag == 1 && buttonIndex == 0) {
        [self showMBProcess];
        [self updateRegistrationForm];

    }
}
// Show ActionSheet view
- (void)showActionSheetWithTitle:(NSString *)title andTag:(NSInteger)tag {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:title delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
    actionSheet.tag = tag;
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}


#pragma mark - Implement alert View
// Show Alert view
- (void)showAlertViewWithTitle:(NSString *)title message:(NSString *)message tag:(NSInteger)tag {
    UIAlertView *alertView;
    
    switch (tag) {
        case UpdateSuccess:
        case UpdateError:
            alertView = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            break;

        default:
            break;
    }
    alertView.tag = tag;
    [alertView show];
}

// UIAlert View Delegate method
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        switch (alertView.tag) {
            case UpdateError:
                break;
            case UpdateSuccess:
                if (self.delegate) {
                    [self.delegate updateRegistrationContractSuccess];
                }
                break;
            default:
                break;
        }
    }
    
}

@end
