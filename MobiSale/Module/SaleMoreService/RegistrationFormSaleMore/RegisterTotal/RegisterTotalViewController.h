//
//  RegisterTotalViewController.h
//  MobiSale
//
//  Created by ISC on 8/10/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "RegistrationFormSaleMorePageViewController.h"

enum ActionSheetTagType {
    back = 1,
    save = 2
};

enum AlertViewTagType {
    UpdateSuccess = 1,
    UpdateError = 2
};
@protocol RegisterTotalViewDelegate <NSObject>

@optional
- (void)updateRegistrationContractSuccess;

@end

@interface RegisterTotalViewController : BaseViewController
@property (strong, nonatomic) UpdateRegistrationContractModel *updateRegistrationContractModel;

@property (strong, nonatomic)  id<RegisterTotalViewDelegate> delegate;

// View Type: Create or Update Registration Form
@property (assign, nonatomic) ViewType viewType;

// Tiền IPTV
@property (assign, nonatomic) NSInteger iptvMoney;
// Tiền Internet
@property (assign, nonatomic) NSInteger internetMoney;
//Tien DEVICE
@property (assign, nonatomic) NSString * devicesMoney;
// Tổng tiền = Tiền IPTV + Tiền Internet
@property (assign, nonatomic) NSInteger totalMoney;
//mang array thiet bi
@property (strong, nonatomic)NSMutableArray *arrayListDevices;

@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UITextField *iptvMoneyTextField;
@property (strong, nonatomic) IBOutlet UITextField *internetMoneyTextField;
@property (strong, nonatomic) IBOutlet UITextField *totalMoneyTextField;
@property (weak, nonatomic) IBOutlet UITextField *devicesMoneyTextField;
@property (strong, nonatomic) IBOutlet UIButton *updateButton;

- (void)getData;

@end
