//
//  IPTVServiceViewController.h
//  MobiSale
//
//  Created by ISC on 8/10/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "RegistrationFormSaleMorePageViewController.h"


/*--------------------------------------------------------------------------*/

#define MAXIMUM 100
#define MINIMUM 0

@protocol IncreaseDecreaseViewDelegate <NSObject>

@optional
- (void)returnDataFrom:(UIView *)increaseDecreaseView;

@end

@interface IncreaseDecreaseView : UIView
@property (assign, nonatomic) NSInteger maximum;
@property (assign, nonatomic) NSInteger minimum;
@property (assign, nonatomic) NSInteger count;

@property (strong, nonatomic) id <IncreaseDecreaseViewDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIButton *plusButton;
@property (weak, nonatomic) IBOutlet UIButton *minusButton;
@property (weak, nonatomic) IBOutlet UITextField *numberTextField;

- (IBAction)buttonPressed:(id)sender;

- (void)reloadData;

@end

/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/

@interface ExtraPackageItemView : UIView

@property (weak, nonatomic) IBOutlet IncreaseDecreaseView *chargeTimesView;
@property (weak, nonatomic) IBOutlet IncreaseDecreaseView *prepaidMonthView;
@property (weak, nonatomic) IBOutlet UIButton *checkBoxButton;
@property (weak, nonatomic) IBOutlet UILabel *moneyPackageLable;

- (void)reloadData;

@end

/*--------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------*/

@protocol IPTVServiceViewDelegate <NSObject>

@optional
- (void)callbackViewWithMessage:(NSString *)message;

@end

@interface IPTVServiceViewController : BaseViewController <IncreaseDecreaseViewDelegate>

@property (strong, nonatomic) UpdateRegistrationContractModel *updateRegistrationContractModel;

@property (strong, nonatomic) id<IPTVServiceViewDelegate> delegate;

// View Type: Create or Update Registration Form
@property (assign, nonatomic) ViewType viewType;

@property (strong, nonatomic) NSMutableArray *deploymentFormArray;
@property (strong, nonatomic) NSMutableArray *servicePackageArray;
@property (strong, nonatomic) NSMutableArray *promotionFirstBoxArray;
@property (strong, nonatomic) NSMutableArray *promptionSecondBoxArray;
@property (assign, nonatomic) NSInteger amountTotal;
@property (assign, nonatomic) NSInteger prepaidTotal;
@property (assign, nonatomic) NSInteger deviceTotal;

@property (assign, nonatomic) BOOL flagTotalIPTV;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeightLC;

// Yêu cầu cài đặt
@property (weak, nonatomic) IBOutlet UISwitch *installRequestSwitch;
// Yêu cầu khoan tường
@property (weak, nonatomic) IBOutlet UISwitch *wallDrillingRequest;

// Hình thức triển khai
@property (weak, nonatomic) IBOutlet UIButton *deploymentFormButton;
// Gói dịch vụ
@property (weak, nonatomic) IBOutlet UIButton *servicePackageButton;
// Khuyến mãi Box đầu tiên
@property (weak, nonatomic) IBOutlet UIButton *promotionFirstBoxButton;
// Khuyễn mãi Box thứ 2 trở đi
@property (weak, nonatomic) IBOutlet UIButton *promptionSecondBoxButton;

// Số lượng BOX
@property (strong, nonatomic) IBOutlet IncreaseDecreaseView *boxCountView;
// Số lượng PLC
@property (strong, nonatomic) IBOutlet IncreaseDecreaseView *plcCountView;
// Số STB thu hồi
@property (strong, nonatomic) IBOutlet IncreaseDecreaseView *stbRecoveryView;
// Số lần tính cước
@property (strong, nonatomic) IBOutlet IncreaseDecreaseView *chargeTimesView;

// Fim +
@property (strong, nonatomic) IBOutlet ExtraPackageItemView *fimPlusView;
// Fim hot
@property (strong, nonatomic) IBOutlet ExtraPackageItemView *fimHotView;
// K +
@property (strong, nonatomic) IBOutlet ExtraPackageItemView *kPlusView;
// Đặc sắc HD
@property (strong, nonatomic) IBOutlet ExtraPackageItemView *hdFeaturedView;
// VTV Cab
@property (strong, nonatomic) IBOutlet ExtraPackageItemView *vtvCabView;

// Giá câu lệnh khuyến mãi Box đầu tiên
@property (strong, nonatomic) IBOutlet UILabel *promotionFirstBoxPriceLabel;
// Giá câu lệnh khuyến mãi Box thứ 2 trở đi
@property (strong, nonatomic) IBOutlet UILabel *promotionSecondBoxPriceLabel;

// Tổng tiền
@property (weak, nonatomic) IBOutlet UILabel *moneyTotalLabel;

- (IBAction)switchPressed:(id)sender;
- (IBAction)buttonSelectPressed:(id)sender;
- (IBAction)buttonReloadTotalAmountPressed:(id)sender;

@end

