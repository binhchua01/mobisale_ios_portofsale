//
//  IPTVServiceViewController.m
//  MobiSale
//
//  Created by ISC on 8/10/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "IPTVServiceViewController.h"

/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/

@implementation IncreaseDecreaseView

- (void)drawRect:(CGRect)rect {
    [self.plusButton setStyle];
    [self.minusButton setStyle];
    self.numberTextField.userInteractionEnabled = NO;
    if (self.maximum <= 0) {
        self.maximum = MAXIMUM;
    }
    
}

- (void)reloadData {
    FlagTotalIPTV = NO;

    NSInteger number = [self.numberTextField.text integerValue];
    if (number > self.maximum) {
        number = self.maximum;
    }
    [self.numberTextField setText:StringFormat(@"%li", (long)number)];
    
}

- (IBAction)buttonPressed:(id)sender {
    FlagTotalIPTV = NO;
    
    NSInteger number = [self.numberTextField.text integerValue];
    if (sender == self.plusButton) {
        // increase 1
        number ++;
        if (number > self.maximum) {
            number = self.maximum;
        }
    }
    else if (sender == self.minusButton) {
        // decrease 1
        number --;
        if (number < self.minimum) {
            number = self.minimum;
        }
    }
    self.count = number;
    [self.numberTextField setText:StringFormat(@"%li", (long)number)];
    [self.numberTextField layoutIfNeeded];
    
    if (self.delegate) {
        [self.delegate returnDataFrom:self];
    }
}

@end

/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/

@implementation ExtraPackageItemView
- (void) drawRect:(CGRect)rect {
    [self styleBorderMap];
}

- (void)reloadData {
    FlagTotalIPTV = NO;

    self.checkBoxButton.selected = NO;
    self.chargeTimesView.userInteractionEnabled = NO;
    self.prepaidMonthView.userInteractionEnabled = NO;
    [self.chargeTimesView.numberTextField setText:@"0"];
    [self.prepaidMonthView.numberTextField setText:@"0"];
    
}

- (IBAction)checkBoxButtonPressed:(id)sender {
    FlagTotalIPTV = NO;

    self.checkBoxButton.selected = !self.checkBoxButton.selected;
    if (self.checkBoxButton.selected) {
        // if check box is YES, enable Increase/Decrease
        self.chargeTimesView.userInteractionEnabled = YES;
        self.prepaidMonthView.userInteractionEnabled = YES;
        self.chargeTimesView.minimum = 1;
        [self.chargeTimesView.numberTextField setText:@"1"];
        
    } else {
        // if check box is NO, disable Increase/Decrease
        self.chargeTimesView.userInteractionEnabled = NO;
        self.prepaidMonthView.userInteractionEnabled = NO;
        [self.chargeTimesView.numberTextField setText:@"0"];
    }
    
    [self.prepaidMonthView.numberTextField setText:@"0"];
    
}

@end

/*------------------------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
            /* ------------------Cho phép đăng ký thêm Box IPTV------------------------ */

@interface IPTVServiceViewController ()<NIDropDownDelegate>

@end

@implementation IPTVServiceViewController {
    NIDropDown *dropDownView;
    UIButton *buttonSelected;
    KeyValueModel *deploymentFormSelected, *servicePackageSelected;
    PromotionModel  *promotionFirstBoxSelected, *promotionSecondBoxSelected;
    IPTVPrice *iptvPrice;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UITapGestureRecognizer *screenTGR = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:screenTGR];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkingData)
                                                 name:@"CheckingData"
                                               object:nil];

    self.boxCountView.delegate = self;
    
    self.promotionFirstBoxButton.titleLabel.numberOfLines = 0;
    self.promotionFirstBoxButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.promptionSecondBoxButton.titleLabel.numberOfLines = 0;
    self.promptionSecondBoxButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    [self getData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    _flagTotalIPTV = FlagTotalIPTV;
    [self setupData];
    // Checking data selected.
//    [self checkingData];
    if (!_flagTotalIPTV) {
        [self showMBProcess];
        [self getTotalAmountIPTV];
    }
}

- (void)callbackWithMessage:(NSString *)message {
    if (self.delegate) {
        [self.delegate callbackViewWithMessage:message];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)checkInternetofIPTVOnly {
    //[[NSNotificationCenter defaultCenter] postNotificationName:<#(nonnull NSNotificationName)#> object:<#(nullable id)#>];
    
}

#pragma mark - Setup data for update registration form
- (void)setupData {
    // Yêu cầu cài đặt
    if ([self.installRequestSwitch isOn] && [self.boxCountView.numberTextField.text integerValue] > 0) {
        self.updateRegistrationContractModel.iptvRequestSetup = @"1";
    } else {
        self.updateRegistrationContractModel.iptvRequestSetup = @"0";
        
    }
    // Yêu cầu khoan tường
    if ([self.wallDrillingRequest isOn] && [self.boxCountView.numberTextField.text integerValue] > 0) {
        self.updateRegistrationContractModel.iptvRequestDrillWall = @"1";
    } else {
        self.updateRegistrationContractModel.iptvRequestDrillWall = @"0";
        
    }
    // Hình thức triển khai
    self.updateRegistrationContractModel.iptvStatus = StringFormat(@"%@", deploymentFormSelected.Key ?:@"0");
    // Số lượng Box
    self.updateRegistrationContractModel.iptvBoxCount = self.boxCountView.numberTextField.text;
    // Số lượng PLC
    self.updateRegistrationContractModel.iptvPLCCount = self.plcCountView.numberTextField.text;
    // Số lượng STB thu hồi
    self.updateRegistrationContractModel.iptvReturnSTBCount = self.stbRecoveryView.numberTextField.text;
    // Gói dịch vụ
    self.updateRegistrationContractModel.iptvPackage = StringFormat(@"%@", servicePackageSelected.Key ?:@"0");
    // ID CLKM Box đầu tiên
    self.updateRegistrationContractModel.iptvPromotionID = StringFormat(@"%@", promotionFirstBoxSelected.PromotionId ?:@"0");
    self.updateRegistrationContractModel.iptvPromotionType = StringFormat(@"%@", promotionFirstBoxSelected.Type ?:@"-1");
    // ID CLKM Box thứ 2 trở đi
    self.updateRegistrationContractModel.iptvPromotionIDBoxOrder = StringFormat(@"%@", promotionSecondBoxSelected.PromotionId ?:@"0");
    self.updateRegistrationContractModel.iptvPromotionTypeBoxOrder = StringFormat(@"%@", promotionSecondBoxSelected.Type ?:@"-1");
    self.updateRegistrationContractModel.iptvPromotionDesc = StringFormat(@"%@", promotionFirstBoxSelected.PromotionName ?:@"");
    
    // Số lần tính cước
    self.updateRegistrationContractModel.iptvChargeTimes = self.chargeTimesView.numberTextField.text;
    // Số tiền trả trước = Tiền của CLKM Box đầu tiên
    self.updateRegistrationContractModel.iptvPrepaid = StringFormat(@"%@", promotionFirstBoxSelected.Amount ?:@"0");
    // VTV Cab
    self.updateRegistrationContractModel.iptvVTVChargeTimes = self.vtvCabView.chargeTimesView.numberTextField.text;
    self.updateRegistrationContractModel.iptvVTVPrepaidMonth = self.vtvCabView.prepaidMonthView.numberTextField.text;
    // Fim +
    self.updateRegistrationContractModel.iptvFimPlusChargeTimes = self.fimPlusView.chargeTimesView.numberTextField.text;
    self.updateRegistrationContractModel.iptvFimPlusPrepaidMonth = self.fimPlusView.prepaidMonthView.numberTextField.text;
    // Fim Hot
    self.updateRegistrationContractModel.iptvFimHotChargeTimes = self.fimHotView.chargeTimesView.numberTextField.text;
    self.updateRegistrationContractModel.iptvFimHotPrepaidMonth = self.fimHotView.prepaidMonthView.numberTextField.text;
    // K +
    self.updateRegistrationContractModel.iptvKPlusChargeTimes = self.kPlusView.chargeTimesView.numberTextField.text;
    self.updateRegistrationContractModel.iptvKPlusPrepaidMonth = self.kPlusView.prepaidMonthView.numberTextField.text;
    // Đặc sắc HD
    self.updateRegistrationContractModel.iptvVTCChargeTimes = self.hdFeaturedView.chargeTimesView.numberTextField.text;
    self.updateRegistrationContractModel.iptvVTCPrepaidMonth = self.hdFeaturedView.prepaidMonthView.numberTextField.text;
}

- (void)checkingData {
    if ([self.updateRegistrationContractModel.iptvBoxCount integerValue] > 0) {
        if ([self.updateRegistrationContractModel.iptvPackage isEqual:@"0"]) {
            [self callbackWithMessage:@"Vui lòng chọn gói dịch vụ IPTV!"];
            return;
        }
        if (self.updateRegistrationContractModel.serviceType == 0 && [self.updateRegistrationContractModel.iptvPromotionID isEqual:@"0"]) {
            [self callbackWithMessage:@"Vui lòng chọn khuyến mãi Box đầu tiên!"];
            return;
        }
    }
}

#pragma mark - Load data
/* Load data when load view */
- (void)getData {
    
    [self getDataExtraPackagePrice:@"-1" contract:self.updateRegistrationContractModel.contract ?:@"" RegCode:self.updateRegistrationContractModel.regCode ?:@"" iptvPromotionID:@""];
    
    switch (self.viewType) {
        case CreateRegisterForm:
            
            [self getDataForCreateRegistrationForm];
            
            break;
            
        case UpdateRegisterForm:
            
            [self getDataForUpdateRegistrationForm];
            
            break;
            
        default:
            break;
    }
}
/* Load data when create registration form */
- (void)getDataForCreateRegistrationForm {
    // Set view
    [self.boxCountView.numberTextField setText:@"0"];
    [self setEnableButton:NO];
    [self setEnableExtraPackageView:NO];
    // Load data
    [self getDataDeploymentFormWithID:@"0"];
    [self getDataServicePackageWithID:@"0"];
}

/* Load data when update registration form */
- (void)getDataForUpdateRegistrationForm {
    // Set view
    // Yêu cầu cài đặt
    if ([self.updateRegistrationContractModel.iptvRequestSetup integerValue] == 1) {
        [self.installRequestSwitch setOn:YES];
    }
    // Yêu cầu khoan tường
    if ([self.updateRegistrationContractModel.iptvRequestDrillWall integerValue] == 1) {
        [self.wallDrillingRequest setOn:YES];
    }
    // Số lượng PLC
    [self.plcCountView.numberTextField setText:self.updateRegistrationContractModel.iptvPLCCount];
    // Số lượng STB thu hồi
    [self.stbRecoveryView.numberTextField setText:self.updateRegistrationContractModel.iptvReturnSTBCount];
    // Hình thức triển khai
    [self getDataDeploymentFormWithID:@"0"];
    // Số lượng Box
    [self.boxCountView.numberTextField setText:self.updateRegistrationContractModel.iptvBoxCount];

    if ([self.updateRegistrationContractModel.iptvBoxCount integerValue] > 0) {
        [self setEnableButton:YES];
        [self setEnableExtraPackageView:YES];
        [self setMaximum:[self.updateRegistrationContractModel.iptvBoxCount integerValue]];
        // Load data
        // Gói dịch vụ
        [self getDataServicePackageWithID:self.updateRegistrationContractModel.iptvPackage];
        // Khuyến mãi Box đầu tiên
        [self showMBProcess];
        [self getDataPromotionFirstBoxWithID:self.updateRegistrationContractModel.iptvPromotionID];

        /// If số Box > 1 thì load khuyễn mãi Box thứ 2 trở đi
        /// else disable
        if (self.updateRegistrationContractModel.serviceType == 0) {
            if ([self.updateRegistrationContractModel.iptvBoxCount integerValue] > 1) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self getDataPromotionSecondBoxWithID:self.updateRegistrationContractModel.iptvPromotionIDBoxOrder];
                });
                
            } else {
                self.promptionSecondBoxButton.enabled = NO;
            }
            
        } else {
            // Khuyến mãi Box thứ 2 trở đi
            [self getDataPromotionSecondBoxWithID:self.updateRegistrationContractModel.iptvPromotionIDBoxOrder];
        }
        
        // Số lần tính cước
        [self.chargeTimesView.numberTextField setText:self.updateRegistrationContractModel.iptvChargeTimes];
        // Fim +
        [self getDataExtraPackageWithChargeTimes:self.updateRegistrationContractModel.iptvFimPlusChargeTimes withPrepaidMonth:self.updateRegistrationContractModel.iptvFimPlusPrepaidMonth andExtraPackageView:self.fimPlusView];
        // Fim Hot
        [self getDataExtraPackageWithChargeTimes:self.updateRegistrationContractModel.iptvFimHotChargeTimes withPrepaidMonth:self.updateRegistrationContractModel.iptvFimHotPrepaidMonth andExtraPackageView:self.fimHotView];
        // K +
        [self getDataExtraPackageWithChargeTimes:self.updateRegistrationContractModel.iptvKPlusChargeTimes withPrepaidMonth:self.updateRegistrationContractModel.iptvKPlusPrepaidMonth andExtraPackageView:self.kPlusView];
        // Đặc sắc HD
        [self getDataExtraPackageWithChargeTimes:self.updateRegistrationContractModel.iptvVTCChargeTimes withPrepaidMonth:self.updateRegistrationContractModel.iptvVTCPrepaidMonth andExtraPackageView:self.hdFeaturedView];
        // VTV Cab
        [self getDataExtraPackageWithChargeTimes:self.updateRegistrationContractModel.iptvVTVChargeTimes withPrepaidMonth:self.updateRegistrationContractModel.iptvVTVPrepaidMonth andExtraPackageView:self.vtvCabView];
        // Tổng tiền
        [self setTextForAmountLabel:self.moneyTotalLabel andText:self.updateRegistrationContractModel.iptvTotal];
        
    } else {
        [self setEnableButton:NO];
        [self setEnableExtraPackageView:NO];
        [self getDataServicePackageWithID:@"0"];
    }
}

- (void)getDataExtraPackageWithChargeTimes:(NSString *)chargTimes withPrepaidMonth:(NSString *)prepaidMonth andExtraPackageView:(ExtraPackageItemView *)extraPackageView {
    [extraPackageView.chargeTimesView.numberTextField setText:chargTimes];
    [extraPackageView.prepaidMonthView.numberTextField setText:prepaidMonth];
    if ([chargTimes integerValue] > 0) {
        [extraPackageView.checkBoxButton setSelected:YES];
    }
}

/* Load data deployment form */
- (void)getDataDeploymentFormWithID:(NSString *)deploymentFormID {
    if (self.deploymentFormArray.count <= 0) {
        KeyValueModel *item = [[KeyValueModel alloc] initWithName:@"0" description:@"Thêm Box"];
        self.deploymentFormArray = [NSMutableArray arrayWithObjects:item, nil];
    }
    
    for (deploymentFormSelected in self.deploymentFormArray) {
        if ([deploymentFormSelected.Key isEqual:deploymentFormID]) {
            [self.deploymentFormButton setTitle:deploymentFormSelected.Values forState:UIControlStateNormal];
            return;
        }
    }
    
    [self setButtonTitleDefaultWithButton:self.deploymentFormButton];
}

/* Load data service package */
- (void)getDataServicePackageWithID:(NSString *)servicePackageID {
    if (self.servicePackageArray.count <= 0) {
        self.servicePackageArray = [Common getServicePackage];
    }
    
    for (servicePackageSelected in self.servicePackageArray) {
        if ([servicePackageSelected.Key isEqual: servicePackageID]) {
            [self.servicePackageButton setTitle:servicePackageSelected.Values forState:UIControlStateNormal];
            return;
        }
    }
    
    [self setButtonTitleDefaultWithButton:self.servicePackageButton];
    
}

/* Load data promotion first box */
- (void)getDataPromotionFirstBoxWithID:(NSString *)promtionID {
    if (self.promotionFirstBoxArray.count > 0) {
        promotionFirstBoxSelected = nil;
        [self setButtonTitleDefaultWithButton:self.promotionFirstBoxButton];
        [self setTextForAmountLabel:self.promotionFirstBoxPriceLabel andText:@"0"];
        [self hideMBProcess];
        return;
    }
    // Kiểm tra kết nối internet
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    [shared.appProxy GetPromotionIPTVList:servicePackageSelected.Key withBoxOrder:@"1" CustomerType:(NSString *)@"0" Contract:self.updateRegistrationContractModel.contract ?:@"" RegCode:self.updateRegistrationContractModel.contract ?:@"" Handler:^(id result, NSString *errorCode, NSString *message) {
        
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        
        self.promotionFirstBoxArray = result;
        if (self.promotionFirstBoxArray.count <= 0) {
            [Common_client showAlertMessage:@"Không có dữ liệu câu lệnh khuyến mãi"];
            [self hideMBProcess];
            return ;
        }
        
        for (promotionFirstBoxSelected in self.promotionFirstBoxArray) {
            if ([promotionFirstBoxSelected.PromotionId isEqual:promtionID]) {
                [self.promotionFirstBoxButton setTitle:promotionFirstBoxSelected.PromotionName forState:UIControlStateNormal];
                [self setTextForAmountLabel:self.promotionFirstBoxPriceLabel andText:promotionFirstBoxSelected.Amount];
                [self hideMBProcess];
                return;
            }
        }
        
        [self setButtonTitleDefaultWithButton:self.promotionFirstBoxButton];
        [self setTextForAmountLabel:self.promotionFirstBoxPriceLabel andText:@"0"];
        [self hideMBProcess];
        return;
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetPromotionIPTVList",[error localizedDescription]]];
        [self hideMBProcess];
    }];
    
}

/* Load data promotion second box */
- (void)getDataPromotionSecondBoxWithID:(NSString *)promtionID {
    if (self.promptionSecondBoxArray.count > 0) {
        promotionSecondBoxSelected = nil;
        [self setButtonTitleDefaultWithButton:self.promptionSecondBoxButton];
        [self setTextForAmountLabel:self.promotionSecondBoxPriceLabel andText:@"0"];
        [self.promptionSecondBoxArray removeAllObjects];
    }
    // Kiểm tra kết nối internet
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    [shared.appProxy GetPromotionIPTVList:servicePackageSelected.Key withBoxOrder:@"2" CustomerType:(NSString *)@"0" Contract:@"" RegCode:@"" Handler:^(id result, NSString *errorCode, NSString *message) {
        
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        
        self.promptionSecondBoxArray = result;
        if (self.promptionSecondBoxArray.count <= 0) {
            [Common_client showAlertMessage:@"Không có dữ liệu câu lệnh khuyến mãi"];
            [self hideMBProcess];
            return ;
        }
        
        for (promotionSecondBoxSelected in self.promptionSecondBoxArray) {
            if ([promotionSecondBoxSelected.PromotionId isEqual:promtionID]) {
                [self.promptionSecondBoxButton setTitle:promotionSecondBoxSelected.PromotionName forState:UIControlStateNormal];
                [self setTextForAmountLabel:self.promotionSecondBoxPriceLabel andText:promotionSecondBoxSelected.Amount];
                [self hideMBProcess];
                return;
            }
        }
        
        [self setButtonTitleDefaultWithButton:self.promptionSecondBoxButton];
        [self setTextForAmountLabel:self.promotionSecondBoxPriceLabel andText:@"0"];
        [self hideMBProcess];
        return;
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetPromotionIPTVList",[error localizedDescription]]];
        [self hideMBProcess];
    }];
}

-(void)getDataExtraPackagePrice:(NSString *)iptvPromotionType contract:(NSString *)contract RegCode:(NSString *)regCode iptvPromotionID:(NSString *)promotionID {
    // Kiểm tra kết nối internet
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    ShareData *share = [ShareData instance];
//    NSDictionary *dict = [NSDictionary dictionaryWithObject:share.currentUser.userName forKey:@"UserName"];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setValue:share.currentUser.userName forKeyPath:@"UserName"];
    [dict setValue:iptvPromotionType forKeyPath:@"IPTVPromotionType"];
    [dict setValue:promotionID forKeyPath:@"IPTVPromotionID"];
    [dict setValue:contract forKeyPath:@"Contract"];
    [dict setValue:regCode forKeyPath:@"RegCode"];
    
    [share.registrationFormProxy getIPTVPrice:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        if(![errorCode isEqualToString:@"0"]){
//            [self getDataExtraPackagePrice];
            [self getDataExtraPackagePrice:iptvPromotionType contract:contract RegCode:regCode iptvPromotionID:promotionID];
            return ;
        }
        
        iptvPrice = result;
        NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
        numberFormatter.numberStyle = NSNumberFormatterDecimalStyle;
        
        self.fimPlusView.moneyPackageLable.text    = StringFormat(@"(%@ VNĐ)",[numberFormatter stringFromNumber:[NSNumber numberWithFloat:[iptvPrice.FimPlusPrice intValue]]]);
        
        self.fimHotView.moneyPackageLable.text     = StringFormat(@"(%@ VNĐ)",[numberFormatter stringFromNumber:[NSNumber numberWithFloat:[iptvPrice.FimHotPrice intValue]]]);
        self.kPlusView.moneyPackageLable.text      = StringFormat(@"(%@ VNĐ)",[numberFormatter stringFromNumber:[NSNumber numberWithFloat:[iptvPrice.KPlusPrice intValue]]]);
        self.hdFeaturedView.moneyPackageLable.text = StringFormat(@"(%@ VNĐ)",[numberFormatter stringFromNumber:[NSNumber numberWithFloat:[iptvPrice.DacSacHDPrice intValue]]]);
        self.vtvCabView.moneyPackageLable.text     = StringFormat(@"(%@ VNĐ)",[numberFormatter stringFromNumber:[NSNumber numberWithFloat:[iptvPrice.VTVCabPrice intValue]]]);
        
        [self hideMBProcess];

    } errorHandler:^(NSError *error) {
        NSLog(@"%@",[error description]);
    }];
}

/* Set title default for button */
- (void)setButtonTitleDefaultWithButton:(UIButton *)sender {
    NSString *title = @"";
    if (sender == self.deploymentFormButton) {
        title = @"[Chọn hình thức triển khai]";
        [self.deploymentFormButton setTitle:title forState:UIControlStateNormal];
        return;
    }
    
    if (sender == self.servicePackageButton) {
        title = @"[Chọn gói dịch vụ]";
        [self.servicePackageButton setTitle:title forState:UIControlStateNormal];
        return;
    }
    
    if (sender == self.promotionFirstBoxButton) {
        title = @"[Chọn câu lệnh khuyến mãi]";
        [self.promotionFirstBoxButton setTitle:title forState:UIControlStateNormal];
        return;
    }
    
    if (sender == self.promptionSecondBoxButton) {
        title = @"[Chọn câu lệnh khuyến mãi]";
        [self.promptionSecondBoxButton setTitle:title forState:UIControlStateNormal];
        return;
    }
}

#pragma mark - UISwitch action
- (IBAction)switchPressed:(id)sender {
    
}

#pragma mark - UIButton action
- (IBAction)buttonSelectPressed:(id)sender {
    buttonSelected = sender;
    // Chọn gói dịch vụ
    if (sender == self.servicePackageButton) {
        [self showDropDownAtButton:sender withArrayData:self.servicePackageArray];
        return;
    }
    
    // Chọn câu lệnh khuyến mãi Box đầu tiên
    if (sender == self.promotionFirstBoxButton) {
        [self showDropDownAtButton:sender withArrayData:self.promotionFirstBoxArray];
//        [self getDataExtraPackagePrice:@"-1" contract:self.updateRegistrationContractModel.contract ?:@"" RegCode:self.updateRegistrationContractModel.regCode ?:@""];
        return;
    }
    
    // Chọn câu lệnh khuyến mãi Box thứ hai trở đi
    if (sender == self.promptionSecondBoxButton) {
        [self showDropDownAtButton:sender withArrayData:self.promptionSecondBoxArray];
        return;
    }
}

- (IBAction)buttonReloadTotalAmountPressed:(id)sender {
    [self showMBProcess];
    [self getTotalAmountIPTV];
}

#pragma mark - IncreaseDecreaseViewDelegate methods
- (void)returnDataFrom:(UIView *)increaseDecreaseView {
    if (increaseDecreaseView == self.boxCountView) {
        if (self.updateRegistrationContractModel.serviceType == 0) {
            if (self.boxCountView.count > 0) {
                [self setEnableButton:YES];
                [self setEnableExtraPackageView:YES];
                
            } else {
                [self setEnableButton:NO];
                [self setEnableExtraPackageView:NO];
                
                return;
            }
            
            if (self.boxCountView.count > 1) {
                self.promptionSecondBoxButton.enabled = YES;
                [self showMBProcess];
                [self getDataPromotionSecondBoxWithID:@"0"];
                
            } else {
                self.promptionSecondBoxButton.enabled = NO;
                [self setButtonTitleDefaultWithButton:self.promptionSecondBoxButton];
                [self setTextForAmountLabel:self.promotionSecondBoxPriceLabel andText:@"0"];
                promotionSecondBoxSelected = nil;
            }

        } else {
            if (self.boxCountView.count > 0) {
                [self setEnableButton:YES];
                [self setEnableExtraPackageView:YES];
                [self showMBProcess];
                [self getDataPromotionSecondBoxWithID:@"0"];
                //chon voi so luong box >= 1
                [deploymentFormSelected setValue:@"4" forKey:@"Key"];

            } else {
                [self setEnableButton:NO];
                [self setEnableExtraPackageView:NO];
            }
        }
        
        // Setup Số lần tính cước khuyến mãi trả trước
        if (self.boxCountView.count > 0) {
            self.chargeTimesView.minimum = 1;
            self.chargeTimesView.numberTextField.text = @"1";
            
        } else {
            self.chargeTimesView.minimum = 0;
            self.chargeTimesView.numberTextField.text = @"0";
        }
        
        [self setMaximum:self.boxCountView.count];
    }
}

#pragma mark - Enable View
- (void)setEnableExtraPackageView:(BOOL)status {
    self.fimPlusView.checkBoxButton.enabled = status;
    self.fimHotView.checkBoxButton.enabled = status;
    self.kPlusView.checkBoxButton.enabled = status;
    self.hdFeaturedView.checkBoxButton.enabled = status;
    self.vtvCabView.checkBoxButton.enabled = status;
    
    if (!status) {
        [self.fimPlusView reloadData];
        [self.fimHotView reloadData];
        [self.kPlusView reloadData];
        [self.hdFeaturedView reloadData];
        [self.vtvCabView reloadData];
    }
}

- (void)setEnableButton:(BOOL)status {
    self.servicePackageButton.enabled = status;
    self.promotionFirstBoxButton.enabled = status;
    self.promptionSecondBoxButton.enabled = status;
    
    if (self.updateRegistrationContractModel.serviceType == 1 || self.updateRegistrationContractModel.serviceType == 2) {
        self.promotionFirstBoxButton.enabled = NO;
        promotionFirstBoxSelected = nil;
    }
    
    if (!status) {
        [self setButtonTitleDefaultWithButton:self.servicePackageButton];
        [self setButtonTitleDefaultWithButton:self.promotionFirstBoxButton];
        [self setButtonTitleDefaultWithButton:self.promptionSecondBoxButton];
        
        [self setTextForAmountLabel:self.promotionFirstBoxPriceLabel andText:@"0"];
        [self setTextForAmountLabel:self.promotionSecondBoxPriceLabel andText:@"0"];

        promotionFirstBoxSelected = promotionSecondBoxSelected = nil;
        
        servicePackageSelected = nil;
    }
}

#pragma mark - Set maximum count in extra package view

- (void)setMaximum:(NSInteger)maximum {
    self.chargeTimesView.maximum                = maximum;
    self.fimPlusView.chargeTimesView.maximum    = maximum;
    self.fimHotView.chargeTimesView.maximum     = maximum;
    self.kPlusView.chargeTimesView.maximum      = maximum;
    self.hdFeaturedView.chargeTimesView.maximum = maximum;
    self.vtvCabView.chargeTimesView.maximum     = maximum;
    
    [self.chargeTimesView                reloadData];
    [self.fimPlusView.chargeTimesView    reloadData];
    [self.fimHotView.chargeTimesView     reloadData];
    [self.kPlusView.chargeTimesView      reloadData];
    [self.hdFeaturedView.chargeTimesView reloadData];
    [self.vtvCabView.chargeTimesView     reloadData];
    
}


#pragma mark - NIDropDown method
- (NSMutableArray *)getDataForDropDownView:(NSArray *)inputArray andButton:(UIButton *)sender {
    NSMutableArray *outputArray = [[NSMutableArray alloc] init];
    
    if (sender == self.servicePackageButton) {
        for (KeyValueModel *item in inputArray) {
            [outputArray addObject:item.Values];
        }
        return outputArray;
        
    }
    
    if (sender == self.promotionFirstBoxButton || sender == self.promptionSecondBoxButton) {
        for (PromotionModel *item in inputArray) {
            [outputArray addObject:item.PromotionName];
        }
        return outputArray;
        
    }
    
    return nil;
    
}

- (void)showDropDownAtButton:(UIButton *)sender withArrayData:(NSArray*)arrData{
    [self.view endEditing:YES];
    
    if (dropDownView == nil) {
        NSArray *data = [self getDataForDropDownView:arrData andButton:sender];
        CGFloat height = 160.0;
        CGFloat width = 0.0;
        if (data.count < 4) {
            height = 40*data.count;
        }
        
        NSString *animation = @"up";
        if (sender == self.servicePackageButton || sender == self.promotionFirstBoxButton) {
            animation = @"down";
        }
        dropDownView = [[NIDropDown alloc] showDropDownAtButton:sender height:&height width:&width data:data images:nil animation:animation];
        dropDownView.delegate = self;
        
        return;
    }
    [dropDownView hideDropDown:sender];
    [self rel];
}

- (void)niDropDownDelegateMethod:(NIDropDown *)sender {
    [self rel];
}

- (void)rel {
    dropDownView = nil;
}

- (void)dropDownListView:(NIDropDown *)dropDownListView didSelectIndexPath:(NSIndexPath *)indexPath atButton:(UIButton *)button{
    [self didDropDownSelected:indexPath.row atButton:button];
}

- (void)didDropDownSelected:(NSInteger)row atButton:(UIButton *)button {
    
    FlagTotalIPTV = NO;
    
    // Chọn gói dịch vụ
    if (button == self.servicePackageButton) {
        servicePackageSelected = self.servicePackageArray[row];
        [self showMBProcess];
        [self getDataPromotionFirstBoxWithID:@"0"];
        [self getDataPromotionSecondBoxWithID:@"0"];

        return;
    }
    
    // Chọn câu lệnh khuyến mãi Box đầu tiên
    if (button == self.promotionFirstBoxButton) {
        promotionFirstBoxSelected = self.promotionFirstBoxArray[row];
        [self setTextForAmountLabel:self.promotionFirstBoxPriceLabel andText:promotionFirstBoxSelected.Amount];
        
        [self showMBProcess];
        [self getDataExtraPackagePrice:promotionFirstBoxSelected.Type contract:self.updateRegistrationContractModel.contract ?:@"" RegCode:self.updateRegistrationContractModel.regCode ?:@"" iptvPromotionID:promotionFirstBoxSelected.PromotionId];
        
        return;
    }
    
    // Chọn câu lệnh khuyến mãi Box thứ hai trở đi
    if (button == self.promptionSecondBoxButton) {
        promotionSecondBoxSelected = self.promptionSecondBoxArray[row];
        [self setTextForAmountLabel:self.promotionSecondBoxPriceLabel andText:promotionSecondBoxSelected.Amount];

        return;
    }
}

// Hide keyboard and drop down view if them show
- (void)handleSingleTap:(UITapGestureRecognizer *)sender {
    if (dropDownView !=nil) {
        [dropDownView hideDropDown:buttonSelected];
        dropDownView = nil;
    }
}

#pragma mark - Total amount
// Get total IPTV amount
- (void)getTotalAmountIPTV {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    
    NSLog(@"-----------Reloading IPTV Total.....");
    ShareData *shared = [ShareData instance];
    [shared.registrationFormProxy GetIPTVTotal:shared.currentUser.userName
                                      sPackage:servicePackageSelected.Key
                                  iPromotionID:promotionFirstBoxSelected.PromotionId
                                  iChargeTimes:self.chargeTimesView.numberTextField.text
                                      iPrepaid:@"0"
                                     iBoxCount:self.boxCountView.numberTextField.text
                                     iPLCCount:self.plcCountView.numberTextField.text
                                    iReturnSTB:self.stbRecoveryView.numberTextField.text
                           iVTVCabPrepaidMonth:self.vtvCabView.prepaidMonthView.numberTextField.text
                            iVTVCabChargeTimes:self.vtvCabView.chargeTimesView.numberTextField.text
                            iKPlusPrepaidMonth:self.kPlusView.prepaidMonthView.numberTextField.text
                             iKPlusChargeTimes:self.kPlusView.chargeTimesView.numberTextField.text
                              iVTCPrepaidMonth:self.hdFeaturedView.prepaidMonthView.numberTextField.text
                               iVTCChargeTimes:self.hdFeaturedView.chargeTimesView.numberTextField.text
                              iHBOPrepaidMonth:@"0"
                               iHBOChargeTimes:@"0"
                                  IServiceType:@"0"
                           iFimPlusChargeTimes:self.fimPlusView.chargeTimesView.numberTextField.text
                          iFimPlusPrepaidMonth:self.fimPlusView.prepaidMonthView.numberTextField.text
                            iFimHotChargeTimes:self.fimHotView.chargeTimesView.numberTextField.text
                           iFimHotPrepaidMonth:self.fimHotView.prepaidMonthView.numberTextField.text
                      iIPTVPromotionIDBoxOrder:promotionSecondBoxSelected.PromotionId
                             IPTVPromotionType:promotionFirstBoxSelected.Type ?:@"-1"
                     IPTVPromotionTypeBoxOrder:promotionSecondBoxSelected.Type ?:@"-1"
                                      Contract:self.updateRegistrationContractModel.contract ?:@""
                                       RegCode:self.updateRegistrationContractModel.regCode ?:@""
                                Completehander:^(id result, NSString *errorCode, NSString *message) {
                                    
                                    if ([message isEqualToString:@"het phien lam viec"]) {
                                        [self ShowAlertErrorSession];
                                        [self LogOut];
                                        [self hideMBProcess];
                                        return;
                                    }
                                    if(![errorCode isEqualToString:@"0"]){
                                        [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
                                        [self hideMBProcess];
                                        return ;
                                    }
                                    NSDictionary *dict = result;
                                    
                                    // Tiền thiết bị
                                    self.deviceTotal = [dict[@"DeviceTotal"] integerValue];
                                    self.updateRegistrationContractModel.iptvDeviceTotal = StringFormat(@"%li", self.deviceTotal ?:0);
                                    // Tiền trả trước
                                    self.prepaidTotal = [dict[@"PrepaidTotal"] integerValue];
                                    self.updateRegistrationContractModel.iptvPrepaidTotal = StringFormat(@"%li", self.prepaidTotal ?:0);
                                    // Tổng tiền IPTV
                                    NSString *total = StringFormat(@"%@", dict[@"Total"]);
                                    self.amountTotal = [total integerValue];
                                    [self setTextForAmountLabel:self.moneyTotalLabel andText:total];
                                    self.updateRegistrationContractModel.iptvTotal = total;
                                    
                                    [self postContentIPTVTotalNotification];
                                    
                                    // Cờ kiểm tra đã tính tổng tiền IPTV chưa (NO: chưa, YES: rồi)
                                    FlagTotalIPTV = YES;
                                    
                                    [self hideMBProcess];
                                    
                                } errorHandler:^(NSError *error) {
                                    [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@- %@",@"GetIPTVTotal",[error localizedDescription]]];
                                    [self hideMBProcess];
                                }];
}

- (void)postContentIPTVTotalNotification {
    [[NSNotificationCenter defaultCenter] postNotificationName:@"IPTVTotal" object:nil];
}

// Set label money total
- (void)setTextForAmountLabel:(UILabel *)label andText:(NSString *)text {
    NSNumberFormatter *nf = [NSNumberFormatter new];
    nf.numberStyle = NSNumberFormatterDecimalStyle;
    
    text = [nf stringFromNumber:[NSNumber numberWithFloat:[text intValue]]];
    
    if (label == self.moneyTotalLabel) {
        label.text = StringFormat(@"%@ VNĐ", text);
        
        return;
    }
    
    [label setText:StringFormat(@"(%@ VNĐ)", text)];
}

@end

