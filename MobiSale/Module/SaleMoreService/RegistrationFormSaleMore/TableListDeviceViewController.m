//
//  TableListDeviceViewController.m
//  MobiSale
//
//  Created by Tran Vu on 7/2/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import "TableListDeviceViewController.h"
#define LISTDEVICE_CELL @"ListDeviceEquipmentCell"
#import "ListDeviceAddViewController.h"
#import "RegistrationFormModel.h"
#import "ListDeviceViewController.h"
#import "RegistrationFormModel.h"
@interface TableListDeviceViewController ()<UITableViewDataSource,UITableViewDelegate,ListDeviceViewControllerDelegate>
@property (strong, nonatomic)NSMutableArray *arrayListDeviceAdd;
@property (strong, nonatomic)ListDeviceViewController *ls;
@end

@implementation TableListDeviceViewController
@synthesize delegate;
-(NSMutableArray *)arrayListDeviceAdd {
    
    if (!_arrayListDeviceAdd) {
        _arrayListDeviceAdd = [[NSMutableArray alloc] init];
    }
    return _arrayListDeviceAdd;
}

-(NSMutableArray *)arrayData {
    
    if (!_arrayData) {
        _arrayData = [[NSMutableArray alloc] init];
    }
    return _arrayData;
}
- (ListDeviceViewController *) ls {
    if (!_ls) {
        _ls = [[ListDeviceViewController alloc] init];
    }
    return _ls;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableViewInfo.dataSource = self;
    self.tableViewInfo.delegate = self;
    

    self.btnClickedListDevice.layer.cornerRadius = 5;
    
    self.btnClickedListDevice.layer.shadowOffset = CGSizeMake(0, 3);
    self.btnClickedListDevice.layer.shadowColor = [UIColor blackColor].CGColor;
    self.btnClickedListDevice.layer.shadowRadius = 1.0;
    self.btnClickedListDevice.layer.shadowOpacity = .5;

    
}
- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self.tableViewInfo reloadData];
   // self.ls.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
- (void)reloadDataTable {
    
    //self.arrayData =
    [self.tableViewInfo reloadData];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = 0;
    
    count = self.arrayData.count;
    
    return count;
}
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 160;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ListDeviceEquipmentCell *cell = (ListDeviceEquipmentCell *)[tableView dequeueReusableCellWithIdentifier:LISTDEVICE_CELL];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:LISTDEVICE_CELL owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    cell.lblNameDevices.text = [self.arrayData [indexPath.row]valueForKey:@"DeviceName"];
    
    cell.array = self.arrayData;
    cell.indexPath = indexPath;
    cell.deviceID = [self.arrayData [indexPath.row]valueForKey:@"DeviceID"];
    cell.arrayDevice  = self.arrayData[indexPath.row];
    //StringFormat(@"%d",[[self.arrayData [indexPath.row]valueForKey:@"Number"] intValue]);
    //[self.arrayData[indexPath.row]valueForKey:@"Number"];
    cell.countTextField.text = StringFormat(@"%d",[[self.arrayData [indexPath.row]valueForKey:@"Number"] intValue]);
    [cell.btnPromotionListDevice setTitle:StringFormat(@"%@",[self.arrayData[indexPath.row]valueForKey:@"PromotionDeviceText"]) forState:UIControlStateNormal];
    
    return cell;
    
}

- (void)sendListDeviceChecked:(NSMutableArray *)arraySelectedDevies {
    
    self.arrayData = arraySelectedDevies;

}

- (IBAction)btnListDevice_Clicked:(id)sender {
    
    [delegate showListDevice];
  
}



@end
