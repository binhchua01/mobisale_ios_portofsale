//
//  ListDeviceAddViewController.m
//  MobiSale
//
//  Created by Tran Vu on 7/2/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//

#import "ListDeviceAddViewController.h"
#import "ShareData.h"
#import "RegistrationFormModel.h"

@interface ListDeviceAddViewController ()<UITableViewDelegate,UITableViewDataSource>
@property(nonatomic,strong) NSMutableArray *arrayData;
@property(nonatomic,strong) NSMutableArray *arraySelectedDevies;

@end

@implementation ListDeviceAddViewController
@synthesize delegate;
- (NSMutableArray *)arrayData {
    if (!_arrayData) {
        _arrayData = [[NSMutableArray alloc] init];
    }
    return _arrayData;
}
- (NSMutableArray*)arraySelectedDevies {
    if (!_arraySelectedDevies) {
        _arraySelectedDevies = [[NSMutableArray alloc] init];
    }
    return _arraySelectedDevies;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
      [self loadListDevice:@"1"];
    
    self.tableViewInfo.delegate = self;
    self.tableViewInfo.dataSource = self;
    self.tableViewInfo.allowsSelection = YES;
    [self.tableViewInfo setAllowsMultipleSelectionDuringEditing:YES];
    [self.tableViewInfo setAllowsSelectionDuringEditing:YES];
    [self.tableViewInfo setEditing:YES animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
   
}
- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

}


- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
    
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return  self.arrayData.count;
    
}
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"cell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    
    if (cell == nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        
    }
    
    if (self.arrayData.count > 0) {
        
        cell.textLabel.text = [self.arrayData [indexPath.row]valueForKey:@"DeviceName"];
    }

    return cell;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 3;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.arraySelectedDevies addObject:self.arrayData[indexPath.row]];
[delegate sendListDeviceChecked:self.arraySelectedDevies];
    
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.arraySelectedDevies removeObject:self.arrayData[indexPath.row]];
     [delegate sendListDeviceChecked:self.arraySelectedDevies];
   
    
}

- (void)loadListDevice :(NSString *)type  {
    //kiem tra ket noi Internet
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
        
    }
    
    ShareData *shared = [ShareData instance];
    [shared.appProxy GetListDevice:type Handler:^(id result,NSString *errorCode,NSString *message){
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            return ;
        }
        
        if ([message isEqualToString:@"Error Service: Unauthorized"]) {
            [self ShowAlertErrorSession];
            [self hideMBProcess];
            
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            
            return ;
        }
        
        NSArray *arrData = result;
        if (arrData.count <= 0) {
            [self hideMBProcess];
            return;
            
        } else {
            
            self.arrayData = [NSMutableArray arrayWithArray:arrData];
            [self.tableViewInfo reloadData];
        }
        
        
    } errorHandler:^(NSError *error){
        
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetListDevice",[error localizedDescription]]];
        [self hideMBProcess];
    }];
    
}


@end
