//
//  CustomerInfoViewController.h
//  MobiSale
//
//  Created by ISC on 8/10/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "RegistrationFormSaleMorePageViewController.h"

@interface CustomerInfoViewController : BaseViewController
@property (strong, nonatomic) UpdateRegistrationContractModel *updateRegistrationContractModel;
// View Type: Create or Update Registration Form
@property (assign, nonatomic) ViewType viewType;
@property (strong, nonatomic) NSMutableArray *phoneTypeArray;

@property (weak, nonatomic) IBOutlet UILabel *fullNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *contractLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UIButton *phoneTypeButton;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *contactTextField;
@property (weak, nonatomic) IBOutlet UIView *contentView;

@end
