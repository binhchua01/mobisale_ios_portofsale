//
//  CustomerInfoViewController.m
//  MobiSale
//
//  Created by ISC on 8/10/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "CustomerInfoViewController.h"

@interface CustomerInfoViewController ()<UITextFieldDelegate, NIDropDownDelegate>

@end

@implementation CustomerInfoViewController {
    KeyValueModel *phoneTypeSelected;
    NIDropDown *dropDownView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    [self getData];
    [self setViewType];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    
    [self setupData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup data for update registration form
- (void)setupData {
    self.updateRegistrationContractModel.type1 = StringFormat(@"%@", phoneTypeSelected.Key ?:@"0");
    self.updateRegistrationContractModel.email = self.emailTextField.text;
    self.updateRegistrationContractModel.phone1 = self.phoneNumberTextField.text;
    self.updateRegistrationContractModel.contact1 = self.contactTextField.text;
    
}

#pragma mark - Load data when view begin show

/* Load data when load view */
- (void)getData {
    // Họ tên
    [self.fullNameLabel setText:self.updateRegistrationContractModel.fullName];
    // Số hợp đồng
    [self.contractLabel setText:self.updateRegistrationContractModel.contract];
    // Địa chỉ
    [self.addressLabel setText:self.updateRegistrationContractModel.address];
    // Email
    [self.emailTextField setText:self.updateRegistrationContractModel.email];
    
   //kiem tra lay so dien thoai 1
    if (![self.updateRegistrationContractModel.phone1 isEqualToString:@""] && self.updateRegistrationContractModel.phone1 != nil) {
        // Số điện thoại
        [self.phoneNumberTextField setText:self.updateRegistrationContractModel.phone1];
        // Người liên hệ
        [self.contactTextField setText:self.updateRegistrationContractModel.contact1];
        
    }
    //kiem tra lay so dien thoai 2
    if (![self.updateRegistrationContractModel.phone2 isEqualToString:@""] && self.updateRegistrationContractModel.phone2 !=nil) {
        // Số điện thoại
        [self.phoneNumberTextField setText:self.updateRegistrationContractModel.phone2];
        // Người liên hệ
        [self.contactTextField setText:self.updateRegistrationContractModel.contact2];
    }
    
    switch (self.viewType) {
        case CreateRegisterForm:
            
            [self getDataForCreateRegistrationForm];
            
            break;
            
        case UpdateRegisterForm:
            
            [self getDataForUpdateRegistrationForm];
            
            break;
            
        default:
            break;
    }
}
/* Load data when create registration form */
- (void)getDataForCreateRegistrationForm {
     [self getDataPhoneTypeWithID:@"4"];
}

/* Load data when update registration form */
- (void)getDataForUpdateRegistrationForm {
    // Loại điện thoại
    [self getDataPhoneTypeWithID:self.updateRegistrationContractModel.type1];
        
}

- (void)getDataPhoneTypeWithID:(NSString *)phoneTypeID {
    if (self.phoneTypeArray.count <= 0) {
        self.phoneTypeArray = [NSMutableArray arrayWithArray:[Common getPhoneType]];
    }
    for (phoneTypeSelected in self.phoneTypeArray) {
        if ([phoneTypeSelected.Key isEqual:phoneTypeID]) {
            [self.phoneTypeButton setTitle:phoneTypeSelected.Values forState:UIControlStateNormal];
            return;
        }
    }
    phoneTypeSelected = self.phoneTypeArray[3];
    [self.phoneTypeButton setTitle:phoneTypeSelected.Values forState:UIControlStateNormal];

}

- (void)setViewType {
    self.emailTextField.delegate = self;
    self.phoneNumberTextField.delegate = self;
    self.contactTextField.delegate = self;
    
    [self.fullNameLabel autoFixFrame];
    [self.addressLabel autoFixFrame];
    [self.contentView styleBorderMap];
}

#pragma mark - UITextFieldDelegate methods
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (dropDownView !=nil) {
        [dropDownView hideDropDown:self.phoneTypeButton];
        dropDownView = nil;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}


#pragma mark - Handle event touch on screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self handleSingleTap:nil];
}

// Hide keyboard and drop down view if them show
- (void)handleSingleTap:(UITapGestureRecognizer *)sender {
    [self.view endEditing:YES];
    if (dropDownView !=nil) {
        [dropDownView hideDropDown:self.phoneTypeButton];
        dropDownView = nil;
    }
}


#pragma mark - NIDropDown method
- (void)showDropDownAtButton:(UIButton *)sender withArrayData:(NSArray*)arrData{
    [self.view endEditing:YES];
    
    if (dropDownView == nil) {
        NSMutableArray *arrOutput = [[NSMutableArray alloc] init];
        for (KeyValueModel *item in arrData) {
            [arrOutput addObject:item.Values];
        }
        
        CGFloat height = 150.0;

        CGFloat width = 0.0;
        dropDownView = [[NIDropDown alloc] showDropDownAtButton:sender height:&height width:&width data:arrOutput images:nil animation:@"up"];
        dropDownView.delegate = self;
        
        return;
    }
    [dropDownView hideDropDown:sender];
    [self rel];
}

- (void)niDropDownDelegateMethod:(NIDropDown *)sender {
    [self rel];
}

- (void)rel {
    dropDownView = nil;
}

- (void)dropDownListView:(NIDropDown *)dropDownListView didSelectIndexPath:(NSIndexPath *)indexPath atButton:(UIButton *)button{
    [self didDropDownSelected:indexPath.row atButton:button];
}

- (void)didDropDownSelected:(NSInteger)row atButton:(UIButton *)button {
    if (button == self.phoneTypeButton) {
        phoneTypeSelected = self.phoneTypeArray[row];
        return;
    }
    
}

#pragma mark - UIButton action
- (IBAction)phoneTypeButtonPressed:(id)sender {
    [self showDropDownAtButton:self.phoneTypeButton withArrayData:self.phoneTypeArray];
}


@end
