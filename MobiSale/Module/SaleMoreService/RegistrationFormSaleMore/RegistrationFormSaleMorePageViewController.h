//
//  RegistrationFormSaleMorePageViewController.h
//  MobiSale
//
//  Created by ISC on 8/10/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SiUtils.h"
#import "Common.h"
#import "ShareData.h"
#import "NIDropDown.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "UILabel+FPTCustom.h"
#import "UIView+FPTCustom.h"
#import "UIButton+FPTCustom.h"
//#import "CustomButton.h"
#import "KeyValueModel.h"

#import "ViewPagerController.h"
#import "RegistrationFormDetailRecord.h"
#import "UpdateRegistrationContractModel.h"

#import "InfoContractRecord.h"
#import "PromotionModel.h"
#import "PromotionComboModel.h"
#import "IPTVPrice.h"


typedef enum: NSUInteger {
    CreateRegisterForm = 1,
    UpdateRegisterForm
    
} ViewType;

static NSString * iptvTotalKey = @"IPTVTotal";
static NSString * internetTotalKey = @"InternetTotal";

static BOOL FlagTotalIPTV = YES;

@interface RegistrationFormSaleMorePageViewController :ViewPagerController

@property (strong, nonatomic) UpdateRegistrationContractModel *updateRegistrationContractModel;
// View Type: Create or Update Registration Form
@property (assign, nonatomic) ViewType viewType;
// Tiêu đề của các tab
@property (strong, nonatomic) NSMutableArray *tabArray;

@property (strong, nonatomic) KeyValueModel *item;

@end
