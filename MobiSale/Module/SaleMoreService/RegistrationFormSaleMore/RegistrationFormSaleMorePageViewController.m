 //
//  RegistrationFormSaleMorePageViewController.m
//  MobiSale
//
//  Created by ISC on 8/10/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

/*
 -	Yêu cầu:
    o	Cho phép cập nhật một số thông tin của PĐK (email, số điện thoại) các thông tin khác hệ thống sẽ tự lấy thông tin từ HĐ điền vào PĐK (app không cần quan tâm).
    o	Tùy thuộc vào loại DV KH đang sử dụng mà cho phép đăng ký thêm những loại DV nào (ServiceType):
        ♣	Nếu KH đang sử dụng Internet Only (ServiceType = 1):
            •	Cho phép đăng ký thêm Box IPTV;
            •	Cho phép đăng ký Combo, câu lệnh khuyến mãi Combo sẽ lấy theo ID CLKM Internet của HĐ.
        ♣	Nếu KH đang sử dụng IPTV Only (ServiceType = 1):
            •	Cho phép đăng ký thêm Box IPTV;
            •	Cho phép đăng ký thêm dịch vụ Internet;
            •	Cho phép đăng ký Combo, câu lệnh khuyến mãi Combo sẽ lấy theo ID CLKM Internet của PĐK.
        ♣	Nếu KH đang sử dụng Internet + IPTV (ServiceType = 2):
            •	Cho phép đăng ký thêm Box IPTV;
            •	Nếu chưa đăng ký Combo thì cho phép đăng ký thêm Combo, CLKM Combo sẽ lấy theo ID CLKM Internet của HĐ;
    o	Khi cập nhật PĐK phải kiểm tra PĐK phải đăng ký ít nhất 1 loại DV (internet hoặc IPTV):
        ♣	Kiểm tra có Internet dựa vào PromotionID;
        ♣	Kiểm tra có đăng ký IPTV dựa vào số lượng Box & đã cập nhật tổng tiền;
 -	Thực hiện:
    o	API 83 – GetPromotionCombo: Lấy CLMK Combo
    o	API 53 – GetListLocalTypePost: lấy LocalType dựa theo số HĐ
    o	API 84 – UpdateRegistrationContract: Cập nhật PĐK bán thêm dịch vụ.
    o	Và các API khác đang sử dụng khi tạo PĐK hiện tại.

 */

#import "RegistrationFormSaleMorePageViewController.h"
#import "CustomerInfoViewController.h"
#import "InternetServiceViewController.h"
#import "IPTVServiceViewController.h"
#import "ListDeviceAddViewController.h"
#import "TableListDeviceViewController.h"
#import "RegisterTotalViewController.h"
#import "RegistrationFormModel.h"
#import "BaseViewController.h"
#import "ListDeviceViewController.h"
#import "ShareData.h"
#import <Crashlytics/Crashlytics.h>

#define TitleCreate @"TẠO PĐK BÁN THÊM"
#define TitleUpdate @"CẬP NHẬT PĐK BÁN THÊM"

static NSString *ThongTinKhachHang = @"THÔNG TIN KHÁCH HÀNG";
static NSString *DichVuInternet = @"DỊCH VỤ INTERNET";
static NSString *DichVuIPTV = @"DỊCH VỤ IPTV";
//DichVuThietBi
static NSString *DichVuThietBi = @"DỊCH VỤ BÁN THIẾT BỊ";
static NSString *TongTien = @"TỔNG TIỀN";

@interface RegistrationFormSaleMorePageViewController ()<ViewPagerDelegate, ViewPagerDataSource, RegisterTotalViewDelegate, IPTVServiceViewDelegate, InternetServiceViewDelegate,ListDeviceAddViewControllerDelegate, UIActionSheetDelegate,TableListDeviceViewControllerDelegate,ListDeviceViewControllerDelegate>{
    
    MBProgressHUD *HUD;
}
@property (strong, nonatomic) CustomerInfoViewController *customerInfoView;
@property (strong, nonatomic) InternetServiceViewController *internetServiceView;
@property (strong, nonatomic) IPTVServiceViewController *iptvServiceView;
@property (strong, nonatomic) ListDeviceAddViewController *listDeviceAddView;
@property (strong, nonatomic) TableListDeviceViewController *tableListDeviceView;
@property (strong, nonatomic) RegisterTotalViewController *registerTotalView;
@property (strong, nonatomic) ListDeviceViewController *ls;

//vutt11

@property(nonatomic,strong) NSMutableArray *arraySelectedDevies;
//array listDevice getAPI
@property (strong, nonatomic) NSMutableArray *arrayListDevice;
@property (strong, nonatomic) NSMutableArray *arrayData;

@end

@implementation RegistrationFormSaleMorePageViewController{
    
}
- (NSMutableArray*)arrayData {
    if (!_arrayData) {
        _arrayData = [[NSMutableArray alloc] init];
    }
    return _arrayData;
}

- (NSMutableArray*)arraySelectedDevies {
    if (!_arraySelectedDevies) {
        _arraySelectedDevies = [[NSMutableArray alloc] init];
    }
    return _arraySelectedDevies;
}
- (NSMutableArray*)arrayListDevice {
    if (!_arrayListDevice) {
        _arrayListDevice = [[NSMutableArray alloc] init];
    }
    return _arrayListDevice;
}
- (TableListDeviceViewController *)tableListDeviceView {
    if (!_tableListDeviceView) {
        _tableListDeviceView = [[TableListDeviceViewController alloc]init];
    }
    return _tableListDeviceView;
}
- (ListDeviceViewController *) ls {
    if (!_ls) {
        _ls = [[ListDeviceViewController alloc] init];
    }
    return _ls;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableListDeviceView.delegate = self;
    self.ls.delegate = self;
    // Do any additional setup after loading the view.
    self.delegate = self;
    self.dataSource = self;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:@"CheckingData"
                                               object:nil];
    
    
    // add right left button
    self.navigationItem.leftBarButtonItem = [self backBarButtonItem];


    
    if (self.updateRegistrationContractModel == nil) {
        self.updateRegistrationContractModel = [[UpdateRegistrationContractModel alloc] init];
    }
    
    //    _updateRegistrationContractModel.arraylistDevice
    [[RegistrationFormModel sharedInstance] setListDevicePackagesArray:self.updateRegistrationContractModel.arraylistDevice];

    // Set title view
    switch (self.viewType) {
        case CreateRegisterForm:
            self.title = TitleCreate;
            break;
        case UpdateRegisterForm:
            self.title = TitleUpdate;
            break;
        default:
            break;
    }
    /*
     *  Set data for tab
     */
    KeyValueModel *item1 = [[KeyValueModel alloc] initWithName:ThongTinKhachHang description:@"ic_customer_info"];
    KeyValueModel *item2 = [[KeyValueModel alloc] initWithName:DichVuInternet description:@"ic_internet"];
    KeyValueModel *item3 = [[KeyValueModel alloc] initWithName:DichVuIPTV description:@"ic_iptv"];
    KeyValueModel *item4 = [[KeyValueModel alloc] initWithName:DichVuThietBi description:@"ic_tableListDevice"];
    KeyValueModel *item5 = [[KeyValueModel alloc] initWithName:TongTien description:@"ic_register_total"];
   
    self.tabArray = [NSMutableArray arrayWithObjects:item1, item2, item3, item4,item5, nil];
    
    [self selectTabAtIndex:0];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - ViewPagerDataSource methods
- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager {
    return self.tabArray.count;
}

- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index {
    KeyValueModel *item = self.tabArray[index];
    
    UILabel *titleLabel = [UILabel new];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont systemFontOfSize:14.0];
    titleLabel.text = item.Key;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.numberOfLines = 0;
    [titleLabel sizeToFit];
    

    return titleLabel;
}

- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index {
 

    KeyValueModel *item;
    
    item = self.tabArray[index];
    return [self setContentViewForTabWithKey:item.Key];
    
}

- (CGFloat)viewPager:(ViewPagerController *)viewPager valueForOption:(ViewPagerOption)option withDefault:(CGFloat)value {
    switch (option) {
        case ViewPagerOptionStartFromSecondTab:
            return 0.0;
        case ViewPagerOptionCenterCurrentTab:
            return 1.0;
        case ViewPagerOptionTabLocation:
            return 1.0;
        case ViewPagerOptionTabHeight:
            return 39.0;
        case ViewPagerOptionTabOffset:
            return 36.0;
        case ViewPagerOptionTabWidth:
            return 200.0;
        case ViewPagerOptionFixFormerTabsPositions:
            return 1.0;
        case ViewPagerOptionFixLatterTabsPositions:
            return 1.0;
        default:
            return value;
    }
}

- (UIColor *)viewPager:(ViewPagerController *)viewPager colorForComponent:(ViewPagerComponent)component withDefault:(UIColor *)color {
    switch (component) {
        case ViewPagerIndicator:
            return [UIColor orangeColor];
        case ViewPagerTabsView:
            return [[UIColor colorWithRed:0.133 green:0.698 blue:0.569 alpha:1] colorWithAlphaComponent:1.0];
        case ViewPagerContent:
            return [UIColor whiteColor];
        default:
            return color;
    }
}
#pragma mark - ViewPagerDelegate methods
- (void)viewPager:(ViewPagerController *)viewPager didChangeTabToIndex:(NSUInteger)index {
    
    
    @try {
        if (index == 4) {
            
            if ([[RegistrationFormModel sharedInstance] listDevicePackagesArray] == [NSNull null]) {
                [[RegistrationFormModel sharedInstance] setTotalDevice:0];
                [self.registerTotalView getData];
                return;
            }
            
            self.arrayListDevice = [[RegistrationFormModel sharedInstance] listDevicePackagesArray];
            
            
            if (self.arrayListDevice.count > 0) {
                
                self.updateRegistrationContractModel.arraylistDevice = self.arrayListDevice;
                
                [self getTotalDevice:self.arrayListDevice];
            } else {
                
                [[RegistrationFormModel sharedInstance] setTotalDevice:0];
                [self.registerTotalView getData];
            }
            
        }
        
    }
    @catch (NSException *exception) {
        
        [CrashlyticsKit recordCustomExceptionName:@"RegistrationFormSaleMorePageViewController - funtion didChangeTabToIndex - getData- tính tổng tiền" reason:[[ShareData instance].currentUser.userName lowercaseString]frameArray:@[]];
        
    }
    
}

- (void)viewPager:(ViewPagerController *)viewPager didChangeTabToIndex:(NSUInteger)index fromIndex:(NSUInteger)previousIndex {
    
}

#pragma mark - Set content view for tab
- (UIViewController *)setContentViewForTabWithKey:(NSString *)key {
    
    
    if (key == ThongTinKhachHang) {
        if (self.customerInfoView == nil) {
            self.customerInfoView = [[CustomerInfoViewController alloc] init];
            self.customerInfoView.viewType = self.viewType;
            self.customerInfoView.updateRegistrationContractModel = self.updateRegistrationContractModel;
        }

        return self.customerInfoView;
    }
    
    if (key == DichVuInternet) {
        if (self.internetServiceView == nil) {
            self.internetServiceView = [[InternetServiceViewController alloc] init];
            self.internetServiceView.viewType = self.viewType;
            self.internetServiceView.delegate = self;
            self.internetServiceView.updateRegistrationContractModel = self.updateRegistrationContractModel;
        }
        
        return self.internetServiceView;
    }
    
    if (key == DichVuIPTV) {
        if (self.iptvServiceView == nil) {
            self.iptvServiceView = [[IPTVServiceViewController alloc] init];
            self.iptvServiceView.viewType = self.viewType;
            self.iptvServiceView.delegate = self;
            self.iptvServiceView.updateRegistrationContractModel = self.updateRegistrationContractModel;
        }

        return self.iptvServiceView;
    }

    if (key == DichVuThietBi) {
        
        if (self.tableListDeviceView == nil)
        {
           // self.tableListDeviceView = [[TableListDeviceViewController alloc] init];
            self.tableListDeviceView.arrayData = [[RegistrationFormModel sharedInstance] listDevicePackagesArray];
        }
        //kiem tra hien thi list thiet bi
        
        if (![_updateRegistrationContractModel.arraylistDevice isEqual: [NSNull null]]){
            
            self.tableListDeviceView.arrayData = _updateRegistrationContractModel.arraylistDevice;
            [self.tableListDeviceView.tableViewInfo reloadData];
        }
        
        return self.tableListDeviceView;
    }
    
    if (key == TongTien) {
        if (self.registerTotalView == nil) {
            self.registerTotalView = [[RegisterTotalViewController alloc] init];
            self.registerTotalView.viewType = self.viewType;
            self.registerTotalView.updateRegistrationContractModel = self.updateRegistrationContractModel;
            self.registerTotalView.delegate = self;
            
        }
        
    
        return self.registerTotalView;
    }

    return nil;
}

#pragma mark - RegisterTotalViewDelegate method
- (void)updateRegistrationContractSuccess {
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark - IPTVServiceViewDelegate & InternetServiceViewDelegate methods
- (void)callbackViewWithMessage:(NSString *)message {
    [Common_client showAlertMessage:message];
}

#pragma mark - Set left button bar

- (UIBarButtonItem *)backBarButtonItem
{
    UIImage *image = [SiUtils imageWithImageHeight:[[UIImage imageNamed:@"ic_pre"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] height:30];
    return [[UIBarButtonItem alloc]
            initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered
            target:self
            action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Bạn chắc chắn muốn thoát?" delegate:self cancelButtonTitle:@"Không" destructiveButtonTitle:@"Có" otherButtonTitles:nil, nil];
    actionSheet.tag = back;
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
}

#pragma mark - action sheet delegate method
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        switch (actionSheet.tag) {
            case back:
                [self.navigationController popViewControllerAnimated:YES];
                break;
            case save:
                break;
            default:
                break;
        }
    }
}

- (void)sendListDeviceChecked:(NSMutableArray *)arraySelectedDevies {
    
    //self.arraySelectedDevies = arraySelectedDevies;
    [self createdModelDeviceCreated:arraySelectedDevies];
    
}

- (void)createdModelDeviceCreated:(NSMutableArray*)arrayData{
    [self.arraySelectedDevies removeAllObjects];
    [[RegistrationFormModel sharedInstance]setListDevicePackagesArray:self.arraySelectedDevies];
    
    for (int i = 0; i< arrayData.count; i++) {
        NSString *deviceName,*deviceID;
        deviceID = StringFormat(@"%@",[arrayData[i]valueForKey:@"DeviceID"]);
        deviceName = StringFormat(@"%@",[arrayData[i]valueForKey:@"DeviceName"]);
        
        [[RegistrationFormModel sharedInstance] setParamater:deviceID DeviceName:deviceName PromotionDeviceID:@"0" PromotionDeviceText:@"Vui lòng chọn CLKM" Number:@"1"];
    }
    
}

-(void) receiveTestNotification:(NSNotification*)notification
{
    NSDictionary* userInfo = notification.userInfo;
    [self.arrayListDevice addObject:userInfo];
    [[RegistrationFormModel sharedInstance] setArrayListDeviceAdd:self.arrayListDevice];
    
}

//Total ListDevice
- (void)getTotalDevice :(NSMutableArray *)arrayListDevice {
    
    //check network
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcesslistRegis];
        return;
    }
    [self showMBProcessListRegis];
    ShareData *shared = [ShareData instance];
    [shared.appProxy GetTotalDevice:arrayListDevice CustomerStatus:@"1" Handler:^(id result,NSString *errorCode,NSString *message){
        
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self LogOut];
            [self hideMBProcesslistRegis];
            return ;
        }
        if ([message isEqualToString:@"Error Service: Unauthorized"]) {
           // [Common_client showAlertMessage:StringFormat(@"%@",message)];
            NSLog(@"%@",message);
            [self LogOut];
            [self hideMBProcesslistRegis];
            
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcesslistRegis];
            return ;
        }
        NSArray *arr = result;
        
        if (arr.count <= 0) {
            [self hideMBProcesslistRegis];
            return;
            
        } else {
            NSString *amountDevice;
            NSCharacterSet *character = [NSCharacterSet whitespaceCharacterSet];

            amountDevice = StringFormat(@"%@",[arr valueForKey:@"Amount"]);
             amountDevice = [amountDevice stringByReplacingOccurrencesOfString:@"(" withString:@""];
             amountDevice = [amountDevice stringByReplacingOccurrencesOfString:@")" withString:@""];
            amountDevice = [amountDevice stringByTrimmingCharactersInSet:character];
            amountDevice = [amountDevice stringByReplacingOccurrencesOfString:@"\n" withString:@""];
             amountDevice = [amountDevice stringByReplacingOccurrencesOfString:@" " withString:@""];
    
            [[RegistrationFormModel sharedInstance] setTotalDevice:amountDevice];
            
            [self.registerTotalView getData];
            
            //[[NSNotificationCenter defaultCenter]postNotificationName:@"DeviceTotal" object:self userInfo:nil];
            
            [self hideMBProcesslistRegis];
        }
        
    } errorHandler:^(NSError *error){
        
        [self hideMBProcesslistRegis];
    }];
    
}
 
- (void)showListDevice {
    self.ls.arrayDevieChecked = [[RegistrationFormModel sharedInstance] listDevicePackagesArray];
    self.ls.Id = self.updateRegistrationContractModel.ID;
    [self presentViewController:self.ls animated:YES completion:nil];
    
}

- (void)cancelListDevices:(NSMutableArray *)arrayData {
    [self cratedModelDeviceCreated:arrayData];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)cratedModelDeviceCreated:(NSMutableArray*)arrayData{
    
   [self.arrayData removeAllObjects];
    [[RegistrationFormModel sharedInstance]setListDevicePackagesArray:self.arrayData];
    
    for (int i = 0; i< arrayData.count; i++) {
        NSString *deviceName,*deviceID;
        deviceID = StringFormat(@"%@",[arrayData[i]valueForKey:@"DeviceID"]);
        deviceName = StringFormat(@"%@",[arrayData[i]valueForKey:@"DeviceName"]);
        
        [[RegistrationFormModel sharedInstance] setParamater:deviceID DeviceName:deviceName PromotionDeviceID:@"0" PromotionDeviceText:@"Vui lòng chọn CLKM" Number:@"1"];
    }

    //[[NSNotificationCenter defaultCenter]postNotificationName:@"ReloadDataTable" object:nil];
    if (arrayData.count > 0) {
        NSLog(@"dang di qua day");
    }
    else {
        
        [[RegistrationFormModel sharedInstance] setTotalDevice:0];
        
    }
    self.tableListDeviceView.arrayData = [[RegistrationFormModel sharedInstance] listDevicePackagesArray];
    [self.tableListDeviceView reloadDataTable];
    
}

- (void)showMBProcessListRegis {
    [HUD hide:YES];
    if (self.navigationController.view == nil) {
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
    } else {
        
        HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    }
    HUD.delegate = (id<MBProgressHUDDelegate>)self;
    HUD.labelText = @"Loading...";
    [HUD show:YES];
    
}

- (void)hideMBProcesslistRegis {
    
    [HUD hide:YES];
}

//
-(void)LogOut{
    self.menuContainerViewController.menuState = MFSideMenuStateClosed;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:[NSBundle mainBundle]];
    if(IS_IPHONE4){
        storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone4" bundle:[NSBundle mainBundle]];
    }
    MFSideMenuContainerViewController *container = self.menuContainerViewController;
    
    NSString *firstViewControllerName = @"LoginViewController";
    
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:firstViewControllerName];
    [container setCenterViewController:navigationController];
    //container.enableMenu = NO;
    
}

@end
