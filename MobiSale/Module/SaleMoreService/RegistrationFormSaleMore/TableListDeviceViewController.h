//
//  TableListDeviceViewController.h
//  MobiSale
//
//  Created by Tran Vu on 7/2/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//
@protocol TableListDeviceViewControllerDelegate <NSObject>
@optional
- (void)getListDeviceChecked :(NSMutableArray *)arraySelectedDevies;
- (void)showListDevice;
@end


#import <UIKit/UIKit.h>
#import "ListDeviceEquipmentCell.h"
@interface TableListDeviceViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *btnClickedListDevice;

@property (weak, nonatomic) IBOutlet UITableView *tableViewInfo;
@property (strong, nonatomic) NSMutableArray *arrayData;
@property (weak, nonatomic)id<TableListDeviceViewControllerDelegate>delegate;
- (void)reloadDataTable;
@end
