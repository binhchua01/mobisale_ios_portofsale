//
//  ListDeviceAddViewController.h
//  MobiSale
//
//  Created by Tran Vu on 7/2/17.
//  Copyright © 2017 FPT.RAD.FTool. All rights reserved.
//
@protocol ListDeviceAddViewControllerDelegate <NSObject>

@optional
- (void)sendListDeviceChecked :(NSMutableArray *)arraySelectedDevies;

@end
#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Common_client.h"

@interface ListDeviceAddViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UITableView *tableViewInfo;
@property (weak,nonatomic)id<ListDeviceAddViewControllerDelegate>delegate;

@end
