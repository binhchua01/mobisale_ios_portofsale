//
//  InternetServiceViewController.m
//  MobiSale
//
//  Created by ISC on 8/10/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "InternetServiceViewController.h"
#import "SearchPromotionViewController.h"

@interface InternetServiceViewController ()<NIDropDownDelegate, SearchPromotionDelegate, UIAlertViewDelegate>

@end

@implementation InternetServiceViewController {
    NIDropDown *dropDownView;
    UIButton *buttonSelected;
    KeyValueModel *servicePackageSelected;
    PromotionModel  *promotionSelected;
    PromotionComboModel *comboSelected;
    UIAlertView *alertView;
    BOOL checkThongBao;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    UITapGestureRecognizer *screenTGR = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(handleSingleTap:)];
    [self.view addGestureRecognizer:screenTGR];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkingData)
                                                 name:@"CheckingData"
                                               object:nil];
    
    [self getData];
    checkThongBao = YES;
    self.updateRegistrationContractModel.checkSwitchInternet = NO;
    
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    [self setupData];
    // Checking data selected...
    //    [self checkingData];
    
}
- (void)viewDidDisappear:(BOOL)animated {
    
    if (checkThongBao == YES) {
        if (![self.updateRegistrationContractModel.localType isEqual:@"0"] && _internetServiceSwitch.isOn) {
            [self checkThongBaoBanThem];
            checkThongBao = NO;
        }
    }
}

- (void)callbackWithMessage:(NSString *)message {
    if (self.delegate) {
        [self.delegate callbackViewWithMessage:message];
    }
}

- (void) checkThongBaoBanThem {
    
        alertView = [[UIAlertView alloc]initWithTitle:@"Thông báo" message:@"Bạn đang bán thêm gói Internet cho IPTV Only.Bạn có muốn tiếp tục không " delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel", nil];
        [alertView show];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Setup data for update registration form
- (void)setupData {
    if (self.updateRegistrationContractModel.serviceType == 1) {
        // Gói dịch vụ
        self.updateRegistrationContractModel.localType = servicePackageSelected.Key ?:@"0";
        // ID CLKM
        self.updateRegistrationContractModel.contractPromotionID = promotionSelected.PromotionId ?:@"0";
        // Tổng tiền Internet
        self.updateRegistrationContractModel.internetTotal = promotionSelected.Amount ?:@"0";
        [self postContentInternetTotalNotification];
    }
    // ID CLKM Combo
    self.updateRegistrationContractModel.promotionComboID =  StringFormat(@"%li", comboSelected.comboID ?:0);
    
}

- (void)postContentInternetTotalNotification {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"InternetTotal" object:nil];
    
}
//checking Internet

- (void)checkingData {
    if (self.updateRegistrationContractModel.serviceType == 1) {
        if (self.internetServiceSwitch.isOn) {
            if ([self.updateRegistrationContractModel.localType isEqual:@"0"]) {
                // Chưa chọn gói dịch vụ Internet
                [self callbackWithMessage:@"Vui lòng chọn Gói dịch vụ Internet!"];
                return;
            }
            
            if ([self.updateRegistrationContractModel.promotionID isEqual:@"0"]) {
                // Chưa chọn CLKM internet
                [self callbackWithMessage:@"Vui lòng chọn Khuyến mãi dịch vụ Internet!"];
                return;
            }
            
        }
        
    }
}

#pragma mark - Load data when view begin show
- (void)getData {
    
    /* Internet Only */
    /*
     * Cho phép đăng ký combo, câu lệnh khuyến mãi combo sẽ lấy
     * theo ID câu lệnh khuyến mãi Internet của Hợp đồng.
     */
    if (self.updateRegistrationContractModel.serviceType == 0) {
        [self.internetServiceSwitch setEnabled:NO];
        [self setEnableInternetServiceView:NO];
        [self setEnableComboView:YES];
        [self getPromotionComboData];
        return;
    }
    
    /* IPTV Only */
    /*
     * Cho phép đăng ký thêm dịch vụ internet.
     * Cho phép đăng ký combo, câu lệnh khuyến mãi combo sẽ lấy
     * theo ID câu lệnh khuyến mãi Internet của Hợp đồng.
     */
    
    if (self.updateRegistrationContractModel.serviceType == 1) {
        [self showMBProcess];

        [self loadLocalTypePostWithServiceType:@"2"
                           withServerPackageID:self.updateRegistrationContractModel.localType
                                andPromotionID:self.updateRegistrationContractModel.contractPromotionID];
        
        //19/10/2016 Hieu Do Edit
//        [self setEnableInternetServiceView:YES];
//        [self setEnableComboView:YES];
        [self setEnableInternetServiceView:NO];
        [self setEnableComboView:NO];

        return;
    }
    
    /* IPTV + Internet */
    /*
     * Nếu chưa đăng ký Combp thì cho phép đăng ký combo, câu lệnh khuyến mãi combo sẽ lấy
     * theo ID câu lệnh khuyến mãi Internet của Hợp đồng.
     */
    if (self.updateRegistrationContractModel.serviceType == 2) {
        
        [self.internetServiceSwitch setEnabled:NO];
        [self setEnableInternetServiceView:NO];
        
        if (self.updateRegistrationContractModel.comboStatus == 0) {
            [self setEnableComboView:YES];
        } else {
            [self.comboSwitch setEnabled:NO];
            [self setEnableComboView:NO];
        }
        
        [self getPromotionComboData];
        return;
    }
    
}

- (void)getPromotionComboData {
    [self.servicePackageButton setTitle:self.updateRegistrationContractModel.localTypeName
                               forState:UIControlStateNormal];
    [self.promotionButton setTitle:self.updateRegistrationContractModel.contractPromotionName
                          forState:UIControlStateNormal];
    
    if (self.viewType == UpdateRegisterForm) {
        
        [self showMBProcess];
        [self loadPromotionComboWithID:self.updateRegistrationContractModel.promotionComboID
                       withPromotionID:self.updateRegistrationContractModel.contractPromotionID
                          andLocalType:self.updateRegistrationContractModel.localType];
        return;
    }
    
    if (self.viewType == CreateRegisterForm) {
        [self showMBProcess];
        [self loadPromotionComboWithID:@"0"
                       withPromotionID:self.updateRegistrationContractModel.contractPromotionID
                          andLocalType:self.updateRegistrationContractModel.localType];
        return;
    }
}

#pragma mark - Button Action
- (IBAction)buttonPressed:(id)sender {
    buttonSelected = sender;
    if (sender == self.servicePackageButton && self.servicePackageArray.count > 0) {
        [self showDropDownAtButton:sender withArrayData:self.servicePackageArray];
        return;
    }
    
    if (sender == self.promotionButton && self.promotionArray.count > 0) {
        SearchPromotionViewController *vc = [[SearchPromotionViewController alloc] init];
        vc.delegate = self;

        vc.promotionModel = promotionSelected;
        vc.arrData = [self getDataForDropDownView:self.promotionArray andButton:sender];
        [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomTop];
        return;
    }
    
    if (sender == self.comboButton && self.comboArray.count > 0) {
        [self showDropDownAtButton:sender withArrayData:self.comboArray];
        return;
    }
}

#pragma mark - Switch action
- (IBAction)switchPressed:(id)sender {
    BOOL status = NO;

    if (sender == self.internetServiceSwitch) {
        if ([self.internetServiceSwitch isOn]) {
            
            self.updateRegistrationContractModel.checkSwitchInternet = YES;
            status = YES;
        }
        [self setEnableInternetServiceView:status];
        [self setEnableComboView:status];
        
        return;
    }
    
    if (sender == self.comboSwitch) {
        if ([self.comboSwitch isOn]) {
            status = YES;
        }
        [self setEnableComboView:status];
        
        return;
    }
}

#pragma mark - Enable Internet service view and combo view
- (void)setEnableInternetServiceView:(BOOL)status {
    [self.internetServiceSwitch setOn:status];
    [self.servicePackageButton setEnabled:status];
    [self.promotionButton setEnabled:status];
    if (!status) {
        [self clearDataServicePackage];
        [self clearDataPromotionInternet];
    }
}

- (void)setEnableComboView:(BOOL)status {
    [self.comboSwitch setOn:status];
    [self.comboButton setEnabled:status];
    if (!status) {
        [self clearDataPromotionCombo];
    }
}

#pragma mark - SearchPromotionDelegate methods
- (void)cancelSearchPromotionView:(PromotionModel *)selectedPromo {
    if (selectedPromo == nil) {
        return;
    }
    promotionSelected = selectedPromo;
    [self.promotionButton setTitle:promotionSelected.PromotionName forState:UIControlStateNormal];
    
    [self showMBProcess];
    [self loadPromotionComboWithID:@"0" withPromotionID:promotionSelected.PromotionId andLocalType:servicePackageSelected.Key];
        
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
}


#pragma mark - Handle event touch on screen
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self handleSingleTap:nil];
}

// Hide keyboard and drop down view if them show
- (void)handleSingleTap:(UITapGestureRecognizer *)sender {
    if (dropDownView !=nil) {
        [dropDownView hideDropDown:buttonSelected];
        dropDownView = nil;
    }
}


#pragma mark - NIDropDown method
- (NSMutableArray *)getDataForDropDownView:(NSArray *)inputArray andButton:(UIButton *)sender {
    NSMutableArray *outputArray = [[NSMutableArray alloc] init];
    if (sender == self.servicePackageButton) {
        for (KeyValueModel *item in inputArray) {
            [outputArray addObject:item.Values];
        }
        return outputArray;
        
    }
    
    if (sender == self.promotionButton) {
        for (PromotionModel *item in inputArray) {
            [outputArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:item.PromotionName,@"DisplayText",item,@"PromotionModel", nil]];

        }
        return outputArray;
        
    }
    
    if (sender == self.comboButton) {
        for (PromotionComboModel *item in inputArray) {
            [outputArray addObject:item.comboDescription];
        }
        return outputArray;
        
    }
    return nil;
    
}

- (void)showDropDownAtButton:(UIButton *)sender withArrayData:(NSArray*)arrData{
    [self.view endEditing:YES];
    
    if (dropDownView == nil) {
        NSArray *data = [self getDataForDropDownView:arrData andButton:sender];
        CGFloat height = 200.0;
        if (data.count < 4) {
            height = data.count*40;
        }
        CGFloat width = 0.0;
        NSString *animation = @"down";
        if (sender == self.comboButton) {
            animation = @"up";
        }
        dropDownView = [[NIDropDown alloc] showDropDownAtButton:sender height:&height width:&width data:data images:nil animation:animation];
        dropDownView.delegate = self;
        
        return;
    }
    [dropDownView hideDropDown:sender];
    [self rel];
}

- (void)niDropDownDelegateMethod:(NIDropDown *)sender {
    [self rel];
}

- (void)rel {
    dropDownView = nil;
}

- (void)dropDownListView:(NIDropDown *)dropDownListView didSelectIndexPath:(NSIndexPath *)indexPath atButton:(UIButton *)button{
    [self didDropDownSelected:indexPath.row atButton:button];
}

- (void)didDropDownSelected:(NSInteger)row atButton:(UIButton *)button {
    if (button == self.servicePackageButton) {
        servicePackageSelected = self.servicePackageArray[row];
        [self showMBProcess];
        promotionSelected = nil;
        [self clearDataPromotionCombo];
        [self loadPromotion:@"0" andLocalType:servicePackageSelected.Key];
        return;
    }
    
    if (button == self.comboButton) {
        comboSelected = self.comboArray[row];
        return;
    }
    
}

#pragma mark - Clear Data
- (void)clearDataServicePackage {
    servicePackageSelected = nil;
    [self.servicePackageButton setTitle:@"[Chọn gói dịch vụ]" forState:UIControlStateNormal];
}

- (void)clearDataPromotionInternet {
    if (servicePackageSelected == nil) {
        [self.promotionArray removeAllObjects];
    }
    promotionSelected = nil;
    [self.promotionButton setTitle:@"[Chọn câu lệnh khuyến mãi]" forState:UIControlStateNormal];
    
}

- (void)clearDataPromotionCombo {
    if (promotionSelected == nil && self.updateRegistrationContractModel.serviceType == 1) {
        [self.comboArray removeAllObjects];
    }
    comboSelected = nil;
    [self.comboButton setTitle:@"[Chọn câu lệnh khuyến mãi Combo]" forState:UIControlStateNormal];
}

#pragma mark - Load list service package with service type: internet or internet-IPTV

- (void)loadLocalTypePostWithServiceType:(NSString *)serviceType withServerPackageID:(NSString *)serverPackageID andPromotionID:(NSString *)promotionID {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    ShareData *shared = [ShareData instance];
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:serviceType forKey:@"ServiceType"];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    [dict setObject:self.updateRegistrationContractModel.contract forKey:@"Contract"];
    
    [shared.appProxy GetListLocalTypePost:dict completionHandler:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        //vutt11
        self.servicePackageArray = result;
        
        if (self.servicePackageArray.count <= 0 ) {
            servicePackageSelected = nil;
            [self.servicePackageButton setTitle:@"[Không có câu lệnh khuyến mãi]" forState:UIControlStateNormal];
            
            [self hideMBProcess];
            
            return;
        }
        
        if([serverPackageID isEqualToString:@"0"]){
            
            servicePackageSelected = nil;
            [self.servicePackageButton setTitle:@"[Chọn gói dịch vụ]" forState:UIControlStateNormal];
            
        }else {
            
            for (servicePackageSelected in self.servicePackageArray) {
                if([serverPackageID isEqualToString:servicePackageSelected.Key]){
                    [self.servicePackageButton setTitle:servicePackageSelected.Values forState:UIControlStateNormal];
                    
                    [self loadPromotion:promotionID andLocalType:servicePackageSelected.Key];
                    
                    return;
                }
            }
        }
        
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetListLocalTypePost",[error localizedDescription]]];
        [self hideMBProcess];
        
    }];
    
}

#pragma mark - Load list promotion statement with service package id

-(void)loadPromotion:(NSString *)promotionID andLocalType:(NSString *)localtype {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    [shared.appProxy GetListPromotion:localtype Complatehander:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if (errorCode.length < 1) {
            errorCode=@"<null>";
        }
        if(![errorCode isEqualToString:@"<null>"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        self.promotionArray = result;
        
        if (self.promotionArray.count <= 0 ) {
            [self showAlertBox:@"Thông Báo" message:@"Không có câu lệnh khuyến mãi"];
            promotionSelected = nil;
            [self.promotionButton setTitle:@"[Không có câu lệnh khuyến mãi]" forState:UIControlStateNormal];
            
            [self hideMBProcess];
            
            return;
        }
        
        if ([promotionID isEqualToString:@"0"]) {
            promotionSelected = nil;
            [self.promotionButton setTitle:@"[Chọn câu lệnh khuyến mãi]" forState:UIControlStateNormal];
            
        }
        //before is else
        if (self.promotionArray.count>0)
            
         {
            for (promotionSelected in self.promotionArray) {
                if([promotionID isEqualToString:promotionSelected.PromotionId]){
                    [self.promotionButton setTitle:promotionSelected.PromotionName forState:UIControlStateNormal];
                    
                    [self loadPromotionComboWithID:self.updateRegistrationContractModel.promotionComboID withPromotionID:promotionID andLocalType:localtype];
                    
                    return;
                }
            }
        }
        
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetListPromotion",[error localizedDescription]]];
    }];
    
}

#pragma mark - Load list promotion statement with service package id

-(void)loadPromotionComboWithID:(NSString *)ID withPromotionID:(NSString *)promotionID andLocalType:(NSString *)localtype {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    
    ShareData *shared = [ShareData instance];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:promotionID forKey:@"PromotionID"];
    [dict setObject:localtype forKey:@"LocalType"];
    [dict setObject:shared.currentUser.userName forKey:@"UserName"];
    [dict setObject:self.updateRegistrationContractModel.contract forKey:@"Contract"];
    
    [shared.saleMoreServiceProxy getPromotionCombo:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        
        self.comboArray = result;
        
        if (self.comboArray.count <= 0) {
            [self showAlertBox:@"Thông Báo" message:@"Không có câu lệnh khuyến mãi Combo"];
            
            comboSelected = nil;
            [self.comboButton setTitle:@"[Không có câu lệnh khuyến mãi Combo]" forState:UIControlStateNormal];
            
            [self hideMBProcess];
            
            return;
        }
        
        if ([ID isEqualToString:@"0"]) {
            comboSelected = nil;
            [self.comboButton setTitle:@"[Chọn câu lệnh khuyến mãi Combo]" forState:UIControlStateNormal];
            
            [self hideMBProcess];
            
            return;
            
        } else {
            for (comboSelected in self.comboArray) {
                if([ID integerValue] == comboSelected.comboID){
                    [self.comboButton setTitle:comboSelected.comboDescription forState:UIControlStateNormal];
                    
                    [self hideMBProcess];
                    
                    return;
                }
            }
        }
        
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        [self hideMBProcess];
        [self showAlertBox:@"Thông Báo" message:[NSString stringWithFormat:@"%@-%@",@"GetListPromotion",[error localizedDescription]]];
    }];
    
}

#pragma mark - action delegate method
- (void)alertView:(UIAlertView *)alertView1 clickedButtonAtIndex:(NSInteger)buttonIndex

{
    switch (buttonIndex) {
        case 1:
            [self.navigationController popViewControllerAnimated:YES];
            
            break;
            
        default:
            break;
    }
    
}

@end
