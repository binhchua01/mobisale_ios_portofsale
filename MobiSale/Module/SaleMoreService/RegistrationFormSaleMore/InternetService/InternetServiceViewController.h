//
//  InternetServiceViewController.h
//  MobiSale
//
//  Created by ISC on 8/10/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "RegistrationFormSaleMorePageViewController.h"

@protocol InternetServiceViewDelegate <NSObject>

@optional
- (void)callbackViewWithMessage:(NSString *)message;

@end

@interface InternetServiceViewController : BaseViewController

@property (strong, nonatomic) UpdateRegistrationContractModel *updateRegistrationContractModel;

@property (strong, nonatomic) id<InternetServiceViewDelegate> delegate;
// View Type: Create or Update Registration Form
@property (assign, nonatomic) ViewType viewType;

@property (strong, nonatomic) NSMutableArray *servicePackageArray;
@property (strong, nonatomic) NSMutableArray *promotionArray;
@property (strong, nonatomic) NSMutableArray *comboArray;

@property (weak, nonatomic) IBOutlet UISwitch *internetServiceSwitch;
// Gói dịch vụ
@property (weak, nonatomic) IBOutlet UIButton *servicePackageButton
;
// Khuyến mãi
@property (weak, nonatomic) IBOutlet UIButton *promotionButton;

@property (weak, nonatomic) IBOutlet UISwitch *comboSwitch;
// Khuyến mãi Combo
@property (weak, nonatomic) IBOutlet UIButton *comboButton;

- (IBAction)buttonPressed:(id)sender;
- (IBAction)switchPressed:(id)sender;

@end
