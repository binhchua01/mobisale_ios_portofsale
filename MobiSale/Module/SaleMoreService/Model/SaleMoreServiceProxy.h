//
//  SaleMoreServiceProxy.h
//  MobiSale
//
//  Created by ISC on 8/8/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseProxy.h"

#define Response_Result              @"ResponseResult"
#define Error_Description            @"Error"
#define Error_Code                   @"ErrorCode"
#define List_Object                  @"ListObject"

@interface SaleMoreServiceProxy : BaseProxy

// Tìm hợp đồng bán thêm dịch vụ
- (void)getContractList:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Lấy CLKM Combo
- (void)getPromotionCombo:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Cập nhật PĐK bán thêm dịch vụ
- (void)updateRegistrationContract:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Thu tiền PĐK bán thêm bằng tiền mặt
- (void)depositRegisterContract:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Thu tiền PĐK bán thêm sau khi thanh toàn bằng mPOS thành công
- (void)depositRegisterContractmPOS:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Kiểm tra trước khi Thanh toán PĐK bán thêm bằng mPOS
- (void)checkDepositRegisterContract:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
@end
