//
//  PromotionComboModel.h
//  MobiSale
//
//  Created by ISC on 8/12/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PromotionComboModel : NSObject

// ID CLKM Combo
@property (assign, nonatomic) NSInteger comboID;
// ID CLKM Internet
@property (assign, nonatomic) NSInteger promotionID;
// ID CLKM IPTV
@property (assign, nonatomic) NSInteger promotionPayTVID;
// Nội dung CLKM Combo
@property (strong, nonatomic) NSString *comboDescription;

- (id)initWithData:(NSDictionary *)data;

@end
