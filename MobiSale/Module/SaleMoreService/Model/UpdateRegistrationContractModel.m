//
//  UpdateRegistrationContractModel.m
//  MobiSale
//
//  Created by ISC on 8/19/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "UpdateRegistrationContractModel.h"
#import "ShareData.h"



@implementation UpdateRegistrationContractModel

-(NSMutableArray *)arraylistDevice {
    
    if (!_arraylistDevice) {
        _arraylistDevice = [[NSMutableArray alloc] init];
    }
    return _arraylistDevice;
}

- (id) initWithRegistrationFormDetailRecord:(RegistrationFormDetailRecord *)record {
    if (record != nil) {
        
        self.userName   = record.UserName;
        self.regCode    = record.RegCode;
        self.contract   = record.Contract;
        self.fullName   = record.FullName;
        self.address    = record.Address;
        self.email      = record.Email;
        self.phone1     = record.Phone_1;
        self.type1      = record.Type_1;
        self.contact1   = record.Contact_1;
        //get Phone 2
        self.phone2     = record.Phone_2;
        self.contact2   = record.Contact_2;
        if ([record.LocalType integerValue] != 0) {
            self.localType  = record.LocalType;

        } else {
            self.localType  = record.contractLocalType;

        }
        self.localTypeName = record.LocalTypeName;
        self.serviceType   = [record.contractServiceType integerValue];
        self.serviceTypeName = record.contractServiceTypeName;
        self.promotionID         = record.PromotionID;
        self.promotionComboID    = record.promotionComboID;
        self.iptvUseCombo        = record.IPTVUseCombo;
        self.iptvRequestSetup    = record.IPTVRequestSetUp;
        self.iptvRequestDrillWall= record.IPTVRequestDrillWall;
        self.iptvStatus      = record.IPTVStatus;
        self.iptvPromotionID = record.IPTVPromotionID;
        self.iptvPackage     = record.IPTVPackage;
        self.iptvBoxCount    = record.IPTVBoxCount;
        self.iptvPLCCount    = record.IPTVPLCCount;
        self.iptvReturnSTBCount = record.IPTVReturnSTBCount;
        self.iptvChargeTimes    = record.IPTVChargeTimes;
        self.iptvPrepaid        = record.IPTVPrepaid;
        self.iptvVTVChargeTimes = record.IPTVVTVChargeTimes;
        self.iptvVTVPrepaidMonth    = record.IPTVVTVPrepaidMonth;
        self.iptvKPlusChargeTimes   = record.IPTVKPlusChargeTimes;
        self.iptvKPlusPrepaidMonth  = record.IPTVKPlusPrepaidMonth;
        self.iptvVTCChargeTimes     = record.IPTVVTCChargeTimes;
        self.iptvVTCPrepaidMonth    = record.IPTVVTCPrepaidMonth;
        self.iptvFimPlusChargeTimes = record.IPTVFimPlusChargeTimes;
        self.iptvFimPlusPrepaidMonth= record.IPTVFimPlusPrepaidMonth;
        self.iptvFimHotChargeTimes  = record.IPTVFimHotChargeTimes;
        self.iptvFimHotPrepaidMonth = record.IPTVFimHotPrepaidMonth;
        self.iptvPromotionIDBoxOrder= record.IPTVPromotionIDBoxOrder;
        self.iptvDeviceTotal        = record.IPTVDeviceTotal;
        self.iptvPrepaidTotal       = record.IPTVPrepaidTotal;
        self.iptvTotal              = record.IPTVTotal;
        self.internetTotal          = record.InternetTotal;
        self.total                  = record.Total;
        self.deviceTotal            = record.DeviceTotal;
        self.contractLocalType      = record.contractLocalType;
        self.contractComboStatus    = record.contractComboStatus;
        self.contractPromotionID    = record.contractPromotionID;
        self.contractPromotionName  = record.contractPromotionName;
        
        self.arraylistDevice        = record.ListDevice;
        self.ID                     = record.ID;
        
    }

    return self;
}

- (id)initWithInfoContractRecord:(InfoContractRecord *)record {
    self.userName  = [ShareData instance].currentUser.userName;   // Tên đăng nhập
    self.contract  = record.contract;       // Số hợp đồng
    self.fullName  = record.fullName;       // Họ tên
    self.address   = record.address;        // Điạ chỉ khách hàng
    self.phone1    = record.phone_1;        // Số điện thoại 1
    self.contact1  = record.contact_1;      // Người liên hệ 1
    self.email     = record.email;          // email khách hàng
    self.localType = StringFormat(@"%li", (long)record.localType);      // loại dịch vụ
    self.localTypeName = record.localTypeName;  // Loại dịch vụ
    
    // Số lượng Box đã đăng ký
    self.boxCount = record.boxCount;
    // Loại dịch vụ đã đăng ký( 0: internet, 1:IPTV, 2: Internet+IPTV, ngoài ra "Không xác định")
    self.serviceType = record.serviceType;
    // Mô tả loại dịch vụ
    self.serviceTypeName = record.serviceTypeName;
    // ID PĐK bán thêm (0: chưa tạo PĐK, > 0: ID PĐK đã tạo trước đó)
    self.regIDNew = record.regIDNew;
    // Mã PĐK bán thêm(rỗng: chưa tạo PĐK bán thêm, khác rỗng: cho phép update)
    self.regCodeNew = record.regCodeNew;
    // Tình trạng Combo(0: chưa combo, >0: đã combo)
    self.comboStatus = record.comboStatus;
    // Mô tả tình trạng combo
    self.comboStatusName = record.comboStatusName;
    // ID câu lệnh khuyến mãi internet (dùng để lấy câu lệnh khuyến mãi combo/ select lại)
    self.contractPromotionID = StringFormat(@"%li", (long)record.promotionID);
    // Tên câu lệnh khuyến mãi
    self.contractPromotionName = record.promotionName;
    
    return self;
}

- (NSMutableDictionary *)setupData {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    if ([self.arraylistDevice isEqual:[NSNull null]]) {
        
        self.arraylistDevice = [[NSMutableArray alloc] init];
    }
    
    [dict setObject: self.userName      ?:@""    forKey:@"UserName"];
    [dict setObject: self.regCode       ?:@""    forKey:@"RegCode"];
    [dict setObject: self.contract      ?:@""    forKey:@"Contract"];
    [dict setObject: self.email         ?:@""    forKey:@"Email"];
    [dict setObject: self.phone1        ?:@""    forKey:@"Phone_1"];
    [dict setObject: self.type1         ?:@"0"   forKey:@"Type_1"];
    [dict setObject: self.contact1      ?:@""    forKey:@"Contact_1"];
    [dict setObject: self.localType     ?:@"0"   forKey:@"LocalType"];
    [dict setObject: self.contractPromotionID   ?:@"0"   forKey:@"PromotionID"];
    [dict setObject: self.promotionComboID  ?:@"0"   forKey:@"PromotionComboID"];
    [dict setObject: self.iptvUseCombo      ?:@"0"   forKey:@"IPTVUseCombo"];
    [dict setObject: self.iptvRequestSetup  ?:@"0"   forKey:@"IPTVRequestSetUp"];
    [dict setObject: self.iptvRequestDrillWall  ?:@"0"   forKey:@"IPTVRequestDrillWall"];
    [dict setObject: self.iptvStatus            ?:@"0"   forKey:@"IPTVStatus"];
    [dict setObject: self.iptvPromotionID       ?:@"0"   forKey:@"IPTVPromotionID"];
    [dict setObject: self.iptvPackage           ?:@""    forKey:@"IPTVPackage"];
    [dict setObject: self.iptvBoxCount          ?:@"0"   forKey:@"IPTVBoxCount"];
    [dict setObject: self.iptvPLCCount          ?:@"0"   forKey:@"IPTVPLCCount"];
    [dict setObject: self.iptvReturnSTBCount    ?:@"0"   forKey:@"IPTVReturnSTBCount"];
    [dict setObject: self.iptvChargeTimes       ?:@"0"   forKey:@"IPTVChargeTimes"];
    [dict setObject: self.iptvPrepaid           ?:@"0"   forKey:@"IPTVPrepaid"];
    [dict setObject: self.iptvVTVChargeTimes    ?:@"0"   forKey:@"IPTVVTVChargeTimes"];
    [dict setObject: self.iptvVTVPrepaidMonth   ?:@"0"   forKey:@"IPTVVTVPrepaidMonth"];
    [dict setObject: self.iptvKPlusChargeTimes  ?:@"0"   forKey:@"IPTVKPlusChargeTimes"];
    [dict setObject: self.iptvKPlusPrepaidMonth ?:@"0"   forKey:@"IPTVKPlusPrepaidMonth"];
    [dict setObject: self.iptvVTCChargeTimes    ?:@"0"   forKey:@"IPTVVTCChargeTimes"];
    [dict setObject: self.iptvVTCPrepaidMonth   ?:@"0"   forKey:@"IPTVVTCPrepaidMonth"];
    [dict setObject: self.iptvFimPlusChargeTimes    ?:@"0"   forKey:@"iFimPlusChargeTimes"];
    [dict setObject: self.iptvFimPlusPrepaidMonth   ?:@"0"   forKey:@"iFimPlusPrepaidMonth"];
    [dict setObject: self.iptvFimHotChargeTimes     ?:@"0"   forKey:@"iFimHotChargeTimes"];
    [dict setObject: self.iptvFimHotPrepaidMonth    ?:@"0"   forKey:@"iFimHotPrepaidMonth"];
    [dict setObject: self.iptvPromotionIDBoxOrder   ?:@"0"   forKey:@"iIPTVPromotionIDBoxOrder"];
    [dict setObject: self.iptvDeviceTotal           ?:@"0"   forKey:@"IPTVDeviceTotal"];
    [dict setObject: self.iptvPrepaidTotal          ?:@"0"   forKey:@"IPTVPrepaidTotal"];
    [dict setObject: self.iptvTotal                 ?:@"0"   forKey:@"IPTVTotal"];
    [dict setObject: self.internetTotal             ?:@"0"   forKey:@"InternetTotal"];
    [dict setObject: self.total                     ?:@"0"   forKey:@"Total"];
    
    [dict setObject:self.arraylistDevice            ?:@""  forKey:@"ListDevice"];
    
    ShareData *shared = [ShareData instance];
    [dict setObject:shared.currentUser.LocationID            ?:@"0"   forKey:@"LocationID"];
    
    
    return dict;
}

@end
