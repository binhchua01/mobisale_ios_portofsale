//
//  PromotionComboModel.m
//  MobiSale
//
//  Created by ISC on 8/12/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "PromotionComboModel.h"
#import "AppDelegate.h"
#import "ShareData.h"

@implementation PromotionComboModel

- (id)initWithData:(NSDictionary *)data {
    @try {
        self.comboID = [data[@"ComboID"] integerValue];
        self.promotionID = [data[@"PromotionID"] integerValue];
        self.promotionPayTVID = [data[@"PromotionPayTVID"] integerValue];
        self.comboDescription = data[@"Description"];
        
        return self;
        
    } @catch (NSException *exception) {
        NSLog(@"%@", exception.description);
          [CrashlyticsKit recordCustomExceptionName:@"PromotionComboModel - funtion (saveReceiveRemoteNotification)-Error handle Promotion" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
    }
}

@end
