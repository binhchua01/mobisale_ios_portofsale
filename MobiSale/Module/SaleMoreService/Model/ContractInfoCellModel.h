//
//  ContracInfoCellModel.h
//  MobiSale
//
//  Created by ISC on 8/8/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContractInfoCellModel : NSObject

// Địa chỉ khách hàng
@property (strong, nonatomic) NSString *address;
// Tình trạng đăng ký combo (= 0: chưa đăng ký, >0: đã đăng ký)
@property (assign, nonatomic) NSInteger comboStatus;
// Mô tả tình trạng đăng ký combo
@property (strong, nonatomic) NSString *comboStatusName;
 // Số hợp đồng
@property (strong, nonatomic) NSString *contract;
// Tên khách hàng
@property (strong, nonatomic) NSString *fullName;
// Loại dịch vụ internet
@property (strong, nonatomic) NSString *localTypeName;
// Số điện thoại khách hàng
@property (strong, nonatomic) NSString *phone;
// Loại dịch vụ đã đăng ký (0: internet, 1: IPTV, 2: internet & IPTV, ngoài ra là "Không xác định")
@property (assign, nonatomic) NSInteger serviceType;
// Mô tả loại dịch vụ đã đăng ký
@property (strong, nonatomic) NSString *serviceTypeName;

- (id)initWithData:(NSDictionary *)data;

@end
