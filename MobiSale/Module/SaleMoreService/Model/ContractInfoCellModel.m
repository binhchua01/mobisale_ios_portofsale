//
//  ContracInfoCellModel.m
//  MobiSale
//
//  Created by ISC on 8/8/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "ContractInfoCellModel.h"
#import "AppDelegate.h"
#import "ShareData.h"
@implementation ContractInfoCellModel

- (id)initWithData:(NSDictionary *)data {
    @try {
        self.address         = data[@"Address"];
        self.comboStatus     = [data[@"ComboStatus"] integerValue];
        self.comboStatusName = data[@"ComboStatusName"];
        self.contract        = data[@"Contract"];
        self.fullName        = data[@"FullName"];
        self.localTypeName   = data[@"LocalTypeName"];
        self.phone           = data[@"Phone"];
        self.serviceType     = [data[@"ServiceType"] integerValue];
        self.serviceTypeName = data[@"ServiceTypeName"];
        
        return self;
        
    } @catch (NSException *exception) {
        NSLog(@"%@", exception.description);
         [CrashlyticsKit recordCustomExceptionName:@"ContractInfoCellModel - funtion (saveReceiveRemoteNotification)" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
        return nil;
    }
}

@end
