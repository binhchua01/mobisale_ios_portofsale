//
//  SaleMoreServiceProxy.m
//  MobiSale
//
//  Created by ISC on 8/8/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "SaleMoreServiceProxy.h"
#import "ContractInfoCellModel.h"
#import "PromotionComboModel.h"
#import "FPTUtility.h"
#import "Common_client.h"
#import "AppDelegate.h"
#import "ShareData.h"
//mGetObjectList
#define mGetObjectList               @"GetObjectList"
#define mGetPromotionCombo            @"GetPromotionCombo"
#define mUpdateRegistrationContract   @"UpdateRegistrationContract"
#define mDepositRegisterContract      @"DepositRegisterContract"
#define mDepositRegisterContractmPOS  @"DepositRegisterContractmPOS"
#define mCheckDepositRegisterContract @"CheckDepositRegisterContract"

@implementation SaleMoreServiceProxy

// Tìm hợp đồng bán thêm dịch vụ
- (void)getContractList:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    [self requestWithPath:mGetObjectList withParameters:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if(![errorCode isEqualToString:@"0"]){
            handler(nil, errorCode, message);
            return;
        }
        NSArray *data = result;
        if(data.count <= 0){
            errorCode = @"0";
            handler(nil, errorCode, @"Không có dữ liệu trả về!");
            return;
        }
        
        NSMutableArray *mArray = [NSMutableArray array];
        for (NSDictionary *dict in data) {
            ContractInfoCellModel *item = [[ContractInfoCellModel alloc] initWithData:dict];
            [mArray addObject:item];
        }
        
        handler(mArray, errorCode, @"Tải dữ liệu thành công!");
        
    } errorHandler:^(NSError *error) {
        errHandler(error);
        
    }];
    
}

// Lấy CLKM Combo
- (void)getPromotionCombo:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    [self requestWithPath:mGetPromotionCombo withParameters:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if(![errorCode isEqualToString:@"0"]){
            handler(nil, errorCode, message);
            return;
        }
        NSArray *data = result;
        if(data.count <= 0){
            errorCode = @"0";
            handler(nil, errorCode, @"Không có dữ liệu trả về!");
            return;
        }
        
        NSMutableArray *mArray = [NSMutableArray array];
        for (NSDictionary *dict in data) {
            PromotionComboModel *item = [[PromotionComboModel alloc] initWithData:dict];
            [mArray addObject:item];
        }
        
        handler(mArray, errorCode, @"Tải dữ liệu thành công!");
        
    } errorHandler:^(NSError *error) {
        errHandler(error);
        
    }];
    
}

// Cập nhật PĐK bán thêm dịch vụ
- (void)updateRegistrationContract:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    [self requestWithPath:mUpdateRegistrationContract withParameters:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if(![errorCode isEqualToString:@"0"]){
            handler(nil, errorCode, message);
            return;
        }
        NSArray *data = result;
        
        if(data.count <= 0){
            errorCode = @"0";
            handler(nil, errorCode, @"Không có dữ liệu trả về!");
            return;
        }
        
        NSDictionary *dict = data[0];
        message = dict[@"Result"];
        NSString *resultID = dict[@"ResultID"];
     
        
        handler(data, resultID, message);
        
    } errorHandler:^(NSError *error) {
        errHandler(error);
        
    }];
    
}

// Thu tiền PĐK bán thêm bằng tiền mặt
- (void)depositRegisterContract:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    [self requestWithPath:mDepositRegisterContract withParameters:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if(![errorCode isEqualToString:@"0"]){
            handler(nil, errorCode, message);
            return;
        }
        NSArray *data = result;
        if(data.count <= 0){
            errorCode = @"0";
            handler(nil, errorCode, @"Không có dữ liệu trả về!");
            return;
        }
        
        NSDictionary *dict = data[0];
        message = dict[@"Result"];
        NSString *resultID = dict[@"ResultID"];
        
        
        handler(data, resultID, message);
        
    } errorHandler:^(NSError *error) {
        errHandler(error);
        
    }];
    
}

// Thu tiền PĐK bán thêm sau khi thanh toàn bằng mPOS thành công
- (void)depositRegisterContractmPOS:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    [self requestWithPath:mDepositRegisterContractmPOS withParameters:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if(![errorCode isEqualToString:@"0"]){
            handler(nil, errorCode, message);
            return;
        }
        NSArray *data = result;
        if(data.count <= 0){
            errorCode = @"0";
            handler(nil, errorCode, @"Không có dữ liệu trả về!");
            return;
        }
        
        NSDictionary *dict = data[0];
        message = dict[@"Result"];
        NSString *resultID = dict[@"ResultID"];
        
        
        handler(data, resultID, message);
        
    } errorHandler:^(NSError *error) {
        errHandler(error);
        
    }];
    
}

// Kiểm tra trước khi Thanh toán PĐK bán thêm bằng mPOS
- (void)checkDepositRegisterContract:(NSDictionary *)dict completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    [self requestWithPath:mCheckDepositRegisterContract withParameters:dict completeHandler:^(id result, NSString *errorCode, NSString *message) {
        if(![errorCode isEqualToString:@"0"]){
            handler(nil, errorCode, message);
            return;
        }
        NSArray *data = result;
        if(data.count <= 0){
            errorCode = @"0";
            handler(nil, errorCode, @"Không có dữ liệu trả về!");
            return;
        }
        
        NSDictionary *dict = data[0];
        message = dict[@"Result"];
        NSString *resultID = dict[@"ResultID"];
        
        
        handler(data, resultID, message);
        
    } errorHandler:^(NSError *error) {
        errHandler(error);
        
    }];
    
}

#pragma mark - Create request to Web service
- (void)requestWithPath:(NSString *)path withParameters:(NSDictionary *)parameters completeHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, path)];
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    //create json
    NSString *postString = [parameters JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endRequest:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err) {
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endRequest:(NSData *)result response:(NSURLResponse *)response
 completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    /*Parse json response*/
    @try {
        NSDictionary *jsonDict = [self.parser objectWithData:result];
        jsonDict = [jsonDict objectForKey:Response_Result]?:nil;
        NSString *error_code = [NSString stringWithFormat:@"%@", [jsonDict objectForKey:Error_Code]?:nil] ;
        NSString *error_description = StringFormat(@"%@",[jsonDict objectForKey: Error_Description]);
       NSArray *data = [jsonDict objectForKey:List_Object]?:nil;
        
        handler(data, error_code, error_description);
    }
    
    @catch (NSException *exception) {
        NSLog(@"%@", exception.description);
        handler(nil,nil,@"Error!");
         [CrashlyticsKit recordCustomExceptionName:@"SaleMoreServiceProxy - funtion (saveReceiveRemoteNotification)-Error handle SaleMoreServiceProxy" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
    }
}

@end
