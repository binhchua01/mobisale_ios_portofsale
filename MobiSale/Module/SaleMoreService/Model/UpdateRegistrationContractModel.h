//
//  UpdateRegistrationContractModel.h
//  MobiSale
//
//  Created by ISC on 8/19/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RegistrationFormDetailRecord.h"
#import "InfoContractRecord.h"

@interface UpdateRegistrationContractModel : NSObject

// Tên đăng nhập
@property (strong, nonatomic) NSString *userName;
// Phiếu đăng ký
@property (strong, nonatomic) NSString *regCode;
// Số hợp đồng
@property (strong, nonatomic) NSString *contract;
// Tên khách hàng
@property (strong, nonatomic) NSString *fullName;
// Địa chỉ khách hàng
@property (strong, nonatomic) NSString *address;
// Email khách hàng
@property (strong, nonatomic) NSString *email;
// Số điện thoại 1 của khách hàng
@property (strong, nonatomic) NSString *phone1;
// Số điện thoại 2 của khách hàng
@property (strong, nonatomic) NSString *phone2;
// Loại số điện thoại 1
@property (strong, nonatomic) NSString *type1;
// Người liên hệ 1
@property (strong, nonatomic) NSString *contact1;
// Người liên hệ 2
@property (strong, nonatomic) NSString *contact2;

// Gói dịch vụ Internet (F2, F3,..)
@property (strong, nonatomic) NSString *localType;
@property (strong, nonatomic) NSString *localTypeName;
// ID CLKM Internet
@property (strong, nonatomic) NSString *promotionID;
// ID CLKM Combo
@property (strong, nonatomic) NSString *promotionComboID;
// IPTV sử dụng combo
@property (strong, nonatomic) NSString *iptvUseCombo;
// Yêu cầu cài đặt IPTV
@property (strong, nonatomic) NSString *iptvRequestSetup;
// Yêu cầu khoan tường
@property (strong, nonatomic) NSString *iptvRequestDrillWall;
// Hình thức triển khai (2: Nâng cấp line, 4: Thêm box)
@property (strong, nonatomic) NSString *iptvStatus;
// Gói dịch vụ IPTV
@property (strong, nonatomic) NSString *iptvPackage;
// ID CLKM IPTV box đầu tiên
@property (strong, nonatomic) NSString *iptvPromotionID;
// Số tiền trả trước (tiền CLKM Box 1)
@property (strong, nonatomic) NSString *iptvPrepaid;
// ID CLKM IPTV Box thứ 2 trở đi
@property (strong, nonatomic) NSString *iptvPromotionIDBoxOrder;
/*StevenNguyen*/
// Loại CLKM của box đầu tiên
@property (strong, nonatomic) NSString *iptvPromotionType;
// Loại CLKM của box thứ 2 trở đi
@property (strong, nonatomic) NSString *iptvPromotionTypeBoxOrder;
// Mô tả CLKM box 1
@property (strong, nonatomic) NSString *iptvPromotionDesc;
/**************/
// Số lượng Box
@property (strong, nonatomic) NSString *iptvBoxCount;
// Số lượng thiết bị PLC
@property (strong, nonatomic) NSString *iptvPLCCount;
// Số lượng STB thu hồi
@property (strong, nonatomic) NSString *iptvReturnSTBCount;
// Số lần tính cước IPTV
@property (strong, nonatomic) NSString *iptvChargeTimes;
// Số tháng trả trước Extra VTV
@property (strong, nonatomic) NSString *iptvVTVPrepaidMonth;
// Số lần trả trước Extra VTV
@property (strong, nonatomic) NSString *iptvVTVChargeTimes;
// Số tháng trả trước Extra K+
@property (strong, nonatomic) NSString *iptvKPlusPrepaidMonth;
// Số lần trả trước Extra K+
@property (strong, nonatomic) NSString *iptvKPlusChargeTimes;
// Số tháng trả trước Extra VTC (Đặc sắc HD)
@property (strong, nonatomic) NSString *iptvVTCPrepaidMonth;
// Số lần trả trước Extra VTC (Đặc sắc HD)
@property (strong, nonatomic) NSString *iptvVTCChargeTimes;
// Số tháng trả trước Extra Fim+
@property (strong, nonatomic) NSString *iptvFimPlusPrepaidMonth;
// Số lần trả trước Extra Fim+
@property (strong, nonatomic) NSString *iptvFimPlusChargeTimes;
// Số tháng trả trước Extra Fim hot
@property (strong, nonatomic) NSString *iptvFimHotPrepaidMonth;
// Số lần trả trước Extra Fim hot
@property (strong, nonatomic) NSString *iptvFimHotChargeTimes;
// Tổng tiền thiết bị IPTV (lấy từ API GetIPTVTotal)
@property (strong, nonatomic) NSString *iptvDeviceTotal;
// Tổng tiền trả trước IPTV (lấy từ API GetIPTVTotal)
@property (strong, nonatomic) NSString *iptvPrepaidTotal;
// Tổng tiền IPTV
@property (strong, nonatomic) NSString *iptvTotal;
// Tổng tiền Internet
@property (strong, nonatomic) NSString *internetTotal;
// Tổng tiền = Tổng tiền IPTV + Tổng tiền Internet
@property (strong, nonatomic) NSString *total;
// Tổng tiền thiết bị
@property (strong, nonatomic) NSString *deviceTotal ;

/*khong update*/

// Số lượng Box đã đăng ký
@property (assign, nonatomic) NSInteger boxCount;
// Loại dịch vụ đã đăng ký( 0: internet, 1:IPTV, 2: Internet+IPTV, ngoài ra "Không xác định")
@property (assign, nonatomic) NSInteger serviceType;
// Mô tả loại dịch vụ
@property (strong, nonatomic) NSString *serviceTypeName;
// ID PĐK bán thêm (0: chưa tạo PĐK, > 0: ID PĐK đã tạo trước đó)
@property (assign, nonatomic) NSInteger regIDNew;
// Mã PĐK bán thêm(rỗng: chưa tạo PĐK bán thêm, khác rỗng: cho phép update)
@property (strong, nonatomic) NSString *regCodeNew;
// Tình trạng Combo(0: chưa combo, >0: đã combo)
@property (assign, nonatomic) NSInteger comboStatus;
// Mô tả tình trạng combo
@property (strong, nonatomic) NSString *comboStatusName;
// Tên câu lệnh khuyến mãi
@property (strong, nonatomic) NSString *promotionName;

// ID CLKM của hợp đồng
@property (strong, nonatomic) NSString *contractPromotionID;
// Tên CLKM của hợp đồng
@property (strong, nonatomic) NSString *contractPromotionName;
// Tình trạng đăng ký CLKM của hợp đồng
@property (strong, nonatomic) NSString *contractComboStatus;
// Gói dịch vụ Internet của hợp đồng
@property (strong, nonatomic) NSString *contractLocalType;
// check chon switch Internet
@property ( nonatomic) BOOL checkSwitchInternet;

// truyền listDevies
@property (strong,nonatomic) NSMutableArray *arraylistDevice;
//ID
@property (strong,nonatomic) NSString *ID;


- (NSMutableDictionary *)setupData;
- (id)initWithRegistrationFormDetailRecord:(RegistrationFormDetailRecord *)record;
- (id)initWithInfoContractRecord:(InfoContractRecord *)record;
@end
