//
//  SaleMoreServiceViewController.h
//  MobiSale
//
//  Created by ISC on 8/8/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface SaleMoreServiceViewController : BaseViewController
// Số kết quả tìm kiếm được
@property (weak, nonatomic) IBOutlet UILabel *resultNumberLabel;
// table hiển thị kết quả tìm kiếm
@property (weak, nonatomic) IBOutlet UITableView *resultTableView;
// image icon up/down on
@property (weak, nonatomic) IBOutlet UIImageView *upDownImageView;
// button loại tìm kiếm: 1: Số hợp đồng, 2: Họ tên, 3: Số điện thoại
@property (weak, nonatomic) IBOutlet UIButton *agentButton;
// text nhập nội dung cần tìm
@property (weak, nonatomic) IBOutlet UITextField *agentTextField;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchViewHeight;


@end
