//
//  CustomerCareRecord.h
//  MobiSale
//
//  Created by ISC-DanTT on 11/30/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StatusDescriptionRecord : NSObject

@property (strong, nonatomic) NSString *code;   // Mã khảo sát
@property (strong, nonatomic) NSString *csat;   // CSAT
@property (strong, nonatomic) NSString *point;  // Điểm tương ứng với CSAT
@property (strong, nonatomic) NSString *note;   // Ghi chú khảo sát
@property (strong, nonatomic) NSString *confirmCode; // Mã xác nhận
@property (strong, nonatomic) NSString *time;   // Thời gian tiến hành khảo sát

- (id)initWithData:(NSDictionary *)dict;

@end

@interface CustomerCareRecord : NSObject

@property (nonatomic, strong) NSString* address;    // Địa chỉ khách hàng
@property (nonatomic, strong) NSString* contract;   // Số hợp đồng
@property (nonatomic, strong) NSString* currentPage; // Trang hiện tại
@property (nonatomic, strong) NSString* email;      // Email khách hàng
@property (nonatomic, strong) NSString* fullName;   // Họ tên khách hàng
@property (nonatomic, strong) NSString* ID;
@property (nonatomic, strong) NSString* levelID; // Cấp độ quan trọng (tăng dần từ 1 - 3)
@property (nonatomic, strong) NSString* objStatusDesc; // Nội dung thông báo (data trả về dạng HTML)
@property (nonatomic, strong) NSString* objStatusName;  // Tiêu đề thông báo
@property (nonatomic, strong) NSString* phoneNumber;    // Số điện thoại khách hàng
@property (nonatomic, strong) NSString* rowNumber;      // Số thứ tự dòng hiện tại
@property (nonatomic, strong) NSString* startDate;      // Ngày bắt đầu xử lý
@property (nonatomic, strong) NSString* totalPage;      // Tổng số trang
@property (nonatomic, strong) NSString* totalRow;       // Tổng số dòng
@property (nonatomic, strong) NSString* createDate;     // Ngày tạo
@property (nonatomic, strong) NSString* createBy;       // Người tạo
@property (nonatomic, strong) NSString* code;           // Mã định danh thông báo (do nguồn gửi tạo)
@property (nonatomic, strong) NSString* divisionID; // ID phòng ban gửi thông báo
@property (nonatomic, strong) NSString* saleName;   // Account sale
@property (nonatomic, strong) NSString* saleFullName;   // Họ tên sale
@property (nonatomic, strong) NSString* saleEmail;  // Email sale
@property (nonatomic, strong) NSString* salePhone;  // Số điện thoại sale

@property (nonatomic, strong) StatusDescriptionRecord *statusDescRecord;

- (id)initWithData:(NSDictionary *)dict;

@end
