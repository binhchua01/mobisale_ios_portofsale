//
//  CustomerCareProxy.h
//  MobiSale
//
//  Created by ISC-DanTT on 11/30/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "BaseProxy.h"
@interface CustomerCareProxy : BaseProxy
- (void)getCustomerCareListWithPageNumber:(NSString*)pageNumber agent:(NSString*)agent agentName:(NSString*)agentName levelID:(NSUInteger)levelID completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;

- (void)getToDoListWithContract:(NSString*)contract pageNumber:(NSString*)pageNumber Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
- (void)updateToDoListDetailWithID:(NSString*)todolistID cateType:(NSString*)careType descripton:(NSString*)description status:(NSString*)status Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
- (void)getToDoListDetailWithContract:(NSString*)contract totoListID:(NSString*)totoListID Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
- (void)getActivitiesList:(NSString*)contract pageNumber:(NSString*)pageNumber completeHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
- (void)getActivitiesListOfSale:(NSString*)contract pageNumber:(NSString*)pageNumber completeHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
// Xác nhận đã nhận thông báo
- (void)confirmNotification:(NSDictionary *)dict completeHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler;
@end
