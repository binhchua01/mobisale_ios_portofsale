//
//  ActivityRecord.m
//  MobiSale
//
//  Created by ISC-DanTT on 12/7/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ActivityRecord.h"

@implementation ActivityRecord

@synthesize activity_Name, activity_Type, activity_Type_Name, activity_By, activity_Code, content, contract, currentPage, date, objectID, remark, totalPage, totalRow;

@end
