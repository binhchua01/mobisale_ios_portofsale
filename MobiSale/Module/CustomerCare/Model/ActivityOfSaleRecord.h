//
//  ActivityOfSaleRecord.h
//  MobiSale
//
//  Created by ISC-DanTT on 12/9/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ActivityOfSaleRecord : NSObject

@property (nonatomic,strong) NSString* activity_Name;
@property (nonatomic,strong) NSString* activity_Type;
@property (nonatomic,strong) NSString* activity_Type_Name;
@property (nonatomic,strong) NSString* activity_By;
@property (nonatomic,strong) NSString* activity_Code;
@property (nonatomic,strong) NSString* activity_Date;
@property (nonatomic,strong) NSString* content;
@property (nonatomic,strong) NSString* contract;
@property (nonatomic,strong) NSString* currentPage;
@property (nonatomic,strong) NSString* assign_Date;
@property (nonatomic,strong) NSString* objectID;
@property (nonatomic,strong) NSString* remark;
@property (nonatomic,strong) NSString* totalPage;
@property (nonatomic,strong) NSString* totalRow;
@property (nonatomic,strong) NSString* customer_Address;
@property (nonatomic,strong) NSString* customer_Fullname;
@property (nonatomic,strong) NSString* customer_Phone;
@property (nonatomic,strong) NSString* staffID;

@end
