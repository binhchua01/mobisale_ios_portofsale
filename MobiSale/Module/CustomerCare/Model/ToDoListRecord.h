//
//  ToDoListRecord.h
//  MobiSale
//
//  Created by ISC-DanTT on 11/30/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CustomerCareRecord.h"

@interface ToDoListRecord : NSObject

@property (nonatomic, strong) NSString* contract;   // Số hợp đồng
@property (nonatomic, strong) NSString* createBy;   // Người tạo
@property (nonatomic, strong) NSString* createDate; // Ngày tạo
@property (nonatomic, strong) NSString* currentPage;    // Trang hiện tại
@property (nonatomic, strong) NSString* divisionName;   // Tên phòng ban
@property (nonatomic, strong) NSString* fullName;       // Họ tên Khách hàng
@property (nonatomic, strong) NSString* email;          // Email khách hàng
@property (nonatomic, strong) NSString* ID;             // ID cảnh báo
@property (nonatomic, strong) NSString* levelID;        // Cấp độ quan trọng
@property (nonatomic, strong) NSString* levelName;      // Tên cấp độ
@property (nonatomic, strong) NSString* objStatusDesc;  // Nội dung cảnh báo
@property (nonatomic, strong) NSString* objStatusName;  // Tiêu đề cảnh báo
@property (nonatomic, strong) NSString* phoneNumber;    // Số điện thoại khách hàng
@property (nonatomic, strong) NSString* processTime;    // Thơi gian xử lý (đơn vị phút)
@property (nonatomic, strong) NSString* rowNumber;      // Số thứ tự dòng hiện tại
@property (nonatomic, strong) NSString* status;         // Tình trạng xử lý
@property (nonatomic, strong) NSString* totalPage;      // Tổng số trang
@property (nonatomic, strong) NSString* totalRow;       // Tổng số dòng
@property (nonatomic, strong) NSString* startDate;      // Ngày giao xử lý

@property (nonatomic, strong) NSString* processTimeString;
@property (nonatomic, strong) NSString* endDate;        // Ngày kết thúc
@property (nonatomic, strong) NSString* statusName;     //Tình trạng xử lý
@property (nonatomic, strong) NSString* code;           // Mã thông báo do nguồn gửi tạo


@property (nonatomic, strong) NSString* divisionID; // ID phòng ban gửi thông báo
@property (nonatomic, strong) NSString* saleName;   // Account sale
@property (nonatomic, strong) NSString* saleFullName;   // Họ tên sale
@property (nonatomic, strong) NSString* saleEmail;  // Email sale
@property (nonatomic, strong) NSString* salePhone;  // Số điện thoại sale

@property (nonatomic, strong) StatusDescriptionRecord *statusDescRecord;

- (id)initWithData:(NSDictionary *)dict;

@end
