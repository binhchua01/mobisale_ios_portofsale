//
//  ToDoListRecord.m
//  MobiSale
//
//  Created by ISC-DanTT on 11/30/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ToDoListRecord.h"
#import "SBJson.h"

@implementation ToDoListRecord

@synthesize contract, createBy, createDate, currentPage, divisionName, fullName, email, ID, levelID, levelName, objStatusName, objStatusDesc, phoneNumber, processTime, rowNumber, status, totalPage, totalRow, startDate;

- (id)initWithData:(NSDictionary *)dict {
    NSString *str = @"(null)";
    
    self.contract = StringFormat(@"%@",[dict objectForKey:@"Contract"]);
    self.contract = ([self.contract isEqualToString: str]) ? @"": self.contract ;
    
    self.createBy = StringFormat(@"%@",[dict objectForKey:@"CreateBy"]);
    self.createBy = ([self.createBy isEqualToString: str]) ? @"": self.createBy ;
    
    self.createDate = StringFormat(@"%@",[dict objectForKey:@"CreateDate"]);
    self.createDate = ([self.createDate isEqualToString: str]) ? @"": self.createDate ;
    
    self.divisionName = StringFormat(@"%@",[dict objectForKey:@"DivisionName"]);
    self.divisionName = ([self.divisionName isEqualToString: str]) ? @"": self.divisionName;
    
    self.currentPage = StringFormat(@"%@",[dict objectForKey:@"CurrentPage"]);
    self.currentPage = ([self.currentPage isEqualToString: str]) ? @"": self.currentPage ;
    
    self.fullName = StringFormat(@"%@",[dict objectForKey:@"FullName"]);
    self.fullName = ([self.fullName isEqualToString: str]) ? @"": self.fullName ;
    
    self.email = StringFormat(@"%@",[dict objectForKey:@"Email"]);
    self.email = ([self.email isEqualToString: str]) ? @"": self.email ;
    
    self.ID = StringFormat(@"%@",[dict objectForKey:@"ID"]);
    self.ID = ([self.ID isEqualToString: str]) ? @"": self.ID ;
    
    self.levelID = StringFormat(@"%@",[dict objectForKey:@"LevelID"]);
    self.levelID = ([self.levelID isEqualToString: str]) ? @"": self.levelID ;
    
    self.levelName = StringFormat(@"%@",[dict objectForKey:@"LevelName"]);
    self.levelName = ([self.levelName isEqualToString: str]) ? @"": self.levelName ;
    
    self.objStatusDesc = StringFormat(@"%@",[dict objectForKey:@"ObjStatusDesc"]);
    self.objStatusDesc = ([self.objStatusDesc isEqualToString: str]) ? @"": self.objStatusDesc ;
    
    self.objStatusName = StringFormat(@"%@",[dict objectForKey:@"ObjStatusName"]);
    self.objStatusName = ([self.objStatusName isEqualToString: str]) ? @"": self.objStatusName ;
    
    self.phoneNumber = StringFormat(@"%@",[dict objectForKey:@"PhoneNumber"]);
    self.phoneNumber = ([self.phoneNumber isEqualToString: str]) ? @"": self.phoneNumber ;
    
    self.processTime = StringFormat(@"%@",[dict objectForKey:@"ProcessTime"]);
    self.processTime = ([self.processTime isEqualToString: str]) ? @"": self.processTime ;
    
    self.rowNumber = StringFormat(@"%@",[dict objectForKey:@"RowNumber"]);
    self.rowNumber = ([self.rowNumber isEqualToString: str]) ? @"": self.rowNumber ;
    
    self.status = StringFormat(@"%@",[dict objectForKey:@"Status"]);
    self.status = ([self.status isEqualToString: str]) ? @"": self.status ;
    
    self.totalPage = StringFormat(@"%@",[dict objectForKey:@"TotalPage"]);
    self.totalPage = ([self.totalPage isEqualToString: str]) ? @"": self.totalPage ;
    
    self.totalRow = StringFormat(@"%@",[dict objectForKey:@"TotalRow"]);
    self.totalRow = ([self.totalRow isEqualToString: str]) ? @"": self.totalRow ;
    
    self.startDate = StringFormat(@"%@",[dict objectForKey:@"StartDate"]);
    self.startDate = ([self.startDate isEqualToString: str]) ? @"": self.startDate ;
    
    self.processTimeString = dict[@"ProcessTimeString"] ?:@"";
    self.endDate  = dict[@"EndDate"] ?:@"";
    self.statusName = dict[@"StatusName"] ?:@"";
    self.divisionID = StringFormat(@"%@", dict[@"DivisionID"] ?:@"");
    self.saleEmail  = dict[@"SaleEmail"] ?:@"";
    self.salePhone  = dict[@"SalePhone"] ?:@"";
    self.saleFullName = dict[@"SaleFullName"] ?:@"";
    self.saleName   = dict[@"SaleName"] ?:@"";
    self.code       = dict[@"Code"] ?:@"";
    
    SBJsonParser *parser = [[SBJsonParser alloc] init];
    NSDictionary *dictStatusDesc = [parser objectWithString:self.objStatusDesc];
    if (dictStatusDesc != nil) {
        self.statusDescRecord = [[StatusDescriptionRecord alloc] initWithData:dictStatusDesc];
        
    }
    
    return  self;
}

@end
