//
//  ActivityRecord.h
//  MobiSale
//
//  Created by ISC-DanTT on 12/7/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ActivityRecord : NSObject

@property (nonatomic,strong) NSString* activity_Name;
@property (nonatomic,strong) NSString* activity_Type;
@property (nonatomic,strong) NSString* activity_Type_Name;
@property (nonatomic,strong) NSString* activity_By;
@property (nonatomic,strong) NSString* activity_Code;
@property (nonatomic,strong) NSString* content;
@property (nonatomic,strong) NSString* contract;
@property (nonatomic,strong) NSString* currentPage;
@property (nonatomic,strong) NSString* date;
@property (nonatomic,strong) NSString* objectID;
@property (nonatomic,strong) NSString* remark;
@property (nonatomic,strong) NSString* totalPage;
@property (nonatomic,strong) NSString* totalRow;

@end
