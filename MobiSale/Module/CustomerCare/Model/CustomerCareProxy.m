//
//  CustomerCareProxy.m
//  MobiSale
//
//  Created by ISC-DanTT on 11/30/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "CustomerCareProxy.h"
#import "ShareData.h"
#import "CustomerCareRecord.h"
#import "ToDoListRecord.h"
#import "ToDoListDetailRecord.h"
#import "ActivityRecord.h"
#import "ActivityOfSaleRecord.h"
#import "FPTUtility.h"

#define GetCustomerCareList         @"GetCustomerCareList" // POST
#define GetToDoList                 @"GetToDoList"
#define UpdateToDoListDetail        @"UpdateToDoListDetail"
#define GetToDoListDetail           @"GetToDoListDetail"
#define GetActivityListByContract   @"GetActivityListByContract"
#define GetActivityList             @"GetActivityList"
#define ConfirmNotification         @"ConfirmNotification"

@implementation CustomerCareProxy

#pragma mark - Get Customer care List
- (void)getCustomerCareListWithPageNumber:(NSString*)pageNumber agent:(NSString*)agent agentName:(NSString*)agentName levelID:(NSUInteger)levelID completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    
    NSString *username = [ShareData instance].currentUser.userName;
    
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, GetCustomerCareList)]; //POST
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:username ?:@"" forKey:@"UserName"];
    [dict setObject:agent ?:@"0" forKey:@"Agent"];
    [dict setObject:agentName ?:@"" forKey:@"AgentName"];
    [dict setObject:pageNumber forKey:@"PageNumber"];
    [dict setObject:StringFormat(@"%lu",(unsigned long)levelID) forKey:@"Level"];

    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetCustomerCareList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    [callOp start];
}

- (void)endGetCustomerCareList:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *responseResult = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    NSArray *listObject = [responseResult objectForKey:@"ListObject"]?:nil;
    NSString *ErrorCode = StringFormat(@"%@",[responseResult objectForKey:@"ErrorCode"]);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, @"Có lỗi, vui lòng thử lại!");
        return;
    }
    
    if(listObject.count <= 0){
        handler(nil, @"", @"Không có kết quả trả về!");
        return;
    }
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in listObject){
//        CustomerCareRecord*p = [self ParseRecordList:d];
        CustomerCareRecord*p = [[CustomerCareRecord alloc] initWithData:d];

        [mArray addObject:p];
    }
    
    handler(mArray, ErrorCode, @"");
}

- (void)getToDoListWithContract:(NSString*)contract pageNumber:(NSString*)pageNumber Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    
    NSString *username = [ShareData instance].currentUser.userName;
    
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI,GetToDoList)]; //POST
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:username ?:@"" forKey:@"UserName"];
    [dict setObject:contract ?:@"" forKey:@"Contract"];
    [dict setObject:pageNumber ?:@"1" forKey:@"PageNumber"];
    [dict setObject:@"0" forKey:@"Agent"];
    [dict setObject:@"" forKey:@"AgentName"];
    
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        [self endGetToDoList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    [callOp start];
    
}

- (void)endGetToDoList:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *responseResult = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    NSArray *listObject = [responseResult objectForKey:@"ListObject"]?:nil;
    NSString *ErrorCode = StringFormat(@"%@",[responseResult objectForKey:@"ErrorCode"]);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, @"Có lỗi, vui lòng thử lại!");
        return;
    }
    NSString *listObjectString = [listObject description];
    if([listObjectString isEqualToString:@"(\n)"] || [listObjectString isEqualToString:@"<null>"]){
        handler(nil, @"", @"Không có kết quả trả về!");
        return;
    }
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in listObject){
//        ToDoListRecord *p = [self ParseRecordToDoList:d];
        ToDoListRecord *p = [[ToDoListRecord alloc] initWithData:d];

        [mArray addObject:p];
    }
    
    handler(mArray, ErrorCode, @"");
}

- (void)updateToDoListDetailWithID:(NSString*)todolistID cateType:(NSString*)careType descripton:(NSString*)description status:(NSString*)status Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSString *username = [ShareData instance].currentUser.userName;
    
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, UpdateToDoListDetail)]; //POST
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:username forKey:@"UserName"];
    [dict setObject:todolistID forKey:@"ToDoListID"];
    [dict setObject:careType forKey:@"CareType"];
    [dict setObject:description ?:@"" forKey:@"Desc"];
    [dict setObject:status forKey:@"Status"];
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endUpdateToDoListDetail:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
    
    
}

- (void)endUpdateToDoListDetail:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *responseResult = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    NSArray *listObject = [responseResult objectForKey:@"ListObject"]?:nil;
    NSString *errorCode = StringFormat(@"%@",[responseResult objectForKey:@"ErrorCode"]);
    
    if(![errorCode isEqualToString:@"0"]){
        handler(nil, errorCode, @"Có lỗi xảy ra! Vui lòng thử lại");
        return;
    }
    NSString *message = StringFormat(@"%@",[[listObject objectAtIndex:0] objectForKey:@"Result"]);
    handler(listObject, errorCode, message);
}

- (void)getToDoListDetailWithContract:(NSString*)contract totoListID:(NSString*)totoListID Completehander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    BaseOperation * callOp = [[BaseOperation alloc] init];
    NSString *username = [ShareData instance].currentUser.userName;
    
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@",urlAPI, GetToDoListDetail)]; //POST
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:username forKey:@"UserName"];
    [dict setObject:@"1" forKey:@"Agent"];
    [dict setObject:totoListID ?:@"" forKey:@"AgentName"];
    [dict setObject:contract forKey:@"Contract"];
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetToDoListDetail:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    
    [callOp start];
}

- (void)endGetToDoListDetail:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *responseResult = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    NSArray *listObject = [responseResult objectForKey:@"ListObject"]?:nil;
    NSString *ErrorCode = StringFormat(@"%@",[responseResult objectForKey:@"ErrorCode"]);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, @"Có lỗi, vui lòng thử lại!");
        return;
    }
    
    if(listObject.count <= 0){
        handler(nil, @"", @"Không có kết quả trả về!");
        return;
    }
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in listObject){
        ToDoListDetailRecord *p = [self parseToDoListDetailRecord:d];
        [mArray addObject:p];
    }
    handler(mArray, ErrorCode, @"");
}

- (void)getActivitiesList:(NSString*)contract pageNumber:(NSString*)pageNumber completeHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    BaseOperation * callOp = [[BaseOperation alloc] init];
    
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@", urlAPI, GetActivityListByContract)]; //POST
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:contract ?:@"" forKey:@"Contract"];
    [dict setObject:pageNumber forKey:@"PageNumber"];
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetActivitiesList:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    [callOp start];
}

- (void)endGetActivitiesList:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *responseResult = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    NSArray *listObject = [responseResult objectForKey:@"ListObject"]?:nil;
    NSString *ErrorCode = StringFormat(@"%@",[responseResult objectForKey:@"ErrorCode"]);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, @"Có lỗi, vui lòng thử lại!");
        return;
    }
    
    NSString *listObjectString = [listObject description];
    if([listObjectString isEqualToString:@"(\n)"] || [listObjectString isEqualToString:@"<null>"]){
        handler(nil, @"", @"Không có kết quả trả về!");
        return;
    }
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in listObject){
        ActivityRecord*p = [self parseActivityRecord:d];
        [mArray addObject:p];
    }
    
    handler(mArray, ErrorCode, @"");
    
}


- (void)getActivitiesListOfSale:(NSString*)contract pageNumber:(NSString*)pageNumber completeHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    BaseOperation * callOp = [[BaseOperation alloc] init];
    
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@", urlAPI, GetActivityList)]; //POST
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    NSString *username = [ShareData instance].currentUser.userName; //@"I1.ThuanLV"
    
    NSString *postString = @"";
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setObject:username forKey:@"UserName"];
    [dict setObject:contract ?:@"" forKey:@"Contract"];
    [dict setObject:pageNumber forKey:@"PageNumber"];
    postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
    [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endGetActivitiesListOfSale:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    [callOp start];
}

- (void)endGetActivitiesListOfSale:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    // add by DanTT 2015.10.12
    /*Parse json response header*/
    NSString *headerErrorCode = [Common_client parseJsonHeaderResponseAPI:(NSHTTPURLResponse *)response];
    if (![headerErrorCode isEqualToString:@"0"]) {
        handler (nil, headerErrorCode, @"het phien lam viec");
        return;
    }
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *responseResult = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    NSArray *listObject = [responseResult objectForKey:@"ListObject"]?:nil;
    NSString *ErrorCode = StringFormat(@"%@",[responseResult objectForKey:@"ErrorCode"]);
    
    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, @"Có lỗi, vui lòng thử lại!");
        return;
    }
    NSString *listObjectString = [listObject description];
    if([listObjectString isEqualToString:@"(\n)"] || [listObjectString isEqualToString:@"<null>"]){
        handler(nil, @"", @"Không có kết quả trả về!");
        return;
    }
    
    NSMutableArray *mArray = [NSMutableArray array];
    for(NSDictionary *d in listObject){
        ActivityOfSaleRecord*p = [self parseActivityOfSaleRecord:d];
        [mArray addObject:p];
    }
    
    handler(mArray, ErrorCode, @"");
    
}

- (void)confirmNotification:(NSDictionary *)dict completeHander:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    BaseOperation * callOp = [[BaseOperation alloc] init];
    
    NSURL *url = [NSURL URLWithString:StringFormat(@"%@/%@", urlAPI, ConfirmNotification)]; //POST
    callOp.request = [NSMutableURLRequest requestWithURL:url];
    
    NSString *postString = [dict JSONRepresentation];
    
    [(NSMutableURLRequest*)callOp.request setHTTPMethod:@"POST"];
    [(NSMutableURLRequest*)callOp.request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    // add by DanTT 2015.10.12, add info header request
   [Common_client setHeaderRequestAPI:(NSMutableURLRequest *)callOp.request];
    
    [(NSMutableURLRequest*)callOp.request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSString *base64 = [FPTUtility base64:postString];
    NSString *md5 = [FPTUtility md5Encrypt:base64];
    [(NSMutableURLRequest*)callOp.request setValue:[NSString stringWithFormat:@"%@",md5] forHTTPHeaderField:@"checksum"];
    
    
    callOp.completionHandler = ^(NSData *result, NSURLResponse *res) {
        
        [self endConfirmNotification:result response:res completionHandler:handler errorHandler:errHandler];
    };
    
    callOp.errorHandler = ^(NSError *err){
        errHandler(err);
    };
    [callOp start];
}

- (void)endConfirmNotification:(NSData *)result response:(NSURLResponse *)response completionHandler:(DidGetResultBlock)handler errorHandler:(ErrorBlock)errHandler {
    NSInteger httpCode = [(NSHTTPURLResponse*)response statusCode];
    if(httpCode != 200){
        NSError *err = [[NSError alloc] initWithDomain:HTTPDOMAIN code:httpCode userInfo:nil];
        errHandler(err);
        return;
    }
    
    
    /*Parse json response*/
    NSDictionary *jsonDict = [self.parser objectWithData:result];
    NSDictionary *responseResult = [jsonDict objectForKey:@"ResponseResult"]?:nil;
    NSArray *listObject = [responseResult objectForKey:@"ListObject"]?:nil;
    NSString *ErrorCode = StringFormat(@"%@",[responseResult objectForKey:@"ErrorCode"]);

    if(![ErrorCode isEqualToString:@"0"]){
        handler(nil, ErrorCode, @"Có lỗi, vui lòng thử lại!");
        return;
    }
    
    if (listObject.count > 0) {
        NSDictionary *d = listObject[0];
        NSString *resultID = StringFormat(@"%@", d[@"ResultID"] ?:@"");
        NSString *result  = d[@"Result"] ?:@"";
        handler(d,resultID,result);
        return;
    }
    
    handler(nil, ErrorCode, @"Xác nhận thất bại!");
    
}

- (CustomerCareRecord *) ParseRecordList:(NSDictionary *)dict{
    CustomerCareRecord *rc = [[CustomerCareRecord alloc]init];
    NSString *str = @"(null)";
    
    rc.address = StringFormat(@"%@",[dict objectForKey:@"Address"]);
    rc.address = ([rc.address isEqualToString: str]) ? @"": rc.address ;

    rc.contract = StringFormat(@"%@",[dict objectForKey:@"Contract"]);
    rc.contract = ([rc.contract isEqualToString: str]) ? @"": rc.contract ;

    rc.currentPage = StringFormat(@"%@",[dict objectForKey:@"CurrentPage"]);
    rc.currentPage = ([rc.currentPage isEqualToString: str]) ? @"": rc.currentPage ;

    rc.email = StringFormat(@"%@",[dict objectForKey:@"Email"]);
    rc.email = ([rc.email isEqualToString: str]) ? @"": rc.email ;

    rc.fullName = StringFormat(@"%@",[dict objectForKey:@"FullName"]);
    rc.fullName = ([rc.fullName isEqualToString: str]) ? @"": rc.fullName ;

    rc.ID = StringFormat(@"%@",[dict objectForKey:@"ID"]);
    rc.ID = ([rc.ID isEqualToString: str]) ? @"": rc.ID ;

    rc.levelID = StringFormat(@"%@",[dict objectForKey:@"LevelID"]);
    rc.levelID = ([rc.levelID isEqualToString: str]) ? @"": rc.levelID ;

    rc.objStatusDesc = StringFormat(@"%@",[dict objectForKey:@"ObjStatusDesc"]);
    rc.objStatusDesc = ([rc.objStatusDesc isEqualToString: str]) ? @"": rc.objStatusDesc ;

    rc.objStatusName = StringFormat(@"%@",[dict objectForKey:@"ObjStatusName"]);
    rc.objStatusName = ([rc.objStatusName isEqualToString: str]) ? @"": rc.objStatusName ;

    rc.phoneNumber = StringFormat(@"%@",[dict objectForKey:@"PhoneNumber"]);
    rc.phoneNumber = ([rc.phoneNumber isEqualToString: str]) ? @"": rc.phoneNumber ;

    rc.rowNumber = StringFormat(@"%@",[dict objectForKey:@"RowNumber"]);
    rc.rowNumber = ([rc.rowNumber isEqualToString: str]) ? @"": rc.rowNumber ;

    rc.startDate = StringFormat(@"%@",[dict objectForKey:@"StartDate"]);
    rc.startDate = ([rc.startDate isEqualToString: str]) ? @"": rc.startDate ;

    rc.totalPage = StringFormat(@"%@",[dict objectForKey:@"TotalPage"]);
    rc.totalPage = ([rc.totalPage isEqualToString: str]) ? @"": rc.totalPage ;

    rc.totalRow = StringFormat(@"%@",[dict objectForKey:@"TotalRow"]);
    rc.totalRow = ([rc.totalRow isEqualToString: str]) ? @"": rc.totalRow ;

    rc.createDate = StringFormat(@"%@",[dict objectForKey:@"CreateDate"]);
    rc.createDate = ([rc.createDate isEqualToString: str]) ? @"": rc.createDate ;

    rc.createBy = StringFormat(@"%@",[dict objectForKey:@"CreateBy"]);
    rc.createBy = ([rc.createBy isEqualToString: str]) ? @"": rc.createBy ;

    rc.divisionID   = StringFormat(@"%@", dict[@"DivisionID"] ?:@"");
    rc.saleName     = dict[@"SaleName"] ?:@"";
    rc.saleFullName = dict[@"SaleFullName"] ?:@"";
    rc.saleEmail    = dict[@"Email"] ?:@"";
    rc.salePhone    = dict[@"SalePhone"] ?:@"";
    rc.code         = dict[@"Code"] ?:@"";
    return rc;
}

-(ToDoListRecord *) ParseRecordToDoList:(NSDictionary *)dict{
    ToDoListRecord *rc = [[ToDoListRecord alloc]init];
    NSString *str = @"(null)";
    
    rc.contract = StringFormat(@"%@",[dict objectForKey:@"Contract"]);
    rc.contract = ([rc.contract isEqualToString: str]) ? @"": rc.contract ;

    rc.createBy = StringFormat(@"%@",[dict objectForKey:@"CreateBy"]);
    rc.createBy = ([rc.createBy isEqualToString: str]) ? @"": rc.createBy ;

    rc.createDate = StringFormat(@"%@",[dict objectForKey:@"CreateDate"]);
    rc.createDate = ([rc.createDate isEqualToString: str]) ? @"": rc.createDate ;
    
    rc.divisionName = StringFormat(@"%@",[dict objectForKey:@"DivisionName"]);
    rc.divisionName = ([rc.divisionName isEqualToString: str]) ? @"": rc.divisionName;

    rc.currentPage = StringFormat(@"%@",[dict objectForKey:@"CurrentPage"]);
    rc.currentPage = ([rc.currentPage isEqualToString: str]) ? @"": rc.currentPage ;

    rc.fullName = StringFormat(@"%@",[dict objectForKey:@"FullName"]);
    rc.fullName = ([rc.fullName isEqualToString: str]) ? @"": rc.fullName ;

    rc.email = StringFormat(@"%@",[dict objectForKey:@"Email"]);
    rc.email = ([rc.email isEqualToString: str]) ? @"": rc.email ;

    rc.ID = StringFormat(@"%@",[dict objectForKey:@"ID"]);
    rc.ID = ([rc.ID isEqualToString: str]) ? @"": rc.ID ;

    rc.levelID = StringFormat(@"%@",[dict objectForKey:@"LevelID"]);
    rc.levelID = ([rc.levelID isEqualToString: str]) ? @"": rc.levelID ;

    rc.levelName = StringFormat(@"%@",[dict objectForKey:@"LevelName"]);
    rc.levelName = ([rc.levelName isEqualToString: str]) ? @"": rc.levelName ;

    rc.objStatusDesc = StringFormat(@"%@",[dict objectForKey:@"ObjStatusDesc"]);
    rc.objStatusDesc = ([rc.objStatusDesc isEqualToString: str]) ? @"": rc.objStatusDesc ;

    rc.objStatusName = StringFormat(@"%@",[dict objectForKey:@"ObjStatusName"]);
    rc.objStatusName = ([rc.objStatusName isEqualToString: str]) ? @"": rc.objStatusName ;

    rc.phoneNumber = StringFormat(@"%@",[dict objectForKey:@"PhoneNumber"]);
    rc.phoneNumber = ([rc.phoneNumber isEqualToString: str]) ? @"": rc.phoneNumber ;

    rc.processTime = StringFormat(@"%@",[dict objectForKey:@"ProcessTime"]);
    rc.processTime = ([rc.processTime isEqualToString: str]) ? @"": rc.processTime ;

    rc.rowNumber = StringFormat(@"%@",[dict objectForKey:@"RowNumber"]);
    rc.rowNumber = ([rc.rowNumber isEqualToString: str]) ? @"": rc.rowNumber ;

    rc.status = StringFormat(@"%@",[dict objectForKey:@"Status"]);
    rc.status = ([rc.status isEqualToString: str]) ? @"": rc.status ;

    rc.totalPage = StringFormat(@"%@",[dict objectForKey:@"TotalPage"]);
    rc.totalPage = ([rc.totalPage isEqualToString: str]) ? @"": rc.totalPage ;

    rc.totalRow = StringFormat(@"%@",[dict objectForKey:@"TotalRow"]);
    rc.totalRow = ([rc.totalRow isEqualToString: str]) ? @"": rc.totalRow ;

    rc.startDate = StringFormat(@"%@",[dict objectForKey:@"StartDate"]);
    rc.startDate = ([rc.startDate isEqualToString: str]) ? @"": rc.startDate ;
    
    rc.processTimeString = dict[@"ProcessTimeString"] ?:@"";
    rc.endDate  = dict[@"EndDate"] ?:@"";
    rc.statusName = dict[@"StatusName"] ?:@"";
    rc.divisionID = StringFormat(@"%@", dict[@"DivisionID"] ?:@"");
    rc.saleEmail  = dict[@"SaleEmail"] ?:@"";
    rc.salePhone  = dict[@"SalePhone"] ?:@"";
    rc.saleFullName = dict[@"SaleFullName"] ?:@"";
    rc.saleName   = dict[@"SaleName"] ?:@"";
    rc.code       = dict[@"Code"] ?:@"";
    
    return rc;
}

- (ToDoListDetailRecord*) parseToDoListDetailRecord: (NSDictionary*) dict{
    ToDoListDetailRecord *rc = [[ToDoListDetailRecord alloc] init];
    NSString *str = @"(null)";
    
    rc.ID = StringFormat(@"%@",[dict objectForKey:@"ID"]);
    rc.ID = ([rc.ID isEqualToString: str]) ? @"": rc.ID ;

    rc.toDoListID = StringFormat(@"%@",[dict objectForKey:@"ToDoListID"]);
    rc.toDoListID = ([rc.toDoListID isEqualToString: str]) ? @"": rc.toDoListID ;

    rc.careType = StringFormat(@"%@",[dict objectForKey:@"CareType"]);
    rc.careType = ([rc.careType isEqualToString: str]) ? @"": rc.careType ;

    rc.description = StringFormat(@"%@",[dict objectForKey:@"Description"]);
    rc.description = ([rc.description isEqualToString: str]) ? @"": rc.description ;

    rc.createBy = StringFormat(@"%@",[dict objectForKey:@"CreateBy"]);
    rc.createBy = ([rc.createBy isEqualToString: str]) ? @"": rc.createBy ;

    rc.date = StringFormat(@"%@",[dict objectForKey:@"Date"]);
    rc.date = ([rc.date isEqualToString: str]) ? @"": rc.date ;

    rc.status = StringFormat(@"%@",[dict objectForKey:@"Status"]);
    rc.status = ([rc.status isEqualToString: str]) ? @"": rc.status ;

    return rc;
}

- (ActivityRecord*)parseActivityRecord:(NSDictionary*)dict{
    
    ActivityRecord *rc = [[ActivityRecord alloc]init];
    NSString *str = @"(null)";
    
    rc.activity_Name = StringFormat(@"%@",[dict objectForKey:@"Activity_Name"]);
    rc.activity_Name = ([rc.activity_Name isEqualToString: str]) ? @"": rc.activity_Name ;

    rc.activity_Type = StringFormat(@"%@",[dict objectForKey:@"Activity_Type"]);
    rc.activity_Type = ([rc.activity_Type isEqualToString: str]) ? @"": rc.activity_Type ;

    rc.activity_Type_Name = StringFormat(@"%@",[dict objectForKey:@"Activity_Type_Name"]);
    rc.activity_Type_Name = ([rc.activity_Type_Name isEqualToString: str]) ? @"": rc.activity_Type_Name ;

    rc.activity_By = StringFormat(@"%@",[dict objectForKey:@"Activity_by"]);
    rc.activity_By = ([rc.activity_By isEqualToString: str]) ? @"": rc.activity_By ;

    rc.activity_Code = StringFormat(@"%@",[dict objectForKey:@"Activity_code"]);
    rc.activity_Code = ([rc.activity_Code isEqualToString: str]) ? @"": rc.activity_Code ;

    rc.content = StringFormat(@"%@",[dict objectForKey:@"Content"]);
    rc.content = ([rc.content isEqualToString: str]) ? @"": rc.content ;

    rc.contract = StringFormat(@"%@",[dict objectForKey:@"Contract"]);
    rc.contract = ([rc.contract isEqualToString: str]) ? @"": rc.contract ;

    rc.currentPage = StringFormat(@"%@",[dict objectForKey:@"CurrentPage"]);
    rc.currentPage = ([rc.currentPage isEqualToString: str]) ? @"": rc.currentPage ;

    rc.date = StringFormat(@"%@",[dict objectForKey:@"Date"]);
    rc.date = ([rc.date isEqualToString: str]) ? @"": rc.date ;

    rc.objectID = StringFormat(@"%@",[dict objectForKey:@"Objectid"]);
    rc.objectID = ([rc.objectID isEqualToString: str]) ? @"": rc.objectID ;

    rc.remark = StringFormat(@"%@",[dict objectForKey:@"Remark"]);
    rc.remark = ([rc.remark isEqualToString: str]) ? @"": rc.remark ;

    rc.totalPage = StringFormat(@"%@",[dict objectForKey:@"TotalPage"]);
    rc.totalPage = ([rc.totalPage isEqualToString: str]) ? @"": rc.totalPage ;

    rc.totalRow = StringFormat(@"%@",[dict objectForKey:@"TotalRow"]);
    rc.totalRow = ([rc.totalRow isEqualToString: str]) ? @"": rc.totalRow ;
    
    return rc;
}

- (ActivityOfSaleRecord*)parseActivityOfSaleRecord:(NSDictionary*)dict{
    
    ActivityOfSaleRecord *rc = [[ActivityOfSaleRecord alloc]init];
    NSString *str = @"(null)";
    
    rc.activity_Name = StringFormat(@"%@",[dict objectForKey:@"activity_name"]);
    rc.activity_Name = ([rc.activity_Name isEqualToString: str]) ? @"": rc.activity_Name ;

    rc.activity_Type = StringFormat(@"%@",[dict objectForKey:@"activity_type"]);
    rc.activity_Type = ([rc.activity_Type isEqualToString: str]) ? @"": rc.activity_Type ;

    rc.activity_Type_Name = StringFormat(@"%@",[dict objectForKey:@"activity_type_name"]);
    rc.activity_Type_Name = ([rc.activity_Type_Name isEqualToString: str]) ? @"": rc.activity_Type_Name ;

    rc.activity_By = StringFormat(@"%@",[dict objectForKey:@"activity_by"]);
    rc.activity_By = ([rc.activity_By isEqualToString: str]) ? @"": rc.activity_By ;

    rc.activity_Code = StringFormat(@"%@",[dict objectForKey:@"activity_code"]);
    rc.activity_Code = ([rc.activity_Code isEqualToString: str]) ? @"": rc.activity_Code ;

    rc.activity_Date = StringFormat(@"%@",[dict objectForKey:@"activity_date"]);
    rc.activity_Date = ([rc.activity_Date isEqualToString: str]) ? @"": rc.activity_Date ;

    rc.content = StringFormat(@"%@",[dict objectForKey:@"content"]);
    rc.content = ([rc.content isEqualToString: str]) ? @"": rc.content ;

    rc.contract = StringFormat(@"%@",[dict objectForKey:@"contract"]);
    rc.contract = ([rc.contract isEqualToString: str]) ? @"": rc.contract ;

    rc.currentPage = StringFormat(@"%@",[dict objectForKey:@"CurrentPage"]);
    rc.currentPage = ([rc.currentPage isEqualToString: str]) ? @"": rc.currentPage ;

    rc.assign_Date = StringFormat(@"%@",[dict objectForKey:@"assign_date"]);
    rc.assign_Date = ([rc.assign_Date isEqualToString: str]) ? @"": rc.assign_Date ;

    rc.objectID = StringFormat(@"%@",[dict objectForKey:@"objectid"]);
    rc.objectID = ([rc.objectID isEqualToString: str]) ? @"": rc.objectID ;

    rc.remark = StringFormat(@"%@",[dict objectForKey:@"remark"]);
    rc.remark = ([rc.remark isEqualToString: str]) ? @"": rc.remark ;

    rc.totalPage = StringFormat(@"%@",[dict objectForKey:@"TotalPage"]);
    rc.totalPage = ([rc.totalPage isEqualToString: str]) ? @"": rc.totalPage ;

    rc.totalRow = StringFormat(@"%@",[dict objectForKey:@"TotalRow"]);
    rc.totalRow = ([rc.totalRow isEqualToString: str]) ? @"": rc.totalRow ;

    rc.staffID = StringFormat(@"%@",[dict objectForKey:@"staffid"]);
    rc.staffID = ([rc.staffID isEqualToString: str]) ? @"": rc.staffID ;

    rc.customer_Phone = StringFormat(@"%@",[dict objectForKey:@"customer_phone"]);
    rc.customer_Phone = ([rc.customer_Phone isEqualToString: str]) ? @"": rc.customer_Phone ;

    rc.customer_Fullname = StringFormat(@"%@",[dict objectForKey:@"customer_fullname"]);
    rc.customer_Fullname = ([rc.customer_Fullname isEqualToString: str]) ? @"": rc.customer_Fullname ;

    rc.customer_Address = StringFormat(@"%@",[dict objectForKey:@"customer_address"]);
    rc.customer_Address = ([rc.customer_Address isEqualToString: str]) ? @"": rc.customer_Address ;

    return rc;
}
@end
