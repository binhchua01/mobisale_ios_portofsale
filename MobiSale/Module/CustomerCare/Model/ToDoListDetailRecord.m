//
//  ToDoListDetailRecord.m
//  MobiSale
//
//  Created by ISC-DanTT on 12/1/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ToDoListDetailRecord.h"

@implementation ToDoListDetailRecord
@synthesize ID, toDoListID, careType, description, createBy, date, status;
@end
