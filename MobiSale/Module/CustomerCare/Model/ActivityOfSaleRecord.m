//
//  ActivityOfSaleRecord.m
//  MobiSale
//
//  Created by ISC-DanTT on 12/9/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ActivityOfSaleRecord.h"

@implementation ActivityOfSaleRecord
@synthesize activity_Name, activity_Type, activity_Type_Name, activity_By, activity_Code, content, contract, currentPage, objectID, remark, totalPage, totalRow, customer_Address, customer_Fullname, customer_Phone, assign_Date, staffID, activity_Date;
@end
