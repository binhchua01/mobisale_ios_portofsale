//
//  CustomerCareRecord.m
//  MobiSale
//
//  Created by ISC-DanTT on 11/30/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "CustomerCareRecord.h"
#import "SBJson.h"
#import "AppDelegate.h"
#import "ShareData.h"

@implementation StatusDescriptionRecord

- (id)initWithData:(NSDictionary *)dict {
    @try {
        self.code = StringFormat(@"%@", dict[@"code"]);
        self.csat = StringFormat(@"%@", dict[@"csat"]);
        self.point = StringFormat(@"%@", dict[@"point"]);
        self.note = StringFormat(@"%@", dict[@"note"]);
        self.confirmCode = StringFormat(@"%@", dict[@"confirm_code"]);
        self.time = StringFormat(@"%@", dict[@"time"]);

        return self;
        
    } @catch (NSException *exception) {
        
         [CrashlyticsKit recordCustomExceptionName:@"CustomerCareRecord - funtion (saveReceiveRemoteNotification - Error handle StatusDescriptionRecord)" reason:[[ShareData instance].currentUser.userName lowercaseString] frameArray:@[]];
        
        return nil;
        
    }
}

@end

@implementation CustomerCareRecord

- (id)initWithData:(NSDictionary *)dict {
    self.address        = StringFormat(@"%@", dict[@"Address"] ?:@"");
    self.contract       = StringFormat(@"%@", dict[@"Contract"] ?:@"");
    self.currentPage    = StringFormat(@"%@", dict[@"CurrentPage"] ?:@"");
    self.email          = StringFormat(@"%@", dict[@"Email"] ?:@"");
    self.fullName       = StringFormat(@"%@", dict[@"FullName"] ?:@"");
    self.ID             = StringFormat(@"%@", dict[@"ID"] ?:@"");
    self.levelID        = StringFormat(@"%@", dict[@"LevelID"] ?:@"");
    self.objStatusName  = StringFormat(@"%@", dict[@"ObjStatusName"] ?:@"");
    self.objStatusDesc  = StringFormat(@"%@", dict[@"ObjStatusDesc"] ?:@""); // html or json
    self.phoneNumber    = StringFormat(@"%@", dict[@"PhoneNumber"] ?:@"");
    self.rowNumber      = StringFormat(@"%@", dict[@"RowNumber"] ?:@"");
    self.startDate      = StringFormat(@"%@", dict[@"StartDate"] ?:@"");
    self.totalPage      = StringFormat(@"%@", dict[@"TotalPage"] ?:@"");
    self.totalRow       = StringFormat(@"%@", dict[@"TotalRow"] ?:@"");
    self.createDate     = StringFormat(@"%@", dict[@"CreateDate"] ?:@"");
    self.createBy       = StringFormat(@"%@", dict[@"CreateBy"] ?:@"");
    self.divisionID     = StringFormat(@"%@", dict[@"DivisionID"] ?:@"");
    self.saleName       = StringFormat(@"%@", dict[@"SaleName"] ?:@"");
    self.saleFullName   = StringFormat(@"%@", dict[@"SaleFullName"] ?:@"");
    self.saleEmail      = StringFormat(@"%@", dict[@"Email"] ?:@"");
    self.salePhone      = StringFormat(@"%@", dict[@"SalePhone"] ?:@"");
    self.code           = StringFormat(@"%@", dict[@"Code"] ?:@"");
    
    SBJsonParser *parser = [[SBJsonParser alloc] init];
    NSDictionary *dictStatusDesc = [parser objectWithString:self.objStatusDesc];
    if (dictStatusDesc != nil) {
        self.statusDescRecord = [[StatusDescriptionRecord alloc] initWithData:dictStatusDesc];

    }
    
    return  self;
}

@end
