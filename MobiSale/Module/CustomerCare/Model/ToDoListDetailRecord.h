//
//  ToDoListDetailRecord.h
//  MobiSale
//
//  Created by ISC-DanTT on 12/1/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ToDoListDetailRecord : NSObject
@property (nonatomic,strong) NSString*ID;
@property (nonatomic,strong) NSString* toDoListID;
@property (nonatomic,strong) NSString* careType;
@property (nonatomic,strong) NSString* description;
@property (nonatomic,strong) NSString* createBy;
@property (nonatomic,strong) NSString*date;
@property (nonatomic,strong) NSString* status;
@end
