//
//  ActivitiesListViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 12/7/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ActivitiesListViewController.h"
#import "ActivityRecord.h"
#import "ActivityViewCell.h"
#import "Common_client.h"
#import "ShareData.h"
#import "DemoTableFooterView.h"

@interface ActivitiesListViewController (){
    
    IBOutlet NSLayoutConstraint *constraintsTop;
}

@end

@implementation ActivitiesListViewController{
    NSMutableArray *arrList;
    NSMutableArray *arrData;
    DemoTableFooterView *footerView;
    NSInteger totalPage;
    NSInteger currentPage;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (!IS_OS_8_OR_LATER) {
        constraintsTop.constant = 62;
    }
    self.title = @"HOẠT ĐỘNG";
    self.screenName = @"HOẠT ĐỘNG KHÁCH HÀNG";
    arrData = [[NSMutableArray alloc] init];
    // set the custom view for "load more". See DemoTableFooterView.xib.
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DemoTableFooterView" owner:self options:nil];
    footerView = (DemoTableFooterView *)[nib objectAtIndex:0];
    
    // add refresh control when scroll top of tableview
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.listTableView addSubview:refreshControl];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [arrData removeAllObjects];
    // load data
    [self loadDataAtPageNumber:@"1"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    if (arrData.count >0) {
        return arrData.count;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* identifier = @"ActivityCell";
    ActivityViewCell *cell = (ActivityViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ActivityViewCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    CGFloat height = 0;
    if (arrData.count > 0) {
        ActivityRecord *rc = [arrData objectAtIndex:indexPath.section];
        if (rc.activity_Name.length > 21) {
            NSInteger numberOfLines = rc.activity_Name.length/21 +1;
            height = 13*numberOfLines + height;
        }
        else if (rc.activity_Type_Name.length > 13) {
            NSInteger numberOfLines = rc.activity_Type_Name.length/13 +1;
            height = 13*numberOfLines + height;
        }
        if (rc.activity_By.length > 21) {
            NSInteger numberOfLines = rc.activity_By.length/21 +1;
            height = 13*numberOfLines + height;
        }
        if (rc.content.length > 33) {
            NSInteger numberOfLines = rc.content.length/33 +1;
            height = 13*numberOfLines + height;
        }
        if (rc.remark.length > 33) {
            NSInteger numberOfLines = rc.remark.length/33 +1;
            height = 13*numberOfLines + height;
        }
    }
    return 253+height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* identifier = @"ActivityCell";
    ActivityViewCell *cell = (ActivityViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ActivityViewCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    if (arrData.count > 0) {
        ActivityRecord *rc = [arrData objectAtIndex:indexPath.section];
        cell.lblActivityName.text = rc.activity_Name;
        cell.lblActivityTypeName.text = rc.activity_Type_Name;
        cell.lblActivityCode.text = rc.activity_Code;
        cell.lblActivityBy.text = rc.activity_By;
        cell.lblContent.text = rc.content;
        cell.lblRemark.text = rc.remark;
        cell.lblDate.text = rc.date;
        
        [cell.lblActivityTypeName setNumberOfLines:0];
        [cell.lblActivityTypeName sizeToFit];
        
        [cell.lblActivityName setNumberOfLines:0];
        [cell.lblActivityName sizeToFit];
        
        [cell.lblActivityBy setNumberOfLines:0];
        [cell.lblActivityBy sizeToFit];
        
        [cell.lblContent setNumberOfLines:0];
        [cell.lblContent sizeToFit];
        
        [cell.lblRemark setNumberOfLines:0];
        [cell.lblRemark sizeToFit];
    }
    return cell;
}

#pragma mark - Scroll view delegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    if (maximumOffset - currentOffset < 0) {
        [self loadMore];
    }
}

#pragma mark - LoadData
-(void)loadDataAtPageNumber:(NSString*)pageNumber {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    [self showMBProcess];
    ShareData *shared = [ShareData instance];
    [shared.customerCareProxy getActivitiesList:shared.customerCareRecord.contract pageNumber:pageNumber completeHander:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(result == nil){
            [self ShowAlertBoxEmpty];
            [self hideMBProcess];
            [self.listTableView setHidden:YES];
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        arrList = result;
        [arrData addObjectsFromArray:arrList];
        ActivityRecord *rc = [arrList objectAtIndex:0];
        totalPage = [rc.totalPage integerValue];
        currentPage = [rc.currentPage integerValue];
        [self.listTableView reloadData];
        [self hideMBProcess];
        [self loadMoreCompleted];
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
        return;
    }];
}

#pragma refresh table view
- (void)refresh:(UIRefreshControl *)refreshControl {
    [arrData removeAllObjects];
    // load data
    [self loadDataAtPageNumber:@"1"];
    [refreshControl endRefreshing];
}

#pragma mark - Load More
//
// The method -loadMore was called and will begin fetching data for the next page (more).
// Do custom handling of -footerView if you need to.
//
- (void) willBeginLoadingMore {
    [footerView.activityIndicator startAnimating];
}

// Do UI handling after the "load more" process was completed. In this example, -footerView will
// show a "No more items to load" text.
//
- (void)loadMoreCompleted {
    [footerView.activityIndicator stopAnimating];
}

- (void)loadMore {
    // Do your async loading here
    self.listTableView.tableFooterView = footerView;
    currentPage ++;
    if (currentPage <= totalPage) {
        [self willBeginLoadingMore];
        [self loadDataAtPageNumber:StringFormat(@"%li",(long)currentPage)];
        return;
    }
    footerView.infoLabel.hidden = NO;
}
@end
