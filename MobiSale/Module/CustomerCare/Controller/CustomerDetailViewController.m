//
//  CustomerDetailViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 11/30/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "CustomerDetailViewController.h"
#import "Common.h"

@interface CustomerDetailViewController ()<UIAlertViewDelegate>{
    UIAlertView *alertCall_1;
    UIAlertView *alertCall_2;
    UIAlertView *alertSendEmail;
}

@end

@implementation CustomerDetailViewController
@synthesize record;
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"THÔNG TIN";
    self.screenName = @"KS ĐỘ HÀI LÒNG KHÁCH HÀNG";
    [self setType];
    [self loadData];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)loadData{
    self.lblFullName.text = record.fullName;
    self.lblContract.text = record.contract;
//    [self setUnderLineForTitle:self.btnPhoneNumber_1 title:record.phoneNumber];
//    [self setUnderLineForTitle:self.btnEmail title:record.email];
//    [self setUnderLineForTitle:self.btnPhoneNumber_2 title:@""];
    [self.btnPhoneNumber_1 setTitle:record.phoneNumber forState:UIControlStateNormal];
    [self.btnEmail setTitle:record.email forState:UIControlStateNormal];
    [self.btnPhoneNumber_2 setTitle:@"" forState:UIControlStateNormal];
    self.lblEmail.text = record.email;
    self.lblPhoneNumber_1.text = record.phoneNumber;
    self.txtAddress.text = record.address;
    if (self.btnPhoneNumber_1.titleLabel.text.length <=0 ) {
        [self.btnPhoneNumber_1 setEnabled:NO];
    }
    if (self.btnEmail.titleLabel.text.length <= 0) {
        [self.btnEmail setEnabled:NO];
    }
    [self.btnPhoneNumber_2 setEnabled:NO];
}

- (void)setType{
    self.txtAddress.editable = NO;
    self.txtAddress.layer.borderColor= [UIColor colorWithRed:215.0/255.0 green:215.0/255.0 blue:215.0/255.0 alpha:1].CGColor;
    self.txtAddress.layer.borderWidth = 0.7f;
    self.txtAddress.layer.cornerRadius = 6.0f;
}


- (IBAction)btnPhoneNumber1_Clicked:(id)sender {
    alertCall_1 = [[UIAlertView alloc] initWithTitle:self.btnPhoneNumber_1.titleLabel.text message:nil delegate:self cancelButtonTitle:@"Gọi" otherButtonTitles:@"Đóng", nil];
    [alertCall_1 show];
}

- (IBAction)btnPhoneNumber2_Clicked:(id)sender {
    alertCall_2 = [[UIAlertView alloc] initWithTitle:self.btnPhoneNumber_2.titleLabel.text message:nil delegate:self cancelButtonTitle:@"Gọi" otherButtonTitles:@"Đóng", nil];
    [alertCall_2 show];
}

- (IBAction)btnEmail_Clicked:(id)sender {
    alertSendEmail = [[UIAlertView alloc] initWithTitle:self.btnEmail.titleLabel.text message:nil delegate:self cancelButtonTitle:@"Gửi email" otherButtonTitles:@"Đóng", nil];
    [alertSendEmail show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView == alertCall_1) {
        switch (buttonIndex) {
            case 0:{
                [Common callPhone:self.btnPhoneNumber_1.titleLabel.text];
            }
                break;
            case 1:
                break;
            default:
                break;
        }
    }
    if (alertView == alertCall_2) {
        switch (buttonIndex) {
            case 0:{
                [Common callPhone:self.btnPhoneNumber_2.titleLabel.text];
            }
                break;
            case 1:
                break;
            default:
                break;
        }
    }
    if (alertView == alertSendEmail) {
        switch (buttonIndex) {
            case 0:{
                    [self sendEmailToAddress:self.btnEmail.titleLabel.text];
            }
                break;
            case 1:
                break;
            default:
                break;
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
