//
//  CustomerCareListViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 11/30/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "CustomerCareListViewController.h"
#import "CustomerCareListViewCell.h"
#import "ShareData.h"
#import "SiUtils.h"
#import "NIDropDown.h"
#import "DemoTableFooterView.h"
#import <MessageUI/MessageUI.h>
#import "DetailCareCustomerViewController.h"
#import "CustomerDetailViewController.h"
#import "ToDoListViewController.h"
#import "ActivitiesListViewController.h"


@interface CustomerCareListViewController ()<UITableViewDataSource,UITableViewDelegate, MFMailComposeViewControllerDelegate, NIDropDownDelegate>

@end

@implementation CustomerCareListViewController{
    NSMutableArray *arrList;
    NSMutableArray *arrData;
    NIDropDown *dropDown;
    NSArray *arrOptions;
    DemoTableFooterView *footerView;
    NSInteger totalPage;
    NSInteger currentPage;
    IBOutlet NSLayoutConstraint *topSpaceConstraint;
    CGFloat startOffset;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"KS ĐỘ HÀI LÒNG KHÁCH HÀNG";
    
    arrData = [[NSMutableArray alloc]init];
    arrOptions = @[@"Tất cả",@"Số HĐ"];
    //[self.txtSearchContent setUserInteractionEnabled:NO];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    // set the custom view for "load more". See DemoTableFooterView.xib.
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DemoTableFooterView" owner:self options:nil];
    footerView = (DemoTableFooterView *)[nib objectAtIndex:0];
    startOffset = -20;
    
    // add refresh control when scroll top of tableview
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.listTableView addSubview:refreshControl];
    //load data
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [arrData removeAllObjects];
    footerView.infoLabel.hidden = YES;
    [self loadDataWithPageNumber:@"1" agent:@"0" agentName:@"" levelID:self.levelID];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    if (arrData.count >0) {
        return arrData.count;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* identifier = @"CustomerCareListCell";
    CustomerCareListViewCell *cell = (CustomerCareListViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CustomerCareListViewCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    
    CustomerCareRecord *rc = [[CustomerCareRecord alloc] init];
    
    CGFloat heightStatusDesc, heightStatusName, heightContract, heightFullName, heightAddress, heightStatusDescView = 0.0, heightStaffInfo  = 0;
    
    if (arrData.count > 0) {
        rc = [arrData objectAtIndex:indexPath.section];

        heightStatusName = [self getHeightLabelWithString:rc.objStatusName withHeight:21.0f andWidth:186.0f];
        heightFullName   = [self getHeightLabelWithString:rc.fullName      withHeight:21.0f andWidth:251.0f];
        heightContract   = [self getHeightLabelWithString:rc.contract      withHeight:21.0f andWidth:98.0f];
        heightAddress    = [self getHeightLabelWithString:rc.address       withHeight:21.0f andWidth:243.0f];
        
        {
            [cell.lblObjStatusDesc setHTML:rc.objStatusDesc];
            heightStatusDesc = [self getHeightLabelWithString:cell.lblObjStatusDesc.text withHeight:21.0f andWidth:304.0f];
        }
        
        if ([rc.divisionID isEqualToString:@"82"]) {
            heightStaffInfo = 100.0f;
        }
        
        return 290.0f + heightStatusDesc +heightStatusName +heightAddress +heightContract +heightFullName + heightStatusDescView + heightStaffInfo;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* identifier = @"CustomerCareListCell";
    CustomerCareListViewCell *cell = (CustomerCareListViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CustomerCareListViewCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    cell.delegate = self;
    if (arrData.count > 0) {
        CustomerCareRecord *rc = [arrData objectAtIndex:indexPath.section];
        cell.cellModel = rc;
        
    }
    return cell;
}

#pragma mark - TableView didSelected Cell

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.view endEditing:YES];
//    UIViewController * vc = [self setViewCustomerCareInfoAtIndexPath:indexPath];
    DetailCareCustomerViewController *vc = [[DetailCareCustomerViewController alloc] initWithData:[arrData objectAtIndex:indexPath.section]];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Scroll view delegate
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [self.view endEditing:YES];
    if (dropDown !=nil) {
        [dropDown hideDropDown:self.btnOptions];
        [self rel];
    }
    CGFloat currentOffset = scrollView.contentOffset.y;
    if (currentOffset > startOffset) {
        [self hideSearching];
    }else{
        [self showSearching];
    }
    startOffset = currentOffset;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    if (maximumOffset - currentOffset < 0) {
        [self loadMore];
    }
}

- (CGFloat)getHeightLabelWithString:(NSString *)string withHeight:(CGFloat)height andWidth:(CGFloat)width {
    CGSize constraint = CGSizeMake(width, 20000.0f);
        // constratins the size of the table row according to the text
    CGRect textRect = [string boundingRectWithSize:constraint options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:13]} context:nil];
    
    return MAX(textRect.size.height,height) - height;
}

- (IBAction)btnOptions_Clicked:(id)sender {
    [self showDropDownAtButton:sender withArrayData:arrOptions];
}

- (IBAction)btnSearch_Clicked:(id)sender {
    [self searching];
}
/*
#pragma mark - Set view Care Customers
- (UIViewController *)setViewCustomerCareInfoAtIndexPath:(NSIndexPath*) indexPath{
    UITabBarController *tabBar = [[UITabBarController alloc]init];
    CustomerDetailViewController *vc1 = [[CustomerDetailViewController alloc] init];
    ToDoListViewController *vc2 = [[ToDoListViewController alloc] init];
    ActivitiesListViewController *vc3 = [[ActivitiesListViewController alloc] init];
    [vc1.tabBarItem setTitle:@"THÔNG TIN"];
    [vc1.tabBarItem setImage:[UIImage imageNamed:@"Info_32"]];
    [vc2.tabBarItem setTitle:@"SỰ KIỆN"];
    [vc2.tabBarItem setImage:[SiUtils imageWithImageHeight:[UIImage imageNamed:@"toDoList"] height:39]];
    [vc3.tabBarItem setTitle:@"HOẠT ĐỘNG"];
    [vc3.tabBarItem setImage:[SiUtils imageWithImageHeight:[UIImage imageNamed:@"activity_48"] height:39]];
    ShareData *shared = [ShareData instance];
    shared.customerCareRecord = [arrData objectAtIndex:indexPath.section];
    vc1.record = shared.customerCareRecord;
    [tabBar setViewControllers:[NSArray arrayWithObjects:vc1,vc2,vc3, nil]];
    
    tabBar.navigationItem.titleView = [self settitleOfNavigationBar:shared.customerCareRecord.fullName subTitle:shared.customerCareRecord.contract];
    UIColor *tintColor = [UIColor blackColor];
    if ([shared.customerCareRecord.levelID isEqualToString:@"1"]) {
        tintColor = [UIColor greenColor];
    }
    if ([shared.customerCareRecord.levelID isEqualToString:@"2"]) {
        tintColor = [UIColor orangeColor];
    }
    if ([shared.customerCareRecord.levelID isEqualToString:@"3"]) {
        tintColor = [UIColor redColor];
    }
    [tabBar.tabBar setTintColor:tintColor];

    return tabBar;
}
*/
#pragma mark - LoadData
-(void)loadDataWithPageNumber:(NSString*)pageNumber agent:(NSString*)agent agentName:(NSString*)agentName levelID:(NSUInteger)levelID{
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    [self showMBProcess];
    ShareData *shared = [ShareData instance];
    [shared.customerCareProxy getCustomerCareListWithPageNumber:pageNumber agent:agent agentName:agentName levelID:self.levelID completehander:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(result == nil){
            [self ShowAlertBoxEmpty];
            [self hideMBProcess];
            [self.listTableView setHidden:YES];
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        shared.arrCustomerCare = result;
        arrList = result;
        [arrData addObjectsFromArray:arrList];
        CustomerCareRecord *rc = [arrList objectAtIndex:0];
        totalPage = [rc.totalPage integerValue];
        currentPage = [rc.currentPage integerValue];
        [self.listTableView setHidden:NO];
        [self.listTableView reloadData];
        [self hideMBProcess];
        [self loadMoreCompleted];
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
        return;
    }];
}

#pragma mark - CustomerCareListCellDelegate method
- (void)confirmNotificationWithData:(CustomerCareRecord *)data {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:data.statusDescRecord.confirmCode ?:@"" forKey:@"Code"];
    [dict setObject:data.saleName forKey:@"Name"];
    [dict setObject:@"" forKey:@"Note"];
    
    [self showMBProcess];
    
    ShareData *shared = [ShareData instance];
    [shared.customerCareProxy confirmNotification:dict completeHander:^(id result, NSString *errorCode, NSString *message) {
        if ([errorCode isEqual:@"1"]) {
            // Cập nhật thành công
            // ...
        }
        [self showAlertBox:@"Thông báo" message:message];
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        [self showAlertBox:@"Lỗi" message:[error localizedDescription]];
        [self hideMBProcess];

    }];
}

/*
- (UIView*) settitleOfNavigationBar:(NSString*)title subTitle:(NSString*)subTitle {
    UIColor *textColor = [UIColor blackColor];
    ShareData *shared = [ShareData instance];
    if ([shared.customerCareRecord.levelID isEqualToString:@"1"]) {
        textColor = [UIColor greenColor];
    }
    if ([shared.customerCareRecord.levelID isEqualToString:@"2"]) {
        textColor = [UIColor orangeColor];
    }
    if ([shared.customerCareRecord.levelID isEqualToString:@"3"]) {
        textColor = [UIColor redColor];
    }
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = textColor;
    titleLabel.font = [UIFont boldSystemFontOfSize:18];
    titleLabel.text = title;
    [titleLabel sizeToFit];
    
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 19, 0, 0)];
    subTitleLabel.backgroundColor = [UIColor clearColor];
    subTitleLabel.textColor = textColor;
    subTitleLabel.font = [UIFont systemFontOfSize:13];
    subTitleLabel.text = subTitle;
    [subTitleLabel sizeToFit];
    
    UIView *twoLineTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAX(subTitleLabel.frame.size.width, titleLabel.frame.size.width), 30)];
    [twoLineTitleView addSubview:titleLabel];
    [twoLineTitleView addSubview:subTitleLabel];
    
    float widthDiff = subTitleLabel.frame.size.width - titleLabel.frame.size.width;
    
    if (widthDiff > 0) {
        CGRect frame = titleLabel.frame;
        frame.origin.x = widthDiff / 2;
        titleLabel.frame = CGRectIntegral(frame);
    }else{
        CGRect frame = subTitleLabel.frame;
        frame.origin.x = fabsf(widthDiff) / 2;
        subTitleLabel.frame = CGRectIntegral(frame);
    }
    
    return twoLineTitleView;
}
*/
#pragma refresh table view
- (void)refresh:(UIRefreshControl *)refreshControl {
    [arrData removeAllObjects];
    [self loadDataWithPageNumber:@"1" agent:@"0" agentName:@"" levelID:self.levelID];
    [refreshControl endRefreshing];
}

#pragma mark - Load More
//
// The method -loadMore was called and will begin fetching data for the next page (more).
// Do custom handling of -footerView if you need to.
//
- (void) willBeginLoadingMore {
    [footerView.activityIndicator startAnimating];
}

// Do UI handling after the "load more" process was completed. In this example, -footerView will
// show a "No more items to load" text.
//
- (void)loadMoreCompleted {
    [footerView.activityIndicator stopAnimating];
}

- (void)loadMore {
    // Do your async loading here
    self.listTableView.tableFooterView = footerView;
    currentPage ++;
    if (currentPage <= totalPage) {
        [self willBeginLoadingMore];
        [self loadDataWithPageNumber:StringFormat(@"%li",(long)currentPage) agent:@"0" agentName:@"" levelID:self.levelID];
        return;
    }
    footerView.infoLabel.hidden = NO;
    
}

#pragma mark - setShowAndHideKeyboard
-(void)keyboardWillShow {
    // Animate the current view out of the way
    if (dropDown !=nil) {
        [dropDown hideDropDown:self.btnOptions];
        [self rel];
    }
}

#pragma mark - show dropdown list
- (void)handleSingleTap:(UITapGestureRecognizer *) sender{
    [self.view endEditing:YES];
    if (dropDown !=nil) {
        [dropDown hideDropDown:self.btnOptions];
        [self rel];
    }
}

- (void)showDropDownAtButton:(UIButton *)sender withArrayData:(NSArray*)arrayData{
    [self.view endEditing:YES];
    if (dropDown == nil) {
        CGFloat height = 80;
        CGFloat width = 0;
        dropDown = [[NIDropDown alloc] showDropDownAtButton:sender height:&height width:&width data:arrayData images:nil animation:@"down"];
        dropDown.delegate = self;
        return;
    }
    [dropDown hideDropDown:sender];
    [self rel];
}

#pragma mark - NIDropDown Delegate method
- (void)dropDownListView:(NIDropDown *)dropDownListView didSelectIndexPath:(NSIndexPath *)indexPath atButton:(UIButton *)button{
    [self didDropDownSelected:indexPath.row atButton:button];
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
}

-(void)rel{
    dropDown = nil;
}

- (void) didDropDownSelected:(NSInteger)row atButton:(UIButton *)button{
    if (button == self.btnOptions) {
        self.txtSearchContent.text = @"";
        [self.txtSearchContent becomeFirstResponder];
        if (row==0) {
            self.txtSearchContent.placeholder = @"Nhập thông tin cần tìm";
            return;
        }
        self.txtSearchContent.placeholder = @"Nhập mã HĐ cần tìm";
        return;
    }
}

- (void)showSearching {
    topSpaceConstraint.constant = 0;
    [UIView animateWithDuration:0.25
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

- (void)hideSearching {
    topSpaceConstraint.constant = -27;
    [UIView animateWithDuration:0.25
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

#pragma mark - UISearchBar delegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self searching];
}

- (void)searching{
    [self.view endEditing:YES];
    [arrData removeAllObjects];
    if ([self.btnOptions.titleLabel.text isEqualToString:@"Tất cả"]) {
        [self loadDataWithPageNumber:@"1" agent:@"0" agentName:self.txtSearchContent.text levelID:self.levelID];
        return;
    }
    if (self.txtSearchContent.text.length <=0) {
        [self showAlertBox:@"Thông báo" message:@"Vui lòng nhập thông tin cần tìm!"];
        return;
    }
    [self loadDataWithPageNumber:@"1" agent:@"2" agentName:self.txtSearchContent.text levelID:self.levelID];
}

@end
