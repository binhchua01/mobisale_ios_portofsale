//
//  ToDoListDetailViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 12/1/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ToDoListDetailViewController.h"
#import "ToDoListDetailRecord.h"
#import "ToDoListDetailViewCell.h"
#import "ShareData.h"
#import "ToDoListUpdateViewController.h"

@interface ToDoListDetailViewController ()<UITableViewDataSource, UITableViewDelegate, ToDoListUpdateDelegate>

@end

@implementation ToDoListDetailViewController{
    NSMutableArray *arrList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"QUÁ TRÌNH CSKH";
    self.screenName = self.title;
    [self addRightButtonNavaigationItem];
    // add refresh control when scroll top of tableview
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.listTableView addSubview:refreshControl];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    if (arrList.count >0) {
        return arrList.count;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 155;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* identifier = @"ToDoListDetailCell";
    ToDoListDetailViewCell *cell = (ToDoListDetailViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ToDoListDetailViewCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    if (arrList.count > 0) {
        ToDoListDetailRecord *rc = [arrList objectAtIndex:indexPath.section];
        cell.lblCareType.text = rc.careType;
        cell.lblDateProcess.text = rc.date;
        cell.txtDescription.text = rc.description;
        NSString *status;
        if ([rc.status isEqualToString:@"0"]) {
         status = @"Xử lý chưa hoàn tất";
        }else if ([rc.status isEqualToString:@"1"]) {
            status = @"Xử lý hoàn tất";
        }else{
            status = @"";
        }
        cell.lblStatus.text = status ;
    }
    return cell;
}

#pragma mark - LoadData
-(void)loadData{
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    [self showMBProcess];
    ShareData *shared = [ShareData instance];
    [shared.customerCareProxy getToDoListDetailWithContract:shared.customerCareRecord.contract totoListID:shared.toDoListRecord.ID Completehander:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(result == nil){
            [self ShowAlertBoxEmpty];
            [self hideMBProcess];
            [self.listTableView setHidden:YES];
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        arrList = result;
        [self.listTableView reloadData];
        [self hideMBProcess];
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
        return;
    }];
}

#pragma refresh table view
- (void)refresh:(UIRefreshControl *)refreshControl {
    [arrList removeAllObjects];
    [self loadData];
    [refreshControl endRefreshing];
}

- (void) addRightButtonNavaigationItem{
    // add right bar button
    UIImage *image = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"ic_create_potentail_obj"] height:30];
    UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc]
                                       initWithImage:[image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleBordered
                                       target:self
                                       action:@selector(showToDoListUpDateView:)];
    self.navigationItem.rightBarButtonItem = rightBarButton;
}

- (void)showToDoListUpDateView:(id)sender{
    ToDoListUpdateViewController *vc = [[ToDoListUpdateViewController alloc] initWithNibName:@"ToDoListUpdateViewController" bundle:nil];
    vc.delegate = self;
    [self presentPopupViewController:vc animationType:MJPopupViewAnimationSlideBottomTop];
}

-(void)CancelPopup{
    [self loadData];
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideBottomTop];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
