//
//  CutomerCareListPageViewController.h
//  MobiSale
//
//  Created by ISC on 7/27/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewPagerController.h"

@interface CustomerCareListPageViewController : ViewPagerController

@property (assign, nonatomic) NSInteger tabNumber; // Số tab
@property (strong, nonatomic) NSMutableArray *tabTitleArray; // Tiêu đề của các tab

@end
