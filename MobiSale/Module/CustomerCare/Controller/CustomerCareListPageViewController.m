//
//  CutomerCareListPageViewController.m
//  MobiSale
//
//  Created by ISC on 7/27/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "CustomerCareListPageViewController.h"
#import "CustomerCareListViewController.h"
#import "SiUtils.h"

@interface CustomerCareListPageViewController ()<ViewPagerDataSource, ViewPagerDelegate>

@end

@implementation CustomerCareListPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view from its nib.
    
    self.delegate = self;
    self.dataSource = self;
    self.tabNumber = 3;
    self.tabTitleArray = [NSMutableArray arrayWithObjects:@"LEVEL 1", @"LEVEL 2", @"LEVEL 3", nil];
    [self selectTabAtIndex:0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - ViewPagerDataSource methods
- (NSUInteger)numberOfTabsForViewPager:(ViewPagerController *)viewPager {
    return self.tabNumber;
}

- (UIView *)viewPager:(ViewPagerController *)viewPager viewForTabAtIndex:(NSUInteger)index {

    UILabel *label = [UILabel new];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont systemFontOfSize:14.0];
    label.text = self.tabTitleArray[index];

    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    label.numberOfLines = 0;
    [label sizeToFit];
    
    return label;
}

- (UIViewController *)viewPager:(ViewPagerController *)viewPager contentViewControllerForTabAtIndex:(NSUInteger)index {
    CustomerCareListViewController *vc = [[CustomerCareListViewController alloc] init];
    vc.levelID = index + 1;
    return vc;
}

- (CGFloat)viewPager:(ViewPagerController *)viewPager valueForOption:(ViewPagerOption)option withDefault:(CGFloat)value {
    switch (option) {
        case ViewPagerOptionStartFromSecondTab:
            return 1.0;
        case ViewPagerOptionCenterCurrentTab:
            return 0.0;
        case ViewPagerOptionTabLocation:
            return 1.0;
        case ViewPagerOptionTabHeight:
            return 39.0;
        case ViewPagerOptionTabOffset:
            return 36.0;
        case ViewPagerOptionTabWidth:
            //return UIInterfaceOrientationIsLandscape(self.interfaceOrientation) ? 128.0 : 96.0;
            return [SiUtils getScreenFrameSize].width / self.tabNumber;
        case ViewPagerOptionFixFormerTabsPositions:
            return 0.0;
        case ViewPagerOptionFixLatterTabsPositions:
            return 0.0;
        default:
            return value;
    }
}

- (UIColor *)viewPager:(ViewPagerController *)viewPager colorForComponent:(ViewPagerComponent)component withDefault:(UIColor *)color {
    switch (component) {
        case ViewPagerIndicator:
            return [UIColor orangeColor];
        case ViewPagerTabsView:
            return [[UIColor colorWithRed:0.133 green:0.698 blue:0.569 alpha:1] colorWithAlphaComponent:1.0];
        case ViewPagerContent:
            return [UIColor whiteColor];
        default:
            return color;
    }
}

@end
