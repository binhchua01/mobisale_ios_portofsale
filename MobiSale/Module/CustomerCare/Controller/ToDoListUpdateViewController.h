//
//  ToDoListUpdateViewController.h
//  MobiSale
//
//  Created by ISC-DanTT on 12/1/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@protocol ToDoListUpdateDelegate <NSObject>

-(void)CancelPopup;

@end

@interface ToDoListUpdateViewController : BaseViewController
@property (strong, nonatomic) IBOutlet UISwitch *switchDone;
@property (strong, nonatomic) IBOutlet UISwitch *switchNotDone;
@property (strong, nonatomic) IBOutlet UIButton *btnCareType;
@property (strong, nonatomic) IBOutlet UITextView *txtNote;

@property (retain, nonatomic) NSMutableArray *arrCareType;

@property (retain, nonatomic) id <ToDoListUpdateDelegate> delegate;
@end
