//
//  ToDoListViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 11/30/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ToDoListViewController.h"
#import "ToDoListViewCell.h"
#import "ToDoListRecord.h"
#import "ShareData.h"
#import "ToDoListDetailViewController.h"
#import "Common.h"
#import "DemoTableFooterView.h"

@interface ToDoListViewController () <UITableViewDelegate, UITableViewDataSource>{
    NSMutableArray *arrData;
    DemoTableFooterView *footerView;
    NSInteger totalPage;
    NSInteger currentPage;
    IBOutlet NSLayoutConstraint *constraintsTop;
}

@end

@implementation ToDoListViewController{
    NSMutableArray *arrList;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"SỰ KIỆN";
    self.screenName = self.title;
    if (!IS_OS_8_OR_LATER) {
        constraintsTop.constant = 62;
    }
    arrData = [[NSMutableArray alloc]init];
    // set the custom view for "load more". See DemoTableFooterView.xib.
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DemoTableFooterView" owner:self options:nil];
    footerView = (DemoTableFooterView *)[nib objectAtIndex:0];
    
    // add refresh control when scroll top of tableview
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.listTableView addSubview:refreshControl];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [arrData removeAllObjects];
    //load data
    [self loadDataAtPageNumber:@"1"];
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (self.delegate) {
        [self.delegate stopTimer];
    }
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if (self.delegate) {
        [self.delegate stopTimer];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    if (arrData.count >0) {
        return arrData.count;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* identifier = @"ToDoListCell";
    ToDoListViewCell *cell = (ToDoListViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ToDoListViewCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    
    CGFloat heightStatusDesc, heightStatusName, heightDivisionName, heightFullName, heightStatusDescView = 0.0, heightStaffInfo  = 0;
    
    ToDoListRecord *rc = [[ToDoListRecord alloc] init];
    if (arrData.count > 0) {
        rc = [arrData objectAtIndex:indexPath.section];
        heightStatusName = [self getHeightLabelWithString:rc.objStatusName           withHeight:21 andWidth:201];
        heightFullName   = [self getHeightLabelWithString:rc.fullName                withHeight:21 andWidth:137];
        heightDivisionName   = [self getHeightLabelWithString:rc.divisionName        withHeight:21 andWidth:85];
        
        {
            [cell.lblObjStatusDesc setHTML:rc.objStatusDesc];
            heightStatusDesc = [self getHeightLabelWithString:cell.lblObjStatusDesc.text withHeight:21.0f andWidth:304.0f];
        }
        
        if ([rc.divisionID isEqualToString:@"82"]) {
            heightStaffInfo = 100.0f;
        }
        
        return 330 + heightStatusDesc+ heightStatusName+ heightDivisionName+ heightFullName + heightStatusDescView + heightStaffInfo;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* identifier = @"ToDoListCell";
    ToDoListViewCell *cell = (ToDoListViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ToDoListViewCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    cell.delegate = self;
    self.delegate = (id)cell;
    if (arrData.count > 0) {
        ToDoListRecord *rc = [arrData objectAtIndex:indexPath.section];
        
        cell.cellModel = rc;
    }
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Pass the selected object to the new view controller.
    ShareData *shared = [ShareData instance];
    shared.toDoListRecord = [arrData objectAtIndex:indexPath.section];
    // Create the next view controller.
    ToDoListDetailViewController *vc = [[ToDoListDetailViewController alloc] initWithNibName:@"ToDoListDetailViewController" bundle:nil];
    // Push the view controller.
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Scroll view delegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    CGFloat currentOffset = scrollView.contentOffset.y;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    if (maximumOffset - currentOffset < 0) {
        [self loadMore];
    }
}

- (CGFloat)getHeightLabelWithString:(NSString *)string withHeight:(CGFloat)height andWidth:(CGFloat)width {
    CGSize constraint = CGSizeMake(width, 20000.0f);
    // constratins the size of the table row according to the text
    CGRect textRect = [string boundingRectWithSize:constraint options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica Neue" size:13]} context:nil];
    
    return MAX(textRect.size.height,height) - height;
}

#pragma mark - LoadData
-(void)loadDataAtPageNumber:(NSString*)pageNumber {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    [self showMBProcess];
    ShareData *shared = [ShareData instance];
    [shared.customerCareProxy getToDoListWithContract:shared.customerCareRecord.contract pageNumber:@"1" Completehander:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(result == nil){
            [self ShowAlertBoxEmpty];
            [self hideMBProcess];
            [self.listTableView setHidden:YES];
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        shared.arrCustomerCare = result;
        arrList = result;
        [arrData addObjectsFromArray:arrList];
        ToDoListRecord *rc = [arrList objectAtIndex:0];
        totalPage = [rc.totalPage integerValue];
        currentPage = [rc.currentPage integerValue];
        [self.listTableView reloadData];
        [self hideMBProcess];
        [self loadMoreCompleted];
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
        return;
    }];
}

#pragma mark - ToDoListCellDelegate method
- (void)confirmNotificationWithData:(CustomerCareRecord *)data {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:data.statusDescRecord.confirmCode ?:@"" forKey:@"Code"];
    [dict setObject:data.saleName forKey:@"Name"];
    [dict setObject:@"" forKey:@"Note"];
    
    [self showMBProcess];
    
    ShareData *shared = [ShareData instance];
    [shared.customerCareProxy confirmNotification:dict completeHander:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        [self showAlertBox:@"Thông báo" message:message];
        [self hideMBProcess];
        
    } errorHandler:^(NSError *error) {
        
    }];
}

#pragma refresh table view
- (void)refresh:(UIRefreshControl *)refreshControl {
    [arrData removeAllObjects];
    // load data
    [self loadDataAtPageNumber:@"1"];
    [refreshControl endRefreshing];
}

#pragma mark - Load More
//
// The method -loadMore was called and will begin fetching data for the next page (more).
// Do custom handling of -footerView if you need to.
//
- (void) willBeginLoadingMore {
    [footerView.activityIndicator startAnimating];
}

// Do UI handling after the "load more" process was completed. In this example, -footerView will
// show a "No more items to load" text.
//
- (void)loadMoreCompleted {
    [footerView.activityIndicator stopAnimating];
}

- (void)loadMore {
    // Do your async loading here
    self.listTableView.tableFooterView = footerView;
    currentPage ++;
    if (currentPage <= totalPage) {
        [self willBeginLoadingMore];
        [self loadDataAtPageNumber:StringFormat(@"%li",(long)currentPage)];
        return;
    }
    footerView.infoLabel.hidden = NO;
}


@end
