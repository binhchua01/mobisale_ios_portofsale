//
//  ActivitiesListViewController.h
//  MobiSale
//
//  Created by ISC-DanTT on 12/7/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ActivitiesListViewController :BaseViewController<UIScrollViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *listTableView;

@end
