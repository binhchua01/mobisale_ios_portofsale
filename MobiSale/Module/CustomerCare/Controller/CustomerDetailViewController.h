//
//  CustomerDetailViewController.h
//  MobiSale
//
//  Created by ISC-DanTT on 11/30/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomerCareRecord.h"
#include "BaseViewController.h"

@interface CustomerDetailViewController : BaseViewController

@property (retain, nonatomic) CustomerCareRecord *record;

@property (strong, nonatomic) IBOutlet UILabel *lblFullName;
@property (strong, nonatomic) IBOutlet UILabel *lblContract;
@property (strong, nonatomic) IBOutlet UILabel *lblServiceType;
@property (strong, nonatomic) IBOutlet UILabel *lblEmail;
@property (strong, nonatomic) IBOutlet UILabel *lblPhoneNumber_1;
@property (strong, nonatomic) IBOutlet UILabel *lblPhoneNumber_2;
@property (strong, nonatomic) IBOutlet UITextView *txtAddress;
@property (strong, nonatomic) IBOutlet UIButton *btnEmail;
@property (strong, nonatomic) IBOutlet UIButton *btnPhoneNumber_1;
@property (strong, nonatomic) IBOutlet UIButton *btnPhoneNumber_2;

@end
