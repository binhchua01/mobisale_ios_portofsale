//
//  NotificationListViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 12/31/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "NotificationListViewController.h"
#import "CustomerCareRecord.h"
#import "ToDoListViewController.h"
#import "ActivitiesListViewController.h"
#import "CustomerDetailViewController.h"
#import "SiUtils.h"
#import "ShareData.h"

@interface NotificationListViewController (){
    NSMutableArray *arrRecord;
    CustomerCareRecord *record;
}

@end

@implementation NotificationListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"THÔNG BÁO";
    self.screenName = self.title;
    self.listTableView.dataSource = self;
    self.listTableView.delegate = self;

}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // get data from NSUserDefaults
    arrRecord = [[[NSUserDefaults standardUserDefaults] arrayForKey:@"notification"] mutableCopy];
    if (arrRecord.count <= 0) {
        [self.listTableView setHidden:YES];
        self.tabBarItem.badgeValue = 0;
    } else {
        self.tabBarItem.badgeValue = StringFormat(@"%lu",(unsigned long)arrRecord.count);
        [self.listTableView setHidden:NO];
    };
    [self.listTableView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return arrRecord.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 59;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        [cell.textLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Medium" size:18]];
        [cell.detailTextLabel setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:15]];

        
    }
    // Configure the cell...
    record = [[CustomerCareRecord alloc] init];
    record = [self ParseRecordList:[arrRecord objectAtIndex:indexPath.section]];
    
    cell.textLabel.text = StringFormat(@"%@",record.objStatusName);
    cell.detailTextLabel.text = StringFormat(@"%@ : %@ ",record.contract, record.objStatusDesc);
    
    UIImage *cellImage = [SiUtils imageWithImageHeight:[UIImage imageNamed:@"ic_launcher"] height:32];
    cell.imageView.image = cellImage;
    return cell;
    
    return cell;
}



// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


#pragma mark - Table view delegate
// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UIViewController * vc = [self setViewCustomerCareInfoAtIndexPath:indexPath];
    [arrRecord removeObjectAtIndex:indexPath.section];
    [[NSUserDefaults standardUserDefaults] setObject:arrRecord forKey:@"notification"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.navigationController pushViewController:vc animated:YES];
}


-(CustomerCareRecord *)ParseRecordList:(NSDictionary *)dict{
    CustomerCareRecord *rc = [[CustomerCareRecord alloc]init];
    
    rc.address = StringFormat(@"%@",[dict objectForKey:@"Address"]);
    rc.contract = StringFormat(@"%@",[dict objectForKey:@"Contract"]);
    rc.currentPage = StringFormat(@"%@",[dict objectForKey:@"CurrentPage"]);
    rc.email = StringFormat(@"%@",[dict objectForKey:@"Email"]);
    rc.fullName = StringFormat(@"%@",[dict objectForKey:@"FullName"]);
    rc.ID = StringFormat(@"%@",[dict objectForKey:@"ID"]);
    rc.levelID = StringFormat(@"%@",[dict objectForKey:@"LevelID"]);
    rc.objStatusDesc = StringFormat(@"%@",[dict objectForKey:@"ObjStatusDesc"]);
    rc.objStatusName = StringFormat(@"%@",[dict objectForKey:@"ObjStatusName"]);
    rc.phoneNumber = StringFormat(@"%@",[dict objectForKey:@"PhoneNumber"]);
    rc.rowNumber = StringFormat(@"%@",[dict objectForKey:@"RowNumber"]);
    rc.startDate = StringFormat(@"%@",[dict objectForKey:@"StartDate"]);
    rc.totalPage = StringFormat(@"%@",[dict objectForKey:@"TotalPage"]);
    rc.totalRow = StringFormat(@"%@",[dict objectForKey:@"TotalRow"]);
    rc.createDate = StringFormat(@"%@",[dict objectForKey:@"CreateDate"]);
    rc.createBy = StringFormat(@"%@",[dict objectForKey:@"CreateBy"]);
    
    return rc;
}


#pragma mark - Set view Care Customers
- (UIViewController *)setViewCustomerCareInfoAtIndexPath:(NSIndexPath*) indexPath{
    UITabBarController *tabBar = [[UITabBarController alloc]init];
    CustomerDetailViewController *vc1 = [[CustomerDetailViewController alloc] init];
    ToDoListViewController *vc2 = [[ToDoListViewController alloc] init];
    ActivitiesListViewController *vc3 = [[ActivitiesListViewController alloc] init];
    [vc1.tabBarItem setTitle:@"Thông tin"];
    [vc1.tabBarItem setImage:[UIImage imageNamed:@"Info_32"]];
    [vc2.tabBarItem setTitle:@"Sự kiện"];
    [vc2.tabBarItem setImage:[SiUtils imageWithImageHeight:[UIImage imageNamed:@"toDoList"] height:39]];
    [vc3.tabBarItem setTitle:@"Hoạt động"];
    [vc3.tabBarItem setImage:[SiUtils imageWithImageHeight:[UIImage imageNamed:@"activity_48"] height:39]];
    ShareData *shared = [ShareData instance];
    shared.customerCareRecord = [self ParseRecordList:[arrRecord objectAtIndex:indexPath.section]];
    vc1.record = shared.customerCareRecord;
    [tabBar setViewControllers:[NSArray arrayWithObjects:vc1,vc2,vc3, nil]];
    
    tabBar.navigationItem.titleView = [self settitleOfNavigationBar:shared.customerCareRecord.fullName subTitle:shared.customerCareRecord.contract];
    UIColor *tintColor = [UIColor blackColor];
    if ([shared.customerCareRecord.levelID isEqualToString:@"1"]) {
        tintColor = [UIColor redColor];
    }
    if ([shared.customerCareRecord.levelID isEqualToString:@"2"]) {
        tintColor = [UIColor orangeColor];
    }
    if ([shared.customerCareRecord.levelID isEqualToString:@"3"]) {
        tintColor = [UIColor greenColor];
    }
    [tabBar.tabBar setTintColor:tintColor];
    
    return tabBar;
}


- (UIView*) settitleOfNavigationBar:(NSString*)title subTitle:(NSString*)subTitle {
    UIColor *textColor = [UIColor blackColor];
    ShareData *shared = [ShareData instance];
    if ([shared.customerCareRecord.levelID isEqualToString:@"1"]) {
        textColor = [UIColor redColor];
    }
    if ([shared.customerCareRecord.levelID isEqualToString:@"2"]) {
        textColor = [UIColor orangeColor];
    }
    if ([shared.customerCareRecord.levelID isEqualToString:@"3"]) {
        textColor = [UIColor greenColor];
    }
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = textColor;
    titleLabel.font = [UIFont boldSystemFontOfSize:18];
    titleLabel.text = title;
    [titleLabel sizeToFit];
    
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 19, 0, 0)];
    subTitleLabel.backgroundColor = [UIColor clearColor];
    subTitleLabel.textColor = textColor;
    subTitleLabel.font = [UIFont systemFontOfSize:13];
    subTitleLabel.text = subTitle;
    [subTitleLabel sizeToFit];
    
    UIView *twoLineTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAX(subTitleLabel.frame.size.width, titleLabel.frame.size.width), 30)];
    [twoLineTitleView addSubview:titleLabel];
    [twoLineTitleView addSubview:subTitleLabel];
    
    float widthDiff = subTitleLabel.frame.size.width - titleLabel.frame.size.width;
    
    if (widthDiff > 0) {
        CGRect frame = titleLabel.frame;
        frame.origin.x = widthDiff / 2;
        titleLabel.frame = CGRectIntegral(frame);
    }else{
        CGRect frame = subTitleLabel.frame;
        frame.origin.x = fabsf(widthDiff) / 2;
        subTitleLabel.frame = CGRectIntegral(frame);
    }
    
    return twoLineTitleView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
