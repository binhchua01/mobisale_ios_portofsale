//
//  ToDoListUpdateViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 12/1/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "ToDoListUpdateViewController.h"
#import "ShareData.h"
#import "NIDropDown.h"
#import "ToDoListDetailRecord.h"
#import "KeyValueModel.h"
#import "Common.h"

#define kOFFSET_FOR_KEYBOARD 80.0

@interface ToDoListUpdateViewController () <NIDropDownDelegate>{
    KeyValueModel *selectedCareType;
}

@end

@implementation ToDoListUpdateViewController{
    NIDropDown *dropDown;
    NSArray *arrData;
    BOOL isSelected;
    ToDoListDetailRecord *record;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.screenName = @"XỬ LÝ SỰ KIỆN";
    [self setType];
    record = [[ToDoListDetailRecord alloc] init];
    record.status = @"0";
    self.arrCareType = [Common getCareType];
    selectedCareType = [self.arrCareType objectAtIndex:0];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)switchDone_Selected:(id)sender {
    if (self.switchDone.on == YES) {
        record.status = @"1";
        self.switchNotDone.on = NO;
        return;
    }
    if (self.switchDone.on == NO) {
        record.status = @"0";
        self.switchNotDone.on = YES;
        return;
    }
}

- (IBAction)switchNotDone_Selected:(id)sender {
    if (self.switchNotDone.on == YES) {
        record.status = @"0";
        self.switchDone.on = NO;
        return;
    }
    if (self.switchNotDone.on == NO) {
        record.status = @"1";
        self.switchDone.on = YES;
        return;
    }
}

- (IBAction)btnCareType_Clicked:(id)sender {
    [self pareArrayForKeyWith:self.arrCareType button:sender];

}

- (IBAction)btnCancel_Clicked:(id)sender {
    if(self.delegate){
        [self.delegate CancelPopup];
    }
}
- (IBAction)btnUpdate_Clicked:(id)sender {
    [self.view endEditing:YES];
    [self getUpdate];
}

- (void) setType{
    // set textView note
    self.txtNote.layer.borderColor= [UIColor colorWithRed:0 green:0.173 blue:0.294 alpha:1].CGColor;
    self.txtNote.layer.borderWidth = 1.f;
    self.txtNote.layer.cornerRadius = 6.0f;
    
    UITapGestureRecognizer *tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    //tapper.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapper];
    
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

}


- (void)handleSingleTap:(UITapGestureRecognizer *) sender{
    [self.view endEditing:YES];
    if (dropDown !=nil) {
        [dropDown hideDropDown:self.btnCareType];
        [self rel];
    }
    
    
}

- (void) getUpdate {
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        [self hideMBProcess];
        return;
    }
    ShareData* shared = [ShareData instance];
    [self showMBProcess];
    [shared.customerCareProxy updateToDoListDetailWithID:shared.toDoListRecord.ID cateType:selectedCareType.Key descripton:self.txtNote.text status:record.status Completehander:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        [self showAlertBoxWithDelayDismiss:@"Thông báo" message:message];
        [self hideMBProcess];
        [self.delegate CancelPopup];
        
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
    }];
    
}

#pragma mark - setShowAndHideKeyboard
-(void)keyboardWillShow {
    // Animate the current view out of the way
    if (isSelected == NO) {
        [self setViewMovedUp:YES];
        isSelected = YES;
    }
    if (dropDown !=nil) {
        [dropDown hideDropDown:self.btnCareType];
        [self rel];
    }
}

-(void)keyboardWillHide {
    isSelected = NO;
    [self setViewMovedUp:NO];
}


//method to move the view up/down whenever the keyboard is shown/dismissed
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    if (movedUp){
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        //rect.size.height += kOFFSET_FOR_KEYBOARD;
    }
    else{
        // revert back to the normal state.
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        //rect.size.height -= kOFFSET_FOR_KEYBOARD;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
}

#pragma mark - show dropdown list
- (void)pareArrayForKeyWith:(NSArray*)arrInput button:(UIButton*)button{
    int i = 0;
    KeyValueModel *temp;
    NSMutableArray *arrOutput = [[NSMutableArray alloc] init];
    for (i = 0; i < arrInput.count; i++) {
        temp = [arrInput objectAtIndex:i];
        [arrOutput addObject:temp.Values];
    }
    [self showDropDownAtButton:button withArrayData:arrOutput];
}

- (void)showDropDownAtButton:(UIButton *)sender withArrayData:(NSArray*)arrayData{
    [self.view endEditing:YES];
    if (dropDown == nil) {
        CGFloat height = 161;
        CGFloat width = 0;
        dropDown = [[NIDropDown alloc] showDropDownAtButton:sender height:&height width:&width data:arrayData images:nil animation:@"down"];
        dropDown.delegate = self;
        return;
    }
    [dropDown hideDropDown:sender];
    [self rel];
}

#pragma mark - NIDropDown Delegate method
- (void)dropDownListView:(NIDropDown *)dropDownListView didSelectIndexPath:(NSIndexPath *)indexPath atButton:(UIButton *)button{
    [self didDropDownSelected:indexPath.row atButton:button];
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
}

-(void)rel{
    dropDown = nil;
}

- (void) didDropDownSelected:(NSInteger)row atButton:(UIButton *)button{
    if (button == self.btnCareType) {
        selectedCareType = [self.arrCareType objectAtIndex:row];
        return;
    }
}


@end
