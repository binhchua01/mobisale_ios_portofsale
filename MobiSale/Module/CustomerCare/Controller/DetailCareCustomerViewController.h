//
//  DetailCareCustomerViewController.h
//  MobiSale
//
//  Created by ISC on 9/22/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShareData.h"
#import "CustomerCareRecord.h"
#import "ToDoListViewController.h"
#import "CustomerDetailViewController.h"
#import "ActivitiesListViewController.h"

@interface DetailCareCustomerViewController : UITabBarController

@property (strong, nonatomic) CustomerCareRecord *record;
@property (strong, nonatomic) CustomerDetailViewController *customerDetailViewController;
@property (strong, nonatomic) ToDoListViewController *toDoListViewController;
@property (strong, nonatomic) ActivitiesListViewController *activitiesListViewController;

- (instancetype)initWithData:(CustomerCareRecord *)data;

@end
