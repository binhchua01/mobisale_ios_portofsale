//
//  CustomerCareListViewController.h
//  MobiSale
//
//  Created by ISC-DanTT on 11/30/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
@interface CustomerCareListViewController :BaseViewController <UIScrollViewDelegate, UISearchBarDelegate>

@property (assign, nonatomic) NSUInteger levelID;

@property (strong, nonatomic) IBOutlet UITableView *listTableView;
@property (strong, nonatomic) IBOutlet UIButton *btnOptions;
@property (strong, nonatomic) IBOutlet UISearchBar *txtSearchContent;

@end
