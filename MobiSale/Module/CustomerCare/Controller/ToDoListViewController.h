//
//  ToDoListViewController.h
//  MobiSale
//
//  Created by ISC-DanTT on 11/30/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@protocol ToDoListViewDelegate <NSObject>
- (void)stopTimer;
@end

@interface ToDoListViewController : BaseViewController<UIScrollViewDelegate>
@property (strong, nonatomic) IBOutlet UITableView *listTableView;

@property (retain, nonatomic) id<ToDoListViewDelegate> delegate;

@end
