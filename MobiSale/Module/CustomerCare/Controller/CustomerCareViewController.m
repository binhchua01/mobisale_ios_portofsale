//
//  ViewController.m
//  MobiSale
//
//  Created by ISC on 7/27/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "CustomerCareViewController.h"
#import "CustomerCareListPageViewController.h"
#import "NotificationListViewController.h"
#import "ShareData.h"

@interface CustomerCareViewController ()

@end

@implementation CustomerCareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"KS ĐỘ HÀI LÒNG KHÁCH HÀNG";
    [self setViewCustomerCare];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)setViewCustomerCare {
    CustomerCareListPageViewController *vc1 = [[CustomerCareListPageViewController alloc] init];
    NotificationListViewController *vc2 = [[NotificationListViewController alloc] init];
    [vc1.tabBarItem setTitle:@"DS KHÁCH HÀNG"];
    [vc1.tabBarItem setImage:[UIImage imageNamed:@"list_32"]];
    [vc2.tabBarItem setTitle:@"THÔNG BÁO"];
    [vc2.tabBarItem setImage:[UIImage imageNamed:@"notification_32"]];
    
    NSString *userName = [[ShareData instance].currentUser.userName lowercaseString];
    NSString *keyUserDefautls = StringFormat(@"%@-notification",userName);
    NSMutableArray *arrRecord = [[[NSUserDefaults standardUserDefaults] arrayForKey:keyUserDefautls] mutableCopy];
    if (arrRecord.count <= 0) {
        vc2.tabBarItem.badgeValue = 0;
    } else {
        vc2.tabBarItem.badgeValue = StringFormat(@"%lu",(unsigned long)arrRecord.count);
    }
    [self setViewControllers:[NSArray arrayWithObjects:vc1,vc2, nil]];
}

@end
