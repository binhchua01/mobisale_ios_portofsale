//
//  DetailCareCustomerViewController.m
//  MobiSale
//
//  Created by ISC on 9/22/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import "DetailCareCustomerViewController.h"


@implementation DetailCareCustomerViewController

@synthesize customerDetailViewController, toDoListViewController, activitiesListViewController;

- (instancetype)initWithData:(CustomerCareRecord *)data {
    if (self = [super init]) {
        if (data != nil) {
            self.record = data;
        }
        
        [self setView];
    }
    return self;
}

#pragma mark - Set view care Customers
- (void)setView {
    customerDetailViewController = [[CustomerDetailViewController alloc] init];
    toDoListViewController = [[ToDoListViewController alloc] init];
    activitiesListViewController = [[ActivitiesListViewController alloc] init];
    [customerDetailViewController.tabBarItem setTitle:@"THÔNG TIN"];
    [customerDetailViewController.tabBarItem setImage:[UIImage imageNamed:@"Info_32"]];
    [toDoListViewController.tabBarItem setTitle:@"SỰ KIỆN"];
    [toDoListViewController.tabBarItem setImage:[SiUtils imageWithImageHeight:[UIImage imageNamed:@"toDoList"] height:39]];
    [activitiesListViewController.tabBarItem setTitle:@"HOẠT ĐỘNG"];
    [activitiesListViewController.tabBarItem setImage:[SiUtils imageWithImageHeight:[UIImage imageNamed:@"activity_48"] height:39]];
    
    ShareData *shared = [ShareData instance];
    shared.customerCareRecord = self.record;
    customerDetailViewController.record = self.record;
    
    [self setViewControllers:[NSArray arrayWithObjects:customerDetailViewController,toDoListViewController,activitiesListViewController,nil]];
    
    self.navigationItem.titleView = [self settitleOfNavigationBar:shared.customerCareRecord.fullName subTitle:shared.customerCareRecord.contract];
    UIColor *tintColor = [UIColor blackColor];
    if ([shared.customerCareRecord.levelID isEqualToString:@"1"]) {
        tintColor = [UIColor greenColor];
    }
    if ([shared.customerCareRecord.levelID isEqualToString:@"2"]) {
        tintColor = [UIColor orangeColor];
    }
    if ([shared.customerCareRecord.levelID isEqualToString:@"3"]) {
        tintColor = [UIColor redColor];
    }
    [self.tabBar setTintColor:tintColor];
    
}


- (UIView*)settitleOfNavigationBar:(NSString*)title subTitle:(NSString*)subTitle {
    UIColor *textColor = [UIColor blackColor];
    ShareData *shared = [ShareData instance];
    if ([shared.customerCareRecord.levelID isEqualToString:@"1"]) {
        textColor = [UIColor greenColor];
    }
    if ([shared.customerCareRecord.levelID isEqualToString:@"2"]) {
        textColor = [UIColor orangeColor];
    }
    if ([shared.customerCareRecord.levelID isEqualToString:@"3"]) {
        textColor = [UIColor redColor];
    }
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = textColor;
    titleLabel.font = [UIFont boldSystemFontOfSize:18];
    titleLabel.text = title;
    [titleLabel sizeToFit];
    
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 19, 0, 0)];
    subTitleLabel.backgroundColor = [UIColor clearColor];
    subTitleLabel.textColor = textColor;
    subTitleLabel.font = [UIFont systemFontOfSize:13];
    subTitleLabel.text = subTitle;
    [subTitleLabel sizeToFit];
    
    UIView *twoLineTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAX(subTitleLabel.frame.size.width, titleLabel.frame.size.width), 30)];
    [twoLineTitleView addSubview:titleLabel];
    [twoLineTitleView addSubview:subTitleLabel];
    
    float widthDiff = subTitleLabel.frame.size.width - titleLabel.frame.size.width;
    
    if (widthDiff > 0) {
        CGRect frame = titleLabel.frame;
        frame.origin.x = widthDiff / 2;
        titleLabel.frame = CGRectIntegral(frame);
    }else{
        CGRect frame = subTitleLabel.frame;
        frame.origin.x = fabsf(widthDiff) / 2;
        subTitleLabel.frame = CGRectIntegral(frame);
    }
    
    return twoLineTitleView;
}

@end
