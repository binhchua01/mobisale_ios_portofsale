//
//  AcitivitiesListOfSaleViewController.m
//  MobiSale
//
//  Created by ISC-DanTT on 12/9/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import "AcitivitiesListOfSaleViewController.h"
#import "ShareData.h"
#import "Common_client.h"
#import "ActivityOfSaleRecord.h"
#import "ActivityOfSaleViewCell.h"
#import "CustomerDetailViewController.h"
#import "ToDoListViewController.h"
#import "ActivitiesListViewController.h"
#import "CustomerCareRecord.h"
#import "NIDropDown.h"
#import "DemoTableFooterView.h"

@interface AcitivitiesListOfSaleViewController ()<NIDropDownDelegate>{
    NSMutableArray *arrList;
    NSMutableArray *arrData;
    NIDropDown *dropDown;
    NSArray *arrOptions;
    DemoTableFooterView *footerView;
    NSInteger totalPage;
    NSInteger currentPage;
    IBOutlet NSLayoutConstraint *topSpaceConstraint;
    CGFloat startOffset;
}

@end

@implementation AcitivitiesListOfSaleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"HOẠT ĐỘNG KHÁCH HÀNG";
    self.screenName = self.title;
    arrData = [[NSMutableArray alloc]init];
    arrOptions = @[@"Tất cả",@"Số HĐ"];
    //[self.txtSearchContent setUserInteractionEnabled:NO];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    // set the custom view for "load more". See DemoTableFooterView.xib.
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"DemoTableFooterView" owner:self options:nil];
    footerView = (DemoTableFooterView *)[nib objectAtIndex:0];
    startOffset = -20;
    
    // add refresh control when scroll top of tableview
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [self.listTableView addSubview:refreshControl];

}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    //load data
    [arrData removeAllObjects];
    footerView.infoLabel.hidden = YES;
    [self loadDataAtPageNumber:@"1" contract:@""];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    if (arrData.count >0) {
        return arrData.count;
    }
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* identifier = @"ActivityOfSaleCell";
    ActivityOfSaleViewCell *cell = (ActivityOfSaleViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ActivityOfSaleViewCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    CGFloat height = 0;
    NSInteger numberOfLines = 0;
    if (arrData.count > 0) {
        ActivityOfSaleRecord *rc = [arrData objectAtIndex:indexPath.section];
        if (rc.activity_Name.length > 21) {
            numberOfLines = rc.activity_Name.length/21 +1;
            height = 13*numberOfLines + height;
        }
        else if (rc.activity_Type_Name.length > 13) {
            numberOfLines = rc.activity_Type_Name.length/13 +1;
            height = 13*numberOfLines + height;
        }
        if (rc.customer_Fullname.length > 29) {
            numberOfLines = rc.customer_Fullname.length/29 +1;
            height = 13*numberOfLines + height;

        }
        if (rc.activity_By.length > 21) {
            numberOfLines = rc.activity_By.length/21 +1;
            height = 13*numberOfLines + height;
        }
        if (rc.content.length > 33) {
            numberOfLines = rc.content.length/33 +1;
            height = 13*numberOfLines + height;
        }
        if (rc.remark.length > 33) {
            numberOfLines = rc.remark.length/33 +1;
            height = 13*numberOfLines + height;
        }
    }
    return 304+height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString* identifier = @"ActivityOfSaleCell";
    ActivityOfSaleViewCell *cell = (ActivityOfSaleViewCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ActivityOfSaleViewCell" owner:self options:nil];
    cell = [nib objectAtIndex:0];
    if (arrData.count > 0) {
        ActivityOfSaleRecord *rc = [arrData objectAtIndex:indexPath.section];
        cell.lblFullname.text = rc.customer_Fullname;
        cell.lblActivityName.text = rc.activity_Name;
        cell.lblActivityTypeName.text = rc.activity_Type_Name;
        cell.lblActivityCode.text = rc.activity_Code;
        cell.lblActivityBy.text = rc.activity_By;
        cell.lblContent.text = rc.content;
        cell.lblRemark.text = rc.remark;
        cell.lblActivityDate.text = rc.activity_Date;
        cell.lblAssignDate.text = rc.assign_Date;
        
       // auto fix content cell
        [cell.lblContent setNumberOfLines:0];
        [cell.lblContent sizeToFit];
        
        [cell.lblFullname setNumberOfLines:0];
        [cell.lblFullname sizeToFit];
        
        [cell.lblActivityTypeName setNumberOfLines:0];
        [cell.lblActivityTypeName sizeToFit];
        
        [cell.lblActivityName setNumberOfLines:0];
        [cell.lblActivityName sizeToFit];
        
        [cell.lblActivityBy setNumberOfLines:0];
        [cell.lblActivityBy sizeToFit];
        
        [cell.lblContent setNumberOfLines:0];
        [cell.lblContent sizeToFit];
        
        [cell.lblRemark setNumberOfLines:0];
        [cell.lblRemark sizeToFit];
    }
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UIViewController * vc = [self setViewCustomerCareInfoAtIndexPath:indexPath];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Scroll view delegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    CGFloat currentOffset = scrollView.contentOffset.y;
    if (currentOffset > startOffset) {
        [self hideSearching];
    }else{
        [self showSearching];
    }
    startOffset = currentOffset;
    CGFloat maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
    if (maximumOffset - currentOffset < 0) {
        [self loadMore];
    }
}

- (IBAction)btnOptions_Clicked:(id)sender {
    [self showDropDownAtButton:sender withArrayData:arrOptions];
}

- (IBAction)btnSearch_Clicked:(id)sender {
    [self searching];
}

#pragma mark - LoadData
-(void)loadDataAtPageNumber:(NSString*)pageNumber contract:(NSString*)contract{
    if (![Common_client isNetworkAvailable]) {
        [Common_client showAlertMessage:Mesage_Network];
        return;
    }
    [self showMBProcess];
    ShareData *shared = [ShareData instance];
    [shared.customerCareProxy getActivitiesListOfSale:contract pageNumber:pageNumber completeHander:^(id result, NSString *errorCode, NSString *message) {
        if ([message isEqualToString:@"het phien lam viec"]) {
            [self ShowAlertErrorSession];
            [self LogOut];
            [self hideMBProcess];
            return;
        }
        if(result == nil){
            [self ShowAlertBoxEmpty];
            [self hideMBProcess];
            [self.listTableView setHidden:YES];
            return;
        }
        if(![errorCode isEqualToString:@"0"]){
            [Common_client showAlertMessage:[NSString stringWithFormat:@"%@",message]];
            [self hideMBProcess];
            return ;
        }
        arrList =result ;
        [arrData addObjectsFromArray:arrList];
        ActivityOfSaleRecord *rc= [arrList objectAtIndex:0];
        totalPage = [rc.totalPage integerValue];
        currentPage = [rc.currentPage integerValue];
        [self.listTableView setHidden:NO];
        [self.listTableView reloadData];
        [self hideMBProcess];
        [self loadMoreCompleted];
    } errorHandler:^(NSError *error) {
        [Common_client showAlertMessage:[NSString stringWithFormat:@"Có lỗi -%@",[error localizedDescription]]];
        [self hideMBProcess];
        return;
    }];
}

#pragma mark - Set view Care Customers
- (UIViewController *)setViewCustomerCareInfoAtIndexPath:(NSIndexPath*) indexPath{
    UITabBarController *tabBar = [[UITabBarController alloc]init];
    CustomerDetailViewController *vc1 = [[CustomerDetailViewController alloc] init];
    ToDoListViewController *vc2 = [[ToDoListViewController alloc] init];
    ActivitiesListViewController *vc3 = [[ActivitiesListViewController alloc] init];
    [vc1.tabBarItem setTitle:@"THÔNG TIN"];
    [vc1.tabBarItem setImage:[UIImage imageNamed:@"Info_32"]];
    [vc2.tabBarItem setTitle:@"SỰ KIỆN"];
    [vc2.tabBarItem setImage:[SiUtils imageWithImageHeight:[UIImage imageNamed:@"toDoList"] height:39]];
    [vc3.tabBarItem setTitle:@"HOẠT ĐỘNG"];
    [vc3.tabBarItem setImage:[SiUtils imageWithImageHeight:[UIImage imageNamed:@"activity_48"] height:39]];
    ShareData *shared = [ShareData instance];
    shared.customerCareRecord = [self parseActivityOfSaleRecord: [arrData objectAtIndex:indexPath.section]];
    vc1.record = shared.customerCareRecord;
    [tabBar setViewControllers:[NSArray arrayWithObjects:vc1,vc2,vc3, nil]];
    
    tabBar.navigationItem.titleView = [self settitleOfNavigationBar:shared.customerCareRecord.fullName subTitle:shared.customerCareRecord.contract];
    return tabBar;
}


- (UIView*) settitleOfNavigationBar:(NSString*)title subTitle:(NSString*)subTitle {
    UIColor *textColor = [UIColor blackColor];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = textColor;
    titleLabel.font = [UIFont boldSystemFontOfSize:18];
    titleLabel.text = title;
    [titleLabel sizeToFit];
    
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 19, 0, 0)];
    subTitleLabel.backgroundColor = [UIColor clearColor];
    subTitleLabel.textColor = textColor;
    subTitleLabel.font = [UIFont systemFontOfSize:13];
    subTitleLabel.text = subTitle;
    [subTitleLabel sizeToFit];
    
    UIView *twoLineTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAX(subTitleLabel.frame.size.width, titleLabel.frame.size.width), 30)];
    [twoLineTitleView addSubview:titleLabel];
    [twoLineTitleView addSubview:subTitleLabel];
    
    float widthDiff = subTitleLabel.frame.size.width - titleLabel.frame.size.width;
    
    if (widthDiff > 0) {
        CGRect frame = titleLabel.frame;
        frame.origin.x = widthDiff / 2;
        titleLabel.frame = CGRectIntegral(frame);
    }else{
        CGRect frame = subTitleLabel.frame;
        frame.origin.x = fabsf(widthDiff) / 2;
        subTitleLabel.frame = CGRectIntegral(frame);
    }
    
    return twoLineTitleView;
}

-(CustomerCareRecord*)parseActivityOfSaleRecord:(ActivityOfSaleRecord *)record{
    CustomerCareRecord *rc = [[CustomerCareRecord alloc]init];
    
    rc.address = record.customer_Address;
    rc.contract = record.contract;
    rc.currentPage = record.currentPage;
    rc.email = @"";
    rc.fullName = record.customer_Fullname;
    rc.ID = record.objectID;
    rc.levelID = @"";
    rc.objStatusDesc = record.content;
    rc.objStatusName = record.activity_Name;
    rc.phoneNumber = record.customer_Phone;
    rc.rowNumber = @"";
    rc.startDate =record.activity_Date;
    rc.totalPage =record.totalPage;
    rc.totalRow = record.totalRow;
    rc.createDate = record.assign_Date;
    rc.createBy = record.activity_By;
    
    return rc;
}

#pragma refresh table view
- (void)refresh:(UIRefreshControl *)refreshControl {
    [arrData removeAllObjects];
    // load data
    [self loadDataAtPageNumber:@"1" contract:@""];
    [refreshControl endRefreshing];
}

#pragma mark - Load More
//
// The method -loadMore was called and will begin fetching data for the next page (more).
// Do custom handling of -footerView if you need to.
//
- (void) willBeginLoadingMore {
    [footerView.activityIndicator startAnimating];
}

// Do UI handling after the "load more" process was completed. In this example, -footerView will
// show a "No more items to load" text.
//
- (void)loadMoreCompleted {
    [footerView.activityIndicator stopAnimating];
}

- (void)loadMore {
    // Do your async loading here
    self.listTableView.tableFooterView = footerView;
    currentPage ++;
    if (currentPage <= totalPage) {
        [self willBeginLoadingMore];
        [self loadDataAtPageNumber:StringFormat(@"%li",(long)currentPage) contract:@""];
        return;
    }
    footerView.infoLabel.hidden = NO;
}


#pragma mark - setShowAndHideKeyboard
-(void)keyboardWillShow {
    // Animate the current view out of the way
    if (dropDown !=nil) {
        [dropDown hideDropDown:self.btnOptions];
        [self rel];
    }
}

#pragma mark - show dropdown list
- (void)handleSingleTap:(UITapGestureRecognizer *) sender{
    [self.view endEditing:YES];
    if (dropDown !=nil) {
        [dropDown hideDropDown:self.btnOptions];
        [self rel];
    }
}

- (void)showDropDownAtButton:(UIButton *)sender withArrayData:(NSArray*)arrayData{
    [self.view endEditing:YES];
    if (dropDown == nil) {
        CGFloat height = 80;
        CGFloat width = 0;
        dropDown = [[NIDropDown alloc] showDropDownAtButton:sender height:&height width:&width data:arrayData images:nil animation:@"down"];
        dropDown.delegate = self;
        return;
    }
    [dropDown hideDropDown:sender];
    [self rel];
}

#pragma mark - NIDropDown Delegate method
- (void)dropDownListView:(NIDropDown *)dropDownListView didSelectIndexPath:(NSIndexPath *)indexPath atButton:(UIButton *)button{
    [self didDropDownSelected:indexPath.row atButton:button];
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
}

-(void)rel{
    dropDown = nil;
}

- (void) didDropDownSelected:(NSInteger)row atButton:(UIButton *)button{
    if (button == self.btnOptions) {
        self.txtSearchContent.text = @"";
        [self.txtSearchContent becomeFirstResponder];
        if (row==0) {
            self.txtSearchContent.placeholder = @"Nhập thông tin cần tìm";
            return;
        }
        self.txtSearchContent.placeholder = @"Nhập mã HĐ cần tìm";
        return;
    }
}


- (void)showSearching {
    topSpaceConstraint.constant = 69;
    [UIView animateWithDuration:0.25
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

- (void)hideSearching {
    topSpaceConstraint.constant = 29;
    [UIView animateWithDuration:0.25
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

#pragma mark - UISearchBar delegate
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [self searching];
}

- (void)searching {
    [self.view endEditing:YES];
    [arrData removeAllObjects];
    if ([self.btnOptions.titleLabel.text isEqualToString:@"Tất cả"]) {
        [self loadDataAtPageNumber:@"1" contract:self.txtSearchContent.text];
        return;
    }
    if (self.txtSearchContent.text.length <=0) {
        [self showAlertBox:@"Thông báo" message:@"Vui lòng nhập thông tin cần tìm!"];
        return;
    }
    [self loadDataAtPageNumber:@"1" contract:self.txtSearchContent.text];
}
@end
