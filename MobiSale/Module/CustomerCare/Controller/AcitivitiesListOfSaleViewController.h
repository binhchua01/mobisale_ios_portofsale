//
//  AcitivitiesListOfSaleViewController.h
//  MobiSale
//
//  Created by ISC-DanTT on 12/9/15.
//  Copyright (c) 2015 FPT.RAD.FTool. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface AcitivitiesListOfSaleViewController : BaseViewController <UIScrollViewDelegate, UISearchBarDelegate>
@property (strong, nonatomic) IBOutlet UITableView *listTableView;
@property (strong, nonatomic) IBOutlet UIButton *btnOptions;
@property (strong, nonatomic) IBOutlet UISearchBar *txtSearchContent;
@property (strong, nonatomic) IBOutlet UIButton *btnSearch;

@end
