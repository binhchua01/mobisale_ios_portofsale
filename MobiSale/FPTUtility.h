//
//  FPTUtility.h
//  MobiSale
//
//  Created by TranTuanVu on 12/6/16.
//  Copyright © 2016 FPT.RAD.FTool. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FPTUtility : NSObject

+ (NSString *) base64:(NSString *) input;
+ (NSString *) md5Encrypt:(NSString *) input;

@end
